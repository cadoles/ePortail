################################
# Makefile pour eole-eportail
################################

SOURCE=eportail
VERSION=3.0
EOLE_VERSION=2.3
PKGAPPS=web
#FLASK_MODULE=<APPLICATION>

################################
# Début de zone à ne pas éditer
################################

include eole.mk
include apps.mk

################################
# Fin de zone à ne pas éditer
################################

# Makefile rules dedicated to application
# if exists
ifneq (, $(strip $(wildcard $(SOURCE).mk)))
include $(SOURCE).mk
endif
