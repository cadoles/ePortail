# -*- coding: utf-8 -*-
"""
    Update de la base balado à chaque reconfigure
"""

from eolesql.db_test import test_var

DB = "eportail"
FILENAMES = ['/usr/share/eole/mysql/eole-eportail/updates/eportail-update-1.sql']

def test_response(result=None):
    """
        Renvoie True si eportail est activé
    """
    return test_var('activer_eportail')

conf_dict = dict(db=DB, test_active=test_response, filenames=FILENAMES,
                 version='Mise à jour de ePortail')
