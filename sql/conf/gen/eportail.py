#-*- coding: utf-8 -*-
##############################################################################
#
# eportail.py
#
##############################################################################
"""
    Config pour eportail
"""
from eolesql.db_test import db_exists, test_var

eportail_TABLEFILENAMES = ['/usr/share/eole/mysql/eole-eportail/gen/eportail-create-1.sql']

def test():
    """
    test l'existence de la base de donnée eportail
    """
    return test_var('activer_eportail') and not db_exists('eportail')

conf_dict = dict(filenames=eportail_TABLEFILENAMES, test=test)

