<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?
	include_once("include/include.php");				/* Général		*/
	include_once($config["templatedir"]."/html.php");	/* Entete HTML	*/
	include_once($config["templatedir"]."/popup.php");
	$db1=new ps_db;
	
	$idpan=$_GET['idpan'];
	if($idpan=="") $idpan=$_POST['$idpan']; 
	$lbpan=$_POST['lbpan'];
	$lburl=$_POST['lburl'];
	$fgweb=$_POST['fgweb'];
	
	
	// Création d'une page Editeur
	if($fgweb!="") {
		// Recherche du dernier numéro d'ordre de panel de l'utilisateur 
		$q="SELECT panel_order FROM env_panel WHERE panel_user_id=".$_SESSION['user_id']." AND panel_visible ORDER BY panel_order DESC";
		$db1->query($q);
		$order=1;
		if($db1->next_record()) {
			$order=$db1->f('panel_order')+1;
		}
	
		// Création du panel
		$q="UPDATE env_panel SET panel_label='$lbpan', panel_url='$lburl' WHERE panel_id=$idpan";
		$db1->query($q);
		
		$jsaction="recharge(".$idpan.")";
	}	
?>

<script type="text/JavaScript">
	function recharge(idpan) {
		parent.document.location.href='index.php?idpan='+idpan;
	}
	<?php echo $jsaction ?>
</script>

</head>

<body>

<?
	$q="SELECT * FROM env_panel WHERE panel_id=$idpan;";
	$db1->query($q);
	if($db1->next_record()) {
		$lbpan=$db1->f('panel_label');
		$lburl=$db1->f('panel_url');
		$tppan=$db1->f('panel_type_id');
	}
?>

<h1>Modification d'un onglet</h1>

<div id="envolepage">
<form name="Formulaire" method="post">
<input id='idpan' name='idpan' type='hidden' value="<? echo $idpan; ?>">

<h2>Donnez un nom à votre nouvel onglet</h2>
<input id="panel_label" name="lbpan" class="biginput" type="text" size="150" style="width: 99%" value="<? echo $lbpan; ?>">

<h2 <? if($tppan!=1) echo "style='display:none'" ?>>Saisir l'adresse du site</h2>
<input id="panel_url" name="lburl" class="biginput" type="text" size="150" style="width: 99%; <? if($tppan!=1) echo "display:none;" ?>" value="<? echo $lburl; ?>">

<br><br>
<input id="fgweb" name="fgweb" class="input-rounded-button" type="submit" value="Choisir" style="width: 99%" />

<script type="text/javascript">
		$('#panel_label').focus();
</script>
	
</form>
</div>

</body>
</html>
