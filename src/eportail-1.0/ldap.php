<?
	
	include_once("include/envole.php");
	include_once("include/mysql.php");
	include_once("include/delete.php");
	include_once("include/session.php");
	include_once($config["templatedir"]."/html.php");	/* Entete HTML		*/
	include_once($config["templatedir"]."/frame.php");	/* Entete HTML		*/
		
	$db1=new ps_db;

	echo "</head>";
	echo "<body>";
	
	echo "<h1>SYNCHRONISATION LDAP</h1>";
	if(!$config["activerLDAP"]) {
		die("<h3>Module LDAP désactiver</h3>");
	}

	
	echo "<br><h3>CONNEXION</h3>";
	$ds = ldap_connect ( $config['LDAPserver'], $config['LDAPport'] );
	
	if ($ds) {
		$r=ldap_bind($ds,$config['LDAProotdn'],$config['LDAProotpw']);
		
		// Parcourt de l'ensemble des utilisateurs LDAP pour synchronisation avec base
		echo "<br><br><h3>SYNCHRONISATION UTILISATEUR LDAP > BDD</h3>";
		$res = ldap_search($ds,$config['LDAPracine'],$config['LDAPFiltreUser']);
		$entryID=ldap_first_entry($ds,$res);
	
		for ($entryID=ldap_first_entry($ds,$res); $entryID!=false; $entryID=ldap_next_entry($ds,$entryID))
		{
			// Récupération des informations de l'utilisateur
			$login=ldap_get_values($ds,$entryID,$config['LDAPLoginAttribut']);
			$lastname=ldap_get_values($ds,$entryID,$config['LDAPLastNameAttribut']);
			$firstname=ldap_get_values($ds,$entryID,$config['LDAPFirstNameAttribut']);
			$email=ldap_get_values($ds,$entryID,$config['LDAPEmailAttribut']);
			$ldapadmin=ldap_get_values($ds,$entryID,$config['LDAPAdminAttribut']);
			$ldapprofil=ldap_get_values($ds,$entryID,$config['LDAPProfilAttribut']);
			if($ldapadmin[0]=="1") {
				$profil=1;
			}
			else {
				$profil = $config['LDAPProfil'][$ldapprofil[0]];
			}
			
			// Rechercher si l'utilisateur existe déjà
			echo "<br>Mise à jour de l'utilisateur = ".$login[0];
			$q="SELECT user_id FROM env_user WHERE user_login='$login[0]'";
			$db1->query($q);
			if($db1->next_record()) {
				$q="UPDATE env_user SET user_lastname='$lastname[0]', user_firstname='$firstname[0]', user_email='$email[0]', user_profil_id=$profil WHERE user_login='$login[0]'";
				$db1->query($q);
			}
			else {
				$q="INSERT INTO env_user(user_login, user_lastname, user_firstname, user_email, user_avatar, user_profil_id, user_mod) VALUES('$login[0]','$lastname[0]','$firstname[0]','$email[0]','anonyme.jpg',$profil,'big')";
				$db1->query($q);
			}
		}
		
		if($config['LDAPDelUser']) {
			echo "<br><br><h3>SYNCHRONISATION UTILISATEUR BDD > LDAP</h3>";
			$q="SELECT user_id, user_login FROM env_user";
			$db1->query($q);
			$fgtrv=false;
			while($db1->next_record()) {	
				$res = ldap_search($ds,$config['LDAPracine'],str_replace("*",$db1->f("user_login"),$config['LDAPFiltreUser']));
				$entryID=ldap_first_entry($ds,$res);
				if(!$entryID) {
					$fgtrv=true;
					echo "<br>Suppression de l'utilisateur = ".$db1->f("user_login");
					delUser($db1->f("user_id"));
				}
			}
			
			if(!$fgtrv) echo "<br>Aucune modification nécessaire";
		}
		
		// Récupération de l'ID de l'utilisateur Admin
		$q="SELECT user_id FROM env_user WHERE user_login='admin'";
		$db1->query($q);
		if(!$db1->next_record()) die("<br><br>Erreur Fatale aucun utilisateur Admin");
		$idadm=$db1->f("user_id");
		
		// Parcourt des groupes LDAP
		echo "<br><br><h3>SYNCHRONISATION GROUPE LDAP > BDD</h3>";
		$r=ldap_bind($ds,$config['LDAProotdn'],$config['LDAProotpw']);
		$res = ldap_search($ds,$config['LDAPracine'],$config['LDAPFiltreGroup']);
		$entryID=ldap_first_entry($ds,$res);		

		for ($entryID=ldap_first_entry($ds,$res); $entryID!=false; $entryID=ldap_next_entry($ds,$entryID))
		{
			
			// Récupération des informations du groupe
			$ldapid=ldap_get_values($ds,$entryID,$config['LDAPGrpIdAttribut']);
			$name=ldap_get_values($ds,$entryID,$config['LDAPGrpDisplayNameAttribut']);
			$description=ldap_get_values($ds,$entryID,$config['LDAPGrpDescriptionAttribut']);
			$type=ldap_get_values($ds,$entryID,$config['LDAPGrpTypeAttribut']);
			$member=ldap_get_values($ds,$entryID,$config['LDAPGrpMemberAttribut']);
			
			if($name[0]=="") $name[0]=$description[0];
			if($type[0]=="") $type[0]="Base";
			
			// Rechercher si le groupe existe déjà
			echo "<br>Mise à jour du groupe = ".$ldapid[0]." ".$name[0];
			
			$q="SELECT group_id FROM env_group WHERE group_ldapid=$ldapid[0]";
			$db1->query($q);
			if($db1->next_record()) {
				$q="UPDATE env_group SET group_name='$name[0]', group_label='$description[0]', group_category='$type[0]', group_user_id=$idadm, group_public=false WHERE group_ldapid=$ldapid[0]";
				$db1->query($q);
			}
			else {
				$q="INSERT INTO env_group(group_name, group_label, group_category, group_user_id, group_public, group_ldapid, group_avatar) VALUES('$name[0]','$description[0]','$type[0]',$idadm,false,$ldapid[0],'anonyme.jpg')";
				$db1->query($q);
			}
			
			// Rattachement des utilisateurs au groupe
			$q="SELECT group_id FROM env_group WHERE group_ldapid=$ldapid[0]";
			$db1->query($q);
			if($db1->next_record()) {
				// Récupération de l'id du groupe
				$idgrp=$db1->f("group_id");
				
				// Suppression des utilisateurs du groupe
				$q="DELETE FROM env_groupuser WHERE groupuser_group_id=$idgrp";
				$db1->query($q);
				
				// Parcourt des utilisateurs membre du group LDAP
				for ($i=0; $i<count($member); $i++) {
					// Recuperation de l'id de l'utilisateur
					$q="SELECT user_id FROM env_user WHERE user_login='".$member[$i]."'";
					$db1->query($q);
					if($db1->next_record()) {
						$idusr=$db1->f("user_id");
						$q="INSERT INTO env_groupuser(groupuser_group_id, groupuser_user_id) VALUES($idgrp,$idusr)";
						$db1->query($q);
					}
				}
			}
		}
		
		if($config['LDAPDelGroup']) {
			echo "<br><br><h3>SYNCHRONISATION GROUPE BDD > LDAP</h3>";
			$q="SELECT group_id, group_name, group_ldapid FROM env_group";
			$db1->query($q);
			$fgtrv=false;
			while($db1->next_record()) {	
				$res = ldap_search($ds,$config['LDAPracine'],"gidNumber=".$db1->f("group_ldapid"));
				$entryID=ldap_first_entry($ds,$res);
				if(!$entryID) {
					$fgtrv=true;
					echo "<br>Suppression du groupe = ".$db1->f("group_name");
					delGroup($db1->f("group_id"));
				}
			}
			
			if(!$fgtrv) echo "<br>Aucune modification nécessaire";
		}		
		
		// Déconnexion
		echo "<br><br><h3>DECONNEXION</h3><br><br><br>";

		ldap_close($ds);
	}
	else {
		echo "<br>Impossible de se connecter au serveur LDAP";
	}

	echo "</body>";
	echo "</html>";
?>
