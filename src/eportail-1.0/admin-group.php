<?
	include_once("include/include.php");				/* Général			*/
	include_once("include/admin.php");					/* Page Admin		*/
	include_once("include/delete.php");					/* Fonction Delete	*/
	include_once($config["templatedir"]."/html.php");	/* Entete HTML		*/
	include_once($config["templatedir"]."/frame.php");	/* Entete HTML		*/
	
	$db1=new ps_db;
	$db2=new ps_db;
	
	$tpmod				=$_POST['tpmod'];
	$vlmod				=$_POST['vlmod'];
	$vladd				=$_POST['vladd'];
	$vlsup				=$_POST['vlsup'];
	
	$group_id			=$_POST['group_id'];
	$group_name			=$_POST['group_name'];
	$group_label		=$_POST['group_label'];
	$group_user_id		=$_POST['group_user_id'];
	$group_category		=$_POST['group_category'];
	$group_public		=$_POST['group_public'];
	$group_avatar		=$_POST['group_avatar'];

	
	/*--> Controle de cohérance */
	if($vladd!=""||$vlmod!="") {
		$fgerr="";

		if($group_name==""||$group_label==""||$group_user_id==""||$group_public==""||$group_avatar==""||$group_category=="") {
			$jsaction="alert('Vous devez renseigner l\'ensemble des informations');";
			$fgerr=1;
		}
	}
	
	/*--> Rester sur le mode encours */
	if($vlmod!=""&&fgerr!="") $tpmod="MODIFY";
	if($vladd!=""&&fgerr!="") $tpmod="SUBMIT";
		
	/*--> Submit */
	if($vladd!=""&&$fgerr=="") {
		$q="INSERT INTO env_group(group_name, group_label, group_user_id, group_category, group_public, group_avatar) VALUES('".addslashes($group_name)."','".addslashes($group_label)."',$group_user_id,'$group_category',$group_public,'$group_avatar')";
		$db1->query($q);
		$tpmod="";
	}

	/*--> Modify */
	if($vlmod!=""&&$fgerr=="") {
		echo $group_category;
		$q="UPDATE env_group SET group_name='".addslashes($group_name)."', group_label='".addslashes($group_label)."', group_user_id=$group_user_id, group_category='".$group_category."', group_public=$group_public, group_avatar='$group_avatar' WHERE group_id=$group_id";
		$db1->query($q);
		$tpmod="";
	}
	
	/*--> Delete */
	if($vlsup!="") {
		delGroup($group_id);
		$tpmod="";
	}

	echo "<script>$jsaction</script>";
	echo "<script type='text/javascript' src='".$config['libtable']."'></script>";
	
?>	

<script>
	$(document).ready(function() {
		$('#datatable').dataTable( {
			"oLanguage": { "sUrl": "<? echo $config['javascriptdir']; ?>/dataTables.txt" }
		} );
	} );	
</script>

<?
	echo "</head>";
	echo "<body>";

	echo "<form name='Formulaire' enctype='multipart/form-data' action='' method='post'>";
	echo "<input id='group_id' name='group_id' type='hidden' value='".$group_id."'>";
	echo "<input id='tpmod' name='tpmod' type='hidden' value=''>";
	
  
//-- DISPLAY -------------------------------------------------------------------------------------------------------------------------------

if($tpmod=="") { 
    echo "<h1>GESTION DES GROUPES</h1>";
    
	echo "<input id='fgadd' name='fgadd' class='input-rounded-button' onClick='document.Formulaire.group_id.value=\"\"; document.Formulaire.tpmod.value=\"SUBMIT\"' type='submit' value='Ajouter' />";
	echo "<br>";

	echo "<table class id='datatable'>";
    echo "<thead>";
	echo "<th width='70px'>Action</th>";
	echo "<th>ID</th>";
	echo "<th width='90px'>Avatar</th>";
	echo "<th>Nom</th>";
	echo "<th>Description</th>";
	echo "<th>Prorpiétaire</th>";
	echo "<th>Catégorie</th>";
	echo "<th>Public</th>";
	echo "<th>Membres</th>";
	echo "</thead>";
	
	$q="SELECT * FROM env_group, env_user, env_profil WHERE group_user_id=user_id AND profil_id=user_profil_id";
	$db1->query($q);
	while($db1->next_record()){	
		echo "<tr valign=top>";

		echo "<td>";
		echo "<div id='btnupd'>";
		echo "<a onClick='document.Formulaire.group_id.value=".$db1->f('group_id')."; document.Formulaire.tpmod.value=\"MODIFY\";document.Formulaire.submit();'>";
		echo "<img src='images/blank.gif' width='16px' height='16px'>";
		echo "</a>";
		echo "</div>";

		echo "<div id='btndel'>";
		echo "<a onClick='document.Formulaire.group_id.value=".$db1->f('group_id')."; document.Formulaire.tpmod.value=\"DELETE\";document.Formulaire.submit();'>";
		echo "<img src='images/blank.gif' width='16px' height='16px'>";
		echo "</a>";
		echo "</div>";
		echo "</td>";

		echo "<td>";
		echo $db1->f('group_id');
		echo "</td>";
		
		echo "<td>";
		echo "<img src='".$config["avatardir"]."/".$db1->f('group_avatar')."' width='90px' height='90px'></img>";
		echo "</td>";
		
		echo "<td>";
		echo $db1->f('group_name')."<br>";
		echo "</td>";

		echo "<td>";
		echo $db1->f('group_label')."<br>";
		echo "</td>";

		echo "<td>";
		echo $db1->f('user_login')."<br>";
		echo "</td>";
		
		echo "<td>";
		echo $db1->f('group_category')."<br>";
		echo "</td>";

		echo "<td>";
		if($db1->f('group_public')) echo "oui<br>";
		else echo "non<br>";
		echo "</td>";
		
		echo "<td style='font-size: 70%'>";
		$q="SELECT * FROM env_groupuser, env_user WHERE groupuser_group_id=".$db1->f("group_id")." AND groupuser_user_id=user_id";
		$db2->query($q);
		while($db2->next_record()) {
			echo $db2->f("user_login")."<br>";
		}
		echo "</td>";		

		echo "</tr>";
	} 
	echo "</table>";
}

//-- SUBMIT --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="SUBMIT") {
	// Entete du formulaire
    echo "<h1>AJOUT GROUPE</h1>";
	echo "<input id='vladd' name='vladd' class='input-rounded-button' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='input-rounded-button' type='submit' value='Annuler' />";
	echo "</br></br>";

	// Valeur par défaut
	$group_user_id=$_SESSION['user_id'];
	$q="SELECT * FROM env_user, env_profil  WHERE user_id=$group_user_id AND env_profil.profil_id=env_user.user_profil_id";
	$db1->query($q);
	if($db1->next_record()){		
		$lbavatar	= $db1->f('user_avatar');
		$lbproprio	= $db1->f('user_firstname')." ".$db1->f('user_lastname');
		$lbpseudo	= $db1->f('user_pseudo');
		$lblogin	= $db1->f('user_login');
		$lbprofil	= $db1->f('profil_label');
		
	}

	if($group_avatar=="") {$group_avatar="anonyme.jpg";}
	$group_public=0;
}

//-- MODIFY --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="MODIFY") {
	// Entete du formulaire
    echo "<h1>MODIFICATION GROUPE</h1>";
	echo "<input id='vlmod' name='vlmod' class='input-rounded-button' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='input-rounded-button' type='submit' value='Annuler' />";
	echo "</br></br>";

	// Valeur par défaut
	$q = "SELECT * FROM env_group, env_user,env_profil WHERE group_id=$group_id AND group_user_id=user_id AND env_profil.profil_id=env_user.user_profil_id ";
	$db1->query($q);
	if($db1->next_record()) {
		$group_name 		= $db1->f("group_name");
		$group_label 		= $db1->f("group_label");
		$group_user_id 		= $db1->f("group_user_id");
		$group_category	 	= $db1->f("group_category");
		$group_public 		= $db1->f("group_public");
		$group_avatar		= $db1->f("group_avatar");
		$lbavatar			= $db1->f("user_avatar");
		$lbproprio			= $db1->f("user_firstname")." ".$db1->f("user_lastname");
		$lbpseudo			= $db1->f("user_pseudo");
		$lblogin			= $db1->f("user_login");
		$lbprofil			= $db1->f("profil_label");		
	}
}

//-- DELETE --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="DELETE") {
	echo "<h1>SUPPRESSION UTILISATEUR</h1>";
	echo "<input id='vlsup' name='vlsup' class='input-rounded-button' type='submit' value='Confirmer la suppression' />";
	echo "&nbsp;";
	echo "<input class='input-rounded-button' type='submit' value='Annuler' />";    

	// Valeur par défaut
	$q="SELECT * FROM env_user, env_profil WHERE group_id=$group_id AND env_profil.profil_id=env_user.user_profil_id";
	$db1->query($q);
	if($db1->next_record()) {
		echo "<br><div id='avatar' class='avatar_admin'>";

		echo "<img src='".$config["avatardir"]."/".$db1->f('group_avatar')."' width='90px' height='90px'></img>";
		echo "<div>";
		echo $db1->f('group_nanme')." ".$db1->f('group_label')."<br>";
		echo "<span id='avatar_description'>";
		echo $db1->f('group_label')."<br>";
		echo "</span>";
		echo "</div>";

		echo "</div>";
	}
	
	
}


//-- FORMULAIRE -----------------------------------------------------------------------------------------------------------------------------

if($tpmod=="SUBMIT"||$tpmod=="MODIFY") {
?>

 	<h2>Coordonnée</h2>

	<fieldset id="encadrer">
		<?
			$lbsty00="style='width:25%; border-bottom: dotted 1px;'";
			$lbsty01="type='text' size='53' style='width: 74%;'";
			$lbsty02="type='password' size='53' style='width: 74%;'";
			$lbsty03="style='width: 74%;'";
		?>

		<span id="encadrer_label" <? echo $lbsty00; ?>>ID Groupe*</span>
		<input 	value="<? echo $group_id; ?>"
				id="group_id_bis"
				name="group_id_bis"	
				readonly
				<? echo $lbsty01; ?>>

		<p>
		<span id="encadrer_label" <? echo $lbsty00; ?>>Nom*</span>
		<input	value="<? echo $group_name; ?>"
				id="group_name" 
				name="group_name"
				<? echo $lbsty01; ?>>
		</p>

		<p>
		
		<p>
		<span id="encadrer_label" <? echo $lbsty00; ?>>Description*</span>
		<input 	value="<? echo $group_label; ?>"
				id="group_label"
				name="group_label"
				<? echo $lbsty01; ?>>
		</p>

		<p>
		<span id="encadrer_label" <? echo $lbsty00; ?>>Categorie*</span>
		<input	value="<? echo $group_category; ?>"
				id="group_category"
				name="group_category"
				<? echo $lbsty01; ?>>
		</p>


		<p>
		<span id="encadrer_label" <? echo $lbsty00; ?>>Public*</span>
		<select	id="group_public"
				name="group_public"
				size="1"
				<? echo $lbsty03; ?>>>		
			<option value="0"	<? if($group_public=='0')	{ echo 'selected'; }?>>non</option>
			<option value="1"	<? if($group_public=='1')	{ echo 'selected'; }?>>oui</option>
		</select>
		</p>
						
	</fieldset>

 	<h2>Propriétaire</h2>
	<fieldset id="encadrer">
		<?
			echo "<div>";
			echo "<a href='#?o=1&w=700&h=530&u=popup-userselection.php?igico=igpro&idico=group_user_id&idlab=descrption_user' rel='popup_name' class='poplight' title='Choisir un Utilisateur'><span class='link-rounded-button'>Changer</span></a>";
			echo "</div>";

			echo "<div id='avatar' class='avatar_admin'>";

			echo "<img id='igpro' src='".$config["avatardir"]."/".$lbavatar."' width='90px' height='90px'></img>";
			echo "<div id='descrption_user'>";
			echo $lbproprio."<br>";
			echo "<span id='avatar_description'>";
			echo "Login : ".$lblogin."<br>";
			echo "Pseudo : ".$lbpseudo."<br>";
			echo "Profil : ".$lbprofil."<br>";
			echo "</span>";
			echo "</div>";
			echo "</div>";
		?>
	
		<input 	value="<? echo $group_user_id; ?>"
				id="group_user_id"
				name="group_user_id"	
				type="hidden"
				readonly
				<? echo $lbsty01; ?>>
		
	</fieldset>
	
	<h2>Avatar du Groupe</h2>

	<fieldset id="encadrer">
		<?
			echo "<div id='avatar' class='avatar_admin'>";
			echo "<img id='img_avatar' name='img_avatar' src='".$config["avatardir"]."/".$group_avatar."' width='90px' height='90px'></img>";
			echo "<div>";
		
			echo "<div id='btnupd'>";
			echo "<a href='#?o=1&w=700&h=530&u=jcrop.php?dirimg=".$config["avatardir"]."&lbico=group_avatar&igico=img_avatar' rel='popup_name' class='poplight' title='Ajouter un avatar'>";
			echo "<img src='images/blank.gif' width='16px' height='16px'>";
			echo "</a>";
			echo "</div>";

			echo "<div id='btndel'>";
			echo "<a onClick='document.getElementById(\"img_avatar\").src=\"".$config["avatardir"]."/anonyme.jpg\"; document.Formulaire.group_avatar.value=\"anonyme.jpg\";'>";
			echo "<img src='images/blank.gif' width='16px' height='16px'>";
			echo "</a>";
			echo "</div>";	
			
			echo "<input id='group_avatar' name='group_avatar' type='hidden' value='".$group_avatar."'>";
		?>
	</fieldset>
	
	<? if($tpmod=="MODIFY") { ?>
	<h2>Membres</h2>
	
	<fieldset id="encadrer">
	<?	
		echo "<input id='fgadd' name='fgadd' class='input-rounded-button' onClick='document.Formulaire.user_id.value=\"\"; document.Formulaire.tpmod.value=\"SUBMIT\"' type='submit' value='Ajouter' />";
		echo "<br>";

		echo "<table class id='datatable'>";
		echo "<thead>";
		echo "<th width='70px'>Action</th>";
		echo "<th>ID</th>";
		echo "<th width='90px'>Avatar</th>";
		echo "<th>Nom</th>";
		echo "<th>Login</th>";
		echo "<th>Pseudo</th>";
		echo "<th>Profil</th>";
		echo "</thead>";
		
		$q="SELECT * FROM env_groupuser, env_user, env_profil WHERE groupuser_group_id=$group_id AND groupuser_user_id=user_id AND env_profil.profil_id=env_user.user_profil_id";
		$db1->query($q);
		while($db1->next_record()){	
			echo "<tr>";

			echo "<td>";
			echo "<div id='btnupd'>";
			echo "<a onClick='document.Formulaire.user_id.value=".$db1->f('user_id')."; document.Formulaire.tpmod.value=\"MODIFY\";document.Formulaire.submit();'>";
			echo "<img src='images/blank.gif' width='16px' height='16px'>";
			echo "</a>";
			echo "</div>";

			echo "<div id='btndel'>";
			echo "<a onClick='document.Formulaire.user_id.value=".$db1->f('user_id')."; document.Formulaire.tpmod.value=\"DELETE\";document.Formulaire.submit();'>";
			echo "<img src='images/blank.gif' width='16px' height='16px'>";
			echo "</a>";
			echo "</div>";
			echo "</td>";

			echo "<td>";
			echo $db1->f('user_id');
			echo "</td>";
			
			echo "<td>";
			echo "<img src='".$config["avatardir"]."/".$db1->f('user_avatar')."' width='90px' height='90px'></img>";
			echo "</td>";
			
			echo "<td>";
			echo $db1->f('user_firstname')." ".$db1->f('user_lastname')."<br>";
			echo "</td>";

			echo "<td>";
			echo $db1->f('user_login')."<br>";
			echo "</td>";

			echo "<td>";
			echo $db1->f('user_pseudo')."<br>";
			echo "</td>";

			echo "<td>";
			echo $db1->f('profil_label')."<br>";
			echo "</td>";

			echo "</tr>";
		} 
		echo "</table>";
	?>
	
	
	
	</fieldset>
	
	<? } ?>
	
	<script type="text/javascript">
			$('#group_name').focus();
	</script>
<?
}



//-- FOOTER ---------------------------------------------------------------------------------------------------------------------------------

echo "</form>";
echo "</body>";
echo "</html>";
?>







	
	
