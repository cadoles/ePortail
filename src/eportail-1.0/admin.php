<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?
	include_once("include/include.php");					/* Général				*/
	include_once("include/admin.php");						/* Page Admin			*/
	include_once($config["templatedir"]."/html.php");		/* Entete HTML			*/
	include_once($config["javascriptdir"]."/iframe.php");	/* Gestion iframe		*/
	
	$db1=new ps_db;
	$db2=new ps_db;
	$idpan=$_GET['idpan'];

	// Rechercher si le panel envoyé par paramètre existe bien
	if($idpan!="") {
		$q="SELECT admin_panel_id FROM env_admin_panel WHERE admin_panel_id=".$idpan;
		$db1->query($q);
		if(!$db1->next_record()) { $idpan=""; }
	}

	
	// Rechercher le premier panel si aucun panel n'a été envoyé en paramétre
	if($idpan=="") {
		$q="SELECT admin_panel_id FROM env_admin_panel ORDER BY admin_panel_order LIMIT 1";
		$db1->query($q);
		if($db1->next_record()) { $idpan=$db1->f('admin_panel_id'); }		
	}
	
	// Simuler un click d'onglet si on a un onglet de positionné
	$jsaction = "ClickOnglet(".$idpan.");";
?>

<script type="text/javascript">
	// Gestion de l'affichage du lien sur simple click d'un Panel
	$(function()
	{
		$(".userpanellabel a").click(function() {
			var a = $(this);
			
			// Recuperation de l'id de l'onglet
			var query= a.attr("href").split('?');
			var dim= query[1].split('&');
			var idpan = dim[0].split('=')[1]; 
			
			// Sauvegarder l'id de l'onglet
			$("#idpan").val(idpan);
			
			// Changement de l'adresse pour qu'un refresh tombe sur le meme ongle
			history.pushState(null, "page", "admin.php?idpan="+idpan);
			
			// Affichage de l'iframe associée à l'onglet
			$('#envolepage iframe').attr("src",a.attr("label"));
			
			// Retour faux pour que le href ne se lance pas
			return false;
		});
	});	

	// Fonction qui simule le choix d'un panel
	$(document).ready(function()
	{
		function ClickOnglet(idpan) {
			var panel = "#userpanel_".concat(idpan);
			$(panel+" a").click();
		}
	
		<? echo $jsaction; ?>
	});	
</script>

</head>

<body>
<div id="envoleheader">
	<? include_once("header-user.php"); ?>
	<? include_once("header-admin.php"); ?>
</div>

<div id="envolepanelvertical">
	<ul id="userpanel">
	<?
		$q="SELECT admin_panel_group_id, admin_panel_group_name FROM env_admin_panel_group ORDER BY admin_panel_group_order";
		$db1->query($q);
		while($db1->next_record()){		
			echo "<h1>".$db1->f('admin_panel_group_name')."</h1>";
			
			$q="SELECT * FROM env_admin_panel WHERE admin_panel_group_id='".$db1->f('admin_panel_group_id')."' ORDER BY admin_panel_order";
			$db2->query($q);

			while($db2->next_record()){		
				$order=$order+1;
		?>
		
		<li id="userpanel_<? echo $db2->f('admin_panel_id'); ?>">
			<div class="userpanelhandle">
				<div class="userpanellabel">
					<a href="?idpan=<? echo $db2->f('admin_panel_id'); ?>" label="<? echo $db2->f('admin_panel_url'); ?>" class="userpanellabelllink"><? echo $db2->f('admin_panel_label'); ?></a>
					<input type="text" value="<? echo $db2->f('admin_panel_label'); ?>">
				</div>
			</div>
		</li>
	<?
		}
		}
	?>
	</ul>

	<div id="envolepage">
		<div id="info"></div>
		<form method="get" name="formulaire"> 
			<input type="hidden" name="idpan" id="idpan" value="<? echo $idpan; ?>" /> 
		</form>

		<iframe src="" width=100% height=100% frameborder=0 />
	</div>

</div>

</body>
</html>
