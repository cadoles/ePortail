<?
	include_once("include/include.php");				/* Général		*/
	include_once($config["templatedir"]."/html.php");	/* Entete HTML	*/
	include_once($config["templatedir"]."/popup.php");
	$db1=new ps_db;
	
	$idpan=$_GET['idpan'];
	$idapp=$_GET['idapp'];
	
	if($idpan=="") {
		$idpan=$_POST['idpan'];
		$idapp=$_POST['idapp'];
	}

	$lbpan=$_POST['lbpan'];	
	$fgclo=$_POST['fgclo'];
	
	if($fgclo!="") {
		// Création du widget
		$q="UPDATE env_page_application SET page_application_label='$lbpan' WHERE page_application_id=$idapp";
		$db1->query($q);		

		// Action javascript Ajout du panel directement + fermeture de la popup
		$jsaction="recharge(".$idpan.")";
	}
	
?>

<script type="text/JavaScript">
	
	
	function AjoutAppli(idgrp,idapp) {
		$("#info").load("process-userpageappgroup-mod.php?idgrp="+idgrp+"&idapp="+idapp);
		setTimeout(function () {$("#Formulaire").submit()},1000);
	}

	function DelAppli(idgrp,idapp) {
		$("#info").load("process-userpageappgroup-del.php?idgrp="+idgrp+"&idapp="+idapp);
		setTimeout(function () {$("#Formulaire").submit()},1000);
	}

	
	function recharge(idpan) {
		parent.document.location.reload();
	}
	<?php echo $jsaction ?>
</script>

</head>

<body>
<div id="info"></div>
	
<?
	$q="SELECT * FROM env_page_application WHERE page_application_id=$idapp";
	$db1->query($q);
	if($db1->next_record()) {
		$lbapp=$db1->f('page_application_label');
	}
?>

<h1>Modifier d'un groupe d'application</h1>

<div id="envolepage">
<form id="Formulaire" name="Formulaire" method="post">


<h2>Nom à votre groupe</h2>
<input id="lbpan" name="lbpan" class="biginput" type="text" size="150" style="width: 99%" value="<? echo $lbapp; ?>">

<br>
<br>
<h2>Applications Disponibles</h2>
<em>Cliquez sur l'icone de l'application pour l'ajouter au groupe</em><br>
<div style="display: inline-block; margin-bottom:20px;x; min-height:20px;">
<?
	$q="SELECT * FROM env_application, env_appicon
			WHERE NOT EXISTS(SELECT appgroup_application_id FROM env_page_appgroup WHERE appgroup_page_application_id=$idapp AND appgroup_application_id=application_id)
			AND application_icon=appicon_id";
	$db1->query($q);
	while($db1->next_record()) {

		echo "<div id='avatar' class='avatar_admin' style='cursor:pointer;'>";

		echo "<img src='".$config["appicondir"]."/".$db1->f('appicon_url')."' width='90px' height='90px' onClick='AjoutAppli($idapp,".$db1->f("application_id").")'></img>";
		echo "<div>";
		echo $db1->f('application_label')."<br>";
		echo "<span id='avatar_description'>";
		echo "id : ".$db1->f('application_name')."<br>";
		echo "url : <a href='".$db1->f('application_url')."' target='_black'>".substr($db1->f('application_url'),0,25)."</a><br>";
		echo "</span>";
		echo "</div>";
		echo "</div>";

	}			
?>
</div>

<h2>Supprimer une application du groupe</h2>
<em>Cliquez sur l'icone de l'application pour la supprimer du groupe</em><br>
<div style="display: inline-block; margin-bottom:20px; min-height:20px;">
<?
	$q="SELECT * FROM env_page_appgroup, env_application, env_appicon WHERE appgroup_page_application_id=$idapp AND application_id=appgroup_application_id AND appicon_id=application_icon";
	$db1->query($q);
	while($db1->next_record()) {

		echo "<div id='avatar' class='avatar_admin' style='cursor:pointer;'>";

		echo "<img src='".$config["appicondir"]."/".$db1->f('appicon_url')."' width='90px' height='90px' onClick='DelAppli($idapp,".$db1->f("application_id").")'></img>";
		echo "<div>";
		echo $db1->f('application_name')."<br>";
		echo "<span id='avatar_description'>";
		echo "id : ".$db1->f('application_label')."<br>";
		echo "url : <a href='".$db1->f('application_url')."' target='_black'>".substr($db1->f('application_url'),0,25)."</a><br>";
		echo "</span>";
		echo "</div>";
		echo "</div>";

	}			
?>
</div>

<center>
<input id="fgclo" name="fgclo" class="input-rounded-button" type="submit" style="width: 99%" value="Fermer" />
<input type="hidden" name="idpan" id="idpan" value="<? echo $idpan; ?>" /> 
<input type="hidden" name="idapp" id="idpan" value="<? echo $idapp; ?>" /> 
</center>
</form>
</div>

</body>
</html>
