<?
	include_once("include/include.php");				/* Général			*/
	include_once($config["templatedir"]."/html.php");	/* Entete HTML		*/
	
	$db1=new ps_db;
	$db2=new ps_db;
	$idpan=$_GET['idpan'];

	include_once($config["templatedir"]."/widget.php");	/* Entete Widget	*/		
?>

<script type="text/javascript">  
	$(document).ready(function () {  
		$(".column").sortable({  
			connectWith: ".column",  
			handle: '.WidgetHeader',  
			opacity: 0.6,
			update: function ()
			{
				var a = $(this);

				$('.column').each(function(index) {
					var order = $(this).sortable('serialize');
					$("#info").load("process-userpageapplication-order.php?"+order+"&idloc="+$(this).attr('id')+"&idpan=<? echo $idpan; ?>");
				});
			} 
		});  

		$(".column").disableSelection();		
	});  
	
	function delgroup(idgrp) {
		if(confirm("Confirmez-vous la suppression de ce groupe d'application ?")) {
			$("#info").load("process-userpageapplication-del.php?id="+idgrp);
			setTimeout(function () {location.reload()},1000);			
		}
	}
	
	function chooseIcon(idtpl,url) {
		if(idtpl>1) {
			loc = $('#envolepage');
			$('#envolepage iframe').attr("src",url);
		}
		else {
			document.location=url;
		}
	}
</script>

</head>

<?
	// Récupérer le template de la page widget et le propriétaire de la page
	$q="SELECT panel_page_template_id, panel_user_id FROM env_panel WHERE panel_id=".$idpan;
	$db1->query($q);
	if($db1->next_record()) {
		$idtpl=$db1->f('panel_page_template_id');
		$idpro=$db1->f('panel_user_id');
	}

	// Si l'utilisateur en cours est le propriétaire : il peut modifier la page
	if($idpro==$_SESSION['user_id']||$_SESSION['user_profil']==1) {
		echo "<div id='widgetadmin'><a href='#?o=1&w=600&h=250&u=popup-userappgroup-add.php?idpan=".$idpan."' rel='popup_name' class='poplight' title='Ajouter un groupe d'application'>Ajouter un Groupe d'Applications</a></div>";
	}
?>

<div id="info"></div>


<div id="widgetpage">
<?
	// Récupérer le code html du template
	$q="SELECT * FROM env_page_template WHERE page_template_id=".$idtpl;
	$db1->query($q);

	if($db1->next_record()) { echo $db1->f('page_template_html'); }
		
	// Parcourrir l'ensemble des widget de la page pour les créer dynamiquement
	$q="SELECT * FROM env_page_application WHERE page_application_panel_id=".$idpan." ORDER BY page_application_loc, page_application_order DESC";
	$db1->query($q);
	while($db1->next_record()) {
	?>
		<script type="text/javascript">  
			// Recherche de la colonne où le widget est localisé
			loc = $('#<? echo $db1->f('page_application_loc'); ?>');
			
			if(loc.length == 0) {
				loc=$('#C1R1');
			}
			
			// iframe widget
			<?
				$lbentete=$db1->f('page_application_label');
				if($idpro==$_SESSION['user_id']) {
					$lbentete=$lbentete."<div id='btnupd' style='float:right;'>";
					$lbentete=$lbentete."<a href='#?o=1&w=700&h=530&u=popup-userappgroup-mod.php?idpan=".$idpan."&idapp=".$db1->f('page_application_id')."' rel='popup_name' class='poplight' title='Modifier Groupe Application'>";
					$lbentete=$lbentete."<img src='images/blank.gif' width='16px' height='16px'>";
					$lbentete=$lbentete."</a>";
					$lbentete=$lbentete."</div>";

					$lbentete=$lbentete."<div id='btndel' style='float:right;'>";
					$lbentete=$lbentete."<a onClick='delgroup(".$db1->f('page_application_id').");'>";
					$lbentete=$lbentete."<img src='images/blank.gif' width='16px' height='16px'>";
					$lbentete=$lbentete."</a>";	
					$lbentete=$lbentete."</div>";				
				}


				$q="SELECT * FROM env_page_appgroup, env_application, env_appicon WHERE appgroup_page_application_id=".$db1->f("page_application_id")." AND appgroup_application_id=application_id AND application_icon=appicon_id";
				$db2->query($q);
				$iframewidget='';
				while($db2->next_record()){	
					$iframewidget=$iframewidget."<div id='appicon' class='appicon' style='display:inline-block; float:initial;' >";
					$iframewidget=$iframewidget."<a onClick='chooseIcon(".$idtpl.",$$".$db2->f('application_url')."$$)' style='cursor:pointer;'>";
					$iframewidget=$iframewidget."<img id='imgapp".$db2->f('appicon_id')."' src='".$config['appicondir']."/".$db2->f('appicon_url')."' width='90px' height='90px'></img>";
					$iframewidget=$iframewidget."<div id='appicontitre' class='appicontitre'>";
					$iframewidget=$iframewidget.$db2->f('application_label')."<br>";
					$iframewidget=$iframewidget."<span id='appicondescription'>";
					$iframewidget=$iframewidget.$db2->f('application_name');
					$iframewidget=$iframewidget."</span>";
					$iframewidget=$iframewidget."</div>";


					$iframewidget=$iframewidget."</a>";
					$iframewidget=$iframewidget."</div>";
				} 	

			?>
			// Ajout du widget
			loc.prepend('<div id="widget_<? echo $db1->f('page_application_id'); ?>" class="Widget"><div class="WidgetHeader"><? echo str_replace("'","\"",$lbentete); ?></div><div class="WidgetBody"><? echo str_replace("$$","\'",str_replace("'","\"",$iframewidget)); ?></div></div>');
		</script>
	<?
	}
?>

<? if ($idtpl > 1) { ?>
<script>
	loc = $('#envolepanelvertical');
	loc.append("<div id='envolepage'><iframe src='' width=100% height=100% frameborder=0 /></div>");
</script>
<? } ?>
</div>
</body>
