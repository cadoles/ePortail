<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?
	include_once("include/include.php");				/* Général		*/
	include_once($config["templatedir"]."/html.php");	/* Entete HTML	*/
	include_once($config["templatedir"]."/popup.php");
	$db1=new ps_db;
	
	$fgedt=$_POST['fgedt'];
	$fgweb=$_POST['fgweb'];
	$fgwid=$_POST['fgwid'];
	$lbpan=$_POST['lbpan'];
	$lburl=$_POST['lburl'];
	
	
	// Création d'une page Editeur
	if($fgedt!="") {
		// Recherche du dernier numéro d'ordre de panel de l'utilisateur 
		$q="SELECT panel_order FROM env_panel WHERE panel_user_id=".$_SESSION['user_id']." AND panel_visible ORDER BY panel_order DESC";
		$db1->query($q);
		$order=1;
		if($db1->next_record()) {
			$order=$db1->f('panel_order')+1;
		}
	
		// Création du panel
		$q="INSERT INTO env_panel (panel_label, panel_url, panel_user_id, panel_order, panel_type_id, panel_visible,panel_page_html) VALUES('".$lbpan."','editeur.php',".$_SESSION['user_id'].",".$order.",3,1,'<center><br><br><br>Modifier votre page pour insérer votre texte</center>')";
		$db1->query($q);
		
		// Récupération du panel généré
		$q="SELECT panel_id FROM env_panel WHERE panel_user_id=".$_SESSION['user_id']." AND panel_order=".$order." AND panel_visible";
		$db1->query($q);
		if($db1->next_record()) {
			$idpan=$db1->f('panel_id');
		}
		
		// Action javascript Ajout du panel directement + fermeture de la popup
		$jsaction="recharge(".$idpan.")";
	}
	
	// Création d'une page URL
	if($fgweb!="") {
		// Recherche du dernier numéro d'ordre de panel de l'utilisateur 
		$q="SELECT panel_order FROM env_panel WHERE panel_user_id=".$_SESSION['user_id']." AND panel_visible ORDER BY panel_order DESC";
		$db1->query($q);
		$order=1;
		if($db1->next_record()) {
			$order=$db1->f('panel_order')+1;
		}
	
		// Création du panel
		$q="INSERT INTO env_panel (panel_label, panel_url, panel_user_id, panel_order, panel_type_id, panel_visible) VALUES('".$lbpan."','".$lburl."',".$_SESSION['user_id'].",".$order.",1,1)";
		$db1->query($q);
		
		// Récupération du panel généré
		$q="SELECT panel_id FROM env_panel WHERE panel_user_id=".$_SESSION['user_id']." AND panel_order=".$order." AND panel_visible";
		$db1->query($q);
		if($db1->next_record()) {
			$idpan=$db1->f('panel_id');
		}
		
		// Action javascript Ajout du panel directement + fermeture de la popup
		$jsaction="recharge(".$idpan.")";
	}
	
	// Création d'une page Widget
	if($fgwid!="") {
		// Recherche du dernier numéro d'ordre de panel de l'utilisateur 
		$q="SELECT panel_order FROM env_panel WHERE panel_user_id=".$_SESSION['user_id']." AND panel_visible ORDER BY panel_order DESC";
		$db1->query($q);
		$order=1;
		if($db1->next_record()) {
			$order=$db1->f('panel_order')+1;
		}
	
		// Création du panel
		$q="INSERT INTO env_panel (panel_label, panel_url, panel_user_id, panel_order, panel_type_id, panel_visible,panel_page_css_name,panel_page_template_id) VALUES('".$lbpan."','widget.php',".$_SESSION['user_id'].",".$order.",4,1,'widget.css',1)";
		$db1->query($q);
		
		// Récupération du panel généré
		$q="SELECT panel_id FROM env_panel WHERE panel_user_id=".$_SESSION['user_id']." AND panel_order=".$order." AND panel_visible";
		$db1->query($q);
		if($db1->next_record()) {
			$idpan=$db1->f('panel_id');
		}
		
		// Action javascript Ajout du panel directement + fermeture de la popup
		$jsaction="recharge(".$idpan.")";
	}	
?>

<script type="text/JavaScript">
	function recharge(idpan) {
		parent.document.location.href='index.php?idpan='+idpan;
	}
	<?php echo $jsaction ?>
</script>

</head>

<body>

<h1>Création d'un onglet</h1>

<div id="envolepage">
<form id="Formulaire" name="Formulaire" method="post">

<h2>Donnez un nom à votre nouvel onglet</h2>
<input id="panel_label" name="lbpan" class="biginput" type="text" size="150" style="width: 99%" value="Nouvelle Page">

<br>
<br>
<h2>Choisissez un type d'Onglet</h2>

<div class="box" style="width:165px; height:210px">
	<div style="height: 175px; ">
		<center>
		<h3>Page Editeur</h3>
		<br>
		<img src="images/pagetext.png">
		</center>
	</div>
	
	<center>
		<input id="fgedt" name="fgedt" style="position: relative;" class="input-rounded-button" type="submit" value="Choisir"/>
	</center>
</div>

<div class="box" style="width:165px; height:210px">
	<div style="height: 175px; ">
		<center>
		<h3>Page Web</h3>
		<br>
		<img src="images/pageweb.png">
		</center>
		
		Saisir l'adresse du site :<br>
		<input id="panel_url" name="lburl" type="text" size="150" style="width: 99%" value="http://">
	</div>

	<center>
	<input id="fgweb" name="fgweb" class="input-rounded-button" type="submit" value="Choisir" />
	</center>
</div>

<div class="box" style="width:165px; height:210px">
	<div style="height: 175px; ">
		<center>
		<h3>Page Widgets</h3>
		<br>
		<img src="images/pagewid.png">
		</center>
	</div>
	
	<center>
		<input id="fgwid" name="fgwid" style="position: relative;" class="input-rounded-button" type="submit" value="Choisir"/>
	</center>
</div>

<div class="box" style="width:165px; height:200px; display: none;">
<h3><center></center></h3>
</div>

	
</form>
</div>

</body>
</html>
