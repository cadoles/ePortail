-- création de la base de donnée
CREATE DATABASE eportail CHARACTER SET utf8;

-- Creation utilisateur
grant all privileges on eportail.* to eportail@%%adresse_ip_web identified by 'eportail';
flush privileges ;

-- connexion à la base
\r eportail
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";




--
-- Base de données: `eportail`
--

-- --------------------------------------------------------

--
-- Structure de la table `ajax_chat_bans`
--

CREATE TABLE IF NOT EXISTS `ajax_chat_bans` (
  `userID` int(11) NOT NULL,
  `userName` varchar(64) COLLATE utf8_bin NOT NULL,
  `dateTime` datetime NOT NULL,
  `ip` varbinary(16) NOT NULL,
  PRIMARY KEY (`userID`),
  KEY `userName` (`userName`),
  KEY `dateTime` (`dateTime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `ajax_chat_bans`
--


-- --------------------------------------------------------

--
-- Structure de la table `ajax_chat_invitations`
--

CREATE TABLE IF NOT EXISTS `ajax_chat_invitations` (
  `userID` int(11) NOT NULL,
  `channel` int(11) NOT NULL,
  `dateTime` datetime NOT NULL,
  PRIMARY KEY (`userID`,`channel`),
  KEY `dateTime` (`dateTime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `ajax_chat_invitations`
--


-- --------------------------------------------------------

--
-- Structure de la table `ajax_chat_messages`
--

CREATE TABLE IF NOT EXISTS `ajax_chat_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `userName` varchar(64) COLLATE utf8_bin NOT NULL,
  `userRole` int(1) NOT NULL,
  `channel` int(11) NOT NULL,
  `dateTime` datetime NOT NULL,
  `ip` varbinary(16) NOT NULL,
  `text` text COLLATE utf8_bin,
  PRIMARY KEY (`id`),
  KEY `message_condition` (`id`,`channel`,`dateTime`),
  KEY `dateTime` (`dateTime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

--
-- Contenu de la table `ajax_chat_messages`
--


-- --------------------------------------------------------

--
-- Structure de la table `ajax_chat_online`
--

CREATE TABLE IF NOT EXISTS `ajax_chat_online` (
  `userID` int(11) NOT NULL,
  `userName` varchar(64) COLLATE utf8_bin NOT NULL,
  `userRole` int(1) NOT NULL,
  `channel` int(11) NOT NULL,
  `dateTime` datetime NOT NULL,
  `ip` varbinary(16) NOT NULL,
  PRIMARY KEY (`userID`),
  KEY `userName` (`userName`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `ajax_chat_online`
--


-- --------------------------------------------------------

--
-- Structure de la table `env_admin_panel`
--

CREATE TABLE IF NOT EXISTS `env_admin_panel` (
  `admin_panel_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_panel_label` varchar(250) NOT NULL,
  `admin_panel_url` varchar(250) NOT NULL,
  `admin_panel_order` int(11) NOT NULL,
  `admin_panel_group_id` varchar(250) NOT NULL,
  PRIMARY KEY (`admin_panel_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Contenu de la table `env_admin_panel`
--

INSERT INTO `env_admin_panel` (`admin_panel_id`, `admin_panel_label`, `admin_panel_url`, `admin_panel_order`, `admin_panel_group_id`) VALUES
(1, 'Gestion des Utilisateurs', 'admin-user.php', 1, '1'),
(2, 'Gestion des Applications', 'admin-application.php', 1, '2'),
(3, 'Gestion des Icones', 'admin-appicon.php', 2, '2'),
(4, 'Gestion des Pages Applicatives', 'admin-pageapplication.php', 1, '4'),
(5, 'Gestion des Pages Editeurs', 'admin-pageediteur.php', 2, '4'),
(6, 'Gestion des Onglets par Profil', 'admin-panelprofil.php', 1, '5'),
(7, 'Gestion des Pages Url', 'admin-pageurl.php', 3, '4'),
(8, '<br>Synchronisation LDAP', 'ldap.php', 3, '1'),
(9, 'Gestion des Groupes', 'admin-group.php', 2, '1'),
(10, 'Gestion de la Bibliothèque de Widgets', 'admin-widget.php', 1, '3'),
(11, 'Gestion des Pages Widgets', 'admin-pagewidget.php', 4, '4');

-- --------------------------------------------------------

--
-- Structure de la table `env_admin_panel_group`
--

CREATE TABLE IF NOT EXISTS `env_admin_panel_group` (
  `admin_panel_group_id` int(11) NOT NULL,
  `admin_panel_group_name` varchar(255) NOT NULL,
  `admin_panel_group_order` int(11) NOT NULL,
  PRIMARY KEY (`admin_panel_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `env_admin_panel_group`
--

INSERT INTO `env_admin_panel_group` (`admin_panel_group_id`, `admin_panel_group_name`, `admin_panel_group_order`) VALUES
(1, 'Utilisateurs / Groupes', 1),
(2, 'Applications', 2),
(4, 'Pages', 4),
(5, 'Onglets', 5),
(3, 'Bibliothèque de Widgets ', 3);

-- --------------------------------------------------------

--
-- Structure de la table `env_appicon`
--

CREATE TABLE IF NOT EXISTS `env_appicon` (
  `appicon_id` int(11) NOT NULL AUTO_INCREMENT,
  `appicon_url` varchar(255) NOT NULL,
  PRIMARY KEY (`appicon_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `env_appicon`
--


-- --------------------------------------------------------

--
-- Structure de la table `env_application`
--

CREATE TABLE IF NOT EXISTS `env_application` (
  `application_id` int(11) NOT NULL AUTO_INCREMENT,
  `application_name` varchar(255) NOT NULL,
  `application_label` varchar(255) NOT NULL,
  `application_url` varchar(255) NOT NULL,
  `application_icon` varchar(255) NOT NULL,
  PRIMARY KEY (`application_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `env_application`
--


-- --------------------------------------------------------

--
-- Structure de la table `env_group`
--

CREATE TABLE IF NOT EXISTS `env_group` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(250) NOT NULL,
  `group_label` varchar(255) NOT NULL,
  `group_user_id` int(11) NOT NULL,
  `group_category` varchar(255) NOT NULL,
  `group_public` tinyint(1) NOT NULL,
  `group_ldapid` int(11) NOT NULL,
  `group_avatar` varchar(255) NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `env_group`
--


-- --------------------------------------------------------

--
-- Structure de la table `env_groupuser`
--

CREATE TABLE IF NOT EXISTS `env_groupuser` (
  `groupuser_user_id` int(11) NOT NULL,
  `groupuser_group_id` int(11) NOT NULL,
  UNIQUE KEY `groupuser_user_id` (`groupuser_user_id`,`groupuser_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `env_groupuser`
--


-- --------------------------------------------------------

--
-- Structure de la table `env_page_appgroup`
--

CREATE TABLE IF NOT EXISTS `env_page_appgroup` (
  `appgroup_page_application_id` int(11) DEFAULT NULL,
  `appgroup_application_id` int(11) DEFAULT NULL,
  `appgroup_order` int(11) NOT NULL,
  UNIQUE KEY `appgroup_index` (`appgroup_page_application_id`,`appgroup_application_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `env_page_appgroup`
--


-- --------------------------------------------------------

--
-- Structure de la table `env_page_application`
--

CREATE TABLE IF NOT EXISTS `env_page_application` (
  `page_application_id` int(11) NOT NULL AUTO_INCREMENT,
  `page_application_loc` varchar(250) NOT NULL,
  `page_application_order` int(11) NOT NULL,
  `page_application_label` varchar(250) NOT NULL,
  `page_application_style` varchar(250) NOT NULL,
  `page_application_type_id` varchar(250) NOT NULL,
  `page_application_panel_id` int(11) NOT NULL,
  PRIMARY KEY (`page_application_id`),
  KEY `widget_panel_id` (`page_application_panel_id`,`page_application_loc`,`page_application_order`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `env_page_application`
--


-- --------------------------------------------------------

--
-- Structure de la table `env_page_template`
--

CREATE TABLE IF NOT EXISTS `env_page_template` (
  `page_template_id` int(11) NOT NULL AUTO_INCREMENT,
  `page_template_order` int(11) NOT NULL,
  `page_template_icon` varchar(250) NOT NULL,
  `page_template_html` longtext NOT NULL,
  PRIMARY KEY (`page_template_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `env_page_template`
--

INSERT INTO `env_page_template` (`page_template_id`, `page_template_order`, `page_template_icon`, `page_template_html`) VALUES
(1, 1, '', '    <div id="C1R1" class="column" style="width: 33%">  \r\n        <div width="100%">&nbsp;</div>\r\n    </div>  \r\n  \r\n    <div id="C2R1" class="column" style="width: 33%">  \r\n	<div width="100%">&nbsp;</div>  \r\n    </div>  \r\n  \r\n    <div id="C3R1" class="column" style="width: 33%">  \r\n	<div width="100%">&nbsp;</div>\r\n    </div>'),
(2, 2, '', '<div id="envolepanelvertical">\r\n	<div id="C1R1" class="column">  \r\n	        <div width="100%">&nbsp;</div>\r\n	</div> \r\n</div>\r\n\r\n  '),
(3, 3, '', '<div id="envolepanelvertical">\r\n	<div id="C1R1" class="column">  \r\n	        <div width="100%">&nbsp;</div>\r\n	</div> \r\n</div>');

-- --------------------------------------------------------

--
-- Structure de la table `env_page_widget`
--

CREATE TABLE IF NOT EXISTS `env_page_widget` (
  `page_widget_id` int(11) NOT NULL AUTO_INCREMENT,
  `page_widget_loc` varchar(250) NOT NULL,
  `page_widget_order` int(11) NOT NULL,
  `page_widget_label` varchar(250) NOT NULL,
  `page_widget_style` varchar(250) NOT NULL,
  `page_widget_type_id` varchar(250) NOT NULL,
  `page_widget_panel_id` int(11) NOT NULL,
  `page_widget_url` varchar(250) NOT NULL,
  `page_widget_editeur_id` int(11) NOT NULL,
  `page_widget_height` varchar(10) NOT NULL,
  `page_widget_fgborder` tinyint(1) NOT NULL,
  PRIMARY KEY (`page_widget_id`),
  KEY `widget_panel_id` (`page_widget_panel_id`,`page_widget_loc`,`page_widget_order`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `env_page_widget`
--


-- --------------------------------------------------------

--
-- Structure de la table `env_panel`
--

CREATE TABLE IF NOT EXISTS `env_panel` (
  `panel_id` int(11) NOT NULL AUTO_INCREMENT,
  `panel_label` varchar(250) NOT NULL,
  `panel_url` varchar(250) NOT NULL,
  `panel_order` int(11) NOT NULL,
  `panel_visible` tinyint(1) NOT NULL,
  `panel_user_id` int(11) NOT NULL,
  `panel_type_id` int(11) NOT NULL,
  `panel_page_template_id` int(11) DEFAULT NULL,
  `panel_page_css_name` varchar(255) NOT NULL,
  `panel_page_html` longtext NOT NULL,
  PRIMARY KEY (`panel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `env_panel`
--


-- --------------------------------------------------------

--
-- Structure de la table `env_panel_profil`
--

CREATE TABLE IF NOT EXISTS `env_panel_profil` (
  `panel_profil_panel_id` int(11) NOT NULL,
  `panel_profil_profil_id` int(11) NOT NULL,
  `panel_profil_order` int(11) NOT NULL,
  UNIQUE KEY `panel_profil_panel_id` (`panel_profil_panel_id`,`panel_profil_profil_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `env_panel_profil`
--


-- --------------------------------------------------------

--
-- Structure de la table `env_panel_type`
--

CREATE TABLE IF NOT EXISTS `env_panel_type` (
  `panel_type_type_id` int(11) NOT NULL,
  `panel_type_type_name` varchar(255) NOT NULL,
  UNIQUE KEY `panel_type_id` (`panel_type_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `env_panel_type`
--

INSERT INTO `env_panel_type` (`panel_type_type_id`, `panel_type_type_name`) VALUES
(1, 'url'),
(2, 'application'),
(3, 'editeur'),
(4, 'Widget');

-- --------------------------------------------------------

--
-- Structure de la table `env_profil`
--

CREATE TABLE IF NOT EXISTS `env_profil` (
  `profil_id` int(11) NOT NULL AUTO_INCREMENT,
  `profil_label` varchar(250) NOT NULL,
  PRIMARY KEY (`profil_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=100 ;

--
-- Contenu de la table `env_profil`
--

INSERT INTO `env_profil` (`profil_id`, `profil_label`) VALUES
(1, 'administrateur'),
(2, 'moderateur'),
(3, 'utilisateur'),
(99, 'invite');

-- --------------------------------------------------------

--
-- Structure de la table `env_type`
--

CREATE TABLE IF NOT EXISTS `env_type` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_nature` varchar(50) NOT NULL,
  `type_label` varchar(250) NOT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `env_type`
--

INSERT INTO `env_type` (`type_id`, `type_nature`, `type_label`) VALUES
(1, 'PANEL', 'URL');

-- --------------------------------------------------------

--
-- Structure de la table `env_user`
--

CREATE TABLE IF NOT EXISTS `env_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_login` varchar(250) NOT NULL,
  `user_password` varchar(250) NOT NULL,
  `user_lastname` varchar(250) NOT NULL,
  `user_firstname` varchar(250) NOT NULL,
  `user_pseudo` varchar(255) NOT NULL,
  `user_avatar` varchar(250) NOT NULL,
  `user_email` varchar(250) NOT NULL,
  `user_mod` varchar(250) NOT NULL,
  `user_profil_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_login` (`user_login`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1257 ;

--
-- Contenu de la table `env_user`
--

INSERT INTO `env_user` (`user_id`, `user_login`, `user_password`, `user_lastname`, `user_firstname`, `user_pseudo`, `user_avatar`, `user_email`, `user_mod`, `user_profil_id`) VALUES
(1, 'admin', '5eae19b1180e7541657ce5a7f921d750', 'admin', 'admin', 'Administrateur', 'thumbnail_1355834561.jpg', 'admin@virtualbox23.ac-arno.fr', 'big', 1);

-- --------------------------------------------------------

--
-- Structure de la table `env_widget_type`
--

CREATE TABLE IF NOT EXISTS `env_widget_type` (
  `widget_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `widget_type_name` varchar(255) NOT NULL,
  `widget_type_label` varchar(255) NOT NULL,
  `widget_type_icon` varchar(255) NOT NULL,
  `widget_type_url` varchar(255) NOT NULL,
  `widget_type_code` longtext NOT NULL,
  `widget_type_height` varchar(10) NOT NULL,
  `widget_type_fgborder` tinyint(1) NOT NULL,
  `widget_type_content_id` int(11) NOT NULL,
  PRIMARY KEY (`widget_type_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Contenu de la table `env_widget_type`
--

INSERT INTO `env_widget_type` (`widget_type_id`, `widget_type_name`, `widget_type_label`, `widget_type_icon`, `widget_type_url`, `widget_type_code`, `widget_type_height`, `widget_type_fgborder`, `widget_type_content_id`) VALUES
(-500, 'Page Web', 'Afficher une page web', 'url.jpg', '', '', '300px', 0, 1),
(-490, 'Flux RSS', 'Afficher le contenu d''un flux RSS provenant d''un autre site', 'rss.gif', '', '', '300px', 0, 2),
(-480, 'Page Editeur', 'Votre propre texte à éditer', 'editeur.jpg', 'editeur.php', '', '600px', 0, 3),
(5, 'Gadget Météo', 'Petit gadget pour afficher la météo', 'thumbnail_1357727103.jpg', 'http://widget.widgetmeteo.com/meteo-fr/blog/meteo-fr.html', '', '240px', 0, 4),
(6, 'Gadget Page Jaune', 'Petit gadget pour réaliser des recherches dans les pages jaune', 'thumbnail_1357729088.png', 'http://51w3pt.widget.uwa.netvibes.com/widget/frame?platform=igoogle&uwaUrl=http%3A%2F%2Fwidget.pagesjaunes.fr%2Fuwa%2Findex.html', '', '240px', 0, 4),
(7, 'Gadget Horloge', 'Afficher l''heure', 'thumbnail_1357729685.jpg', 'http://www.gmodules.com/gadgets/ifr?view=home&url=http://www.gstatic.com/ig/modules/datetime_v3/datetime_v3.xml&lang=fr', '', '180px', 0, 4),
(8, '20 Minutes', 'Les News du quotidien 20 Minutes', 'thumbnail_1357729800.jpg', 'http://520p8x.widget.uwa.netvibes.com/widget/frame?platform=igoogle&uwaUrl=http%3A%2F%2Fuwa.netvibes.com%2Fapps%2Fmultifeed.php%3Fprovider%3D20minutes%26title%3D20Minutes.fr', '', '280px', 0, 4),
(9, 'Le Monde', 'Flux RSS du quotidien le Monde', 'thumbnail_1357745255.jpg', 'http://rss.lemonde.fr/c/205/f/3050/index.rss', '', '300px', 1, 2),
(10, 'Gadget Traduction', 'Traduction Français / Anglais', 'thumbnail_1357834089.jpg', 'http://51wbfc.widget.uwa.netvibes.com/widget/frame?platform=igoogle&uwaUrl=http%3A%2F%2Fnvmodules.typhon.net%2Fantoine%2Fcnrs.html', '<script type="text/javascript">\r\nvar UWA = {hosts:{"uwa":"http://uwa.netvibes.com","exposition":"http://uwa.netvibes.com"}}, UWA_ASYNC = UWA_ASYNC || [];\r\n    UWA_ASYNC.push({url: "http://nvmodules.typhon.net/antoine/traducteur.html",options:{"title":"Traducteur","buildHeader":true,"displayHeader":false}});\r\n(function () {\r\n    var a = document.getElementsByTagName("script"), b = a[a.length - 1] || document.body.lastChild, c = b.parentNode, d = document.createElement("script"), e = document.createElement("div");\r\n    e.id = "UWA_ASYNC"; d.type = "text/javascript"; d.async = true;\r\n    d.src = ("https:" == document.location.protocol ? "https://" : "http://") + UWA.hosts.uwa.split("://")[1] + "/lib/c/UWA/js/UWA_Embedded.js?v=preview4";\r\n    c.insertBefore(d, b); c.insertBefore(e, b)\r\n})();\r\n</script>', '150px', 0, 5);

-- --------------------------------------------------------

--
-- Structure de la table `env_widget_type_content`
--

CREATE TABLE IF NOT EXISTS `env_widget_type_content` (
  `widget_type_content_id` int(11) NOT NULL AUTO_INCREMENT,
  `widget_type_content_name` varchar(255) NOT NULL,
  PRIMARY KEY (`widget_type_content_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `env_widget_type_content`
--

INSERT INTO `env_widget_type_content` (`widget_type_content_id`, `widget_type_content_name`) VALUES
(1, 'Page Web'),
(2, 'Flux RSS'),
(3, 'Page Editeur'),
(4, 'Gadget'),
(5, 'Code');
