<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?
	include_once("include/include.php");				/* Général		*/
	include_once($config["templatedir"]."/html.php");	/* Entete HTML	*/
	include_once($config["templatedir"]."/popup.php");
	$db1=new ps_db;
	
	$idpan=$_GET['idpan'];
	$idwid=$_POST['idwid'];
	$fgwid=$_POST['fgwid'];
	
	$fgval=$_POST['fgval'];
	$wid_label=$_POST['wid_label'];
	$wid_url=$_POST['wid_url'];
	$wid_height=$_POST['wid_height'];
	$wid_border=$_POST['wid_border'];
	
	if($idpan=="") { $idpan=$_POST['idpan']; }
	
	$fgclo=$_POST['fgclo'];
	

	if($fgwid!="") {
		$fgparam=1;
	}

	if($fgval!="") {
		// Hauteur par défaut
		if($wid_height==0) $wid_height="";
		else $wid_height=$wid_height.'px';
		
		// Recherche du type de contenu du widget
		$q="SELECT * FROM env_widget_type WHERE widget_type_id=$idwid";
		$db1->query($q);
		if($db1->next_record()) {
			$tpwid=$db1->f("widget_type_content_id");
		}
		
		// Rechercher le dernier order de la location C1R1 de cette page
		$order=1;
		$q="SELECT page_widget_order FROM env_page_widget WHERE page_widget_panel_id=$idpan AND page_widget_loc='C1R1' ORDER BY page_widget_order DESC LIMIT 1";
		$db1->query($q);
		if($db1->next_record()) {
			$order=$db1->f('page_widget_order')+1;
		}
		
		// Si création widget de type editeur creation de la page à vide
		if($tpwid==3) {
			// Recherche du dernier numéro d'ordre de panel de l'utilisateur 
			$q="SELECT panel_order FROM env_panel WHERE panel_user_id=".$_SESSION['user_id']." ORDER BY panel_order DESC";
			$db1->query($q);
			$order=1;
			if($db1->next_record()) {
				$order=$db1->f('panel_order')+1;
			}
		
			// Création du panel
			$q="INSERT INTO env_panel (panel_label, panel_url, panel_user_id, panel_order, panel_type_id, panel_visible,panel_page_html) VALUES('".$wid_label."','editeur.php',".$_SESSION['user_id'].",".$order.",3,0,'<center><br><br><br>Modifier votre page pour insérer votre texte</center>')";
			$db1->query($q);
			
			// Récupération du panel généré
			$q="SELECT panel_id FROM env_panel WHERE panel_user_id=".$_SESSION['user_id']." AND panel_order=".$order." AND NOT panel_visible";
			$db1->query($q);
			if($db1->next_record()) {
				$idedt=$db1->f('panel_id');
			}
		
			$wid_url="editeur.php?idpan=$idedt";
		}
		
		
		// Création du widget
		$q="INSERT INTO env_page_widget (page_widget_loc,page_widget_order,page_widget_type_id,page_widget_panel_id,page_widget_label,page_widget_url,page_widget_editeur_id,page_widget_height,page_widget_fgborder) VALUES('C1R1',$order,$idwid,$idpan,'$wid_label','$wid_url','$idedt','$wid_height',$wid_border)";
		$db1->query($q);	
		
		$fgclo=1;
	}
		
	
	if($fgclo!="") {
		// Action javascript Ajout du panel directement + fermeture de la popup
		$jsaction="recharge(".$idpan.")";
	}
	
?>

<script type="text/JavaScript">
	function recharge(idpan) {
		parent.parent.document.location.href='index.php?idpan='+idpan;
		//iframeclosepopup();
	}
	<?php echo $jsaction ?>
</script>

</head>

<body>

<h1>Ajout d'un widget</h1>

<div id="envolepage">
<form name="Formulaire" method="post">

<?
//== SELECTION DU WIDGET =====================================================================================================================================================================
if($fgparam!=1) {
	
	$q="SELECT * FROM env_widget_type";
	$db1->query($q);
	while($db1->next_record()) {
	?>
		<div class="bibliowidget">
			<div class="bibliowidgetimg" style="background-image: url(<? echo $config["widgeticondir"]."/".$db1->f('widget_type_icon'); ?>)">
				<img src="images/blank.gif">
				<div class="bibliowidgetlink">
					<center>
						<input id="fgwid" name="fgwid" style="position: relative;" class="input-rounded-button" type="submit" onClick="$('#idwid').val(<? echo $db1->f("widget_type_id"); ?>);" value="Choisir"/>
					</center>
				</div>
			</div>
			
			<div class="bibliowidgettext">
				<h2><? echo $db1->f("widget_type_name"); ?></h2>
				<? echo $db1->f("widget_type_label"); ?>
			</div>
		</div>
	<?
	}
	?>

	<center>
	<input id="fgclo" name="fgclo" class="input-rounded-button" type="submit" value="Annuler" />	
	</center>
<?
}
//== PARAMETRAGE DU WIDGET ===================================================================================================================================================================
else {

	// Recherche du paramétrage par défaut du widget
	$q="SELECT * FROM env_widget_type WHERE widget_type_id=$idwid";
	$db1->query($q);
	if($db1->next_record()) {
		$ulwid=$db1->f("widget_type_url");
		$brwid=$db1->f("widget_type_fgborder");
		$hewid=str_replace("px","",$db1->f("widget_type_height"));
	}	
	
?>
	<script>
		$("#fgclo").val('Annuler');
	</script>
	
 	<h2>Paramètres</h2>

	<fieldset id="encadrer">
		<?
			$lbsty00="style='width:40%; border-bottom: dotted 1px;'";
			$lbsty01="type='text' size='53' style='width: 59%;'";
			$lbsty02="type='password' size='53' style='width: 59%;'";
			$lbsty03="style='width: 59%;'";
		?>

		<span id="encadrer_label" <? echo $lbsty00; ?>>Titre</span>
		<input 	value="<? echo $wid_label; ?>"
				id="wid_label"
				name="wid_label"	
				<? echo $lbsty01; ?>>


		<p <? if($ulwid!="") { echo "style='display: none;'"; } ?>>
		<span id="encadrer_label" <? echo $lbsty00; ?>>URL</span>
		<input	value="<? echo $ulwid; ?>"
				id="wid_url" 
				name="wid_url"
				<? echo $lbsty01; ?>>
		</p>
		
		<p>
		<span id="encadrer_label" <? echo $lbsty00; ?>>Hauteur*</span>
		<input	value="<? echo $hewid; ?>"
				id="wid_height" 
				name="wid_height"
				type="number"
				<? echo $lbsty01; ?>>
		</p>		

		<p>
		<span id="encadrer_label" <? echo $lbsty00; ?>>Afficher une bordure autour du widget*</span>
		<select	id="wid_border"
				name="wid_border"
				size="1"
				<? echo $lbsty03; ?>>		
			<option value="0"	<? if($brwid=='0')	{ echo 'selected'; }?>>non</option>
			<option value="1"	<? if($brwid=='1')	{ echo 'selected'; }?>>oui</option>
		</select>
		</p>		
	</fieldset>
	<br>
	
	<center>
	<input id="fgval" name="fgval" class="input-rounded-button" type="submit" value="Valider" />
	<input id="fgclo" name="fgclo" class="input-rounded-button" type="submit" value="Annuler" />	
	</center>
	
	<script type="text/javascript">
		$('#wid_label').focus();
	</script>	
<?
}
?>


<input type="hidden" name="idpan" id="idpan" value="<? echo $idpan; ?>" /> 
<input type="hidden" name="idwid" id="idwid" value="<? echo $idwid; ?>" /> 

	
</form>
</div>

</body>
</html>
