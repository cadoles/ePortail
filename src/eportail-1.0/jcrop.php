<?php
	error_reporting (E_ALL ^ E_NOTICE);
	session_start(); //Do not remove this

	include_once("include/include.php");				/* Général		*/
	include_once($config["templatedir"]."/html.php");	/* Entete HTML	*/
	include_once($config["templatedir"]."/popup.php");

	$db1=new ps_db;
	
	//only assign a new timestamp if the session variable is empty
	if (!isset($_SESSION['random_key']) || strlen($_SESSION['random_key'])==0||$_GET['dirimg']!=""){
		$_SESSION['random_key'] = strtotime(date('Y-m-d H:i:s')); //assign the timestamp to the session variable
		$_SESSION['user_file_ext']= "";
	}

	// Répertoire de stockage des miniatures
	if($_GET['dirimg']=="") {
		$upload_dir=$_SESSION['dirimg'];
		$igico=$_SESSION['igico'];
		$lbico=$_SESSION['lbico'];
		$query=$_SESSION['query'];
		
		$idico=$_SESSION['idico'];
		$queryid=$_SESSION['queryid'];
		$idbdd=$_SESSION['idbdd'];
		
		$refresh=$_SESSION['refresh'];
	}
	else{
		$_SESSION['dirimg']= $_GET['dirimg'];
		$_SESSION['igico']= $_GET['igico'];
		$_SESSION['lbico']= $_GET['lbico'];
		$_SESSION['query']= $_GET['query'];

		$_SESSION['idico']= $_GET['idico'];		
		$_SESSION['queryid']= $_GET['queryid'];
		$_SESSION['idbdd']= $_GET['idbdd'];
		
		$_SESSION['refresh']= $_GET['refresh'];
		$upload_dir=$_GET['dirimg'];
	}
	
	
	$upload_path 			= $upload_dir."/";								// The path to where the image will be saved
	$large_image_prefix 	= "resize_";									// The prefix name to large image
	$thumb_image_prefix 	= "thumbnail_";									// The prefix name to the thumb image
	$large_image_name 		= $large_image_prefix.$_SESSION['random_key']; 	// New name of the large image (append the timestamp to the filename)
	$thumb_image_name 		= $thumb_image_prefix.$_SESSION['random_key']; 	// New name of the thumbnail image (append the timestamp to the filename)
	$max_file 				= "3"; 											// Maximum file size in MB
	$max_height				= "400";										// Max height allowed for the large image
	$max_width				= "590";										// Max width allowed for the large image
	$thumb_width 			= "90";											// Width of thumbnail image
	$thumb_height 			= "90";											// Height of thumbnail image

	// Format d'image autorisée
	$allowed_image_types = array('image/pjpeg'=>"jpg",'image/jpeg'=>"jpg",'image/jpg'=>"jpg",'image/png'=>"png",'image/x-png'=>"png",'image/gif'=>"gif");
	$allowed_image_ext = array_unique($allowed_image_types);
	$image_ext = "";
	foreach ($allowed_image_ext as $mime_type => $ext) {
		$image_ext.= strtoupper($ext)." ";
	}


	##########################################################################################################
	# IMAGE FUNCTIONS																						 #
	# You do not need to alter these functions																 #
	##########################################################################################################
	function resizeImage($image,$width,$height,$scale) {
		list($imagewidth, $imageheight, $imageType) = getimagesize($image);
		$imageType = image_type_to_mime_type($imageType);
		$newImageWidth = ceil($width * $scale);
		$newImageHeight = ceil($height * $scale);
		$newImage = imagecreatetruecolor($newImageWidth,$newImageHeight);
		switch($imageType) {
			case "image/gif":
				$source=imagecreatefromgif($image); 
				break;
			case "image/pjpeg":
			case "image/jpeg":
			case "image/jpg":
				$source=imagecreatefromjpeg($image); 
				break;
			case "image/png":
			case "image/x-png":
				$source=imagecreatefrompng($image); 
				break;
		}
		imagecopyresampled($newImage,$source,0,0,0,0,$newImageWidth,$newImageHeight,$width,$height);
		
		switch($imageType) {
			case "image/gif":
				imagegif($newImage,$image); 
				break;
			case "image/pjpeg":
			case "image/jpeg":
			case "image/jpg":
				imagejpeg($newImage,$image,90); 
				break;
			case "image/png":
			case "image/x-png":
				imagepng($newImage,$image);  
				break;
		}
		
		chmod($image, 0777);
		return $image;
	}
	//You do not need to alter these functions
	function resizeThumbnailImage($thumb_image_name, $image, $width, $height, $start_width, $start_height, $scale){
		list($imagewidth, $imageheight, $imageType) = getimagesize($image);
		$imageType = image_type_to_mime_type($imageType);
		
		$newImageWidth = ceil($width * $scale);
		$newImageHeight = ceil($height * $scale);
		$newImage = imagecreatetruecolor($newImageWidth,$newImageHeight);
		switch($imageType) {
			case "image/gif":
				$source=imagecreatefromgif($image); 
				break;
			case "image/pjpeg":
			case "image/jpeg":
			case "image/jpg":
				$source=imagecreatefromjpeg($image); 
				break;
			case "image/png":
			case "image/x-png":
				$source=imagecreatefrompng($image); 
				break;
		}
		imagecopyresampled($newImage,$source,0,0,$start_width,$start_height,$newImageWidth,$newImageHeight,$width,$height);
		switch($imageType) {
			case "image/gif":
				imagegif($newImage,$thumb_image_name); 
				break;
			case "image/pjpeg":
			case "image/jpeg":
			case "image/jpg":
				imagejpeg($newImage,$thumb_image_name,90); 
				break;
			case "image/png":
			case "image/x-png":
				imagepng($newImage,$thumb_image_name);  
				break;
		}
		chmod($thumb_image_name, 0777);
		return $thumb_image_name;
	}

	// Calcul de la hauteur
	function getHeight($image) {
		$size = getimagesize($image);
		$height = $size[1];
		return $height;
	}

	// Cacul de la largeur
	function getWidth($image) {
		$size = getimagesize($image);
		$width = $size[0];
		return $width;
	}

	// Localisation de l'image
	$large_image_location = $upload_path.$large_image_name.$_SESSION['user_file_ext'];
	$thumb_image_location = $upload_path.$thumb_image_name.$_SESSION['user_file_ext'];
	
	// Vérifier l'existance d'une image existe ou non
	if (file_exists($large_image_location)){
		if(file_exists($thumb_image_location)){
			$thumb_photo_exists = "<img src=\"".$upload_path.$thumb_image_name.$_SESSION['user_file_ext']."\" alt=\"Thumbnail Image\"/>";
		}else{
			$thumb_photo_exists = "";
		}
		
		$large_photo_exists = "<img src=\"".$upload_path.$large_image_name.$_SESSION['user_file_ext']."\" alt=\"Large Image\"/>";
	} else {
		$large_photo_exists = "";
		$thumb_photo_exists = "";
	}

	if (isset($_POST["upload"])) { 
		//Get the file information
		$userfile_name = $_FILES['image']['name'];
		$userfile_tmp = $_FILES['image']['tmp_name'];
		$userfile_size = $_FILES['image']['size'];
		$userfile_type = $_FILES['image']['type'];
		$filename = basename($_FILES['image']['name']);
		$file_ext = strtolower(substr($filename, strrpos($filename, '.') + 1));
		
		//Only process if the file is a JPG, PNG or GIF and below the allowed limit
		if((!empty($_FILES["image"])) && ($_FILES['image']['error'] == 0)) {
			
			foreach ($allowed_image_types as $mime_type => $ext) {
				//loop through the specified image types and if they match the extension then break out
				//everything is ok so go and check file size
				if($file_ext==$ext && $userfile_type==$mime_type){
					$error = "";
					break;
				}else{
					$error = "Seule les images aux formats suivant sont acceptées <strong>".$image_ext."</strong><br />";
				}
			}
			//check if the file size is above the allowed limit
			if ($userfile_size > ($max_file*1048576)) {
				$error.= "L'image doit être d'une taille inférieure à ".$max_file."MB";
			}
			
		}else{
			$error= "Veuillez sélectionner une image";
		}
		//Everything is ok, so we can upload the image.
		if (strlen($error)==0){
			
			if (isset($_FILES['image']['name'])){
				//this file could now has an unknown file extension (we hope it's one of the ones set above!)
				$large_image_location = $upload_path.$large_image_name.".".$file_ext;
				$thumb_image_location = $upload_path.$thumb_image_name.".".$file_ext;
				
				//put the file ext in the session so we know what file to look for once its uploaded
				$_SESSION['user_file_ext']=".".$file_ext;
				
				move_uploaded_file($userfile_tmp, $large_image_location);
				chmod($large_image_location, 0777);
				
				$width = getWidth($large_image_location);
				$height = getHeight($large_image_location);
				
				//Scale the image if it is greater than the width set above
				$scale = $max_height/$height;
				if(($width*$scale)>$max_width) {
					$scale = $max_width/$width;
				}
				$uploaded = resizeImage($large_image_location,$width,$height,$scale);
				
				//Delete the thumbnail file so the user can create a new one
				if (file_exists($thumb_image_location)) {
					unlink($thumb_image_location);
				}
			}
			//Refresh the page to show the new uploaded image
			header("location:".$_SERVER["PHP_SELF"]);
			exit();
		}
	}

	if (isset($_POST["upload_thumbnail"]) && strlen($large_photo_exists)>0) {
		//Get the new coordinates to crop the image.
		$x1 = $_POST["x1"];
		$y1 = $_POST["y1"];
		$x2 = $_POST["x2"];
		$y2 = $_POST["y2"];
		$w = $_POST["w"];
		$h = $_POST["h"];
		
		//Scale the image to the thumb_width set above
		$scale = $thumb_width/$w;
		$cropped = resizeThumbnailImage($thumb_image_location, $large_image_location,$w,$h,$x1,$y1,$scale);
				
		//Reload the page again to view the thumbnail
		header("location:".$_SERVER["PHP_SELF"]);
		exit();
	}


	if ($_GET['a']=="submit" && strlen($_GET['t'])>0){
		$large_image_location = $upload_path.$large_image_prefix.$_GET['t'];
		$thumb_image_location = $upload_path.$thumb_image_prefix.$_GET['t'];
		if (file_exists($large_image_location)) {
			unlink($large_image_location);
		}
		
		$jsaction="";
		if($lbico!="") $jsaction=$jsaction." parent.document.getElementById('$lbico').value='".$thumb_image_prefix.$_GET['t']."';";
		if($igico!="") $jsaction=$jsaction." parent.document.getElementById('$igico').src='$thumb_image_location';";
		
		
			
		if($query!="") {
			$q=str_replace("$$","'".$thumb_image_prefix.$_GET['t']."'","$query");
			$db1->query($q);
		}
		
		if($queryid!=""&&$idico!="") {
			
			$q=$queryid;
			$db1->query($q);
			if($db1->next_record()) {
				$jsaction=$jsaction." parent.document.getElementById('$idico').value='".$db1->f($idbdd)."';";
			}
		}
		
		if($refresh!="") $jsaction=$jsaction."parent.document.location.reload() ; ";
		$jsaction=$jsaction." iframeclosepopup();";
	}

	if ($_GET['a']=="delete" && strlen($_GET['t'])>0){
	//get the file locations 
		$large_image_location = $upload_path.$large_image_prefix.$_GET['t'];
		$thumb_image_location = $upload_path.$thumb_image_prefix.$_GET['t'];
		if (file_exists($large_image_location)) {
			unlink($large_image_location);
		}
		if (file_exists($thumb_image_location)) {
			unlink($thumb_image_location);
		}

		$jsaction="iframeclosepopup();";
	}
?>	

	<script type="text/javascript" src="<? echo $config['dirjcrop']; ?>/js/jquery-pack.js"></script>
	<script type="text/javascript" src="<? echo $config['dirjcrop']; ?>/js/jquery.imgareaselect.min.js"></script>
	
<script type="text/JavaScript">
	<?php echo $jsaction ?>
</script>	
</head>



<body>
	
<?php
	// Script javascript uniquement si présence d'une imgage téléchargée
	if(strlen($large_photo_exists)>0){
		$current_large_image_width = getWidth($large_image_location);
		$current_large_image_height = getHeight($large_image_location);
?>

<script type="text/javascript">
	function preview(img, selection) { 
		var scaleX = <?php echo $thumb_width;?> / selection.width; 
		var scaleY = <?php echo $thumb_height;?> / selection.height; 
		
		$('#thumbnail + div > img').css({ 
			width: Math.round(scaleX * <?php echo $current_large_image_width;?>) + 'px', 
			height: Math.round(scaleY * <?php echo $current_large_image_height;?>) + 'px',
			marginLeft: '-' + Math.round(scaleX * selection.x1) + 'px', 
			marginTop: '-' + Math.round(scaleY * selection.y1) + 'px' 
		});
		$('#x1').val(selection.x1);
		$('#y1').val(selection.y1);
		$('#x2').val(selection.x2);
		$('#y2').val(selection.y2);
		$('#w').val(selection.width);
		$('#h').val(selection.height);
	} 

	$(document).ready(function () { 
		$('#save_thumb').click(function() {
			var x1 = $('#x1').val();
			var y1 = $('#y1').val();
			var x2 = $('#x2').val();
			var y2 = $('#y2').val();
			var w = $('#w').val();
			var h = $('#h').val();
			if(x1=="" || y1=="" || x2=="" || y2=="" || w=="" || h==""){
				alert("You must make a selection first");
				return false;
			}else{
				return true;
			}
		});
	}); 

	$(window).load(function () { 
		$('#thumbnail').imgAreaSelect({ aspectRatio: '1:<?php echo $thumb_height/$thumb_width;?>', onSelectChange: preview }); 
	});

</script>
<?php }?>


<?php

	if($large_photo_exists==""){
		echo "<h1>ETAPE 1 - TELECHARGEZ UNE IMAGE</h1>";
		echo "<form name='photo' enctype='multipart/form-data' action='".$_SERVER["PHP_SELF"]."' method='post'>";
		echo "<br><input type='file' name='image' size='30' class='inputfile' /><br><br>";
		echo "<input type='submit' name='upload' value='Télécharger'class='input-rounded-button'  />";
		echo "</form>";
	}
	else {
		if(strlen($large_photo_exists)>0 && $thumb_photo_exists==""){
			echo "<h1>ETAPE 2 - CREATION DE LA MINIATURE</h1>";
			
			echo "<form name='thumbnail' action='".$_SERVER["PHP_SELF"]."' method='post'>";
			echo "<input type='hidden' name='x1' value='' id='x1' />";
			echo "<input type='hidden' name='y1' value='' id='y1' />";
			echo "<input type='hidden' name='x2' value='' id='x2' />";
			echo "<input type='hidden' name='y2' value='' id='y2' />";
			echo "<input type='hidden' name='w'  value='' id='w'  />";
			echo "<input type='hidden' name='h'  value='' id='h'  />";
			echo "<br><input type='submit' name='upload_thumbnail' value='Générer la miniature' id='save_thumb' class='input-rounded-button'  /><br><br>";
			echo "</form>";
		}
		else {
			echo "<h1>ETAPE 3 - CONFIRMATION</h1>";
				echo "<br>";
				echo "<a href=\"".$_SERVER["PHP_SELF"]."?a=delete&t=".$_SESSION['random_key'].$_SESSION['user_file_ext']."\" class='link-rounded-button'>Annuler</a>";
				echo "&nbsp;";
				echo "<a href=\"".$_SERVER["PHP_SELF"]."?a=submit&t=".$_SESSION['random_key'].$_SESSION['user_file_ext']."\" class='link-rounded-button'>Valider</a><br>";
				echo "<br>";
		}
	}
		

	// Afficher l'erreur
	if(strlen($error)>0){
		echo "<br><strong>Erreur : </strong><br>".$error."<br><br>";
	}

	if(strlen($large_photo_exists)>0 && strlen($thumb_photo_exists)>0){
		echo $large_photo_exists."&nbsp;".$thumb_photo_exists;

		//Clear the time stamp session and user file extension
		$_SESSION['random_key']= "";
		$_SESSION['user_file_ext']= "";
	}
	
	else{
		if(strlen($large_photo_exists)>0){
		?>
			<div align="center">
				<img src="<?php echo $upload_path.$large_image_name.$_SESSION['user_file_ext'];?>" style="float: left; margin-right: 10px;" id="thumbnail" alt="Create Thumbnail" />
				<div style="border:1px #e5e5e5 solid; float:left; position:relative; overflow:hidden; width:<?php echo $thumb_width;?>px; height:<?php echo $thumb_height;?>px;">
					<img src="<?php echo $upload_path.$large_image_name.$_SESSION['user_file_ext'];?>" style="position: relative;" alt="Thumbnail Preview" />
				</div>
				<br style="clear:both;"/>

			</div>
		<? } ?>
	<? } ?>

</body>
</html>
