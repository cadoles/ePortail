<?
$config=Array();

/* Serveur */
$config['urlbase']						= "https://envole.ac-dijon.fr/envole-portail/";

/* CAS */
$config['activerCAS']					= true;

/* Synchronisation LDAP */
$config['activerLDAP']					= true;
$config['LDAPserver']					= "localhost";
$config['LDAPport'] 					= "389";
$config['LDAProotdn'] 					= "cn=admin,o=gouv,c=fr";
$config['LDAPracine'] 					= "o=gouv,c=fr";
$config['LDAProotpw']					= "123456";

$config['LDAPFiltreUser']				= "uid=*";
$config['LDAPLoginAttribut']			= "uid";
$config['LDAPAdminAttribut']			= "typeadmin";
$config['LDAPProfilAttribut']			= "ENTPersonProfils";
$config['LDAPProfil']['eleve']			= 3; /* Utilisateur	*/
$config['LDAPProfil']['responsable']	= 3; /* Utilisateur	*/
$config['LDAPProfil']['enseignant']		= 2; /* Modérateur 	*/
$config['LDAPLastNameAttribut']			= "sn";
$config['LDAPFirstNameAttribut']		= "givenName";
$config['LDAPEmailAttribut']			= "mail";
$config['LDAPDelUser']					= true;

$config['LDAPFiltreGroup']				= "memberUid=*";
$config['LDAPGrpIdAttribut']			= "gidNumber";
$config['LDAPGrpDisplayNameAttribut']	= "displayName";
$config['LDAPGrpTypeAttribut']			= "type";
$config['LDAPGrpDescriptionAttribut']	= "description";
$config['LDAPGrpMemberAttribut']		= "memberUid";
$config['LDAPDelGroup']					= true;


/* Database */
$config['dbhost'] 						= "localhost";
$config['dblogin'] 						= "envole";
$config['dbpassword'] 					= "envole";
$config['dbname'] 						= "envole";

/* Librairie */
$config['libjquery']					= "javascript/jquery-1.3.2.min.js";
$config['libjqueryui']					= "javascript/jquery-ui-1.7.1.custom.min.js";
$config['libtable']						= "javascript/jquery.dataTables.js";
$config['libpopup']						= "javascript/popup.js";
$config['librss']						= "plugins/feedek/FeedEk.js";

/* Plugins */
$config['dirjcrop']						= "plugins/jcrop";

/* Style */
$config['styledir']						= "styles/dijon/css";

/* Template */
$config['templatedir']					= "styles/dijon/tpl";

/* Variables Globales */
$config['envtitle']						= "Envole-Portail";
$config['envinscription']				= "oui";
$config['avatardir']					= "images/avatar";
$config['appicondir']					= "images/appicon";
$config['widgeticondir']				= "images/widget";
$config['javascriptdir']				= "javascript";


?>
