<?

function delPageApplication($idapp) {
	$dbdel=new ps_db;
	
	// Suppression des page application groupe
	$q="DELETE FROM env_page_appgroup WHERE appgroup_page_application_id=$idapp";
	$dbdel->query($q);
	
	// Suppression de la page application en elle meme
	$q="DELETE FROM env_page_application WHERE page_application_id=$idapp";
	$dbdel->query($q);
}

function delPanel($idpan) {
	$dbdel=new ps_db;
	
	// Suppression des page application du panel
	$q="SELECT * FROM env_page_application WHERE page_application_panel_id=$idpan";
	$dbdel->query($q);
	while($dbdel->next_record()) {
		delPageApplication($dbdel->f("page_application_id"));
	}

	// Suppression des page widget du panel
	$q="DELETE FROM env_page_widget WHERE page_widget_editeur_id=$idpan";
	$dbdel->query($q);
	
	// Suppression des page widget du panel
	$q="DELETE FROM env_page_widget WHERE page_widget_panel_id=$idpan";
	$dbdel->query($q);
	
	// Suppression du lien panel profil
	$q="DELETE FROM env_panel_profil WHERE pane_profil_panel_id=$idpan";
	$dbdel->query($q);

	// Suppression du panel en lui meme
	$q="DELETE FROM env_panel WHERE panel_id=$idpan";
	$dbdel->query($q);
}

function delApplication($idapp) {
	$dbdel=new ps_db;
	
	// Suppression des page application groupe
	$q="DELETE FROM env_page_appgroup WHERE appgroup_application_id=$idapp";
	$dbdel->query($q);
	
	// Suppression de l'application en question
	$q="DELETE FROM env_application WHERE application_id=$idapp";
	$dbdel->query($q);
	
}

function delWidget($idwid) {
	$dbdel=new ps_db;
	
	// Suppression de la page editeur associée
	$q="SELECT * FROM env_page_widget WHERE page_widget_id=$idwid AND page_widget_editeur_id>0";
	$dbdel->query($q);
	while($dbdel->next_record()) {
		delPanel($dbdel->f("page_widget_editeur_id"));
	}	
	
	// Suppression de la page widget associée
	$q="DELETE FROM env_page_widget WHERE page_widget_id=$idwid";
	$dbdel->query($q);
}

function delTypeWidget($idwid) {
	$dbdel=new ps_db;
	
	// Suppression des pages editeur associées
	$q="SELECT * FROM env_page_widget WHERE page_widget_type_id=$idwid AND page_widget_editeur_id>0";
	$dbdel->query($q);
	while($dbdel->next_record()) {
		delPanel($dbdel->f("page_widget_editeur_id"));
	}	
	
	// Suppression des pages widget associées
	$q="DELETE FROM env_page_widget WHERE page_widget_type_id=$idwid";
	$dbdel->query($q);
	
	
	// Suppression du widget en lui meme
	$q="DELETE FROM env_widget_type WHERE widget_type_id=$idwid";
	$dbdel->query($q);
}

function delGroup($idgrp) {
	$dbdel=new ps_db;

	// Suppression du lien utilisateur groupe
	$q="DELETE FROM env_groupuser WHERE groupuser_group_id=$idgrp";
	$dbdel->query($q);
	
	// Suppression du group en lui meme
	$q="DELETE FROM env_group WHERE group_id=$idgrp";
	$dbdel->query($q);
}

function delUser($idusr) {
	$dbdel=new ps_db;
	
	// Suppression du lien utilisateur groupe
	$q="DELETE FROM env_groupuser WHERE groupuser_user_id=$idusr";
	$dbdel->query($q);
	
	// Suppression des groupes propriétaire à l'utilisateur
	$q="SELECT group_id FROM env_group WHERE group_user_id=$idusr";
	$dbdel->query($q);
	while($dbdel->next_record()) {
		delGroup($dbdel->f("group_id"));
	}
	
	// Suppression des pages de l'utilisateur
	$q="SELECT panel_id FROM env_panel WHERE panel_user_id=$idusr";
	$dbdel->query($q);
	while($dbdel->next_record()) {
		delPanel($dbdel->f("panel_id"));
	}

	// Suppression de l'utilisateur en lui meme
	$q="DELETE FROM env_user WHERE user_id=$idusr";
	$dbdel->query($q);
}


?>
