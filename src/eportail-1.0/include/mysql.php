<?php

/***********************************************************************

query($q) - Established connection to database and runs the query returning
a query ID if successfull.

next_record() - Returns the next row in the RecordSet for the last query run.
Returns False if RecordSet is empty or at the end.

num_rows()  -Returns the number of rows in the RecordSet from a query.

f($field_name) - Returns the value of the given field name for the current
record in the RecordSet.

sf($field_name) - Returns the value of the field name from the $vars variable
if it is set, otherwise returns the value of the current
record in the RecordSet.  Useful for handling forms that have
been submitted with errors.  This way, fields retain the values
sent in the $vars variable (user input) instead of the database
values.

p($field_name) - Prints the value of the given field name for the current
record in the RecordSet.

sp($field_name) - Prints the value of the field name from the $vars variable
if it is set, otherwise prints the value of the current
record in the RecordSet.  Useful for handling forms that have
been submitted with errors.  This way, fields retain the values
sent in the $vars variable (user input) instead of the database
values.


************************************************************************/

class ps_DB {
  
  var $lid = 0;				// Link ID for database connection
  var $qid = 0;      		// Query ID for current query
  var $row;      			// Current row in query result set
  var $record = array();  	// Current row record data
  var $error = "";    		// Error Message
  var $errno = "";    		// Error Number
  
 
  
  // Connects to DB and returns DB lid
  // PRIVATE
  function connect() {
    global $config; 

    if ($this->lid == 0) {
		$this->lid = mysql_pconnect($config['dbhost'],$config['dblogin'],$config['dbpassword']);
		if (!$this->lid) {
			echo "Impossible de se connecter au host";			
			$this->halt("Impossible de se connecter au host.");
		}

		if (!@mysql_select_db($config['dbname'],$this->lid)) {
			echo "Impossible de se connecter à la base de données";
			$this->halt("Impossible de se connecter à la base de données");
			return 0;
		}
    }
    mysql_set_charset('utf8',$this->lid);
    return $this->lid;
  }
  
  
  // Runs query and sets up the query id for the class.
  // PUBLIC
  function query($q) {
    
    if (empty($q))
    return 0;
    
    if (!$this->connect()) {
      return 0;
    }

    if ($this->qid) {
      @mysql_free_result($this->qid);
      $this->qid = 0;
    }

    $this->qid = @mysql_query($q, $this->lid);
    $this->row   = 0;
    $this->errno = mysql_errno();
    $this->error = mysql_error();
    if (!$this->qid) {
      $this->halt("Requête invalide: ".$q);
    }

    return $this->qid;
  }


  // Return next record in result set
  // PUBLIC
  function next_record() {

    if (!$this->qid) {
      $this->halt("Enregistrement suivant demandé sans requête.");
      return 0;
    }

    $this->record = @mysql_fetch_array($this->qid);
    $this->row   += 1;
    $this->errno  = mysql_errno();
    $this->error  = mysql_error();

    $stat = is_array($this->record);
    return $stat;
  }


  // Field Value
  // PUBLIC
  function f($field_name) {
    return stripslashes($this->record[$field_name]);
  }

  // Selective field value
  // PUBLIC
  function sf($field_name) {
    global $vars, $default;

    if ($vars["error"] and $vars["$field_name"]) {
      return stripslashes($vars["$field_name"]);
      } elseif ($default["$field_name"]) {
      return stripslashes($default["$field_name"]);
      } else {
      return stripslashes($this->record[$field_name]);
    }
  }

  // Print field
  // PUBLIC
  function p($field_name) {
    print stripslashes($this->record[$field_name]);
  }

  // Selective print field
  // PUBLIC
  function sp($field_name) {
    global $vars, $default;

    if ($vars["error"] and $vars["$field_name"]) {
      print stripslashes($vars["$field_name"]);
      } elseif ($default["$field_name"]) {
      print stripslashes($default["$field_name"]);
      } else {
      print stripslashes($this->record[$field_name]);
    }
  }

  // Returns the number of rows in query
  function num_rows() {

    if ($this->lid) {
      return @mysql_numrows($this->qid);
    }
    else {
      return 0;
    }
  }





  // Halt and display error message
  // PRIVATE
  function halt($msg) {
    $this->error = @mysql_error($this->lid);
    $this->errno = @mysql_errno($this->lid);
     if ($fp1 = @fopen("error.txt","a+")){
        $string1  = date("d-m-Y H:i")." Erreur de BD   :".$msg."\n";
        $string1 .= date("d-m-Y H:i")." Erreur MySQL :".$this->errno." -  ".$this->error.".\n";
        fputs($fp1,$string1);
        }; //fsi
   // printf("</td></tr></table><b>Erreur de base de données:</b> %s<br>\n", $msg);
    //printf("<b>Erreur MySQL</b>: %s (%s)<br></font>\n",
    //$this->errno,
    //$this->error);
    
    //exit;
    
  }
  
}
?>
