<?
	include_once("include/include.php");				/* Général			*/
	include_once($config["templatedir"]."/html.php");	/* Entete HTML		*/
	
	$db1=new ps_db;
	$idpan=$_GET['idpan'];
	
	include_once($config["templatedir"]."/widget.php");	/* Entete Widget	*/	
?>

	<script type="text/javascript" src="plugins/ckeditor/ckeditor.js"></script>
	<!--
	<script src="plugins/ckeditor/_sample/sample.js" type="text/javascript"></script>
	<link href="plugins/ckeditor/_sample/sample.css" rel="stylesheet" type="text/css" />
	!-->
	
	<script type="text/javascript">
		CKEDITOR.config.height = 1000;
		
		var editor, html = '';

		function createEditor()
		{
			if ( editor )
				return;


			// Create a new editor inside the <div id="editor">, setting its value to html
			var config = {};
			editor = CKEDITOR.appendTo( 'editor', config, html );
	
			$('#createEditor').css( "display","none" );
			
			$('#removeEditor').css( "display","inline" );
		}

		function removeEditor()
		{
			if ( !editor )
				return;

			// Retrieve the editor contents. In an Ajax application, this data would be
			// sent to the server or used in any other way.
			document.getElementById( 'editorcontents' ).innerHTML = html = editor.getData();
			label=encodeURIComponent(editor.getData());
			$("#info").load("process-usereditor-update.php?idpan=<? echo $idpan; ?>&html="+label);

			// Destroy the editor.
			editor.destroy();
			editor = null;
			
			$('#createEditor').css( "display","initial" );
			$('#removeEditor').css( "display","none" );	
		}

	</script>
<style>

#widgetadmin {
	background-color: #f0f0f0;
	height:20px;
	font-size: 0.8em;
	text-align: right;
	padding: 0px 10px 0px 0px;
	margin-bottom: 10px;
}

#widgetadmin a {color: #464646; }

#editorcontents {
	width: 100%;
	margin: 0px auto 0px auto;
}

</style>
	
<?
	// Récupérer le template de la page widget et le propriétaire de la page
	$q="SELECT panel_user_id, panel_page_html FROM env_panel WHERE panel_id=".$idpan;
	$db1->query($q);
	if($db1->next_record()) {
		$idpro=$db1->f('panel_user_id');
		$html=$db1->f('panel_page_html');
	}

	// Si l'utilisateur en cours est le propriétaire : il peut modifier la page
	if($idpro==$_SESSION['user_id']||$_SESSION['user_profil']==1) {
		echo "<div id='widgetadmin'>";
		echo "<a id='createEditor' title='Modifier la Page' onclick='createEditor();' style='cursor:pointer;'>Modifier la Page</a>";
		echo "<a id='removeEditor' title='Enregister les Modifications' onclick='removeEditor();' style='display:none; cursor:pointer;'>Enregister les Modifications</a>";
		echo "</div>";
	}
?>

	<!-- This div will hold the editor. -->
	<div id="editor">
	</div>
	

	<!-- This div will be used to display the editor contents. -->
	<div id="editorcontents">
	<? echo $html; ?>
	<script>
		html = '<? echo str_replace(chr(10),'',$html); ?>';
	</script>
	</div>

</body>
</html>
