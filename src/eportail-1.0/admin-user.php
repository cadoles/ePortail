<?
	include_once("include/include.php");				/* Général			*/
	include_once("include/admin.php");					/* Page Admin		*/
	include_once("include/delete.php");					/* Fonction Delete	*/
	include_once($config["templatedir"]."/html.php");	/* Entete HTML		*/
	include_once($config["templatedir"]."/frame.php");	/* Entete HTML		*/
	
	$db1=new ps_db;
	
	$tpmod				=$_POST['tpmod'];
	$vlmod				=$_POST['vlmod'];
	$vladd				=$_POST['vladd'];
	$vlsup				=$_POST['vlsup'];
	
	$user_id			=$_POST['user_id'];
	$user_login			=$_POST['user_login'];
	$user_password		=$_POST['user_password'];
	$user_passwordbis	=$_POST['user_passwordbis'];
	$user_lastname		=$_POST['user_lastname'];
	$user_firstname		=$_POST['user_firstname'];
	$user_pseudo		=$_POST['user_pseudo'];
	$user_email			=$_POST['user_email'];
	$user_profil_id		=$_POST['user_profil_id'];
	$user_mod			=$_POST['user_mod'];
	$user_avatar		=$_POST['user_avatar'];
	
	
	/*--> Controle de cohérance */
	if($vladd!=""||$vlmod!="") {
		$fgerr="";

		if($vladd!=""&&$user_password=="") {
			$jsaction="alert('Un mot de passe est obligatoire pour tout nouveau utilisateur');";
			$fgerr=1;
		}

		if($user_password!=$user_passwordbis) {
			$jsaction="alert('Votre mot de passe n\'est pas confirmé');";
			$fgerr=1;
		}

		if($user_login==""||$user_lastname==""||$user_firstname==""||$user_email==""||$user_profil_id=="") {
			$jsaction="alert('Vous devez renseigner l\'ensemble des informations');";
			$fgerr=1;
		}
		
		$q="SELECT * FROM env_user WHERE (user_login='$user_login' OR user_email='$user_email' OR ('$user_pseudo'!='' AND user_pseudo='$user_pseudo'))";
		if($vlmod!="") $q=$q." AND user_id!=$user_id";
		$db1->query($q);
		if($db1->next_record()){			
			$jsaction="alert('Un utilisateur avec ce login ou cet email ou ce pseudo existe déjà');";
			$fgerr=1;
		}
	}
	
	/*--> Rester sur le mode encours */
	if($vlmod!=""&&fgerr!="") $tpmod="MODIFY";
	if($vladd!=""&&fgerr!="") $tpmod="SUBMIT";
		
	/*--> Submit */
	if($vladd!=""&&$fgerr=="") {
		$q="INSERT INTO env_user(user_login, user_password, user_lastname, user_firstname, user_pseudo, user_email, user_avatar, user_profil_id, user_mod) VALUES('$user_login',md5('$user_password'),'$user_lastname','$user_firstname','$user_pseudo','$user_email','$user_avatar',$user_profil_id,'$user_mod')";
		$db1->query($q);
		$tpmod="";
	}

	/*--> Modify */
	if($vlmod!=""&&$fgerr=="") {
		$q="UPDATE env_user SET user_login='$user_login', user_lastname='$user_lastname', user_firstname='$user_firstname', user_pseudo='$user_pseudo', user_email='$user_email', user_avatar='$user_avatar', user_profil_id=$user_profil_id, user_mod='$user_mod' WHERE user_id=$user_id";
		$db1->query($q);
		
		// Changement de mot de passe
		if($user_password!="") {
			$q="UPDATE env_user SET user_password='".md5($user_password)."' WHERE user_id=$user_id";
			$db1->query($q);
		}
		$tpmod="";
	}
	
	/*--> Delete */
	if($vlsup!="") {
		delUser($user_id);
		$tpmod="";
	}

	echo "<script>$jsaction</script>";
	echo "<script type='text/javascript' src='".$config['libtable']."'></script>";
	
?>	

<script>
	$(document).ready(function() {
		$('#datatable').dataTable( {
			"oLanguage": { "sUrl": "<? echo $config['javascriptdir']; ?>/dataTables.txt" }
		} );
	} );	
</script>

<?
	echo "</head>";
	echo "<body>";

	echo "<form name='Formulaire' enctype='multipart/form-data' action='' method='post'>";
	echo "<input id='user_id' name='user_id' type='hidden' value='".$user_id."'>";
	echo "<input id='tpmod' name='tpmod' type='hidden' value=''>";
	
  
//-- DISPLAY -------------------------------------------------------------------------------------------------------------------------------

if($tpmod=="") { 
    echo "<h1>GESTION DES UTILISATEURS</h1>";
    
	echo "<input id='fgadd' name='fgadd' class='input-rounded-button' onClick='document.Formulaire.user_id.value=\"\"; document.Formulaire.tpmod.value=\"SUBMIT\"' type='submit' value='Ajouter' />";
	echo "<br>";

	echo "<table class id='datatable'>";
    echo "<thead>";
	echo "<th width='70px'>Action</th>";
	echo "<th>ID</th>";
	echo "<th width='90px'>Avatar</th>";
	echo "<th>Nom</th>";
	echo "<th>Login</th>";
	echo "<th>Pseudo</th>";
	echo "<th>Profil</th>";
	echo "</thead>";
	
	$q="SELECT * FROM env_user, env_profil WHERE env_profil.profil_id=env_user.user_profil_id";
	$db1->query($q);
	while($db1->next_record()){	
		echo "<tr>";

		echo "<td>";
		echo "<div id='btnupd'>";
		echo "<a onClick='document.Formulaire.user_id.value=".$db1->f('user_id')."; document.Formulaire.tpmod.value=\"MODIFY\";document.Formulaire.submit();'>";
		echo "<img src='images/blank.gif' width='16px' height='16px'>";
		echo "</a>";
		echo "</div>";

		echo "<div id='btndel'>";
		echo "<a onClick='document.Formulaire.user_id.value=".$db1->f('user_id')."; document.Formulaire.tpmod.value=\"DELETE\";document.Formulaire.submit();'>";
		echo "<img src='images/blank.gif' width='16px' height='16px'>";
		echo "</a>";
		echo "</div>";
		echo "</td>";

		echo "<td>";
		echo $db1->f('user_id');
		echo "</td>";
		
		echo "<td>";
		echo "<img src='".$config["avatardir"]."/".$db1->f('user_avatar')."' width='90px' height='90px'></img>";
		echo "</td>";
		
		echo "<td>";
		echo $db1->f('user_firstname')." ".$db1->f('user_lastname')."<br>";
		echo "</td>";

		echo "<td>";
		echo $db1->f('user_login')."<br>";
		echo "</td>";

		echo "<td>";
		echo $db1->f('user_pseudo')."<br>";
		echo "</td>";

		echo "<td>";
		echo $db1->f('profil_label')."<br>";
		echo "</td>";

		echo "</tr>";
	} 
	echo "</table>";
}

//-- SUBMIT --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="SUBMIT") {
	// Entete du formulaire
    echo "<h1>AJOUT UTILISATEUR</h1>";
	echo "<input id='vladd' name='vladd' class='input-rounded-button' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='input-rounded-button' type='submit' value='Annuler' />";
	echo "</br></br>";

	// Valeur par défaut
	if($user_mod=="") {$user_mod="big";}
	if($user_profil_id=="") {$user_profil_id="3";}
	if($user_avatar=="") {$user_avatar="anonyme.jpg";}
}

//-- MODIFY --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="MODIFY") {
	// Entete du formulaire
    echo "<h1>MODIFICATION UTILISATEUR</h1>";
	echo "<input id='vlmod' name='vlmod' class='input-rounded-button' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='input-rounded-button' type='submit' value='Annuler' />";
	echo "</br></br>";

	// Valeur par défaut
	$q = "SELECT * FROM env_user WHERE user_id=$user_id";
	$db1->query($q);
	if($db1->next_record()) {
		$user_login 		= $db1->f("user_login");
		$user_lastname 		= $db1->f("user_lastname");
		$user_firstname 	= $db1->f("user_firstname");
		$user_pseudo	 	= $db1->f("user_pseudo");
		$user_email 		= $db1->f("user_email");
		$user_profil_id		= $db1->f("user_profil_id");
		$user_mod			= $db1->f("user_mod");
		$user_avatar		= $db1->f("user_avatar");
	}
}

//-- DELETE --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="DELETE") {
	echo "<h1>SUPPRESSION UTILISATEUR</h1>";
	echo "<input id='vlsup' name='vlsup' class='input-rounded-button' type='submit' value='Confirmer la suppression' />";
	echo "&nbsp;";
	echo "<input class='input-rounded-button' type='submit' value='Annuler' />";    

	// Valeur par défaut
	$q="SELECT * FROM env_user, env_profil WHERE user_id=$user_id AND env_profil.profil_id=env_user.user_profil_id";
	$db1->query($q);
	if($db1->next_record()) {
		echo "<br><div id='avatar' class='avatar_admin'>";

		echo "<img src='".$config["avatardir"]."/".$db1->f('user_avatar')."' width='90px' height='90px'></img>";
		echo "<div>";
		echo $db1->f('user_firstname')." ".$db1->f('user_lastname')."<br>";
		echo "<span id='avatar_description'>";
		echo "Login : ".$db1->f('user_login')."<br>";
		echo "Pseudo : ".$db1->f('user_pseudo')."<br>";
		echo "Profil : ".$db1->f('profil_label')."<br>";			
		echo "</span>";
		echo "</div>";

		echo "</div>";
	}
	
	
}


//-- FORMULAIRE -----------------------------------------------------------------------------------------------------------------------------

if($tpmod=="SUBMIT"||$tpmod=="MODIFY") {
?>

 	<h2>Coordonnée</h2>

	<fieldset id="encadrer">
		<?
			$lbsty00="style='width:25%; border-bottom: dotted 1px;'";
			$lbsty01="type='text' size='53' style='width: 74%;'";
			$lbsty02="type='password' size='53' style='width: 74%;'";
			$lbsty03="style='width: 74%;'";
		?>

		<span id="encadrer_label" <? echo $lbsty00; ?>>ID Utilisateur*</span>
		<input 	value="<? echo $user_id; ?>"
				id="user_id_bis"
				name="user_id_bis"	
				readonly
				<? echo $lbsty01; ?>>

		<p>
		<span id="encadrer_label" <? echo $lbsty00; ?>>Login*</span>
		<input	value="<? echo $user_login; ?>"
				id="user_login" 
				name="user_login"
				<? echo $lbsty01; ?>>
		</p>

		<p>
		<span id="encadrer_label" <? echo $lbsty00; ?>>Password*</span>
		<input 	value="<? echo $user_password; ?>"	
				id="user_password"
				name="user_password"
				<? echo $lbsty02; ?>>
		</p>

		<p>
		<span id="encadrer_label" <? echo $lbsty00; ?>>Configmer Password*</span>
		<input 	value="<? echo $user_passwordbis; ?>"	
				id="user_passwordbis"
				name="user_passwordbis"
				<? echo $lbsty02; ?>>
		</p>

		<p>
		<span id="encadrer_label" <? echo $lbsty00; ?>>Nom*</span>
		<input 	value="<? echo $user_lastname; ?>"
				id="user_lastname"
				name="user_lastname"
				<? echo $lbsty01; ?>>
		</p>

		<p>
		<span id="encadrer_label" <? echo $lbsty00; ?>>Prénom*</span>
		<input	value="<? echo $user_firstname; ?>"
				id="user_firstname"
				name="user_firstname"
				<? echo $lbsty01; ?>>
		</p>

		<p>
		<span id="encadrer_label" <? echo $lbsty00; ?>>Pseudo</span>
		<input	value="<? echo $user_pseudo; ?>"
				id="user_pseudo"
				name="user_pseudo"
				<? echo $lbsty01; ?>>
		</p>
				
		<p>
		<span id="encadrer_label" <? echo $lbsty00; ?>>Email*</span>
		<input	value="<? echo $user_email; ?>"
				id="user_email"
				name="user_email"
				<? echo $lbsty01; ?>>
		</p>		
	</fieldset>

 	<h2>Profil</h2>

	<fieldset id="encadrer">
		<p>
		<span id="encadrer_label" <? echo $lbsty00; ?>>Profil</span>
		<select	id="user_profil_id"
				name="user_profil_id"
				size="1"
				<? echo $lbsty03; ?>>>
			
			<?
				$q="SELECT * FROM env_profil";
				$db1->query($q);
				while($db1->next_record()){	
					$lbsel="";
					if($user_profil_id==$db1->f("profil_id")) {
						$lbsel="selected";
					}
					echo "<option value='".$db1->f("profil_id")."' ".$lbsel.">".$db1->f("profil_label")."</option>";
				}
			?>
		</select>
		</p>
		
		<p>
		<span id="encadrer_label" <? echo $lbsty00; ?>>Mode d'Affichage</span>
		<select	id="user_mod"
				name="user_mod"
				size="1"
				<? echo $lbsty03; ?>>>		
			<option value=""	<? if($user_mod=='')	{ echo 'selected'; }?>>sans bannière</option>
			<option value="big"	<? if($user_mod=='big')	{ echo 'selected'; }?>>avec bannière</option>
		</select>
		</p>
	</fieldset>
	
	<h2>Avatar</h2>

	<fieldset id="encadrer">
		<?
			echo "<div id='avatar' class='avatar_admin'>";
			echo "<img id='img_avatar' name='img_avatar' src='".$config["avatardir"]."/".$user_avatar."' width='90px' height='90px'></img>";
			echo "<div>";
		
			echo "<div id='btnupd'>";
			echo "<a href='#?o=1&w=700&h=530&u=jcrop.php?dirimg=".$config["avatardir"]."&lbico=user_avatar&igico=img_avatar' rel='popup_name' class='poplight' title='Ajouter un avatar'>";
			echo "<img src='images/blank.gif' width='16px' height='16px'>";
			echo "</a>";
			echo "</div>";

			echo "<div id='btndel'>";
			echo "<a onClick='document.getElementById(\"img_avatar\").src=\"".$config["avatardir"]."/anonyme.jpg\"; document.Formulaire.user_avatar.value=\"anonyme.jpg\";'>";
			echo "<img src='images/blank.gif' width='16px' height='16px'>";
			echo "</a>";
			echo "</div>";	
			
			echo "<input id='user_avatar' name='user_avatar' type='hidden' value='".$user_avatar."'>";
		?>
	</fieldset>
	
	<script type="text/javascript">
			$('#user_login').focus();
	</script>
<?
}



//-- FOOTER ---------------------------------------------------------------------------------------------------------------------------------

echo "</form>";
echo "</body>";
echo "</html>";
?>







	
	
