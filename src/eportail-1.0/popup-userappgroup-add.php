<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?
	include_once("include/include.php");				/* Général		*/
	include_once($config["templatedir"]."/html.php");	/* Entete HTML	*/
	include_once($config["templatedir"]."/popup.php");
	$db1=new ps_db;
	
	$idpan=$_GET['idpan'];
	
	if($idpan=="") {
		$idpan=$_POST['idpan'];
	}

	$lbpan=$_POST['lbpan'];	
	$fgclo=$_POST['fgclo'];
	
	if($fgclo!="") {
		// Rechercher le dernier order de la location C1R1 de cette page
		$order=1;
		$q="SELECT page_application_order FROM env_page_application WHERE page_application_panel_id=$idpan AND page_application_loc='C1R1' ORDER BY page_application_order DESC LIMIT 1";
		$db1->query($q);
		if($db1->next_record()) {
			$order=$db1->f('page_application_order')+1;
		}

		// Création du widget
		$q="INSERT INTO env_page_application (page_application_loc,page_application_order,page_application_type_id,page_application_panel_id,page_application_label) VALUES('C1R1',$order,1,$idpan,'$lbpan')";
		$db1->query($q);		

		// Action javascript Ajout du panel directement + fermeture de la popup
		$jsaction="recharge(".$idpan.")";
	}
	
?>

<script type="text/JavaScript">
	function recharge(idpan) {
		parent.document.location.reload();
	}
	<?php echo $jsaction ?>
</script>

</head>

<body>

<h1>Ajout d'un groupe d'application</h1>

<div id="envolepage">
<form name="Formulaire" method="post">


<h2>Donnez un nom à votre groupe</h2>
<input id="lbpan" name="lbpan" class="biginput" type="text" size="150" style="width: 99%" value="Groupe Application">

<br>
<br>
<center>
<input id="fgclo" name="fgclo" class="input-rounded-button" type="submit" style="width: 99%" value="Ajouter" />
<input type="hidden" name="idpan" id="idpan" value="<? echo $idpan; ?>" /> 
</center>
	
</form>
</div>

</body>
</html>
