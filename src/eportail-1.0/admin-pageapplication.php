<?
	include_once("include/include.php");				/* Général			*/
	include_once("include/admin.php");					/* Page Admin		*/
	include_once("include/delete.php");					/* Fonction Delete	*/
	include_once($config["templatedir"]."/html.php");	/* Entete HTML		*/
	include_once($config["templatedir"]."/frame.php");	/* Entete HTML		*/
	
	$db1=new ps_db;
	
	$tpmod				=$_POST['tpmod'];
	$vlmod				=$_POST['vlmod'];
	$vladd				=$_POST['vladd'];
	$vlsup				=$_POST['vlsup'];
	
	$id					=$_POST['id'];
	$label				=$_POST['label'];
	$idproprio			=$_POST['idproprio'];
	$idtemplate			=$_POST['idtemplate'];
	
	/*--> Controle de cohérance */
	if($vladd!=""||$vlmod!="") {
		$fgerr="";
		if($label==""||$idproprio==""||$idtemplate=="") {
			$jsaction="alert('Vous devez renseigner l\'ensemble des informations');";
			$fgerr=1;
		}
	}
	
	/*--> Rester sur le mode encours */
	if($vlmod!=""&&fgerr!="") $tpmod="MODIFY";
	if($vladd!=""&&fgerr!="") $tpmod="SUBMIT";
		
	/*--> Submit */
	if($vladd!=""&&$fgerr=="") {
		$q="INSERT INTO env_panel(panel_label, panel_user_id, panel_url, panel_type_id, panel_page_template_id, panel_page_css_name) VALUES('$label',$idproprio,'application.php',2,$idtemplate,'application0$idtemplate.css')";
		$db1->query($q);
		$tpmod="";
	}

	/*--> Modify */
	if($vlmod!=""&&$fgerr=="") {
		$q="UPDATE env_panel SET panel_label='$label', panel_user_id=$idproprio, panel_page_template_id=$idtemplate, panel_page_css_name='application0$idtemplate.css' WHERE panel_id=$id";
		$db1->query($q);
		$tpmod="";
	}
	
	/*--> Delete */
	if($vlsup!="") {
		delPanel($id);
	}
	

	echo "<script>$jsaction</script>";
	echo "<script type='text/javascript' src='".$config['libtable']."'></script>";
	
?>	

<script>
	$(document).ready(function() {
		$('#example').dataTable( {
			"oLanguage": { "sUrl": "<? echo $config['javascriptdir']; ?>/dataTables.txt" }
		} );
	} );	
</script>

<?	
	echo "</head>";
	echo "<body>";

	echo "<form name='Formulaire' enctype='multipart/form-data' action='' method='post'>";
	echo "<input id='id' name='id' type='hidden' value='".$id."'>";
	echo "<input id='tpmod' name='tpmod' type='hidden' value=''>";
	
  
//-- DISPLAY -------------------------------------------------------------------------------------------------------------------------------

if($tpmod=="") { 
    echo "<h1>GESTION DES PAGES APPLICATIVES</h1>";
    
	echo "<input id='fgadd' name='fgadd' class='input-rounded-button' onClick='document.Formulaire.id.value=\"\"; document.Formulaire.tpmod.value=\"SUBMIT\"' type='submit' value='Ajouter' />";
	echo "<br>";
 
	$q="SELECT * FROM env_panel,env_user, env_profil WHERE panel_type_id=2 AND user_id=panel_user_id AND env_profil.profil_id=env_user.user_profil_id ORDER BY panel_id DESC";
	$db1->query($q);

	echo "<table class id='example'>";
    echo "<thead>";
	echo "<th width='70px'>Action</th>";
	echo "<th>ID</th>";
	echo "<th width='300px'>Label</th>";
	echo "<th>Propriètaire</th>";
	echo "</thead>";
	
	while($db1->next_record()){	
		echo "<tr>";

		echo "<td>";
		echo "<div id='btnupd'>";
		echo "<a onClick='document.Formulaire.id.value=".$db1->f('panel_id')."; document.Formulaire.tpmod.value=\"MODIFY\";document.Formulaire.submit();'>";
		echo "<img src='images/blank.gif' width='16px' height='16px'>";
		echo "</a>";
		echo "</div>";

		echo "<div id='btnopt'>";
		echo "<a href='application.php?idpan=".$db1->f('panel_id')."' target='_black'>";
		echo "<img src='images/blank.gif' width='16px' height='16px'>";
		echo "</a>";
		echo "</div>";
		
		echo "<div id='btndel'>";
		echo "<a onClick='document.Formulaire.id.value=".$db1->f('panel_id')."; document.Formulaire.tpmod.value=\"DELETE\";document.Formulaire.submit();'>";
		echo "<img src='images/blank.gif' width='16px' height='16px'>";
		echo "</a>";
		echo "</div>";
		echo "</td>";

		echo "<td>";
		echo $db1->f('panel_id');
		echo "</td>";

		echo "<td>";
		echo $db1->f('panel_label');
		echo "</td>";

		echo "<td>";
			echo "<div id='avatar' class='avatar_admin'>";

			echo "<img src='".$config["avatardir"]."/".$db1->f('user_avatar')."' width='90px' height='90px'></img>";
			echo "<div>";
			echo  $db1->f('user_firstname')." ".$db1->f('user_lastname')."<br>";
			echo "<span id='avatar_description'>";
			echo "Login : ".$db1->f('user_login')."<br>";
			echo "Pseudo : ".$db1->f('user_pseudo')."<br>";
			echo "Profil : ".$db1->f('profil_label')."<br>";
			echo "</span>";
			echo "</div>";

			echo "</div>";
		echo "</td>";

		echo "</tr>";
	} 

	echo "</table>";
}

//-- SUBMIT --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="SUBMIT") {
	// Entete du formulaire
    echo "<h1>AJOUT PAGE APPLICATIVE</h1>";
	echo "<input id='vladd' name='vladd' class='input-rounded-button' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='input-rounded-button' type='submit' value='Annuler' />";
	echo "</br></br>";

	// Valeur par défaut
	// aucune
	$idproprio=$_SESSION['user_id'];
	$idtemplate=1;
	
	$q="SELECT * FROM env_user, env_profil  WHERE user_id=$idproprio AND env_profil.profil_id=env_user.user_profil_id";
	$db1->query($q);
	if($db1->next_record()){		
		$lbavatar	= $db1->f('user_avatar');
		$lbproprio	= $db1->f('user_firstname')." ".$db1->f('user_lastname');
		$lbpseudo	= $db1->f('user_pseudo');
		$lblogin	= $db1->f('user_login');
		$lbprofil	= $db1->f('profil_label');
	}
}

//-- MODIFY --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="MODIFY") {
	// Entete du formulaire
    echo "<h1>MODIFICATION PAGE APPLICATIVE</h1>";
	echo "<input id='vlmod' name='vlmod' class='input-rounded-button' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='input-rounded-button' type='submit' value='Annuler' />";
	echo "</br></br>";

	// Valeur par défaut
	$q = "SELECT * FROM env_panel, env_user, env_profil WHERE panel_id=$id AND panel_user_id=user_id AND env_profil.profil_id=env_user.user_profil_id";
	$db1->query($q);
	if($db1->next_record()) {
		$idproprio	= $db1->f('panel_user_id');
		$label 		= $db1->f('panel_label');
		$lbavatar	= $db1->f('user_avatar');
		$lbproprio	= $db1->f('user_firstname')." ".$db1->f('user_lastname');
		$lbpseudo	= $db1->f('user_pseudo');
		$lblogin	= $db1->f('user_login');
		$lbprofil	= $db1->f('profil_label');
		$idtemplate	= $db1->f('panel_page_template_id');
	}
}

//-- DELETE --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="DELETE") {
	echo "<h1>SUPPRESSION PAGE APPLICATIVE</h1>";
	echo "<input id='vlsup' name='vlsup' class='input-rounded-button' type='submit' value='Confirmer la suppression' />";
	echo "&nbsp;";
	echo "<input class='input-rounded-button' type='submit' value='Annuler' />";    
	
	$q = "SELECT * FROM env_panel, env_user, env_profil WHERE panel_id=$id AND panel_user_id=user_id AND env_profil.profil_id=env_user.user_profil_id";
	$db1->query($q);
	if($db1->next_record()) {	
		echo "<h2>Description</h2>";
		echo "<fieldset id='encadrer'>";
		$lbsty00="style='width:25%; border-bottom: dotted 1px;'";
		$lbsty01="type='text' size='53' style='width: 74%;'";
		$lbsty02="type='password' size='53' style='width: 74%;'";
		$lbsty03="style='width: 74%;'";

		echo "<span id='encadrer_label' $lbsty00>ID Application*</span>";
		echo "<input value='$id' readonly $lbsty01>";
		echo "<p>";
		echo "<span id='encadrer_label' $lbsty00>Nom*</span>";
		echo "<input value='".$db1->f('panel_label')."' readonly $lbsty01>";
		echo "</p>";
		echo "</fieldset>";
		
		echo "<h2>Propriétaire</h2>";
		echo "<fieldset id='encadrer'>";
		echo "<div id='avatar' class='avatar_admin'>";

		echo "<img src='".$config["avatardir"]."/".$db1->f('user_avatar')."' width='90px' height='90px'></img>";
		echo "<div>";
		echo $db1->f('user_firstname')." ".$db1->f('user_lastname')."<br>";
		echo "<span id='avatar_description'>";
		echo "Login : ".$db1->f('user_login')."<br>";
		echo "Pseudo : ".$db1->f('user_pseudo')."<br>";
		echo "Profil : ".$db1->f('profil_label')."<br>";
		echo "</span>";
		echo "</div>";

		echo "</div>";
		echo "</fieldset>";
	}
}


//-- FORMULAIRE -----------------------------------------------------------------------------------------------------------------------------

if($tpmod=="SUBMIT"||$tpmod=="MODIFY") {
?>

 	<h2>Description</h2>

	<fieldset id="encadrer">
		<?
			$lbsty00="style='width:25%; border-bottom: dotted 1px;'";
			$lbsty01="type='text' size='53' style='width: 74%;'";
			$lbsty02="type='password' size='53' style='width: 74%;'";
			$lbsty03="style='width: 74%;'";
		?>

		<span id="encadrer_label" <? echo $lbsty00; ?>>ID Application*</span>
		<input 	value="<? echo $id; ?>"
				id="id_bis"
				name="id_bis"	
				readonly
				<? echo $lbsty01; ?>>

		<p>
		<span id="encadrer_label" <? echo $lbsty00; ?>>Nom*</span>
		<input	value="<? echo $label; ?>"
				id="label" 
				name="label"
				<? echo $lbsty01; ?>>
		</p>

	</fieldset>
	
 	<h2>Propriétaire</h2>
	<fieldset id="encadrer">
		<?
			echo "<div id='avatar' class='avatar_admin'>";

			echo "<img src='".$config["avatardir"]."/".$lbavatar."' width='90px' height='90px'></img>";
			echo "<div>";
			echo $lbproprio."<br>";
			echo "<span id='avatar_description'>";
			echo "Login : ".$lblogin."<br>";
			echo "Pseudo : ".$lbpseudo."<br>";
			echo "Profil : ".$lbprofil."<br>";
			echo "</span>";
			echo "</div>";

			echo "</div>";
		?>

		<input 	value="<? echo $idproprio; ?>"
				id="idproprio"
				name="idproprio"	
				type="hidden"
				readonly
				<? echo $lbsty01; ?>>		
	</fieldset>
	
 	<h2>Modèle</h2>
	<fieldset id="encadrer">
		<div style="float: left; width:20px; clear: both; height: 120px;"><input type="radio" id="idtemplate" name="idtemplate" value="1" <? if($idtemplate==1) echo "checked"; ?>></div> <div style="float: left;"><img src="images/app-style-01.png"></div><div style="float: left; padding-left:10px;">Seule les icones sont visibles<br>Quand l'utilisateur clique sur l'icone, la frame en cours est remplacée par celle de l'application.</div>
		<div style="float: left; width:20px; clear: both; height: 120px;"><input type="radio" id="idtemplate" name="idtemplate" value="2" <? if($idtemplate==2) echo "checked"; ?>></div> <div style="float: left;"><img src="images/app-style-02.png"></div><div style="float: left; padding-left:10px;">Les icones sont visibles et sont accompagnés de la description<br>Quand l'utilisateur clique sur l'icone, l'application en question est ouverte à côté de la liste des applications</div>
		<div style="float: left; width:20px; clear: both; height: 120px;"><input type="radio" id="idtemplate" name="idtemplate" value="3" <? if($idtemplate==3) echo "checked"; ?>></div> <div style="float: left;"><img src="images/app-style-03.png"></div><div style="float: left; padding-left:10px;">Seule le titre de l'application est affichée, sous la forme d'une menu vertical<br>Quand l'utilisateur clique sur l'application, elle s'ouvre à côté du menu</div>
	</fieldset>

	<script type="text/javascript">
			$('#label').focus();
	</script>
<?
}



//-- FOOTER ---------------------------------------------------------------------------------------------------------------------------------

echo "</form>";
echo "</body>";
echo "</html>";
?>







	
	
