<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?
	include_once("include/include.php");				/* Général			*/
	include_once($config["templatedir"]."/html.php");	/* Entete HTML		*/
	$db1=new ps_db;
	$db2=new ps_db;
	
	$idpan=$_GET['idpan'];
	
	include_once($config["templatedir"]."/widget.php");	/* Entete Widget	*/	
	
	
	// Récupérer le template de la page widget et le propriétaire de la page
	$q="SELECT panel_page_template_id, panel_user_id FROM env_panel WHERE panel_id=".$idpan;
	$db1->query($q);
	if($db1->next_record()) {
		$idtpl=$db1->f('panel_page_template_id');
		$idpro=$db1->f('panel_user_id');
	}
?>


<script type="text/javascript">  
     function sizeFrame() {
          jQuery("#iframe_13", top.document).css({ height: 0 });
          var heightDiv = jQuery("#iframe_13", top.document).contents().find('body').attr('scrollHeight');
          alert(heightDiv);
          jQuery("#iframe_13", top.document).css({ height: heightDiv });
     }
   
   
	<? if($idpro==$_SESSION['user_id']||$_SESSION['user_profil']==1) { ?>
	$(document).ready(function () {  
		$(".column").sortable({  
			connectWith: ".column",  
			handle: '.WidgetHeader',  
			opacity: 0.6,
			update: function ()
			{
				var a = $(this);

				$('.column').each(function(index) {
					var order = $(this).sortable('serialize');
					$("#info").load("process-userwidget-order.php?"+order+"&idloc="+$(this).attr('id')+"&idpan=<? echo $idpan; ?>");
				});
			} 
		});  

		$(".column").disableSelection();					
	});  
	
	<? } ?>
	
	function delwidget(idwid) {
		if(confirm("Confirmez-vous la suppression de ce widget ?")) {
			$("#info").load("process-userwidget-del.php?id="+idwid);
			setTimeout(function () {location.reload()},1000);			
		}
	}	
</script>


</head>

<?
	// Si l'utilisateur en cours est le propriétaire : il peut modifier la page
	if($idpro==$_SESSION['user_id']||$_SESSION['user_profil']==1) {
		echo "<div id='widgetadmin'><a href='#?o=1&w=700&h=400&u=popup-userwidget-add.php?idpan=".$idpan."' rel='popup_name' class='poplight' title='Ajouter un widget'>Ajouter un Widget</a></div>";
	}	
?>

<div id="info"></div>


<div id="widgetpage">
<?
	// Récupérer le code html du template
	$q="SELECT * FROM env_page_template WHERE page_template_id=".$idtpl;
	$db1->query($q);
	if($db1->next_record()) { echo $db1->f('page_template_html'); }
		
	// Parcourrir l'ensemble des widget de la page pour les créer dynamiquement
	$q="SELECT * FROM env_page_widget, env_widget_type WHERE page_widget_panel_id=".$idpan." AND page_widget_type_id=widget_type_id ORDER BY page_widget_loc, page_widget_order DESC";
	$db1->query($q);
	while($db1->next_record()) {

		// Class du widget en fonction de l'affichage ou non d'une bordure
		if($db1->f('page_widget_fgborder'))
			$widclass="WidgetBody";
		else
			$widclass="WidgetBodyLight";
		
		// Récupérer le type de contenu du widget
		$tpcon="";
		$q="SELECT * FROM env_widget_type WHERE widget_type_id=".$db1->f("page_widget_type_id");
		$db2->query($q);
		if($db2->next_record()) {
			$tpcon=$db2->f('widget_type_content_id');
			$lbcod=$db2->f('widget_type_code');
		}
			
		// Définition de la source de l'url : soit celle de l'utilisateur soit celle du widget si type de widget systeme l'url est forcement déterminé par l'utilisateur
		if($db1->f('widget_type_id') < 0)
			$url=$db1->f('page_widget_url');
		else
			$url=$db1->f('widget_type_url');
		

		// Définition de l'url en fonction du type de contenu du widget si type rss on force la page rss.php en http
		if($tpcon==2) 
			$page_widget_url=str_replace("https://","http://",$config['urlbase'])."/rss.php?idwid=".$db1->f('page_widget_id');
		elseif($tpcon==5) 
			$page_widget_url="code.php?idwid=".$db1->f('page_widget_id');
		else
			$page_widget_url=$url;
		
			
		// Div conteneur global
		$iframe="<div id='widget_".$db1->f('page_widget_id')."' class='Widget'>";
		
		// Div titre
		$iframe=$iframe."<div class='WidgetHeader'>".$db1->f('page_widget_label');

		// Si propriétaire de la page ou administrateur bouton modifier / supprimer
		if($idpro==$_SESSION['user_id']||$_SESSION['user_profil']==1) {							
			$iframe=$iframe."
				<div id='btnupd' style='float:right;'>
				<a href='#?o=1&w=700&h=530&u=popup-userwidget-mod.php?idwid=".$db1->f('page_widget_id')."' rel='popup_name' class='poplight' title='Modifier Groupe Application'>
				<img src='images/blank.gif' width='16px' height='16px'>
				</a>
				</div>

				<div id='btndel' style='float:right;'>
				<a onClick='delwidget(".$db1->f('page_widget_id').");'>
				<img src='images/blank.gif' width='16px' height='16px'>
				</a>
				</div>";				
		}	 

		// Fermeture de la Div titre
		$iframe=$iframe."</div>";
		
		// Div corps
		$iframe=$iframe."<div class='$widclass'>";
		
		// la frame
		$iframe=$iframe."
			<iframe
				id='frame_".$db1->f('page_widget_id')."'
				src='$page_widget_url'
				width=100%
				height='".$db1->f('page_widget_height')."'		
				frameborder=0
				scrolling=auto></iframe>";
		
		// Fermeture de la Div corps
		echo "</div>";
			
			
		// Fermeture de la Div global
		$iframe=$iframe."</div>";
		
		// Cas hauteur nulle
		//if($db1->f('page_widget_height')=="") $iframe=str_replace("height=''","",$iframe);
		
		// Suppression des retour chariot pour bonne interprétation en javascript
		$iframe=str_replace("\n","",$iframe);
	?>
		<script type="text/javascript">  
			// Recherche de la colonne où le widget est localisé
			loc = $('#<? echo $db1->f('page_widget_loc'); ?>');
			
			
			// Ajout du widget
			charloc="<? echo addslashes($iframe); ?>";
			
			loc.prepend(charloc);
			//loc.prepend('<div id="widget_<? echo $db1->f('page_widget_id'); ?>" class="Widget"><div class="WidgetHeader"><? echo $db1->f('page_widget_label'); ?></div><div class="WidgetBody"><iframe src="'+iframewidget+'" width=100% height=175px frameborder=0></iframe></div></div>');
		</script>
		
	<?
	}
	
	
?>


</div>
</body>
