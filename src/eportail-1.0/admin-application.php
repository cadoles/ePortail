<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?
	include_once("include/include.php");				/* Général			*/
	include_once("include/admin.php");					/* Page Admin		*/
	include_once("include/delete.php");					/* Fonction Delete	*/
	include_once($config["templatedir"]."/html.php");	/* Entete HTML		*/
	include_once($config["templatedir"]."/frame.php");	/* Entete HTML		*/
	
	$db1=new ps_db;
	
	$tpmod				=$_POST['tpmod'];
	$vlmod				=$_POST['vlmod'];
	$vladd				=$_POST['vladd'];
	$vlsup				=$_POST['vlsup'];
	
	$id					=$_POST['id'];
	$name				=$_POST['name'];
	$label				=$_POST['label'];
	$url				=$_POST['url'];
	$icon				=$_POST['icon'];
	
	/*--> Controle de cohérance */
	if($vladd!=""||$vlmod!="") {
		$fgerr="";

		if($name==""||$label==""||$url==""||$icon=="") {
			$jsaction="alert('Vous devez renseigner l\'ensemble des informations');";
			$fgerr=1;
		}
		
		$q="SELECT * FROM env_application WHERE (application_name='$name')";
		if($vlmod!="") $q=$q." AND application_id!=$id";
		$db1->query($q);
		if($db1->next_record()){			
			$jsaction="alert('Une application avec ce nom existe déjà');";
			$fgerr=1;
		}
	}
	
	/*--> Rester sur le mode encours */
	if($vlmod!=""&&fgerr!="") $tpmod="MODIFY";
	if($vladd!=""&&fgerr!="") $tpmod="SUBMIT";
		
	/*--> Submit */
	if($vladd!=""&&$fgerr=="") {
		$q="INSERT INTO env_application(application_name, application_label, application_url, application_icon) VALUES('$name','$label','$url','$icon')";
		$db1->query($q);
		$tpmod="";
	}

	/*--> Modify */
	if($vlmod!=""&&$fgerr=="") {
		$q="UPDATE env_application SET application_name='$name', application_label='$label', application_url='$url', application_icon='$icon' WHERE application_id=$id";
		$db1->query($q);
		$tpmod="";
	}
	
	/*--> Delete */
	if($vlsup!="") {
		delApplication($id);
	}
	

	echo "<script>$jsaction</script>";
	echo "</head>";
	echo "<body>";

	echo "<form name='Formulaire' enctype='multipart/form-data' action='' method='post'>";
	echo "<input id='id' name='id' type='hidden' value='".$id."'>";
	echo "<input id='tpmod' name='tpmod' type='hidden' value=''>";
	
  
//-- DISPLAY -------------------------------------------------------------------------------------------------------------------------------

if($tpmod=="") { 
    echo "<h1>GESTION DES APPLICATIONS</h1>";
    
	echo "<input id='fgadd' name='fgadd' class='input-rounded-button' onClick='document.Formulaire.id.value=\"\"; document.Formulaire.tpmod.value=\"SUBMIT\"' type='submit' value='Ajouter' />";
	echo "<br>";

	
	$q="SELECT * FROM env_application, env_appicon WHERE application_icon=appicon_id";
	$db1->query($q);
	while($db1->next_record()){	
		echo "<div id='avatar' class='avatar_admin'>";

		echo "<img src='".$config["appicondir"]."/".$db1->f('appicon_url')."' width='90px' height='90px'></img>";
		echo "<div>";
		echo $db1->f('application_label')."<br>";
		echo "<span id='avatar_description'>";
		echo "id : ".$db1->f('application_name')."<br>";
		echo "url : <a href='".$db1->f('application_url')."' target='_black'>".substr($db1->f('application_url'),0,25)."</a><br>";
		
		echo "<div id='btnupd'>";
		echo "<a onClick='document.Formulaire.id.value=".$db1->f('application_id')."; document.Formulaire.tpmod.value=\"MODIFY\";document.Formulaire.submit();'>";
		echo "<img src='images/blank.gif' width='16px' height='16px'>";
		echo "</a>";
		echo "</div>";

		echo "<div id='btndel'>";
		echo "<a onClick='document.Formulaire.id.value=".$db1->f('application_id')."; document.Formulaire.tpmod.value=\"DELETE\";document.Formulaire.submit();'>";
		echo "<img src='images/blank.gif' width='16px' height='16px'>";
		echo "</a>";
		echo "</div>";
				
		echo "</span>";
		echo "</div>";

		echo "</div>";
	} 
}

//-- SUBMIT --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="SUBMIT") {
	// Entete du formulaire
    echo "<h1>AJOUT APPLICATION</h1>";
	echo "<input id='vladd' name='vladd' class='input-rounded-button' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='input-rounded-button' type='submit' value='Annuler' />";
	echo "</br></br>";

	// Valeur par défaut
	// aucune
}

//-- MODIFY --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="MODIFY") {
	// Entete du formulaire
    echo "<h1>MODIFICATION APPLICATION</h1>";
	echo "<input id='vlmod' name='vlmod' class='input-rounded-button' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='input-rounded-button' type='submit' value='Annuler' />";
	echo "</br></br>";

	// Valeur par défaut
	$q = "SELECT * FROM env_application, env_appicon WHERE application_id=$id AND application_icon=appicon_id";
	$db1->query($q);
	if($db1->next_record()) {
		$name 		= $db1->f("application_name");
		$label 		= $db1->f("application_label");
		$url 		= $db1->f("application_url");
		$urlicon 	= $db1->f("appicon_url");
		$icon 		= $db1->f("application_icon");
	}
}

//-- DELETE --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="DELETE") {
	echo "<h1>SUPPRESSION APPLICATION</h1>";
	echo "<input id='vlsup' name='vlsup' class='input-rounded-button' type='submit' value='Confirmer la suppression' />";
	echo "&nbsp;";
	echo "<input class='input-rounded-button' type='submit' value='Annuler' />";    
	
	// Valeur par défaut
	$q = "SELECT * FROM env_application, env_appicon WHERE application_id=$id AND application_icon=appicon_id";
	$db1->query($q);
	if($db1->next_record()) {	
		echo "<br><div id='avatar' class='avatar_admin'>";

		echo "<img src='".$config["appicondir"]."/".$db1->f('appicon_url')."' width='90px' height='90px'></img>";
		echo "<div>";
		echo $db1->f('application_label')."<br>";
		echo "<span id='avatar_description'>";
		echo "id : ".$db1->f('application_name')."<br>";
		echo "url : <a href='".$db1->f('application_url')."' target='_black'>".$db1->f('application_url')."</a><br>";				
		echo "</span>";
		echo "</div>";

		echo "</div>";	
	}
}


//-- FORMULAIRE -----------------------------------------------------------------------------------------------------------------------------

if($tpmod=="SUBMIT"||$tpmod=="MODIFY") {
?>

 	<h2>Description</h2>

	<fieldset id="encadrer">
		<?
			$lbsty00="style='width:25%; border-bottom: dotted 1px;'";
			$lbsty01="type='text' size='53' style='width: 74%;'";
			$lbsty02="type='password' size='53' style='width: 74%;'";
			$lbsty03="style='width: 74%;'";
		?>

		<span id="encadrer_label" <? echo $lbsty00; ?>>ID Application*</span>
		<input 	value="<? echo $id; ?>"
				id="id_bis"
				name="id_bis"	
				readonly
				<? echo $lbsty01; ?>>
				

		<p>
		<span id="encadrer_label" <? echo $lbsty00; ?>>Nom*</span>
		<input	value="<? echo $name; ?>"
				id="name" 
				name="name"
				<? echo $lbsty01; ?>>
		</p>

		<p>
		<span id="encadrer_label" <? echo $lbsty00; ?>>Libellé*</span>
		<input 	value="<? echo $label; ?>"	
				id="label"
				name="label"
				<? echo $lbsty01; ?>>
		</p>

		<p>
		<span id="encadrer_label" <? echo $lbsty00; ?>>Url*</span>
		<input 	value="<? echo $url; ?>"	
				id="url"
				name="url"
				<? echo $lbsty01; ?>>
		</p>	
	</fieldset>

 	<h2>Icone</h2>

	<fieldset id="encadrer">
		<input 	value="<? echo $icon; ?>"
				id="icon"
				name="icon"	
				type="hidden"
				readonly
				<? echo $lbsty01; ?>>
		
		<div id='appicon' class='appicon'>
		<img id="imgapp" src="<? echo $config['appicondir']."/".$urlicon; ?>" width="90px" height="90px"></img><br>
		</div>
		<div>
		<span id='avatar_description'>
		<br>
		<a href='#?o=1&w=700&h=530&u=popup-adminappicon.php?igico=imgapp&idico=icon' rel='popup_name' class='poplight' title='Choisir un icone'><span class='link-rounded-button'>Choisir</span></a>
		<br>
		<a href='#?o=1&w=700&h=530&u=jcrop.php?dirimg=<? echo $config["appicondir"]; ?>&igico=imgapp&idico=icon&query=INSERT INTO env_appicon (appicon_url) VALUES($$)&queryid=SELECT appicon_id FROM env_appicon DES ORDER BY appicon_id DESC LIMIT 1&idbdd=appicon_id' rel='popup_name' class='poplight' title='Ajouter un icone'><span class='link-rounded-button'>Ajouter</span></a>
		</span>
		</div>
		
	
	</fieldset>
	
	<script type="text/javascript">
			$('#panel_label').focus();
	</script>
<?
}



//-- FOOTER ---------------------------------------------------------------------------------------------------------------------------------

echo "</form>";
echo "</body>";
echo "</html>";
?>







	
	
