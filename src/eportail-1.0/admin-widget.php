<?
	include_once("include/include.php");				/* Général			*/
	include_once("include/admin.php");					/* Page Admin		*/
	include_once("include/delete.php");					/* Fonction Delete	*/
	include_once($config["templatedir"]."/html.php");	/* Entete HTML		*/
	include_once($config["templatedir"]."/frame.php");	/* Entete HTML		*/
	
	$db1=new ps_db;
	
	$tpmod					=$_POST['tpmod'];
	$vlmod					=$_POST['vlmod'];
	$vladd					=$_POST['vladd'];
	$vlsup					=$_POST['vlsup'];
	
	$widget_type_id			=$_POST['widget_type_id'];
	$widget_type_name 		=$_POST['widget_type_name'];
	$widget_type_label 		=$_POST['widget_type_label'];
	$widget_type_url 		=$_POST['widget_type_url'];
	$widget_type_height		=$_POST['widget_type_height'];
	$widget_type_fgborder 	=$_POST['widget_type_fgborder'];
	$widget_type_content_id	=$_POST['widget_type_content_id'];
	$widget_type_icon		=$_POST['widget_type_icon'];
	$widget_type_code		=$_POST['widget_type_code'];
	
	
	/*--> Controle de cohérance */
	if($vladd!=""||$vlmod!="") {
		$fgerr="";

		if($widget_type_name==""||$widget_type_label==""||$widget_type_height==""||$widget_type_fgborder==""||$widget_type_content_id=="") {
			$jsaction="alert('Vous devez renseigner l\'ensemble des informations');";
			$fgerr=1;
		}
	}
	
	/*--> Rester sur le mode encours */
	if($vlmod!=""&&fgerr!="") $tpmod="MODIFY";
	if($vladd!=""&&fgerr!="") $tpmod="SUBMIT";
		
	/*--> Submit */
	if($vladd!=""&&$fgerr=="") {
		$q="INSERT INTO env_widget_type(widget_type_name, widget_type_label, widget_type_url, widget_type_code, widget_type_height, widget_type_fgborder, widget_type_content_id, widget_type_icon) VALUES('".addslashes($widget_type_name)."','".addslashes($widget_type_label)."','$widget_type_url','".addslashes($widget_type_code)."','".$widget_type_height."px','$widget_type_fgborder','$widget_type_content_id','$widget_type_icon')";
		$db1->query($q);
		$tpmod="";
	}

	/*--> Modify */
	if($vlmod!=""&&$fgerr=="") {
		$q="UPDATE env_widget_type SET widget_type_name='".addslashes($widget_type_name)."', widget_type_label='".addslashes($widget_type_label)."', widget_type_url='$widget_type_url', widget_type_code='".addslashes($widget_type_code)."', widget_type_height='".$widget_type_height."px', widget_type_fgborder='$widget_type_fgborder', widget_type_icon='$widget_type_icon', widget_type_content_id=$widget_type_content_id WHERE widget_type_id=$widget_type_id";
		$db1->query($q);
		
		// Changement de mot de passe
		if($user_password!="") {
			$q="UPDATE env_user SET user_password='".md5($user_password)."' WHERE widget_type_id=$widget_type_id";
			$db1->query($q);
		}
		$tpmod="";
	}
	
	/*--> Delete */
	if($vlsup!="") {
		delTypeWidget($widget_type_id);
		$tpmod="";
	}

	echo "<script>$jsaction</script>";
	echo "<script type='text/javascript' src='".$config['libtable']."'></script>";
	
?>	

<script>
	$(document).ready(function() {
		$('#datatable').dataTable( {
			"oLanguage": { "sUrl": "<? echo $config['javascriptdir']; ?>/dataTables.txt" }
		} );
		
		
		if($('#widget_type_content_id').val()==5) {
			$('.code').css("display", "block");
		}
		
		$('#widget_type_content_id').bind("change", function() {		
			if($('#widget_type_content_id').val()==5) {
				$('.code').css("display", "block");
			}			
			else {
				$('#widget_type_code').val('');
				
				$('.code').css("display", "none");
			}
		});
		
	} );	
</script>

<?
	echo "</head>";
	echo "<body>";

	echo "<form name='Formulaire' enctype='multipart/form-data' action='' method='post'>";
	echo "<input id='widget_type_id' name='widget_type_id' type='hidden' value='".$widget_type_id."'>";
	echo "<input id='tpmod' name='tpmod' type='hidden' value=''>";
	
  
//-- DISPLAY -------------------------------------------------------------------------------------------------------------------------------

if($tpmod=="") { 
    echo "<h1>GESTION DE LA BIBLIOTHEQUE DE WIDGETS</h1>";
    
	echo "<input id='fgadd' name='fgadd' class='input-rounded-button' onClick='document.Formulaire.widget_type_id.value=\"\"; document.Formulaire.tpmod.value=\"SUBMIT\"' type='submit' value='Ajouter' />";
	echo "<br>";

	echo "<table class id='datatable'>";
    echo "<thead>";
	echo "<th width='70px'>Action</th>";
	echo "<th width='70px'>ID</th>";
	echo "<th width='90px'>Icone</th>";
	echo "<th>Nom</th>";
	echo "<th width='500px'>Description</th>";
	echo "<th width='90px'>Hauteur</th>";
	echo "<th width='90px'>Bordure</th>";
	echo "<th width='90px'>Type Contenu</th>";
	echo "</thead>";
	
	$q="SELECT * FROM env_widget_type, env_widget_type_content WHERE env_widget_type.widget_type_content_id=env_widget_type_content.widget_type_content_id";
	$db1->query($q);
	while($db1->next_record()){	
		echo "<tr>";

		echo "<td>";
		echo "<div id='btnupd'>";
		echo "<a onClick='document.Formulaire.widget_type_id.value=".$db1->f('widget_type_id')."; document.Formulaire.tpmod.value=\"MODIFY\";document.Formulaire.submit();'>";
		echo "<img src='images/blank.gif' width='16px' height='16px'>";
		echo "</a>";
		echo "</div>";

		echo "<div id='btndel'>";
		echo "<a onClick='document.Formulaire.widget_type_id.value=".$db1->f('widget_type_id')."; document.Formulaire.tpmod.value=\"DELETE\";document.Formulaire.submit();'>";
		echo "<img src='images/blank.gif' width='16px' height='16px'>";
		echo "</a>";
		echo "</div>";
		echo "</td>";

		echo "<td align='center'>";
		echo $db1->f('widget_type_id');
		echo "</td>";
		
		echo "<td align='center'>";
		echo "<img src='".$config["widgeticondir"]."/".$db1->f('widget_type_icon')."' width='90px' height='90px'></img>";
		echo "</td>";
		
		echo "<td>";
		echo $db1->f('widget_type_name');
		echo "</td>";

		echo "<td>";
		echo $db1->f('widget_type_label');
		echo "</td>";

		echo "<td align='center'>";
		echo $db1->f('widget_type_height');
		echo "</td>";

		echo "<td align='center'>";
		if($db1->f('widget_type_fgborder'))
			echo "oui";
		else
			echo "non";
		echo "</td>";
		
		echo "<td align='center'>";
		echo $db1->f('widget_type_content_name');
		echo "</td>";		
	
		echo "</tr>";
	} 
	echo "</table>";
}

//-- SUBMIT --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="SUBMIT") {
	// Entete du formulaire
    echo "<h1>AJOUT WIDGET</h1>";
	echo "<input id='vladd' name='vladd' class='input-rounded-button' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='input-rounded-button' type='submit' value='Annuler' />";
	echo "</br></br>";

	// Valeur par défaut
	if($widget_type_fgborder=="") 	{$widget_type_fgborder		="0";}
	if($widget_type_height=="") 	{$widget_type_height		="300";}
	if($widget_type_content_id=="") {$widget_type_content_id	= 1;}
	if($widget_type_icon=="") 		{$widget_type_icon			= "anonyme.jpg";}
}

//-- MODIFY --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="MODIFY") {
	// Entete du formulaire
    echo "<h1>MODIFICATION WIDGET</h1>";
	echo "<input id='vlmod' name='vlmod' class='input-rounded-button' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='input-rounded-button' type='submit' value='Annuler' />";
	echo "</br></br>";

	// Valeur par défaut
	$q = "SELECT * FROM env_widget_type WHERE widget_type_id=$widget_type_id";
	$db1->query($q);
	if($db1->next_record()) {
		$widget_type_name 			= $db1->f("widget_type_name");
		$widget_type_label 			= $db1->f("widget_type_label");
		$widget_type_url 			= $db1->f("widget_type_url");
		$widget_type_height		 	= $db1->f("widget_type_height");
		$widget_type_fgborder 		= $db1->f("widget_type_fgborder");
		$widget_type_content_id		= $db1->f("widget_type_content_id");
		$widget_type_icon			= $db1->f("widget_type_icon");
		$widget_type_code			= $db1->f("widget_type_code");
	}
}

//-- DELETE --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="DELETE") {
	echo "<h1>SUPPRESSION WIDGET</h1>";
	echo "<input id='vlsup' name='vlsup' class='input-rounded-button' type='submit' value='Confirmer la suppression' />";
	echo "&nbsp;";
	echo "<input class='input-rounded-button' type='submit' value='Annuler' />";    
	echo "</br></br>";

	// Valeur par défaut
	$q = "SELECT * FROM env_widget_type WHERE widget_type_id=$widget_type_id";
	$db1->query($q);
	if($db1->next_record()) {
		$widget_type_name 			= $db1->f("widget_type_name");
		$widget_type_label 			= $db1->f("widget_type_label");
		$widget_type_url 			= $db1->f("widget_type_url");
		$widget_type_height		 	= $db1->f("widget_type_height");
		$widget_type_fgborder 		= $db1->f("widget_type_fgborder");
		$widget_type_content_id		= $db1->f("widget_type_content_id");
		$widget_type_icon			= $db1->f("widget_type_icon");
		$widget_type_code			= $db1->f("widget_type_code");
	}
}


//-- FORMULAIRE -----------------------------------------------------------------------------------------------------------------------------

if($tpmod=="SUBMIT"||$tpmod=="MODIFY"||$tpmod=="DELETE") {
?>

 	<h2>Coordonnée</h2>

	<fieldset id="encadrer">
		<?
			$lbsty00="style='width:25%; border-bottom: dotted 1px;'";
			$lbsty01="type='text' size='53' style='width: 74%;'";
			$lbsty02="type='password' size='53' style='width: 74%;'";
			$lbsty03="style='width: 74%;'";
		?>

		<span id="encadrer_label" <? echo $lbsty00; ?>>ID*</span>
		<input 	value="<? echo $widget_type_id; ?>"
				id="widget_type_id_bis"
				name="widget_type_id_bis"	
				readonly
				<? echo $lbsty01; ?>>

		<p>
		<span id="encadrer_label" <? echo $lbsty00; ?>>Nom*</span>
		<input	value="<? echo $widget_type_name; ?>"
				id="widget_type_name" 
				name="widget_type_name"
				<? echo $lbsty01; ?>>
		</p>

		<p>
		<span id="encadrer_label" <? echo $lbsty00; ?>>Description*</span>
		<input 	value="<? echo $widget_type_label; ?>"	
				id="widget_type_label"
				name="widget_type_label"
				<? echo $lbsty01; ?>>
		</p>

		<p>
		<span id="encadrer_label" <? echo $lbsty00; ?>>Url</span>
		<input 	value="<? echo $widget_type_url; ?>"	
				id="widget_type_url"
				name="widget_type_url"
				<? echo $lbsty01; ?>>
		</p>

		<p>
		<span id="encadrer_label" <? echo $lbsty00; ?>>Hauteur*</span>
		<input 	value="<? echo str_replace("px","",$widget_type_height); ?>"
				id="widget_type_height"
				name="widget_type_height"
				type="number"
				<? echo $lbsty01; ?>>
		</p>

		<p>
		<span id="encadrer_label" <? echo $lbsty00; ?>>Bordure*</span>
		<select	id="widget_type_fgborder"
				name="widget_type_fgborder"
				size="1"
				<? echo $lbsty03; ?>>		
			<option value="0"	<? if($widget_type_fgborder=='0'||$widget_type_fgborder=='')	{ echo 'selected'; }?>>non</option>
			<option value="1"	<? if($widget_type_fgborder=='1')								{ echo 'selected'; }?>>oui</option>
		</select>
		</p>

		<p>
		<span id="encadrer_label" <? echo $lbsty00; ?>>Type de contenu*</span>
		<select	id="widget_type_content_id"
				name="widget_type_content_id"
				size="1"
				<? echo $lbsty03; ?>>>
			
			<?
				$q="SELECT * FROM env_widget_type_content";
				$db1->query($q);
				while($db1->next_record()){	
					$lbsel="";
					if($widget_type_content_id==$db1->f("widget_type_content_id")) {
						$lbsel="selected";
					}
					echo "<option value='".$db1->f("widget_type_content_id")."' ".$lbsel.">".$db1->f("widget_type_content_name")."</option>";
				}
			?>
		</select>
		</p>	
	</fieldset>

	<h2>Icone</h2>

	<fieldset id="encadrer">
		<?
			echo "<div id='avatar' class='avatar_admin'>";
			echo "<img id='img_avatar' name='img_avatar' src='".$config["widgeticondir"]."/".$widget_type_icon."' width='90px' height='90px'></img>";
			echo "<div>";
		
			echo "<div id='btnupd'>";
			echo "<a href='#?o=1&w=700&h=530&u=jcrop.php?dirimg=".$config["widgeticondir"]."&lbico=widget_type_icon&igico=img_avatar' rel='popup_name' class='poplight' title='Ajouter un avatar'>";
			echo "<img src='images/blank.gif' width='16px' height='16px'>";
			echo "</a>";
			echo "</div>";

			echo "<div id='btndel'>";
			echo "<a onClick='document.getElementById(\"img_avatar\").src=\"".$config["widgeticondir"]."/anonyme.jpg\"; document.Formulaire.widget_type_icon.value=\"anonyme.jpg\";'>";
			echo "<img src='images/blank.gif' width='16px' height='16px'>";
			echo "</a>";
			echo "</div>";	
			
			echo "<input id='widget_type_icon' name='widget_type_icon' type='hidden' value='".$widget_type_icon."'>";
		?>
	</fieldset>
	
	<h2 class="code" style="display: none;">Code</h2>
	
	<fieldset id="encadrer" class="code" style="display: none;">
		<textarea name="widget_type_code" id="widget_type_code" style="width:100%; height: 400px;"><? echo $widget_type_code ?></textarea>	
	</fieldset>
	
	&nbsp;
	&nbsp;
	
	<script type="text/javascript">
		$('#idget_type_name').focus();
	</script>
<?
}



//-- FOOTER ---------------------------------------------------------------------------------------------------------------------------------

echo "</form>";
echo "</body>";
echo "</html>";
?>







	
	
