	$(document).ready(function() {
		// Sur le clique d'un lien de class poplight
		$('a.poplight[href^=#]').click(function() {
			var popID = $(this).attr('rel'); 	//Get Popup Name
			var popURL = $(this).attr('href'); //Get Popup href to define size
			
			// Creation de la popup modale
			$('body').append('<div id="'+popID+'" class="popup_block"></div>');
			
			// Recuperation des parametres envoyé dans la balise href
			var query= popURL.split('?');
			var dim= query[1].split('&');
			var popOrder 	= dim[0].split('=')[1]; 
			var popWidth 	= dim[1].split('=')[1]; 
			var popHeight 	= dim[2].split('=')[1]; 
			var popUrl 		= dim[3].split('=')[1];
			
			if(query[2]!="") {
				var param="?"+query[2];
			}
			
			// calcul de l'order de la popup
			popOrder = 10000+(Number(popOrder)*10);

			// Creation de l'Iframe
			$('#' + popID).prepend('<iframe src="'+popUrl+param+'" width="100%" height="100%" frameborder=0 />');

			// Fade in the Popup and add close button
			$('#' + popID).fadeIn().css({ 'width': Number( popWidth )}).prepend('<div id="popup_close" class="popup_close"><a href="#" rel="'+popID+'" class="popup_close_link"><img src="images/blank.gif" class="popup_close_img" title="Close Window" alt="Close" /></a></div>');
			$('#' + popID).fadeIn().css({ 'height': Number( popHeight )});
			$('#' + popID).fadeIn().css({ 'z-index': Number(popOrder)});
			
			// Centrer la popup
			var popMargTop = ($('#' + popID).height() + 80) / 2;
			var popMargLeft = ($('#' + popID).width() + 80) / 2;
			
			// Appliquer les marges
			$('#' + popID).css({ 
				'margin-top' : -popMargTop,
				'margin-left' : -popMargLeft
			});
			
			// Fade le fond
			$('body').append('<div id="fade'+popID+'" class="fade"></div>'); //Add the fade layer to bottom of the body tag.
			$('#fade'+popID).css({'filter' : 'alpha(opacity=80)'}).fadeIn(); //Fade in the fade layer 
			$('#fade'+popID).css({'z-index': Number( popOrder ) - 1}).fadeIn()
			
			return false;
		});
		
		
		// Fermeture de la popup modate
		$('a.popup_close_link').live('click', function() { 
			var popID = $(this).attr('rel'); 	//Get Popup Name
		
			$('#fade'+popID+' , #'+popID).fadeOut(function() {
				$('#fade'+popID+' , a.close, #'+popID).remove();  
			});
			
			return false;
		});
	});
	
	
	function iframeclosepopup() {
		var a = $("a.popup_close_link", window.parent.document);
		var popID = a.attr('rel'); 	//Get Popup Name
		
		var b = $('#fade'+popID, window.parent.document);
		var c = $('#'+popID, window.parent.document);
		b.remove();
		c.remove();
	}
	
