	function htmlEscape(str) {
		return String(str)
				.replace(/&/g, '&amp;')
				.replace(/"/g, '&quot;')
				.replace(/'/g, '&#39;')
				.replace(/</g, '&lt;')
				.replace(/>/g, '&gt;')
				.replace(/ /g, '%20;');
	}

	function OuvreHeader() {
		$("#info").load("process-userheader.php?usermod=big");
		$("#envoleheader").addClass('big');
		$("#userheader").addClass('big');
		$("#useradmin").addClass('big');
		$(".userpanel_right").addClass('big');
		$("#ouvreheader").addClass('big');		
		
        // Hauteur iframe au refresh de la page
        var heightbody = $('html').height();
        var heightheader = $('#envoleheader').height();
        var topheader = "margin-top: " + String(heightheader) + "px !important";
        
        $("html").css("cssText",topheader);
        $("#envolepage iframe").height(heightbody - heightheader);
	}	

	function FermerHeader() {
		$("#info").load("process-userheader.php?usermod=");
		$("#envoleheader").removeClass('big');
		$("#userheader").removeClass('big');
		$("#useradmin").removeClass('big');
		$(".userpanel_right").removeClass('big');
		$("#ouvreheader").removeClass('big');	
		
        // Hauteur iframe au refresh de la page
        var heightbody = $('html').height();
        var heightheader = $('#envoleheader').height();
        var topheader = "margin-top: " + String(heightheader) + "px !important";
        
        $("html").css("cssText",topheader);
        $("#envolepage iframe").height(heightbody - heightheader);
	}

    function sleep(milliseconds) {
        var start = new Date().getTime();

        for (var i = 0; i < 1e7; i++) {
            if ((new Date().getTime() - start) > milliseconds){
                break;
            }
        }
    }
