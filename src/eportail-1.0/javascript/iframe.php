<script type="text/javascript">
	$('document').ready(function(){
		// Ajout de la classe associée au mode d'affichage 
		var user_mod="<? echo $_SESSION['user_mod']; ?>";
		$("#envoleheader").addClass(user_mod);
		$("#userheader").addClass(user_mod);
		$("#useradmin").addClass(user_mod);
		$(".userpanel_right").addClass(user_mod);
		$("#ouvreheader").addClass(user_mod);
	
        // Hauteur iframe au refresh de la page
        var heightbody = $('html').height();
        var heightheader = $('#envoleheader').height();
        var topheader = "margin-top: " + String(heightheader) + "px !important";
        
        $("html").css("cssText",topheader);
        $("#envolepage iframe").height(heightbody - heightheader);
    });

	// Hauteur iframe au resize de la window
	$(window).resize(function() {
        // Hauteur iframe au refresh de la page
        var heightbody = $('html').height();
        var heightheader = $('#envoleheader').height();
        var topheader = "margin-top: " + String(heightheader) + "px !important";
        
        $("html").css("cssText",topheader);
        $("#envolepage iframe").height(heightbody - heightheader);
	});
</script>
