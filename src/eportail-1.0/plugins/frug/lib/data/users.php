<?php
/*
 * @package AJAX_Chat
 * @author Sebastian Tschan
 * @copyright (c) Sebastian Tschan
 * @license GNU Affero General Public License
 * @link https://blueimp.net/ajax/
 */

// Insertion des variables envole
include("../../include/envole.php");
$users = array();

// Connection à la base de données
$con = mysql_connect($config['dbhost'],$config['dblogin'],$config['dbpassword']);
if (!$con) {
	die('Could not connect: ' . mysql_error());
}
mysql_select_db( $config['dbname'], $con);


// Création de l'ensemble des utilisateurs
$resultuser = mysql_query("SELECT * FROM env_user");
while($rowuser = mysql_fetch_array($resultuser)) {
	// Création de l'instance de l'utilisateur
	$users[$rowuser['user_id']] = array();
	
	// On détermine son profil
	if($rowuser['user_profil_id']==1)
		$tpprofil=AJAX_CHAT_ADMIN;
	elseif($rowuser['user_profil_id']==2)
		$tpprofil=AJAX_CHAT_MODERATOR;
	elseif($rowuser['user_profil_id']==3)
		$tpprofil=AJAX_CHAT_USER;
	else
		$tpprofil=AJAX_CHAT_GUEST;
		
	// Enregistrement des caractéristiques de l'utilisateur
	$users[$rowuser['user_id']]['userRole']=$tpprofil;
	$users[$rowuser['user_id']]['userName']=$rowuser['user_login'];
	$users[$rowuser['user_id']]['password']="essai";
	$users[$rowuser['user_id']]['channels'] = array(0,1);
	
	// Création de l'ensemble des utilisateurs
	if($rowuser['user_profil_id']==1)
		$resultgroup = mysql_query("SELECT * FROM env_group");
	else
		$resultgroup = mysql_query("SELECT * FROM env_group, env_groupuser WHERE group_id=groupuser_group_id AND groupuser_user_id=".$rowuser['user_id']);
		
	$k=0;
	while($rowugroup = mysql_fetch_array($resultgroup)) {
		$k++;
		$users[$rowuser['user_id']]['channels'][$k]=$rowugroup['group_id'];
	}
}

/*
for($j=2;$j<(count($users)-4)$j++){
$result = mysql_query("SELECT * FROM users_channels WHERE userName='".$users[$j]['userName']."'");

$k=0;
while($row = mysql_fetch_array($result))
  {
     $k++;
     $user[$j]['channels'][$k]=$row['number'];
  } 
}
*/


mysql_close($con);







/* ANCIEN CODE 
// List containing the registered chat users:
$users = array();

// Default guest user (don't delete this one):
$users[0] = array();
$users[0]['userRole'] = AJAX_CHAT_GUEST;
$users[0]['userName'] = null;
$users[0]['password'] = null;
$users[0]['channels'] = array(0);

// Sample admin user:
$users[1] = array();
$users[1]['userRole'] = AJAX_CHAT_ADMIN;
$users[1]['userName'] = 'admin';
$users[1]['password'] = 'admin';
$users[1]['channels'] = array(0,1);

// Sample moderator user:
$users[2] = array();
$users[2]['userRole'] = AJAX_CHAT_MODERATOR;
$users[2]['userName'] = 'moderator';
$users[2]['password'] = 'moderator';
$users[2]['channels'] = array(0,1);

// Sample registered user:
$users[3] = array();
$users[3]['userRole'] = AJAX_CHAT_USER;
$users[3]['userName'] = 'user';
$users[3]['password'] = 'user';
$users[3]['channels'] = array(0,1);
*/
?>
