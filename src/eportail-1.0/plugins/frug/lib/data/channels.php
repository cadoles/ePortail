<?php
/*
 * @package AJAX_Chat
 * @author Sebastian Tschan
 * @copyright (c) Sebastian Tschan
 * @license GNU Affero General Public License
 * @link https://blueimp.net/ajax/
 */

// Insertion des variables envole
include("../../include/envole.php");
$channels = array();

// Sample channel list:
$channels[0] = 'Public';
$channels[1] = 'Private';

// Connection à la base de données
$con = mysql_connect($config['dbhost'],$config['dblogin'],$config['dbpassword']);
if (!$con) {
	die('Could not connect: ' . mysql_error());
}
mysql_select_db( $config['dbname'], $con);

// Création de l'ensemble des channels
$resultgroup = mysql_query("SELECT * FROM env_group");
while($rowgroup = mysql_fetch_array($resultgroup)) {
	$channels[$rowgroup['group_id']] = $rowgroup['group_label'];
}

?>
