<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?
	include_once("include/include.php");				/* Général		*/
	include_once($config["templatedir"]."/html.php");	/* Entete HTML	*/
	include_once($config["templatedir"]."/popup.php");
	$db1=new ps_db;
	
	$tpmod=$_GET['tpmod'];
	$idpro=$_GET['idpro'];
	$idpan=$_GET['idpan'];
	
	if($idpro=="")  $idpro=$_POST['idpro'];
	if($idpan=="")  $idpan=$_POST['idpan'];

	$vladd=$_POST['vladd'];	
	$vlmod=$_POST['vlmod'];	
	$fgclo=$_POST['fgclo'];
	$order=$_POST['order'];
	
	if($vladd!="") {
		// Rechercher le dernier order 
		$order=1;
		$q="SELECT panel_profil_order FROM env_panel_profil WHERE panel_profil_profil_id=$idpro ORDER BY panel_profil_order DESC LIMIT 1";
		$db1->query($q);
		if($db1->next_record()) {
			$order=$db1->f('panel_profil_order')+1;
		}

		// Création 
		$q="INSERT INTO env_panel_profil (panel_profil_panel_id,panel_profil_profil_id,panel_profil_order) VALUES($idpan,$idpro,$order)";
		$db1->query($q);		

		// Action javascript Ajout du panel directement + fermeture de la popup
		$jsaction="recharge(".$idpan.")";
	}
	
	if($vlmod!="") {
		$q="UPDATE env_panel_profil SET panel_profil_order=$order WHERE panel_profil_profil_id=$idpro AND panel_profil_panel_id=$idpan";
		$db1->query($q);	
		
		// Action javascript Ajout du panel directement + fermeture de la popup
		$jsaction="recharge(".$idpan.")";
	}
	
	if($tpmod=="DELETE") {
		$q="DELETE FROM env_panel_profil WHERE  panel_profil_profil_id=$idpro  AND panel_profil_panel_id=$idpan";
		$db1->query($q);			

		// Action javascript Ajout du panel directement + fermeture de la popup
		$jsaction="recharge(".$idpan.")";
	}
	
	
	if($fgclo!="") {
		$jsaction="recharge(".$idpan.")";
	}

	echo "<script type='text/javascript' src='".$config['libtable']."'></script>";
	
?>	

<script>
	$(document).ready(function() {
		$('#datatable').dataTable( {
			"oLanguage": { "sUrl": "<? echo $config['javascriptdir']; ?>/dataTables.txt" }
		} );
	} );	

	function recharge(idpan,type) {
		parent.document.Formulaire.fgreload.value="MODIFY",
		parent.document.Formulaire.submit();
		iframeclosepopup();
	}
	<?php echo $jsaction ?>
</script>

</head>

<body>

<div id="envolepage">
<form name="Formulaire" method="post">

<?
	echo "<input id='idpro' name='idpro' type='hidden' value='".$idpro."'>";
	echo "<input id='idpan' name='idpan' type='hidden' value='".$idpan."'>";
	echo "<input id='vladd' name='vladd' type='hidden' value=''>";

if($tpmod=="SUBMIT"){
	echo "<h1>Ajout d'une page au profil</h1>";
	
	echo "<table class id='datatable'>";
	echo "<thead>";
	echo "<th width='40px'>Action</th>";
	echo "<th>ID</th>";
	echo "<th>Nom</th>";
	echo "<th>Type</th>";
	echo "<th>Propriétare</th>";
	echo "</thead>";
	
	$q="SELECT * FROM env_panel, env_panel_type, env_user, env_profil
		WHERE panel_type_type_id=panel_type_id
		AND user_id=panel_user_id
		AND profil_id=user_profil_id
		AND NOT EXISTS(SELECT panel_profil_panel_id FROM env_panel_profil WHERE panel_profil_panel_id=panel_id AND panel_profil_profil_id=$idpro)
		ORDER BY panel_id DESC";

	$db1->query($q);
	while($db1->next_record()){	
		echo "<tr>";

		echo "<td>";
		echo "<div id='btnadd'>";
		echo "<a onClick='document.Formulaire.idpan.value=".$db1->f('panel_id')."; document.Formulaire.vladd.value=\"SUBMIT\";document.Formulaire.submit();'>";
		echo "<img src='images/blank.gif' width='16px' height='16px'>";
		echo "</a>";
		echo "</div>";
		echo "</td>";

		echo "<td>";
		echo $db1->f('panel_id');
		echo "</td>";

		echo "<td>";
		echo $db1->f('panel_label');
		echo "</td>";

		echo "<td>";
		echo $db1->f('panel_type_type_name');
		echo "</td>";
		
		echo "<td>";
			echo "<div id='avatar' class='avatar_admin'>";

			echo "<img src='".$config["avatardir"]."/".$db1->f('user_avatar')."' width='90px' height='90px'></img>";
			echo "<div>";
			echo  $db1->f('user_firstname')." ".$db1->f('user_lastname')."<br>";
			echo "<span id='avatar_description'>";
			echo "Login : ".$db1->f('user_login')."<br>";
			echo "Profil : ".$db1->f('profil_label')."<br>";
			echo "</span>";
			echo "</div>";

			echo "</div>";
		echo "</td>";

		echo "</tr>";
	} 

	echo "</table>";
	
	echo "<br>";
	echo "<br>";
	echo "<center>";
	echo "<input id='fgclo' name='fgclo' class='input-rounded-button' type='submit' value='Fermer' />";
	echo "</center>";
}

if($tpmod=="MODIFY") {
	echo "<h1>Modification d'une page au profil</h1>";
	echo "<br>";
	echo "<input id='vlmod' name='vlmod' class='input-rounded-button' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input id='fgclo' name='fgclo' class='input-rounded-button' type='submit' value='Annuler' />";
	echo "<br>";
	echo "<br>";
		
	$q="SELECT * FROM env_panel_profil, env_panel, env_panel_type, env_user, env_profil
		WHERE panel_type_type_id=panel_type_id
		AND user_id=panel_user_id
		AND profil_id=user_profil_id
		AND panel_profil_profil_id=$idpro
		AND panel_profil_panel_id=$idpan
		AND panel_id=panel_profil_panel_id";
	$db1->query($q);
	if($db1->next_record()){		
			
?>
	<fieldset id="encadrer">
		<?
			$lbsty00="style='width:25%; border-bottom: dotted 1px;'";
			$lbsty01="type='text' size='53' style='width: 74%;'";
			$lbsty02="type='password' size='53' style='width: 74%;'";
			$lbsty03="style='width: 74%;'";
		?>

		<span id="encadrer_label" <? echo $lbsty00; ?>>ID Page*</span>
		<input 	value="<? echo $idpan; ?>"
				id="id_bis"
				name="id_bis"	
				readonly
				<? echo $lbsty01; ?>>

		<p>
		<span id="encadrer_label" <? echo $lbsty00; ?>>Nom*</span>
		<input	value="<? echo $db1->f("panel_label"); ?>"
				id="label" 
				name="label"
				readonly
				<? echo $lbsty01; ?>>
		</p>

		<span id="encadrer_label" <? echo $lbsty00; ?>>Type*</span>
		<input	value="<? echo $db1->f("panel_type_type_name"); ?>"
				id="type" 
				name="type"
				readonly
				<? echo $lbsty01; ?>>
		</p>

		<span id="encadrer_label" <? echo $lbsty00; ?>>Order*</span>
		<input	value="<? echo $db1->f("panel_profil_order"); ?>"
				id="order" 
				name="order"
				<? echo $lbsty01; ?>>
		</p>
	</fieldset>

	<script type="text/javascript">
			$('#order').focus();
	</script>


<?
	}
}
?>

	
</form>
</div>

</body>
</html>
