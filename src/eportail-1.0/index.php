<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?
	include_once("include/include.php");					/* Général				*/
	include_once($config["templatedir"]."/html.php");		/* Entete HTML			*/
	include_once($config["javascriptdir"]."/iframe.php");	/* Gestion iframe		*/
	
	$db1=new ps_db;
	$idpan=$_GET['idpan'];

	// Rechercher si le panel envoyé par paramètre existe bien
	if($idpan!="") {
		$q="SELECT panel_id FROM env_panel WHERE panel_id=".$idpan;
		$db1->query($q);
		if(!$db1->next_record()) {
			$idpan="";
		}
		else {
			$fglok=false;
			$q="SELECT * FROM env_panel_profil, env_panel WHERE panel_profil_profil_id=".$_SESSION['user_profil']." AND panel_id=$idpan AND panel_profil_panel_id=panel_id ORDER BY panel_profil_order LIMIT 1";
			$db1->query($q);
			if($db1->next_record()) { $fglok=true; }
		}
	}

	// Rechercher le premier panel si aucun panel n'a été envoyé en paramétre
	if($idpan=="") {
		$fglok=true;
		
		$q="SELECT * FROM env_panel_profil, env_panel WHERE panel_profil_profil_id=".$_SESSION['user_profil']." AND panel_profil_panel_id=panel_id ORDER BY panel_profil_order LIMIT 1";
		$db1->query($q);
		if($db1->next_record()) {
			$idpan=$db1->f('panel_id');
		}		
		else {
			$q="SELECT panel_id FROM env_panel WHERE panel_user_id=".$_SESSION['user_id']." AND panel_visible ORDER BY panel_order LIMIT 1";
			$db1->query($q);
			if($db1->next_record()) {
				$idpan=$db1->f('panel_id');
				$fglok=false;
			}		
		}
	}
	
	// Simuler un click d'onglet si on a un onglet de positionné
	$jsaction = "ClickOnglet(".$idpan.");";
?>

<script type="text/javascript">
	// Gestion du double click sur Panel
	var divdbl = $(".userpanellabel a");
    divdbl.dblclick(function () { divdbl.toggleClass('dbl'); });

	// Gestion de l'ordre modifiable des panels
	$(document).ready(function()
	{
		$("#userpanel").sortable({
			handle : '.userpanelhandle',
			update : function ()
			{
				var order = $('#userpanel').sortable('serialize');
				$("#info").load("process-userpanel-order.php?"+order);
			}
		});
	});
	
	// Gestion de l'affichage du lien sur simple click d'un Panel
	$(function()
	{
		$(".userpanellabel a").click(function() {
			var a = $(this);
			
			// Recuperation de l'id de l'onglet
			var query= a.attr("href").split('?');
			var dim= query[1].split('&');
			var idpan = dim[0].split('=')[1];
			var fglok = dim[1].split('=')[1];
			
			// Sauvegarder l'id de l'onglet
			$("#idpan").val(idpan);
			
			
			if ($("#userpanelsup").length > 0){
				if(fglok=="true") {
					$("#userpanelsup").css('display', 'none');
					$("#userpanelupd").css('display', 'none');
				}
				else {
					$("#userpanelsup").css('display', 'inline');
					$("#userpanelupd").css('display', 'inline');
				}
			 }
			
			
			// Changement de l'adresse pour qu'un refresh tombe sur le meme ongle
			history.pushState(null, "page", "index.php?idpan="+idpan);
			
			// Affichage de l'iframe associée à l'onglet
			$('#envolepage iframe').attr("src",a.attr("label"));
			
			// Retour faux pour que le href ne se lance pas
			return false;
		});
	});	
	
	
	
	// Gestion de la modification du label sur double click
	$(function()
	{
		$(".userpanellabel a").dblclick(function() {
			var a = $(this);
			a.parent().addClass("edit");
			a.next().val(a.text()).focus();
			return false;
		});
				
		$(".userpanellabel a").dblclick(function() {
			var a = $(this);
			a.parent().addClass("edit");
			a.next().val(a.text()).focus();
			return false;
		});

		$(".userpanellabel :text").blur(function() {
			var txt = $(this);
			if (txt.val() == "") {
				alert("Vous devez saisir un label");
				txt.focus();
			}
			else {
				//txt.prev().attr("href", txt.val());
				txt.prev().html(txt.val());
				txt.parent().removeClass("edit");

				var id = txt.parent().parent().parent().attr("id");
				var label = txt.val();
				label = encodeURIComponent(label);
				
				$("#info").load("process-userpanel-label.php?id="+id+"&label="+label);
			}
		});
	});	

	// Gestion de la suppresion d'un panel
	function clickpanelsup	() {
		var idpan=$("#idpan").val();

		if(confirm("Confirmez-vous la suppression de cet onglet ?")) {
			$("#info").load("process-userpanel-sup.php?id="+idpan);
			document.body.style.cursor = "wait"
			setTimeout(function () {location.reload()},1000);	
		}
		return false;
	}
	
	function clickpanelmod() {
		var idpan=$("#idpan").val();
		$('#userpanelupd a').attr("href", "#?o=1&w=600&h=450&u=popup-userpanel-mod.php?idpan="+idpan);
	}
	

	// Fonction qui simule le choix d'un panel
	$(document).ready(function()
	{
		function ClickOnglet(idpan) {
			var panel = "#userpanel_".concat(idpan);
			$(panel+" a").click();
		}
	
		<? echo $jsaction; ?>
	});
</script>

<style>

</style>


</head>

<body>
<div id="envoleheader">
	
	<!-- Header !-->
	<? include_once("header-user.php"); ?>

	<ul id="fixepanel">
	<?
		$q="SELECT * FROM env_panel_profil, env_panel WHERE panel_profil_profil_id=".$_SESSION['user_profil']." AND panel_profil_panel_id=panel_id ORDER BY panel_profil_order";
		$db1->query($q);
		$order=0; 
		$fgpan=0;
		while($db1->next_record()){	
			$fgpan=1;
	?>
		<li id="userpanel_<? echo $db1->f('panel_id'); ?>">
			<div class="fixepanelhandle">
				<?
					$label_panel=$db1->f('panel_url');
					if($db1->f('panel_type')!=1) $label_panel=$label_panel."?idpan=".$db1->f('panel_id');
				?>
									
				<div class="userpanellabel">
					<a href="?idpan=<? echo $db1->f('panel_id'); ?>&fglok=true" label="<? echo $label_panel; ?>" class="fixepanellabelllink"><? echo $db1->f('panel_label'); ?></a>
				</div>
			</div>
		</li>
	<?			
		}
	?>
	</ul>

	
	<?
		if($_SESSION['user_profil'] < 99&&$fgpan==1) {
			echo "<div id='userfavori'></div>";
		}
	?>
	
	<ul id="userpanel">
	<?
		$q="SELECT * FROM env_panel
					WHERE panel_user_id=".$_SESSION['user_id']." 
					AND panel_visible
					AND NOT EXISTS(SELECT * FROM env_panel_profil WHERE panel_profil_panel_id=panel_id AND panel_profil_profil_id=".$_SESSION['user_profil'].")
					ORDER BY panel_order";
					
		$db1->query($q);
		$order=0; 
		while($db1->next_record()){		
			$order=$order+1;
	?>
		<li id="userpanel_<? echo $db1->f('panel_id'); ?>">
			<div class="userpanelhandle">
				<?
					$label_panel=$db1->f('panel_url');
					if($db1->f('panel_type')!=1) $label_panel=$label_panel."?idpan=".$db1->f('panel_id');
				?>
									
				<div class="userpanellabel">
					<a href="?idpan=<? echo $db1->f('panel_id'); ?>&fglok=false" label="<? echo $label_panel; ?>" class="userpanellabelllink"><? echo $db1->f('panel_label'); ?></a>
					<input type="text" value="<? echo $db1->f("panel_label"); ?>">
				</div>
			</div>
		</li>
	<?
		}
	?>
	</ul>

	<?
		if($_SESSION['user_profil'] < 4) {
	?>
	
	<div id="userpaneladd">
		<a href="#?o=1&w=600&h=450&u=popup-userpanel-add.php" rel="popup_name" class="poplight" title="Ajouter un onglet"><img src="images/blank.gif" width="16px" height="16px"></a>
	</div>
	
	<div id="userpanelupd">
		<a href="#" onClick="clickpanelmod();" rel="popup_name" class="poplight" title="Modifier un onglet"><img src="images/blank.gif" width="16px" height="16px"></a>
	</div>


	<div id="userpanelsup">
		<a href="#" onClick="clickpanelsup();" title="Supprimer l'onglet en cours"><img src="images/blank.gif"  width="16px" height="16px"></a>
	</div>
	
	<!--
	<div id="userpanelopt">
		<a href="#?o=1&w=600&h=450&u=popup-userpanel-opt.php" rel="popup_name" class="poplight" title="Ajouter un onglet"><img src="images/blank.gif"  width="16px" height="16px"></a>
	</div>
	!-->
	
	<? } ?>		

	<!-- Menu d'administration !-->
	<? include_once("header-admin.php"); ?>


</div>

<div id="envolepage">
	
<div id="info"></div>
<form method="get" name="formulaire"> 
	<input type="hidden" name="idpan" id="idpan" value="<? echo $idpan; ?>" /> 
</form>

<iframe src="" width=100% height=100% frameborder=0 />

</div>

</body>
</html>
