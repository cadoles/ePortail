<?
	include_once("include/include.php");				/* Général			*/
	include_once($config["templatedir"]."/html.php");	/* Entete HTML		*/
	include_once($config["templatedir"]."/rss.php");	/* Entete Frame		*/
	
	$db1=new ps_db;
	
	$idwid=$_GET["idwid"];
	
	$q="SELECT * FROM env_page_widget, env_widget_type WHERE page_widget_id=".$idwid." AND page_widget_type_id=widget_type_id";
	$db1->query($q);
	if($db1->next_record()) {
		// Définition de la source de l'url : soit celle de l'utilisateur soit celle du widget si type de widget systeme l'url est forcement déterminé par l'utilisateur
		if($db1->f('widget_type_id') < 0)		
			$feedurl=$db1->f("page_widget_url");
		else
			$feedurl=$db1->f("widget_type_url");
	}
	
	echo "<script type='text/javascript' src='".$config['librss']."'></script>";
?>

<script type="text/javascript" >
	$(document).ready(function(){
		$('#divRss').FeedEk({
			FeedUrl : '<? echo $feedurl; ?>',
			MaxCount : 5,
			ShowDesc : true,
			ShowPubDate: true
		});
	});
</script>

<?	
	echo "</head>";
	echo "<body>";	
	
	echo "<div id='divRss'></div>";
	
	echo "</body>";
	echo "</html>";
?>
