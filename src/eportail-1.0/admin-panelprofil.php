<?
	include_once("include/include.php");				/* Général		*/
	include_once("include/admin.php");					/* Page Admin	*/
	include_once($config["templatedir"]."/html.php");	/* Entete HTML	*/
	include_once($config["templatedir"]."/frame.php");	/* Entete HTML	*/
	
	$db1=new ps_db;
	$db2=new ps_db;
	

	$id					=$_POST['id'];
	$tpmod				=$_POST['tpmod'];

	$fgreload			=$_POST['fgreload'];
	$vlmod				=$_POST['vlmod'];
	$vladd				=$_POST['vladd'];
	$vlsup				=$_POST['vlsup'];
	
	$label				=$_POST['label'];
	$idproprio			=$_POST['idproprio'];
	
	/*--> Controle de cohérance */
	if($vladd!=""||$vlmod!="") {
		$fgerr="";
	}
	
	/*--> Rester sur le mode encours */
	if($vlmod!=""&&fgerr!="") $tpmod="MODIFY";
	if($vladd!=""&&fgerr!="") $tpmod="SUBMIT";
	if($fgreload!="") {
		$tpmod=$fgreload; 
		$vladd="";
		$vlmod="";
		$vldel="";
	}
		
	/*--> Submit */
	if($vladd!=""&&$fgerr=="") {
		$q="INSERT INTO env_panel(panel_label, panel_user_id, panel_url, panel_type_id) VALUES('$label',$idproprio,'editeur.php',3)";
		$db1->query($q);
		$tpmod="";
	}

	/*--> Modify */
	if($vlmod!=""&&$fgerr=="") {
		$q="UPDATE env_panel SET panel_label='$label', panel_user_id=$idproprio WHERE panel_id=$id";
		$db1->query($q);
		$tpmod="";
	}
	

	echo "<script>$jsaction</script>";
	echo "<script type='text/javascript' src='".$config['libtable']."'></script>";
	
?>	

<script>
	$(document).ready(function() {
		$('#datatable').dataTable( {
			"oLanguage": { "sUrl": "<? echo $config['javascriptdir']; ?>/dataTables.txt" }
		} );
	} );	
</script>

<?	
	echo "</head>";
	echo "<body>";

	echo "<form name='Formulaire' enctype='multipart/form-data' action='' method='post'>";
	echo "<input id='id' name='id' type='hidden' value='".$id."'>";
	echo "<input id='tpmod' name='tpmod' type='hidden' value=''>";
	echo "<input id='fgreload' name='fgreload' type='hidden' value=''>";
	
  
//-- DISPLAY -------------------------------------------------------------------------------------------------------------------------------

if($tpmod=="") { 
    echo "<h1>GESTION DES ONGLETS PAR PROFIL</h1>";
    

	$q="SELECT * FROM env_profil";
	$db1->query($q);

	echo "<table class id='datatable'>";
    echo "<thead>";
	echo "<th width='40px'>Action</th>";
	echo "<th>ID</th>";
	echo "<th>Profil</th>";
	echo "<th>Onglets</th>";
	echo "</thead>";
	
	while($db1->next_record()){	
		echo "<tr>";

		echo "<td>";
		echo "<div id='btnupd'>";
		echo "<a onClick='document.Formulaire.id.value=".$db1->f('profil_id')."; document.Formulaire.tpmod.value=\"MODIFY\";document.Formulaire.submit();'>";
		echo "<img src='images/blank.gif' width='16px' height='16px'>";
		echo "</a>";
		echo "</div>";
		echo "</td>";

		echo "<td>";
		echo $db1->f('profil_id');
		echo "</td>";

		echo "<td>";
		echo $db1->f('profil_label');
		echo "</td>";

		echo "<td>";
		$q="SELECT * FROM env_panel_profil, env_panel WHERE panel_profil_profil_id=".$db1->f("profil_id")." AND panel_profil_panel_id=panel_id ORDER BY panel_profil_order";
		$db2->query($q);
		while($db2->next_record()){	
			echo $db2->f("panel_id")." = ".$db2->f("panel_label")."<br>";
		}
		echo "</td>";

		echo "</tr>";
	} 

	echo "</table>";
}

//-- SUBMIT --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="SUBMIT") {

}

//-- MODIFY --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="MODIFY") {
	// Entete du formulaire
    echo "<h1>MODIFICATION ONGLET PAR PROFIL</h1>";
	echo "<input id='vlmod' name='vlmod' class='input-rounded-button' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='input-rounded-button' type='submit' value='Annuler' />";
	echo "</br></br>";

	// Valeur par défaut
	$q = "SELECT * FROM env_profil WHERE profil_id=$id";
	$db1->query($q);
	if($db1->next_record()) {
		$label 		= $db1->f('profil_label');
	}
}

//-- DELETE --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="DELETE") {
	echo "<h1>SUPPRESSION APPLICATION</h1>";
	echo "<input id='vlsup' name='vlsup' class='input-rounded-button' type='submit' value='Confirmer la suppression' />";
	echo "&nbsp;";
	echo "<input class='input-rounded-button' type='submit' value='Annuler' />";    
	
	// Valeur par défaut
	$q = "SELECT * FROM desktopitem WHERE desktopid=$desktopid";
	$db1->query($q);
	if($db1->next_record()) {	
		echo "<div id='avatar' class='avatar_admin'>";

		echo "<img src='".$db1->f('application_icon')."' width='90px' height='90px'></img>";
		echo "<div>";
		echo $db1->f('application_label')."<br>";
		echo "<span id='avatar_description'>";
		echo "id : ".$db1->f('application_name')."<br>";
		echo "url : <a href='".$db1->f('application_url')."' target='_black'>".$db1->f('application_url')."</a><br>";				
		echo "</span>";
		echo "</div>";

		echo "</div>";	
	}
}


//-- FORMULAIRE -----------------------------------------------------------------------------------------------------------------------------

if($tpmod=="SUBMIT"||$tpmod=="MODIFY") {
?>

 	<h2>Profil</h2>

	<fieldset id="encadrer">
		<?
			$lbsty00="style='width:25%; border-bottom: dotted 1px;'";
			$lbsty01="type='text' size='53' style='width: 74%;'";
			$lbsty02="type='password' size='53' style='width: 74%;'";
			$lbsty03="style='width: 74%;'";
		?>

		<span id="encadrer_label" <? echo $lbsty00; ?>>ID Application*</span>
		<input 	value="<? echo $id; ?>"
				id="id_bis"
				name="id_bis"	
				readonly
				<? echo $lbsty01; ?>>

		<p>
		<span id="encadrer_label" <? echo $lbsty00; ?>>Nom*</span>
		<input	value="<? echo $label; ?>"
				id="label" 
				name="label"
				readonly
				<? echo $lbsty01; ?>>
		</p>

	</fieldset>
	
	<br>
 	<h2>Liste des Onglets</h2>
 	<?
		
		echo "<a href='#?o=1&w=900&h=530&u=popup-adminpanelprofil-add.php?idpro=".$id."&tpmod=SUBMIT' rel='popup_name' class='poplight' title='Ajouter Page'><span class='link-rounded-button'>Ajouter</span></a>";

		echo "<table class id='datatable'>";
		echo "<thead>";
		echo "<th width='40px'>Action</th>";
		echo "<th>ID</th>";
		echo "<th>Nom</th>";
		echo "<th>Type</th>";
		echo "<th>Ordre</th>";
		echo "</thead>";

		$q="SELECT * FROM env_panel_profil, env_panel, env_panel_type WHERE panel_profil_profil_id=".$id." AND panel_profil_panel_id=panel_id AND panel_type_id = panel_type_type_id ORDER BY panel_profil_order";
		$db2->query($q);
		while($db2->next_record()){	
			echo "<tr>";
			echo "<td>";
			
			echo "<div id='btnupd'>";
			echo "<a href='#?o=1&w=900&h=530&u=popup-adminpanelprofil-add.php?idpro=".$id."&idpan=".$db2->f("panel_id")."&tpmod=MODIFY' rel='popup_name' class='poplight' title='Supprimer Page'>";
			echo "<img src='images/blank.gif' width='16px' height='16px'>";
			echo "</a>";
			echo "</div>";
			
			echo "<div id='btndel'>";
			echo "<a href='#?o=1&w=900&h=530&u=popup-adminpanelprofil-add.php?idpro=".$id."&idpan=".$db2->f("panel_id")."&tpmod=DELETE' rel='popup_name' class='poplight' title='Supprimer Page'>";
			echo "<img src='images/blank.gif' width='16px' height='16px'>";
			echo "</a>";
			echo "</div>";
						
			
			echo "</td>";
			
			echo "<td>".$db2->f("panel_id")."</td>";
			echo "<td>".$db2->f("panel_label")."</td>";
			echo "<td>".$db2->f("panel_type_type_name")."</td>";
			echo "<td>".$db2->f("panel_profil_order")."</td>";
			
			echo "</tr>";
		}		
		
		echo "</table>";
	?>
	
	<script type="text/javascript">
			$('#label').focus();
	</script>
<?
}



//-- FOOTER ---------------------------------------------------------------------------------------------------------------------------------

echo "</form>";
echo "</body>";
echo "</html>";
?>







	
	
