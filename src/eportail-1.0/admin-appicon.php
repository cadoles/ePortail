<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?
	include_once("include/include.php");				/* Général		*/
	include_once("include/admin.php");					/* Page Admin	*/
	include_once($config["templatedir"]."/html.php");	/* Entete HTML	*/
	include_once($config["templatedir"]."/frame.php");	/* Entete HTML	*/
	
	$db1=new ps_db;
	$db2=new ps_db;
	
	$tpmod=$_POST['tpmod'];
	$vladd=$_POST['vladd'];
	$vlsup=$_POST['vlsup'];
	
	$appicon_id			=$_POST['appicon_id'];
	$appicon_url		=$_POST['appicon_url'];
	
	/*--> Controle de cohérance */
	if($vladd!="") {
		$fgerr="";

		if($appicon_url=="") {
			$jsaction="alert('Vous devez renseigner l\'ensemble des informations');";
			$fgerr=1;
		}
	}
	
	/*--> Rester sur le mode encours */
	if($vlmod!=""&&fgerr!="") $tpmod="MODIFY";
	if($vladd!=""&&fgerr!="") $tpmod="SUBMIT";
		
	/*--> Submit */
	if($vladd!=""&&$fgerr=="") {
		$q="INSERT INTO env_appicon(appicon_url) VALUES('$appicon_url')";
		$db1->query($q);
		$tpmod="";
	}

	/*--> Delete */
	if($vlsup!="") {
		$q="DELETE FROM env_appicon WHERE appicon_id=$appicon_id";
		$db1->query($q);
		$tpmod="";
	}
	?>
	
	<?
	echo "<script>$jsaction</script>";
	echo "</head>";
	echo "<body>";

	echo "<form name='Formulaire' enctype='multipart/form-data' action='' method='post'>";
	echo "<input id='appicon_id' name='appicon_id' type='hidden' value='".$appicon_id."'>";
	echo "<input id='tpmod' name='tpmod' type='hidden' value=''>";
	
  
//-- DISPLAY -------------------------------------------------------------------------------------------------------------------------------

if($tpmod=="") { 
    echo "<h1>GESTION DES ICONES</h1>";
    
    echo "<a href='#?o=1&w=700&h=530&u=jcrop.php?dirimg=".$config["appicondir"]."&query=INSERT INTO env_appicon (appicon_url) VALUES($$)&refresh=1' rel='popup_name' class='poplight' title='Ajouter un icone'><span class='link-rounded-button'>Ajouter</span></a>";
    
	//echo "<input id='fgadd' name='fgadd' class='input-rounded-button' onClick='document.Formulaire.appicon_id.value=\"\"; document.Formulaire.tpmod.value=\"SUBMIT\"' type='submit' value='Ajouter' />";
	echo "<br>";

	
	$q="SELECT * FROM env_appicon";
	$db1->query($q);
	while($db1->next_record()){	
		echo "<div id='appicon' class='appicon'>";

		echo "<img id='imgapp".$db1->f('appicon_id')."' src='".$config['appicondir']."/".$db1->f('appicon_url')."' width='90px' height='90px'></img>";
		echo "<div>";
		echo "<span id='avatar_description'>";
		echo "<div id='btnupd'>";
		echo "<a href='#?o=1&w=700&h=530&u=jcrop.php?dirimg=".$config["appicondir"]."&igico=imgapp".$db1->f('appicon_id')."&query=UPDATE env_appicon SET appicon_url=$$ WHERE appicon_id=".$db1->f('appicon_id')."' rel='popup_name' class='poplight' title='Ajouter un icone'>";
		echo "<img src='images/blank.gif' width='16px' height='16px'>";
		echo "</a>";
		echo "</div>";

		// Pas de suppression possible si icone lié à une application
		$q="SELECT application_id FROM env_application WHERE application_icon=".$db1->f('appicon_id');
		$db2->query($q);
		if(!$db2->next_record()) {
			echo "<div id='btndel'>";
			echo "<a onClick='document.Formulaire.appicon_id.value=".$db1->f('appicon_id')."; document.Formulaire.tpmod.value=\"DELETE\";document.Formulaire.submit();'>";
			echo "<img src='images/blank.gif' width='16px' height='16px'>";
			echo "</a>";
			echo "</div>";
		}
		
		echo "</span>";
		echo "</div>";

		echo "</div>";
		
	} 

}

//-- DELETE --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="DELETE") {
	echo "<h1>SUPPRESSION ICONE</h1>";
	echo "<input id='vlsup' name='vlsup' class='input-rounded-button' type='submit' value='Confirmer la suppression' />";
	echo "&nbsp;";
	echo "<input class='input-rounded-button' type='submit' value='Annuler' />";    

	$q = "SELECT * FROM env_appicon WHERE appicon_id=$appicon_id";
	$db1->query($q);
	if($db1->next_record()) {
		echo "<br><div id='appicon' class='appicon'>";
		echo "<img id='imgapp".$db1->f('appicon_id')."' src='".$config['appicondir']."/".$db1->f('appicon_url')."' width='90px' height='90px'></img>";
		echo "</div>";
	}
}


//-- FOOTER ---------------------------------------------------------------------------------------------------------------------------------

echo "</form>";
echo "</body>";
echo "</html>";
?>







	
	
