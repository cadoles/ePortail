<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?
	include_once("include/include.php");				/* Général		*/
	include_once($config["templatedir"]."/html.php");	/* Entete HTML	*/
	include_once($config["templatedir"]."/frame.php");
	$db1=new ps_db;
	
	$user_id			=$_SESSION['user_id'];

	$vlmod				=$_POST['vlmod'];
	$vlann				=$_POST['vlann'];
	
	$user_login			=$_POST['user_login'];
	$user_password		=$_POST['user_password'];
	$user_passwordbis	=$_POST['user_passwordbis'];
	$user_lastname		=$_POST['user_lastname'];
	$user_firstname		=$_POST['user_firstname'];
	$user_pseudo		=$_POST['user_pseudo'];
	$user_email			=$_POST['user_email'];
	$user_avatar		=$_POST['user_avatar'];
	
	
	/*--> Controle de cohérance */
	if($vlmod!="") {
		$fgerr="";

		if($user_password!=$user_passwordbis) {
			$jsaction="alert('Votre mot de passe n\'est pas confirmé');";
			$fgerr=1;
		}

		if($user_login==""||$user_lastname==""||$user_firstname==""||$user_email=="") {
			$jsaction="alert('Vous devez renseigner l\'ensemble des informations');";
			$fgerr=1;
		}
		
		$q="SELECT * FROM env_user WHERE (user_login='$user_login' OR user_email='$user_email' OR ('$user_pseudo'!='' AND user_pseudo='$user_pseudo'))";
		if($vlmod!="") $q=$q." AND user_id!=$user_id";
		$db1->query($q);
		if($db1->next_record()){			
			$jsaction="alert('Un utilisateur avec ce login ou cet email ou ce pseudo existe déjà');";
			$fgerr=1;
		}
	}
	
	/*--> Rester sur le mode encours */
	if($vlmod!=""&&fgerr!="") $tpmod="MODIFY";
		
	/*--> Modify */
	if($vlmod!=""&&$fgerr=="") {
		$q="UPDATE env_user SET user_login='$user_login', user_lastname='$user_lastname', user_firstname='$user_firstname', user_pseudo='$user_pseudo', user_email='$user_email', user_avatar='$user_avatar' WHERE user_id=$user_id";
		$db1->query($q);
		
		// Changement de mot de passe
		if($user_password!="") {
			$q="UPDATE env_user SET user_password='".md5($user_password)."' WHERE user_id=$user_id";
			$db1->query($q);
		}
		$tpmod="";
		
		$_SESSION['user_login']			= $user_login;
		$_SESSION['user_firstname']		= $user_firstname;
		$_SESSION['user_lastname']		= $user_lastname;
		$_SESSION['user_pseudo']		= $user_pseudo;
		$_SESSION['user_avatar']		= $user_avatar;		
		
		$jsaction="recharge();";
	}
	
	if($vlann!="") $jsaction="recharge();";
	
	?>

	<script type="text/JavaScript">
		function recharge() {
			parent.document.location.reload();
		}
		<?php echo $jsaction ?>
	</script>
	
	<?
	echo "</head>";
	echo "<body>";
    
    echo "<form name='Formulaire' enctype='multipart/form-data' action='' method='post'>";
	echo "<input id='user_id' name='user_id' type='hidden' value='".$user_id."'>";
	echo "<input id='tpmod' name='tpmod' type='hidden' value=''>";
	
  
	// Entete du formulaire

    echo "<h1>PROFIL</h1>";
   	echo "<input id='vlmod' name='vlmod' class='input-rounded-button' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input id='vlann' name='vlann' class='input-rounded-button' type='submit' value='Annuler' />";
	echo "</br></br>";

	// Valeur par défaut
	$q = "SELECT * FROM env_user WHERE user_id=$user_id";
	$db1->query($q);
	if($db1->next_record()) {
		$user_login 		= $db1->f("user_login");
		$user_lastname 		= $db1->f("user_lastname");
		$user_firstname 	= $db1->f("user_firstname");
		$user_pseudo	 	= $db1->f("user_pseudo");
		$user_email 		= $db1->f("user_email");
		$user_profil_id		= $db1->f("user_profil_id");
		$user_mod			= $db1->f("user_mod");
		$user_avatar		= $db1->f("user_avatar");
	}

?>

 	<h2>Coordonnée</h2>

	<fieldset id="encadrer">
		<?
			$lbsty00="style='width:25%; border-bottom: dotted 1px;'";
			$lbsty01="type='text' size='53' style='width: 74%;'";
			$lbsty02="type='password' size='53' style='width: 74%;'";
			$lbsty03="style='width: 74%;'";
		?>

		<span id="encadrer_label" <? echo $lbsty00; ?>>ID Utilisateur*</span>
		<input 	value="<? echo $user_id; ?>"
				id="user_id_bis"
				name="user_id_bis"	
				readonly
				<? echo $lbsty01; ?>>

		<p>
		<span id="encadrer_label" <? echo $lbsty00; ?>>Login*</span>
		<input	value="<? echo $user_login; ?>"
				id="user_login" 
				name="user_login"
				<? echo $lbsty01; ?>>
		</p>

		<p>
		<span id="encadrer_label" <? echo $lbsty00; ?>>Password*</span>
		<input 	value="<? echo $user_password; ?>"	
				id="user_password"
				name="user_password"
				<? echo $lbsty02; ?>>
		</p>

		<p>
		<span id="encadrer_label" <? echo $lbsty00; ?>>Configmer Password*</span>
		<input 	value="<? echo $user_passwordbis; ?>"	
				id="user_passwordbis"
				name="user_passwordbis"
				<? echo $lbsty02; ?>>
		</p>

		<p>
		<span id="encadrer_label" <? echo $lbsty00; ?>>Nom*</span>
		<input 	value="<? echo $user_lastname; ?>"
				id="user_lastname"
				name="user_lastname"
				<? echo $lbsty01; ?>>
		</p>

		<p>
		<span id="encadrer_label" <? echo $lbsty00; ?>>Prénom*</span>
		<input	value="<? echo $user_firstname; ?>"
				id="user_firstname"
				name="user_firstname"
				<? echo $lbsty01; ?>>
		</p>

		<p>
		<span id="encadrer_label" <? echo $lbsty00; ?>>Pseudo</span>
		<input	value="<? echo $user_pseudo; ?>"
				id="user_pseudo"
				name="user_pseudo"
				<? echo $lbsty01; ?>>
		</p>
				
		<p>
		<span id="encadrer_label" <? echo $lbsty00; ?>>Email*</span>
		<input	value="<? echo $user_email; ?>"
				id="user_email"
				name="user_email"
				<? echo $lbsty01; ?>>
		</p>		
	</fieldset>

	<h2>Avatar</h2>

	<fieldset id="encadrer">
		<?
			echo "<div id='avatar' class='avatar_admin'>";
			echo "<img id='img_avatar' name='img_avatar' src='".$config["avatardir"]."/".$user_avatar."' width='90px' height='90px'></img>";
			echo "<div>";
		
			echo "<div id='btnupd'>";
			echo "<a href='#?o=1&w=700&h=530&u=jcrop.php?dirimg=".$config["avatardir"]."&lbico=user_avatar&igico=img_avatar' rel='popup_name' class='poplight' title='Ajouter un avatar'>";
			echo "<img src='images/blank.gif' width='16px' height='16px'>";
			echo "</a>";
			echo "</div>";

			echo "<div id='btndel'>";
			echo "<a onClick='document.getElementById(\"img_avatar\").src=\"".$config["avatardir"]."/anonyme.jpg\"; document.Formulaire.user_avatar.value=\"anonyme.jpg\";'>";
			echo "<img src='images/blank.gif' width='16px' height='16px'>";
			echo "</a>";
			echo "</div>";	
			
			echo "<input id='user_avatar' name='user_avatar' type='hidden' value='".$user_avatar."'>";
		?>
	</fieldset>
	
	<script type="text/javascript">
			$('#user_login').focus();
	</script>

<?

//-- FOOTER ---------------------------------------------------------------------------------------------------------------------------------

echo "</form>";
echo "</body>";
echo "</html>";
?>







	
	
