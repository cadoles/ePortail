<?

	$fgdebug = false;
		
	$dbprofil1 = new ps_db;
	$dbprofil2 = new ps_db;
	$dbprofil3 = new ps_db;
    
	$tbitem		= array();		// Liste des items associés à l'utilisateur
	$tbcat  	= array();		// Liste des catégories d'items associés à l'utilisateur
	$tbpage 	= array();		// Liste des pages associées à l'utilisateur
	$tbflux 	= array();		// Liste des flux associés à l'utilisateur
	$tbattrs	= array();		// Liste des attributs SSO de l'utilisateur
    $tbtmp		= array();		// Tableau temporaire
    
//== PROFIL ============================================================================================================================================
	
// Récupérer l'ensemble des catégories d'item associé au profil de l'utilisateur
	// 0 = categorie order
	// 1 = categorie id
	// 2 = categorie label
	
	$dbprofil1->query("SELECT application_categorie_id, application_categorie_label FROM env_application_profil, env_application, env_icon, env_application_categorie
					WHERE application_profil_profil=".$_SESSION['user_profil']."
					AND application_profil_application=application_id
					AND application_icon=icon_id
					AND application_categorie=application_categorie_id
					GROUP BY application_categorie_id
					ORDER BY application_categorie_order, application_order");
	while($dbprofil1->next_record()) {
		array_push($tbcat,array($dbprofil1->f("application_categorie_order"),$dbprofil1->f("application_categorie_id"),$dbprofil1->f("application_categorie_label")));
	}
	
	
	
// Récupérer l'ensemble des applications associé au profil de l'utilisateur
	// 0 = categorie order
	// 1 = application order
	// 2 = categorie id
	// 3 = application id
	// 4 = applcation label
	// 5 = application description
	// 6 = application url
	// 7 = icone url
	// 8 = application mode d'ouverture
	
	$dbprofil1->query("SELECT * FROM env_application_profil, env_application, env_icon, env_application_categorie
					WHERE application_profil_profil=".$_SESSION['user_profil']."
					AND application_profil_application=application_id
					AND application_icon=icon_id
					AND application_categorie=application_categorie_id
					ORDER BY application_categorie_order, application_order");
	while($dbprofil1->next_record()) {
		array_push($tbitem,array($dbprofil1->f("application_categorie_order"),$dbprofil1->f("application_order"),$dbprofil1->f("application_categorie"),$dbprofil1->f("application_id"),$dbprofil1->f("application_label"),$dbprofil1->f("application_description"),$dbprofil1->f("application_url"),$dbprofil1->f("icon_url"),$dbprofil1->f("application_open")));
	}
	
// Récupérer l'ensemble des pages associées au profil de l'utilisateur
	// 0 = page order
	// 1 = page id
	// 2 = page label
	
	$dbprofil1->query("SELECT * FROM env_panel_profil, env_panel
					WHERE panel_profil_profil=".$_SESSION['user_profil']."
					AND panel_profil_panel=panel_id
					ORDER BY panel_order");
					
	while($dbprofil1->next_record()) {
		array_push($tbpage,array($dbprofil1->f("panel_order"),$dbprofil1->f("panel_id"),$dbprofil1->f("panel_label")));
	}

// Récupérer l'ensemble des flux associés au profil de l'utilisateur
	// 0 = flux order
	// 1 = flux id
	// 2 = flux label
	// 3 = type
	// 4 = flux url
	// 5 = flux number
	
	$dbprofil1->query("SELECT * FROM env_flux_profil, env_flux
					WHERE flux_profil_profil=".$_SESSION['user_profil']."
					AND flux_profil_flux=flux_id
					ORDER BY flux_order");
					
	while($dbprofil1->next_record()) {
		array_push($tbflux,array($dbprofil1->f("flux_order"),$dbprofil1->f("flux_id"),$dbprofil1->f("flux_name"),$dbprofil1->f("flux_type"),$dbprofil1->f("flux_url"),$dbprofil1->f("flux_number")));
	}
	
//== PROFIL LDAP =======================================================================================================================================

	// Connection au LDAP
	$fgLDAP=$config['activerLDAP'];
	if($config['activerLDAP']&&$_SESSION['user_login']!="") {
		$ds = ldap_connect($config['LDAPserver'], $config['LDAPport']);
		if (!$ds) {
			$config['activerLDAP']=false;
		}

		$r=ldap_bind($ds,$config['LDAPreaderdn'],$config['LDAPreaderpw']);	
		if(!$r) {
			$config['activerLDAP']=false;
		}
	}

	if($config['activerLDAP']&&$_SESSION['user_login']!="") {
	
		if($fgdebug) echo "<h1>GESTION DES PROFIL LDAP</h1>";
		
		// Déterminer dans quelles communauté appartient l'utilisateur
		$dbprofil1->query("SELECT * FROM env_ldap_community");
		while($dbprofil1->next_record()) {
			$fgmembre=false;
			$lbfilter=str_replace("#login#",$_SESSION['user_login'],$dbprofil1->f("ldap_community_filter"));
			if($fgdebug) echo "<h2>".$dbprofil1->f("ldap_community_label")."</h2>Filtre Communauté= ".$lbfilter;			
			
			if($dbprofil1->f("ldap_community_filter")=="") {
				$fgmembre=true;
				if($fgdebug) echo "<br>appartient à la communauté = ".$dbprofil1->f("ldap_community_label");
			}
			else {
				$res = ldap_search($ds,$config['LDAPracine'],$lbfilter);

				$ldapinfo = ldap_get_entries($ds, $res);
				if($ldapinfo["count"]>0) {
					$fgmembre=true;
					if($fgdebug) echo "<br>appartient à la communauté = ".$dbprofil1->f("ldap_community_label");
				}
			}
			
			// Il fait partie de cette communauté
			if($fgmembre) {
				if($fgdebug) echo "<ul>";
				
				// Parcourt de l'ensemble des profil LDAP de la communauté
				$dbprofil2->query("SELECT * FROM env_ldap_profil WHERE ldap_profil_ldap_community=".$dbprofil1->f("ldap_community_id"));
				while($dbprofil2->next_record()) {
					$fgmembre=false;
					$lbfilter=str_replace("#login#",$_SESSION['user_login'],$dbprofil2->f("ldap_profil_filter"));
					if($fgdebug) echo "<h4>".$dbprofil2->f("ldap_profil_label")."</h4>Filtre Profil= ".$lbfilter;
					
					if($dbprofil2->f("ldap_profil_filter")=="") {
						$fgmembre=true;
						if($fgdebug) echo "<br>appartient au profil = ".$dbprofil2->f("ldap_profil_label");
					}
					else {			
						$res = ldap_search($ds,$config['LDAPracine'],$lbfilter);
						$ldapinfo = ldap_get_entries($ds, $res);
						if($ldapinfo["count"]>0) {
							$fgmembre=true;
							if($fgdebug) echo "<br>appartient au profil = ".$dbprofil2->f("ldap_profil_label");
						}
					}
					
					// Il fait partie du profil
					if($fgmembre) {
						// Récupérer l'ensemble des catégories d'item associé au profil de l'utilisateur
							// 0 = categorie order
							// 1 = categorie id
							// 2 = categorie label
				
						$dbprofil3->query("SELECT application_categorie_id, application_categorie_label FROM env_application_ldap_profil, env_application, env_icon, env_application_categorie
										WHERE application_ldap_profil_ldap_profil=".$dbprofil2->f("ldap_profil_id")."
										AND application_ldap_profil_application=application_id
										AND application_icon=icon_id
										AND application_categorie=application_categorie_id
										GROUP BY application_categorie_id
										ORDER BY application_categorie_order, application_order");
						while($dbprofil3->next_record()) {
							if($fgdebug) echo "<br>Catégorie item = ".$dbprofil3->f("application_categorie_label");
							
							$tbtmp=array($dbprofil3->f("application_categorie_order"),$dbprofil3->f("application_categorie_id"),$dbprofil3->f("application_categorie_label"));
							if(!in_array($tbtmp,$tbcat)) {
								if($fgdebug) echo " ajoutée";
								array_push($tbcat,$tbtmp);
							}
						}
						
						// Récupérer l'ensemble des applications associé au profil de l'utilisateur
							// 0 = categorie order
							// 1 = application order
							// 2 = categorie id
							// 3 = application id
							// 4 = applcation label
							// 5 = application description
							// 6 = application url
							// 7 = icone url
							// 8 = application mode d'ouverture
							
						$dbprofil3->query("SELECT * FROM env_application_ldap_profil, env_application, env_icon, env_application_categorie
										WHERE application_ldap_profil_ldap_profil=".$dbprofil2->f("ldap_profil_id")."
										AND application_ldap_profil_application=application_id
										AND application_icon=icon_id
										AND application_categorie=application_categorie_id
										ORDER BY application_categorie_order, application_order");
						while($dbprofil3->next_record()) {
							if($fgdebug) echo "<br>Item = ".$dbprofil3->f("application_label");
							
							$tbtmp=array($dbprofil3->f("application_categorie_order"),$dbprofil3->f("application_order"),$dbprofil3->f("application_categorie"),$dbprofil3->f("application_id"),$dbprofil3->f("application_label"),$dbprofil3->f("application_description"),$dbprofil3->f("application_url"),$dbprofil3->f("icon_url"),$dbprofil3->f("application_open"));
							if(!in_array($tbtmp,$tbitem)) {
								if($fgdebug) echo " ajoutée";
								array_push($tbitem,$tbtmp);
							}							
						}						
						
						// Récupérer l'ensemble des pages associées au profil de l'utilisateur
							// 0 = page order
							// 1 = page id
							// 2 = page label
							
						$dbprofil3->query("SELECT * FROM env_panel_ldap_profil, env_panel
										WHERE panel_ldap_profil_ldap_profil=".$dbprofil2->f("ldap_profil_id")."
										AND panel_ldap_profil_panel=panel_id
										ORDER BY panel_order");
										
						while($dbprofil3->next_record()) {
							if($fgdebug) echo "<br>Page = ".$dbprofil3->f("panel_label");

							$tbtmp=array($dbprofil3->f("panel_order"),$dbprofil3->f("panel_id"),$dbprofil3->f("panel_label"));
							if(!in_array($tbtmp,$tbpage)) {
								if($fgdebug) echo " ajoutée";
								array_push($tbpage,$tbtmp);
							}
						}
						
						// Récupérer l'ensemble des flux associés au profil de l'utilisateur
							// 0 = flux order
							// 1 = flux id
							// 2 = flux label
							// 3 = type
							// 4 = flux url
							// 5 = flux number
							
						$dbprofil3->query("SELECT * FROM env_flux_ldap_profil, env_flux
										WHERE flux_ldap_profil_profil=".$dbprofil2->f("ldap_profil_id")."
										AND flux_ldap_profil_flux=flux_id
										ORDER BY flux_order");
										
						while($dbprofil3->next_record()) {
							if($fgdebug) echo "<br>Flux = ".$dbprofil3->f("flux_name");
							
							$tbtmp=array($dbprofil3->f("flux_order"),$dbprofil3->f("flux_id"),$dbprofil3->f("flux_name"),$dbprofil3->f("flux_type"),$dbprofil3->f("flux_url"),$dbprofil3->f("flux_number"));
							if(!in_array($tbtmp,$tbflux)) {
								if($fgdebug) echo " ajoutée";
								array_push($tbflux,$tbtmp);
							}							
						}						
					}
				}
				
				if($fgdebug) echo "</ul>";			
			}
		}
	}


//== PROFIL SSO ========================================================================================================================================	

	if($config['activerCAS']&&$_SESSION['user_login']!="") {
		if($fgdebug) echo "<hr><h1>GESTION DES PROFIL SSO</h1>";
		
		// Parcours de l'ensemble des informations détaillées de l'utilisateur
		if($fgdebug) echo "<h2>LISTE DES ATTRIBUTS ASSOCIES</h2>";
		$user = EolephpCAS::getUser();
		$details = EolephpCAS::getDetails();
		$user_datas = $details;	
		
		if($fgdebug) echo "<ul>";
		
		foreach ($user_datas as $tbuser) {
			foreach ($tbuser as $key => $attribut) {
				foreach($attribut as $u) {
					if($fgdebug) echo "<br>$key = $u";
					
					// On recherche s'il exite un attribut parametré pour cette valeur
					$dbprofil1->query("SELECT * FROM env_sso_attribut WHERE sso_attribut_name='".addslashes($key)."' AND sso_attribut_value='".addslashes($u)."'");

					// Si oui on sauvegarde l'id de l'attribut
					if($dbprofil1->next_record()) {
						array_push($tbattrs,$dbprofil1->f('sso_attribut_id'));
						if($fgdebug) echo "<span style='color: red;font_width: strong;'> >> OK</span> >> ".$dbprofil1->f('sso_attribut_id');
					}
				}
			}
		}

		if($fgdebug) echo "</ul>";
		
		// Déterminer dans quelles communauté appartient l'utilisateur
		$lstattrs=implode(",",$tbattrs);
		$dbprofil1->query("SELECT * FROM env_sso_community");
		while($dbprofil1->next_record()) {
			$fgmembre=false;
			if($fgdebug) echo "<h2>".$dbprofil1->f("sso_community_label")."</h2>";
			
			if($dbprofil1->f("sso_community_id")==-1) {
				$fgmembre=true;
				if($fgdebug) echo "appartient à la communauté = ".$dbprofil1->f("sso_community_label");
			}
			else {
				// Appartient à la communauté si au moins un id dans la liste des attributs
				$dbprofil2->query("SELECT * FROM env_sso_community_attribut
								   WHERE sso_community_attribut_community=".$dbprofil1->f("sso_community_id")."
								   AND sso_community_attribut_attribut in (".$lstattrs.")");

				if($dbprofil2->next_record()) {
					$fgmembre=true;
					if($fgdebug) echo "appartient à la communauté = ".$dbprofil1->f("sso_community_label");
				}
			}
			
			// Il fait partie de cette communauté
			if($fgmembre) {		
				// Appartient au profil si au moins un id dans la liste des attributs de cette communauté
				$dbprofil2->query("SELECT * FROM env_sso_profil, env_sso_profil_attribut
							       WHERE sso_profil_community=".$dbprofil1->f("sso_community_id")."
							       AND sso_profil_id = sso_profil_attribut_profil
							       AND sso_profil_attribut_attribut IN ('".$lstattrs."')");
										       
				// Il fait partie du profil
				if($dbprofil2->next_record()) {
					if($fgdebug) echo "<h4>".$dbprofil2->f("sso_profil_label")."</h4>";
					
					// Récupérer l'ensemble des catégories d'item associé au profil de l'utilisateur
						// 0 = categorie order
						// 1 = categorie id
						// 2 = categorie label
			
					$dbprofil3->query("SELECT application_categorie_id, application_categorie_label FROM env_application_sso_profil, env_application, env_icon, env_application_categorie
									WHERE application_sso_profil_sso_profil=".$dbprofil2->f("sso_profil_id")."
									AND application_sso_profil_application=application_id
									AND application_icon=icon_id
									AND application_categorie=application_categorie_id
									GROUP BY application_categorie_id
									ORDER BY application_categorie_order, application_order");
					while($dbprofil3->next_record()) {
						if($fgdebug) echo "<br>Catégorie item = ".$dbprofil3->f("application_categorie_label");
						
						$tbtmp=array($dbprofil3->f("application_categorie_order"),$dbprofil3->f("application_categorie_id"),$dbprofil3->f("application_categorie_label"));
						if(!in_array($tbtmp,$tbcat)) {
							if($fgdebug) echo " ajoutée";
							array_push($tbcat,$tbtmp);
						}
					}
					
					// Récupérer l'ensemble des applications associé au profil de l'utilisateur
						// 0 = categorie order
						// 1 = application order
						// 2 = categorie id
						// 3 = application id
						// 4 = applcation label
						// 5 = application description
						// 6 = application url
						// 7 = icone url
						// 8 = application mode d'ouverture
						
					$dbprofil3->query("SELECT * FROM env_application_sso_profil, env_application, env_icon, env_application_categorie
									WHERE application_sso_profil_sso_profil=".$dbprofil2->f("sso_profil_id")."
									AND application_sso_profil_application=application_id
									AND application_icon=icon_id
									AND application_categorie=application_categorie_id
									ORDER BY application_categorie_order, application_order");
					while($dbprofil3->next_record()) {
						if($fgdebug) echo "<br>Item = ".$dbprofil3->f("application_label");
						
						$tbtmp=array($dbprofil3->f("application_categorie_order"),$dbprofil3->f("application_order"),$dbprofil3->f("application_categorie"),$dbprofil3->f("application_id"),$dbprofil3->f("application_label"),$dbprofil3->f("application_description"),$dbprofil3->f("application_url"),$dbprofil3->f("icon_url"),$dbprofil3->f("application_open"));
						if(!in_array($tbtmp,$tbitem)) {
							if($fgdebug) echo " ajoutée";
							array_push($tbitem,$tbtmp);
						}							
					}						
					
					// Récupérer l'ensemble des pages associées au profil de l'utilisateur
						// 0 = page order
						// 1 = page id
						// 2 = page label
						
					$dbprofil3->query("SELECT * FROM env_panel_sso_profil, env_panel
									WHERE panel_sso_profil_sso_profil=".$dbprofil2->f("sso_profil_id")."
									AND panel_sso_profil_panel=panel_id
									ORDER BY panel_order");
									
					while($dbprofil3->next_record()) {
						if($fgdebug) echo "<br>Page = ".$dbprofil3->f("panel_label");

						$tbtmp=array($dbprofil3->f("panel_order"),$dbprofil3->f("panel_id"),$dbprofil3->f("panel_label"));
						if(!in_array($tbtmp,$tbpage)) {
							if($fgdebug) echo " ajoutée";
							array_push($tbpage,$tbtmp);
						}
					}
					
					// Récupérer l'ensemble des flux associés au profil de l'utilisateur
						// 0 = flux order
						// 1 = flux id
						// 2 = flux label
						// 3 = type
						// 4 = flux url
						// 5 = flux number

						
					$dbprofil3->query("SELECT * FROM env_flux_sso_profil, env_flux
									WHERE flux_sso_profil_profil=".$dbprofil2->f("sso_profil_id")."
									AND flux_sso_profil_flux=flux_id
									ORDER BY flux_order");
									
					while($dbprofil3->next_record()) {
						if($fgdebug) echo "<br>Flux = ".$dbprofil3->f("flux_name");
						
						$tbtmp=array($dbprofil3->f("flux_order"),$dbprofil3->f("flux_id"),$dbprofil3->f("flux_name"),$dbprofil3->f("flux_type"),$dbprofil3->f("flux_url"),$dbprofil3->f("flux_number"));
						if(!in_array($tbtmp,$tbflux)) {
							if($fgdebug) echo " ajoutée";
							array_push($tbflux,$tbtmp);
						}							
					}	
				}	
			}
		}
	}
	
	
	
	
	
	// Trie des Tableaux
	asort($tbitem);
	asort($tbcat);
	asort($tbpage);
	asort($tbflux);

	if($fgdebug) die();	

?>

