<?
	require_once 'CAS-1.3.1/eoleCAS.php';
	require_once 'configCAS/cas.inc.php';
	
	eolephpCAS::client(CAS_VERSION_2_0, __CAS_SERVER, __CAS_PORT, '', true);		

	if (__CAS_LOGOUT){
		if (method_exists(eolephpCAS, 'eolelogoutRequests')){
			eolephpCAS::eolelogoutRequests(false);
		}
	}

	if (__CAS_VALIDER_CA) {
		EolephpCAS::setCasServerCACert(__CAS_CA_LOCATION); // verification par rapport a la CA
	} else {
		if (method_exists("EolephpCAS", "setNoCasServerValidation")){
			EolephpCAS::setNoCasServerValidation();
		}
	}
	
	if(EolephpCAS::checkAuthentication()) {
		$user = EolephpCAS::getUser();
		$dbcas = new ps_db;

		$_SESSION['user_login'] = $user;
		
		$q="SELECT * FROM env_user WHERE user_login='".$_SESSION['user_login'] ."'";
		$dbcas->query($q);
		if($dbcas->next_record()){
			$_SESSION['user_id']			=$dbcas->f("user_id");
			$_SESSION['user_login']			=$dbcas->f("user_login");
			$_SESSION['user_firstname']		=$dbcas->f("user_firstname");
			$_SESSION['user_lastname']		=$dbcas->f("user_lastname");
			$_SESSION['user_pseudo']		=$dbcas->f("user_pseudo");
			$_SESSION['user_avatar']		=$dbcas->f("user_avatar");
			$_SESSION['user_mod']			=$dbcas->f("user_mod");
			$_SESSION['user_profil'] 		=$dbcas->f("user_profil");
		}
		else {
			include("autocreate.php");
		}
	}
	else {
		$_SESSION['user_id']			= "";
	}

?>
