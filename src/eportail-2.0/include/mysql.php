<?php

class ps_DB {
  
  var $lid = 0;				// Link ID for database connection
  var $qid = 0;      		// Query ID for current query
  var $row;      			// Current row in query result set
  var $record = array();  	// Current row record data
  var $error = "";    		// Error Message
  var $errno = "";    		// Error Number
  var $dataname;		// Base
 
  
  // Database
  function dbname($dbdata) {
	$this->dataname=$dbdata;
  }
  
  // Connects to DB and returns DB lid
  function connect() {
    global $config; 
	global $dataname; 
	
    if ($this->lid == 0) {
		$this->lid = mysql_pconnect($config['dbhost'],$config['dblogin'],$config['dbpassword']);
		
		if (!$this->lid) {
			echo "Impossible de se connecter au host";			
			$this->halt("Impossible de se connecter au host.");
		}
    }


	if($this->dataname=="") {
		if (!@mysql_select_db($config['dbname'],$this->lid)) {
			echo "Impossible de se connecter à la base de données";
			$this->halt("Impossible de se connecter à la base de données");
			return 0;
		}
	}
	else {
		if (!@mysql_select_db($this->dataname,$this->lid)) {

			echo "Impossible de se connecter à la base de données";
			$this->halt("Impossible de se connecter à la base de données");
			return 0;
		}			
	}
		    
	mysql_query ("set character_set_client='utf8'"); 
	mysql_query ("set character_set_results='utf8'"); 
	mysql_query ("set collation_connection='utf8_general_ci'");
	    
    return $this->lid;
  }
  
  
  // Runs query and sets up the query id for the class.
  // PUBLIC
  function query($q) {
    
    if (empty($q))
    return 0;
    
    if (!$this->connect()) {
      return 0;
    }

    if ($this->qid) {
      @mysql_free_result($this->qid);
      $this->qid = 0;
    }

    $this->qid = @mysql_query($q, $this->lid);
    $this->row   = 0;
    $this->errno = mysql_errno();
    $this->error = mysql_error();
    if (!$this->qid) {
      $this->halt("Requête invalide: ".$q);
    }

    return $this->qid;
  }


  // Return next record in result set
  // PUBLIC
  function next_record() {

    if (!$this->qid) {
      $this->halt("Enregistrement suivant demandé sans requête.");
      return 0;
    }

	
    $this->record = @mysql_fetch_array($this->qid);
    $this->row   += 1;
    $this->errno  = mysql_errno();
    $this->error  = mysql_error();

    $stat = is_array($this->record);
    return $stat;
  }


  // Field Value
  // PUBLIC
  function f($field_name) {
	  return stripslashes($this->record[$field_name]);
  }

  // Selective field value
  // PUBLIC
  function sf($field_name) {
    global $vars, $default;

    if ($vars["error"] and $vars["$field_name"]) {
      return stripslashes($vars["$field_name"]);
      } elseif ($default["$field_name"]) {
      return stripslashes($default["$field_name"]);
      } else {
      return stripslashes($this->record[$field_name]);
    }
  }

  // Print field
  // PUBLIC
  function p($field_name) {
    print stripslashes($this->record[$field_name]);
  }

  // Selective print field
  // PUBLIC
  function sp($field_name) {
    global $vars, $default;

    if ($vars["error"] and $vars["$field_name"]) {
      print stripslashes($vars["$field_name"]);
      } elseif ($default["$field_name"]) {
      print stripslashes($default["$field_name"]);
      } else {
      print stripslashes($this->record[$field_name]);
    }
  }

  // Returns the number of rows in query
  function num_rows() {

    if ($this->lid) {
      return @mysql_numrows($this->qid);
    }
    else {
      return 0;
    }
  }

  // Halt and display error message
  // PRIVATE
  function halt($msg) {
    $this->error = @mysql_error($this->lid);
    $this->errno = @mysql_errno($this->lid);
     if ($fp1 = @fopen("error.txt","a+")){
        $string1  = date("d-m-Y H:i")." Erreur de BD   :".$msg."\n";
        $string1 .= date("d-m-Y H:i")." Erreur MySQL :".$this->errno." -  ".$this->error.".\n";
        fputs($fp1,$string1);
        }; //fsi
   // printf("</td></tr></table><b>Erreur de base de données:</b> %s<br>\n", $msg);
    //printf("<b>Erreur MySQL</b>: %s (%s)<br></font>\n",
    //$this->errno,
    //$this->error);
    
    //exit;
    
  }
  
}
?>
