<?

function delPanel($idpan) {
	$dbdel=new ps_db;
	
	// Suppression des widget du panel
	$q="DELETE FROM env_panel_widget WHERE panel_widget_panel=$idpan";
	$dbdel->query($q);
	
	// Suppression du lien panel profil
	$q="DELETE FROM env_panel_profil WHERE panel_profil_panel=$idpan";
	$dbdel->query($q);

	// Suppression des profil flux sso
	$q="DELETE FROM env_panel_sso_profil WHERE panel_sso_profil_panel=$idapp";
	$dbdel->query($q);

	// Suppression des profil flux ldap
	$q="DELETE FROM env_panel_ldap_profil WHERE panel_ldap_profil_panel=$idapp";
	$dbdel->query($q);
	
	// Suppression du panel en lui meme
	$q="DELETE FROM env_panel WHERE panel_id=$idpan";
	$dbdel->query($q);
}

function delApplication($idapp) {
	$dbdel=new ps_db;

	// Suppression des profil flux
	$q="DELETE FROM env_application_profil WHERE application_profil_application=$idapp";
	$dbdel->query($q);
	
	// Suppression des profil flux sso
	$q="DELETE FROM env_application_sso_profil WHERE application_sso_profil_application=$idapp";
	$dbdel->query($q);

	// Suppression des profil flux ldap
	$q="DELETE FROM env_application_ldap_profil WHERE application_ldap_profil_application=$idapp";
	$dbdel->query($q);
	
	// Suppression de l'application en question
	$q="DELETE FROM env_application WHERE application_id=$idapp";
	$dbdel->query($q);
}

function delFlux($idflu) {
	$dbdel=new ps_db;
	
	// Suppression des messages du flux
	$q="DELETE FROM env_fluxmsg WHERE fluxmsg_flux=$idflu";
	$dbdel->query($q);

	// Suppression des profil flux
	$q="DELETE FROM env_flux_profil WHERE flux_profil_flux=$idflu";
	$dbdel->query($q);
	
	// Suppression des profil flux sso
	$q="DELETE FROM env_flux_sso_profil WHERE flux_sso_profil_flux=$idflu";
	$dbdel->query($q);

	// Suppression des profil flux ldap
	$q="DELETE FROM env_flux_ldap_profil WHERE flux_ldap_profil_flux=$idflu";
	$dbdel->query($q);
	
	// Suppression du flux en question
	$q="DELETE FROM env_flux WHERE flux_id=$idflu";
	$dbdel->query($q);
}

function delPanelWidget($idwid) {
	$dbdel=new ps_db;

	// Suppression de la panel widget carrousel
	$q="DELETE FROM env_panel_widget_carousel WHERE panel_widget_carousel_widget=$idwid";
	$dbdel->query($q);

	// Suppression de la panel widget bookmark
	$q="DELETE FROM env_panel_widget_bookmark WHERE panel_widget_bookmark_widget=$idwid";
	$dbdel->query($q);
			
	// Suppression de la panel widget associée
	$q="DELETE FROM env_panel_widget WHERE panel_widget_id=$idwid";
	$dbdel->query($q);
}

function delWidget($idwid) {
	$dbdel=new ps_db;
	
	// Suppression des panel widget associées
	$q="SELECT panel_widget_id FROM env_panel_widget WHERE panel_widget_widget=$idwid";
	$dbdel->query($q);
	while($dbdel->next_record()) {
		delPanelWidget($dbdel->f("panel_widget_id"));
	}
	
	// Suppression du widget en lui meme
	$q="DELETE FROM env_widget WHERE widget_id=$idwid";
	$dbdel->query($q);
}

function delLdapProfil($idprf) {
	$dbdel=new ps_db;
	
	// Suppression de page profil ldap
	$q="DELETE FROM env_panel_ldap_profil WHERE panel_ldap_profil_ldap_profil=$idprf";
	$dbdel->query($q);
	
	// Suppression des application profil ldap
	$q="DELETE FROM env_application_ldap_profil WHERE application_ldap_profil_ldap_profil=$idprf";
	$dbdel->query($q);

	// Suppression des flux profil ldap
	$q="DELETE FROM env_flux_ldap_profil WHERE flux_ldap_profil_profil=$idprf";
	$dbdel->query($q);
	
	// Suppression du profils en lui même
	$q="DELETE FROM env_ldap_profil WHERE ldap_profil_id=$idprf";
	$dbdel->query($q);
	
}

function delLdapCommunity($idcom) {
	$dbdel=new ps_db;

	// Suppression des profils ldap associés
	$q="SELECT * FROM env_ldap_profil WHERE ldap_profil_ldap_community=$idcom";
	$dbdel->query($q);
	while($dbdel->next_record()) {
		delLdapProfil($dbdel->f("ldap_profil_id"));
	}	
	
	// Suppression de la communauté en elle même
	$q="DELETE FROM env_ldap_community WHERE ldap_community_id=$idcom";
	$dbdel->query($q);	
}

function delSsoProfil($idprf) {
	$dbdel=new ps_db;
	
	// Suppression de page profil sso
	$q="DELETE FROM env_panel_sso_profil WHERE panel_sso_profil_sso_profil=$idprf";
	$dbdel->query($q);
	
	// Suppression des application profil sso
	$q="DELETE FROM env_application_sso_profil WHERE application_sso_profil_sso_profil=$idprf";
	$dbdel->query($q);

	// Suppression des flux profil ldap
	$q="DELETE FROM env_flux_sso_profil WHERE flux_sso_profil_profil=$idprf";
	$dbdel->query($q);
	
	// Suppression du profils en lui même
	$q="DELETE FROM env_sso_profil WHERE sso_profil_id=$idprf";
	$dbdel->query($q);
	
}

function delSsoCommunity($idcom) {
	$dbdel=new ps_db;

	// Suppression des profils ldap associés
	$q="SELECT * FROM env_sso_profil WHERE sso_profil_community=$idcom";
	$dbdel->query($q);
	while($dbdel->next_record()) {
		delSsoProfil($dbdel->f("sso_profil_id"));
	}	
	
	// Suppression de la communauté en elle même
	$q="DELETE FROM env_sso_community WHERE sso_community_id=$idcom";
	$dbdel->query($q);	
}

function delGroup($idgrp) {
	$dbdel=new ps_db;

	// Suppression du lien utilisateur groupe
	$q="DELETE FROM env_groupuser WHERE groupuser_group_id=$idgrp";
	$dbdel->query($q);
	
	// Suppression du group en lui meme
	$q="DELETE FROM env_group WHERE group_id=$idgrp";
	$dbdel->query($q);
}

function delUser($idusr) {
	$dbdel=new ps_db;
	
	// Suppression des panel de l'utilisateur
	$q="SELECT panel_id FROM env_panel WHERE panel_user=$idusr";
	$dbdel->query($q);
	while($dbdel->next_record()) {
		delPanel($dbdel->f("panel_id"));
	}

	// Suppression de l'utilisateur en lui meme
	$q="DELETE FROM env_user WHERE user_id=$idusr";
	$dbdel->query($q);
}


?>
