<?
$config=Array();

/* Database */
$dbpassword="oengaixe";
$config['dbpassword']					= $dbpassword;
$config['dbhost']						= "192.0.2.50";
$config['dblogin']						= "eportail";
$config['dbname'] 						= "eportail";

/* Serveur */
$config['host'] 						= "amonecole23.ac-arno.fr";
$config['urlbase']						= "https://amonecole23.ac-arno.fr/eportail";
$config['localdirectory']				= "/var/www/html/eportail";

/* URL Pages  */
$config['urlaccueil']					= "envole.php";
$config['urldossier']					= "/owncloud";
$config['urlemail']						= "/roundcube";
$config['urlrecherche']					= "/bergamote";

/* CAS */
$config['activerCAS']					= false;

/* Variables Globales */
$config['themedir']						= "";				// Nom du theme si vide theme par défaut
$config['consoleadmin']					= true;				// Ouverture de la console d'administration
$config['envtitle']						= "amonecole23";	// Titre du site
$config['locallogo']   					= '';				// Logo personnalisé
$config['localheader'] 					= '';				// Bannière personnalisé

if(file_exists($config['localdirectory']."/local/config/config.php")) {
	include($repository."local/config/config.php");
}
?>
