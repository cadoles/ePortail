<?
	require_once 'CAS-1.3.1/eoleCAS.php';
	require_once 'configCAS/cas.inc.php';

	eolephpCAS::client(__CAS_VERSION, __CAS_SERVER, __CAS_PORT, '', false);

	if (__CAS_VALIDER_CA) {
		eolephpCAS::setCasServerCACert(__CAS_CA_LOCATION);
	}
	else {
		if (method_exists(eolephpCAS, 'setNoCasServerValidation')){
			eolephpCAS::setNoCasServerValidation();
		}
	}

	eolephpCAS::logout(array("url"=>$config['urlbase']."/index.php"));
?>
