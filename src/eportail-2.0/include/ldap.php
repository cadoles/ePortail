<?

/* LDAP */
$config['activerLDAPModif']				= true;		/* Permettre la modification des paramétres LDAP 	*/
$config['activerLDAP']					= false;	/* Activer le mode LDAP								*/
$config['activerProfilLDAP']			= false;	/* Gestion des Profil LDAP							*/

// Adresse du Serveur LDAP
$config['LDAPserver']					= "";

$config['LDAPport'] 					= "";
$config['LDAPreaderdn'] 				= "";
$config['LDAPreaderpw']					= "";
$config['LDAPracine'] 					= "";
$config['LDAPfilteruser'] 				= "";
$config['LDAPfirstname'] 				= "";
$config['LDAPlastname'] 				= "";
$config['LDAPemail'] 					= "";

if($config['activerLDAPModif']&&file_exists($config['localdirectory']."/local/config/ldap.php")) {
	include($repository."local/config/ldap.php");
}

?>
