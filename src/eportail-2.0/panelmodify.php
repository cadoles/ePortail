<?
	include_once("include/include.php");
	include("header.php");	
	
	$db1=new ps_db;
	
	$fgedt=$_POST['fgedt'];
	$fgweb=$_POST['fgweb'];
	$fgwid=$_POST['fgwid'];
	$lbpan=$_POST['lbpan'];
	$lburl=$_POST['lburl'];
	$tpwid=$_POST['tpwid'];
	$tpmod=$_POST['tpmod'];
	$vlmod=$_POST['vlmod'];
	$id   =$_POST['id'];

	if($id=="") $id=$_GET["id"];
	if($tpmod=="") $tpmod=$_GET["mode"];
	
	// Permissions = si non utiisateur exist
	if($_SESSION['user_id']=="") die();
	
	// Mode Modification
	if($tpmod=="modpanel"){
		// Permissions = si non non propriéataire de la page exist
		$q="SELECT panel_user FROM env_panel WHERE panel_id=".$id;
		$db1->query($q);
		if($db1->next_record()) {
			$idpro=$db1->f('panel_user');
		}
		if($idpro!=$_SESSION['user_id']&&$_SESSION['user_profil']!=1) {
			die();
		}
		
		// Modification
		if($vlmod!="") {
			// Gérer les erreurs
			if($lbpan==""&&$lburl=="") {
				$jsaction="alert('Vous devez renseigner l\'ensemble des informations');";
			}
			else {
				$q="UPDATE env_panel SET panel_label='".addslashes($lbpan)."', panel_url='$lburl' WHERE panel_id=$id";
				$db1->query($q);
				
				// Action javascript Ajout du panel directement + fermeture de la popup
				$jsaction="recharge(".$id.")";				
			}
		}
	}
	
	// Mode Création
	else {
		// Création d'une page Editeur
		if($fgedt!="") {
			// Gérer les erreurs
			if($lbpan=="") {
				$jsaction="alert('Vous devez renseigner l\'ensemble des informations');";
			}
			else {		
				// Recherche du dernier numéro d'ordre de panel de l'utilisateur 
				$q="SELECT panel_order FROM env_panel WHERE panel_user=".$_SESSION['user_id']." ORDER BY panel_order DESC";
				$db1->query($q);
				$order=1;
				if($db1->next_record()) {
					$order=$db1->f('panel_order')+1;
				}
			
				// Création du panel
				$q="INSERT INTO env_panel (panel_label, panel_url, panel_user, panel_order, panel_type, panel_html) VALUES('".$lbpan."','editeur.php',".$_SESSION['user_id'].",".$order.",3,'<center><br><br><br>Modifier votre page pour insérer votre texte</center>')";
				$db1->query($q);
				
				// Récupération du panel généré
				$q="SELECT panel_id FROM env_panel WHERE panel_user=".$_SESSION['user_id']." AND panel_order=".$order;
				$db1->query($q);
				if($db1->next_record()) {
					$idpan=$db1->f('panel_id');
				}
				
				// Action javascript Ajout du panel directement + fermeture de la popup
				$jsaction="recharge(".$idpan.")";
			}
		}
		
		// Création d'une page URL
		if($fgweb!="") {
			// Gérer les erreurs
			if($lbpan==""||$lburl=="") {
				$jsaction="alert('Vous devez renseigner l\'ensemble des informations');";
			}
			else {
				// Recherche du dernier numéro d'ordre de panel de l'utilisateur 
				$q="SELECT panel_order FROM env_panel WHERE panel_user=".$_SESSION['user_id']." ORDER BY panel_order DESC";
				$db1->query($q);
				$order=1;
				if($db1->next_record()) {
					$order=$db1->f('panel_order')+1;
				}
			
				// Création du panel
				$q="INSERT INTO env_panel (panel_label, panel_url, panel_user, panel_order, panel_type) VALUES('".addslashes($lbpan)."','".$lburl."',".$_SESSION['user_id'].",".$order.",1)";
				$db1->query($q);
				
				// Récupération du panel généré
				$q="SELECT panel_id FROM env_panel WHERE panel_user=".$_SESSION['user_id']." AND panel_order=".$order;
				$db1->query($q);
				if($db1->next_record()) {
					$idpan=$db1->f('panel_id');
				}
				
				// Action javascript Ajout du panel directement + fermeture de la popup
				$jsaction="recharge(".$idpan.")";
			}
		}
		
		// Création d'une page Widget
		if($fgwid!="") {
			// Gérer les erreurs
			if($lbpan=="") {
				$jsaction="alert('Vous devez renseigner l\'ensemble des informations');";
			}
			else {			
				// Recherche du dernier numéro d'ordre de panel de l'utilisateur 
				$q="SELECT panel_order FROM env_panel WHERE panel_user=".$_SESSION['user_id']." ORDER BY panel_order DESC";
				$db1->query($q);
				$order=1;
				if($db1->next_record()) {
					$order=$db1->f('panel_order')+1;
				}
			
				// Création du panel
				$q="INSERT INTO env_panel (panel_label, panel_url, panel_user, panel_order, panel_type, panel_template) VALUES('".$lbpan."','widget.php',".$_SESSION['user_id'].",".$order.",4,1)";
				$db1->query($q);
				
				// Récupération du panel généré
				$q="SELECT panel_id FROM env_panel WHERE panel_user=".$_SESSION['user_id']." AND panel_order=".$order;
				$db1->query($q);
				if($db1->next_record()) {
					$idpan=$db1->f('panel_id');
				}
				
				// Action javascript Ajout du panel directement + fermeture de la popup
				$jsaction="recharge(".$idpan.")";
			}
		}
	}	
?>

<script type="text/JavaScript">
	function closemodal() {
		window.parent.closemodal();	
	}
	
	function recharge(idpan) {
		parent.parent.document.location.href='index.php?action=page&id='+idpan;
	}
	<?php echo $jsaction ?>
</script>

<div id='page-wrapper'>
<div class='container-fluid'>
	
<form id='formulaire' class='form-horizontal' role='form' method='post' enctype='multipart/form-data'>
<input id='id' name='id' type='hidden' value='<? echo $id; ?>'>
<input id='tpmod' name='tpmod' type='hidden' value='<? echo $tpmod; ?>'>
	
<!-- MODIFICATION ------------------------------------------------------------------------------------------------------------------------------------>
<?

	if($tpmod=="modpanel"){
		$q="SELECT * FROM env_panel WHERE panel_id=$id";
		$db1->query($q);
		if($db1->next_record()) {
			if($lbpan=="") $lbpan=$db1->f("panel_label");
			if($lburl=="") $lburl=$db1->f("panel_url");
			$tpwid=$db1->f("panel_type");
		}

?>
<input id='tpwid' name='tpwid' type='hidden' value='<? echo $tpwid; ?>'>

<div class='form-group'>
<div class='col-sm-12'>
<input id='vlmod' name='vlmod' class='btn btn-primary' type='submit' value='Valider' />
&nbsp;
<input class='btn btn-primary' type='submit' onClick='closemodal();' value='Annuler' />
</div>
</div>


<div class="form-group">
	<label for="lbpan" class="col control-label">Nom*</label>
	<div class="col"><input name="lbpan" id="lbpan" type="titre" class="form-control" placeholder="Nom" value="<? echo $lbpan; ?>"></div>
</div>	
		
<div class="form-group" style="<? if($tpwid!=1) echo "display:none"?>">
	<label for="lbpan" class="col control-label">URL*</label>
	<div class="col"><input name="lburl" id="lburl" type="titre" class="form-control" placeholder="Url*" value="<? echo $lburl; ?>"></div>
</div>	

<!-- CREATION ---------------------------------------------------------------------------------------------------------------------------------------->
<? } else { ?>
<div class="form-group">
	<label for="lbpan" class="col control-label">Donnez un nom à votre nouvel onglet*</label>
	<div class="col"><input name="lbpan" id="lbpan" type="titre" class="form-control" placeholder="Nom" value="<? echo $lbpan; ?>"></div>
</div>

<div style='float:left; width:33%'>
<div class="thumbnail" style="margin-top:10px; border:none;">
<img class="imgreflect" alt="editeur" src="local/images/icon/00-editeur.jpg" style="border-radius:10px;" />
<div class="caption">
<h4 style="text-align:center">Page Editeur</h4>
<p style="text-align:center">
<input id='fgval' name='fgedt' class='btn btn-primary' type='submit' value='Choisir' />
</p>
</div>
</div>
</div>

<div style='float:left; width:33%'>
<div class="thumbnail" style="margin-top:10px; border:none;">
<img class="imgreflect" alt="url" src="local/images/icon/00-url.jpg" style="border-radius:10px;" />
<div class="caption">
<h4 style="text-align:center">Page Web</h4>
<p style="text-align:center">
<input id='fgweb' name='fgweb' class='btn btn-primary' type='submit' value='Choisir' />
<div class="form-group">
	<center><label for="lburl" class="col-xs-12 control-label">Saisir l'adresse du site*</label></center>
	<div class="col-xs-12"><input name="lburl" id="lburl" type="titre" class="form-control" placeholder="Url" value="<? echo $lburl; ?>"></div>
</div>
</p>
</div>
</div>
</div>

<div style='float:left; width:33%'>
<div class="thumbnail" style="margin-top:10px; border:none;">
<img class="imgreflect" alt="widget" src="local/images/icon/00-widget.jpg" style="border-radius:10px;" />
<div class="caption">
<h4 style="text-align:center">Page Widgets</h4>
<p style="text-align:center">
<input id='fgwid' name='fgwid' class='btn btn-primary' type='submit' value='Choisir' />
</p>
</div>
</div>
</div>



<? } ?>

<script>
	$(document).ready(function() {
		$('#lbpan').focus();
	});
	
	$('form').bind("keypress", function(e) {
	  if (e.keyCode == 13) {               
		e.preventDefault();
		return false;
	  }
	});	
</script>

</form>
</div>
</div>

<!-- FOOTER --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<?
	include($repository."footer.php");
?>


