<?
	include("include/include.php");
	include("header.php");
	
	// A FAIRE Bloquer la page au invité
	
	
	// Récupération des paramétres
	$directory		= $_GET["directory"];	// cible du upload
	$typeupload		= $_GET["typeupload"];	// image | all 
	$multiupload	= $_GET["multiupload"];	// true | false 
	$resize			= $_GET["resize"];		// true | false uniquement si typeupload = image
	$size			= $_GET["size"];		// taille en px pour le rezise d'une image
	$rename			= $_GET["rename"];		// true | false renommage des fichiers uploadés
	$prefix			= $_GET["prefix"];		// prefix des fichiers renommées 
	
	$refresh		= $_GET["refresh"];		// Rafraichir le père fermeture
	$query			= $_GET["query"];		// query à executer sur le pere vide | insertappico | updateappico
	$idquery		= $_GET["idquery"];		// identifiant unique associé à la query
	
	$repository		= $_GET["repository"];	// repository du père
	$idimage		= $_GET["idimage"];		// id html de l'image à afficher
	$idinput		= $_GET["idinput"];		// id html de l'input de stockage
	
	// Valeur par défaut
	if($directory=="")		$directory="local/upload";
	if($typeupload=="")		$typeupload="all";
	if($multiupload=="")	$multiupload=false;
	if($resize=="")			$resize=false;
	if($size=="")			$size="";
	if($rename=="")			$rename=true;
	if($prefix=="")			$prefix="file_";
	if($query=="")			$query="";
	if($idquery=="")		$idquery="";
	
	// Definition de l'url action
	$action = "lib/dropzone/upload.php?directory=$directory&typeupload=$typeupload&multiupload=$multiupload&resize=$resize&size=$size&rename=$rename&prefix=$prefix&query=$query&idquery=$idquery";
?>
<link href="lib/dropzone/css/dropzone.css" type="text/css" rel="stylesheet" />

<form action="<? echo $action; ?>" class="dropzone" id="MyDropZone"></form>

<? include("footer.php"); ?>
	
<script src="lib/dropzone/dropzone.min.js"></script>

<script>
Dropzone.options.MyDropZone = {
	<? if(!$multiupload) { ?>
	maxFiles: 1,
	<? } ?>
	
	<? if($typeupload=="image") { ?>
	acceptedMimeTypes: 'image/*',
	<? } ?>
	
	success: function(file, response){
		<? if($refresh) { ?>
		window.parent.recharge();
		<? } ?>

		<? if($idimage!="") { ?>
		parent.document.getElementById("<? echo $idimage; ?>").src="<? echo $repository.$directory; ?>/"+response['name'];
		<? } ?>

		<? if($idinput!="") { ?>
		parent.document.getElementById("<? echo $idinput ?>").value=response['name'];
		<? } ?>
			
		window.parent.closemodal();	
	}
};

</script>
