<?
	include("include/include.php");
	include("include/delete.php");
	include("header.php");

	$action		= $_GET["action"];
	$id			= $_GET["id"];
	
	if($action=="") $action="accueil";
	
	$srchterm = $_POST["srchterm"];
	if($srchterm!="")
		$action = "recherche";
	
	$dbpage = new ps_db;
?>

<script src='lib/jquery-ui/ui/jquery-ui.js'></script>

<script type="text/javascript">  
	$(document).ready(function () {  
		$(".mypanel").sortable({
			tolerance: 'pointer',
			revert: 'invalid',
			//handle: 'li', 
			opacity: 0.6,
			//placeholder: 'placeholder',
			forceHelperSize: true,
			
			update: function ()
			{
				var a = $(this);

				$('li', $(".mypanel")).each(function(index, elem) {
                     var $listItem = $(elem);
                     var newIndex = $listItem.index();
                     var id = $(elem).attr('rel');
                     /*
                     console.log("TAG   = "+$(elem).prop('tagName'));
                     console.log("INDEX = "+index);
                     console.log("ID = "+id);
                     */
                     $("#info").load("process/process-userpanel-order.php?id="+id+"&order="+newIndex);
				});
			} 			
		});			
    
		$(".column").disableSelection();					
	});  
</script>

<body id="bodyindex">
  
<!-- HEADER ------------------------------------------------------------------------------------------------------------------------------------------>

		


<!-- BARRE DE NAVIGATION PRINCIPALE------------------------------------------------------------------------------------------------------------------->  
    <nav id="mynavbar" class="navbar navbar-inverse  navbar-fixed-top" role="navigation">
		<div id="myheader" class="hidden-xs" <? if($config["localheader"]!="") echo "style='background-image:url(\"local/images/header/header.png\");'"; ?>>
			<? if($_SESSION['user_login']!="") { ?>
			<div id="myprofil">
				<?
					if($_SESSION['user_pseudo']!="")
						echo $_SESSION['user_pseudo'];
					else
						echo $_SESSION['user_firstname']." ".$_SESSION['user_lastname'];
				?>
				<br><a class="glyphicon glyphicon-user" href="index.php?action=profil"></a>
				
				<!-- Icone Dossier -->
				<? if($config['urldossier']!="") {?>
				<a class="glyphicon glyphicon-folder-open" href="index.php?action=dossier"></a>
				<? } ?>
				
				<!-- Icone Email -->
				<? if($config['urlemail']!="") {?>
				<a class="glyphicon glyphicon-envelope" href="index.php?action=email"></a>
				<? } ?>
				
				<!-- Icone Administration -->
				<? if($_SESSION['user_profil']==1 && $config["consoleadmin"]) {?>
				<a class="glyphicon glyphicon-cog" href="index.php?action=administration"></a>
				<? } ?>
				
				<!-- Icone Connexion -->
				<? if($_SESSION['user_login']!="") { ?>
					<a class="glyphicon glyphicon-lock" href="logout.php" title="Déconnexion"></a>
				<? } ?>
				
				<!-- Icone Recherche -->
				<? if($config['urlrecherche']!="") {?>
				<form role="search" method='post' >
				<div id="searchbox" class="input-group">
					<input id="srchterm" name="srchterm" type="text" class="form-control" placeholder="Recherche" id="srch-term">
					<div class="input-group-btn">
						<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
					</div>
				</div>
				</form>
				<? } ?>

			</div>
			
			<? if($_SESSION['user_avatar']!="") { ?>
			<a href="index.php?action=profil"><img id="myavatar" src="<? echo "local/images/avatar/".$_SESSION['user_avatar']; ?>" /></a>
			<? } else { ?>
			<a href="index.php?action=profil"><div id="myavatar" class="myavatarvide"></div></a>
			<? } ?>
			<? } ?>
				
			<a href="index.php" alt="<? echo $config['envtitle']; ?>"><div id="mylogo" <? if($config["locallogo"]!="") echo "style='background-image:url(\"local/images/logo/logo.png\");'"; ?>></div></a>
		</div>
		
		<!-- Menu minimum -->
		<div class="navbar-header">
			<div class="navbar-toggle">
				<a class="glyphicon glyphicon-user" href="index.php?action=profil"></a>

				<!-- Icone Dossier -->
				<? if($config['urldossier']!="") {?>
				<a class="glyphicon glyphicon-folder-open" href="index.php?action=dossier"></a>
				<? } ?>
				
				<!-- Icone Email -->
				<? if($config['urlemail']!="") {?>
				<a class="glyphicon glyphicon-envelope" href="index.php?action=email"></a>
				<? } ?>
				
				<!-- Icone Recherche -->
				<? if($config['urlrecherche']!="") {?>
				<a class="glyphicon glyphicon-search" href="index.php?action=recherche"></a>
				<? } ?>
				
				<!-- Icone Administration -->
				<? if($_SESSION['user_profil']==1 && $config["consoleadmin"]) {?>
				<a class="glyphicon glyphicon-cog" href="index.php?action=administration"></a>
				<? } ?>
				
				<!-- Icone Connexion -->
				<? if($_SESSION['user_login']!="") { ?>
					<a class="glyphicon glyphicon-lock" href="logout.php" title="Déconnexion"></a>
				<? } ?>
			
				<!-- Icone regroupant le menu en mode tablette -->
				<a class="glyphicon glyphicon-align-justify" data-toggle="collapse" data-toggle="collapse" data-target=".navbar-ex1-collapse"></a>
			</div>

			<a class="navbar-brand" href="index.php?action=accueil"><? echo $config["envtitle"] ?></a>

		</div>

		<!-- Menu principale -->
		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav">
				<?
					// A partir de la deuxieme page, la première est la page d'accueil
					$firstpage=true;
					foreach ($tbpage as $page) {
						if(!$firstpage) {
							$lbactive="";
							if($action=="page"&&$id==$page[1]) $lbactive="active";
							
							echo "<li class='$lbactive'><a href='index.php?action=page&id=".$page[1]."'>".$page[2]."</a></li>";
						}
						$firstpage=false;
					}
				?>
			</ul>

			<ul class="nav navbar-nav mypanel">
				<?
					if($_SESSION['user_id']!="") {
						$q="SELECT * FROM env_panel WHERE panel_user=".$_SESSION['user_id']." ORDER BY panel_order";
						$dbpage->query($q);
						while($dbpage->next_record()) {
							$lbactive="";
							if($action=="page"&&$id==$dbpage->f("panel_id")) {
								$lbactive="active";
								$idsel=$dbpage->f("panel_id");
							}
							
							echo "<li rel='".$dbpage->f("panel_id")."' class='$lbactive'><a href='index.php?action=page&id=".$dbpage->f("panel_id")."'>".$dbpage->f("panel_label")."</a></li>";
						}
					}
				?>
			</ul>

			<ul class="nav navbar-nav">
				<?
					if($_SESSION['user_id']!="") {
						echo "<li style='width:81px;'>";
						echo "<a class='glyphicon glyphicon-plus' style='cursor: pointer;float: left; width:25px; padding:2px;' data-toggle='modal' data-target='.bs-item-modal' title='Ajouter onglet' onClick='changeFrame(\"addpanel\",0)'></a>";
						
						if($idsel!="") {
							echo "<a class='glyphicon glyphicon-file' style='cursor: pointer;float: left; width:25px; padding:2px;' data-toggle='modal' data-target='.bs-item-modal' title='Modifier onglet' onClick='changeFrame(\"modpanel\",$idsel)'></a>";
							echo "<a class='glyphicon glyphicon-remove' style='cursor: pointer;float: left; width:25px; padding:2px;' title='Supprimer onglet' onClick='SupPanel(".$idsel.")'></a>";
						}
						echo "</li>";
					}
				?>
			</ul>
			
			<ul class="nav navbar-nav navbar-right">
				<!-- Icone Connexion -->
				<? if($_SESSION['user_login']=="") { ?>
					<li><a href="login.php">Connexion</a></li>
				<? } ?>
			</ul>
		</div>
	</nav>   
    

<!---------------------------------------------------------------------------------------------------------------------------------------------------->      
<!-- CONTAINER IFRAME -------------------------------------------------------------------------------------------------------------------------------->      
<!---------------------------------------------------------------------------------------------------------------------------------------------------->      
	<div class="container">
		<div class="row">
			<div class="span12">

<!-- ACCEUIL ----------------------------------------------------------------------------------------------------------------------------------------->
				<?
					if($action=="accueil") {
						if($tbpage[0][1]!="") {
							$action="page";
							$id=$tbpage[0][1];
						}
						else
							echo "<iframe class='myframe' src='".$config['urlaccueil']."' style='height:100%; width:100%' frameborder='0' ></iframe>";
					}

				?>

<!-- PAGE -------------------------------------------------------------------------------------------------------------------------------------------->
				<?
					if($action=="page") {
						$dbpage->query("SELECT * FROM env_panel WHERE panel_id=$id");
						if($dbpage->next_record()) {
							// Cas application
							if($dbpage->f('panel_type')==2) {
								include("application.php");
							}
							
							// Cas editeur ou widget
							elseif($dbpage->f('panel_type')==3||$dbpage->f('panel_type')==4) {
								echo "<iframe class='myframe' src='".$dbpage->f('panel_url')."?type=panel&id=".$id."' style='height:100%; width:100%' frameborder='0' ></iframe>";
							}
								
							// Cas url simple
							else {
								echo "<iframe class='myframe' src='".$dbpage->f('panel_url')."' style='height:100%; width:100%' frameborder='0' ></iframe>";
							}
						}
					}
				?>						

<!-- DOSSIER ----------------------------------------------------------------------------------------------------------------------------------------->
				<?
					if($action=="dossier") { 
						echo "<iframe class='myframe' src='".$config['urldossier']."' style='height:100%; width:100%' frameborder='0' ></iframe>";
					}
				?>	
				
<!-- EMAIL ------------------------------------------------------------------------------------------------------------------------------------------->
				<?
					if($action=="email") { 
						echo "<iframe class='myframe' src='".$config['urlemail']."' style='height:100%; width:100%' frameborder='0' ></iframe>";
					}
				?>	

<!-- PROFIL ------------------------------------------------------------------------------------------------------------------------------------------>
				<?
					if($action=="profil") { 
						echo "<iframe class='myframe' src='profil.php' style='height:100%; width:100%' frameborder='0' ></iframe>";
					}
				?>
				
<!-- RECHERCHE---------------------------------------------------------------------------------------------------------------------------------------->
				<?
					if($action=="recherche") { 
						$url=$config['urlrecherche'];
						if($srchterm!="") 
							$url=$url."?opensearch=".urlencode($srchterm);
							
						echo "<iframe class='myframe' src='$url' style='height:100%; width:100%' frameborder='0' ></iframe>";
					}
				?>	

<!-- ADMINISTRATION----------------------------------------------------------------------------------------------------------------------------------->
				<?
					if($action=="administration") { 
						echo "<iframe class='myframe' src='admin/config.php' style='height:100%; width:100%' frameborder='0' ></iframe>";
					}
				?>	
				
			</div>
		</div>
	</div>

<!-- POPUP-------------------------------------------------------------------------------------------------------------------------------------------->
	<div id="mymodal" class="modal fade bs-item-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-lg" style=" max-width:655px; width:80%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">ONGLET</h4>
			</div>
			<div class="modal-body">
				<iframe id="framemodal" frameborder=0 width="100%" height="520px"></iframe>
			</div>
		</div>
	  </div>
	</div>	

	<!-- chargement process -->
	<div id="info"></div>
	
<!-- FOOTER --------------------------------------------------------------------------------------------------------------------------------------- -->  	
	<?
		include("footer.php");
	?>

<!-- SCRIPT --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<script type="text/javascript">
	function changeFrame(tpmod,idpan) {
		if(tpmod=="addpanel") {
			srcframe="panelmodify.php?tpmod="+tpmod;
		}
		else {
			srcframe="panelmodify.php?mode="+tpmod+"&id="+idpan;
		}
		$("#framemodal").attr("src",srcframe);
	}
	
	function SupPanel(idpan) {
		if(confirm("Confirmez-vous la suppression de cet onglet ?")) {
			$("#info").load("process/process-userpanel-del.php?id="+idpan);
			
			setTimeout(function () {document.location.href='index.php';},1000);	
		}
	}
	
	function recharge() {
		$("#formulaire").submit();
	}
	
	function closemodal() {
		$('#mymodal').modal('hide');
	}
	

	<?php echo $jsaction ?>
	
</script>
