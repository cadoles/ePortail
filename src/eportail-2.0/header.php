<!DOCTYPE html>
<html>
	
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="shortcut icon" href="<? echo $repository; ?>lib/bootstrap/assets/ico/favicon.png">

		<title><? echo $config['envtitle']?></title>

		<!-- Bootstrap core CSS -->
		<link href="<? echo $repository; ?>lib/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
		<link href="<? echo $repository; ?>lib/bootstrap/font-awesome/css/font-awesome.css" rel="stylesheet">

		<!-- Style par défaut -->
		<link href="<? echo $repository; ?>style/style.css" rel="stylesheet">    
		<link href="<? echo $repository; ?>lib/datatables/DT_bootstrap.css" rel="stylesheet">
		<link href="<? echo $repository; ?>lib/jquery-ui/themes/base/jquery-ui.css" rel="stylesheet">
		<link href="<? echo $repository; ?>lib/datepicker/css/datepicker.css" rel="stylesheet">

		<!-- Style custom -->
		<? if($config['themedir']!="") { ?>
		<link href="<? echo $repository.'style/themes/'.$config['themedir'].'/style.css' ?>" rel="stylesheet">    
		<? } ?>
		
		<script src="<? echo $repository; ?>lib/bootstrap/assets/js/jquery.js"></script>
		<script src="<? echo $repository; ?>lib/bootstrap/dist/js/bootstrap.min.js"></script>
		<script src="<? echo $repository; ?>lib/datatables/jquery.dataTables.js"</script>	
		<script src="<? echo $repository; ?>lib/datatables/DT_bootstrap.js"></script>	
		<script src="<? echo $repository; ?>lib/bootstrap/js/tooltip.js"></script>
		<script src="<? echo $repository; ?>lib/datepicker/js/bootstrap-datepicker.js"></script>
		<script src="<? echo $repository; ?>lib/datepicker/js/locales/bootstrap-datepicker.fr.js"></script>
		<script src="<? echo $repository; ?>lib/envole/envole.js"></script>			
	</head>
	
