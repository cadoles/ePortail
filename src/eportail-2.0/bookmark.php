<?
	include($repository."include/include.php");
	include($repository."header.php");
		
	echo "<body>";
	
	$db1=new ps_db;
	$db2=new ps_db;
		
	$tpwid				=$_GET['tpwid'];
	
	$tpmod				=$_POST['tpmod'];
	$vlmod				=$_POST['vlmod'];
	$vladd				=$_POST['vladd'];
	$vlsup				=$_POST['vlsup'];
	
	$id					=$_POST['id'];
	$name				=$_POST['name'];
	$url				=$_POST['url'];
	$open				=$_POST['open'];
	$icon				=$_POST['icon'];
	$idwid				=$_POST['idwid'];
	
	if($idwid=="") $idwid=$_GET['id'];
	
	
	// Récupérer le panel du widget
	$q="SELECT * FROM env_panel_widget WHERE panel_widget_id=".$idwid;
	$db1->query($q);
	$height="250";
	if($db1->next_record()) {
		$idpan=$db1->f('panel_widget_panel');
		$height=$db1->f('panel_widget_height');
	}
	
	// Récupérer le template de la page widget et le propriétaire de la page
	$q="SELECT panel_user, panel_html FROM env_panel WHERE panel_id=".$idpan;
	$db1->query($q);
	if($db1->next_record()) {
		$idpro=$db1->f('panel_user');
		$html=$db1->f('panel_html');
	}

	// Si l'utilisateur en cours est le propriétaire ou administrateur : il peut modifier la page
	$fgproprio=false;
	if($idpro==$_SESSION['user_id']||$_SESSION['user_profil']==1) {
		$fgproprio=true;
	}
	
	/*--> Controle de cohérance */
	if($vladd!=""||$vlmod!="") {
		$fgerr="";

		if($name==""||$url==""||$icon==""||$open=="") {
			$jsaction="alert('Vous devez renseigner l\'ensemble des informations');";
			$fgerr=1;
		}
	}
	
	/*--> Rester sur le mode encours */
	if($vlmod!=""&&$fgerr!="") $tpmod="MODIFY";
	if($vladd!=""&&$fgerr!="") $tpmod="SUBMIT";
		
	/*--> Submit */
	if($vladd!=""&&$fgerr=="") {
		$q="INSERT INTO env_panel_widget_bookmark(panel_widget_bookmark_name, panel_widget_bookmark_url, panel_widget_bookmark_open, panel_widget_bookmark_icon, panel_widget_bookmark_user, panel_widget_bookmark_widget) VALUES('".addslashes($name)."','".urlencode($url)."','$open','$icon','".$_SESSION['user_id']."','$idwid')";
		$db1->query($q);
		$tpmod="";
	}
	
	/*--> Delete */
	if($vlsup!="") {
		$q="DELETE FROM env_panel_widget_bookmark WHERE panel_widget_bookmark_id='$id'";
		$db1->query($q);
		$tpmod="";
	}

?>
<style>
	body{
		overflow-x:hidden; 
	}
	
	.badge-delete {
		font-size: 14px;
		cursor:pointer;
		position: relative;
		top: 0px;
		margin-bottom: -20px;
		margin-left: 60px;
		height: 20px;
		text-decoration:none;
	}
	
	.badge-delete:hover {
		text-decoration:none !important;
	}
		
</style>
<?

	echo "<div class='container-fluid'>";
	echo "<form id='formulaire' class='form-horizontal' role='form' method='post' enctype='multipart/form-data'>";
	
	echo "<input id='id' name='id' type='hidden' value='".$id."'>";
	echo "<input id='idwid' name='idwid' type='hidden' value='".$idwid."'>";
	echo "<input id='tpmod' name='tpmod' type='hidden' value=''>";
	

//-- DISPLAY -------------------------------------------------------------------------------------------------------------------------------

	if($tpmod=="") { 
		if($tpwid=="") {
			$db1->query("SELECT * FROM env_panel_widget_bookmark, env_icon WHERE panel_widget_bookmark_widget=$idwid AND panel_widget_bookmark_icon=icon_id ORDER BY panel_widget_bookmark_name");
		}
		else {
			$db1->query("SELECT * FROM env_panel_widget_bookmark, env_icon WHERE panel_widget_bookmark_user=".$_SESSION["user_id"]." AND panel_widget_bookmark_icon=icon_id ORDER BY panel_widget_bookmark_widget, panel_widget_bookmark_name");
		}
		
		$catwid="0";
		echo "<fieldset>";
		$fgitem=false;
		while($db1->next_record()) {
			if($tpwid!=""&&$catwid!=$db1->f("panel_widget_bookmark_widget")){
				if($fgproprio) {
					echo "<div id='myitem'>";
					//echo "<button type='submit' class='btn btn-default' onClick='$(\"#idwid\").val(\"".$db1->f("panel_widget_bookmark_widget")."\"); $(\"#id\").val(\"\"); $(\"#tpmod\").val(\"SUBMIT\");'><span class='glyphicon glyphicon-plus'></span></button>";
					echo "<button type='submit' class='btn btn-default' onClick='$(\"#idwid\").val(\"".$catwid."\"); $(\"#id\").val(\"\"); $(\"#tpmod\").val(\"SUBMIT\");'><span class='glyphicon glyphicon-plus'></span></button>";
					echo "</div>";
				}
				
				if($db1->f("panel_widget_bookmark_widget")!="") {
					$db2->query("SELECT * FROM env_panel_widget WHERE panel_widget_id=".$db1->f("panel_widget_bookmark_widget"));
					if($db2->next_record()) {
						echo "</fieldset>";
						echo "<div class='form-group'>";
						echo "<label style='margin-left:15px'><br>";
						echo $db2->f("panel_widget_label")."</label>";
						echo "<fieldset style='clear:both'>";
					}
				}
				$catwid=$db1->f("panel_widget_bookmark_widget");
			}
			
			echo "<div id='myitem' class='myitem' rel='".$db1->f("panel_widget_bookmark_id")."'>";

			echo "<a id='badge-delete-".$db1->f("panel_widget_bookmark_id")."' style='display:none;' class='badge-delete glyphicon glyphicon-remove' onClick='$(\"#idwid\").val(\"".$idwid."\"); $(\"#id\").val(\"".$db1->f("panel_widget_bookmark_id")."\"); $(\"#tpmod\").val(\"DELETE\");$(\"#formulaire\").submit();'></a>";

			if($db1->f("panel_widget_bookmark_open")==0)
				echo "<div onClick='chargeApplication(\"".urldecode($db1->f("panel_widget_bookmark_url"))."\");'>";
			else
				echo "<div><a href='".urldecode($db1->f("panel_widget_bookmark_url"))."' target=_blank>";
				
			echo "<img src='local/images/icon/".$db1->f("icon_url")."'/>";
			echo "<br>";
			echo $db1->f("panel_widget_bookmark_name");
			
			if($db1->f("panel_widget_bookmark_open")==0) echo "</div>";
			else echo "</a></div>";
			
			echo "</div>";
			
		}
		
		if($fgproprio) {
			if($tpwid=="") {
				echo "<div id='myitem'>";
				echo "<button type='submit' class='btn btn-default' onClick='$(\"#idwid\").val(\"".$idwid."\"); $(\"#id\").val(\"\"); $(\"#tpmod\").val(\"SUBMIT\");'><span class='glyphicon glyphicon-plus'></span></button>";
				echo "</div>";
			}
			else {
				echo "<div id='myitem'>";
				echo "<button type='submit' class='btn btn-default' onClick='$(\"#idwid\").val(\"".$catwid."\"); $(\"#id\").val(\"\"); $(\"#tpmod\").val(\"SUBMIT\");'><span class='glyphicon glyphicon-plus'></span></button>";
				echo "</div>";
			}
		}
	}

//-- SUBMIT --------------------------------------------------------------------------------------------------------------------------------

	elseif($tpmod=="SUBMIT") {
		echo "<br>";
		echo "<div class='form-group'>";
		echo "<div class='col-sm-12'>";
		echo "<input id='vladd' name='vladd' class='btn btn-primary' type='submit' value='Valider' />";
		echo "&nbsp;";
		echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
		echo "</div>";
		echo "</div>";	

		// Valeur par défaut
		$open=0;
		
		// Récupérer l'url de l'image en cours
		if($icon!="") {
			$db1->query("SELECT * from env_icon WHERE icon_id=$icon");
			if($db1->next_record()) $urlicon=$db1->f("icon_url");
		}
		
		
	}

//-- DELETE --------------------------------------------------------------------------------------------------------------------------------

	elseif($tpmod=="DELETE") {
		echo "<br>";
		echo "<div class='form-group'>";
		echo "<div class='col-sm-12'>";
		echo "<input id='vlsup' name='vlsup' class='btn btn-primary' type='submit' value='Confirmer la suppression' />";
		echo "&nbsp;";
		echo "<input class='btn btn-primary' type='submit' value='Annuler' />";    
		echo "</div>";
		echo "</div>";	  
		
		// Valeur par défaut
		$q = "SELECT * FROM env_panel_widget_bookmark, env_icon WHERE panel_widget_bookmark_id=$id AND panel_widget_bookmark_icon=icon_id";
		$db1->query($q);
		if($db1->next_record()) {	
			echo "<div id='myavatarprofil' style='float:left; margin-left: 10px; margin-right:5px; margin-bottom:10px; border:none;'>";
			echo "<img id='imgapp' src='local/images/icon/".$db1->f("icon_url")."' width='90px' height='90px'></img>";
			echo "</div>";
			
			echo $db1->f('panel_widget_bookmark_name')."<br>";
			echo "<span id='avatar_description'>";
			echo "url : <a href='".$db1->f('panel_widget_bookmark_url')."' target='_black'>".urldecode($db1->f('panel_widget_bookmark_url'))."</a><br>";				
		}
	}

//-- FORMULAIRE -----------------------------------------------------------------------------------------------------------------------------

	if($tpmod=="SUBMIT"||$tpmod=="MODIFY") {
	?>
		<div class="form-group">
			<label for="name" class="col-sm-3 control-label">Nom*</label>
			<div class="col-sm-6"><input name="name" id="name" type="titre" class="form-control" placeholder="Nom" value="<? echo $name; ?>"></div>
		</div>

		<div class="form-group">
			<label for="url" class="col-sm-3 control-label">Url*</label>
			<div class="col-sm-6"><input name="url" id="url" type="titre" class="form-control" placeholder="Url" value="<? echo urldecode($url); ?>"></div>
		</div>	

		<div class="form-group">
			<label for="open" class="col-sm-3 control-label">Mode d'ouverture*</label>
			<div class="col-sm-6">
				<select name="open" id="open" class="form-control">
					<option value='0' <? if($open==0) echo "selected"; ?>>Dans le portail</option>";
					<option value='1' <? if($open==1) echo "selected"; ?>>Nouvel onglet</option>";
				</select>
			</div>
		</div>
		
		<input 	value="<? echo $icon; ?>" id="icon"	name="icon"	type="hidden"/>
		<?
		echo "<div style='width:140px; margin:auto; '>";
		echo "<div id='myavatarprofil' style='float:left; margin-right:5px; margin-bottom:10px; border:none;'>";
		echo "<img id='imgapp' src='local/images/icon/".$urlicon."' width='90px' height='90px'></img>";
		echo "</div>";
		echo "<div style='float:left;'>";
		echo "<a class='glyphicon glyphicon-folder-close' data-toggle='modal' data-target='.bs-item-modal' title='Séléctionner un icône' onClick='changeFrame(\"selicone\")'></a>";
		echo "</div>";
		echo "</div>";
		?>

		<!-- Popup modification item !-->
		<div id="mymodal" class="modal fade bs-item-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		  <div class="modal-dialog modal-lg" style="width:100%;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">ICÔNE</h4>
				</div>
				<div class="modal-body">
					<iframe id="framemodal" frameborder=0 width="100%" height="<? echo ($height-155)."px"; ?>"></iframe>
				</div>
			</div>
		  </div>
		</div>	

<?
}
	
	include($repository."footer.php");
?>


<script>
	function chargeApplication(srcframe) {
		if($("#itemframe").length>0) {
			$("#itemframe").attr("src",srcframe);
			$("#itemframe").css("display","block");
		}
		else {
			<? if($tpwid=="") { ?>
			window.parent.changeItem(srcframe);	
			<? } else { ?>
			window.parent.chargeApplication(srcframe);	
			<? } ?>
		}
	}	

	$(".myitem")
		.mouseover(function() {
			<? if($fgproprio) { ?>
			var id = $(this).attr('rel');
			$("#badge-delete-"+id).css("display","block");
			<? } ?>
		})
		.mouseout(function() {
			<? if($fgproprio) { ?>
			var id = $(this).attr('rel');
			$("#badge-delete-"+id).css("display","none");
			<? } ?>
		});
			
<? if($tpmod=="SUBMIT"||$tpmod=="MODIFY"||$tpmod=="DELETE") { ?>
	$('#name').focus();
	
	
	function changeFrame(tpmod) {
		srcframe="admin/icon.php?tpmod=SELECT&igico=imgapp&idico=icon";
		$("#framemodal").attr("src",srcframe);
	}
	
	function recharge() {
		$("#formulaire").submit();
	}
	
	function closemodal() {
		$('#mymodal').modal('hide');
	}
	

  	
<? } ?>	
	
	<?php echo $jsaction ?>
</script>
