<?
	include_once("include/include.php");
	include("header.php");	
	
	$db1=new ps_db;
	$db2=new ps_db;
	
	$id=$_GET['id'];
	
	echo "<script src='".$repository."lib/jquery-ui/ui/jquery-ui.js'></script>";
	
	// Récupérer le template de la page widget et le propriétaire de la page
	$q="SELECT panel_template, panel_user FROM env_panel WHERE panel_id=".$id;
	$db1->query($q);
	if($db1->next_record()) {
		$idtpl=$db1->f('panel_template');
		$idpro=$db1->f('panel_user');
	}
?>


<script type="text/javascript">  
     function sizeFrame() {
          jQuery("#iframe_13", top.document).css({ height: 0 });
          var heightDiv = jQuery("#iframe_13", top.document).contents().find('body').attr('scrollHeight');
          alert(heightDiv);
          jQuery("#iframe_13", top.document).css({ height: heightDiv });
     }
   
   
	<? if($idpro==$_SESSION['user_id']||$_SESSION['user_profil']==1) { ?>
	$(document).ready(function () {  
	
		$(".column").sortable({
			tolerance: 'pointer',
			revert: 'invalid',
			connectWith: ".column",  
			handle: '.WidgetHeader', 
			opacity: 0.6,
			placeholder: 'widget placeholder',
			forceHelperSize: true,
			
			update: function ()
			{
				var a = $(this);

				$('.column').each(function(index) {
					var order = $(this).sortable('serialize');
					$("#info").load("process/process-userwidget-order.php?"+order+"&idloc="+$(this).attr('id')+"&id=<? echo $id; ?>");
				});
			} 			
		});			
    
		$(".column").disableSelection();					
	});  
	
	<? } ?>
	
	function delwidget(idwid) {
		console.log("je suis la");
		if(confirm("Confirmez-vous la suppression de ce widget ?")) {
			$("#info").load("process/process-userwidget-del.php?id="+idwid);
			setTimeout(function () {location.reload()},1000);			
		}
	}	
</script>

</head>

<?
	// Si l'utilisateur en cours est le propriétaire : il peut modifier la page
	if($idpro==$_SESSION['user_id']||$_SESSION['user_profil']==1) {
		echo "<div id='widgetadmin'>";
		echo "<a style='cursor: pointer;' data-toggle='modal' data-target='.bs-item-modal' title='Modifier la forme' onClick='changeFrame(\"modtemplate\",$id)'>Modifier la forme</a>";
		echo "&nbsp-&nbsp";
		echo "<a style='cursor: pointer;' data-toggle='modal' data-target='.bs-item-modal' title='Ajouter un widget' onClick='changeFrame(\"addwidget\",0)'>Ajouter un Widget</a>";
		echo "</div>";

		
	}	
?>

<div id="info"></div>

<?
	// Placer une iframe conteneur 
	echo "<iframe id='itemframe' class='myframe' src='' style='height:100%; width:100%; display:none;' frameborder='0' ></iframe>";	

	// Récupérer le code html du template
	$q="SELECT * FROM env_panel_template WHERE panel_template_id=".$idtpl;
	$db1->query($q);
	if($db1->next_record()) { echo $db1->f('panel_template_html'); }
	
	
	// Parcourrir l'ensemble des widget de la page pour les créer dynamiquement
	$q="SELECT * FROM env_panel_widget, env_widget WHERE panel_widget_panel=".$id." AND panel_widget_widget=widget_id ORDER BY panel_widget_order DESC";
	$db1->query($q);
	while($db1->next_record()) {	
		// Définition de la source de l'url : soit celle de l'utilisateur soit celle du widget si type de widget systeme l'url est forcement déterminé par l'utilisateur
		if($db1->f('widget_url') != "")
			$url=$db1->f('widget_url');
		else
			$url=$db1->f('panel_widget_url');
		

		// Définition de l'url en fonction du type de contenu du widget si type rss on force la page rss.php en http
		if($db1->f("widget_type")==2) 
			$panel_widget_url="rss.php?idwid=".$db1->f('panel_widget_id');
		elseif($db1->f("widget_type")==3) 
			$panel_widget_url="editeur.php?type=widget&id=".$db1->f('panel_widget_id');
		elseif($db1->f("widget_type")==4) 
			$panel_widget_url="code.php?idwid=".$db1->f('panel_widget_id');
		elseif($db1->f("widget_type")==5) 
			$panel_widget_url="application.php?mode=widget";
		elseif($db1->f("widget_type")==6)
			$panel_widget_url=$url."?id=".$db1->f('panel_widget_id');
		else
			$panel_widget_url=$url;
		
			
		// Div conteneur global
		$iframe="<div id='widget_".$db1->f('panel_widget_id')."' class='WidgetClass".$db1->f('panel_widget_style')."'>";
		
		// Div titre
		$iframe=$iframe."<div class='WidgetHeader'><a data-toggle='collapse' href='#widgetbody_".$db1->f('panel_widget_id')."'>".$db1->f('panel_widget_label')."</a>";

		// Si propriétaire de la page ou administrateur bouton modifier / supprimer
		if($idpro==$_SESSION['user_id']||$_SESSION['user_profil']==1) {							
			$iframe=$iframe."
				<div class='WidgetHeaderMenu'>
				<a class='glyphicon glyphicon-file'   onClick='changeFrame(\"modwidget\",".$db1->f('panel_widget_id').")'  data-toggle='modal' data-target='.bs-item-modal' title='Modifier'></a>
				<a class='glyphicon glyphicon-remove' onClick='delwidget(".$db1->f('panel_widget_id').");' title='Supprimer'></a>
				</div>";
		}	 

		// Fermeture de la Div titre
		$iframe=$iframe."</div>";
		
		// Déterminer la hauteur
		$hauteur=($db1->f('panel_widget_height')-30)."px";
		
		// Div corps
		$iframe=$iframe."<div id='widgetbody_".$db1->f('panel_widget_id')."' class='WidgetBody collapse in' style='height:".$hauteur."'>";
		
		// Mode iframe
		if($db1->f('widget_mode')==1) {;
			$iframe=$iframe."
				<iframe
					id='frame_".$db1->f('panel_widget_id')."'
					src='$panel_widget_url'
					width=100%
					height='".$hauteur."'		
					frameborder=0
					scrolling=auto></iframe>";
		}
		
		// Fermeture de la Div corps
		echo "</div>";
			
			
		// Fermeture de la Div global
		$iframe=$iframe."</div></div>";
		
		// Cas hauteur nulle
		//if($db1->f('panel_widget_height')=="") $iframe=str_replace("height=''","",$iframe);
		
		// Suppression des retour chariot pour bonne interprétation en javascript
		$iframe=str_replace("\n","",$iframe);
	?>
		<script type="text/javascript">  
			// Recherche de la colonne où le widget est localisé
			loc = $('#<? echo $db1->f('panel_widget_loc'); ?>');
			
			
			if(loc.length==0) {
				loc=$('#C1R1');
				console.log("<? echo $db1->f('panel_widget_loc')?> Taille"+loc.length);
			}
			
			// Ajout du widget
			charloc="<? echo addslashes($iframe); ?>";
			
			//$(".grid").prepend(charloc);
			loc.prepend(charloc);
			//loc.prepend('<div id="widget_<? echo $db1->f('panel_widget_id'); ?>" class="Widget"><div class="WidgetHeader"><? echo $db1->f('panel_widget_label'); ?></div><div class="WidgetBody"><iframe src="'+iframewidget+'" width=100% height=175px frameborder=0></iframe></div></div>');
		</script>
		
	<?
	}
	
	
?>

<!-- Popup gestion widget !-->
<div id="mymodal" class="modal fade bs-item-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" style=" max-width:655px; width:80%;">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title">WIDGET</h4>
		</div>
		<div class="modal-body">
			<iframe id="framemodal" frameborder=0 width="100%" height="520px"></iframe>
		</div>
	</div>
  </div>
</div>	

<!-- FOOTER --------------------------------------------------------------------------------------------------------------------------------------- -->  	

<?
	include("footer.php");
?>

<!-- SCRIPT --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<script type="text/javascript">
	$('#name').focus();
	
	function changeFrame(tpmod,idpwd) {
		if(tpmod=="modtemplate") {
			srcframe="widgettemplate.php?idpan=<? echo $id; ?>";
		}
		else {
			srcframe="widgetmodify.php?idpan=<? echo $id; ?>&mode="+tpmod+"&idpwd="+idpwd;
		}
		$("#framemodal").attr("src",srcframe);
	}

	function configWidget(idpan,idwid,tpwid) {
		srcframe="widgetconfig.php?idpan="+idpan+"&idwid="+idwid+"&tpwid="+tpwid;
		$("#framemodal").attr("src",srcframe);
		$('#mymodal').modal('show');
	}

		
	function recharge() {
		location.reload();
	}
	
	function closemodal() {
		$('#mymodal').modal('hide');
	}

	function changeItem(srcframe) {
		$("#itemframe").attr("src",srcframe);
		$("#itemframe").css("display","block");
		$("#widgetadmin").css("display","none");
		$(".column").css("display","none");
	}
	
	<?php echo $jsaction ?>
	
</script>

