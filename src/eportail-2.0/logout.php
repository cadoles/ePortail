<?
	include_once("include/config.php");
	include_once("include/mysql.php");
	include_once("include/session.php");
	
	session_destroy();
	$_SESSION['user_login'] = "";
	
	if ($config['activerCAS']) {
		include("include/CASlogout.php");
	}
	
	header('Location: index.php');
?>
