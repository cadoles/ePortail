<?
	include($repository."include/include.php");
	include($repository."header.php");
		
	$db1=new ps_db;
	
	$user_id			=$_SESSION['user_id'];
	
	$vlmod				=$_POST['vlmod'];
	
	$user_login			=$_POST['user_login'];
	$user_password		=$_POST['user_password'];
	$user_passwordbis	=$_POST['user_passwordbis'];
	$user_lastname		=$_POST['user_lastname'];
	$user_firstname		=$_POST['user_firstname'];
	$user_pseudo		=$_POST['user_pseudo'];
	$user_email			=$_POST['user_email'];
	$user_avatar		=$_POST['user_avatar'];
	
	
	/*--> Controle de cohérance */
	if($vlmod!="") {
		$fgerr="";

		if($user_password!=$user_passwordbis) {
			$jsaction="alert('Votre mot de passe n\'est pas confirmé');";
			$fgerr=1;
		}

		if($user_lastname==""||$user_firstname==""||$user_email=="") {
			$jsaction="alert('Vous devez renseigner l\'ensemble des informations');";
			$fgerr=1;
		}
		
		$q="SELECT * FROM env_user WHERE (user_login='".addslashes($user_login)."' OR user_email='".addslashes($user_email)."' OR ('".addslashes($user_pseudo)."'!='' AND user_pseudo='".addslashes($user_pseudo)."'))";
		if($vlmod!="") $q=$q." AND user_id!=$user_id";
		$db1->query($q);
		if($db1->next_record()){			
			$jsaction="alert('Un utilisateur avec cet email ou ce pseudo existe déjà');";
			$fgerr=1;
		}
	}
	
	/*--> Rester sur le mode encours */
	if($vlmod!=""&&fgerr!="") $tpmod="MODIFY";
		
	/*--> Modify */
	if($vlmod!=""&&$fgerr=="") {
		$q="UPDATE env_user SET user_lastname='".addslashes($user_lastname)."', user_firstname='".addslashes($user_firstname)."', user_pseudo='".addslashes($user_pseudo)."', user_email='".addslashes($user_email)."', user_avatar='$user_avatar' WHERE user_id=$user_id";
		$db1->query($q);
		
		// Changement de mot de passe
		if($user_password!=""&&$user_passwordbis!="") {
			$q="UPDATE env_user SET user_password='".md5($user_password)."' WHERE user_id=$user_id";
			$db1->query($q);
		}
		$tpmod="";
		
		$_SESSION['user_firstname']		= $user_firstname;
		$_SESSION['user_lastname']		= $user_lastname;
		$_SESSION['user_pseudo']		= $user_pseudo;
		$_SESSION['user_avatar']		= $user_avatar;		
		
		$jsaction="recharge();";
	}
		
	echo "<body>";
    
    echo "<form class='form-horizontal' role='form' method='post' style='width:80%; margin:auto;' enctype='multipart/form-data'>";

	echo "<input id='user_id' name='user_id' type='hidden' value='".$user_id."'>";
	echo "<input id='tpmod' name='tpmod' type='hidden' value=''>";
	
  
	// Entete du formulaire
    echo "<legend><h1>PROFIL</h1></legend>";
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlmod' name='vlmod' type='submit' class='btn btn-primary' value='Enregistrer'>";
	echo "</div>";
	echo "</div>";

	// Valeur par défaut
	$q = "SELECT * FROM env_user WHERE user_id=$user_id";
	$db1->query($q);
	if($db1->next_record()) {
		$user_login 		= $db1->f("user_login");
		$user_lastname 		= $db1->f("user_lastname");
		$user_firstname 	= $db1->f("user_firstname");
		$user_pseudo	 	= $db1->f("user_pseudo");
		$user_email 		= $db1->f("user_email");
		$user_profil_id		= $db1->f("user_profil_id");
		$user_mod			= $db1->f("user_mod");
		$user_avatar		= $db1->f("user_avatar");
	}

	if($user_avatar=="") $class=" class='myavatarvide'";

	echo "<div style='width:110px; margin:auto; '>";
	echo "<div id='myavatarprofil' style='float:left; margin-right:5px; margin-bottom:10px; border:none;' $class >";
	if($user_avatar!="") 
		echo "<img id='img_avatar' name='img_avatar' src='local/images/avatar/".$user_avatar."' width='90px' height='90px'></img>";
	else
		echo "<img id='img_avatar' name='img_avatar' src='style/images/blank.gif' width='90px' height='90px'></img>";
	echo "</div>";
	
	echo "<div style='float:left;'>";
	echo "<a class='glyphicon glyphicon-plus' data-toggle='modal' data-target='.bs-avatar-modal' title='Ajouter un avatar'></a>";
	echo "<br>";
	echo "<a class='glyphicon glyphicon-remove' onClick='$(\"#myavatarprofil\").addClass(\"myavatarvide\"); $(\"#img_avatar\").attr(\"src\",\"\"); $(\"#user_avatar\").val(\"\");' title='Vider l'avatar'></a>";
	echo "</div>";
	echo "</div>";
	
	echo "<input id='user_avatar' name='user_avatar'  type='hidden' value='".$user_avatar."'>";
	?>

	<fieldset id="encadrer" style="clear:both">
		<div class="form-group">
			<label for="user_id_bis" class="col-sm-3 control-label">ID Utilisateur*</label>
			<div class="col-sm-6"><input name="user_id_bis" id="user_id_bis" type="titre" class="form-control" disabled="disabled" placeholder="ID Utilisateur" value="<? echo $user_id; ?>"></div>
		</div>
	
		<div class="form-group">
			<label for="user_login" class="col-sm-3 control-label">Login*</label>
			<div class="col-sm-6"><input name="user_login" id="user_login" type="titre" class="form-control" disabled="disabled" placeholder="Login" value="<? echo $user_login; ?>"></div>
		</div>

		<? if(!$config['activerCAS']) { ?>
		<div class="form-group"> 
			<label for="user_password" class="col-sm-3 control-label">Password*</label>
			<div class="col-sm-6"><input name="user_password" id="user_password" type="password" class="form-control" placeholder="Password" value="<? echo $user_password; ?>"></div>
		</div>		

		<div class="form-group"> 
			<label for="user_passwordbis" class="col-sm-3 control-label">Confirmer Password*</label>
			<div class="col-sm-6"><input name="user_passwordbis" id="user_passwordbis" type="password" class="form-control" placeholder="Confirmer Password" value="<? echo $user_passwordbis; ?>"></div>
		</div>	
		<? }
		else  $disable="readonly"; 
		?>

		<div class="form-group">
			<label for="user_lastname" class="col-sm-3 control-label">Nom*</label>
			<div class="col-sm-6"><input name="user_lastname" id="user_lastname" type="titre" class="form-control" <? echo $disable; ?> placeholder="Nom" value="<? echo $user_lastname; ?>"></div>
		</div>

		<div class="form-group">
			<label for="user_firstname" class="col-sm-3 control-label">Prénom*</label>
			<div class="col-sm-6"><input name="user_firstname" id="user_firstname" type="titre" class="form-control" <? echo $disable; ?> placeholder="Prénom" value="<? echo $user_firstname; ?>"></div>
		</div>

		<div class="form-group">
			<label for="user_email" class="col-sm-3 control-label">Email*</label>
			<div class="col-sm-6"><input name="user_email" id="user_email" type="email" class="form-control" <? echo $disable; ?> placeholder="Email" value="<? echo $user_email; ?>"></div>
		</div>
		
		<div class="form-group">
			<label for="user_pseudo" class="col-sm-3 control-label">Pseudo*</label>
			<div class="col-sm-6"><input name="user_pseudo" id="user_pseudo" type="titre" class="form-control" placeholder="Pseudo" value="<? echo $user_pseudo; ?>"></div>
		</div>
	</fieldset>

	<!-- Popup creation avatar !-->
	<div id="mymodal" class="modal fade bs-avatar-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-lg" style=" max-width:655px; width:80%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">VOTRE AVATAR</h4>
			</div>
			<div class="modal-body">
				<iframe src="jcrop.php?dirimg=local/images/avatar&lbico=user_avatar&igico=img_avatar" frameborder=0 width="100%" height="520px"></iframe>
			</div>
		</div>
	  </div>
	</div>
<?

echo "</form>";

//-- FOOTER ---------------------------------------------------------------------------------------------------------------------------------

	include("footer.php");
?>

	<script type="text/JavaScript">
		$('#user_login').focus();
		
		function recharge() {
			parent.document.location.reload();
		}
		
		function closemodal() {
			$('#mymodal').modal('hide');
		}
		
		<?php echo $jsaction ?>
		
		
	</script>








	
	
