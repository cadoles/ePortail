<?
	include("include/include.php");
	include("header.php");
	
	$db=new ps_db();
	
	echo "<body>";

	// Affichage des catégorie
	$div=array();
	echo "<ul id='navitem' class='nav nav-tabs'>";
	foreach ($tbflux as $flux) {
		if($firstflux=="") $firstflux=$flux[1];
		
		if($flux[3]==0) {
			echo "<li id='rss-".$flux[1]."'><a onClick='loadRSS(\"".$flux[4]."\",".$flux[5].");'>".$flux[2]."</a></li>";
		}
		elseif($flux[3]==1) {
			echo "<li id='rss-".$flux[1]."'><a onClick='loadArticle(".$flux[1].");'>".$flux[2]."</a></li>";
		}
		elseif($flux[3]==2) {
			echo "<li id='rss-".$flux[1]."'><a onClick='loadURL(\"".$flux[4]."\");'>".$flux[2]."</a></li>";
		}		
	}	
	echo "</ul>";
	
	echo "<div id='divRss'></div>";
	
	include("footer.php");
	
	$fctload="$('#rss-".$firstflux." a').click();";
?>

<!-- SCRIPT --------------------------------------------------------------------------------------------------------------------------------------- -->  
<script src="lib/feedek/FeedEk.js"></script>
<script>
	function loadArticle(id) {
		$('#divRss').load('process/process-flux.php?id='+id);
	}

	function loadURL(url) {
		$('#divRss').load(url);
	}

	function loadRSS(url,maxrss) {
		$('#divRss').FeedEk({
			FeedUrl : url,
			MaxCount : maxrss,
			ShowDesc : true,
			ShowPubDate: true
		});
	}		

	$(document).on('click', '#divRss a', function() {
		window.open($(this).attr('href'));
		return false;
	});	 
			
	<? echo $fctload; ?>
</script>

