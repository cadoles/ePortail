<?

	include_once("include/include.php");
	include("header.php");	
	
	$db1=new ps_db;
	$db2=new ps_db;
	
	$idpan=$_GET['idpan'];
	$idwid=$_GET['idwid'];
	$tpwid=$_GET['tpwid'];
	
	if($idpan=="")  $idpan=$_POST['idpan'];
	if($tpwid=="")  $tpwid=$_POST['tpwid'];
	if($idpwd=="")  $idpwd=$_POST['idpwd'];

	$tpmod	=$_POST['tpmod'];
	$vlmod	=$_POST['vlmod'];
	$vladd	=$_POST['vladd'];
	$vlsup	=$_POST['vlsup'];
	$fgclo =$_POST['fgclo'];
		
	$id   =$_POST['id'];
	$html =$_POST['html'];
	$order=$_POST['order'];
	$image=$_POST['image'];
	$url  =$_POST['url'];
	
	// Permissions = si non utiisateur exist
	if($_SESSION['user_id']=="") die();	
	
	// Permissions = si non propriéataire de la page exist
	$q="SELECT panel_user FROM env_panel WHERE panel_id=".$idpan;
	$db1->query($q);
	if($db1->next_record()) {
		$idpro=$db1->f('panel_user');
	}
	if($idpro!=$_SESSION['user_id']&&$_SESSION['user_profil']!=1) {
		die();
	}	


	if($tpwid=="carousel") {
		/*--> Controle de cohérance */
		if($vladd!=""||$vlmod!="") {
			$fgerr="";
			if($image==""||$order=="") {
				$jsaction="alert('Vous devez renseigner l\'ensemble des informations');";
				$fgerr=1;
			}
		}
		
		/*--> Rester sur le mode encours */
		if($vlmod!=""&&fgerr!="") $tpmod="MODIFY";
		if($vladd!=""&&fgerr!="") $tpmod="SUBMIT";
			
		/*--> Submit */
		if($vladd!=""&&$fgerr=="") {
			$html=str_replace(chr(10),'',urldecode($html));
			$q="INSERT INTO env_panel_widget_carousel(panel_widget_carousel_html, panel_widget_carousel_img, panel_widget_carousel_url, panel_widget_carousel_order, panel_widget_carousel_widget) VALUES('$html','$image','$url','$order','$idwid')";
			$db1->query($q);
			$tpmod="";
		}

		/*--> Modify */
		if($vlmod!=""&&$fgerr=="") {
			$html=str_replace(chr(10),'',urldecode($html));
			$q="UPDATE env_panel_widget_carousel SET panel_widget_carousel_html='$html', panel_widget_carousel_img='$image', panel_widget_carousel_url='$url', panel_widget_carousel_order='$order',panel_widget_carousel_widget='$idwid' WHERE panel_widget_carousel_id=$id";
			$db1->query($q);
			$tpmod="";
		}

		/*--> Delete */
		if($vlsup!="") {
			$q="DELETE FROM env_panel_widget_carousel WHERE panel_widget_carousel_id=$id";
			$db1->query($q);
			$tpmod="";
		}			
	}
	
	if($fgclo!="") {
		// Action javascript Ajout du panel directement + fermeture de la popup
		$jsaction="recharge(".$idpan.")";
	}
	
?>

<script src="lib/ckeditor/ckeditor.js"></script>	
<script type="text/JavaScript">
	function closeMe() {
		window.parent.closemodal();	
	}
	
	function recharge(idpan) {
		parent.parent.document.location.href='index.php?action=page&id='+idpan;
	}
	
	function createEditor()
	{
		var heightbody = "150px";
		
		CKEDITOR.config.toolbar_Basic =
		[
			{ name: 'document', items : ['Bold','Italic','Underline'] },
			{ name: 'styles', items : [ 'Styles','Format','Font','FontSize','Link'] }
		];
		
		CKEDITOR.config.toolbar = 'Basic';
		CKEDITOR.config.height = heightbody-120;
	
		// Create a new editor inside the <div id="editor">, setting its value to html
		var config = {};
		editor = CKEDITOR.appendTo( 'editor', config, html );
	}	
	
	function saveEditor()
	{
		document.getElementById( 'html' ).value = html = encodeURIComponent(editor.getData());
	}
	
	<?php echo $jsaction ?>
</script>



<?
//== SELECTION DU WIDGET =====================================================================================================================================================================

echo "<div id='page-wrapper'>";
echo "<div class='container-fluid'>";
echo "<form id='formulaire' class='form-horizontal' role='form' method='post' enctype='multipart/form-data'>";
echo "<input type='hidden' name='idpan' id='idpan' value='$idpan' />";
echo "<input type='hidden' name='idwid' id='idwid' value='$idwid' />";
echo "<input type='hidden' name='idpwd' id='idpwd' value='$idpwd' />";
echo "<input type='hidden' name='id'    id='id'    value='$id'    />";

echo "<input type='hidden' name='tpmod' id='tpmod' />";

//-- DISPLAY -------------------------------------------------------------------------------------------------------------------------------

if($tpmod=="") {
	
	if($tpwid="carousel") {
		echo "<div class='form-group'>";
		echo "<div class='col-sm-12'>";	
		echo "<a class='btn btn-primary' onclick='recharge(".$idpan.");'>Fermer</a>";
		echo "&nbsp";
		echo "<input id='fgadd' name='fgadd' class='btn btn-primary' onClick='$(\"#id\").val(\"\"); $(\"#tpmod\").val(\"SUBMIT\");' type='submit' value='Ajouter' />";
		echo "</div>";
		echo "</div>";	
			
		echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
		echo "<thead>";
		echo "<th width='70px'>Action</th>";
		echo "<th width='70px'>Order</th>";
		echo "<th>Texte</th>";
		echo "<th>Image</th>";
		echo "</thead>";
		
		$q="SELECT * FROM env_panel_widget_carousel WHERE panel_widget_carousel_widget=$idwid ORDER BY panel_widget_carousel_order";
		$db1->query($q);
		while($db1->next_record()){	
			echo "<tr style='font-size:90%'>";

			echo "<td align='center'>";
			echo "<a style='cursor:pointer' class='glyphicon glyphicon-file'   onClick='$(\"#id\").val(\"".$db1->f('panel_widget_carousel_id')."\"); $(\"#tpmod\").val(\"MODIFY\"); $(\"#formulaire\").submit();'></a>";
			echo "<a style='cursor:pointer' class='glyphicon glyphicon-remove' onClick='$(\"#id\").val(\"".$db1->f('panel_widget_carousel_id')."\"); $(\"#tpmod\").val(\"DELETE\"); $(\"#formulaire\").submit();'></a>";
			echo "</td>";

			echo "<td align='center'>";
			echo $db1->f('panel_widget_carousel_order');
			echo "</td>";
			
			echo "<td align='center'>";
			echo $db1->f('panel_widget_carousel_html');
			echo "</td>";
			
			echo "<td align='center'>";
			if($db1->f('panel_widget_carousel_url')!="") echo "<a href='".$db1->f('panel_widget_carousel_url')."' target='_blank'>";
			echo "<img src='local/images/carousel/".$db1->f('panel_widget_carousel_img')."' width='100%' style='float:left'></img>";
			if($db1->f('panel_widget_carousel_url')!="") echo "</a>";
			echo "</td>";
			
			echo "</tr>";
		}	
		echo "</table>";
	}
}

//-- SUBMIT --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="SUBMIT") {

	if($tpwid="carousel") {	
		echo "<div class='form-group'>";
		echo "<div class='col-sm-12'>";
		echo "<input id='vladd' name='vladd' class='btn btn-primary' type='submit' onClick='saveEditor();' value='Valider' />";
		echo "&nbsp;";
		echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
		echo "</div>";
		echo "</div>";	
	}
}

//-- MODIFY --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="MODIFY") {

	if($tpwid="carousel") {	
		echo "<div class='form-group'>";
		echo "<div class='col-sm-12'>";
		echo "<input id='vlmod' name='vlmod' class='btn btn-primary' type='submit' onClick='saveEditor();' value='Valider' />";
		echo "&nbsp;";
		echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
		echo "</div>";
		echo "</div>";	

		// Valeur par défaut
		$q = "SELECT * FROM env_panel_widget_carousel WHERE panel_widget_carousel_id=$id AND panel_widget_carousel_widget=$idwid";
		$db1->query($q);
		if($db1->next_record()) {
			$html	= $db1->f('panel_widget_carousel_html');
			$image	= $db1->f('panel_widget_carousel_img');
			$url	= $db1->f('panel_widget_carousel_url');
			$order	= $db1->f('panel_widget_carousel_order');
		}
	}
}

//-- DELETE --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="DELETE") {
	if($tpwid="carousel") {		
		echo "<div class='form-group'>";
		echo "<div class='col-sm-12'>";
		echo "<input id='vlsup' name='vlsup' class='btn btn-primary' type='submit' value='Confirmer la suppression' />";
		echo "&nbsp;";
		echo "<input class='btn btn-primary' type='submit' value='Annuler' />";    
		echo "</div>";
		echo "</div>";
		
		$q = "SELECT * FROM env_panel_widget_carousel WHERE panel_widget_carousel_id=$id AND panel_widget_carousel_widget=$idwid";
		$db1->query($q);
		if($db1->next_record()) {
			$html	= $db1->f('panel_widget_carousel_html');
			$image	= $db1->f('panel_widget_carousel_img');
			$url	= $db1->f('panel_widget_carousel_url');
			$order	= $db1->f('panel_widget_carousel_order');
		}
	}
}


//-- FORMULAIRE -----------------------------------------------------------------------------------------------------------------------------

if($tpmod=="SUBMIT"||$tpmod=="MODIFY"||$tpmod=="DELETE") {
?>

	<? 	if($tpwid="carousel") {	?>
	<fieldset class="row fieldset" style="clear:both">
		<div class="form-group">
			<label for="order" class="col-sm-3 control-label">Ordre*</label>
			<div class="col-sm-6"><input name="order" id="order" type="number" class="form-control" placeholder="Ordre" value="<? echo $order; ?>"></div>
		</div>

		<div class="form-group">
			<label for="url" class="col-sm-3 control-label">URL</label>
			<div class="col-sm-6"><input name="url" id="url" type="titre" class="form-control" placeholder="Url" value="<? echo $url; ?>"></div>
		</div>
				
		<!-- This div will hold the editor. -->
		<div id="editor"></div>
		<input type='hidden' name='html' id='html'/>
		</div>		
		
		<div>
		<br>
		<?
			echo "<a id='addimage' title='Changer Image' class='btn btn-primary' onclick='changeFrame($idpan,$idwid,\"carousel\");' style='cursor:pointer;'>Changer Image</a>";
			echo "<input type='hidden' name='image' id='image' value='$image' />";
		?>
		<img id="scrimage" src="local/images/carousel/<? echo $image; ?>" width="100%">
		</div>
			
	</fieldset>

	<? }
}

echo "</form></div></div>";

?>

<!-- Popup gestion widget !-->
<div id="mymodal" class="modal fade bs-item-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" style="width:100%;">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title">IMAGE</h4>
		</div>
		<div class="modal-body">
			<iframe id="framemodal" frameborder=0 width="100%" height="395px"></iframe>
		</div>
	</div>
  </div>
</div>	

<!-- FOOTER --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<?
	include($repository."footer.php");
?>

<!-- SCRIPT --------------------------------------------------------------------------------------------------------------------------------------- -->  	
	<script>
		$(document).ready(function() {
			<? if($tpmod=="") { ?>
			$('#datatable').dataTable( {
				"oLanguage": { "sUrl": "<?echo $repository; ?>lib/datatables/dataTables.txt" },
				"iDisplayLength": 50,
				"aaSorting": [[ 1, "asc" ]]
			} );
			<? } ?>
			
			<? if($tpmod!=""&&$tpwid=="carousel") { ?>
			html = '<? echo str_replace(chr(10),'',$html); ?>';
			createEditor();
			<? } ?>
		} );	

		function changeFrame(idpan,idpwd,tpwid) {
			if(tpwid=="carousel") {
				srcframe="dropzone.php?directory=local/images/carousel&typeupload=image&prefix=thumbnail_&idinput=image&idimage=scrimage";
			}
			
			$("#framemodal").attr("src",srcframe);
			$('#mymodal').modal('show');
		}

		function closemodal() {
			$('#mymodal').modal('hide');
		}
		
		<?php echo $jsaction ?>
	</script>


