<?
	include("include/include.php");
	include("header.php");
	
	$i=$_GET["id"];
	$db=new ps_db();
	
	if($tbalert[$i]['type']=='0')
		$fctload="loadRSS('".$tbalert[$i]['rss']."',".$tbalert[$i]['maxrss'].");";
	elseif($tbalert[$i]['type']=='2')
		$fctload="loadURL('".$tbalert[$i]['rss']."');";
	else {
		$db->query("SELECT * FROM alert WHERE idalert=".$tbalert[$i]['id']);
		if ($db->next_record()) {
			$maxrss=$db->f('nbalert');
			
			$db->query("SELECT * FROM alertmsg WHERE idalert=".$tbalert[$i]['id']." ORDER BY datealert DESC LIMIT ".$maxrss);
			while ($db->next_record()) {
				$html=$html."<div class='ItemTitle'><a>".$db->f('titrealert')."</a></div>";
				$html=$html."<div class='ItemDate'>".$db->f('datealert')."</div>";
				$html=$html."<div class='ItemContent'>".$db->f('textalert')."</div>";
			}
		}		
		
		$fctload="";	
	}									
?>

  
<!-- BODY ----------------------------------------------------------------------------------------------------------------------------------------- -->  

<body class="bodyRss">
	<!-- Conteneur flux -->
	<div id="divRss"><? echo $html; ?></div>
	<div style="height:10px"></div>
	
<!-- FOOTER --------------------------------------------------------------------------------------------------------------------------------------- -->  	
	<?
		include("footer.php");
	?>

<!-- SCRIPT --------------------------------------------------------------------------------------------------------------------------------------- -->  
<script src="lib/feedek/FeedEk.js"></script>
<script>
	function loadAlert() {
		$('#divRss').load('process-alert.php?idalert='+$('#idalert').val());
		$("#divRss a").attr("target","_blank");
	}

	function loadURL(url) {
		$('#divRss').load(url);
		$("#divRss a").attr("target","_blank");
	}

	function loadRSS(url,maxrss) {
		$('#divRss').FeedEk({
			FeedUrl : url,
			MaxCount : maxrss,
			ShowDesc : true,
			ShowPubDate: true
		});
	}		
	
	<? echo $fctload; ?>
	
	// Reload toutes les 5 minutes
	setTimeout("location.reload()",350000);
</script>

