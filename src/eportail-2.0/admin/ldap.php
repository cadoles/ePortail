<? 
	$repository="../";
	include($repository."include/include.php");
	include($repository."header.php"); 
		
	if($_SESSION['user_profil'] != 1 || !$config["consoleadmin"] || !$config["activerLDAPModif"]) {
		echo "<br><br><br><center>Vous n'avez les permissions nécessaire</center>";
	}
	
	if($_SESSION['user_profil'] == 1 && $config["consoleadmin"]) {
		echo "<body>";

	$fgval			=$_POST["fgval"];
	
	$activerLDAP	=$_POST["activerLDAP"];
	$LDAPserver		=$_POST["LDAPserver"];
	$LDAPport		=$_POST["LDAPport"];
	$LDAPreaderdn	=$_POST["LDAPreaderdn"];
	$LDAPreaderpw	=$_POST["LDAPreaderpw"];
	$LDAPracine		=$_POST["LDAPracine"];
	$LDAPfilteruser	=$_POST["LDAPfilteruser"];
	$LDAPfirstname	=$_POST["LDAPfirstname"];
	$LDAPlastname	=$_POST["LDAPlastname"];
	$LDAPemail		=$_POST["LDAPemail"];

/*

*/
	
	if($fgval!="") {
		$fp=fopen($config['localdirectory']."/local/config/ldap.php","w");
		fwrite($fp, "<?\n");
		fwrite($fp, "\$config['activerLDAP']	= $activerLDAP;\n");
		fwrite($fp, "\$config['LDAPserver']		= '$LDAPserver';\n");
		fwrite($fp, "\$config['LDAPport'] 		= '$LDAPport';\n");
		fwrite($fp, "\$config['LDAPreaderdn'] 	= '$LDAPreaderdn';\n");
		fwrite($fp, "\$config['LDAPreaderpw']	= '$LDAPreaderpw';\n");
		fwrite($fp, "\$config['LDAPracine'] 	= '$LDAPracine';\n");
		fwrite($fp, "\$config['LDAPfilteruser'] = '$LDAPfilteruser';\n");
		fwrite($fp, "\$config['LDAPfirstname'] 	= '$LDAPfirstname';\n");
		fwrite($fp, "\$config['LDAPlastname'] 	= '$LDAPlastname';\n");
		fwrite($fp, "\$config['LDAPemail'] 		= '$LDAPemail';\n");
		fwrite($fp, "\n?>");
		fclose($fp);
		
		// Reload de la page
		include($repository."local/config/ldap.php");
	}
?>

<div id="wrapper">
	<? include("header.php"); ?>
	
	<div id="page-wrapper">
		<div class="container-fluid">
			<form class="form-horizontal" role="form" method='post' enctype="multipart/form-data">
				<legend><h1>CONFIGURATION LDAP</h1></legend>
				<input type="hidden" name="MAX_FILE_SIZE" value="2097152">
				
				<div class="form-group">
					<div class="col-sm-12">
						<input name="fgval" type="submit" class="btn btn-primary" value="Enregistrer">
					</div>
				</div>

				<div class="form-group">
					<label for="activerLDAP" class="col-sm-3 control-label">Activer LDAP*</label>
					<div class="col-sm-6">
						<select name="activerLDAP" id="activerLDAP" class="form-control">
							<option value="false" <? if($config['activerLDAP']==false) echo 'selected';?> >non</option>
							<option value="true"  <? if($config['activerLDAP']==true)  echo 'selected';?> >oui</option>
						</select>			
					</div>
				</div>

				<div class="form-group">
					<label for="LDAPserver" class="col-sm-3 control-label">Adresse serveur*</label>
					<div class="col-sm-6">
						<input name="LDAPserver" id="LDAPserver" type="titre" class="form-control" placeholder="Titre" value="<? echo $config['LDAPserver']; ?>">
					</div>
				</div>

				<div class="form-group">
					<label for="LDAPport" class="col-sm-3 control-label">Port*</label>
					<div class="col-sm-6">
						<input name="LDAPport" id="LDAPport" type="titre" class="form-control" placeholder="Port" value="<? echo $config['LDAPport']; ?>">
					</div>
				</div>
								
				<div class="form-group">
					<label for="LDAPport" class="col-sm-3 control-label">Login*</label>
					<div class="col-sm-6">
						<input name="LDAPreaderdn" id="LDAPreaderdn" type="titre" class="form-control" placeholder="Login" value="<? echo $config['LDAPreaderdn']; ?>">
					</div>
				</div>
												
				<div class="form-group">
					<label for="LDAPreaderpw" class="col-sm-3 control-label">Mot de Passe*</label>
					<div class="col-sm-6">
						<input name="LDAPreaderpw" id="LDAPreaderpw" type="password" class="form-control" placeholder="Mot de Passe" value="<? echo $config['LDAPreaderpw']; ?>">
					</div>
				</div>

				<div class="form-group">
					<label for="LDAPfilteruser" class="col-sm-3 control-label">Filtre Utilisateur*</label>
					<div class="col-sm-6">
						<input name="LDAPfilteruser" id="LDAPfilteruser" type="titre" class="form-control" placeholder="Filtre Utilisateur" value="<? echo $config['LDAPfilteruser']; ?>">
					</div>
				</div>	
				
				<div class="form-group">
					<label for="LDAPfirstname" class="col-sm-3 control-label">Atribut Prénom*</label>
					<div class="col-sm-6">
						<input name="LDAPfirstname" id="LDAPfirstname" type="titre" class="form-control" placeholder="Atribut Prénom" value="<? echo $config['LDAPfirstname']; ?>">
					</div>
				</div>				

				<div class="form-group">
					<label for="LDAPlastname" class="col-sm-3 control-label">Atribut Nom*</label>
					<div class="col-sm-6">
						<input name="LDAPlastname" id="LDAPlastname" type="titre" class="form-control" placeholder="Atribut Nom" value="<? echo $config['LDAPlastname']; ?>">
					</div>
				</div>	

				<div class="form-group">
					<label for="LDAPemail" class="col-sm-3 control-label">Atribut Email*</label>
					<div class="col-sm-6">
						<input name="LDAPemail" id="LDAPemail" type="titre" class="form-control" placeholder="Atribut Email" value="<? echo $config['LDAPemail']; ?>">
					</div>
				</div>									
			</form>
		</div>
	</div>
</div>
<? } ?>

<!-- FOOTER --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<?
	include($repository."footer.php");
?>

<!-- SCRIPT --------------------------------------------------------------------------------------------------------------------------------------- -->
<script>
	function ShowHide() {
		if($("#locallogo").val()=="1") {
			$('#form-group-locallogo').css('display','block');
			
		}
		else {
			$('#form-group-locallogo').css('display','none');
		}
		
		if($("#localheader").val()=="1") {
			$('#form-group-localheader').css('display','block');
			
		}
		else {
			$('#form-group-localheader').css('display','none');
		}		
	}	

	$(document).ready(function()
	{
		ShowHide();
	});
</script>
