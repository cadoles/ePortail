<?
	$repository="../";
	include($repository."include/include.php");
	include($repository."include/delete.php");
	include($repository."header.php"); 
		
	if($_SESSION['user_profil'] != 1 || !$config["consoleadmin"]) {
		echo "<br><br><br><center>Vous n'avez les permissions nécessaire</center>";
	}
	
	if($_SESSION['user_profil'] == 1 && $config["consoleadmin"]) {
		echo "<body>";
	
	$db1=new ps_db;

	$tptbl	=$_GET['tptbl'];
	$tpmod	=$_GET['tpmod'];
	$id1	=$_GET['id1'];
	$id2	=$_GET['id2'];
	
	if($id1=="")   $id1=$_POST['id1'];
	if($id2=="")   $id2=$_POST['id2'];
	if($tptbl=="") $tptbl=$_POST['tptbl'];
	
	$vladd=$_POST['vladd'];	
	$vlmod=$_POST['vlmod'];	
	$fgclo=$_POST['fgclo'];
	
	if($vladd!="") {
		if($tptbl=="ITEM") {
			$q="INSERT INTO env_application_profil (application_profil_profil,application_profil_application) VALUES($id1,$id2)";
			$db1->query($q);		
		}	
		elseif($tptbl=="ITEMLDAP") {
			$q="INSERT INTO env_application_ldap_profil (application_ldap_profil_ldap_profil,application_ldap_profil_application) VALUES($id1,$id2)";
			$db1->query($q);		
		}	
		elseif($tptbl=="PAGE") {
			$q="INSERT INTO env_panel_profil (panel_profil_profil,panel_profil_panel) VALUES($id1,$id2)";
			$db1->query($q);					
		}		
		elseif($tptbl=="PAGELDAP") {
			$q="INSERT INTO env_panel_ldap_profil (panel_ldap_profil_ldap_profil,panel_ldap_profil_panel) VALUES($id1,$id2)";
			$db1->query($q);					
		}
		elseif($tptbl=="SSOCOMMUNITY") {
			$q="INSERT INTO env_sso_community_attribut (sso_community_attribut_community,sso_community_attribut_attribut) VALUES($id1,$id2)";
			$db1->query($q);					
		}			
		elseif($tptbl=="SSOPROFIL") {
			$q="INSERT INTO env_sso_profil_attribut (sso_profil_attribut_profil,sso_profil_attribut_attribut) VALUES($id1,$id2)";
			$db1->query($q);					
		}	
		elseif($tptbl=="PAGESSO") {
			$q="INSERT INTO env_panel_sso_profil (panel_sso_profil_sso_profil,panel_sso_profil_panel) VALUES($id1,$id2)";
			$db1->query($q);
		}
		elseif($tptbl=="ITEMSSO") {
			$q="INSERT INTO env_application_sso_profil (application_sso_profil_sso_profil,application_sso_profil_application) VALUES($id1,$id2)";
			$db1->query($q);		
		}
		elseif($tptbl=="FLUX") {
			$q="INSERT INTO env_flux_profil (flux_profil_profil,flux_profil_flux) VALUES($id1,$id2)";
			$db1->query($q);					
		}					
		elseif($tptbl=="FLUXSSO") {
			$q="INSERT INTO env_flux_sso_profil (flux_sso_profil_profil,flux_sso_profil_flux) VALUES($id1,$id2)";
			$db1->query($q);					
		}
		elseif($tptbl=="FLUXLDAP") {
			$q="INSERT INTO env_flux_ldap_profil (flux_ldap_profil_profil,flux_ldap_profil_flux) VALUES($id1,$id2)";
			$db1->query($q);					
		}		
	}
	
	if($tpmod=="DELETE") {
		if($tptbl=="ITEM") {
			$q="DELETE FROM env_application_profil WHERE application_profil_profil=$id1 AND application_profil_application=$id2";
			$db1->query($q);			
		}
		elseif($tptbl=="ITEMLDAP") {
			$q="DELETE FROM env_application_ldap_profil WHERE application_ldap_profil_ldap_profil=$id1 AND application_ldap_profil_application=$id2";
			$db1->query($q);			
		}
		elseif($tptbl=="PAGE") {
			$q="DELETE FROM env_panel_profil WHERE panel_profil_profil=$id1 AND panel_profil_panel=$id2";
			$db1->query($q);					
		}	
		elseif($tptbl=="PAGELDAP") {
			$q="DELETE FROM env_panel_ldap_profil WHERE panel_ldap_profil_ldap_profil=$id1 AND panel_ldap_profil_panel=$id2";
			$db1->query($q);					
		}
		elseif($tptbl=="SSOCOMMUNITY") {
			$q="DELETE FROM env_sso_community_attribut WHERE sso_community_attribut_community=$id1 AND sso_community_attribut_attribut=$id2";
			$db1->query($q);					
		}	
		elseif($tptbl=="SSOPROFIL") {
			$q="DELETE FROM env_sso_profil_attribut WHERE sso_profil_attribut_profil=$id1 AND sso_profil_attribut_attribut=$id2";
			$db1->query($q);					
		}	
		elseif($tptbl=="PAGESSO") {
			$q="DELETE FROM env_panel_sso_profil WHERE panel_sso_profil_sso_profil=$id1 AND panel_sso_profil_panel=$id2";
			$db1->query($q);					
		}
		elseif($tptbl=="ITEMSSO") {
			$q="DELETE FROM env_application_sso_profil WHERE application_sso_profil_sso_profil=$id1 AND application_sso_profil_application=$id2";
			$db1->query($q);			
		}		
		elseif($tptbl=="FLUX") {
			$q="DELETE FROM env_flux_profil WHERE flux_profil_profil=$id1 AND flux_profil_flux=$id2";
			$db1->query($q);					
		}	
		elseif($tptbl=="FLUXSSO") {
			$q="DELETE FROM env_flux_sso_profil WHERE flux_sso_profil_profil=$id1 AND flux_sso_profil_flux=$id2";
			$db1->query($q);					
		}
		elseif($tptbl=="FLUXLDAP") {
			$q="DELETE FROM env_flux_ldap_profil WHERE flux_ldap_profil_profil=$id1 AND flux_ldap_profil_flux=$id2";
			$db1->query($q);					
		}
				
		$jsaction="recharge()";
	}

	
	if($fgclo!="") {
		$jsaction="recharge()";
	}	


if($tpmod=="") {
	echo "<form id='formulaire' class='form-horizontal' role='form' method='post' enctype='multipart/form-data'>";

	echo "<input id='id1'   name='id1'   type='hidden' value='".$id1."'>";
	echo "<input id='id2'   name='id2'   type='hidden' value='".$id2."'>";
	echo "<input id='tptbl' name='tptbl' type='hidden' value='".$tptbl."'>";
	echo "<input id='vladd' name='vladd' type='hidden' value=''>";
	
	echo "<a class='btn btn-primary' onclick='recharge();'>Fermer</a><br><br>";
	
	
	if($tptbl=="ITEM") {
		echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
		echo "<thead>";
		echo "<th width='40px'>Action</th>";
		echo "<th width='70px'>Icône</th>";
		echo "<th>Applications</th>";
		echo "</thead>";
	
		$q="SELECT * FROM env_application,env_icon WHERE application_icon=icon_id AND NOT EXISTS(SELECT application_profil_profil FROM env_application_profil WHERE application_profil_profil=$id1 AND application_profil_application=application_id)";
		$db1->query($q);
		while($db1->next_record()){	
			echo "<tr>";

			echo "<td>";
			echo "<a class='glyphicon glyphicon-plus' onClick='$(\"#id2\").val(\"".$db1->f('application_id')."\"); $(\"#vladd\").val(\"SUBMIT\");$(\"#formulaire\").submit();'></a>";
			echo "</td>";

			echo "<td>";
			echo "<a href='".urldecode($db1->f('application_url'))."' target='_blank'><img src='".$repository."local/images/icon/".$db1->f('icon_url')."' width='40px' height='40px'></img></a>";
			echo "</td>";
		
			echo "<td>";
			echo "<a href='".urldecode($db1->f('application_url'))."' target='_blank'>".$db1->f("application_label")."</a><br>";
			echo "</td>";
	
			echo "</tr>";
		}

		echo "</table>";
	}

	if($tptbl=="ITEMLDAP") {
		echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
		echo "<thead>";
		echo "<th width='40px'>Action</th>";
		echo "<th width='70px'>Icône</th>";
		echo "<th>Applications</th>";
		echo "</thead>";
	
		$q="SELECT * FROM env_application,env_icon WHERE application_icon=icon_id AND NOT EXISTS(SELECT application_ldap_profil_ldap_profil FROM env_application_ldap_profil WHERE application_ldap_profil_ldap_profil=$id1 AND application_ldap_profil_application=application_id)";
		$db1->query($q);
		while($db1->next_record()){	
			echo "<tr>";

			echo "<td>";
			echo "<a class='glyphicon glyphicon-plus' onClick='$(\"#id2\").val(\"".$db1->f('application_id')."\"); $(\"#vladd\").val(\"SUBMIT\");$(\"#formulaire\").submit();'></a>";
			echo "</td>";

			echo "<td>";
			echo "<a href='".urldecode($db1->f('application_url'))."' target='_blank'><img src='".$repository."local/images/icon/".$db1->f('icon_url')."' width='40px' height='40px'></img></a>";
			echo "</td>";
		
			echo "<td>";
			echo "<a href='".urldecode($db1->f('application_url'))."' target='_blank'>".$db1->f("application_label")."</a><br>";
			echo "</td>";
	
			echo "</tr>";
		}

		echo "</table>";
	}
		
	elseif($tptbl=="PAGE") {
		echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
		echo "<thead>";
		echo "<th width='40px'>Action</th>";
		echo "<th>Page</th>";
		echo "<th>Type</th>";
		echo "<th>Propriétaire</th>";
		echo "</thead>";

		$q="SELECT * FROM env_panel, env_panel_type, env_user, env_profil
			WHERE panel_type_id=panel_type
			AND user_id=panel_user
			AND profil_id=user_profil
			AND NOT EXISTS(SELECT panel_profil_panel FROM env_panel_profil WHERE panel_profil_panel=panel_id AND panel_profil_profil=$id1)
			ORDER BY panel_order DESC";
		$db1->query($q);
		
		while($db1->next_record()){	
			echo "<tr>";

			echo "<td>";
			echo "<a class='glyphicon glyphicon-plus' onClick='$(\"#id2\").val(\"".$db1->f('panel_id')."\"); $(\"#vladd\").val(\"SUBMIT\");$(\"#formulaire\").submit();'></a>";
			echo "</td>";

			echo "<td>";
			echo $db1->f("panel_label");
			echo "</td>";
		
			echo "<td>";
			echo $db1->f("panel_type_name");
			echo "</td>";

			echo "<td>";
			echo $db1->f('user_login');
			echo "</td>";
	
			echo "</tr>";
		}

		echo "</table>";
	}

	elseif($tptbl=="PAGELDAP") {
		echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
		echo "<thead>";
		echo "<th width='40px'>Action</th>";
		echo "<th>Page</th>";
		echo "<th>Type</th>";
		echo "<th>Propriétaire</th>";
		echo "</thead>";

		$q="SELECT * FROM env_panel, env_panel_type, env_user, env_profil
			WHERE panel_type_id=panel_type
			AND user_id=panel_user
			AND profil_id=user_profil
			AND NOT EXISTS(SELECT panel_ldap_profil_panel FROM env_panel_ldap_profil WHERE panel_ldap_profil_panel=panel_id AND panel_ldap_profil_ldap_profil=$id1)
			ORDER BY panel_order DESC";
		$db1->query($q);
		
		while($db1->next_record()){	
			echo "<tr>";

			echo "<td>";
			echo "<a class='glyphicon glyphicon-plus' onClick='$(\"#id2\").val(\"".$db1->f('panel_id')."\"); $(\"#vladd\").val(\"SUBMIT\");$(\"#formulaire\").submit();'></a>";
			echo "</td>";

			echo "<td>";
			echo $db1->f("panel_label");
			echo "</td>";
		
			echo "<td>";
			echo $db1->f("panel_type_name");
			echo "</td>";

			echo "<td>";
			echo $db1->f('user_login');
			echo "</td>";
	
			echo "</tr>";
		}

		echo "</table>";
	}
	
	elseif($tptbl=="SSOCOMMUNITY") {
		echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
		echo "<thead>";
		echo "<th width='40px'>Action</th>";
		echo "<th>Nom</th>";
		echo "<th>Valeur</th>";
		echo "</thead>";

		$q="SELECT * FROM env_sso_attribut
			WHERE NOT EXISTS(SELECT sso_community_attribut_community FROM env_sso_community_attribut WHERE sso_community_attribut_attribut=sso_attribut_id AND sso_community_attribut_community=$id1)";
		$db1->query($q);
		
		while($db1->next_record()){	
			echo "<tr>";

			echo "<td>";
			echo "<a class='glyphicon glyphicon-plus' onClick='$(\"#id2\").val(\"".$db1->f('sso_attribut_id')."\"); $(\"#vladd\").val(\"SUBMIT\");$(\"#formulaire\").submit();'></a>";
			echo "</td>";

			echo "<td>";
			echo $db1->f("sso_attribut_name");
			echo "</td>";
		
			echo "<td>";
			echo $db1->f("sso_attribut_value");
			echo "</td>";

			echo "</tr>";
		}

		echo "</table>";
	}	


	elseif($tptbl=="SSOPROFIL") {
		echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
		echo "<thead>";
		echo "<th width='40px'>Action</th>";
		echo "<th>Nom</th>";
		echo "<th>Valeur</th>";
		echo "</thead>";

		$q="SELECT * FROM env_sso_attribut
			WHERE NOT EXISTS(SELECT sso_profil_attribut_profil FROM env_sso_profil_attribut WHERE sso_profil_attribut_attribut=sso_attribut_id AND sso_profil_attribut_profil=$id1)";
		$db1->query($q);
		
		while($db1->next_record()){	
			echo "<tr>";

			echo "<td>";
			echo "<a class='glyphicon glyphicon-plus' onClick='$(\"#id2\").val(\"".$db1->f('sso_attribut_id')."\"); $(\"#vladd\").val(\"SUBMIT\");$(\"#formulaire\").submit();'></a>";
			echo "</td>";

			echo "<td>";
			echo $db1->f("sso_attribut_name");
			echo "</td>";
		
			echo "<td>";
			echo $db1->f("sso_attribut_value");
			echo "</td>";

			echo "</tr>";
		}

		echo "</table>";
	}	
	
	elseif($tptbl=="PAGESSO") {
		echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
		echo "<thead>";
		echo "<th width='40px'>Action</th>";
		echo "<th>Page</th>";
		echo "<th>Type</th>";
		echo "<th>Propriétaire</th>";
		echo "</thead>";

		$q="SELECT * FROM env_panel, env_panel_type, env_user, env_profil
			WHERE panel_type_id=panel_type
			AND user_id=panel_user
			AND profil_id=user_profil
			AND NOT EXISTS(SELECT panel_sso_profil_panel FROM env_panel_sso_profil WHERE panel_sso_profil_panel=panel_id AND panel_sso_profil_sso_profil=$id1)
			ORDER BY panel_order DESC";
		$db1->query($q);
		
		while($db1->next_record()){	
			echo "<tr>";

			echo "<td>";
			echo "<a class='glyphicon glyphicon-plus' onClick='$(\"#id2\").val(\"".$db1->f('panel_id')."\"); $(\"#vladd\").val(\"SUBMIT\");$(\"#formulaire\").submit();'></a>";
			echo "</td>";

			echo "<td>";
			echo $db1->f("panel_label");
			echo "</td>";
		
			echo "<td>";
			echo $db1->f("panel_type_name");
			echo "</td>";

			echo "<td>";
			echo $db1->f('user_login');
			echo "</td>";
	
			echo "</tr>";
		}

		echo "</table>";
	}	
	
	elseif($tptbl=="ITEMSSO") {
		echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
		echo "<thead>";
		echo "<th width='40px'>Action</th>";
		echo "<th width='70px'>Icône</th>";
		echo "<th>Applications</th>";
		echo "</thead>";
	
		$q="SELECT * FROM env_application,env_icon WHERE application_icon=icon_id AND NOT EXISTS(SELECT application_sso_profil_sso_profil FROM env_application_sso_profil WHERE application_sso_profil_sso_profil=$id1 AND application_sso_profil_application=application_id)";
		$db1->query($q);
		while($db1->next_record()){	
			echo "<tr>";

			echo "<td>";
			echo "<a class='glyphicon glyphicon-plus' onClick='$(\"#id2\").val(\"".$db1->f('application_id')."\"); $(\"#vladd\").val(\"SUBMIT\");$(\"#formulaire\").submit();'></a>";
			echo "</td>";

			echo "<td>";
			echo "<a href='".urldecode($db1->f('application_url'))."' target='_blank'><img src='".$repository."local/images/icon/".$db1->f('icon_url')."' width='40px' height='40px'></img></a>";
			echo "</td>";
		
			echo "<td>";
			echo "<a href='".urldecode($db1->f('application_url'))."' target='_blank'>".$db1->f("application_label")."</a><br>";
			echo "</td>";
	
			echo "</tr>";
		}

		echo "</table>";
	}


	elseif($tptbl=="FLUX") {
		echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
		echo "<thead>";
		echo "<th width='40px'>Action</th>";
		echo "<th>Flux</th>";
		echo "</thead>";

		$q="SELECT * FROM env_flux
			WHERE NOT EXISTS(SELECT flux_profil_flux FROM env_flux_profil WHERE flux_profil_flux=flux_id AND flux_profil_profil=$id1)
			ORDER BY flux_order DESC";
		$db1->query($q);
		
		while($db1->next_record()){	
			echo "<tr>";

			echo "<td>";
			echo "<a class='glyphicon glyphicon-plus' onClick='$(\"#id2\").val(\"".$db1->f('flux_id')."\"); $(\"#vladd\").val(\"SUBMIT\");$(\"#formulaire\").submit();'></a>";
			echo "</td>";

			echo "<td>";
			echo $db1->f("flux_name");
			echo "</td>";
	
	
			echo "</tr>";
		}
	}
	
	elseif($tptbl=="FLUXSSO") {
		echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
		echo "<thead>";
		echo "<th width='40px'>Action</th>";
		echo "<th>Flux</th>";
		echo "</thead>";

		$q="SELECT * FROM env_flux
			WHERE NOT EXISTS(SELECT flux_sso_profil_flux FROM env_flux_sso_profil WHERE flux_sso_profil_flux=flux_id AND flux_sso_profil_profil=$id1)
			ORDER BY flux_order DESC";
		$db1->query($q);
		
		while($db1->next_record()){	
			echo "<tr>";

			echo "<td>";
			echo "<a class='glyphicon glyphicon-plus' onClick='$(\"#id2\").val(\"".$db1->f('flux_id')."\"); $(\"#vladd\").val(\"SUBMIT\");$(\"#formulaire\").submit();'></a>";
			echo "</td>";

			echo "<td>";
			echo $db1->f("flux_name");
			echo "</td>";
	
	
			echo "</tr>";
		}
		echo "</table>";
	}
	
	elseif($tptbl=="FLUXLDAP") {
		echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
		echo "<thead>";
		echo "<th width='40px'>Action</th>";
		echo "<th>Flux</th>";
		echo "</thead>";

		$q="SELECT * FROM env_flux
			WHERE NOT EXISTS(SELECT flux_ldap_profil_flux FROM env_flux_ldap_profil WHERE flux_ldap_profil_flux=flux_id AND flux_ldap_profil_profil=$id1)
			ORDER BY flux_order DESC";
		$db1->query($q);
		
		while($db1->next_record()){	
			echo "<tr>";

			echo "<td>";
			echo "<a class='glyphicon glyphicon-plus' onClick='$(\"#id2\").val(\"".$db1->f('flux_id')."\"); $(\"#vladd\").val(\"SUBMIT\");$(\"#formulaire\").submit();'></a>";
			echo "</td>";

			echo "<td>";
			echo $db1->f("flux_name");
			echo "</td>";
	
	
			echo "</tr>";
		}
		echo "</table>";
	}
				
	echo "<form>";
}
}
?>

<!-- FOOTER --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<?
	include($repository."footer.php");
?>

<!-- SCRIPT --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<script>
	$(document).ready(function() {
		$('#datatable').dataTable( {
			"oLanguage": { "sUrl": "<?echo $repository; ?>lib/datatables/dataTables.txt" },
			"iDisplayLength": 50,
			"aaSorting": [[ 1, "asc" ]],
		} );
	} );	
	

	function recharge() {
		parent.document.getElementById('fgreload').value="MODIFY",
		window.parent.recharge();	
		window.parent.closemodal();	
	}
	
	<?php echo $jsaction ?>
</script>
