<?
	$repository="../";
	include($repository."include/include.php");
	include($repository."include/delete.php");
	include($repository."header.php"); 

	if($_SESSION['user_profil'] != 1 || !$config["consoleadmin"]) {
		echo "<br><br><br><center>Vous n'avez les permissions nécessaire</center>";
	}
	
	if($_SESSION['user_profil'] == 1 && $config["consoleadmin"]) {
		echo "<body>";
		
	$db1=new ps_db;
	
	$tpmod				=$_POST['tpmod'];
	$vlmod				=$_POST['vlmod'];
	$vladd				=$_POST['vladd'];
	$vlsup				=$_POST['vlsup'];
	
	$id					=$_POST['id'];
	$label				=$_POST['label'];
	$filter				=$_POST['filter'];
	$idcommunity		=$_POST['idcommunity'];
	
	/*--> Controle de cohérance */
	if($vladd!=""||$vlmod!="") {
		$fgerr="";
		if($label==""||$filter==""||$idcommunity=="") {
			$jsaction="alert('Vous devez renseigner l\'ensemble des informations');";
			$fgerr=1;
		}
	}
	
	/*--> Rester sur le mode encours */
	if($vlmod!=""&&fgerr!="") $tpmod="MODIFY";
	if($vladd!=""&&fgerr!="") $tpmod="SUBMIT";
		
	/*--> Submit */
	if($vladd!=""&&$fgerr=="") {
		$q="INSERT INTO env_ldap_profil(ldap_profil_label, ldap_profil_filter,ldap_profil_ldap_community) VALUES('".addslashes($label)."','".addslashes($filter)."',$idcommunity)";
		$db1->query($q);
		$tpmod="";
	}

	/*--> Modify */
	if($vlmod!=""&&$fgerr=="") {
		$q="UPDATE env_ldap_profil SET ldap_profil_label='".addslashes($label)."', ldap_profil_filter='".addslashes($filter)."', ldap_profil_ldap_community=$idcommunity WHERE ldap_profil_id=$id";
		$db1->query($q);
		$tpmod="";
	}

	/*--> Delete */
	if($vlsup!="") {
		delLdapProfil($id);
	}	

	echo "<div id='wrapper'>";
	include("header.php");
	echo "<div id='page-wrapper'>";
	echo "<div class='container-fluid'>";
	echo "<form id='formulaire' class='form-horizontal' role='form' method='post' enctype='multipart/form-data'>";
	
	echo "<input id='id' name='id' type='hidden' value='".$id."'>";
	echo "<input id='tpmod' name='tpmod' type='hidden' value=''>";
	
  
//-- DISPLAY -------------------------------------------------------------------------------------------------------------------------------

if($tpmod=="") { 
    echo "<legend><h1>GESTION DES LDAP</h1></legend>";
    
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='fgadd' name='fgadd' class='btn btn-primary' onClick='$(\"#id\").val(\"\"); $(\"#tpmod\").val(\"SUBMIT\");' type='submit' value='Ajouter' />";
	echo "</div>";
	echo "</div>";
		
	$q="SELECT * FROM env_ldap_profil, env_ldap_community WHERE ldap_profil_ldap_community=ldap_community_id ORDER BY ldap_profil_id";
	$db1->query($q);

	echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
    echo "<thead>";
	echo "<th width='70px'>Action</th>";
	echo "<th >Communauté</th>";
	echo "<th >Libellé</th>";
	echo "<th >Filtre</th>";
	echo "</thead>";
	
	while($db1->next_record()){	
		echo "<tr>";

		echo "<td align='center'>";
		if($db1->f('ldap_profil_id')>0) {
			echo "<a class='glyphicon glyphicon-file' onClick='$(\"#id\").val(\"".$db1->f('ldap_profil_id')."\"); $(\"#tpmod\").val(\"MODIFY\");$(\"#formulaire\").submit();'></a>";
			echo "<a class='glyphicon glyphicon-remove' onClick='$(\"#id\").val(\"".$db1->f('ldap_profil_id')."\"); $(\"#tpmod\").val(\"DELETE\");$(\"#formulaire\").submit();'></a>";
		}
		echo "</td>";

		echo "<td>";
		echo $db1->f('ldap_community_label');
		echo "</td>";
		
		echo "<td>";
		echo $db1->f('ldap_profil_label');
		echo "</td>";
				
		echo "<td>";
		echo $db1->f('ldap_profil_filter');
		echo "</td>";

		echo "</tr>";
	} 

	echo "</table>";
}

//-- SUBMIT --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="SUBMIT") {
	// Entete du formulaire
    echo "<legend><h1>AJOUT LDAP</h1></legend>";
    
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vladd' name='vladd' class='btn btn-primary' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
	echo "</div>";
	echo "</div>";	

	// Valeur par défaut
	$idcommunauty=-1;
}

//-- MODIFY --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="MODIFY") {
	// Entete du formulaire
    echo "<legend><h1>MODIFICATION COMMUNAUTE LDAP</h1></legend>";
   
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlmod' name='vlmod' class='btn btn-primary' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
	echo "</div>";
	echo "</div>";	

	// Valeur par défaut
	$q = "SELECT * FROM env_ldap_profil WHERE ldap_profil_id=$id";
	$db1->query($q);
	if($db1->next_record()) {
		$label 			= $db1->f('ldap_profil_label');
		$filter			= $db1->f('ldap_profil_filter');
		$idcommunauty	= $db1->f('ldap_profil_ldap_community');
	}
}

//-- DELETE --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="DELETE") {
	echo "<legend><h1>SUPPRESSION COMMUNAUTE LDAP</h1></legend>";
	
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlsup' name='vlsup' class='btn btn-primary' type='submit' value='Confirmer la suppression' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";    
	echo "</div>";
	echo "</div>";
	
	// Valeur par défaut
	$q = "SELECT * FROM env_ldap_profil WHERE ldap_profil_id=$id";
	$db1->query($q);
	if($db1->next_record()) {
		$label 			= $db1->f('ldap_profil_label');
		$filter			= $db1->f('ldap_profil_filter');
		$idcommunauty	= $db1->f('ldap_profil_ldap_community');
	}
}


//-- FORMULAIRE -----------------------------------------------------------------------------------------------------------------------------

if($tpmod=="SUBMIT"||$tpmod=="MODIFY"||$tpmod=="DELETE") {
?>
	<fieldset class="row fieldset" style="clear:both">
		<legend>Description</legend>

		<div class="form-group">
			<label for="id_bis" class="col-sm-3 control-label">ID*</label>
			<div class="col-sm-6"><input name="id_bis" id="id_bis" type="titre" class="form-control" disabled="disabled" placeholder="ID Application" value="<? echo $id; ?>"></div>
		</div>
		

		<div class="form-group">
			<label for="label" class="col-sm-3 control-label">Libellé*</label>
			<div class="col-sm-6"><input name="label" id="label" type="titre" class="form-control" placeholder="Libellé" value="<? echo $label; ?>"></div>
		</div>	
		
		
		<div class="form-group">
			<label for="filter" class="col-sm-3 control-label">Filtre LDAP*</label>
			<div class="col-sm-6">
				<input name="filter" id="filter" type="titre" class="form-control" placeholder="Filtre LDAP" value="<? echo $filter; ?>">
				<em>Utilisez le mot clé #login# dans votre filtre LDAP pour identifier l'utilisateur.</br>
				Exemples :</br>
				(&(uid=#login#)(ENTPersonProfils=eleve))</br>
				(&(uid=#login#)(ENTPersonProfils=enseignant))</em>
				
			</div>
		</div>	
		
		<div class="form-group">
			<label for="idcommunity" class="col-sm-3 control-label">Communauté*</label>
			<div class="col-sm-6">
				<select name="idcommunity" id="idcommunity" class="form-control">
					<?
					$q="SELECT * FROM env_ldap_community";
					$db1->query($q);
					while($db1->next_record()) {
						$lbselection="";
						if($db1->f("ldap_community_id")==$idcommunauty) 
							$lbselection = " selected";
						echo "<option value='".$db1->f("ldap_community_id")."' $lbselection>".$db1->f("ldap_community_label")."</option>";
					}
					?>
				</select>			
			</div>
		</div>		
	</fieldset>
<?
}

echo "</form></div></div></div>";

}

?>

<!-- FOOTER --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<?
	include($repository."footer.php");
?>

<!-- SCRIPT --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<? if($tpmod=="") { ?>
	<script>
		$(document).ready(function() {
			$('#datatable').dataTable( {
				"oLanguage": { "sUrl": "<?echo $repository; ?>lib/datatables/dataTables.txt" },
				"iDisplayLength": 50,
				"aaSorting": [[ 1, "asc" ]]
			} );
		} );	
		
		<?php echo $jsaction ?>
	</script>
<? } ?>

<? if($tpmod=="SUBMIT"||$tpmod=="MODIFY"||$tpmod=="DELETE") { ?>
	<script type="text/javascript">
		$('#label').focus();
		
		<?php echo $jsaction ?>
		
	</script>
<? } ?>





	
	
