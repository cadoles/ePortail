<?
	$repository="../";
	include($repository."include/include.php");
	include($repository."include/delete.php");
	include($repository."header.php"); 
		
	if($_SESSION['user_profil'] != 1 || !$config["consoleadmin"]) {
		echo "<br><br><br><center>Vous n'avez les permissions nécessaire</center>";
	}
	
	if($_SESSION['user_profil'] == 1 && $config["consoleadmin"]) {
		echo "<body>";
	
	$db1=new ps_db;
	
	$tpmod				=$_POST['tpmod'];
	$vlmod				=$_POST['vlmod'];
	$vladd				=$_POST['vladd'];
	$vlsup				=$_POST['vlsup'];
	
	$user_id			=$_POST['user_id'];
	$user_login			=$_POST['user_login'];
	$user_password		=$_POST['user_password'];
	$user_passwordbis	=$_POST['user_passwordbis'];
	$user_lastname		=$_POST['user_lastname'];
	$user_firstname		=$_POST['user_firstname'];
	$user_pseudo		=$_POST['user_pseudo'];
	$user_email			=$_POST['user_email'];
	$user_profil		=$_POST['user_profil'];
	$user_mod			=$_POST['user_mod'];
	$user_avatar		=$_POST['user_avatar'];
	
	
	/*--> Controle de cohérance */
	if($vladd!=""||$vlmod!="") {
		$fgerr="";

		if($vladd!=""&&$user_password=="") {
			$jsaction="alert('Un mot de passe est obligatoire pour tout nouveau utilisateur');";
			$fgerr=1;
		}

		if($vladd!=""&&$user_login=="") {
			$jsaction="alert('Un login est obligatoire pour tout nouveau utilisateur');";
			$fgerr=1;
		}
		
		if($user_password!=$user_passwordbis) {
			$jsaction="alert('Votre mot de passe n\'est pas confirmé');";
			$fgerr=1;
		}

		if($user_lastname==""||$user_firstname==""||$user_email==""||$user_profil=="") {
			$jsaction="alert('Vous devez renseigner l\'ensemble des informations');";
			$fgerr=1;
		}
		
		$q="SELECT * FROM env_user WHERE (user_login='".addslashes($user_login)."' OR user_email='".addslashes($user_email)."' OR ('".addslashes($user_pseudo)."'!='' AND user_pseudo='".addslashes($user_pseudo)."'))";
		if($vlmod!="") $q=$q." AND user_id!=$user_id";
		$db1->query($q);
		if($db1->next_record()){			
			$jsaction="alert('Un utilisateur avec ce login ou cet email ou ce pseudo existe déjà');";
			$fgerr=1;
		}
	}
	
	/*--> Rester sur le mode encours */
	if($vlmod!=""&&fgerr!="") $tpmod="MODIFY";
	if($vladd!=""&&fgerr!="") $tpmod="SUBMIT";
		
	/*--> Submit */
	if($vladd!=""&&$fgerr=="") {
		$q="INSERT INTO env_user(user_login, user_password, user_lastname, user_firstname, user_pseudo, user_email, user_avatar, user_profil, user_mod) VALUES('".addslashes($user_login)."',md5('$user_password'),'".addslashes($user_lastname)."','".addslashes($user_firstname)."','".addslashes($user_pseudo)."','".addslashes($user_email)."','$user_avatar',$user_profil,'$user_mod')";
		$db1->query($q);
		$tpmod="";
	}

	/*--> Modify */
	if($vlmod!=""&&$fgerr=="") {
		$q="UPDATE env_user SET user_login='".addslashes($user_login)."', user_lastname='".addslashes($user_lastname)."', user_firstname='".addslashes($user_firstname)."', user_pseudo='".addslashes($user_pseudo)."', user_email='".addslashes($user_email)."', user_avatar='$user_avatar', user_profil=$user_profil, user_mod='$user_mod' WHERE user_id=$user_id";
		$db1->query($q);
		
		// Changement de mot de passe
		if($user_password!="") {
			$q="UPDATE env_user SET user_password='".md5($user_password)."' WHERE user_id=$user_id";
			$db1->query($q);
		}
		$tpmod="";
	}
	
	/*--> Delete */
	if($vlsup!="") {
		delUser($user_id);
		$tpmod="";
	}

	echo "<div id='wrapper'>";
	include("header.php");
	echo "<div id='page-wrapper'>";
	echo "<div class='container-fluid'>";
	echo "<form id='formulaire' class='form-horizontal' role='form' method='post' enctype='multipart/form-data'>";
	
	echo "<input id='user_id' name='user_id' type='hidden' value='".$user_id."'>";
	echo "<input id='tpmod' name='tpmod' type='hidden' value=''>";
	
  
//-- DISPLAY -------------------------------------------------------------------------------------------------------------------------------

if($tpmod=="") { 
    echo "<legend><h1>GESTION DES UTILISATEURS</h1></legend>";

	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='fgadd' name='fgadd' class='btn btn-primary' onClick='$(\"#user_id\").val(\"\"); $(\"#tpmod\").val(\"SUBMIT\");' type='submit' value='Ajouter' />";
	echo "</div>";
	echo "</div>";
	
	echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
    echo "<thead>";
	echo "<th width='70px'>Action</th>";
	echo "<th>ID</th>";
	echo "<th width='90px'>Avatar</th>";
	echo "<th>Nom</th>";
	echo "<th>Login</th>";
	echo "<th>Pseudo</th>";
	echo "<th>Profil</th>";
	echo "</thead>";
	
	$q="SELECT * FROM env_user, env_profil WHERE env_profil.profil_id=env_user.user_profil AND user_id > 0";
	$db1->query($q);
	while($db1->next_record()){	
		echo "<tr>";

		echo "<td align='center'>";
		echo "<a class='glyphicon glyphicon-file' onClick='$(\"#user_id\").val(\"".$db1->f('user_id')."\"); $(\"#tpmod\").val(\"MODIFY\");$(\"#formulaire\").submit();'></a>";
		echo "<a class='glyphicon glyphicon-remove' onClick='$(\"#user_id\").val(\"".$db1->f('user_id')."\"); $(\"#tpmod\").val(\"DELETE\");$(\"#formulaire\").submit();'></a>";
		echo "</td>";

		echo "<td>";
		echo $db1->f('user_id');
		echo "</td>";
		
		echo "<td align='center'>";
		if($db1->f('user_avatar')=="") 
			echo "<img src='".$repository."style/images/blank.gif' class='myavatarvide' width='40px' height='40px'></img>";
		else
			echo "<img src='".$repository."local/images/avatar/".$db1->f('user_avatar')."' width='40px' height='40px'></img>";
		echo "</td>";
		
		echo "<td>";
		echo $db1->f('user_firstname')." ".$db1->f('user_lastname')."<br>";
		echo "</td>";

		echo "<td>";
		echo $db1->f('user_login')."<br>";
		echo "</td>";

		echo "<td>";
		echo $db1->f('user_pseudo')."<br>";
		echo "</td>";

		echo "<td>";
		echo $db1->f('profil_label')."<br>";
		echo "</td>";

		echo "</tr>";
	} 
	echo "</table>";
}

//-- SUBMIT --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="SUBMIT") {
	// Entete du formulaire
    echo "<legend><h1>AJOUT UTILISATEUR</h1></legend>";
	
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vladd' name='vladd' class='btn btn-primary' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
	echo "</div>";
	echo "</div>";	

	// Valeur par défaut
	if($user_mod=="") {$user_mod="big";}
	if($user_profil=="") {$user_profil="3";}
}

//-- MODIFY --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="MODIFY") {
	// Entete du formulaire
    echo "<legend><h1>MODIFICATION UTILISATEUR</h1></legend>";

	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlmod' name='vlmod' class='btn btn-primary' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
	echo "</div>";
	echo "</div>";	

	// Valeur par défaut
	$q = "SELECT * FROM env_user WHERE user_id=$user_id";
	$db1->query($q);
	if($db1->next_record()) {
		$user_login 		= $db1->f("user_login");
		$user_lastname 		= $db1->f("user_lastname");
		$user_firstname 	= $db1->f("user_firstname");
		$user_pseudo	 	= $db1->f("user_pseudo");
		$user_email 		= $db1->f("user_email");
		$user_profil		= $db1->f("user_profil");
		$user_mod			= $db1->f("user_mod");
		$user_avatar		= $db1->f("user_avatar");
	}
}

//-- DELETE --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="DELETE") {
	echo "<legend><h1>SUPPRESSION UTILISATEUR</h1></legend>";

	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlsup' name='vlsup' class='btn btn-primary' type='submit' value='Confirmer la suppression' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";    
	echo "</div>";
	echo "</div>";	

	// Valeur par défaut
	$q="SELECT * FROM env_user, env_profil WHERE user_id=$user_id AND env_profil.profil_id=env_user.user_profil";
	$db1->query($q);
	if($db1->next_record()) {
		if($db1->f('user_avatar')=="") $class=" class='myavatarvide'";
		echo "<div id='myavatarprofil' style='float:left; margin-right:5px; margin-bottom:10px; border:none;' $class >";
		if($db1->f('user_avatar')!="") 
			echo "<img id='img_avatar' name='img_avatar' src='".$repository."local/images/avatar/".$db1->f('user_avatar')."' width='90px' height='90px'></img>";
		else
			echo "<img id='img_avatar' name='img_avatar' src='".$repository."style/images/blank.gif' width='90px' height='90px'></img>";
		echo "</div>";
		
		echo $db1->f('user_firstname')." ".$db1->f('user_lastname')."<br>";
		echo "<span id='avatar_description'>";
		echo "Login : ".$db1->f('user_login')."<br>";
		echo "Pseudo : ".$db1->f('user_pseudo')."<br>";
		echo "Profil : ".$db1->f('profil_label')."<br>";			
		echo "</span>";
		echo "</div>";

		echo "</div>";
	}
	
	
}


//-- FORMULAIRE -----------------------------------------------------------------------------------------------------------------------------

if($tpmod=="SUBMIT"||$tpmod=="MODIFY") {
	if($user_avatar=="") $class=" class='myavatarvide'";

	echo "<div style='width:110px; margin:auto; '>";
	echo "<div id='myavatarprofil' style='float:left; margin-right:5px; margin-bottom:10px; border:none;' $class >";
	if($user_avatar!="") 
		echo "<img id='img_avatar' name='img_avatar' src='".$repository."local/images/avatar/".$user_avatar."' width='90px' height='90px'></img>";
	else
		echo "<img id='img_avatar' name='img_avatar' src='".$repository."style/images/blank.gif' width='90px' height='90px'></img>";
	echo "</div>";
	
	echo "<div style='float:left;'>";
	echo "<a class='glyphicon glyphicon-plus' data-toggle='modal' data-target='.bs-avatar-modal' title='Ajouter un avatar'></a>";
	echo "<br>";
	echo "<a class='glyphicon glyphicon-remove' onClick='$(\"#myavatarprofil\").addClass(\"myavatarvide\"); $(\"#img_avatar\").attr(\"src\",\"\"); $(\"#user_avatar\").val(\"\");' title='Vider l'avatar'></a>";
	echo "</div>";
	echo "</div>";
	
	echo "<input id='user_avatar' name='user_avatar'  type='hidden' value='".$user_avatar."'>";

?>
	
	<fieldset class="row fieldset" style="clear:both">
		<legend>Coordonnés</legend>

		
		<div class="form-group">
			<label for="user_id_bis" class="col-sm-3 control-label">ID Utilisateur*</label>
			<div class="col-sm-6"><input name="user_id_bis" id="user_id_bis" type="titre" class="form-control" disabled="disabled" placeholder="ID Utilisateur" value="<? echo $user_id; ?>"></div>
		</div>
	
		<? if($tpmod=="MODIFY") $disable="readonly"; ?>
		<div class="form-group">
			<label for="user_login" class="col-sm-3 control-label">Login*</label>
			<div class="col-sm-6"><input name="user_login" id="user_login" type="titre" class="form-control" <? echo $disable; ?> placeholder="Login" value="<? echo $user_login; ?>"></div>
		</div>

		<? if(!$config['activerCAS']) { $disable="";?>
		<div class="form-group"> 
			<label for="user_password" class="col-sm-3 control-label">Password*</label>
			<div class="col-sm-6"><input name="user_password" id="user_password" type="password" class="form-control" placeholder="Password" value="<? echo $user_password; ?>"></div>
		</div>		

		<div class="form-group"> 
			<label for="user_passwordbis" class="col-sm-3 control-label">Confirmer Password*</label>
			<div class="col-sm-6"><input name="user_passwordbis" id="user_passwordbis" type="password" class="form-control" placeholder="Confirmer Password" value="<? echo $user_passwordbis; ?>"></div>
		</div>	
		<? }
		else  $disable="readonly"; 
		?>

		<div class="form-group">
			<label for="user_lastname" class="col-sm-3 control-label">Nom*</label>
			<div class="col-sm-6"><input name="user_lastname" id="user_lastname" type="titre" class="form-control" <? echo $disable; ?> placeholder="Nom" value="<? echo $user_lastname; ?>"></div>
		</div>

		<div class="form-group">
			<label for="user_firstname" class="col-sm-3 control-label">Prénom*</label>
			<div class="col-sm-6"><input name="user_firstname" id="user_firstname" type="titre" class="form-control" <? echo $disable; ?> placeholder="Prénom" value="<? echo $user_firstname; ?>"></div>
		</div>

		<div class="form-group">
			<label for="user_email" class="col-sm-3 control-label">Email*</label>
			<div class="col-sm-6"><input name="user_email" id="user_email" type="email" class="form-control" <? echo $disable; ?> placeholder="Email" value="<? echo $user_email; ?>"></div>
		</div>
		
		<div class="form-group">
			<label for="user_pseudo" class="col-sm-3 control-label">Pseudo</label>
			<div class="col-sm-6"><input name="user_pseudo" id="user_pseudo" type="titre" class="form-control" placeholder="Pseudo" value="<? echo $user_pseudo; ?>"></div>
		</div>
	</fieldset>

	<fieldset id="encadrer">
		<legend>Profil</legend>
		
		<div class="form-group">
			<label for="user_profil" class="col-sm-3 control-label">Profil</label>
			<div class="col-sm-6">
				<select name="user_profil" id="user_profil" class="form-control">
				<?
					$q="SELECT * FROM env_profil";
					$db1->query($q);
					while($db1->next_record()){	
						$lbsel="";
						if($user_profil==$db1->f("profil_id")) {
							$lbsel="selected";
						}
						echo "<option value='".$db1->f("profil_id")."' ".$lbsel.">".$db1->f("profil_label")."</option>";
					}
				?>					
				</select>			
			</div>
		</div>
	
		<div class="form-group">
			<label for="user_mod" class="col-sm-3 control-label">Mode d'Affichage</label>
			<div class="col-sm-6">
				<select name="user_mod" id="user_mod" class="form-control">
					<option value=""	<? if($user_mod=='')	{ echo 'selected'; }?>>sans bannière</option>
					<option value="big"	<? if($user_mod=='big')	{ echo 'selected'; }?>>avec bannière</option>
				</select>
			</div>
		</div>
	</fieldset>

	<!-- Popup creation avatar !-->
	<div id="mymodal" class="modal fade bs-avatar-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-lg" style=" max-width:655px; width:80%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">VOTRE AVATAR</h4>
			</div>
			<div class="modal-body">
				<iframe src="<? echo $repository; ?>jcrop.php?dirimg=local/images/avatar&lbico=user_avatar&igico=img_avatar&repository=<? echo $repository; ?>" frameborder=0 width="100%" height="520px"></iframe>
			</div>
		</div>
	  </div>
	</div>	
<?
}

echo "</form></div></div></div>";

}
?>

<!-- FOOTER --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<?
	include($repository."footer.php");
?>

<!-- SCRIPT --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<? if($tpmod=="") { ?>
<script>
		$(document).ready(function() {
			$('#datatable').dataTable( {
				"oLanguage": { "sUrl": "<?echo $repository; ?>lib/datatables/dataTables.txt" },
				"iDisplayLength": 50,
				"aaSorting": [[ 1, "desc" ]],
			} );
		} );	
	</script>
<? } ?>

<? if($tpmod=="SUBMIT"||$tpmod=="MODIFY"||$tpmod=="DELETE") { ?>
	<script type="text/javascript">
		$('#user_lastname').focus();
		
		function recharge() {
			parent.document.location.reload();
		}
		
		function closemodal() {
			$('#mymodal').modal('hide');
		}
	</script>
<? } ?>

<script>
	<?php echo $jsaction ?>
</script>


	
	
