<?
	$repository="../";
	include($repository."include/include.php");
	include($repository."include/delete.php");
	include($repository."header.php"); 

	if($_SESSION['user_profil'] != 1 || !$config["consoleadmin"]) {
		echo "<br><br><br><center>Vous n'avez les permissions nécessaire</center>";
	}
	
	if($_SESSION['user_profil'] == 1 && $config["consoleadmin"]) {
		echo "<body>";
	
	$db1=new ps_db;
	$db2=new ps_db;
	
	$tpmod					=$_POST['tpmod'];
	$vlmod					=$_POST['vlmod'];
	$vladd					=$_POST['vladd'];
	$vlsup					=$_POST['vlsup'];
	
	$widget_id				=$_POST['widget_id'];
	$widget_name 			=$_POST['widget_name'];
	$widget_label 			=$_POST['widget_label'];
	$widget_url 			=$_POST['widget_url'];
	$widget_height			=$_POST['widget_height'];
	$widget_style 			=$_POST['widget_style'];
	$widget_type			=$_POST['widget_type'];
	$widget_mode			=$_POST['widget_mode'];
	$widget_icon			=$_POST['widget_icon'];
	$widget_code			=$_POST['widget_code'];
	$widget_categorie		=$_POST['widget_categorie'];
	
	
	/*--> Controle de cohérance */
	if($vladd!=""||$vlmod!="") {
		$fgerr="";

		if($widget_name==""||$widget_label==""||$widget_height==""||$widget_style==""||$widget_mode==""||$widget_type==""||$widget_categorie=="") {
			$jsaction="alert('Vous devez renseigner l\'ensemble des informations');";
			$fgerr=1;
		}
	}
	
	/*--> Rester sur le mode encours */
	if($vlmod!=""&&fgerr!="") $tpmod="MODIFY";
	if($vladd!=""&&fgerr!="") $tpmod="SUBMIT";
		
	/*--> Submit */
	if($vladd!=""&&$fgerr=="") {
		$q="INSERT INTO env_widget(widget_name, widget_label, widget_url, widget_code, widget_height, widget_style, widget_type, widget_mode, widget_icon, widget_categorie) VALUES('".addslashes($widget_name)."','".addslashes($widget_label)."','$widget_url','".addslashes($widget_code)."','".$widget_height."px','$widget_style','$widget_type','$widget_mode','$widget_icon','$widget_categorie')";
		$db1->query($q);
		$tpmod="";
	}

	/*--> Modify */
	if($vlmod!=""&&$fgerr=="") {
		$q="UPDATE env_widget SET widget_name='".addslashes($widget_name)."', widget_label='".addslashes($widget_label)."', widget_url='$widget_url', widget_code='".addslashes($widget_code)."', widget_height='".$widget_height."px', widget_style='$widget_style', widget_icon='$widget_icon', widget_type=$widget_type, widget_mode=$widget_mode, widget_categorie='$widget_categorie' WHERE widget_id=$widget_id";
		$db1->query($q);

		$tpmod="";
	}
	
	/*--> Delete */
	if($vlsup!="") {
		delWidget($widget_id);
		$tpmod="";
	}

	echo "<div id='wrapper'>";
	include("header.php");
	echo "<div id='page-wrapper'>";
	echo "<div class='container-fluid'>";
	echo "<form id='formulaire' class='form-horizontal' role='form' method='post' enctype='multipart/form-data'>";
	
	echo "<input id='widget_id' name='widget_id' type='hidden' value='".$widget_id."'>";
	echo "<input id='tpmod' name='tpmod' type='hidden' value=''>";
	
  
//-- DISPLAY -------------------------------------------------------------------------------------------------------------------------------

if($tpmod=="") { 
    echo "<legend><h1>GESTION DES WIDGETS</h1></legend>";

	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='fgadd' name='fgadd' class='btn btn-primary' onClick='$(\"#widget_id\").val(\"\"); $(\"#tpmod\").val(\"SUBMIT\");' type='submit' value='Ajouter' />";
	echo "</div>";
	echo "</div>";
	
	echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
    echo "<thead>";
	echo "<th width='70px'>Action</th>";
	echo "<th width='70px'>ID</th>";
	echo "<th width='90px'>Icone</th>";
	echo "<th>Catégorie</th>";
	echo "<th>Nom</th>";
	echo "<th>Description</th>";
	echo "<th>Hauteur</th>";
	echo "<th>Type</th>";
	//echo "<th>Mode</th>";
	echo "</thead>";
	
	$q="SELECT * FROM env_widget, env_widget_type, env_widget_mode, env_icon WHERE widget_type=widget_type_id AND widget_mode=widget_mode_id AND widget_icon=icon_id";
	$db1->query($q);
	while($db1->next_record()){	
		echo "<tr style='font-size:90%'>";

		echo "<td align='center'>";
		echo "<a class='glyphicon glyphicon-file' onClick='$(\"#widget_id\").val(\"".$db1->f('widget_id')."\"); $(\"#tpmod\").val(\"MODIFY\");$(\"#formulaire\").submit();'></a>";
		if($db1->f('widget_id')>0) echo "<a class='glyphicon glyphicon-remove' onClick='$(\"#widget_id\").val(\"".$db1->f('widget_id')."\"); $(\"#tpmod\").val(\"DELETE\");$(\"#formulaire\").submit();'></a>";
		echo "</td>";

		echo "<td align='center'>";
		echo $db1->f('widget_id');
		echo "</td>";
		
		echo "<td align='center'>";
		echo "<img src='".$repository."local/images/icon/".$db1->f('icon_url')."' width='40px' height='40px'></img>";
		echo "</td>";
		
		echo "<td>";
		
		$q="SELECT * FROM env_widget_categorie WHERE widget_categorie_id=".$db1->f('widget_categorie');
		$db2->query($q);
		if($db2->next_record()) echo $db2->f('widget_categorie_label');
		
		echo "</td>";
		
		echo "<td>";
		echo $db1->f('widget_name');
		echo "</td>";

		echo "<td>";
		echo $db1->f('widget_label');
		echo "</td>";

		echo "<td align='center'>";
		echo $db1->f('widget_height');
		echo "</td>";

		echo "<td align='center'>";
		echo $db1->f('widget_type_name');
		echo "</td>";		

		/*
		echo "<td align='center'>";
		echo $db1->f('widget_mode_name');
		echo "</td>";
		*/
		
		echo "</tr>";
	} 
	echo "</table>";
}

//-- SUBMIT --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="SUBMIT") {
	// Entete du formulaire
    echo "<legend><h1>AJOUT WIDGET</h1></legend>";
    
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vladd' name='vladd' class='btn btn-primary' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
	echo "</div>";
	echo "</div>";	  
    


	// Valeur par défaut
	if($widget_style=="") 		$widget_style		="0";
	if($widget_height=="") 		$widget_height		="300";
	if($widget_type=="") 		$widget_type		= 1;
	if($widget_mode=="") 		$widget_mode		= 1;
	if($widget_icon=="")	 	$widget_icon		= "";
	if($widget_categorie=="") 	$widget_categorie	= 0;
}

//-- MODIFY --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="MODIFY") {
	// Entete du formulaire
    echo "<legend><h1>MODIFICATION WIDGET</h1></legend>";
	
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlmod' name='vlmod' class='btn btn-primary' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
	echo "</div>";
	echo "</div>";	

	// Valeur par défaut
	$q = "SELECT * FROM env_widget, env_icon WHERE widget_id=$widget_id AND widget_icon=icon_id";
	$db1->query($q);
	if($db1->next_record()) {
		$widget_name 			= $db1->f("widget_name");
		$widget_label 			= $db1->f("widget_label");
		$widget_url 			= $db1->f("widget_url");
		$widget_height		 	= $db1->f("widget_height");
		$widget_style 			= $db1->f("widget_style");
		$widget_type			= $db1->f("widget_type");
		$widget_mode			= $db1->f("widget_mode");
		$widget_icon			= $db1->f("widget_icon");
		$icon_url				= $db1->f("icon_url");
		$widget_code			= $db1->f("widget_code");
		$widget_categorie		= $db1->f("widget_categorie");
	}
}

//-- DELETE --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="DELETE") {
	echo "<legend><h1>SUPPRESSION WIDGET</h1></legend>";
	
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlsup' name='vlsup' class='btn btn-primary' type='submit' value='Confirmer la suppression' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";    
	echo "</div>";
	echo "</div>";	 	

	// Valeur par défaut
	$q = "SELECT * FROM env_widget, env_icon WHERE widget_id=$widget_id AND widget_icon=icon_id";
	$db1->query($q);
	if($db1->next_record()) {
		$widget_name 			= $db1->f("widget_name");
		$widget_label 			= $db1->f("widget_label");
		$widget_url 			= $db1->f("widget_url");
		$widget_height		 	= $db1->f("widget_height");
		$widget_style 			= $db1->f("widget_style");
		$widget_type			= $db1->f("widget_type");
		$widget_mode			= $db1->f("widget_mode");
		$widget_icon			= $db1->f("widget_icon");
		$icon_url				= $db1->f("icon_url");
		$widget_code			= $db1->f("widget_code");
		$widget_categorie		= $db1->f("widget_categorie");
	}
}


//-- FORMULAIRE -----------------------------------------------------------------------------------------------------------------------------

if($tpmod=="SUBMIT"||$tpmod=="MODIFY"||$tpmod=="DELETE") {
	if($widget_id<0) $fghidden=" style='display:none;'";
?>


	<fieldset class="row fieldset" style="clear:both">
		<legend>Description</legend>

		<div class="form-group">
			<label for="widget_id_bis" class="col-sm-3 control-label">ID*</label>
			<div class="col-sm-6"><input name="widget_id_bis" id="widget_id_bis" type="titre" class="form-control" disabled="disabled" placeholder="ID" value="<? echo $widget_id; ?>"></div>
		</div>

		<div class="form-group">
			<label for="widget_name" class="col-sm-3 control-label">Nom*</label>
			<div class="col-sm-6"><input name="widget_name" id="widget_name" type="titre" class="form-control" placeholder="Nom" value="<? echo $widget_name; ?>"></div>
		</div>

		<div class="form-group">
			<label for="widget_label" class="col-sm-3 control-label">Description</label>
			<div class="col-sm-6">			
				<textarea id="widget_label" name="widget_label" class="form-control" rows="4"><? echo $widget_label; ?></textarea>
			</div>
		</div>

		<div class="form-group" <? echo $fghidden; ?>>
			<label for="widget_url" class="col-sm-3 control-label">Url</label>
			<div class="col-sm-6"><input name="widget_url" id="widget_url" type="titre" class="form-control" placeholder="Url" value="<? echo $widget_url; ?>"></div>
		</div>

		<div class="form-group">
			<label for="widget_height" class="col-sm-3 control-label">Hauteur*</label>
			<div class="col-sm-6"><input name="widget_height" id="widget_url" type="number" class="form-control" placeholder="Url" value="<? echo $widget_height; ?>"></div>
		</div>

		<div class="form-group">
			<label for="widget_categorie" class="col-sm-3 control-label">Catégorie*</label>
			<div class="col-sm-6">
				<select name="widget_categorie" id="widget_categorie" class="form-control">
				<?
					if($categorie==0) $lbsel="selected";
					echo "<option value='0' ".$lbsel.">Aucune</option>";
					
					$q="SELECT * FROM env_widget_categorie";
					$db1->query($q);
					while($db1->next_record()){	
						$lbsel="";
						if($widget_categorie==$db1->f("widget_categorie_id")) {
							$lbsel="selected";
						}
						echo "<option value='".$db1->f("widget_categorie_id")."' ".$lbsel.">".$db1->f("widget_categorie_label")."</option>";
					}
				?>					
				</select>			
			</div>
		</div>
		
		<div class="form-group">
			<label for="widget_style" class="col-sm-3 control-label">Style*</label>
			<div class="col-sm-6">
				<select name="widget_style" id="widget_style" class="form-control">
				<?
					$q="SELECT * FROM env_widget_style";
					$db1->query($q);
					while($db1->next_record()){	
						$lbsel="";
						if($widget_style==$db1->f("widget_style_id")) {
							$lbsel="selected";
						}
						echo "<option value='".$db1->f("widget_style_id")."' ".$lbsel.">".$db1->f("widget_style_label")."</option>";
					}
				?>					
				</select>			
			</div>
		</div>

		<div class="form-group" style="display:none;">
			<label for="widget_mode" class="col-sm-3 control-label">Mode*</label>
			<div class="col-sm-6">
				<select name="widget_mode" id="widget_mode" class="form-control">
				<?
					$q="SELECT * FROM env_widget_mode";
					$db1->query($q);
					while($db1->next_record()){	
						$lbsel="";
						if($widget_mode==$db1->f("widget_mode_id")) {
							$lbsel="selected";
						}
						echo "<option value='".$db1->f("widget_mode_id")."' ".$lbsel.">".$db1->f("widget_mode_name")."</option>";
					}
				?>					
				</select>			
			</div>
		</div>

		<div class="form-group" <? echo $fghidden; ?>>
			<label for="widget_type" class="col-sm-3 control-label">Type*</label>
			<div class="col-sm-6">
				<select name="widget_type" id="widget_type" class="form-control">
				<?
					$q="SELECT * FROM env_widget_type";
					$db1->query($q);
					while($db1->next_record()){	
						$lbsel="";
						if($widget_type==$db1->f("widget_type_id")) {
							$lbsel="selected";
						}
						echo "<option value='".$db1->f("widget_type_id")."' ".$lbsel.">".$db1->f("widget_type_name")."</option>";
					}
				?>					
				</select>			
			</div>
		</div>
	</fieldset>

	<fieldset class="row fieldset" style="clear:both">
		<legend>Icône</legend>
		
		<input 	value="<? echo $widget_icon; ?>" id="widget_icon" name="widget_icon" type="hidden"/>
		<?
		echo "<div style='width:140px; margin:auto; '>";
		echo "<div id='myavatarprofil' style='float:left; margin-right:5px; margin-bottom:10px; border:none;'>";
		echo "<img id='imgapp' src='".$repository."local/images/icon/".$icon_url."' width='90px' height='90px'></img>";
		echo "</div>";
		echo "<div style='float:left;'>";
		echo "<a class='glyphicon glyphicon-folder-close' data-toggle='modal' data-target='.bs-item-modal' title='Séléctionner un icône' onClick='changeFrame(\"selicone\")'></a>";
		echo "</div>";
		echo "</div>";
		?>

		<!-- Popup modification item !-->
		<div id="mymodal" class="modal fade bs-item-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		  <div class="modal-dialog modal-lg" style=" max-width:655px; width:80%;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">ICÔNE</h4>
				</div>
				<div class="modal-body">
					<iframe id="framemodal" frameborder=0 width="100%" height="520px"></iframe>
				</div>
			</div>
		  </div>
		</div>	
				
	</fieldset>


	<fieldset class="code row fieldset" style="display: none; clear: both">
		<legend class="code" style="display: none;">Code</legend>	
		<textarea id="widget_code" name="widget_code" class="form-control" rows="10"><? echo $widget_code; ?></textarea>
	</fieldset>
	
<?
}

echo "</form></div></div></div>";

}

?>

<!-- FOOTER --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<?
	include($repository."footer.php");
?>

<!-- SCRIPT --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<? if($tpmod=="") { ?>
	<script>
		$(document).ready(function() {
			$('#datatable').dataTable( {
				"oLanguage": { "sUrl": "<?echo $repository; ?>lib/datatables/dataTables.txt" },
				"iDisplayLength": 50,
				"aaSorting": [[ 1, "asc" ]]
			} );
		} );	
		
		<?php echo $jsaction ?>
	</script>
<? } ?>

<? if($tpmod=="SUBMIT"||$tpmod=="MODIFY"||$tpmod=="DELETE") { ?>
	<script type="text/javascript">
		$('#label').focus();

		function changeFrame(tpmod) {
			srcframe="icon.php?tpmod=SELECT&igico=imgapp&idico=widget_icon";
			$("#framemodal").attr("src",srcframe);
		}
		
		function recharge() {
			$("#formulaire").submit();
		}
		
		function closemodal() {
			$('#mymodal').modal('hide');
		}
		
		function showCode() {
			if($('#widget_type').val()==4) {
				$('.code').css("display", "block");
			}			
			else {
				$('#widget_code').val('');
				
				$('.code').css("display", "none");
			}
		}
		
		if($('#widget_type').val()==4) {
			$('.code').css("display", "block");
		}
		
		$('#widget_type').bind("change", function() {		
			showCode();
		});
		
		<? if($widget_id>=0) { ?> showCode(); <? } ?>
		<?php echo $jsaction ?>
		
	</script>
<? } ?>







	
	
