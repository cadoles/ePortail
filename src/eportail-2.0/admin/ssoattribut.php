<?
	$repository="../";
	include($repository."include/include.php");
	include($repository."include/delete.php");
	include($repository."header.php"); 

	if($_SESSION['user_profil'] != 1 || !$config["consoleadmin"]) {
		echo "<br><br><br><center>Vous n'avez les permissions nécessaire</center>";
	}
	
	if($_SESSION['user_profil'] == 1 && $config["consoleadmin"]) {
		echo "<body>";
		
	$db1=new ps_db;
	
	$tpmod				=$_POST['tpmod'];
	$vlmod				=$_POST['vlmod'];
	$vladd				=$_POST['vladd'];
	$vlsup				=$_POST['vlsup'];
	
	$id					=$_POST['id'];
	$name				=$_POST['name'];
	$value				=$_POST['value'];
	
	/*--> Controle de cohérance */
	if($vladd!=""||$vlmod!="") {
		$fgerr="";
		if($name==""||$value=="") {
			$jsaction="alert('Vous devez renseigner l\'ensemble des informations');";
			$fgerr=1;
		}
	}
	
	/*--> Rester sur le mode encours */
	if($vlmod!=""&&fgerr!="") $tpmod="MODIFY";
	if($vladd!=""&&fgerr!="") $tpmod="SUBMIT";
		
	/*--> Submit */
	if($vladd!=""&&$fgerr=="") {
		$q="INSERT INTO env_sso_attribut(sso_attribut_name, sso_attribut_value) VALUES('".addslashes($name)."','".addslashes($value)."')";
		$db1->query($q);
		$tpmod="";
	}

	/*--> Modify */
	if($vlmod!=""&&$fgerr=="") {
		$q="UPDATE env_sso_attribut SET sso_attribut_name='".addslashes($name)."', sso_attribut_value='".addslashes($value)."' WHERE sso_attribut_id=$id";
		$db1->query($q);
		$tpmod="";
	}

	/*--> Delete */
	if($vlsup!="") {
		delAttribut($id);
	}	

	echo "<div id='wrapper'>";
	include("header.php");
	echo "<div id='page-wrapper'>";
	echo "<div class='container-fluid'>";
	echo "<form id='formulaire' class='form-horizontal' role='form' method='post' enctype='multipart/form-data'>";
	
	echo "<input id='id' name='id' type='hidden' value='".$id."'>";
	echo "<input id='tpmod' name='tpmod' type='hidden' value=''>";
	
  
//-- DISPLAY -------------------------------------------------------------------------------------------------------------------------------

if($tpmod=="") { 
    echo "<legend><h1>GESTION DES ATTRIBUTS SSO</h1></legend>";
    
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='fgadd' name='fgadd' class='btn btn-primary' onClick='$(\"#id\").val(\"\"); $(\"#tpmod\").val(\"SUBMIT\");' type='submit' value='Ajouter' />";
	echo "</div>";
	echo "</div>";
		
	$q="SELECT * FROM env_sso_attribut";
	$db1->query($q);

	echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
    echo "<thead>";
	echo "<th width='70px'>Action</th>";
	echo "<th >Libellé</th>";
	echo "<th >Filtre</th>";
	echo "</thead>";
	
	while($db1->next_record()){	
		echo "<tr>";

		echo "<td align='center'>";
		if($db1->f('sso_attribut_id')>0) {
			echo "<a class='glyphicon glyphicon-file' onClick='$(\"#id\").val(\"".$db1->f('sso_attribut_id')."\"); $(\"#tpmod\").val(\"MODIFY\");$(\"#formulaire\").submit();'></a>";
			echo "<a class='glyphicon glyphicon-remove' onClick='$(\"#id\").val(\"".$db1->f('sso_attribut_id')."\"); $(\"#tpmod\").val(\"DELETE\");$(\"#formulaire\").submit();'></a>";
		}
		echo "</td>";

		echo "<td>";
		echo $db1->f('sso_attribut_name');
		echo "</td>";
				
		echo "<td>";
		echo $db1->f('sso_attribut_value');
		echo "</td>";

		echo "</tr>";
	} 

	echo "</table>";
}

//-- SUBMIT --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="SUBMIT") {
	// Entete du formulaire
    echo "<legend><h1>AJOUT ATTRIBUT SSO</h1></legend>";
    
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vladd' name='vladd' class='btn btn-primary' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
	echo "</div>";
	echo "</div>";	

	// Valeur par défaut
	// aucune
}

//-- MODIFY --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="MODIFY") {
	// Entete du formulaire
    echo "<legend><h1>MODIFICATION ATTRIBUT SSO</h1></legend>";
   
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlmod' name='vlmod' class='btn btn-primary' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
	echo "</div>";
	echo "</div>";	

	// Valeur par défaut
	$q = "SELECT * FROM env_sso_attribut WHERE sso_attribut_id=$id";
	$db1->query($q);
	if($db1->next_record()) {
		$name 		= $db1->f('sso_attribut_name');
		$value		= $db1->f('sso_attribut_value');
	}
}

//-- DELETE --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="DELETE") {
	echo "<legend><h1>SUPPRESSION ATTRIBUT SSO</h1></legend>";
	
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlsup' name='vlsup' class='btn btn-primary' type='submit' value='Confirmer la suppression' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";    
	echo "</div>";
	echo "</div>";
	
	// Valeur par défaut
	$q = "SELECT * FROM env_sso_attribut WHERE sso_attribut_id=$id";
	$db1->query($q);
	if($db1->next_record()) {
		$name 		= $db1->f('sso_attribut_name');
		$value		= $db1->f('sso_attribut_value');
	}
}


//-- FORMULAIRE -----------------------------------------------------------------------------------------------------------------------------

if($tpmod=="SUBMIT"||$tpmod=="MODIFY"||$tpmod=="DELETE") {
?>
	<fieldset class="row fieldset" style="clear:both">
		<legend>Description</legend>

		<div class="form-group">
			<label for="id_bis" class="col-sm-3 control-label">ID*</label>
			<div class="col-sm-6"><input name="id_bis" id="id_bis" type="titre" class="form-control" disabled="disabled" placeholder="ID Application" value="<? echo $id; ?>"></div>
		</div>
		

		<div class="form-group">
			<label for="name" class="col-sm-3 control-label">Nom*</label>
			<div class="col-sm-6"><input name="name" id="name" type="titre" class="form-control" placeholder="Nom" value="<? echo $name; ?>"></div>
		</div>	
		
		
		<div class="form-group">
			<label for="value" class="col-sm-3 control-label">Valeur*</label>
			<div class="col-sm-6">
				<input name="value" id="value" type="titre" class="form-control" placeholder="Valeur" value="<? echo $value; ?>"></br>
			</div>
			
		</div>	
	</fieldset>
<?
}

echo "</form></div></div></div>";

}

?>

<!-- FOOTER --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<?
	include($repository."footer.php");
?>

<!-- SCRIPT --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<? if($tpmod=="") { ?>
	<script>
		$(document).ready(function() {
			$('#datatable').dataTable( {
				"oLanguage": { "sUrl": "<?echo $repository; ?>lib/datatables/dataTables.txt" },
				"iDisplayLength": 50,
				"aaSorting": [[ 1, "asc" ]]
			} );
		} );	
		
		<?php echo $jsaction ?>
	</script>
<? } ?>

<? if($tpmod=="SUBMIT"||$tpmod=="MODIFY"||$tpmod=="DELETE") { ?>
	<script type="text/javascript">
		$('#label').focus();
		
		<?php echo $jsaction ?>
		
	</script>
<? } ?>





	
	
