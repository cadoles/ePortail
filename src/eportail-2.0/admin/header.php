<? 
	if($_SESSION['user_profil'] != 1 || !$config["consoleadmin"]) {
		echo "<br><br><br><center>".$_SESSION['user_login']." - ".$config["consoleadmin"]." Vous n'avez les permissions nécessaire</center>";
	}
	
	if($_SESSION['user_profil'] == 1 && $config["consoleadmin"]) {
?>
	
	
	<nav id="mynavbar" class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="navbar-header">
			<a class="navbar-brand" href="config.php">Administration</a>
			
			<div class="navbar-toggle">
				<!-- Icone regroupant le menu en mode tablette -->
				<a class="glyphicon glyphicon-align-justify" data-toggle="collapse" data-toggle="collapse" data-target=".navbar-ex1-collapse"></a>
			</div>
		</div>
		
		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul id="mysidebar" class="nav navbar-nav side-nav">
				<legend>Configuration</legend>
				<li id="<? echo basename($_SERVER['PHP_SELF']); ?>" class="<? if(basename($_SERVER['PHP_SELF'])=="config.php") echo 'active'?>">
					<a href="config.php">Générale</a>
					<div></div>
				</li>

				<? if($config["activerLDAPModif"]) { ?>
				<li id="<? echo basename($_SERVER['PHP_SELF']); ?>" class="<? if(basename($_SERVER['PHP_SELF'])=="ldap.php") echo 'active'?>">
					<a href="ldap.php">LDAP</a>
					<div></div>
				</li>
				<? } ?>
				
				<li id="<? echo basename($_SERVER['PHP_SELF']); ?>" class="<? if(basename($_SERVER['PHP_SELF'])=="icon.php") echo 'active'?>">
					<a href="icon.php">Gestion des Icônes</a>
					<div></div>
				</li>		   


				<? if($config["activerCAS"]) { ?>
				<legend>SSO</legend>
				<li id="<? echo basename($_SERVER['PHP_SELF']); ?>" class="<? if(basename($_SERVER['PHP_SELF'])=="ssoattribut.php") echo 'active'?>">
					<a href="ssoattribut.php">Attribut</a>
					<div></div>
				</li>
				<li id="<? echo basename($_SERVER['PHP_SELF']); ?>" class="<? if(basename($_SERVER['PHP_SELF'])=="ssocommunity.php") echo 'active'?>">
					<a href="ssocommunity.php">Communauté</a>
					<div></div>
				</li>
				<li id="<? echo basename($_SERVER['PHP_SELF']); ?>" class="<? if(basename($_SERVER['PHP_SELF'])=="ssoprofil.php") echo 'active'?>">
					<a href="ssoprofil.php">Profil</a>
					<div></div>
				</li>	
				<? } ?>
				
				
				<? if($config["activerLDAP"]) { ?>
				<legend>LDAP</legend>
				<li id="<? echo basename($_SERVER['PHP_SELF']); ?>" class="<? if(basename($_SERVER['PHP_SELF'])=="ldapcommunity.php") echo 'active'?>">
					<a href="ldapcommunity.php">Communauté</a>
					<div></div>
				</li>
				<li id="<? echo basename($_SERVER['PHP_SELF']); ?>" class="<? if(basename($_SERVER['PHP_SELF'])=="ldapprofil.php") echo 'active'?>">
					<a href="ldapprofil.php">Profil</a>
					<div></div>
				</li>
				<? } ?>

				<legend>Utilisateur</legend>
				<li id="<? echo basename($_SERVER['PHP_SELF']); ?>" class="<? if(basename($_SERVER['PHP_SELF'])=="user.php") echo 'active'?>">
					<a href="user.php">Gestion des Utilisateurs</a>
					<div></div>
				</li>
			  
			  
				<legend>Bureau</legend>
				<li id="<? echo basename($_SERVER['PHP_SELF']); ?>" class="<? if(basename($_SERVER['PHP_SELF'])=="applicationcategorie.php") echo 'active'?>">
					<a href="applicationcategorie.php">Gestion des Catégories</a>
					<div></div>
				</li>
				<li id="<? echo basename($_SERVER['PHP_SELF']); ?>" class="<? if(basename($_SERVER['PHP_SELF'])=="application.php") echo 'active'?>">
					<a href="application.php">Gestion des Applications</a>
					<div></div>
				</li>
				<li id="<? echo basename($_SERVER['PHP_SELF']); ?>" class="<? if(basename($_SERVER['PHP_SELF'])=="applicationprofil.php") echo 'active'?>">
					<a href="applicationprofil.php">Applications par Profils</a>
					<div></div>
				</li>	
				<? if($config["activerCAS"]) { ?>
				<li id="<? echo basename($_SERVER['PHP_SELF']); ?>" class="<? if(basename($_SERVER['PHP_SELF'])=="applicationssoprofil.php") echo 'active'?>">
					<a href="applicationssoprofil.php">Applications par Profil SSO</a>
					<div></div>
				</li>	
				<? } ?>
				<? if($config["activerLDAP"]) { ?>
				<li id="<? echo basename($_SERVER['PHP_SELF']); ?>" class="<? if(basename($_SERVER['PHP_SELF'])=="applicationldapprofil.php") echo 'active'?>">
					<a href="applicationldapprofil.php">Applications par Profils LDAP</a>
					<div></div>
				</li>	
				<? } ?>

				<legend>Widget</legend>
				<li id="<? echo basename($_SERVER['PHP_SELF']); ?>" class="<? if(basename($_SERVER['PHP_SELF'])=="widgetcategorie.php") echo 'active'?>">
					<a href="widgetcategorie.php">Gestion des Catégories</a>
					<div></div>
				</li>		
				<li id="<? echo basename($_SERVER['PHP_SELF']); ?>" class="<? if(basename($_SERVER['PHP_SELF'])=="widget.php") echo 'active'?>">
					<a href="widget.php">Gestion des Widgets</a>
					<div></div>
				</li>		
				
				
				<legend>Page</legend>
				<li id="<? echo basename($_SERVER['PHP_SELF']); ?>" class="<? if(basename($_SERVER['PHP_SELF'])=="panelwidget.php") echo 'active'?>">
					<a href="panelwidget.php">Pages type WIDGET</a>
					<div></div>
				</li>
				<li id="<? echo basename($_SERVER['PHP_SELF']); ?>" class="<? if(basename($_SERVER['PHP_SELF'])=="panelediteur.php") echo 'active'?>">
					<a href="panelediteur.php">Pages type EDITEUR</a>
					<div></div>
				</li>
				<li id="<? echo basename($_SERVER['PHP_SELF']); ?>" class="<? if(basename($_SERVER['PHP_SELF'])=="panelurl.php") echo 'active'?>">
					<a href="panelurl.php">Pages type URL</a>
					<div></div>
				</li>
				<li id="<? echo basename($_SERVER['PHP_SELF']); ?>" class="<? if(basename($_SERVER['PHP_SELF'])=="panelprofil.php") echo 'active'?>">
					<a href="panelprofil.php">Pages par Profil</a>
					<div></div>
				</li>	
				<? if($config["activerCAS"]) { ?>
				<li id="<? echo basename($_SERVER['PHP_SELF']); ?>" class="<? if(basename($_SERVER['PHP_SELF'])=="panelssoprofil.php") echo 'active'?>">
					<a href="panelssoprofil.php">Pages par Profil SSO</a>
					<div></div>
				</li>	
				<? } ?>
				<? if($config["activerLDAP"]) { ?>
				<li id="<? echo basename($_SERVER['PHP_SELF']); ?>" class="<? if(basename($_SERVER['PHP_SELF'])=="panelldapprofil.php") echo 'active'?>">
					<a href="panelldapprofil.php">Pages par Profil LDAP</a>
					<div></div>
				</li>	
				<? } ?>

				<legend>Flux</legend>
				<li id="<? echo basename($_SERVER['PHP_SELF']); ?>" class="<? if(basename($_SERVER['PHP_SELF'])=="flux.php") echo 'active'?>">
					<a href="flux.php">Flux</a>
					<div></div>
				</li>
				<li id="<? echo basename($_SERVER['PHP_SELF']); ?>" class="<? if(basename($_SERVER['PHP_SELF'])=="fluxprofil.php") echo 'active'?>">
					<a href="fluxprofil.php">Flux par Profil</a>
					<div></div>
				</li>	
				<? if($config["activerCAS"]) { ?>
				<li id="<? echo basename($_SERVER['PHP_SELF']); ?>" class="<? if(basename($_SERVER['PHP_SELF'])=="fluxssoprofil.php") echo 'active'?>">
					<a href="fluxssoprofil.php">Flux par Profil SSO</a>
					<div></div>
				</li>	
				<? } ?>
				<? if($config["activerLDAP"]) { ?>
				<li id="<? echo basename($_SERVER['PHP_SELF']); ?>" class="<? if(basename($_SERVER['PHP_SELF'])=="fluxldapprofil.php") echo 'active'?>">
					<a href="fluxldapprofil.php">Flux par Profil LDAP</a>
					<div></div>
				</li>	
				<? } ?>
				
				<legend>Synchronisation</legend>
				<? if(is_dir("/var/www/html/balado")) { ?>
				<li class="<? if(basename($_SERVER['PHP_SELF'])=="externe.php?src=/posh-profil/synchro/balado-admin.php") echo 'active'?>">
					<a href="externe.php?src=/posh-profil/synchro/balado-admin.php">Balado</a>
					<div></div>
				</li>	
				<? } ?>
				<? if(is_dir("/var/www/html/cdt")) { ?>
				<li id="<? echo basename($_SERVER['PHP_SELF']); ?>" class="<? if(basename($_SERVER['PHP_SELF'])=="externe.php?src=/posh-profil/synchro/cdt-admin.php") echo 'active'?>">
					<a href="externe.php?src=/posh-profil/synchro/cdt-admin.php">CdT</a>
					<div></div>
				</li>
				<? } ?>
				<? if(is_dir("/var/www/html/iconito")) { ?>
				<li id="<? echo basename($_SERVER['PHP_SELF']); ?>" class="<? if(basename($_SERVER['PHP_SELF'])=="externe.php?src=/posh-profil/synchro/iconito-admin.php") echo 'active'?>">
					<a href="externe.php?src=/posh-profil/synchro/iconito-admin.php">Iconito</a>
					<div></div>
				</li>
				<? } ?>
				<? if(is_dir("/var/www/html/moodle")) { ?>
				<li id="<? echo basename($_SERVER['PHP_SELF']); ?>" class="<? if(basename($_SERVER['PHP_SELF'])=="externe.php?src=/posh-profil/synchro/moodle-admin.php") echo 'active'?>">
					<a href="externe.php?src=/posh-profil/synchro/moodle-admin.php">Moodle</a>
					<div></div>
				</li>
				<? } ?>
				<? if(is_dir("/var/www/html/wordpress")) { ?>
				<li id="<? echo basename($_SERVER['PHP_SELF']); ?>" class="<? if(basename($_SERVER['PHP_SELF'])=="externe.php?src=/posh-profil/synchro/wordpress-admin.php") echo 'active'?>">
					<a href="externe.php?src=/posh-profil/synchro/wordpress-admin.php">Wordpress</a>
					<div></div>
				</li>
				<? } ?>								
				<li>$nbsp;</li>
				<li>$nbsp;</li>					
			</ul>
		</div>
	</nav>
<?
	}
?>

<script>
	//document.getElementById('<? echo basename($_SERVER['PHP_SELF']); ?>').scrollIntoView(true);
    //$("mynavbar").animate({ scrollTop: $(".active").offset().top }, 500);
</script>


