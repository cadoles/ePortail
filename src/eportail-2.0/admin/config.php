<? 
	$repository="../";
	include($repository."include/include.php");
	include($repository."header.php"); 
		
	if($_SESSION['user_profil'] != 1 || !$config["consoleadmin"]) {
		echo "<br><br><br><center>Vous n'avez les permissions nécessaire</center>";
	}
	
	if($_SESSION['user_profil'] == 1 && $config["consoleadmin"]) {
		echo "<body>";

	$fgval			=$_POST["fgval"];
	
	$envtitle		=$_POST["envtitle"];
	$themedir		=$_POST["themedir"];
	$locallogo		=$_POST["locallogo"];
	$localheader	=$_POST["localheader"];

	
	if($fgval!="") {
		if($_FILES['filelocallogo']['tmp_name']!="") {
			$destination = $repository."local/images/logo/logo.png";
			$resultat    = move_uploaded_file($_FILES['filelocallogo']['tmp_name'],$destination);
			if($resultat!=1) {
				echo "<script>alert('Le téléchargement du logo a échoué');</script>";
			}
		}
		
		if($_FILES['filelocalheader']['tmp_name']!="") {
			$destination = $repository."local/images/header/header.png";
			$resultat    = move_uploaded_file($_FILES['filelocalheader']['tmp_name'],$destination);
			if($resultat!=1) {
				echo "<script>alert('Le téléchargement de la bannière a échoué');</script>";
			}
		}		
		
		$fp=fopen($config['localdirectory']."/local/config/config.php","w");
		fwrite($fp, "<?\n");
		fwrite($fp, "\$config['envtitle']    = '".addslashes($envtitle)."';\n");
		fwrite($fp, "\$config['themedir']    = '$themedir';\n");
		fwrite($fp, "\$config['locallogo']   = '$locallogo';\n");
		fwrite($fp, "\$config['localheader'] = '$localheader';\n");
		fwrite($fp, "\n?>");
		fclose($fp);
		
		// Reload de la page
		echo "<script>parent.document.location.reload();</script>";
		
	}
?>

<div id="wrapper">
	<? include("header.php"); ?>
	
	<div id="page-wrapper">
		<div class="container-fluid">
			<form class="form-horizontal" role="form" method='post' enctype="multipart/form-data">
				<legend><h1>CONFIGURATION GENERALE</h1></legend>
				<input type="hidden" name="MAX_FILE_SIZE" value="2097152">
				
				<div class="form-group">
					<div class="col-sm-12">
						<input name="fgval" type="submit" class="btn btn-primary" value="Enregistrer">
					</div>
				</div>

				<div class="form-group">
					<label for="envtitle" class="col-sm-3 control-label">Titre</label>
					<div class="col-sm-6">
						<input name="envtitle" id="envtitle" type="titre" class="form-control" placeholder="Titre" value="<? echo $config['envtitle']; ?>">
					</div>
				</div>
				
				<div class="form-group">
					<label for="themedir" class="col-sm-3 control-label">Thème</label>
					<div class="col-sm-6">
						<select name="themedir" id="themedir" class="form-control">
							<option value="" <? if($config['themedir']=="")   echo 'selected';?> >Thème de base</option>
							<?
								if ($handle = opendir($config['localdirectory'].'/style/themes/')) {
									while (false !== ($entry = readdir($handle))) {
										if ($entry != "." && $entry != ".." && $entry!="readme.txt") {
											$lbsel="";
											if($entry==$config['themedir']) $lbsel=" selected";
											echo "<option value='$entry' $lbsel>$entry</option>";
										}
									}
									closedir($handle);
								}
							?>
						</select>			
					</div>
				</div>		
						
				<div class="form-group">
					<label for="locallogo" class="col-sm-3 control-label">Personnaliser le logo</label>
					<div class="col-sm-6">
						<select name="locallogo" id="locallogo" class="form-control" onChange="ShowHide();">
							<option value=""  <? if($config['locallogo']=="") echo 'selected';?> >non</option>
							<option value="1" <? if($config['locallogo']!="") echo 'selected';?> >oui</option>
						</select>			
					</div>
				</div>

				<div id="form-group-locallogo" class="form-group">
					<label for="filelocallogo" class="col-sm-3 control-label">Logo</label>
					<div class="col-sm-6">
						<input name="filelocallogo" id="filelocallogo" type="file" >
						<?
							echo "<div style='margin:10px 0px 0px 0px; background-color:#cdcdcd;'><img src='".$repository."local/images/logo/logo.png' style='max-width:100%'></div>";
						?>
					</div>
				</div>
				
				<div class="form-group">
					<label for="localheader" class="col-sm-3 control-label">Personnaliser la bannière</label>
					<div class="col-sm-6">
						<select name="localheader" id="localheader" class="form-control" onChange="ShowHide();">
							<option value=""  <? if($config['localheader']=="") echo 'selected';?> >non</option>
							<option value="1" <? if($config['localheader']!="") echo 'selected';?> >oui</option>
						</select>			
					</div>
				</div>

				<div id="form-group-localheader" class="form-group">
					<label for="filelocalheader" class="col-sm-3 control-label">Bannière</label>
					<div class="col-sm-6">
						<input name="filelocalheader" id="filelocalheader" type="file" >
						<?
							echo "<div style='margin:10px 0px 0px 0px; background-color:#cdcdcd;'><img src='".$repository."local/images/header/header.png' style='max-width:100%'></div>";
						?>
					</div>
				</div>	
								
			</form>
		</div>
	</div>
</div>
<? } ?>

<!-- FOOTER --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<?
	include($repository."footer.php");
?>

<!-- SCRIPT --------------------------------------------------------------------------------------------------------------------------------------- -->
<script>
	function ShowHide() {
		if($("#locallogo").val()=="1") {
			$('#form-group-locallogo').css('display','block');
			
		}
		else {
			$('#form-group-locallogo').css('display','none');
		}
		
		if($("#localheader").val()=="1") {
			$('#form-group-localheader').css('display','block');
			
		}
		else {
			$('#form-group-localheader').css('display','none');
		}		
	}	

	$(document).ready(function()
	{
		ShowHide();
	});
</script>
