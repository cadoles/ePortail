<?
	$repository="../";
	include($repository."include/include.php");
	include($repository."include/delete.php");
	include($repository."header.php"); 
		
	if($_SESSION['user_profil'] != 1 || !$config["consoleadmin"]) {
		echo "<br><br><br><center>Vous n'avez les permissions nécessaire</center>";
	}
	
	if($_SESSION['user_profil'] == 1 && $config["consoleadmin"]) {
		echo "<body>";
	
	$db1=new ps_db;
	$db2=new ps_db;
	
	$tpmod			=$_POST['tpmod'];
	$vladd			=$_POST['vladd'];
	$vlsup			=$_POST['vlsup'];
	
	$icon_id		=$_POST['icon_id'];
	$icon_url	=$_POST['icon_url'];

	if($_GET['tpmod']!="") {
		$tpmod=$_GET['tpmod'];
		$igico=$_GET['igico'];
		$idico=$_GET['idico'];
	}
	
	/*--> Controle de cohérance */
	if($vladd!="") {
		$fgerr="";

		if($icon_url=="") {
			$jsaction="alert('Vous devez renseigner l\'ensemble des informations');";
			$fgerr=1;
		}
	}
	
	/*--> Rester sur le mode encours */
	if($vlmod!=""&&fgerr!="") $tpmod="MODIFY";
	if($vladd!=""&&fgerr!="") $tpmod="SUBMIT";
		
	/*--> Submit */
	if($vladd!=""&&$fgerr=="") {
		$q="INSERT INTO env_icon(icon_url) VALUES('$icon_url')";
		$db1->query($q);
		$tpmod="";
	}

	/*--> Delete */
	if($vlsup!="") {
		$q="DELETE FROM env_icon WHERE icon_id=$icon_id";
		$db1->query($q);
		$tpmod="";
	}
	
	if($tpmod!="SELECT") {
		echo "<div id='wrapper'>";
		include("header.php");
		echo "<div id='page-wrapper'>";
		echo "<div class='container-fluid'>";
	}
	
	echo "<form id='formulaire' class='form-horizontal' role='form' method='post' enctype='multipart/form-data'>";
	
	echo "<input id='icon_id' name='icon_id' type='hidden' value='".$icon_id."'>";
	echo "<input id='tpmod' name='tpmod' type='hidden' value=''>";
	
  
//-- DISPLAY -------------------------------------------------------------------------------------------------------------------------------

if($tpmod==""||$tpmod=="SELECT") { 
    if($tpmod=="") {
		echo "<legend><h1>GESTION DES ICÔNES</h1></legend>";
    
		echo "<div class='form-group'>";
		echo "<div class='col-sm-12'>";
		echo "<input id='fgadd' name='fgadd' class='btn btn-primary' data-toggle='modal' data-target='.bs-item-modal' type='submit' value='Ajouter avec découpe' onClick='changeFrame(\"submit\",0)'>";
		echo "&nbsp;<input id='fgupl' name='fgupl' class='btn btn-primary' data-toggle='modal' data-target='.bs-item-modal' type='submit' value='Ajouter sans découpe' onClick='changeFrame(\"submitupl\",0)'>";
		echo "</div>";
		echo "</div>";
	}
	
	$q="SELECT * FROM env_icon";
	$db1->query($q);
	while($db1->next_record()){	
		if($tpmod=="") echo "<div style='width:95px; float:left; '>";
		else echo "<div style='width:45px; float:left; '>";
		
		echo "<div id='myavatarprofil' style='float:left; margin-right:5px; margin-bottom:5px; border:none;'>";
		if($tpmod=="SELECT") echo "<a href='' onClick='selIcone(".$db1->f('icon_id').",\"".$config['urlbase']."/local/images/icon/".$db1->f('icon_url')."\")'>";
		echo "<img id='imgapp".$db1->f('icon_id')."' src='".$repository."local/images/icon/".$db1->f('icon_url')."' width='40px' height='40px'></img>";
		if($tpmod=="SELECT") echo "</a>";
		echo "</div>";
		
		if($tpmod=="") {
			if($db1->f('icon_id')>0) {
				echo "<div style='float:left;'>";
				echo "<a class='glyphicon glyphicon-plus' data-toggle='modal' data-target='.bs-item-modal' title='Modifier un icône avec découpe' onClick='changeFrame(\"update\",".$db1->f('icon_id').")'></a>";
				echo "<br><a class='glyphicon glyphicon-plus' data-toggle='modal' data-target='.bs-item-modal' title='Modifier un icône sans découpe' onClick='changeFrame(\"updateupl\",".$db1->f('icon_id').")'></a>";

				// Pas de suppression possible si icone lié à une application
				$q="SELECT application_id FROM env_application WHERE application_icon=".$db1->f('icon_id');
				$db2->query($q);
				if(!$db2->next_record()) {
					echo "<br><a class='glyphicon glyphicon-remove' onClick='$(\"#icon_id\").val(\"".$db1->f('icon_id')."\"); $(\"#tpmod\").val(\"DELETE\");$(\"#formulaire\").submit();' title='Supprimer icône'></a>";
				}
				
				echo "</div>";
			}
		}
		
		echo "</div>";
	} 
	
	?>
	<!-- Popup modification item !-->
	<div id="mymodal" class="modal fade bs-item-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-lg" style=" max-width:655px; width:80%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">MODIFICATION ITEM</h4>
			</div>
			<div class="modal-body">
				<iframe id="framemodal" frameborder=0 width="100%" height="520px"></iframe>
			</div>
		</div>
	  </div>
	</div>	
	<?
}

//-- DELETE --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="DELETE") {
	echo "<legend><h1>SUPPRESSION ICONE</h1></legend>";
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";	
	echo "<input id='vlsup' name='vlsup' class='btn btn-primary' type='submit' value='Confirmer la suppression' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";    
	echo "</div>";
	echo "</div>";
	
	$q = "SELECT * FROM env_icon WHERE icon_id=$icon_id";
	$db1->query($q);
	if($db1->next_record()) {
		echo "<br><div id='appicon' class='appicon'>";
		echo "<img id='imgapp".$db1->f('icon_id')."' src='".$repository."local/images/icon/".$db1->f('icon_url')."' width='90px' height='90px'></img>";
		echo "</div>";
	}
}

echo "</form>";
if($tpmod!="SELECT") echo "</div></div></div>";

}
?>

<!-- FOOTER --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<?
	include($repository."footer.php");
?>

<!-- SCRIPT --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<script type="text/javascript">
	function changeFrame(mode,iditem) {
		srcframe="<? echo $repository ?>jcrop.php?dirimg=local/images/icon&repository=<? echo $repository ?>";
		
		if(mode=="submit") {
			srcframe=srcframe+"&query=INSERT INTO env_icon (icon_url) VALUES($$)&refresh=1";
		}
		else if(mode=="update") {
			srcframe=srcframe+"&igico=imgapp"+iditem+"&query=UPDATE env_icon SET icon_url=$$ WHERE icon_id="+iditem;
		}
		else if(mode=="submitupl") {
			srcframe="<? echo $repository ?>dropzone.php?directory=local/images/icon&typeupload=image&prefix=thumbnail_&resize=true&size=90&query=insertappico&refresh=1";
		}
		else if(mode=="updateupl") {
			srcframe="<? echo $repository ?>dropzone.php?directory=local/images/icon&typeupload=image&prefix=thumbnail_&resize=true&size=90&query=updateappico&idquery="+iditem+"&repository=<? echo $repository ?>&idimage=imgapp"+iditem;
		}
		
		$("#framemodal").attr("src",srcframe);
	}
	
	function recharge() {
		$("#formulaire").submit();
	}

	function query(type,id,url) {

	}
	
	function closemodal() {
		$('#mymodal').modal('hide');
	}

	function selIcone(idico,igico) {
		parent.document.getElementById('<? echo $idico; ?>').value=idico;
		parent.document.getElementById('<? echo $igico; ?>').src=igico;
		window.parent.closemodal();	
	}

	<?php echo $jsaction ?>
	
</script>








	
	
