<?
	$repository="../";
	include($repository."include/include.php");
	include($repository."include/delete.php");
	include($repository."header.php"); 

	if($_SESSION['user_profil'] != 1 || !$config["consoleadmin"]) {
		echo "<br><br><br><center>Vous n'avez les permissions nécessaire</center>";
	}
	
	if($_SESSION['user_profil'] == 1 && $config["consoleadmin"]) {
		echo "<body>";
		
	$db1=new ps_db;
	
	$tpmod				=$_POST['tpmod'];
	$vlmod				=$_POST['vlmod'];
	$vladd				=$_POST['vladd'];
	$vlsup				=$_POST['vlsup'];
	
	$id					=$_POST['id'];
	$label				=$_POST['label'];
	$order				=$_POST['order'];
	$idproprio			=$_POST['idproprio'];
	
	/*--> Controle de cohérance */
	if($vladd!=""||$vlmod!="") {
		$fgerr="";
		if($label==""||$idproprio=="") {
			$jsaction="alert('Vous devez renseigner l\'ensemble des informations');";
			$fgerr=1;
		}
	}
	
	/*--> Rester sur le mode encours */
	if($vlmod!=""&&fgerr!="") $tpmod="MODIFY";
	if($vladd!=""&&fgerr!="") $tpmod="SUBMIT";
		
	/*--> Submit */
	if($vladd!=""&&$fgerr=="") {
		$q="INSERT INTO env_panel(panel_label, panel_user, panel_url, panel_order, panel_type, panel_template) VALUES('".addslashes($label)."',$idproprio,'widget.php','$order',4,1)";
		$db1->query($q);
		$tpmod="";
	}

	/*--> Modify */
	if($vlmod!=""&&$fgerr=="") {
		$q="UPDATE env_panel SET panel_label='".addslashes($label)."', panel_user=$idproprio, panel_url='widget.php',panel_order='$order' WHERE panel_id=$id";
		$db1->query($q);
		$tpmod="";
	}

	/*--> Delete */
	if($vlsup!="") {
		delPanel($id);
	}	

	echo "<div id='wrapper'>";
	include("header.php");
	echo "<div id='page-wrapper'>";
	echo "<div class='container-fluid'>";
	echo "<form id='formulaire' class='form-horizontal' role='form' method='post' enctype='multipart/form-data'>";
	
	echo "<input id='id' name='id' type='hidden' value='".$id."'>";
	echo "<input id='tpmod' name='tpmod' type='hidden' value=''>";
	
  
//-- DISPLAY -------------------------------------------------------------------------------------------------------------------------------

if($tpmod=="") { 
    echo "<legend><h1>GESTION DES PAGES WIDGET</h1></legend>";
    
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='fgadd' name='fgadd' class='btn btn-primary' onClick='$(\"#id\").val(\"\"); $(\"#tpmod\").val(\"SUBMIT\");' type='submit' value='Ajouter' />";
	echo "</div>";
	echo "</div>";
		
	$q="SELECT * FROM env_panel, env_user, env_profil WHERE panel_type=4 AND user_id=panel_user AND profil_id=user_profil ORDER BY panel_id DESC";
	$db1->query($q);

	echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
    echo "<thead>";
	echo "<th width='70px'>Action</th>";
	echo "<th width='70px'>Ordre</th>";
	echo "<th width='70px'>ID</th>";
	echo "<th>Libellé</th>";
	echo "<th>Propriètaire</th>";
	echo "</thead>";
	
	while($db1->next_record()){	
		echo "<tr>";

		echo "<td align='center'>";
		echo "<a class='glyphicon glyphicon-file' onClick='$(\"#id\").val(\"".$db1->f('panel_id')."\"); $(\"#tpmod\").val(\"MODIFY\");$(\"#formulaire\").submit();'></a>";
		echo "<a class='glyphicon glyphicon-remove' onClick='$(\"#id\").val(\"".$db1->f('panel_id')."\"); $(\"#tpmod\").val(\"DELETE\");$(\"#formulaire\").submit();'></a>";
		echo "</td>";

		echo "<td>";
		echo $db1->f('panel_order');
		echo "</td>";
				
		echo "<td>";
		echo $db1->f('panel_id');
		echo "</td>";

		echo "<td>";
		echo "<a href='".$repository."index.php?action=page&id=".$db1->f('panel_id')."' target='_blank'>".$db1->f('panel_label');
		echo "</td>";
		
		echo "<td>";
		if($db1->f('user_avatar')=="") 
			echo "<img src='".$repository."style/images/blank.gif' class='myavatarvide' width='45px' height='45px' style='float:left;margin-right:5px;'></img>";
		else
			echo "<img src='".$repository."local/images/avatar/".$db1->f('user_avatar')."' width='45px' height='45px' style='float:left;margin-right:5px;'></img>";
		
		echo "<span style='float:left; font-size:80%;'>";
		echo "Login : ".$db1->f('user_login')."<br>";
		echo "Pseudo : ".$db1->f('user_pseudo')."<br>";
		echo "Profil : ".$db1->f('profil_label')."<br>";		
		echo "</span>";
		echo "</td>";
		
		echo "</tr>";
	} 

	echo "</table>";
}

//-- SUBMIT --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="SUBMIT") {
	// Entete du formulaire
    echo "<legend><h1>AJOUT PAGE WIDGET</h1></legend>";
    
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vladd' name='vladd' class='btn btn-primary' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
	echo "</div>";
	echo "</div>";	

	// Valeur par défaut
	// aucune
	$idproprio=-1;
	$order=1;
	
	$q="SELECT * FROM env_user, env_profil  WHERE user_id=$idproprio AND profil_id=user_profil";
	$db1->query($q);
	if($db1->next_record()){		
		$lbavatar	= $db1->f('user_avatar');
		$lbproprio	= $db1->f('user_firstname')." ".$db1->f('user_lastname');
		$lbpseudo	= $db1->f('user_pseudo');
		$lblogin	= $db1->f('user_login');
		$lbprofil	= $db1->f('profil_label');
	}
}

//-- MODIFY --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="MODIFY") {
	// Entete du formulaire
    echo "<legend><h1>MODIFICATION PAGE WIDGET</h1></legend>";
   
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlmod' name='vlmod' class='btn btn-primary' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
	echo "</div>";
	echo "</div>";	

	// Valeur par défaut
	$q = "SELECT * FROM env_panel, env_user, env_profil WHERE panel_id=$id AND panel_user=user_id AND profil_id=user_profil";
	$db1->query($q);
	if($db1->next_record()) {
		$idproprio	= $db1->f('panel_user');
		$label 		= $db1->f('panel_label');
		$lbavatar	= $db1->f('user_avatar');
		$lbproprio	= $db1->f('user_firstname')." ".$db1->f('user_lastname');
		$lbpseudo	= $db1->f('user_pseudo');
		$lblogin	= $db1->f('user_login');
		$lbprofil	= $db1->f('profil_label');
		$idtemplate	= $db1->f('panel_page_template_id');
		$order		= $db1->f('panel_order');
	}
}

//-- DELETE --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="DELETE") {
	echo "<legend><h1>SUPPRESSION PAGE WIDGET</h1></legend>";
	
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlsup' name='vlsup' class='btn btn-primary' type='submit' value='Confirmer la suppression' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";    
	echo "</div>";
	echo "</div>";
	
	$q = "SELECT * FROM env_panel, env_user, env_profil WHERE panel_id=$id AND panel_user=user_id AND profil_id=user_profil";
	$db1->query($q);
	if($db1->next_record()) {
		$idproprio	= $db1->f('panel_user');
		$label 		= $db1->f('panel_label');
		$lbavatar	= $db1->f('user_avatar');
		$lbproprio	= $db1->f('user_firstname')." ".$db1->f('user_lastname');
		$lbpseudo	= $db1->f('user_pseudo');
		$lblogin	= $db1->f('user_login');
		$lbprofil	= $db1->f('profil_label');
		$idtemplate	= $db1->f('panel_page_template_id');
		$order		= $db1->f('panel_order');
	}
}


//-- FORMULAIRE -----------------------------------------------------------------------------------------------------------------------------

if($tpmod=="SUBMIT"||$tpmod=="MODIFY"||$tpmod=="DELETE") {
?>
	<fieldset class="row fieldset" style="clear:both">
		<legend>Description</legend>

		<div class="form-group">
			<label for="id_bis" class="col-sm-3 control-label">ID*</label>
			<div class="col-sm-6"><input name="id_bis" id="id_bis" type="titre" class="form-control" disabled="disabled" placeholder="ID Application" value="<? echo $id; ?>"></div>
		</div>
		

		<div class="form-group">
			<label for="label" class="col-sm-3 control-label">Libellé*</label>
			<div class="col-sm-6"><input name="label" id="label" type="titre" class="form-control" placeholder="Libellé" value="<? echo $label; ?>"></div>
		</div>	
		
		<div class="form-group">
			<label for="order" class="col-sm-3 control-label">Ordre*</label>
			<div class="col-sm-6"><input name="order" id="order" type="number" class="form-control" placeholder="Ordre" value="<? echo $order; ?>"></div>
		</div>
	</fieldset>
	
	<fieldset class="row fieldset" style="clear:both">
		<legend>Propriétaire</legend>
		
		<div style="width:200px; margin:auto;">
		<?
		if($lbavatar=="") 
			echo "<img src='".$repository."style/images/blank.gif' class='myavatarvide' width='45px' height='45px' style='float:left;margin-right:5px;'></img>";
		else
			echo "<img src='".$repository."local/images/avatar/".$lbavatar."' width='45px' height='45px' style='float:left;margin-right:5px;'></img>";
		
		echo "<span style='float:left; font-size:80%;'>";
		echo "Login : ".$lblogin."<br>";
		echo "Pseudo : ".$lbpseudo."<br>";
		echo "Profil : ".$lbprofil."<br>";		
		echo "</span>";
		?>
		</div>	
		
		<input 	value="<? echo $idproprio; ?>" id="idproprio" name="idproprio" type="hidden" readonly>		
	</fieldset>
<?
}

echo "</form></div></div></div>";

}

?>

<!-- FOOTER --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<?
	include($repository."footer.php");
?>

<!-- SCRIPT --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<? if($tpmod=="") { ?>
	<script>
		$(document).ready(function() {
			$('#datatable').dataTable( {
				"oLanguage": { "sUrl": "<?echo $repository; ?>lib/datatables/dataTables.txt" },
				"iDisplayLength": 50,
				"aaSorting": [[ 1, "asc" ]]
			} );
		} );	
		
		<?php echo $jsaction ?>
	</script>
<? } ?>

<? if($tpmod=="SUBMIT"||$tpmod=="MODIFY"||$tpmod=="DELETE") { ?>
	<script type="text/javascript">
		$('#label').focus();
		
		<?php echo $jsaction ?>
		
	</script>
<? } ?>





	
	
