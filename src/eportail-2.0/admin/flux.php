<?
	$repository="../";
	include($repository."include/include.php");
	include($repository."include/delete.php");
	include($repository."header.php"); 
		
	if($_SESSION['user_profil'] != 1 || !$config["consoleadmin"]) {
		echo "<br><br><br><center>Vous n'avez les permissions nécessaire</center>";
	}
	
	if($_SESSION['user_profil'] == 1 && $config["consoleadmin"]) {
		echo "<body>";

	$db1=new ps_db;

	$tpmod				= $_POST['tpmod'];
	$vlmod				= $_POST['vlmod'];
	$vladd				= $_POST['vladd'];
	$vlsup				= $_POST['vlsup'];
	$fgreload			= $_POST['fgreload'];

	$flux_id			= $_POST['flux_id'];
	$flux_name			= $_POST['flux_name'];
	$flux_type			= $_POST['flux_type'];
	$flux_url			= $_POST['flux_url'];
	$flux_order			= $_POST['flux_order'];
	$flux_number		= $_POST['flux_number'];
	
	if($fgreload!="") {
		$tpmod=$fgreload; 
	}

	/*--> Controle de cohérance */
	if($vladd!=""||$vlmod!="") {
		$fgerr="";

		if($flux_name==""||$flux_type==""||$flux_number=="") {
			$jsaction="alert('Vous devez renseigner l\'ensemble des informations');";
			$fgerr=1;
		}
		
		if($fgerr!=1&&$flux_type==0&&$flux_url=="") {
			$jsaction="alert('Vous devez renseigner l\'ensemble des informations');";
			$fgerr=1;
		}
		
		$q="SELECT * FROM env_flux WHERE flux_name='$flux_name'";
		if($vlmod!="") $q=$q." AND flux_id!=$flux_id";
		$db1->query($q);
		if($db1->next_record()){
			$jsaction="alert('Un flux avec ce nom existe déjà');";
			$fgerr=1;
		}		
	}

	/*--> Rester sur le mode encours */
	if($vlmod!=""&&fgerr!="") $tpmod="MODIFY";
	if($vladd!=""&&fgerr!="") $tpmod="SUBMIT";

	/*--> Submit */
	if($vladd!=""&&$fgerr=="") {
		$q="INSERT INTO env_flux(flux_name, flux_type, flux_url, flux_order, flux_number) VALUES('".addslashes($flux_name)."','$flux_type','$flux_url','$flux_order','$flux_number')";
		$db1->query($q);
		$tpmod="";
		if($flux_type==1) {
			$tpmod="MODIFY";
			$flux_id=mysql_insert_id();
		}
	}

	/*--> Modify */
	if($vlmod!=""&&$fgerr=="") {
		$q="UPDATE env_flux SET flux_name='".addslashes($flux_name)."', flux_type='$flux_type', flux_url='$flux_url', flux_order='$flux_order', flux_number='$flux_number' WHERE flux_id=$flux_id";
		$db1->query($q);
		$tpmod="";
	}

	/*--> Delete */
	if($vlsup!="") {
		delFlux($flux_id);
		$tpmod="";
	}

	echo "<div id='wrapper'>";
	include("header.php");
	echo "<div id='page-wrapper'>";
	echo "<div class='container-fluid'>";
	echo "<form id='formulaire' class='form-horizontal' role='form' method='post' enctype='multipart/form-data'>";
	
	echo "<input id='flux_id' name='flux_id' type='hidden' value='".$flux_id."'>";
	echo "<input id='tpmod' name='tpmod' type='hidden' value=''>";
	echo "<input id='fgreload' name='fgreload' type='hidden' value=''>";


//-- DISPLAY -------------------------------------------------------------------------------------------------------------------------------

if($tpmod=="") {
    echo "<h1>GESTION DES FLUX</h1>";

	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='fgadd' name='fgadd' class='btn btn-primary' onClick='$(\"#flux_id\").val(\"\"); $(\"#tpmod\").val(\"SUBMIT\");' type='submit' value='Ajouter' />";
	echo "</div>";
	echo "</div>";

	echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
    echo "<thead>";
	echo "<th width='70px'>Action</th>";
	echo "<th>ID</th>";
	echo "<th>Titre</th>";
	echo "<th>Type</th>";
	echo "<th>Ordre</th>";
	echo "<th>URL</th>";
	echo "</thead>";

	$q="SELECT * FROM env_flux";
	$db1->query($q);
	while($db1->next_record()){
		echo "<tr>";

		echo "<td>";
		echo "<a class='glyphicon glyphicon-file' onClick='$(\"#flux_id\").val(\"".$db1->f('flux_id')."\"); $(\"#tpmod\").val(\"MODIFY\");$(\"#formulaire\").submit();'></a>";
		if($db1->f('flux_id')>0) {
			echo "<a class='glyphicon glyphicon-remove' onClick='$(\"#flux_id\").val(\"".$db1->f('flux_id')."\"); $(\"#tpmod\").val(\"DELETE\");$(\"#formulaire\").submit();'></a>";
		}
		echo "</td>";

		echo "<td>";
		echo $db1->f('flux_id');
		echo "</td>";

		echo "<td>";
		echo $db1->f('flux_name');
		echo "</td>";

		echo "<td>";
		if($db1->f('flux_type')==0) 
			echo "RSS";
		elseif ($db1->f('flux_type')==2) 
			echo "URL";
		else
			echo "Articles";
		echo "</td>";

		echo "<td>";
		echo $db1->f('flux_order');
		echo "</td>";
		
		echo "<td>";
		echo $db1->f('flux_url');
		echo "</td>";		

		echo "</tr>";
	}
	echo "</table>";
}

//-- SUBMIT --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="SUBMIT") {
	// Entete du formulaire
    echo "<legend><h1>AJOUT FLUX</h1></legend>";
	
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vladd' name='vladd' class='btn btn-primary' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
	echo "</div>";
	echo "</div>";	

	// Valeur par défaut
	$flux_type  	= 0;
	$flux_order 	= 1;
	$flux_number 	= 5;
}

//-- MODIFY --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="MODIFY") {
	// Entete du formulaire
    echo "<legend><h1>MODIFICATION FLUX</h1></legend>";

	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlmod' name='vlmod' class='btn btn-primary' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
	echo "</div>";
	echo "</div>";	

	// Valeurs
	$q = "SELECT * FROM env_flux WHERE flux_id=$flux_id";
	$db1->query($q);
	if($db1->next_record()) {
		$flux_name 	= $db1->f("flux_name");
		$flux_type 	= $db1->f("flux_type");
		$flux_url 		= $db1->f("flux_url");
		$flux_order 	= $db1->f("flux_order");
		$flux_number 	= $db1->f("flux_number");
	}
}

//-- DELETE --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="DELETE") {
	echo "<legend><h1>SUPPRESSION APPLICATION</h1></legend>";
	
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlsup' name='vlsup' class='btn btn-primary' type='submit' value='Confirmer la suppression' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";    
	echo "</div>";
	echo "</div>";

	// Valeurs
	$q = "SELECT * FROM env_flux WHERE flux_id=$flux_id";
	$db1->query($q);
	if($db1->next_record()) {
		$flux_name 		= $db1->f("flux_name");
		$flux_type 		= $db1->f("flux_type");
		$flux_url 		= $db1->f("flux_url");
		$flux_order 	= $db1->f("flux_order");
		$flux_number 	= $db1->f("flux_number");		
	}
}


//-- FORMULAIRE -----------------------------------------------------------------------------------------------------------------------------

if($tpmod=="SUBMIT"||$tpmod=="MODIFY"||$tpmod=="DELETE") {
?>

	<fieldset class="row fieldset" style="clear:both">

		<div class="form-group">
			<label for="id_bis" class="col-sm-3 control-label">ID Flux*</label>
			<div class="col-sm-6"><input name="id_bis" id="id_bis" type="titre" class="form-control" disabled="disabled" placeholder="ID Flux" value="<? echo $flux_id; ?>"></div>
		</div>
		
		<div class="form-group">
			<label for="flux_name" class="col-sm-3 control-label">Nom*</label>
			<div class="col-sm-6"><input name="flux_name" id="flux_name" type="titre" class="form-control" placeholder="Nom" value="<? echo $flux_name; ?>"></div>
		</div>

		<div class="form-group">
			<label for="flux_type" class="col-sm-3 control-label">Type*</label>
			<div class="col-sm-6">
				<select name="flux_type" id="flux_type" class="form-control">
					<option value=0	<? if($flux_type==0)	{ echo 'selected'; }?>>RSS</option>
					<option value=1	<? if($flux_type==1)	{ echo 'selected'; }?>>Articles</option>
					<option value=2	<? if($flux_type==2)	{ echo 'selected'; }?>>URL</option>
				</select>
			</div>
		</div>
		
		<div id="blockurl" class="form-group">
			<label for="flux_url" class="col-sm-3 control-label">Url*</label>
			<div class="col-sm-6"><input name="flux_url" id="flux_url" type="titre" class="form-control" placeholder="Url" value="<? echo $flux_url; ?>"></div>
		</div>	

		<div class="form-group">
			<label for="flux_order" class="col-sm-3 control-label">Ordre*</label>
			<div class="col-sm-6"><input name="flux_order" id="flux_order" type="number" class="form-control" placeholder="Ordre" value="<? echo $flux_order; ?>"></div>
		</div>

		<div class="form-group">
			<label for="flux_number" class="col-sm-3 control-label">Nombre d'alertes affichées*</label>
			<div class="col-sm-6"><input name="flux_number" id="flux_order" type="number" class="form-control" placeholder="Niombre" value="<? echo $flux_number; ?>"></div>
		</div>
	</fieldset>


	<fieldset id="harticle" class="row fieldset" style="clear:both">
		<legend>Articles</legend>
		<?
			if($tpmod=="SUBMIT") echo "<center><br><br>Veuillez valider la création de votre Alerte avant de pouvoir y ajouter des Articles</center>";
			else {
				echo "<div class='form-group'>";
				echo "<div class='col-sm-12'>";
				echo "<a class='btn btn-primary' data-toggle='modal' data-target='.bs-item-modal' title='Séléctionner une application' onClick='changeFrame(\"submit\",$flux_id,0)'>Ajouter</a>";
				echo "</div>";
				echo "</div>";	

				echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";				
				echo "<thead>";
				echo "<th width='40px'>Action</th>";
				echo "<th width='150px'>Date</th>";
				echo "<th>Nom</th>";
				echo "</thead>";
				
				$q="SELECT * FROM env_fluxmsg WHERE fluxmsg_flux=".$flux_id." ORDER BY fluxmsg_date";
				$db1->query($q);
				while($db1->next_record()){	
					echo "<tr>";
					echo "<td>";
					echo "<a class='glyphicon glyphicon-file' data-toggle='modal' data-target='.bs-item-modal' onClick='changeFrame(\"modify\",$flux_id,".$db1->f("fluxmsg_id").")' title='Modifier un article' />";
					echo "<a class='glyphicon glyphicon-remove' data-toggle='modal' data-target='.bs-item-modal' onClick='changeFrame(\"delete\",$flux_id,".$db1->f("fluxmsg_id").")' title='Supprimer un article' />";
					echo "</td>";
					
					echo "<td>".$db1->f('fluxmsg_date')."</td>";
					echo "<td>".$db1->f('fluxmsg_name')."</td>";
					
					echo "</tr>";
				}		

				echo "</table>";
			}
		?>
	</fieldset>
	
	<!-- Popup modification articles !-->
	<div id="mymodal" class="modal fade bs-item-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-lg" style="width:90%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">ARTICLES</h4>
			</div>
			<div class="modal-body">
				<iframe id="framemodal" frameborder=0 width="100%" height="520px"></iframe>
			</div>
		</div>
	  </div>
	</div>	
<?
}

echo "</form></div></div></div>";

}
?>

<!-- FOOTER --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<?
	include($repository."footer.php");
?>

<!-- SCRIPT --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<script type="text/javascript">
	<?php echo $jsaction ?>
	
<? if($tpmod=="") { ?>
	$(document).ready(function() {
		$('#datatable').dataTable( {
			"oLanguage": { "sUrl": "<?echo $repository; ?>lib/datatables/dataTables.txt" },
			"iDisplayLength": 50,
			"aaSorting": [[ 1, "asc" ]],
		} );
	} );	
<? } ?>
	
<? if($tpmod=="SUBMIT"||$tpmod=="MODIFY"||$tpmod=="DELETE") { ?>	
	$(document).ready(function() {
		$('#flux_type').change(function() {
			ShowHide();
		});	

		$('#datatable').dataTable( {
			"oLanguage": { "sUrl": "<?echo $repository; ?>lib/datatables/dataTables.txt" },
			"iDisplayLength": 50,
			"aaSorting": [[ 1, "asc" ]],
		} );	
					
		ShowHide();		
	} );
	
	function changeFrame(mode,id1,id2) {
		if(mode=="submit") {
			srcframe="fluxmsg.php?tpmod=SUBMIT&id1="+id1;
		}
		else if(mode=="modify") {
			srcframe="fluxmsg.php?tpmod=MODIFY&id1="+id1+"&id2="+id2;
		}
		else {
			srcframe="fluxmsg.php?tpmod=DELETE&id1="+id1+"&id2="+id2;
		}
		
		$("#framemodal").attr("src",srcframe);
	}
			
	function ShowHide() {
		$this=$("#flux_type");
		
		if($this.val()=="0" || $this.val()=="2") {
			$("#blockurl").css("display","block");
			$("#harticle").css("display","none");
		}
		else {
			$("#blockurl").css("display","none");
			$("#harticle").css("display","block");
		}
	}	

	function recharge() {
		$("#formulaire").submit();
	}
	
	function closemodal() {
		$('#mymodal').modal('hide');
	}	
<? } ?>
	
</script>







