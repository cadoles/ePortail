<?
	$repository="../";
	include($repository."include/include.php");
	include($repository."include/delete.php");
	include($repository."header.php"); 
		
	if($_SESSION['user_profil'] != 1 || !$config["consoleadmin"]) {
		echo "<br><br><br><center>Vous n'avez les permissions nécessaire</center>";
	}
	
	if($_SESSION['user_profil'] == 1 && $config["consoleadmin"]) {
		echo "<body>";

	$db1=new ps_db;

	$tpmod				= $_POST['tpmod'];
	$vlmod				= $_POST['vlmod'];
	$vladd				= $_POST['vladd'];
	$vlsup				= $_POST['vlsup'];
	$fgclo				= $_POST['fgclo'];
	
	$id1				= $_POST['id1'];
	$id2				= $_POST['id2'];
	
	$name				= $_POST['name'];
	$date				= $_POST['date'];
	$html				= $_POST['html'];
	
	if($tpmod=="") $tpmod=$_GET['tpmod'];
	if($id1=="")   $id1=$_GET['id1'];
	if($id2=="")   $id2=$_GET['id2'];
	

	/*--> Controle de cohérance */
	if($vladd!=""||$vlmod!="") {
		$fgerr="";

		if($name==""||$date==""||$html=="") {
			$jsaction="alert('Vous devez renseigner l\'ensemble des informations');";
			$html=str_replace(chr(10),'',urldecode($html));
			$fgerr=1;
		}	
	}

	/*--> Rester sur le mode encours */
	if($vlmod!=""&&fgerr!="") $tpmod="MODIFY";
	if($vladd!=""&&fgerr!="") $tpmod="SUBMIT";

	/*--> Submit */
	if($vladd!=""&&$fgerr=="") {
		$flux_date=explode("/",$date);
		$dflux_date=$flux_date[2]."-".$flux_date[1]."-".$flux_date[0];
		$html=str_replace(chr(10),'',urldecode($html));
		
		$q="INSERT INTO env_fluxmsg(fluxmsg_name, fluxmsg_date, fluxmsg_html,fluxmsg_flux) VALUES('".addslashes($name)."','$dflux_date','$html','$id1')";
		$db1->query($q);
		$tpmod="";
		$jsaction="recharge()";
		
	}

	/*--> Modify */
	if($vlmod!=""&&$fgerr=="") {
		$flux_date=explode("/",$date);
		$dflux_date=$flux_date[2]."-".$flux_date[1]."-".$flux_date[0];
		$html=str_replace(chr(10),'',urldecode($html));
		
		$q="UPDATE env_fluxmsg SET fluxmsg_name='".addslashes($name)."', fluxmsg_date='$dflux_date', fluxmsg_html='$html' WHERE fluxmsg_id=$id2";
		$db1->query($q);
		$tpmod="";
		$jsaction="recharge()";
	}

	/*--> Delete */
	if($vlsup!="") {
		$q="DELETE FROM env_fluxmsg WHERE fluxmsg_id=$id2";
		$db1->query($q);
		$tpmod="";
		$jsaction="recharge()";
	}

	if($fgclo!="") {
		$jsaction="recharge()";
	}	

	echo "<script src='".$repository."lib/ckeditor/ckeditor.js'></script>";
	
	echo "<form id='formulaire' class='form-horizontal' role='form' method='post' enctype='multipart/form-data'>";
	
	echo "<input id='id1' name='id1' type='hidden' value='".$id1."'>";
	echo "<input id='id2' name='id2' type='hidden' value='".$id2."'>";
	echo "<input id='tpmod' name='tpmod' type='hidden' value=''>";


//-- SUBMIT --------------------------------------------------------------------------------------------------------------------------------

if($tpmod=="SUBMIT") {
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vladd' name='vladd' class='btn btn-primary' onClick='saveEditor();' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input id='fgclo' name='fgclo' class='btn btn-primary' type='submit' value='Annuler' />";
	echo "</div>";
	echo "</div>";	

	// Valeur par défaut
	$date	= date("d/m/Y");
}

//-- MODIFY --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="MODIFY") {

	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlmod' name='vlmod' class='btn btn-primary' onClick='saveEditor();' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input id='fgclo' name='fgclo' class='btn btn-primary' type='submit' value='Annuler' />";
	echo "</div>";
	echo "</div>";	

	// Valeurs
	$q = "SELECT * FROM env_fluxmsg WHERE fluxmsg_flux=$id1 AND fluxmsg_id=$id2";
	$db1->query($q);
	if($db1->next_record()) {
		$name 	= $db1->f("fluxmsg_name");
		$date 	= date("d/m/Y",strtotime($db1->f("fluxmsg_date")));
		$html	= $db1->f("fluxmsg_html");
	}
}

//-- DELETE --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="DELETE") {
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlsup' name='vlsup' class='btn btn-primary' type='submit' value='Confirmer la suppression' />";
	echo "&nbsp;";
	echo "<input id='fgclo' name='fgclo' class='btn btn-primary' type='submit' value='Annuler' />";    
	echo "</div>";
	echo "</div>";

	// Valeurs
	$q = "SELECT * FROM env_fluxmsg WHERE fluxmsg_flux=$id1 AND fluxmsg_id=$id2";
	$db1->query($q);
	if($db1->next_record()) {
		$name 	= $db1->f("fluxmsg_name");
		$date 	= date("d/m/Y",strtotime($db1->f("fluxmsg_date")));
		$html	= $db1->f("fluxmsg_html");
	}
}


//-- FORMULAIRE -----------------------------------------------------------------------------------------------------------------------------

if($tpmod=="SUBMIT"||$tpmod=="MODIFY"||$tpmod=="DELETE") {
?>



		<div class="form-group">
			<label for="id_bis" class="col-sm-3 control-label">ID Article*</label>
			<div class="col-sm-6"><input name="id_bis" id="id_bis" type="titre" class="form-control" disabled="disabled" placeholder="ID Article" value="<? echo $id2; ?>"></div>
		</div>
		
		<div class="form-group">
			<label for="name" class="col-sm-3 control-label">Nom*</label>
			<div class="col-sm-6"><input name="name" id="name" type="titre" class="form-control" placeholder="Nom" value="<? echo $name; ?>"></div>
		</div>

		<div class="form-group">
			<label for="date" class="col-sm-3 control-label">Date*</label>
			<div class="col-sm-6"><input name="date" id="date" type="titre" class="form-control" placeholder="Date" value="<? echo $date; ?>"></div>
		</div>

		<!-- This div will hold the editor. -->
		<div id="editor"></div>
		<input type='hidden' name='html' id='html'/>
		</div>	

<?
}

echo "</form>";

}
?>

<!-- FOOTER --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<?
	include($repository."footer.php");
?>

<!-- SCRIPT --------------------------------------------------------------------------------------------------------------------------------------- -->  	

<script type="text/javascript">
	
	$(document).ready(function() {
		$('#date').datepicker({ language: "fr-FR" });
		
		html = '<? echo str_replace(chr(10),'',$html); ?>';
		createEditor();		
	} );
	
	function recharge() {
		parent.document.getElementById('fgreload').value="MODIFY",
		window.parent.recharge();	
		window.parent.closemodal();	
	}

	function createEditor()
	{
		var heightbody = "370";
		
		CKEDITOR.config.toolbar_Basic =
		[
			{ name: 'document', items : ['Bold','Italic','Underline','-','JustifyLeft','JustifyCenter','JustifyRight','-', 'NumberedList','BulletedList','Blockquote' ] },
			{ name: 'insert', items : ['Image','Table','Smiley','Link'] },
			{ name: 'styles', items : [ 'Styles','Format','Font','FontSize'] }
		];

		// Valeur par défaut
		directory="local/upload";
		typeupload="image";
		multiupload=false;
		resize=true;
		size="400px";
		rename=true;
		prefix="file_";
		
		// Definition de l'url action
		action = "/eportail/lib/ckeditor/upload.php?directory="+directory+"&multiupload="+multiupload+"&resize="+resize+"&size="+size+"&rename="+rename+"&prefix="+prefix;

		
		CKEDITOR.config.filebrowserUploadUrl = action+"&typeupload=all";
		CKEDITOR.config.filebrowserImageUploadUrl = action+"&typeupload=image";
		
		CKEDITOR.config.toolbar = 'Basic';
		CKEDITOR.config.height = heightbody-120;
	
		// Create a new editor inside the <div id="editor">, setting its value to html
		var config = {};
		editor = CKEDITOR.appendTo( 'editor', config, html );
	}	
	
	function saveEditor()
	{
		document.getElementById( 'html' ).value = html = encodeURIComponent(editor.getData());
	}
	
	<?php echo $jsaction ?>

</script>







