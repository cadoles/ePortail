<?
	$repository="../";
	include($repository."include/include.php");
	include($repository."include/delete.php");
	include($repository."header.php"); 
		
	if($_SESSION['user_profil'] != 1 || !$config["consoleadmin"]) {
		echo "<br><br><br><center>Vous n'avez les permissions nécessaire</center>";
	}
	
	if($_SESSION['user_profil'] == 1 && $config["consoleadmin"]) {
		echo "<body>";
	
	$db1=new ps_db;
	$db2=new ps_db;
	
	$tpmod				=$_POST['tpmod'];
	$vlmod				=$_POST['vlmod'];
	$vladd				=$_POST['vladd'];
	$vlsup				=$_POST['vlsup'];
	
	$id					=$_POST['id'];
	$name				=$_POST['name'];
	$label				=$_POST['label'];
	$url				=$_POST['url'];
	$open				=$_POST['open'];
	$icon				=$_POST['icon'];
	$order				=$_POST['order'];
	$categorie			=$_POST['categorie'];
	$description		=$_POST['description'];
	
	/*--> Controle de cohérance */
	if($vladd!=""||$vlmod!="") {
		$fgerr="";

		if($name==""||$label==""||$url==""||$icon==""||$open=="") {
			$jsaction="alert('Vous devez renseigner l\'ensemble des informations');";
			$fgerr=1;
		}
		
		if($order<1) {
			$jsaction="alert('Le numéro d\'ordre doit être supérieur à 0');";
			$fgerr=1;
		}
		
		$q="SELECT * FROM env_application WHERE (application_name='$name')";
		if($vlmod!="") $q=$q." AND application_id!=$id";
		$db1->query($q);
		if($db1->next_record()){			
			$jsaction="alert('Une application avec ce nom existe déjà');";
			$fgerr=1;
		}

		$q="SELECT * FROM env_application WHERE application_categorie='$categorie' AND application_order='$order'";
		if($vlmod!="") $q=$q." AND application_id!=$id";
		$db1->query($q);
		if($db1->next_record()){			
			$jsaction="alert('Une application avec ce numéro d\'ordre et pour cette catégorie existe déjà');";
			$fgerr=1;
		}	
	}
	
	/*--> Rester sur le mode encours */
	if($vlmod!=""&&$fgerr!="") $tpmod="MODIFY";
	if($vladd!=""&&$fgerr!="") $tpmod="SUBMIT";
		
	/*--> Submit */
	if($vladd!=""&&$fgerr=="") {
		$q="INSERT INTO env_application(application_name, application_label, application_description, application_url, application_open, application_icon,application_order,application_categorie) VALUES('".addslashes($name)."','".addslashes($label)."','".addslashes($description)."','".urlencode($url)."','$open','$icon','$order','$categorie')";
		$db1->query($q);
		$tpmod="";
	}

	/*--> Modify */
	if($vlmod!=""&&$fgerr=="") {
		$q="UPDATE env_application SET application_name='".addslashes($name)."', application_label='".addslashes($label)."', application_description='".addslashes($description)."', application_url='".urlencode($url)."', application_open='$open', application_icon='$icon', application_order='$order', application_categorie='$categorie' WHERE application_id=$id";
		$db1->query($q);
		$tpmod="";
	}
	
	/*--> Delete */
	if($vlsup!="") {
		delApplication($id);
	}
	


	echo "<div id='wrapper'>";
	include("header.php");
	echo "<div id='page-wrapper'>";
	echo "<div class='container-fluid'>";
	echo "<form id='formulaire' class='form-horizontal' role='form' method='post' enctype='multipart/form-data'>";
	
	echo "<input id='id' name='id' type='hidden' value='".$id."'>";
	echo "<input id='tpmod' name='tpmod' type='hidden' value=''>";
	
  
//-- DISPLAY -------------------------------------------------------------------------------------------------------------------------------

if($tpmod=="") { 
    echo "<legend><h1>GESTION DES APPLICATIONS</h1></legend>";
    
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='fgadd' name='fgadd' class='btn btn-primary' onClick='$(\"#id\").val(\"\"); $(\"#tpmod\").val(\"SUBMIT\");' type='submit' value='Ajouter' />";
	echo "</div>";
	echo "</div>";

	echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
    echo "<thead>";
	echo "<th width='70px'>Action</th>";
	echo "<th width='70px'>Ordre</th>";
	echo "<th width='70px'>ID</th>";
	echo "<th width='70px'>Icône</th>";
	echo "<th width='150px'>Catégorie</th>";
	echo "<th>Nom</th>";
	
	echo "</thead>";
	
	$q="SELECT * FROM env_application, env_icon WHERE application_icon=icon_id";
	$db1->query($q);
	while($db1->next_record()){	
		echo "<tr>";
		
		echo "<td align='center'>";
		echo "<a class='glyphicon glyphicon-file' onClick='$(\"#id\").val(\"".$db1->f('application_id')."\"); $(\"#tpmod\").val(\"MODIFY\");$(\"#formulaire\").submit();'></a>";
		if($db1->f('application_id')>0) {
			echo "<a class='glyphicon glyphicon-remove' onClick='$(\"#id\").val(\"".$db1->f('application_id')."\"); $(\"#tpmod\").val(\"DELETE\");$(\"#formulaire\").submit();'></a>";
		}
		echo "</td>";

		echo "<td>";
		$q="SELECT * FROM env_application_categorie WHERE application_categorie_id=".$db1->f('application_categorie');
		$db2->query($q);
		if($db2->next_record()) {
			echo $db2->f("application_categorie_order")."-";
			$categorie=$db2->f("application_categorie_label");
		}
		else {
			echo "0-";
			$categorie="";
		}
		echo $db1->f("application_order");
		echo "</td>";
		
		echo "<td>";
		echo $db1->f('application_id');
		echo "</td>";
		
		echo "<td>";
		echo "<a href='".urldecode($db1->f('application_url'))."' target='_blank'><img src='".$repository."local/images/icon/".$db1->f('icon_url')."' width='40px' height='40px'></img></a>";
		echo "</td>";

		echo "<td>";
		echo $categorie;
		echo "</td>";

		echo "<td>";
		echo "<a href='".urldecode($db1->f('application_url'))."' target='_blank'>".$db1->f('application_label')."</a>";
		echo "</td>";
		
		echo "</tr>";
	} 
	
	echo "</table>";
}

//-- SUBMIT --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="SUBMIT") {
	// Entete du formulaire
    echo "<legend><h1>AJOUT APPLICATION</h1></legend>";
	
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vladd' name='vladd' class='btn btn-primary' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
	echo "</div>";
	echo "</div>";	

	// Valeur par défaut
	$order = 1;
	$categorie = 0;
	$open=0;
}

//-- MODIFY --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="MODIFY") {
	// Entete du formulaire
    echo "<legend><h1>MODIFICATION APPLICATION</h1></legend>";

	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlmod' name='vlmod' class='btn btn-primary' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
	echo "</div>";
	echo "</div>";	

	// Valeur par défaut
	$q = "SELECT * FROM env_application, env_icon WHERE application_id=$id AND application_icon=icon_id";
	$db1->query($q);
	if($db1->next_record()) {
		$name 			= $db1->f("application_name");
		$label 			= $db1->f("application_label");
		$description	= $db1->f("application_description");
		$url 			= $db1->f("application_url");
		$open 			= $db1->f("application_open");
		$urlicon 		= $db1->f("icon_url");
		$icon 			= $db1->f("application_icon");
		$order 			= $db1->f("application_order");
		$categorie		= $db1->f("application_categorie");
	}
}

//-- DELETE --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="DELETE") {
	echo "<legend><h1>SUPPRESSION APPLICATION</h1></legend>";
	
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlsup' name='vlsup' class='btn btn-primary' type='submit' value='Confirmer la suppression' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";    
	echo "</div>";
	echo "</div>";	  
	
	// Valeur par défaut
	$q = "SELECT * FROM env_application, env_appicon WHERE application_id=$id AND application_icon=icon_id";
	$db1->query($q);
	if($db1->next_record()) {	
		echo "<div id='myavatarprofil' style='float:left; margin-right:5px; margin-bottom:10px; border:none;'>";
		echo "<img id='imgapp' src='".$repository."local/images/icon/".$db1->f("icon_url")."' width='90px' height='90px'></img>";
		echo "</div>";
		
		echo $db1->f('application_label')."<br>";
		echo "<span id='avatar_description'>";
		echo "id : ".$db1->f('application_name')."<br>";
		echo "url : <a href='".$db1->f('application_url')."' target='_black'>".urldecode($db1->f('application_url'))."</a><br>";				
	}
}


//-- FORMULAIRE -----------------------------------------------------------------------------------------------------------------------------

if($tpmod=="SUBMIT"||$tpmod=="MODIFY") {
?>


	<fieldset class="row fieldset" style="clear:both">
		<legend>Description</legend>


		<div class="form-group">
			<label for="id_bis" class="col-sm-3 control-label">ID Application*</label>
			<div class="col-sm-6"><input name="id_bis" id="id_bis" type="titre" class="form-control" disabled="disabled" placeholder="ID Application" value="<? echo $id; ?>"></div>
		</div>

		<div class="form-group">
			<label for="name" class="col-sm-3 control-label">Nom*</label>
			<div class="col-sm-6"><input name="name" id="name" type="titre" class="form-control" placeholder="Nom" value="<? echo $name; ?>"></div>
		</div>

		<div class="form-group">
			<label for="label" class="col-sm-3 control-label">Libellé*</label>
			<div class="col-sm-6"><input name="label" id="label" type="titre" class="form-control" placeholder="Libellé" value="<? echo $label; ?>"></div>
		</div>		

		<div class="form-group">
			<label for="url" class="col-sm-3 control-label">Url*</label>
			<div class="col-sm-6"><input name="url" id="url" type="titre" class="form-control" placeholder="Url" value="<? echo urldecode($url); ?>"></div>
		</div>	

		<div class="form-group">
			<label for="open" class="col-sm-3 control-label">Mode d'ouverture*</label>
			<div class="col-sm-6">
				<select name="open" id="open" class="form-control">
					<option value='0' <? if($open==0) echo "selected"; ?>>Dans le portail</option>";
					<option value='1' <? if($open==1) echo "selected"; ?>>Nouvel onglet</option>";
				</select>
			</div>
		</div>
		
		<div class="form-group">
			<label for="order" class="col-sm-3 control-label">Ordre*</label>
			<div class="col-sm-6"><input name="order" id="order" type="number" class="form-control" placeholder="Ordre" value="<? echo $order; ?>"></div>
		</div>

		<div class="form-group">
			<label for="categorie" class="col-sm-3 control-label">Categorie*</label>
			<div class="col-sm-6">
				<select name="categorie" id="categorie" class="form-control">
				<?
					if($categorie==0) $lbsel="selected";
					echo "<option value='0' ".$lbsel.">Aucune</option>";
					
					$q="SELECT * FROM env_application_categorie";
					$db1->query($q);
					while($db1->next_record()){	
						$lbsel="";
						if($categorie==$db1->f("application_categorie_id")) {
							$lbsel="selected";
						}
						echo "<option value='".$db1->f("application_categorie_id")."' ".$lbsel.">".$db1->f("application_categorie_label")."</option>";
					}
				?>					
				</select>	
			</div>		
		</div>
		
		<div class="form-group">
			<label for="categorie" class="col-sm-3 control-label">Description</label>
			<div class="col-sm-6">			
				<textarea id="description" name="description" class="form-control" rows="4"><? echo $description; ?></textarea>
			</div>
		</div>
	</fieldset>

	<fieldset class="row fieldset" style="clear:both">
		<legend>Icône</legend>
		
		<input 	value="<? echo $icon; ?>" id="icon"	name="icon"	type="hidden"/>
		<?
		echo "<div style='width:140px; margin:auto; '>";
		echo "<div id='myavatarprofil' style='float:left; margin-right:5px; margin-bottom:10px; border:none;'>";
		echo "<img id='imgapp' src='".$repository."local/images/icon/".$urlicon."' width='90px' height='90px'></img>";
		echo "</div>";
		echo "<div style='float:left;'>";
		echo "<a class='glyphicon glyphicon-folder-close' data-toggle='modal' data-target='.bs-item-modal' title='Séléctionner un icône' onClick='changeFrame(\"selicone\")'></a>";
		echo "</div>";
		echo "</div>";
		?>

		<!-- Popup modification item !-->
		<div id="mymodal" class="modal fade bs-item-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		  <div class="modal-dialog modal-lg" style=" max-width:655px; width:80%;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">ICÔNE</h4>
				</div>
				<div class="modal-body">
					<iframe id="framemodal" frameborder=0 width="100%" height="520px"></iframe>
				</div>
			</div>
		  </div>
		</div>	
				
	</fieldset>

<?
}

echo "</form></div></div></div>";

}
?>

<!-- FOOTER --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<?
	include($repository."footer.php");
?>

<!-- SCRIPT --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<? if($tpmod=="") { ?>
<script>
	$(document).ready(function() {
		$('#datatable').dataTable( {
			"oLanguage": { "sUrl": "<?echo $repository; ?>lib/datatables/dataTables.txt" },
			"iDisplayLength": 50,
			"aaSorting": [[ 1, "asc" ]],
		} );
	} );	
	
	<?php echo $jsaction ?>
</script>
<? } ?>

<? if($tpmod=="SUBMIT"||$tpmod=="MODIFY"||$tpmod=="DELETE") { ?>
	<script type="text/javascript">
		$('#name').focus();
		
		function changeFrame(tpmod) {
			srcframe="icon.php?tpmod=SELECT&igico=imgapp&idico=icon";
			$("#framemodal").attr("src",srcframe);
		}
		
		function recharge() {
			$("#formulaire").submit();
		}
		
		function closemodal() {
			$('#mymodal').modal('hide');
		}
		
		<?php echo $jsaction ?>
		
	</script>
<? } ?>







	
	
