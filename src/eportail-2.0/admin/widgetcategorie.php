<?
	$repository="../";
	include($repository."include/include.php");
	include($repository."include/delete.php");
	include($repository."header.php"); 
		
	if($_SESSION['user_profil'] != 1 || !$config["consoleadmin"]) {
		echo "<br><br><br><center>Vous n'avez les permissions nécessaire</center>";
	}
	
	if($_SESSION['user_profil'] == 1 && $config["consoleadmin"]) {
		echo "<body>";
	
	
	$db1=new ps_db;
	$db2=new ps_db;
	
	$tpmod				=$_POST['tpmod'];
	$vlmod				=$_POST['vlmod'];
	$vladd				=$_POST['vladd'];
	$vlsup				=$_POST['vlsup'];
	
	$id					=$_POST['id'];
	$label				=$_POST['label'];

		
	/*--> Controle de cohérance */
	if($vladd!=""||$vlmod!="") {
		$fgerr="";


		if($label=="") {
			$jsaction="alert('Les champs avec * sont obligatoires');";
			$fgerr=1;
		}

		$q="SELECT * FROM env_widget_categorie WHERE widget_categorie_label='$label'";
		if($vlmod!="") $q=$q." AND widget_categorie_id!=$id";
		$db1->query($q);
		if($db1->next_record()){			
			$jsaction="alert('Une catégorie avec ce label existe déjà');";
			$fgerr=1;
		}
		
		$q="SELECT * FROM env_widget_categorie WHERE widget_categorie_order='$order'";
		if($vlmod!="") $q=$q." AND widget_categorie_id!=$id";
		$db1->query($q);
		if($db1->next_record()){			
			$jsaction="alert('Une catégorie avec ce numéro d\'ordre existe déjà');";
			$fgerr=1;
		}		
	}
	
	/*--> Rester sur le mode encours */
	if($vlmod!=""&&fgerr!="") $tpmod="MODIFY";
	if($vladd!=""&&fgerr!="") $tpmod="SUBMIT";
		
	/*--> Submit */
	if($vladd!=""&&$fgerr=="") {
		$q="INSERT INTO env_widget_categorie(widget_categorie_label) VALUES('".addslashes($label)."')";
		$db1->query($q);
		$tpmod="";
	}

	/*--> Modify */
	if($vlmod!=""&&$fgerr=="") {
		$q="UPDATE env_widget_categorie SET widget_categorie_label='".addslashes($label)."' WHERE widget_categorie_id=$id";
		$db1->query($q);
		$tpmod="";
	}
	
	/*--> Delete */
	if($vlsup!="") {
		$q="DELETE FROM env_widget_categorie WHERE widget_categorie_id=$id";
		$db1->query($q);
		$tpmod="";
	}

	echo "<div id='wrapper'>";
	include("header.php");
	echo "<div id='page-wrapper'>";
	echo "<div class='container-fluid'>";
	echo "<form id='formulaire' class='form-horizontal' role='form' method='post' enctype='multipart/form-data'>";
	
	
	echo "<input id='id' name='id' type='hidden' value='".$id."'>";
	echo "<input id='tpmod' name='tpmod' type='hidden' value=''>";
	
  
//-- DISPLAY -------------------------------------------------------------------------------------------------------------------------------

if($tpmod=="") { 
    echo "<legend><h1>GESTION DES CATEGORIES DE WIDGET</h1></legend>";
	
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='fgadd' name='fgadd' class='btn btn-primary' onClick='$(\"#id\").val(\"\"); $(\"#tpmod\").val(\"SUBMIT\");' type='submit' value='Ajouter' />";
	echo "</div>";
	echo "</div>";
    
	echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
    echo "<thead>";
	echo "<th width='70px'>Action</th>";
	echo "<th width='70px'>ID</th>";
	echo "<th>Nom</th>";
	echo "</thead>";
	
	$q="SELECT * FROM env_widget_categorie";
	$db1->query($q);
	while($db1->next_record()){	
		echo "<tr>";

		echo "<td align='center'>";
		echo "<a class='glyphicon glyphicon-file' onClick='$(\"#id\").val(\"".$db1->f('widget_categorie_id')."\"); $(\"#tpmod\").val(\"MODIFY\");$(\"#formulaire\").submit();'></a>";
		// Pas de suppression possible si catégorie liée à une application
		$q="SELECT widget_id FROM env_widget WHERE widget_categorie=".$db1->f('widget_categorie_id');
		$db2->query($q);
		if(!$db2->next_record()) {
			echo "<a class='glyphicon glyphicon-remove' onClick='$(\"#id\").val(\"".$db1->f('widget_categorie_id')."\"); $(\"#tpmod\").val(\"DELETE\");$(\"#formulaire\").submit();'></a>";
		}		
		echo "</td>";

		echo "<td>";
		echo $db1->f('widget_categorie_id');
		echo "</td>";
		
		echo "<td>";
		echo $db1->f('widget_categorie_label')."<br>";
		echo "</td>";

		echo "</tr>";
	} 
	echo "</table>";
}

//-- SUBMIT --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="SUBMIT") {
	// Entete du formulaire
    echo "<legend><h1>AJOUT CATEGORIE DE WIDGET</h1></legend>";

	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vladd' name='vladd' class='btn btn-primary' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
	echo "</div>";
	echo "</div>";		

	// Valeur par défaut
	$order = 0;
}

//-- MODIFY --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="MODIFY") {
	// Entete du formulaire
    echo "<legend><h1>MODIFICATION CATEGORIE DE WIDGET</h1></legend>";

	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlmod' name='vlmod' class='btn btn-primary' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
	echo "</div>";
	echo "</div>";		

	// Valeurs
	$q = "SELECT * FROM env_widget_categorie WHERE widget_categorie_id=$id";
	$db1->query($q);
	if($db1->next_record()) {
		$label 	= $db1->f("widget_categorie_label");
	}
	
	if($order=="") $order=0;
}

//-- DELETE --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="DELETE") {
	echo "<legend><h1>SUPPRESSION CATEGORIE DE WIDGET</h1></legend>";
	
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlsup' name='vlsup' class='btn btn-primary' type='submit' value='Confirmer la suppression' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";    
	echo "</div>";
	echo "</div>";	
	
	// Valeurs
	$q = "SELECT * FROM env_widget_categorie WHERE widget_categorie_id=$id";
	$db1->query($q);
	if($db1->next_record()) {
		$label 	= $db1->f("widget_categorie_label");
	}
}


//-- FORMULAIRE -----------------------------------------------------------------------------------------------------------------------------

if($tpmod=="SUBMIT"||$tpmod=="MODIFY"||$tpmod=="DELETE") {
?>

	<fieldset class="row fieldset" style="clear:both">
		<div class="form-group">
			<label for="id_bis" class="col-sm-3 control-label">ID*</label>
			<div class="col-sm-6"><input name="id_bis" id="id_bis" type="titre" class="form-control" disabled="disabled" placeholder="ID" value="<? echo $id; ?>"></div>
		</div>


		<div class="form-group">
			<label for="label" class="col-sm-3 control-label">Nom*</label>
			<div class="col-sm-6"><input name="label" id="label" type="titre" class="form-control" placeholder="Nom" value="<? echo $label; ?>"></div>
		</div>
	</fieldset>

<?
}

echo "</form></div></div></div>";

}
?>

<!-- FOOTER --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<?
	include($repository."footer.php");
?>

<? if($tpmod=="") { ?>
	<script>
		$(document).ready(function() {
			$('#datatable').dataTable( {
				"oLanguage": { "sUrl": "<?echo $repository; ?>lib/datatables/dataTables.txt" },
				"iDisplayLength": 50,
				"aaSorting": [[ 1, "asc" ]],
			} );
		} );	
	</script>
<? } ?>

<? if($tpmod=="SUBMIT"||$tpmod=="MODIFY"||$tpmod=="DELETE") { ?>
	<script>
			$('#label').focus();
	</script>
<? } ?>

<script>
	<?php echo $jsaction ?>
</script>






	
	
