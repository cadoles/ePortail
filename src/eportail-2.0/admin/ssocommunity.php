<?
	$repository="../";
	include($repository."include/include.php");
	include($repository."include/delete.php");
	include($repository."header.php"); 

	if($_SESSION['user_profil'] != 1 || !$config["consoleadmin"]) {
		echo "<br><br><br><center>Vous n'avez les permissions nécessaire</center>";
	}
	
	if($_SESSION['user_profil'] == 1 && $config["consoleadmin"]) {
		echo "<body>";
		
	$db1=new ps_db;
	$db2=new ps_db;
	
	$tpmod				=$_POST['tpmod'];
	$vlmod				=$_POST['vlmod'];
	$vladd				=$_POST['vladd'];
	$vlsup				=$_POST['vlsup'];
	$fgreload			=$_POST['fgreload'];
		
	$id					=$_POST['id'];
	$label				=$_POST['label'];
	
	/*--> Controle de cohérance */
	if($vladd!=""||$vlmod!="") {
		$fgerr="";
		if($label=="") {
			$jsaction="alert('Vous devez renseigner l\'ensemble des informations');";
			$fgerr=1;
		}
	}
	
	/*--> Rester sur le mode encours */
	if($vlmod!=""&&fgerr!="") $tpmod="MODIFY";
	if($vladd!=""&&fgerr!="") $tpmod="SUBMIT";
		
	/*--> Submit */
	if($vladd!=""&&$fgerr=="") {
		$q="INSERT INTO env_sso_community(sso_community_label) VALUES('".addslashes($label)."')";
		$db1->query($q);
		$id=mysql_insert_id();
		$tpmod="MODIFY";
	}

	/*--> Modify */
	if($vlmod!=""&&$fgerr=="") {
		$q="UPDATE env_sso_community SET sso_community_label='".addslashes($label)."' WHERE sso_community_id=$id";
		$db1->query($q);
		$tpmod="";
	}

	/*--> Delete */
	if($vlsup!="") {
		delSsoCommunity($id);
	}
	
	/*--> Reload */	
	if($fgreload!="") {
		$tpmod=$fgreload; 
		$vladd="";
		$vlmod="";
		$vldel="";
	}

	echo "<div id='wrapper'>";
	include("header.php");
	echo "<div id='page-wrapper'>";
	echo "<div class='container-fluid'>";
	echo "<form id='formulaire' class='form-horizontal' role='form' method='post' enctype='multipart/form-data'>";
	
	echo "<input id='id' name='id' type='hidden' value='".$id."'>";
	echo "<input id='tpmod' name='tpmod' type='hidden' value=''>";
	echo "<input id='fgreload' name='fgreload' type='hidden' value=''>";
  
//-- DISPLAY -------------------------------------------------------------------------------------------------------------------------------

if($tpmod=="") { 
    echo "<legend><h1>GESTION DES COMMUNAUTES SSO</h1></legend>";
    
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='fgadd' name='fgadd' class='btn btn-primary' onClick='$(\"#id\").val(\"\"); $(\"#tpmod\").val(\"SUBMIT\");' type='submit' value='Ajouter' />";
	echo "</div>";
	echo "</div>";
		
	$q="SELECT * FROM env_sso_community ORDER BY sso_community_id";
	$db1->query($q);

	echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
    echo "<thead>";
	echo "<th width='70px'>Action</th>";
	echo "<th >Libellé</th>";
	echo "<th >Attribut</th>";
	echo "</thead>";
	
	while($db1->next_record()){	
		echo "<tr>";

		echo "<td align='center'>";
		if($db1->f('sso_community_id')>0) {
			echo "<a class='glyphicon glyphicon-file' onClick='$(\"#id\").val(\"".$db1->f('sso_community_id')."\"); $(\"#tpmod\").val(\"MODIFY\");$(\"#formulaire\").submit();'></a>";
			echo "<a class='glyphicon glyphicon-remove' onClick='$(\"#id\").val(\"".$db1->f('sso_community_id')."\"); $(\"#tpmod\").val(\"DELETE\");$(\"#formulaire\").submit();'></a>";
		}
		echo "</td>";

		echo "<td>";
		echo $db1->f('sso_community_label');
		echo "</td>";

		echo "<td>";
		$q="SELECT * FROM env_sso_attribut, env_sso_community_attribut WHERE sso_community_attribut_community=".$db1->f("sso_community_id")." AND sso_attribut_id=sso_community_attribut_attribut";
		$db2->query($q);
		$i=0;
		while($db2->next_record()) {
			if($i>0) echo "<br>";
			echo $db2->f("sso_attribut_name")." = ".$db2->f("sso_attribut_value");
			$i=$i+1;
		}
		echo "</td>";
		
		echo "</tr>";
	} 

	echo "</table>";
}

//-- SUBMIT --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="SUBMIT") {
	// Entete du formulaire
    echo "<legend><h1>AJOUT COMMUNAUTE SSO</h1></legend>";
    
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vladd' name='vladd' class='btn btn-primary' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
	echo "</div>";
	echo "</div>";	

	// Valeur par défaut
	// aucune
}

//-- MODIFY --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="MODIFY") {
	// Entete du formulaire
    echo "<legend><h1>MODIFICATION COMMUNAUTE SSO</h1></legend>";
   
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlmod' name='vlmod' class='btn btn-primary' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
	echo "</div>";
	echo "</div>";	

	// Valeur par défaut
	$q = "SELECT * FROM env_sso_community WHERE sso_community_id=$id";
	$db1->query($q);
	if($db1->next_record()) {
		$label 		= $db1->f('sso_community_label');
	}
}

//-- DELETE --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="DELETE") {
	echo "<legend><h1>SUPPRESSION COMMUNAUTE SSO</h1></legend>";
	
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlsup' name='vlsup' class='btn btn-primary' type='submit' value='Confirmer la suppression' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";    
	echo "</div>";
	echo "</div>";
	
	// Valeur par défaut
	$q = "SELECT * FROM env_sso_community WHERE sso_community_id=$id";
	$db1->query($q);
	if($db1->next_record()) {
		$label 		= $db1->f('sso_community_label');
	}
}


//-- FORMULAIRE -----------------------------------------------------------------------------------------------------------------------------

if($tpmod=="SUBMIT"||$tpmod=="MODIFY"||$tpmod=="DELETE") {
?>
	<fieldset class="row fieldset" style="clear:both">
		<legend>Description</legend>

		<div class="form-group">
			<label for="id_bis" class="col-sm-3 control-label">ID*</label>
			<div class="col-sm-6"><input name="id_bis" id="id_bis" type="titre" class="form-control" disabled="disabled" placeholder="ID Application" value="<? echo $id; ?>"></div>
		</div>
		

		<div class="form-group">
			<label for="label" class="col-sm-3 control-label">Libellé*</label>
			<div class="col-sm-6"><input name="label" id="label" type="titre" class="form-control" placeholder="Libellé" value="<? echo $label; ?>"></div>
		</div>
		
		<?
			if($tpmod=="MODIFY") {
				echo "<legend>Attributs</legend>";
				echo "<div class='form-group'>";
				echo "<div class='col-sm-12'>";
				echo "<a class='btn btn-primary' data-toggle='modal' data-target='.bs-item-modal' title='Séléctionner un Attribut' onClick='changeFrame(\"submit\",$id,0)'>Ajouter</a>";
				echo "</div>";
				echo "</div>";

				$q="SELECT * FROM env_sso_attribut, env_sso_community_attribut WHERE sso_community_attribut_community=$id AND sso_attribut_id=sso_community_attribut_attribut";
				$db1->query($q);

				echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
				echo "<thead>";
				echo "<th width='70px'>Action</th>";
				echo "<th>Nom</th>";
				echo "<th>Valeur</th>";
				echo "</thead>";
				
				while($db1->next_record()){
					echo "<tr>";
					
					echo "<td>";
					echo "<a class='glyphicon glyphicon-remove' data-toggle='modal' data-target='.bs-item-modal' onClick='changeFrame(\"delete\",$id,".$db1->f("sso_attribut_id").")' title='Supprimer un attribut' />";
					echo "</td>";
								
					echo "<td>";
					echo $db1->f("sso_attribut_name");
					echo "</td>";
				
					echo "<td>";
					echo $db1->f("sso_attribut_value");
					echo "</td>";					
					
					echo "</tr>";
				}
				
				echo "</table>";
			}
			elseif($tpmod=="SUBMIT") {
				echo "<div class='form-group'><center><br>Veuillez valider avant de pouvoir ajouter un attribut</center></div>";
			}
		?>	
	</fieldset>
	
	<!-- Popup modification attribut !-->
	<div id="mymodal" class="modal fade bs-item-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-lg" style=" max-width:655px; width:80%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">ATTRIBUTS</h4>
			</div>
			<div class="modal-body">
				<iframe id="framemodal" frameborder=0 width="100%" height="520px"></iframe>
			</div>
		</div>
	  </div>
	</div>			
<?
}

echo "</form></div></div></div>";

}

?>

<!-- FOOTER --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<?
	include($repository."footer.php");
?>

<!-- SCRIPT --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<? if($tpmod==""||$tpmod=="MODIFY") { ?>
	<script>
		$(document).ready(function() {
			$('#datatable').dataTable( {
				"oLanguage": { "sUrl": "<?echo $repository; ?>lib/datatables/dataTables.txt" },
				"iDisplayLength": 50,
				"aaSorting": [[ 1, "asc" ]]
			} );
		} );	
		
		<?php echo $jsaction ?>
	</script>
<? } ?>

<? if($tpmod=="SUBMIT"||$tpmod=="MODIFY"||$tpmod=="DELETE") { ?>
	<script type="text/javascript">
		$('#label').focus();

		function changeFrame(mode,id1,id2) {
			if(mode=="submit") {
				srcframe="lien.php?tpmod=&tptbl=SSOCOMMUNITY&id1="+id1;
			}
			else {
				srcframe="lien.php?tpmod=DELETE&tptbl=SSOCOMMUNITY&id1="+id1+"&id2="+id2;
			}
			
			$("#framemodal").attr("src",srcframe);
		}
		
		function recharge() {
			$("#formulaire").submit();
		}
		
		function closemodal() {
			$('#mymodal').modal('hide');
		}
		
		<?php echo $jsaction ?>
		
	</script>
<? } ?>





	
	
