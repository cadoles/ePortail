<?
	$repository="../";
	include($repository."include/include.php");
	include($repository."include/delete.php");
	include($repository."header.php"); 
		
	if($_SESSION['user_profil'] != 1 || !$config["consoleadmin"]) {
		echo "<br><br><br><center>Vous n'avez les permissions nécessaire</center>";
	}
	
	if($_SESSION['user_profil'] == 1 && $config["consoleadmin"]) {
		echo "<body>";
	
	$db1=new ps_db;
	$db2=new ps_db;
	

	$id					=$_POST['id'];
	$tpmod				=$_POST['tpmod'];

	$fgreload			=$_POST['fgreload'];
	$vlmod				=$_POST['vlmod'];
	$vladd				=$_POST['vladd'];
	$vlsup				=$_POST['vlsup'];
	
	$label				=$_POST['label'];
	$idproprio			=$_POST['idproprio'];
	
	/*--> Controle de cohérance */
	if($vladd!=""||$vlmod!="") {
		$fgerr="";
	}
	
	/*--> Rester sur le mode encours */
	if($vlmod!=""&&fgerr!="") $tpmod="MODIFY";
	if($vladd!=""&&fgerr!="") $tpmod="SUBMIT";
	if($fgreload!="") {
		$tpmod=$fgreload; 
		$vladd="";
		$vlmod="";
		$vldel="";
	}
		
	/*--> Submit */
	if($vladd!=""&&$fgerr=="") {
		$q="INSERT INTO env_panel(panel_label, panel_user, panel_url, panel_type) VALUES('$label',$idproprio,'editeur.php',3)";
		$db1->query($q);
		$tpmod="";
	}

	/*--> Modify */
	if($vlmod!=""&&$fgerr=="") {
		$q="UPDATE env_panel SET panel_label='$label', panel_user=$idproprio WHERE panel_id=$id";
		$db1->query($q);
		$tpmod="";
	}
	
	echo "<div id='wrapper'>";
	include("header.php");
	echo "<div id='page-wrapper'>";
	echo "<div class='container-fluid'>";
	echo "<form id='formulaire' class='form-horizontal' role='form' method='post' enctype='multipart/form-data'>";
	
	echo "<input id='id' name='id' type='hidden' value='".$id."'>";
	echo "<input id='tpmod' name='tpmod' type='hidden' value=''>";
	echo "<input id='fgreload' name='fgreload' type='hidden' value=''>";
	
  
//-- DISPLAY -------------------------------------------------------------------------------------------------------------------------------

if($tpmod=="") { 
    echo "<legend><h1>GESTION DES PAGES PAR PROFIL SSO</h1></legend>";
    
	echo "<em>La première page d'un profil sera concidérée comme étant sa page d'accueil et prendra donc le nom de votre site.</em><br>";
	echo "<em>Si un utilisateur posséde plusieurs pages d'accueil cela sera la première dans l'ordre alphabétique qui sera concidérée comme telle.</em><br><br>";
	
	$q="SELECT * FROM env_sso_profil, env_sso_community WHERE sso_profil_community=sso_community_id";
	$db1->query($q);

	echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
    echo "<thead>";
	echo "<th width='40px'>Action</th>";
	echo "<th>Communauté</th>";
	echo "<th>Profil</th>";
	echo "<th>Pages</th>";
	echo "</thead>";
	
	while($db1->next_record()){	
		echo "<tr>";

		echo "<td align='center'>";
		echo "<a class='glyphicon glyphicon-file' onClick='$(\"#id\").val(\"".$db1->f('sso_profil_id')."\"); $(\"#tpmod\").val(\"MODIFY\");$(\"#formulaire\").submit();'></a>";
		echo "</td>";

		echo "<td>";
		echo $db1->f('sso_community_label');
		echo "</td>";
		
		echo "<td>";
		echo $db1->f('sso_profil_label');
		echo "</td>";

		echo "<td>";
		$q="SELECT * FROM env_panel_sso_profil, env_panel WHERE panel_sso_profil_sso_profil=".$db1->f("sso_profil_id")." AND panel_sso_profil_panel=panel_id ORDER BY panel_order";
		$db2->query($q);
		while($db2->next_record()){	
			if($db2->f("panel_type")==1) 			echo "<a href='".$repository.urldecode($db2->f("panel_url"))."' target='_blank'>";
			elseif($db2->f("panel_type")==2) 		echo "<a href='".$repository."application.php?mode=alone' target='_blank'>";
			elseif($db2->f("panel_type")==3) 		echo "<a href='".$repository.urldecode($db2->f("panel_url"))."?id=".$db2->f("panel_id")."' target='_blank'>";
			else  									echo "<a href='".$repository.urldecode($db2->f("panel_url"))."?id=".$db2->f("panel_id")."' target='_blank'>";
			echo $db2->f("panel_label")."</a><br>";
		}
		echo "</td>";

		echo "</tr>";
	} 

	echo "</table>";
}

//-- SUBMIT --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="SUBMIT") {

}

//-- MODIFY --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="MODIFY") {
	// Entete du formulaire
    echo "<legend><h1>MODIFICATION PAGE PAR PROFIL SSO</h1></legend>";

	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlmod' name='vlmod' class='btn btn-primary' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
	echo "</div>";
	echo "</div>";	
	    

	// Valeur par défaut
	$q = "SELECT * FROM env_sso_profil WHERE sso_profil_id=$id";
	$db1->query($q);
	if($db1->next_record()) {
		$label 		= $db1->f('sso_profil_label');
	}
}

//-- DELETE --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="DELETE") {

}


//-- FORMULAIRE -----------------------------------------------------------------------------------------------------------------------------

if($tpmod=="SUBMIT"||$tpmod=="MODIFY") {
?>

	<fieldset class="row fieldset" style="clear:both">
		<legend>Profil</legend>
		
		<div class="form-group">
			<label for="id_bis" class="col-sm-3 control-label">ID SSO Profil*</label>
			<div class="col-sm-6"><input name="id_bis" id="id_bis" type="titre" class="form-control" disabled="disabled" placeholder="ID Profil" value="<? echo $id; ?>"></div>
		</div>
		
		<div class="form-group">
			<label for="label" class="col-sm-3 control-label">Nom*</label>
			<div class="col-sm-6"><input name="label" id="label" type="titre" class="form-control" disabled="disabled" placeholder="Nom" value="<? echo $label; ?>"></div>
		</div>
	</fieldset>
	
	<fieldset class="row fieldset" style="clear:both">
		<legend>Liste des Pages</legend>
		<?
		echo "<div class='form-group'>";
		echo "<div class='col-sm-12'>";
		echo "<a class='btn btn-primary' data-toggle='modal' data-target='.bs-item-modal' title='Séléctionner une application' onClick='changeFrame(\"submit\",$id,0)'>Ajouter</a>";
		echo "</div>";
		echo "</div>";	

		echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
		echo "<thead>";
		echo "<th width='40px'>Action</th>";
		echo "<th>Ordre</th>";
		echo "<th>Page</th>";
		echo "<th>Type</th>";
		echo "<th>Propriétaire</th>";
		echo "</thead>";

		$q="SELECT * FROM env_panel_sso_profil, env_panel, env_panel_type, env_user, env_profil WHERE panel_sso_profil_sso_profil=".$id." AND panel_sso_profil_panel=panel_id AND panel_type = panel_type_id AND panel_user=user_id AND user_profil=profil_id ORDER BY panel_order";
		$db2->query($q);
		while($db2->next_record()){	
			echo "<tr>";

			echo "<td>";
			echo "<a class='glyphicon glyphicon-remove' data-toggle='modal' data-target='.bs-item-modal' onClick='changeFrame(\"delete\",$id,".$db2->f("panel_id").")' title='Supprimer une page' />";
			echo "</td>";
			
			echo "<td>".$db2->f("panel_order")."</td>";
			echo "<td>".$db2->f("panel_label")."</td>";
			echo "<td>".$db2->f("panel_type_name")."</td>";

			echo "<td>";
			if($db2->f('user_avatar')=="") 
				echo "<img src='".$repository."style/images/blank.gif' class='myavatarvide' width='45px' height='45px' style='float:left;margin-right:5px;'></img>";
			else
				echo "<img src='".$repository."local/images/avatar/".$db2->f('user_avatar')."' width='45px' height='45px' style='float:left;margin-right:5px;'></img>";
			
			echo "<span style='float:left; font-size:80%;'>";
			echo "Login : ".$db2->f('user_login')."<br>";
			echo "Pseudo : ".$db2->f('user_pseudo')."<br>";
			echo "Profil : ".$db2->f('sso_profil_label')."<br>";		
			echo "</span>";
			echo "</td>";
					
			echo "</tr>";
		}		
		
		echo "</table>";
		?>
		<!-- Popup modification item !-->
		<div id="mymodal" class="modal fade bs-item-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		  <div class="modal-dialog modal-lg" style=" max-width:655px; width:80%;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">PAGES</h4>
				</div>
				<div class="modal-body">
					<iframe id="framemodal" frameborder=0 width="100%" height="520px"></iframe>
				</div>
			</div>
		  </div>
		</div>			
	</fieldset>
	<?
}

echo "</form></div></div></div>";

}

?>

<!-- FOOTER --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<?
	include($repository."footer.php");
?>

<!-- SCRIPT --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<? if($tpmod=="") { ?>
	<script>
		$(document).ready(function() {
			$('#datatable').dataTable( {
				"oLanguage": { "sUrl": "<?echo $repository; ?>lib/datatables/dataTables.txt" },
				"iDisplayLength": 50,
				"aoColumns": [{ "bSortable": false },null,{ "bSortable": false }],
				"aaSorting": [[ 1, "asc" ]]
			} );
		} );	
		
		<?php echo $jsaction ?>
	</script>
<? } ?>

<? if($tpmod=="SUBMIT"||$tpmod=="MODIFY"||$tpmod=="DELETE") { ?>
	<script type="text/javascript">
		$('#name').focus();

		$(document).ready(function() {
			$('#datatable').dataTable( {
				"oLanguage": { "sUrl": "<?echo $repository; ?>lib/datatables/dataTables.txt" },
				"iDisplayLength": 50,
				"aoColumns": [{ "bSortable": false },null,null,null,null],
				"aaSorting": [[ 1, "asc" ]]
			} );
		} );	
				
		function changeFrame(mode,id1,id2) {
			if(mode=="submit") {
				srcframe="lien.php?tpmod=&tptbl=PAGESSO&id1="+id1;
			}
			else {
				srcframe="lien.php?tpmod=DELETE&tptbl=PAGESSO&id1="+id1+"&id2="+id2;
			}
			
			$("#framemodal").attr("src",srcframe);
		}
		
		function recharge() {
			$("#formulaire").submit();
		}
		
		function closemodal() {
			$('#mymodal').modal('hide');
		}
		
		<?php echo $jsaction ?>
		
	</script>
<? } ?>







	
	
