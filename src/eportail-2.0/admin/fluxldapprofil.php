<?
	$repository="../";
	include($repository."include/include.php");
	include($repository."include/delete.php");
	include($repository."header.php"); 
		
	if($_SESSION['user_profil'] != 1 || !$config["consoleadmin"]) {
		echo "<br><br><br><center>Vous n'avez les permissions nécessaire</center>";
	}
	
	if($_SESSION['user_profil'] == 1 && $config["consoleadmin"]) {
		echo "<body>";
	
	$db1=new ps_db;
	$db2=new ps_db;
	

	$id					=$_POST['id'];
	$tpmod				=$_POST['tpmod'];

	$fgreload			=$_POST['fgreload'];
	$vlmod				=$_POST['vlmod'];
	$vladd				=$_POST['vladd'];
	$vlsup				=$_POST['vlsup'];
	
	$label				=$_POST['label'];
	$idproprio			=$_POST['idproprio'];
	
	/*--> Controle de cohérance */
	if($vladd!=""||$vlmod!="") {
		$fgerr="";
	}
	
	/*--> Rester sur le mode encours */
	if($vlmod!=""&&fgerr!="") $tpmod="MODIFY";
	if($vladd!=""&&fgerr!="") $tpmod="SUBMIT";
	if($fgreload!="") {
		$tpmod=$fgreload; 
		$vladd="";
		$vlmod="";
		$vldel="";
	}

	/*--> Modify */
	if($vlmod!=""&&$fgerr=="") {
		$tpmod="";
	}		

	
	echo "<div id='wrapper'>";
	include("header.php");
	echo "<div id='page-wrapper'>";
	echo "<div class='container-fluid'>";
	echo "<form id='formulaire' class='form-horizontal' role='form' method='post' enctype='multipart/form-data'>";
	
	echo "<input id='id' name='id' type='hidden' value='".$id."'>";
	echo "<input id='tpmod' name='tpmod' type='hidden' value=''>";
	echo "<input id='fgreload' name='fgreload' type='hidden' value=''>";
	
  
//-- DISPLAY -------------------------------------------------------------------------------------------------------------------------------

if($tpmod=="") { 
    echo "<legend><h1>GESTION DES FLUX PAR PROFIL LDAP</h1></legend>";
    
	$q="SELECT * FROM env_ldap_profil, env_ldap_community WHERE ldap_profil_ldap_community=ldap_community_id";
	$db1->query($q);

	echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
    echo "<thead>";
	echo "<th width='40px'>Action</th>";
	echo "<th>Communauté</th>";
	echo "<th>Profil</th>";
	echo "<th>Flux</th>";
	echo "</thead>";
	
	while($db1->next_record()){	
		echo "<tr>";

		echo "<td align='center'>";
		echo "<a class='glyphicon glyphicon-file' onClick='$(\"#id\").val(\"".$db1->f('ldap_profil_id')."\"); $(\"#tpmod\").val(\"MODIFY\");$(\"#formulaire\").submit();'></a>";
		echo "</td>";

		echo "<td>";
		echo $db1->f('ldap_community_label');
		echo "</td>";
		
		echo "<td>";
		echo $db1->f('ldap_profil_label');
		echo "</td>";

		echo "<td>";
		$q="SELECT * FROM env_flux_ldap_profil, env_flux WHERE flux_ldap_profil_profil=".$db1->f("ldap_profil_id")." AND flux_ldap_profil_flux=flux_id ORDER BY flux_order";
		$db2->query($q);
		while($db2->next_record()){	
			echo $db2->f("flux_name")."<br>";
		}
		echo "</td>";

		echo "</tr>";
	} 

	echo "</table>";
}

//-- MODIFY --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="MODIFY") {
	// Entete du formulaire
    echo "<legend><h1>MODIFICATION PAGE PAR PROFIL LDAP</h1></legend>";

	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlmod' name='vlmod' class='btn btn-primary' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
	echo "</div>";
	echo "</div>";	
	    

	// Valeur par défaut
	$q = "SELECT * FROM env_ldap_profil WHERE ldap_profil_id=$id";
	$db1->query($q);
	if($db1->next_record()) {
		$label 		= $db1->f('ldap_profil_label');
	}
}

//-- DELETE --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="DELETE") {

}


//-- FORMULAIRE -----------------------------------------------------------------------------------------------------------------------------

if($tpmod=="SUBMIT"||$tpmod=="MODIFY") {
?>

	<fieldset class="row fieldset" style="clear:both">
		<legend>Profil</legend>
		
		<div class="form-group">
			<label for="id_bis" class="col-sm-3 control-label">ID Profil*</label>
			<div class="col-sm-6"><input name="id_bis" id="id_bis" type="titre" class="form-control" disabled="disabled" placeholder="ID Application" value="<? echo $id; ?>"></div>
		</div>
		
		<div class="form-group">
			<label for="label" class="col-sm-3 control-label">Nom*</label>
			<div class="col-sm-6"><input name="label" id="label" type="titre" class="form-control" disabled="disabled" placeholder="Nom" value="<? echo $label; ?>"></div>
		</div>
	</fieldset>
	
	<fieldset class="row fieldset" style="clear:both">
		<legend>Liste des Flux</legend>
		<?
		echo "<div class='form-group'>";
		echo "<div class='col-sm-12'>";
		echo "<a class='btn btn-primary' data-toggle='modal' data-target='.bs-item-modal' title='Séléctionner une application' onClick='changeFrame(\"submit\",$id,0)'>Ajouter</a>";
		echo "</div>";
		echo "</div>";	

		echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
		echo "<thead>";
		echo "<th width='40px'>Action</th>";
		echo "<th>Ordre</th>";
		echo "<th>Flux</th>";
		echo "</thead>";

		$q="SELECT * FROM env_flux_ldap_profil, env_flux WHERE flux_ldap_profil_profil=".$id." AND flux_ldap_profil_flux=flux_id ORDER BY flux_order";
		$db2->query($q);
		while($db2->next_record()){	
			echo "<tr>";

			echo "<td>";
			echo "<a class='glyphicon glyphicon-remove' data-toggle='modal' data-target='.bs-item-modal' onClick='changeFrame(\"delete\",$id,".$db2->f("flux_id").")' title='Supprimer une page' />";
			echo "</td>";
			
			echo "<td>".$db2->f("flux_order")."</td>";
			echo "<td>".$db2->f("flux_name")."</td>";
					
			echo "</tr>";
		}		
		
		echo "</table>";
		?>
		<!-- Popup modification item !-->
		<div id="mymodal" class="modal fade bs-item-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		  <div class="modal-dialog modal-lg" style=" max-width:655px; width:80%;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">FLUX</h4>
				</div>
				<div class="modal-body">
					<iframe id="framemodal" frameborder=0 width="100%" height="520px"></iframe>
				</div>
			</div>
		  </div>
		</div>			
	</fieldset>
	<?
}

echo "</form></div></div></div>";

}

?>

<!-- FOOTER --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<?
	include($repository."footer.php");
?>

<!-- SCRIPT --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<? if($tpmod=="") { ?>
	<script>
		$(document).ready(function() {
			$('#datatable').dataTable( {
				"oLanguage": { "sUrl": "<?echo $repository; ?>lib/datatables/dataTables.txt" },
				"iDisplayLength": 50,
				"aoColumns": [{ "bSortable": false },null,{ "bSortable": false }],
				"aaSorting": [[ 1, "asc" ]]
			} );
		} );	
		
		<?php echo $jsaction ?>
	</script>
<? } ?>

<? if($tpmod=="SUBMIT"||$tpmod=="MODIFY"||$tpmod=="DELETE") { ?>
	<script type="text/javascript">
		$('#name').focus();

		$(document).ready(function() {
			$('#datatable').dataTable( {
				"oLanguage": { "sUrl": "<?echo $repository; ?>lib/datatables/dataTables.txt" },
				"iDisplayLength": 50,
				"aoColumns": [{ "bSortable": false },null,null],
				"aaSorting": [[ 1, "asc" ]]
			} );
		} );	
				
		function changeFrame(mode,id1,id2) {
			if(mode=="submit") {
				srcframe="lien.php?tpmod=&tptbl=FLUXLDAP&id1="+id1;
			}
			else {
				srcframe="lien.php?tpmod=DELETE&tptbl=FLUXLDAP&id1="+id1+"&id2="+id2;
			}
			
			$("#framemodal").attr("src",srcframe);
		}
		
		function recharge() {
			$("#formulaire").submit();
		}
		
		function closemodal() {
			$('#mymodal').modal('hide');
		}
		
		<?php echo $jsaction ?>
		
	</script>
<? } ?>







	
	
