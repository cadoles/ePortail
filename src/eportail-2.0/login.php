<?
	include("include/include.php");
	include("header.php");
	
	$db1=new ps_db;
	
	if ($config['activerCAS']) {
		include("include/CASlogin.php");
	}
	
	$fglog=$_POST["fglog"];
	$user_login=$_POST["user_login"];
	$user_password=$_POST["user_password"];

	// Vérifier que l'utilisateur existe bien
	if($fglog!="") {
		$q="SELECT * FROM env_user WHERE user_login='$user_login' AND user_password=md5('$user_password')";
		$db1->query($q);
		if($db1->next_record()){
			$_SESSION['user_id']			=$db1->f("user_id");
			$_SESSION['user_login']			=$db1->f("user_login");
			$_SESSION['user_firstname']		=$db1->f("user_firstname");
			$_SESSION['user_lastname']		=$db1->f("user_lastname");
			$_SESSION['user_pseudo']		=$db1->f("user_pseudo");
			$_SESSION['user_avatar']		=$db1->f("user_avatar");
			$_SESSION['user_mod']			=$db1->f("user_mod");
			$_SESSION['user_profil'] 		=$db1->f("user_profil");
			
			header('Location: index.php');
		}
		else {
			$_SESSION['user_id']			= "";
		}
	}
?>	



<body>
    <nav id="mynavbar" class="navbar navbar-inverse  navbar-fixed-top" role="navigation">
		<div id="myheader" class="hidden-xs" <? if($config["localheader"]!="") echo "style='background-image:url(\"local/css/header.png\");'"; ?>>
			<a href="index.php" alt="<? echo $config['envtitle']; ?>"><div id="mylogo" <? if($config["locallogo"]!="") echo "style='background-image:url(\"local/css/logo.png\");'"; ?>></div></a>
		</div>
		
		<!-- Menu minimum -->
		<div class="navbar-header">
			<a class="navbar-brand <? if($action=="accueil") echo "active"; ?>" href="index.php?action=accueil"><? echo $config["envtitle"]; ?></a>
		</div>

		<!-- Menu principale -->
		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav navbar-right">
				<li><a href="login.php">Connexion</a></li>
			</ul>
		</div>
	</nav>  


	<form class="form-horizontal" role="form" method='post' style="margin:90px 0px 0px 0px" enctype="multipart/form-data">	
		<div class="form-group">
			<label for="envtitle" class="col-sm-5 control-label">Login</label>
			<div class="col-sm-2">
				<input name="user_login" id="user_login" type="titre" class="form-control" placeholder="Titre">
			</div>
		</div>

		<div class="form-group">
			<label for="envtitle" class="col-sm-5 control-label">Password</label>
			<div class="col-sm-2">
				<input name="user_password" id="user_password" type="password" class="form-control" placeholder="Password">
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-offset-5 col-sm-2">
				<input id="fglog" name="fglog" type="submit" class="btn btn-primary" value="Envoyer">
			</div>
		</div>
	</form>

<!-- FOOTER --------------------------------------------------------------------------------------------------------------------------------------- -->  	
	<?
		include("footer.php");
	?>

<!-- SCRIPT --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<script type="text/javascript">
	$('document').ready(function(){
		$('#user_login').focus();
    });
</script>
