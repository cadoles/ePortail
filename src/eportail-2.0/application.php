<?
	$mode=$_GET["mode"];
	
	if($mode=="alone"||$mode=="widget") {
		include($repository."include/include.php");
		include($repository."header.php");
		
		echo "<body>";
	}
	
	include($repository."include/profil.php");
	if($mode!="widget") {
		echo "<iframe id='itemframe' class='myframe' src='' style='height:100%; width:100%; display:none;' frameborder='0' ></iframe>";
	}
	
	// Affichage des catégorie
	$div=array();
	echo "<ul id='navitem' class='nav nav-tabs'>";
	foreach ($tbcat as $cat) {
		if($firstcat=="") $firstcat=$cat[1];
		echo "<li><a onClick='changeCategorie(".$cat[1].");'>".$cat[2]."</a></li>";
	}
	
	// Ajout du widget favoris
	echo "<li><a class='glyphicon glyphicon-heart' onClick='changeBookmark();'></a></li>";
	echo "</ul>";
	
	foreach ($tbcat as $cat) {	
		// Calcul des items pour cette catégorie
		echo "<div id='containeritem-".$cat[1]."' class='containeritem' style='display:none'>";
		foreach ($tbitem as $item) {
			if($cat[1]==$item[2]) {
				echo "<div id='myitem' rel='tooltip' title='".htmlspecialchars($item[5],ENT_QUOTES)."' data-placement='bottom'>";

				if($item[8]==0)
					echo "<div onClick='chargeApplication(\"".urldecode($item[6])."\");'>";
				else
					echo "<a href='".urldecode($item[6])."' target=_blank>";
					
				echo "<img src='local/images/icon/".$item[7]."'/><br>";
				echo $item[4];
				
				if($item[8]==0) echo "</div>";
				else echo "</a>";
				echo "</div>";
			}
		}
		echo "</div>";
	}
	
	// Création du bloque favoris
	echo "<div id='containeritem-XX' class='containeritem' style='display:none'>";
	if($mode=="") $tpwid="bureau";
	else $tpwid="application";
	echo "<iframe id='bookmarkframe' class='myframe' src='bookmark.php?tpwid=$tpwid' style='height:100%; width:100%;' frameborder='0' ></iframe>";
	echo "</div>";
	
	// Calcul des items pour cette catégorie
	echo "<div id='containeritem-".$cat[1]."' class='containeritem' style='display:none'>";
		
	
	if($mode=="alone"||$mode=="widget") {
		include($repository."footer.php");
	}
?>


<script>
	function chargeApplication(srcframe) {
		$('[rel=tooltip]').tooltip('destroy');
				
		if($("#itemframe").length>0) {
			$("#itemframe").attr("src",srcframe);
			$("#itemframe").css("display","block");
		}
		else {
			window.parent.changeItem(srcframe);	
		}
	}
	
	function changeCategorie(idcat) {
		$(".containeritem").css("display","none");
		$("#containeritem-"+idcat).css("display","block");
		$("[rel=tooltip]").tooltip({'animation':false});
		return false;
	}
	
	function changeBookmark() {
		$(".containeritem").css("display","none");
		$("#containeritem-XX").css("display","block");
		$("[rel=tooltip]").tooltip({'animation':false});
		

				
		return false;	}
	
	
	$(document).ready(function()
	{	
		changeCategorie(<? echo $firstcat; ?>);
	});
	
</script>
