<?
	include_once("include/include.php");
	include("header.php");	
	
	$db1=new ps_db;
	$idwid=$_GET['id'];

?>

	
<style>

#widgetadmin {
	position:absolute;
	top:0px;
	right:10px;
	font-size: 0.8em;
	text-align: right;
	float:right;
	z-index: 1000;
}

#widgetadmin a {color: #fff; }

body {
	background-color: #333;
}
</style>
	
<?
	// Récupérer le panel du widget
	$q="SELECT * FROM env_panel_widget WHERE panel_widget_id=".$idwid;
	$db1->query($q);
	if($db1->next_record()) {
		$idpan=$db1->f('panel_widget_panel');
	}
	
	// Récupérer le template de la page widget et le propriétaire de la page
	$q="SELECT panel_user, panel_html FROM env_panel WHERE panel_id=".$idpan;
	$db1->query($q);
	if($db1->next_record()) {
		$idpro=$db1->f('panel_user');
		$html=$db1->f('panel_html');
	}

	// Si l'utilisateur en cours est le propriétaire ou administrateur : il peut modifier la page
	if($idpro==$_SESSION['user_id']||$_SESSION['user_profil']==1) {
		echo "<div id='widgetadmin'>";
		echo "<a id='configWidget' title='Configurer' onclick='parent.configWidget($idpan,$idwid,\"carousel\");' style='cursor:pointer;'>Configurer</a>";
		echo "</div>";
	}
?>


<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

	<ol class="carousel-indicators">
		<?
			$lbclass="class='active'";
			$q="SELECT * FROM env_panel_widget_carousel WHERE panel_widget_carousel_widget=".$idwid." ORDER BY panel_widget_carousel_order";
			$db1->query($q);
			$i=0;
			while($db1->next_record()) {
				echo "<li data-target='#carousel-example-generic' data-slide-to='$i' $lbclass></li>";
				$lbclass="";
				$i=$i+1;
			}
		?>
	</ol>

	<div class="carousel-inner">
		<?
			$lbclass="active";
			$q="SELECT * FROM env_panel_widget_carousel WHERE panel_widget_carousel_widget=".$idwid." ORDER BY panel_widget_carousel_order";
			$db1->query($q);
			$i=0;
			while($db1->next_record()) {
				echo "<div class='item $lbclass' style='background: url(local/images/carousel/".$db1->f("panel_widget_carousel_img").") no-repeat center fixed; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;'>";
				if($db1->f('panel_widget_carousel_url')!="") echo "<a href='".$db1->f('panel_widget_carousel_url')."' target='_blank'>";
				echo "<img src='style/images/blank.gif' style='height:100%; width:100%'>";
				if($db1->f('panel_widget_carousel_url')!="") echo "</a>";
				echo "<div class='carousel-caption'>";
				echo $db1->f("panel_widget_carousel_html");
				echo "</div>";
				echo "</div>";
				
				$lbclass="";
				$i=$i+1;
			}
		?>
	</div>
	<!--
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
  </ol>

	<div class="carousel-inner">
		<div class="item active"
			style="
			background: url(http://fc06.deviantart.net/fs71/i/2014/014/1/f/white_fairy_by_danielauhlig-d7263ab.jpg) no-repeat center fixed; 
			-webkit-background-size: cover; /* pour Chrome et Safari */
			-moz-background-size: cover; /* pour Firefox */
			-o-background-size: cover; /* pour Opera */
			background-size: cover; /* version standardisée */">
		  <img src="style/images/blank.gif" style="height:100%; width:100%">	
		  <div class="carousel-caption">
			qsdfqsdfqsdf
		  </div>
		</div>	

		<div class="item"
			style="
			background: url(http://fc02.deviantart.net/fs23/f/2007/336/4/3/rings_by_Pericolos0.jpg) no-repeat center fixed; 
			-webkit-background-size: cover; /* pour Chrome et Safari */
			-moz-background-size: cover; /* pour Firefox */
			-o-background-size: cover; /* pour Opera */
			background-size: cover; /* version standardisée */">
		  <img src="style/images/blank.gif" style="height:100%; width:100%">	
		  <div class="carousel-caption">
			qsdfqsdfqsdf
		  </div>
		</div>	
		
		<div class="item"
			style="
			background: url(http://fc06.deviantart.net/fs71/f/2013/239/5/7/mononoke_by_danielauhlig-d6jwlcy.jpg) no-repeat center fixed; 
			-webkit-background-size: cover; /* pour Chrome et Safari */
			-moz-background-size: cover; /* pour Firefox */
			-o-background-size: cover; /* pour Opera */
			background-size: cover; /* version standardisée */">
		  <img src="style/images/blank.gif" style="height:100%; width:100%">	
		  <div class="carousel-caption">
			qsdfqsdfqsdf
		  </div>
		</div>		
	</div>
-->

  <!-- Controls -->
  <? if($i>0) { ?>
  <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
  </a>
  <? } ?>
</div>
	
	<div id="info"></div>
<?
	include("footer.php");
?>
