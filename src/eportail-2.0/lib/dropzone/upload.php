<?php
	$repository="../../";
	include($repository."include/include.php");
	include("../php/image.php");
	
	$db1=new ps_db;
	
	// A FAIRE Bloquer la page au invité
	
	
	// Récupération des paramétres
	$directory		= $_GET["directory"];	// cible du upload
	$typeupload		= $_GET["typeupload"];	// image | all 
	$multiupload	= $_GET["multiupload"];	// true | false 
	$resize			= $_GET["resize"];		// true | false uniquement si typeupload = image
	$size			= $_GET["size"];		// taille en px pour le rezise d'une image
	$rename			= $_GET["rename"];		// true | false renommage des fichiers uploadés
	$prefix			= $_GET["prefix"];		// prefix des fichiers renommées 
	
	
	$query			= $_GET["query"];		// query à executer sur le pere vide | insertappico | updateappico
	$idquery		= $_GET["idquery"];		// identifiant unique associé à la query

		
	if (!empty($_FILES)) {
		$tempFile = $_FILES['file']['tmp_name'];           
		$targetPath = $config['localdirectory']."/".$directory."/";
		
		if($rename) {
			$targetExtension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);		
			$targetName=$prefix.uniqid().".".$targetExtension;
		}
		else
			$targetName=$_FILES['file']['name'];
		
		$targetFile =  $targetPath.$targetName;

		if($resize) {
			img_resize($tempFile,$targetFile, array('width' => $size, 'height' => $size, 'constraint' => array('width' => $size, 'height' => $size)));
		}
		else {
			move_uploaded_file($tempFile,$targetFile);
		}
		
		if($query=="insertappico") {
			$db1->query("INSERT INTO env_icon (icon_url) VALUES('$targetName')");
		}
		elseif($query=="updateappico") {
			$db1->query("UPDATE env_icon SET icon_url='$targetName' WHERE icon_id=$idquery");
		}
		
		header('Content-type: text/json');
		header('Content-type: application/json');
		$result['name'] = $targetName;
		
		echo json_encode($result);
	}
?>  

