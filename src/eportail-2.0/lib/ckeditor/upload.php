<?php
	$repository="../../";
	include($repository."include/include.php");
	include("../php/image.php");
	
	$db1=new ps_db;
	
	// A FAIRE Bloquer la page au invité
	
	
	// Récupération des paramétres
	$directory		= $_GET["directory"];	// cible du upload
	$typeupload		= $_GET["typeupload"];	// image | all 
	$multiupload	= $_GET["multiupload"];	// true | false 
	$resize			= $_GET["resize"];		// true | false uniquement si typeupload = image
	$size			= $_GET["size"];		// taille en px pour le rezise d'une image
	$rename			= $_GET["rename"];		// true | false renommage des fichiers uploadés
	$prefix			= $_GET["prefix"];		// prefix des fichiers renommées 
	
	if (!empty($_FILES)) {
		$tempFile = $_FILES['upload']['tmp_name'];           
		$targetPath = $config['localdirectory']."/".$directory."/";
		
		if($rename) {
			$targetExtension = pathinfo($_FILES['upload']['name'], PATHINFO_EXTENSION);		
			$targetName=$prefix.uniqid().".".$targetExtension;
		}
		else
			$targetName=$_FILES['upload']['name'];
		
		$targetFile =  $targetPath.$targetName;

		if($resize) {
			img_resize($tempFile,$targetFile, array('width' => $size, 'height' => $size, 'constraint' => array('width' => $size, 'height' => $size)));
		}
		else {
			move_uploaded_file($tempFile,$targetFile);
		}
		
		
		$funcNum = $_GET['CKEditorFuncNum'];
		echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '".str_replace($config['localdirectory']."/","/eportail/",$targetFile)."', '$message');</script>";
	}
?>  

