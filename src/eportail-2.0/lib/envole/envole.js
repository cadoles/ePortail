		function resizeFrame() {
			// Hauteur iframe au refresh de la page
			var heightbody = $('html').height();
			var heightnavbar = $('#mynavbar').height()
			var heightcategory=$('#mynavcategory').height();
			var heightframe = heightbody-heightnavbar;
			
			
			var topheader = "padding-top: " + String(heightnavbar) + "px !important";
			
			//$("html").css("cssText",topheader);
			$(".container").height(heightframe);
			$("body").css("cssText",topheader);
			
			if($("html").width()>767) {
				$("#mysidecategory").height(heightframe);
			}
			else {
				$("#mysidecategory").height(0);
				heightframe=heightframe-heightcategory;
			}
			
			if($(".myframe").attr("id")=="bookmarkframe") {
				heightframe=heightframe-50;
			}
			
			$(".myframe").height(heightframe);
		}
			
		$(document).ready(function()
		{
			$('#mysidecategory li a').on('click', function() { 
				paneID = $(this).attr('href');
				src = $(paneID).attr('data-src');
				// if the iframe hasn't already been loaded once
				if($(paneID+" iframe").attr("src")=="")
				{
					//resizeFrame();
					$(paneID+" iframe").attr("src",src);
					console.log($(paneID+" iframe").attr("src"));
				}
			});
		});
		
		$('document').ready(function(){
			resizeFrame();
		});

		// Hauteur iframe au resize de la window
		$(window).resize(function() {
			resizeFrame();
		});
