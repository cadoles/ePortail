<?
	include_once("include/include.php");
	include("header.php");	
	
	$db1=new ps_db;
	$id=$_GET['id'];
	$type=$_GET['type'];

?>
	<script src="<? echo $repository; ?>lib/ckeditor/ckeditor.js"></script>	

	<script type="text/javascript">
		var editor, html = '';

		function createEditor()
		{
			var heightbody = $('html').height();

			
			console.log(CKEDITOR.config.toolbar);

			CKEDITOR.config.toolbar_Basic =
			[
				{ name: 'document', items : ['Bold','Italic','Underline','-','JustifyLeft','JustifyCenter','JustifyRight','-', 'NumberedList','BulletedList' ] },
				{ name: 'insert', items : ['Image','Table','Smiley','Link'] },
				{ name: 'styles', items : [ 'Styles','Format','Font','FontSize'] }
			];
			
			
			CKEDITOR.config.toolbar_Full =
			[
				{ name: 'document', items : [ 'Source','-','Save','NewPage','DocProps','Preview','Print','-','Templates' ] },
				{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
				{ name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },

				{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
				{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv',
				'-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
				{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },
				{ name: 'insert', items : [ 'Image','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ] },

				{ name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
				{ name: 'colors', items : [ 'TextColor','BGColor' ] },
				{ name: 'tools', items : [ 'Maximize', 'ShowBlocks','-','About' ] }
			];			
			

			// Valeur par défaut
			directory="local/upload";
			typeupload="image";
			multiupload=false;
			resize=true;
			size="400px";
			rename=true;
			prefix="file_";
			
			// Definition de l'url action
			action = "/eportail/lib/ckeditor/upload.php?directory="+directory+"&multiupload="+multiupload+"&resize="+resize+"&size="+size+"&rename="+rename+"&prefix="+prefix;

			
			CKEDITOR.config.filebrowserUploadUrl = action+"&typeupload=all";
			CKEDITOR.config.filebrowserImageUploadUrl = action+"&typeupload=image";

			<?
				if($type=="widget") {
					echo "CKEDITOR.config.toolbar = 'Basic';";
					echo "CKEDITOR.config.height = heightbody-120;";
				}
				else {
					echo "CKEDITOR.config.toolbar = 'Full';";
					echo "CKEDITOR.config.height = heightbody-120;";
				}
			?>
			

        
			if (editor)
				return;

			// Create a new editor inside the <div id="editor">, setting its value to html
			var config = {};
			editor = CKEDITOR.appendTo( 'editor', config, html );
	
			$('#createEditor').css( "display","none" );
			$('#removeEditor').css( "display","inline" );
			
			$('#editorcontents').css( "display","none" );
		}

		function removeEditor()
		{
			if ( !editor )
				return;

			document.getElementById( 'editorcontents' ).innerHTML = html = editor.getData();
			label=encodeURIComponent(editor.getData());
			$("#info").load("process/process-usereditor-update.php?type=<?echo $type;?>&id=<? echo $id; ?>&html="+label);

			// Destroy the editor.
			editor.destroy();
			editor = null;
			
			$('#createEditor').css( "display","initial" );
			$('#removeEditor').css( "display","none" );	
			$('#editorcontents').css( "display","block" );
			
		}
	</script>

<?
	// Récupérer le template de la page widget et le propriétaire de la page
	if($type=="panel") {
		$q="SELECT panel_user, panel_html FROM env_panel WHERE panel_id=".$id;
		$db1->query($q);
		if($db1->next_record()) {
			$idpro=$db1->f('panel_user');
			$html=$db1->f('panel_html');
		}
	}
	elseif($type=="widget") {
		$q="SELECT panel_user, panel_widget_html FROM env_panel,env_panel_widget WHERE panel_id=panel_widget_panel AND panel_widget_id=".$id;
		$db1->query($q);
		if($db1->next_record()) {
			$idpro=$db1->f('panel_user');
			$html=$db1->f('panel_widget_html');
		}
	}
	
	// Si l'utilisateur en cours est le propriétaire ou administrateur : il peut modifier la page
	if($idpro==$_SESSION['user_id']||$_SESSION['user_profil']==1) {
		echo "<div id='widgetadmin'>";
		echo "<a id='createEditor' title='Modifier la Page' onclick='createEditor();' style='cursor:pointer;'>Modifier la Page</a>";
		echo "<a id='removeEditor' title='Enregister les Modifications' onclick='removeEditor();' style='display:none; cursor:pointer;'>Enregister les Modifications</a>";
		echo "</div>";
	}
?>

	<!-- This div will hold the editor. -->
	<div id="editor">
	</div>
	
	<!-- This div will be used to display the editor contents. -->
	<div id="editorcontents">
		<? echo $html; ?>
		<script>
			html = '<? echo str_replace(chr(10),'',$html); ?>';
		</script>
	</div>
	
	<div id="info"></div>
<?
	include("footer.php");
?>
