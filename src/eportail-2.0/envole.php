<?
	include_once("include/include.php");
	include("header.php");	
	$db1=new ps_db;
?>

<style>
	.carousel-inner > .item {
		background: url(style/images/cloud.png) no-repeat center -120px #428BCA;
	}
	
	.carousel-inner > .item  a{
		color: #ffffff;
	}
</style>

<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
	<ol class="carousel-indicators">
		<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
		<li data-target="#carousel-example-generic" data-slide-to="1"></li>
		<li data-target="#carousel-example-generic" data-slide-to="2"></li>
		<li data-target="#carousel-example-generic" data-slide-to="3"></li>
		<li data-target="#carousel-example-generic" data-slide-to="4"></li>
		<li data-target="#carousel-example-generic" data-slide-to="5"></li>
		<li data-target="#carousel-example-generic" data-slide-to="6"></li>
	</ol>

	<div class="carousel-inner" style="height: 250px !important;">
		<div class="item active">
			<div class="carousel-caption" style="top: 0px;">
				<div style="float:left;text-align: left; width: 30%;text-align:right;">
				<img src="local/images/icon/00-roundcube.png" style="float:right; padding:20px;">
				</div>
				<div style="float:left;text-align: left; width: 70%;">
				<h2><a href='https://envole.ac-dijon.fr/wordpress/2013/02/06/roundcube/' target="_blank">Roundcube</a></h2>
				Messagerie permettant la communication entre élèves, professeurs et responsables.
				</div>
			</div>
		</div>	

		<div class="item">
			<div class="carousel-caption" style="top: 0px;">
				<div style="float:left;text-align: left; width: 30%;text-align:right;">
				<img src="local/images/icon/00-ajaxplorer.png" style="float:right; padding:20px;">
				</div>
				<div style="float:left;text-align: left; width: 70%;">
				<h2><a href='https://envole.ac-dijon.fr/wordpress/2013/02/07/ajaxplorer/' target="_blank">Ajaxplorer</a></h2>
				Ajaxplorer est un gestionnaire de fichier en ligne. Ce gestionnaire permet de naviguer dans l’arborescence des fichiers utilisateurs.
				</div>
			</div>
		</div>	

		<div class="item">
			<div class="carousel-caption" style="top: 0px;">
				<div style="float:left;text-align: left; width: 30%;text-align:right;">
				<img src="local/images/icon/00-owncloud.png" style="float:right; padding:20px;">
				</div>
				<div style="float:left;text-align: left; width: 70%;">
				<h2><a href='https://envole.ac-dijon.fr/wordpress/2013/01/31/owncloud/' target="_blank">Owncloud</a></h2>
				Piwigo est un logiciel de galerie photo pour le web, bâti par une communauté active d’utilisateurs et de développeurs. Les extensions rendent Piwigo facilement personnalisable.
				</div>
			</div>
		</div>				

		<div class="item">
			<div class="carousel-caption" style="top: 0px;">
				<div style="float:left;text-align: left; width: 30%;text-align:right;">
				<img src="local/images/icon/00-piwigo.png" style="float:right; padding:20px;">
				</div>
				<div style="float:left;text-align: left; width: 70%;">
				<h2><a href='https://envole.ac-dijon.fr/wordpress/2013/02/07/piwigo/' target="_blank">Piwigo</a></h2>
				ownCloud est une implémentation open source de services en ligne de stockage et d’applications.
				</div>
			</div>
		</div>				

		<div class="item">
			<div class="carousel-caption" style="top: 0px;">
				<div style="float:left;text-align: left; width: 30%;text-align:right;">
				<img src="local/images/icon/00-wordpress.png" style="float:right; padding:20px;">
				</div>
				<div style="float:left;text-align: left; width: 70%;">
				<h2><a href='https://envole.ac-dijon.fr/wordpress/2013/02/07/wordpress/' target="_blank">Wordpress</a></h2>
				WordPress est le nec plus ultra des plates-formes sémantiques de publication personnelle, alliant esthétique, standards du Web et utilisabilité. 
				</div>
			</div>
		</div>

		<div class="item">
			<div class="carousel-caption" style="top: 0px;">
				<div style="float:left;text-align: left; width: 30%;text-align:right;">
				<img src="local/images/icon/00-dokuwiki.png" style="float:right; padding:20px;">
				</div>
				<div style="float:left;text-align: left; width: 70%;">
				<h2><a href='https://envole.ac-dijon.fr/wordpress/2013/02/07/dokuwiki/' target="_blank">Dokuwiki</a></h2>
				DokuWiki est un Wiki, conforme aux standards, simple à utiliser, dont le but est de créer des documentations de toute sorte.
				</div>
			</div>
		</div>			

		<div class="item">
			<div class="carousel-caption" style="top: 0px;">
				<div style="float:left;text-align: left; width: 30%;text-align:right;">
				<img src="local/images/icon/00-fluxbb.png" style="float:right; padding:20px;">
				</div>
				<div style="float:left;text-align: left; width: 70%;">
				<h2><a href='https://envole.ac-dijon.fr/wordpress/2013/02/07/fluxbb/' target="_blank">Fluxbb</a></h2>
				FluxBB est un forum de discussions écrit en PHP rapide et léger. Ses principaux objectifs sont : d’être plus rapide, plus léger et graphiquement moins intense comparé à d’autres logiciels de forum.
				</div>
			</div>
		</div>			
	</div>

	<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev" style="height: 250px !important;">
		<span class="glyphicon glyphicon-chevron-left"></span>
	</a>
	<a class="right carousel-control" href="#carousel-example-generic" data-slide="next" style="height: 250px !important;">
		<span class="glyphicon glyphicon-chevron-right"></span>
	</a>
</div>

<div id='page-wrapper'>
<div class='container-fluid'>

<div class="col-sm-3">
<div class="thumbnail" style="margin-top:10px; border:none;">
<img class="imgreflect" alt="envole" src="style/images/thumb-envole.jpg" style="border-radius:10px;" />
<div class="caption">
<h3 style="text-align:center">Projet Envole</h3>
<p style="text-align:center"><a class='btn btn-primary' href="http://ent-envole.com" target="_blank">Visitez le site vitrine</a></p>
<p style="text-align:justify">Envole est un Espace Num&eacute;rique Personnel pour l&#39;Education. Il propose une interface de type portail Web 2.0 qui permet l&rsquo;interaction entre un utilisateur et son environnement num&eacute;rique r&eacute;sultant de l&rsquo;utilisation de services h&eacute;t&eacute;rog&egrave;nes. Il centralise dans une seule interface l&rsquo;ensemble des applications de l&rsquo;utilisateur : mail, agenda, dossier personnel, B2I, blog, gestion de notes, gestion des absences, etc &hellip;</p>
</div>
</div>
</div>

<div class="col-sm-3">
<div class="thumbnail" style="margin-top:10px; border:none;">
<img class="imgreflect" alt="mutualisation envole" src="style/images/thumb-mutualisation.jpg" style="border-radius:10px;" />
<div class="caption">
<h3 style="text-align:center">Mutualisation Envole</h3>
<p style="text-align:center"><a class='btn btn-primary' href="http://envole.ac-dijon.fr" style="line-height: 1.6;" target="_blank">Visitez le site vitrine</a></p>
<p style="text-align:justify">Afin de fournir un portail de services en ligne &agrave; tous les acteurs du syst&egrave;me &eacute;ducatif, les acad&eacute;mies d&rsquo;Aix-Marseille, de Besan&ccedil;on, de Cr&eacute;teil, de Dijon, de Reims, de La R&eacute;union, d&rsquo;Orl&eacute;ans-Tours et de Poitiers ont d&eacute;cid&eacute; de coop&eacute;rer et de conjuguer leurs efforts pour d&eacute;velopper en commun un espace num&eacute;rique de travail &agrave; destination des &eacute;coles et des &eacute;tablissements scolaires. Envole est aussi mise en &oelig;uvre sur d&rsquo;autres territoires d&rsquo;acad&eacute;mies qui n&rsquo;ont pas sign&eacute; la convention de partenariat mais dont les &eacute;quipes contribuent au d&eacute;veloppement technique.</p>
</div>
</div>
</div>
  	

<div class="col-sm-3">
<div class="thumbnail" style="margin-top:10px; border:none;">
<img class="imgreflect" alt="projet eole" src="style/images/thumb-eole.jpg" style="border-radius:10px;" />
<div class="caption">
<h3 style="text-align:center">Projet Eole</h3>
<p style="text-align:center"><a class='btn btn-primary' href="http://eole.orion.education.fr/" target="_blank">Visitez le site vitrine</a></p>
<p style="text-align:justify">EOLE est une solution informatique clef en mains compos&eacute;e de produits libres packag&eacute;s pour des besoins sp&eacute;cifiques. Les solutions sont pr&ecirc;tes &agrave; l&rsquo;emploi, elles sont faciles &agrave; installer et &agrave; maintenir. Les besoins couverts vont de la messagerie et la gestion des fichiers au serveur de s&eacute;curit&eacute;. Les fonctionnalit&eacute;s propos&eacute;es par EOLE couvrent tous les besoins d&rsquo;un &eacute;tablissement scolaire, que ce soit pour la p&eacute;dagogie ou l&rsquo;administratif. C&rsquo;est la solution informatique officielle de l&rsquo;Education Nationale. EOLE s&rsquo;ouvre maintenant &agrave; d&rsquo;autres administrations.</p>
</div>
</div>
</div>

<div class="col-sm-3">
<div class="thumbnail" style="margin-top:10px; border:none;">
<img class="imgreflect" alt="cadoles" src="style/images/thumb-cadoles.jpg" style="border-radius:10px;" />
<div class="caption">
<h3 style="text-align:center">Cadoles</h3>
<p style="text-align:center"><a class='btn btn-primary' href="http://www.cadoles.com/" target="_blank">Visitez le site vitrine</a></p>
<p style="text-align:justify">Cadoles est, depuis 2011, une soci&eacute;t&eacute; coop&eacute;rative de services informatiques en logiciels libres bas&eacute;e &agrave; Dijon. Cadoles a &eacute;t&eacute; cr&eacute;&eacute;e par trois passionn&eacute;s autour des valeurs des logiciels libres et des formats ouverts. &nbsp; Ces membres sont experts EOLE, Envole, Gaspacho et divers logiciels libres.</p>
</div>
</div>
</div>

</div>
</div>
<?
	include("footer.php");
?>
