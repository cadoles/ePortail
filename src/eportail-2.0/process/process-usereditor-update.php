<?php 

	$repository="../";
	include_once($repository."include/include.php");

	$db1=new ps_db;

	$type=$_GET['type'];
	$id=$_GET['id'];
	$html=$_GET['html'];

	// Récupérer le template de la page widget et le propriétaire de la page
	$q="SELECT panel_user FROM env_panel WHERE panel_id=".$id;
	$db1->query($q);
	if($db1->next_record()) {
		$idpro=$db1->f('panel_user');
	}

	if($type=="panel") {
		$q="SELECT panel_user, panel_html FROM env_panel WHERE panel_id=".$id;
		$db1->query($q);
		if($db1->next_record()) {
			$idpro=$db1->f('panel_user');
		}
	}
	elseif($type=="widget") {
		$q="SELECT panel_user, panel_widget_html FROM env_panel WHERE panel_id=widget_panel_panel AND widget_panel_id=".$id;
		$db1->query($q);
		if($db1->next_record()) {
			$idpro=$db1->f('panel_user');
		}
	}
	
	// Si l'utilisateur en cours est le propriétaire ou administrateur : il peut modifier la page
	if($idpro==$_SESSION['user_id']||$_SESSION['user_profil']==1) {
		if($type=="panel") {
			$q="UPDATE env_panel SET panel_html='$html' WHERE panel_id=$id";
			$db1->query($q);
		}
		elseif($type=="widget") {
			$q="UPDATE env_panel_widget SET panel_widget_html='$html' WHERE panel_widget_id=$id";
			$db1->query($q);
		}
	}
?>
