<?php 
	$repository="../";
	include_once($repository."include/include.php");
	include_once($repository."include/delete.php");

	$db1=new ps_db;
	$id= $_GET['id'];

	// Récupérer le template de la page widget et le propriétaire de la page
	$q="SELECT panel_user FROM env_panel, env_panel_widget WHERE panel_widget_id=$id AND panel_widget_panel=panel_id";
	$db1->query($q);
	if($db1->next_record()) {
		$idpro=$db1->f('panel_user');
	}
	
	// Si l'utilisateur en cours est le propriétaire ou administrateur : il peut supprimer la page
	if($idpro==$_SESSION['user_id']||$_SESSION['user_profil']==1) {
		delPanelWidget($id);
	}
?>
