<?

	include_once("include/include.php");
	include("header.php");	
	
	$db1=new ps_db;
	$db2=new ps_db;
	
	$idpan=$_GET['idpan'];
	$idpwd=$_GET['idpwd'];
	$mode =$_GET['mode'];
	
	$fgval=$_POST['fgval'];
	$idwid=$_POST['idwid'];
	$widget_label=$_POST['widget_label'];
	$widget_url=$_POST['widget_url'];
	$widget_height=$_POST['widget_height'];
	$widget_style=$_POST['widget_style'];
	
	if($idpan=="")  $idpan=$_POST['idpan'];
	if($mode=="")   $mode=$_POST['mode'];
	if($idpwd=="")  $idpwd=$_POST['idpwd'];
	
	$fgclo=$_POST['fgclo'];

	// Permissions = si non utiisateur exist
	if($_SESSION['user_id']=="") die();	
	
	// Permissions = si non non propriéataire de la page exist
	$q="SELECT panel_user FROM env_panel WHERE panel_id=".$idpan;
	$db1->query($q);
	if($db1->next_record()) {
		$idpro=$db1->f('panel_user');
	}
	if($idpro!=$_SESSION['user_id']&&$_SESSION['user_profil']!=1) {
		die();
	}	

	if($idwid!=""||$mode=="modwidget") {
		$fgparam=1;
	}

	if($fgval!=""&&$mode=="addwidget") {
		
		// Recherche du type de contenu du widget
		$q="SELECT * FROM env_widget WHERE widget_id=$idwid";
		$db1->query($q);
		if($db1->next_record()) {
			$tpwid=$db1->f("widget_type");
		}
		
		// Rechercher le dernier order de la location C1R1 de cette page
		$order=1;
		$q="SELECT panel_widget_order FROM env_panel_widget WHERE panel_widget_panel=$idpan AND panel_widget_loc='C1R1' ORDER BY panel_widget_order DESC LIMIT 1";
		$db1->query($q);
		if($db1->next_record()) {
			$order=$db1->f('page_widget_order')+1;
		}
		
		// Création du widget
		$q="INSERT INTO env_panel_widget (panel_widget_loc,panel_widget_order,panel_widget_widget,panel_widget_panel,panel_widget_label,panel_widget_url,panel_widget_editeur,panel_widget_height,panel_widget_style) VALUES('C1R1',$order,$idwid,$idpan,'".addslashes($widget_label)."','$widget_url','$idedt','$widget_height',$widget_style)";
		$db1->query($q);	
		$fgclo=1;
	}
		

	if($fgval!=""&&$mode=="modwidget") {
		
		// Création du widget
		$q="UPDATE env_panel_widget SET panel_widget_label='".addslashes($widget_label)."', panel_widget_url='$widget_url', panel_widget_height=$widget_height, panel_widget_style=$widget_style WHERE panel_widget_id=$idpwd";
		$db1->query($q);	
		$fgclo=1;
	}

	
	if($fgclo!="") {
		// Action javascript Ajout du panel directement + fermeture de la popup
		$jsaction="recharge(".$idpan.")";
	}
	
?>

<script type="text/JavaScript">
	function closemodal() {
		window.parent.closemodal();	
	}
	
	function recharge(idpan) {
		parent.parent.document.location.href='index.php?action=page&id='+idpan;
	}
	<?php echo $jsaction ?>
</script>



<?
//== SELECTION DU WIDGET =====================================================================================================================================================================

echo "<form id='formulaire' class='form-horizontal' role='form' method='post' enctype='multipart/form-data'>";
echo "<input type='hidden' name='idpan' id='idpan' value='$idpan' />";
echo "<input type='hidden' name='idwid' id='idwid' value='$idwid' />";
echo "<input type='hidden' name='idpwd' id='idpwd' value='$idpwd' />";
echo "<input type='hidden' name='mode'  id='mode'  value='$mode'  />";

if($fgparam!=1) {
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";	
	echo "<a class='btn btn-primary' onclick='closemodal();'>Fermer</a>";
	echo "</div>";
	echo "</div>";	
		
	echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
    echo "<thead>";
	echo "<th width='70px'>Action</th>";
	echo "<th width='90px'>Icone</th>";
	echo "<th>Catégorie</th>";
	echo "<th>Nom</th>";
	echo "</thead>";
	
	$q="SELECT * FROM env_widget, env_widget_type, env_icon WHERE widget_type=widget_type_id AND widget_icon=icon_id";
	$db1->query($q);
	while($db1->next_record()){	
		echo "<tr style='font-size:90%'>";

		echo "<td align='center'>";
		echo "<a style='cursor:pointer' class='glyphicon glyphicon-plus' onClick='$(\"#idwid\").val(\"".$db1->f('widget_id')."\"); $(\"#formulaire\").submit();'></a>";
		echo "</td>";

		echo "<td align='center'>";
		echo "<img src='".$repository."local/images/icon/".$db1->f('icon_url')."' width='40px' height='40px'></img>";
		echo "</td>";
		
		echo "<td>";
		
		$q="SELECT * FROM env_widget_categorie WHERE widget_categorie_id=".$db1->f('widget_categorie');
		$db2->query($q);
		if($db2->next_record()) echo $db2->f('widget_categorie_label');
		
		echo "</td>";
		
		echo "<td>";
		echo $db1->f('widget_name')."<br>".$db1->f('widget_label');
		echo "</td>";
		
		echo "</tr>";
	
	}	
	echo "</table>";
}


//== PARAMETRAGE DU WIDGET ===================================================================================================================================================================
else {
	
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='fgval' name='fgval' class='btn btn-primary' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<a class='btn btn-primary' onclick='closemodal();'>Annuler</a>";
	echo "</div>";
	echo "</div>";	 
	
	// Mode modification
	if($mode=="modwidget") {
		$q="SELECT * FROM env_panel_widget WHERE panel_widget_id=$idpwd";
		$db1->query($q);
		if($db1->next_record()) {
			$widget_label	=$db1->f("panel_widget_label");
			$widget_url		=$db1->f("panel_widget_url");
			$widget_style	=$db1->f("panel_widget_style");
			$widget_height	=$db1->f("panel_widget_height");
			$idwid			=$db1->f("panel_widget_widget");
		}	
	}

	// Recherche du paramétrage par défaut du widget
	$q="SELECT * FROM env_widget WHERE widget_id=$idwid";
	$db1->query($q);
	if($db1->next_record()) {
		$lbwid=$db1->f("widget_name");
		$ulwid=$db1->f("widget_url");
		$sywid=$db1->f("widget_style");
		$hewid=$db1->f("widget_height");
		
		if($widget_label=="")	$widget_label=$lbwid;
		if($widget_height=="") 	$widget_height=$hewid;
		if($widget_style=="") 	$widget_style=$sywid;
		
		if($ulwid!="")			$widget_url="";
	}	
	

?>
	<div class="form-group">
		<label for="widget_label" class="col-sm-3 control-label">Titre*</label>
		<div class="col-sm-6"><input name="widget_label" id="widget_label" type="titre" class="form-control" placeholder="Titre" value="<? echo $widget_label; ?>"></div>
	</div>
	
	
	<div class="form-group" <? if($ulwid!="") echo "style='display:none;'"; ?>>
		<label for="widget_url" class="col-sm-3 control-label">URL*</label>
		<div class="col-sm-6"><input name="widget_url" id="widget_url" type="titre" class="form-control" placeholder="URL" value="<? echo $widget_url; ?>"></div>
	</div>
			

	<div class="form-group">
		<label for="widget_height" class="col-sm-3 control-label">Hauteur*</label>
		<div class="col-sm-6"><input name="widget_height" id="widget_height" type="number" class="form-control" placeholder="Hauteur" value="<? echo $widget_height; ?>"></div>
	</div>
		

	<div class="form-group">
		<label for="widget_style" class="col-sm-3 control-label">Style*</label>
		<div class="col-sm-6">
			<select name="widget_style" id="widget_style" class="form-control">
			<?
				$q="SELECT * FROM env_widget_style";
				$db1->query($q);
				while($db1->next_record()){	
					$lbsel="";
					if($widget_style==$db1->f("widget_style_id")) {
						$lbsel="selected";
					}
					echo "<option value='".$db1->f("widget_style_id")."' ".$lbsel.">".$db1->f("widget_style_label")."</option>";
				}
			?>					
			</select>			
		</div>
	</div>

	<script type="text/javascript">
		$('#widget_label').focus();
	</script>	
<?
}
	echo "<form>";
?>

<!-- FOOTER --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<?
	include($repository."footer.php");
?>

<!-- SCRIPT --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<? if($fgparam!=1) { ?>
	<script>
		$(document).ready(function() {
			$('#datatable').dataTable( {
				"oLanguage": { "sUrl": "<?echo $repository; ?>lib/datatables/dataTables.txt" },
				"iDisplayLength": 50,
				"aaSorting": [[ 1, "asc" ]]
			} );
		} );	
		
		<?php echo $jsaction ?>
	</script>
<? } ?>

