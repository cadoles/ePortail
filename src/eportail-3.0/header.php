<?
	if(stripos(" ".$view,"user/")==1) {
		if(file_exists($config["localdirectory"]."/view/user/sidebar.php")) {
			$fgusesidebar=true;
		}
	}
	elseif(stripos(" ".$view,"admin/")==1) {
		if(file_exists($config["localdirectory"]."/view/admin/sidebar.php")) {
			$fgusesidebar=true;
		}
	}
	elseif(stripos(" ".$view,"moderator/")==1) {
		if(file_exists($config["localdirectory"]."/view/moderator/sidebar.php")) {
			$fgusesidebar=true;
		}
	}
	
	if(!$fgusesidebar) {
		echo "<style>html{height: 100%;} body{height: 100%; background-color:transparent;} #page-wrapper{margin:0px; border-left:none;}</style>";
	}
?>

<script type="text/javascript">  
	$(document).ready(function () {  
		$(".mypanel").sortable({
			tolerance: 'pointer',
			revert: 'invalid',
			//handle: 'li', 
			opacity: 0.6,
			//placeholder: 'placeholder',
			forceHelperSize: true,
			
			update: function ()
			{
				var a = $(this);

				$('li', $(".mypanel")).each(function(index, elem) {
					var $listItem = $(elem);
					var newIndex = $listItem.index();
					var id = $(elem).attr('rel');
					/*
					console.log("TAG   = "+$(elem).prop('tagName'));
					console.log("INDEX = "+index);
					console.log("ID = "+id);
					*/
					
                    var key=randomstring(20);
					$("#myload").load("process.php?process=hash.php&key="+key);
					$("#myload").load("process.php?process=process-userpanel-order.php&key="+key+"&id="+id+"&order="+newIndex);
				});
			} 			
		});			
    
		$(".column").disableSelection();					
	});  
</script>

<body>
<div id="wrapper">
	
<!-- Header -->
<nav id="mynavbar" class="navbar navbar-inverse navbar-static-top" role="navigation" style="margin-bottom: 0">
	
<div id="header" class="page-header" <? if($config["localheader"]!="") echo "style='background-image:url(\"local/images/header/header.png\");".$config["localcssheader"]."'"; ?>>
	<a href="<? echo $config["urlbase"] ?>">
	<div id="page-header-logo" <? if($config["locallogo"]!="") echo "style='background-image:url(\"local/images/logo/logo.png\");".$config["localcsslogo"]."'"; ?>></div>
	<div id="page-header-title">
		<h1 <? if($config["titrecss"]!="") echo "style='".$config["titrecss"]."'"; ?>><? echo $config['title'] ?><br>
		<small <? if($config["sstitrecss"]!="") echo "style='".$config["sstitrecss"]."'";?>><? echo $config['sstitle'] ?></small>
	</div>
	</a>
	
	<div id="page-header-tools">
		<?
			if($_SESSION['user_id']=="") {
				echo "<div id='page-header-logout'>";
				if($config['modeRegistration']=='true') echo "<a title='inscription' id='btnregistration' href='index.php?view=user/inscription.php'><i class='fa fa-pencil-square-o fa-fw'></i></a>";
				echo "<a title='connexion' id='btnlogout' href='login.php'><i class='fa fa-sign-in fa-fw'></i></a>";
				echo "</div>";
			}
			else { 
				if($_SESSION['user_avatar']=="") $class=" class='myavatarvide'";

				echo "<a href='index.php?view=user/profil.php'>";
				echo "<div id='myavatar' $class >";
				if($_SESSION['user_avatar']!="") 
					echo "<img id='img_avatar' name='img_avatar' src='local/images/avatar/".$_SESSION['user_avatar']."' width='100%' height='100%'></img>";
				else
					echo "<img id='img_avatar' name='img_avatar' src='style/images/blank.gif' width='100%' height='100%'></img>";
				echo "</div>";
				echo "</a>";
				
				echo "<div id='page-header-user'>";
				echo "<div id='page-header-user-name'>";
				if($_SESSION['user_pseudo']!="") echo $_SESSION['user_pseudo'];	else echo $_SESSION['user_firstname']." ".$_SESSION['user_lastname'];				
				echo "</div>";
				echo "<div id='page-header-user-icon'>";
				if($_SESSION['user_profil']<=2) echo "<a title='administration' href='index.php?view=admin/index.php'><i class='fa fa-gear fa-fw'></i></a>";
				echo "<a title='déconnexion' href='logout.php'><i class='fa fa-sign-out fa-fw'></i></a>";
				echo "</div>";
				echo "</div>";
			}
		?>
	</div>
</div>

<!-- Navigation -->

	<div class="navbar-header">
		
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			<span class="fa fa-bars"></span>
		</button>

		<a class="navbar-brand<? if($id==""||$id==$tbpage[0][1]) echo " navbar-brand-active"; ?>" href="index.php" ><? echo $config['title'] ?></a>
	</div>
		
	<? if(!$fgusesidebar) { ?>
	<div class="collapse navbar-collapse">
		<ul class="nav navbar-nav">
			<?
				// A partir de la deuxieme page, la première est la page d'accueil
				$firstpage=true;
				foreach ($tbpage as $page) {
					if(!$firstpage) {
						$lbactive="";
						if($id==$page[1]) $lbactive="active";
						
						echo "<li class='$lbactive'><a href='index.php?id=".$page[1]."'>".$page[2]."</a></li>";
					}
					$firstpage=false;
				}
			?>
		</ul>
		
		<ul class="nav navbar-nav mypanel">
			<?
				if($_SESSION['user_id']!="") {
					$q="SELECT * FROM ".$config["dbprefixe"]."panel WHERE panel_user=? ORDER BY panel_order";
					$query=$bdd01->prepare($q);
					$query->execute(array($_SESSION['user_id']));
					while($row=$query->fetch()){
						$lbactive="";
						if($id==$row["panel_id"]) {
							$lbactive="active";
							$idsel=$row["panel_id"];
						}
						
						echo "<li rel='".$row["panel_id"]."' class='$lbactive'><a href='index.php?id=".$row["panel_id"]."'>".$row["panel_label"]."</a></li>";
					}
				}
			?>
		</ul>	
		
		<? if($config["modeCustomize"]=='true') { ?>
		<ul class="nav navbar-nav">
			<?
				if($_SESSION['user_id']!="") {
					echo "<li style='width:81px;'>";
					echo "<a class='mybtn fa fa-plus' style='cursor: pointer;float: left; width:25px' data-toggle='modal' data-target='#mymodal-01' title='Ajouter onglet' onClick='ModalLoad(\"mymodal-01\",\"panel-insert\",0,0)'></a>";
					
					if($idsel!="") {
						echo "<a class='mybtn fa fa-file' style='float: left; width:25px; padding-left:5px;' data-toggle='modal' data-target='#mymodal-01' title='Modifier onglet' onClick='ModalLoad(\"mymodal-01\",\"panel-update\",$idsel,0)'></a>";
						echo "<a class='mybtn fa fa-trash' style='float: left; width:25px; padding-left:5px;' title='Supprimer onglet' onClick='SupPanel(".$idsel.")'></a>";
					}
					echo "</li>";
				}
			?>
		</ul>
		<? } ?>
	</div>
	<? } ?>



	<?
		if(stripos(" ".$view,"user/")==1 AND $fgusesidebar) {
			include("view/user/sidebar.php");
		}
		elseif(stripos(" ".$view,"admin/")==1 AND $fgusesidebar) {
			include("view/admin/sidebar.php");
		}
		elseif(stripos(" ".$view,"moderator/")==1 AND $fgusesidebar) {
			include("view/moderator/sidebar.php");
		}		
	?>
</nav>
