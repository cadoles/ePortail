

// création de l'objet metismenu sur le la side bar
$(function() {
	var element=$('#side-menu');
	if(element.length){
		$('#side-menu').metisMenu();
	}
});


// Recalcul des tailles des différents objets sur le load/resize de page
$(function() {
    $(window).bind("load resize", function() {
        //topOffset = 50;
        topOffset = $("#header").height()+$(".navbar-header").height()+1;
        
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse')
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse')
        }

        height = (this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    })
})


// Fonction de génération de clé alèatoire
function randomstring(L){
	var s= '';
	var randomchar=function(){
		var n= Math.floor(Math.random()*62);
		if(n<10) return n; //1-10
		if(n<36) return String.fromCharCode(n+55); //A-Z
		return String.fromCharCode(n+61); //a-z
	}
	while(s.length< L) s+= randomchar();
	return s;
}


// Ouverture de la fenetre modal
function ModalLoad(idmodal,type,id1,id2) {
	key=randomstring(20);

	// GESTION DES ICONES
	if(type=="icon-cropinsert") {
		view="tools/jcrop.php";
		titre="Insertion d'un icône découpé";
	}
	else if(type=="icon-cropupdate") {
		view="tools/jcrop.php";
		titre="Insertion d'un icône découpé";
	}
	else if(type=="icon-dropinsert") {
		view="tools/dropzone.php";
		titre="Insertion d'un icône";
	}
	else if(type=="icon-dropupdate") {
		view="tools/dropzone.php";
		titre="Modification d'un icône";
	}	
	else if(type=="icon-selection") {
		view="tools/icon.php";
		titre="Selection d'un icône";
	}
	
	// GESTION DES AVATARS
	else if(type=="avatar-cropupdate") {
		view="tools/jcrop.php";
		titre="Modification de votre Avatar";		
	}
	else if(type=="ssocommunity-insert") {
		view="admin/lien.php";
		titre="Ajouter un attribut";		
	}
	
	// GESTION DU SSO
	else if(type=="ssocommunity-delete") {
		view="admin/lien.php";
		titre="Suppression d'un attribut";		
	}
	else if(type=="ssoprofil-insert") {
		view="admin/lien.php";
		titre="Ajouter un attribut";		
	}
	else if(type=="ssoprofil-delete") {
		view="admin/lien.php";
		titre="Suppression d'un attribut";		
	}	
	
	// GESTION DES APPLICATIONS
	else if(type=="applicationprofil-insert") {
		view="admin/lien.php";
		titre="Ajouter une application";		
	}
	else if(type=="applicationprofil-delete") {
		view="admin/lien.php";
		titre="Suppression d'une application";		
	}
	else if(type=="applicationssoprofil-insert") {
		view="admin/lien.php";
		titre="Ajouter une application";		
	}
	else if(type=="applicationssoprofil-delete") {
		view="admin/lien.php";
		titre="Suppression d'une application";		
	}
	else if(type=="applicationldapprofil-insert") {
		view="admin/lien.php";
		titre="Ajouter une application";		
	}
	else if(type=="applicationldapprofil-delete") {
		view="admin/lien.php";
		titre="Suppression d'une application";		
	}
	
	// GESTION DES PAGES
	else if(type=="pageprofil-insert") {
		view="admin/lien.php";
		titre="Ajouter une page";		
	}
	else if(type=="pageprofil-delete") {
		view="admin/lien.php";
		titre="Suppression d'une page";		
	}
	else if(type=="pagessoprofil-insert") {
		view="admin/lien.php";
		titre="Ajouter une page";		
	}
	else if(type=="pagessoprofil-delete") {
		view="admin/lien.php";
		titre="Suppression d'une page";		
	}
	else if(type=="pageldapprofil-insert") {
		view="admin/lien.php";
		titre="Ajouter une page";		
	}
	else if(type=="pageldapprofil-delete") {
		view="admin/lien.php";
		titre="Suppression d'une page";		
	}
	
	// GESTION DES FLUX
	else if(type=="fluxmsg-insert") {
		view="admin/fluxmsg.php";
		titre="Ajout d'un Article";		
	}
	else if(type=="fluxmsg-update") {
		view="admin/fluxmsg.php";
		titre="Modification d'un Article";		
	}
	else if(type=="fluxmsg-delete") {
		view="admin/fluxmsg.php";
		titre="Suppression d'un Article";		
	}
	else if(type=="fluxprofil-insert") {
		view="admin/lien.php";
		titre="Ajouter un flux";		
	}
	else if(type=="fluxprofil-delete") {
		view="admin/lien.php";
		titre="Suppression d'un flux";		
	}
	else if(type=="fluxssoprofil-insert") {
		view="admin/lien.php";
		titre="Ajouter un flux";		
	}
	else if(type=="fluxssoprofil-delete") {
		view="admin/lien.php";
		titre="Suppression d'un flux";		
	}
	else if(type=="fluxldapprofil-insert") {
		view="admin/lien.php";
		titre="Ajouter un flux";		
	}
	else if(type=="fluxldapprofil-delete") {
		view="admin/lien.php";
		titre="Suppression d'un flux";		
	}
				
	// GESTION DES ONGLETS
	else if(type=="panel-insert") {
		view="user/panelmodify.php";
		titre="Ajout d'une Page";		
	}
	else if(type=="panel-update") {
		view="user/panelmodify.php";
		titre="Modification d'une Page";		
	}
	
	// GESTION DES WIDGETS
	else if(type=="widget-insert") {
		view="user/widgetmodify.php";
		titre="Ajout d'un widget";		
	}			
	else if(type=="widget-update") {
		view="user/widgetmodify.php";
		titre="Modification d'un widget";		
	}
	else if(type=="widget-template") {
		view="user/widgettemplate.php";
		titre="Modification de la forme";		
	}
	else if(type=="widget-config") {
		view="user/widgetconfig.php";
		titre="Configuration du widget";		
	}
	else if(type=="widget-carouseldrop") {
		view="tools/dropzone.php";
		titre="Télécharger une image";
	}	
			
	// Création des paramétres de sessions de la modal
	
	$( "#myload" ).load("process.php?process=hash.php&key="+key+"&view="+view+"&idmodal="+idmodal+"&type="+type+"&id1="+id1+"&id2="+id2);	
	
	// Définition de la source
	srcframe="modal.php?key="+key;
	
	// Execution de la source
	$("#"+idmodal+" .modal-header h4").text(titre);
	$("#"+idmodal+" #framemodal").attr("src",srcframe);
}


function ModalRecharge() {
	parent.document.getElementById('fgreload').value="MODIFY";
	$("#formulaire").submit();
}

function ModalClose(idmodal) {
	$(idmodal).modal('hide');
}

function resizeFrame() {
	if($("#frameresize").length>0) {
		var heightbody = $('html').height();
		var heightnavbar = $('#mynavbar').height()
		var heightframe = heightbody-heightnavbar;
		$("#frameresize").height(heightframe);
	}
}


function SupPanel(idpan) {
	if(confirm("Confirmez-vous la suppression de cette page ?")) {
		var key=randomstring(20);
		$("#info").load("process.php?process=hash.php&key="+key);
		$("#info").load("process.php?process=process-userpanel-del.php&key="+key+"&id="+idpan);
		setTimeout(function () {document.location.href='index.php';},1000);	
	}
}

	
$('document').ready(function(){
	resizeFrame();
});

$(window).resize(function() {
	resizeFrame();
});

