<?php
	include("../../include/config.php");
	include("../../include/mysql.php");
	include("../../include/session.php");
	include("../php/image.php");
	
	$fp=fopen("/tmp/babylon.log","w");
	
	$keymodal	= $_GET["key"];
	
	// Récupération des paramétres
	$directory		= $_SESSION[$keymodal]["modal-directory"];		// cible du upload
	$typeupload		= $_SESSION[$keymodal]["modal-typeupload"];		// image | all 
	$multiupload	= $_SESSION[$keymodal]["modal-multiupload"];	// true | false 
	$resize			= $_SESSION[$keymodal]["modal-resize"];			// true | false uniquement si typeupload = image
	$size			= $_SESSION[$keymodal]["modal-size"];			// taille en px pour le rezise d'une image
	$rename			= $_SESSION[$keymodal]["modal-rename"];			// true | false renommage des fichiers uploadés
	$prefix			= $_SESSION[$keymodal]["modal-prefix"];			// prefix des fichiers renommées 
	$query			= $_SESSION[$keymodal]["type"];					// query à executer sur le pere vide | insertappico | updateappico
	$idquery		= $_SESSION[$keymodal]["modal-idquery"];		// identifiant unique associé à la query

	fwrite($fp,"DIRECTORY = $directory\n");
	fwrite($fp,"REMANE = $remane\n");
	fwrite($fp,"RESIZE = $resize\n");
	fwrite($fp,"QUERY = $query\n");
	fwrite($fp,"ID QUERY = $idquery\n");
	
	if (!empty($_FILES)) {
		$tempFile = $_FILES['file']['tmp_name'];           
		$targetPath = $config['localdirectory']."/".$directory."/";
		
		if($rename) {
			$targetExtension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);		
			$targetName=$prefix.uniqid().".".$targetExtension;
		}
		else
			$targetName=$_FILES['file']['name'];
		
		$targetFile =  $targetPath.$targetName;
		fwrite($fp,"TARGETFILE = $targetFile\n");
		
		if($resize) {
			img_resize($tempFile,$targetFile, array('width' => $size, 'height' => $size, 'constraint' => array('width' => $size, 'height' => $size)));
			fwrite($fp,"IMAGE RESIZE\n");
		}
		else {
			move_uploaded_file($tempFile,$targetFile);
		}
		
		if($query=="icon-dropinsert") {
			$q="INSERT INTO ".$config["dbprefixe"]."icon (icon_url) VALUES(?)";
			$tbq=array($targetName);
			$query=$bdd01->prepare($q);
			$query->execute($tbq);	
		}
		elseif($query=="icon-dropupdate") {
			$q="UPDATE ".$config["dbprefixe"]."icon SET icon_url=? WHERE icon_id=?";
			fwrite($fp,"QUERY = $q\n");
			$tbq=array($targetName,$idquery);
			$query=$bdd01->prepare($q);
			$query->execute($tbq);
			fwrite($fp,"QUERY OK\n");	
		}
		
		header('Content-type: text/json');
		header('Content-type: application/json');
		$result['name'] = $targetName;
		
		echo json_encode($result);
	}
?>  

