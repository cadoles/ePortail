<?php
try{
	global $bdd01;
	global $bdd02;
	global $bdd03;
	
	$pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
	$pdo_options[PDO::MYSQL_ATTR_INIT_COMMAND] = "SET NAMES utf8";

	$bdd01 = new PDO("mysql:host=".$config["dbhost"].";dbname=".$config["dbname"],$config["dblogin"],$config["dbpassword"],$pdo_options);
	$bdd02 = new PDO("mysql:host=".$config["dbhost"].";dbname=".$config["dbname"],$config["dblogin"],$config["dbpassword"],$pdo_options);
	$bdd03 = new PDO("mysql:host=".$config["dbhost"].";dbname=".$config["dbname"],$config["dblogin"],$config["dbpassword"],$pdo_options);
}
catch (Exception $e){
	die('Erreur : ' . $e->getMessage());
}
?>
