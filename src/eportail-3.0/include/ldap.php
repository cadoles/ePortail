<?

/* LDAP */

$config['activerLDAPModif']				= true;				/* Permettre la modification des paramétres LDAP 	*/
$config['activerLDAP']					= true;				/* Activer le mode LDAP								*/

// Adresse du Serveur LDAP
$config['LDAPserver']					= "127.0.0.1";
$config['LDAPport'] 					= "389";

$config['LDAPreaderdn'] 				= "cn=reader,o=gouv,c=fr";
$config['LDAPreaderpw']					= "95aef242ea634e20232c174fc48efe1d3a7ea101e1a9ac392c4aa238";

$config['LDAPwriterdn'] 				= "";
$config['LDAPwriterpw']					= "";

$config['LDAPracine'] 					= "o=gouv,c=fr";
$config['LDAPorganisation']     		= "ou=education";
$config['LDAPfilterauth'] 				= "uid=#login#";
$config['LDAPfirstname'] 				= "givenname";
$config['LDAPlastname'] 				= "sn";
$config['LDAPemail'] 					= "mail";

if($config['activerLDAPModif']&&file_exists($config['localdirectory']."/local/config/ldap.php")) {
	include("local/config/ldap.php");
}


?>
