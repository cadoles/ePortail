<?
	require_once 'CAS-1.3.1/eoleCAS.php';
	require_once 'configCAS/cas.inc.php';
	
	eolephpCAS::client(CAS_VERSION_2_0, __CAS_SERVER, __CAS_PORT, '', true);		

	if (__CAS_LOGOUT){
		if (method_exists(eolephpCAS, 'eolelogoutRequests')){
			eolephpCAS::eolelogoutRequests(false);
		}
	}

	if (__CAS_VALIDER_CA) {
		EolephpCAS::setCasServerCACert(__CAS_CA_LOCATION); // verification par rapport a la CA
	} else {
		if (method_exists("EolephpCAS", "setNoCasServerValidation")){
			EolephpCAS::setNoCasServerValidation();
		}
	}
	

	if(EolephpCAS::checkAuthentication()) {
		$user = EolephpCAS::getUser();
		$details = EolephpCAS::getDetails();
		
		$user_datas = array('user'=>$user, 'details'=>$details);	
				
		autocreate($user,"",$details["utilisateur"]["firstname"][0],$details["utilisateur"]["lastname"][0],$details["utilisateur"]["email"][0]);
		
		$query=$bdd01->prepare("SELECT * FROM ".$config["dbprefixe"]."user WHERE user_login=?");
		
		$query->execute(array($user));
		if($row=$query->fetch())
		{
			savesession($row,$user,"",isset($_POST['remember']));
		}
	}
	else {
		$_SESSION['user_id'] = "";
	}

?>
