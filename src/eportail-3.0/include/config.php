<?
$config=Array();


/* Serveur */
$config["host"] 						= "scribe23.ac-envole.fr";
$config["urlbase"]						= "https://scribe23.ac-envole.fr/eportail";
$config["localdirectory"]				= "/var/www/html/eportail";
$config["alias"]						= "eportail";


/* Database */
$dbpassword="cohthaek";
$config["dbhost"]						= "127.0.0.1";
$config["dbname"] 						= "eportail";
$config["dblogin"]						= "eportail";
$config["dbpassword"]					= $dbpassword;
$config["dbprefixe"]					= "env_";


/* Connexion */
$config["modeAuthentification"]			= "CAS";						// Mode Authentification = CAS ou LDAP ou MYSQL
$config["modePrivate"]					= 'false';						// Mode privé
$config["modeRemember"]					= 'true';						// Activation de l'auto login via sauvegarde de session

/* Inscription */
$config["modeRegistration"]				= 'false';						// Activer l'inscription en ligne
$config["modeRegistrationConfirm"]		= 'direct';						// Validation de l'inscription direct email ou via validation admin
$config["modeRegistrationExterne"]		= '';							// Si module d'inscription externe mettre url du module
$config["modeProfilExterne"]			= '';							// Si module d'inscription externe mettre url du module
$config["modeAvatarExterne"]			= '';							// Si module d'inscription externe mettre url du module

/* Variables Globales */
$config["theme"]						= "";							// Nom du theme si vide theme par défaut
$config["title"]						= "scribe23";				// Titre du site
$config["sstitle"]						= "Site Envole";				// Sous-Titre du site
$config["locallogo"]   					= "";							// Logo personnalisé
$config["localheader"] 					= "";							// Bannière personnalisé
$config["modeCustomize"]				= 'true';						// Personnalisable par les utilisateur

/* Variables Synchronisation */
$config["synchro-balado"]				= 'true';
$config["synchro-cdt"]					= 'true';
$config["synchro-iconito"]				= 'true';
$config["synchro-moodle"]				= 'true';
$config["synchro-wordpress"]			= 'true';


/* Configuration locale */
if(file_exists($config["localdirectory"]."/local/config/config.php")) {
	include($config["localdirectory"]."/local/config/config.php");
}

$_SERVER["DOCUMENT_ROOT"] = $config["localdirectory"]."/";
?>
