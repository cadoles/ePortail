<?

function delPanel($id) {
	global $bdd01;
	global $config;
	
	// Suppression des widget du panel
	$q="SELECT panel_widget_id FROM ".$config["dbprefixe"]."panel_widget WHERE panel_widget_panel=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
	while($row=$query->fetch()){	
		delPanelWidget($row["panel_widget_id"]);
	}

	$q="DELETE FROM ".$config["dbprefixe"]."panel_widget WHERE panel_widget_panel=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
	
	// Suppression du lien panel profil
	$q="DELETE FROM ".$config["dbprefixe"]."panel_profil WHERE panel_profil_panel=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));

	// Suppression des profil flux sso
	$q="DELETE FROM ".$config["dbprefixe"]."panel_sso_profil WHERE panel_sso_profil_panel=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));

	// Suppression des profil flux ldap
	$q="DELETE FROM ".$config["dbprefixe"]."panel_ldap_profil WHERE panel_ldap_profil_panel=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
	
	// Suppression du panel en lui meme
	$q="DELETE FROM ".$config["dbprefixe"]."panel WHERE panel_id=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
}

function delApplication($id) {
	global $bdd01;
	global $config;

	// Suppression des profil flux
	$q="DELETE FROM ".$config["dbprefixe"]."application_profil WHERE application_profil_application=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
	
	// Suppression des profil flux sso
	$q="DELETE FROM ".$config["dbprefixe"]."application_sso_profil WHERE application_sso_profil_application=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));

	// Suppression des profil flux ldap
	$q="DELETE FROM ".$config["dbprefixe"]."application_ldap_profil WHERE application_ldap_profil_application=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
	
	// Suppression de l'application en question
	$q="DELETE FROM ".$config["dbprefixe"]."application WHERE application_id=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
}

function delFlux($id) {
	global $bdd01;
	global $config;
	
	// Suppression des messages du flux
	$q="DELETE FROM ".$config["dbprefixe"]."fluxmsg WHERE fluxmsg_flux=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));

	// Suppression des profil flux
	$q="DELETE FROM ".$config["dbprefixe"]."flux_profil WHERE flux_profil_flux=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
	
	// Suppression des profil flux sso
	$q="DELETE FROM ".$config["dbprefixe"]."flux_sso_profil WHERE flux_sso_profil_flux=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));

	// Suppression des profil flux ldap
	$q="DELETE FROM ".$config["dbprefixe"]."flux_ldap_profil WHERE flux_ldap_profil_flux=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
	
	// Suppression du flux en question
	$q="DELETE FROM ".$config["dbprefixe"]."flux WHERE flux_id=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
}

function delPanelWidget($id) {
	global $bdd01;
	global $config;

	// Suppression de la panel widget carrousel
	$q="DELETE FROM ".$config["dbprefixe"]."panel_widget_carousel WHERE panel_widget_carousel_widget=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));

	// Suppression de la panel widget bookmark
	$q="DELETE FROM ".$config["dbprefixe"]."panel_widget_bookmark WHERE panel_widget_bookmark_widget=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
			
	// Suppression de la panel widget associée
	$q="DELETE FROM ".$config["dbprefixe"]."panel_widget WHERE panel_widget_id=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
}

function delWidget($id) {
	global $bdd01;
	global $config;
	
	// Suppression des panel widget associées
	$q="SELECT panel_widget_id FROM ".$config["dbprefixe"]."panel_widget WHERE panel_widget_widget=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
	while($row=$query->fetch()){	
		delPanelWidget($row["panel_widget_id"]);
	}
	
	// Suppression du widget en lui meme
	$q="DELETE FROM ".$config["dbprefixe"]."widget WHERE widget_id=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
}

function delLdapProfil($id) {
	global $bdd01;
	global $config;
	
	// Suppression de page profil ldap
	$q="DELETE FROM ".$config["dbprefixe"]."panel_ldap_profil WHERE panel_ldap_profil_ldap_profil=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
	
	// Suppression des application profil ldap
	$q="DELETE FROM ".$config["dbprefixe"]."application_ldap_profil WHERE application_ldap_profil_ldap_profil=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));

	// Suppression des flux profil ldap
	$q="DELETE FROM ".$config["dbprefixe"]."flux_ldap_profil WHERE flux_ldap_profil_profil=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
	
	// Suppression du profils en lui même
	$q="DELETE FROM ".$config["dbprefixe"]."ldap_profil WHERE ldap_profil_id=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
	
}


function delLdapCommunity($id) {
	global $bdd01;
	global $config;

	// Suppression des profils ldap associés
	$q="SELECT * FROM ".$config["dbprefixe"]."ldap_profil WHERE ldap_profil_ldap_community=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
	while($row=$query->fetch()){	
		delLdapProfil($row["ldap_profil_id"]);
	}	
	
	// Suppression de la communauté en elle même
	$q="DELETE FROM ".$config["dbprefixe"]."ldap_community WHERE ldap_community_id=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
}

function delSsoProfil($id) {
	global $bdd01;
	global $config;

	// Suppression des association attribut profil
	$q="DELETE FROM ".$config["dbprefixe"]."sso_profil_attribut WHERE sso_profil_attribut_profil=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
		
	// Suppression de page profil sso
	$q="DELETE FROM ".$config["dbprefixe"]."panel_sso_profil WHERE panel_sso_profil_sso_profil=$id";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));	
	
	// Suppression des application profil sso
	$q="DELETE FROM ".$config["dbprefixe"]."application_sso_profil WHERE application_sso_profil_sso_profil=$id";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
	
	// Suppression des flux profil ldap
	$q="DELETE FROM ".$config["dbprefixe"]."flux_sso_profil WHERE flux_sso_profil_profil=$id";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
		
	// Suppression du profils en lui même
	$q="DELETE FROM ".$config["dbprefixe"]."sso_profil WHERE sso_profil_id=$id";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));	
}

function delSsoCommunity($id) {
	global $bdd01;
	global $config;

	// Suppression des profils ldap associés
	$q="SELECT * FROM ".$config["dbprefixe"]."sso_profil WHERE sso_profil_community=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
	while($row=$query->fetch()){	
		delSsoProfil($row["sso_profil_id"]);
	}	
	
	// Suppression des association attribut community
	$q="DELETE FROM ".$config["dbprefixe"]."sso_community_attribut WHERE sso_community_attribut_community=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));		
	
	// Suppression de la communauté en elle même
	$q="DELETE FROM ".$config["dbprefixe"]."sso_community WHERE sso_community_id=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));		
}

function delSsoAttribut($id) {
	global $bdd01;
	global $config;
	
	// Suppression des association attribut community
	$q="DELETE FROM ".$config["dbprefixe"]."sso_community_attribut WHERE sso_community_attribut_attribut=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));		

	// Suppression des association attribut profil
	$q="DELETE FROM ".$config["dbprefixe"]."sso_profil_attribut WHERE sso_profil_attribut_attribut=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));	
		
	// Suppression de l'attribut en lui meme
	$q="DELETE FROM ".$config["dbprefixe"]."sso_attribut WHERE sso_attribut_id=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
}


function delGroup($idgrp) {
	$dbdel=new ps_db;

	// Suppression du lien utilisateur groupe
	$q="DELETE FROM env_groupuser WHERE groupuser_group_id=$idgrp";
	$dbdel->query($q);
	
	// Suppression du group en lui meme
	$q="DELETE FROM env_group WHERE group_id=$idgrp";
	$dbdel->query($q);
}

function delUser($id) {
	global $bdd01;
	global $config;
	
	// Suppression des panel de l'utilisateur
	$q="SELECT panel_id FROM ".$config["dbprefixe"]."panel WHERE panel_user=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
	while($row=$query->fetch()){	
		delPanel($row["panel_id"]);
	}

	// Suppression de l'utilisateur en lui meme
	$q="DELETE FROM ".$config["dbprefixe"]."user WHERE user_id=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
}


?>
