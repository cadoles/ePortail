<?
	$fgdebug = $_GET["fgdebug"];
	
    
	$tbitem		= array();		// Liste des items associés à l'utilisateur
	$tbcat  	= array();		// Liste des catégories d'items associés à l'utilisateur
	$tbpage 	= array();		// Liste des pages associées à l'utilisateur
	$tbflux 	= array();		// Liste des flux associés à l'utilisateur
	$tbattrs	= array();		// Liste des attributs SSO de l'utilisateur
    $tbtmp		= array();		// Tableau temporaire
    
//== PROFIL ============================================================================================================================================
	
// Récupérer l'ensemble des catégories d'item associé au profil de l'utilisateur
	// 0 = categorie order
	// 1 = categorie id
	// 2 = categorie label
	// 3 = categorie color
	
	$q = "SELECT application_categorie_id, application_categorie_label, application_categorie_color FROM ".$config["dbprefixe"]."application_profil, ".$config["dbprefixe"]."application, ".$config["dbprefixe"]."icon, ".$config["dbprefixe"]."application_categorie
					WHERE application_profil_profil=?
					AND application_profil_application=application_id
					AND application_icon=icon_id
					AND application_categorie=application_categorie_id
					GROUP BY application_categorie_id
					ORDER BY application_categorie_order, application_order";

	$query01=$bdd01->prepare($q);
	$query01->execute(array($_SESSION['user_profil']));
	while($row01=$query01->fetch()) {
		array_push($tbcat,array($row01["application_categorie_order"],$row01["application_categorie_id"],$row01["application_categorie_label"],$row01["application_categorie_color"]));
	}
	
	
	
// Récupérer l'ensemble des applications associé au profil de l'utilisateur
	// 0 = categorie order
	// 1 = application order
	// 2 = categorie id
	// 3 = application id
	// 4 = applcation label
	// 5 = application description
	// 6 = application url
	// 7 = icone url
	// 8 = application mode d'ouverture
	// 9 = application couleur
	
	$q = "SELECT * FROM ".$config["dbprefixe"]."application_profil, ".$config["dbprefixe"]."application, ".$config["dbprefixe"]."icon, ".$config["dbprefixe"]."application_categorie
					WHERE application_profil_profil=?
					AND application_profil_application=application_id
					AND application_icon=icon_id
					AND application_categorie=application_categorie_id
					ORDER BY application_categorie_order, application_order";
	$query01=$bdd01->prepare($q);
	$query01->execute(array($_SESSION['user_profil']));
	while($row01=$query01->fetch()) {
		array_push($tbitem,array($row01["application_categorie_order"],$row01["application_order"],$row01["application_categorie"],$row01["application_id"],$row01["application_label"],$row01["application_description"],$row01["application_url"],$row01["icon_url"],$row01["application_open"],$row01["application_color"]));
	}
	
// Récupérer l'ensemble des pages associées au profil de l'utilisateur
	// 0 = page order
	// 1 = page id
	// 2 = page label
	
	$q = "SELECT * FROM ".$config["dbprefixe"]."panel_profil, ".$config["dbprefixe"]."panel
					WHERE panel_profil_profil=?
					AND panel_profil_panel=panel_id
					ORDER BY panel_order";	
	$query01=$bdd01->prepare($q);
	$query01->execute(array($_SESSION['user_profil']));
	while($row01=$query01->fetch()) {
		array_push($tbpage,array($row01["panel_order"],$row01["panel_id"],$row01["panel_label"]));
	}

// Récupérer l'ensemble des flux associés au profil de l'utilisateur
	// 0 = flux order
	// 1 = flux id
	// 2 = flux label
	// 3 = type
	// 4 = flux url
	// 5 = flux number
	// 6 = flux color
	// 7 = flux style
	
	$q = "SELECT * FROM ".$config["dbprefixe"]."flux_profil, ".$config["dbprefixe"]."flux
					WHERE flux_profil_profil=?
					AND flux_profil_flux=flux_id
					ORDER BY flux_order";
	$query01=$bdd01->prepare($q);
	$query01->execute(array($_SESSION['user_profil']));
	while($row01=$query01->fetch()) {
		array_push($tbflux,array($row01["flux_order"],$row01["flux_id"],$row01["flux_name"],$row01["flux_type"],$row01["flux_url"],$row01["flux_number"],$row01["flux_color"],$row01["flux_style"]));
	}
	
//== PROFIL LDAP =======================================================================================================================================

	// Connection au LDAP
	$fgLDAP=$config['activerLDAP'];
	if($config['activerLDAP']&&$_SESSION['user_login']!="") {
		$ds = ldap_connect($config['LDAPserver'], $config['LDAPport']);
		if (!$ds) {
			$config['activerLDAP']=false;
		}

		$r=ldap_bind($ds,$config['LDAPreaderdn'],$config['LDAPreaderpw']);	
		if(!$r) {
			$config['activerLDAP']=false;
		}
	}

	if($config['activerLDAP']&&$_SESSION['user_login']!="") {
	
		if($fgdebug) echo "<h1>GESTION DES PROFIL LDAP</h1>";
		
		// Déterminer dans quelles communauté appartient l'utilisateur
		$q = "SELECT * FROM ".$config["dbprefixe"]."ldap_community";
		$query01=$bdd01->prepare($q);
		$query01->execute();
		while($row01=$query01->fetch()) {
			$fgmembre=false;
			$lbfilter=str_replace("#login#",$_SESSION['user_login'],$row01["ldap_community_filter"]);
			if($fgdebug) echo "<h2>".$row01["ldap_community_label"]."</h2>Filtre Communauté= ".$lbfilter;			
			
			if($row01["ldap_community_filter"]=="") {
				$fgmembre=true;
				if($fgdebug) echo "<br>appartient à la communauté = ".$row01["ldap_community_label"];
			}
			else {
				$res = ldap_search($ds,$config['LDAPracine'],$lbfilter);

				$ldapinfo = ldap_get_entries($ds, $res);
				if($ldapinfo["count"]>0) {
					$fgmembre=true;
					if($fgdebug) echo "<br>appartient à la communauté = ".$row01["ldap_community_label"];
				}
			}
			
			// Il fait partie de cette communauté
			if($fgmembre) {
				if($fgdebug) echo "<ul>";
				
				// Parcourt de l'ensemble des profil LDAP de la communauté
				$q = "SELECT * FROM ".$config["dbprefixe"]."ldap_profil WHERE ldap_profil_ldap_community=?";
				$query02=$bdd02->prepare($q);
				$query02->execute(array($row01["ldap_community_id"]));
				while($row02=$query02->fetch()) {				
					$fgmembre=false;
					$lbfilter=str_replace("#login#",$_SESSION['user_login'],$row02["ldap_profil_filter"]);
					if($fgdebug) echo "<h4>".$row02["ldap_profil_label"]."</h4>Filtre Profil= ".$lbfilter;
					
					if($row02["ldap_profil_filter"]=="") {
						$fgmembre=true;
						if($fgdebug) echo "<br>appartient au profil = ".$row02["ldap_profil_label"];
					}
					else {			
						$res = ldap_search($ds,$config['LDAPracine'],$lbfilter);
						$ldapinfo = ldap_get_entries($ds, $res);
						if($ldapinfo["count"]>0) {
							$fgmembre=true;
							if($fgdebug) echo "<br>appartient au profil = ".$row02["ldap_profil_label"];
						}
					}
					
					// Il fait partie du profil
					if($fgmembre) {
						// Récupérer l'ensemble des catégories d'item associé au profil de l'utilisateur
							// 0 = categorie order
							// 1 = categorie id
							// 2 = categorie label
							// 3 = categorie color
				
						$q = "SELECT application_categorie_id, application_categorie_label, application_categorie_color FROM ".$config["dbprefixe"]."application_ldap_profil, ".$config["dbprefixe"]."application, ".$config["dbprefixe"]."icon, ".$config["dbprefixe"]."application_categorie
										WHERE application_ldap_profil_ldap_profil=?
										AND application_ldap_profil_application=application_id
										AND application_icon=icon_id
										AND application_categorie=application_categorie_id
										GROUP BY application_categorie_id
										ORDER BY application_categorie_order, application_order";
						$query03=$bdd03->prepare($q);
						$query03->execute(array($row02["ldap_profil_id"]));
						while($row03=$query03->fetch()) {	
							if($fgdebug) echo "<br>Catégorie item = ".$row03["application_categorie_label"];
							
							$tbtmp=array($row03["application_categorie_order"],$row03["application_categorie_id"],$row03["application_categorie_label"],$row03["application_categorie_color"]);
							if(!in_array($tbtmp,$tbcat)) {
								if($fgdebug) echo " ajoutée";
								array_push($tbcat,$tbtmp);
							}
						}
						
						// Récupérer l'ensemble des applications associé au profil de l'utilisateur
							// 0 = categorie order
							// 1 = application order
							// 2 = categorie id
							// 3 = application id
							// 4 = applcation label
							// 5 = application description
							// 6 = application url
							// 7 = icone url
							// 8 = application mode d'ouverture
							// 9 = application coleur
							
						$q = "SELECT * FROM ".$config["dbprefixe"]."application_ldap_profil, ".$config["dbprefixe"]."application, ".$config["dbprefixe"]."icon, ".$config["dbprefixe"]."application_categorie
										WHERE application_ldap_profil_ldap_profil=?
										AND application_ldap_profil_application=application_id
										AND application_icon=icon_id
										AND application_categorie=application_categorie_id
										ORDER BY application_categorie_order, application_order";
						$query03=$bdd03->prepare($q);
						$query03->execute(array($row02["ldap_profil_id"]));
						while($row03=$query03->fetch()) {	
							if($fgdebug) echo "<br>Item = ".$row03["application_label"];
							
							$tbtmp=array($row03["application_categorie_order"],$row03["application_order"],$row03["application_categorie"],$row03["application_id"],$row03["application_label"],$row03["application_description"],$row03["application_url"],$row03["icon_url"],$row03["application_open"],$row03["application_color"]);
							if(!in_array($tbtmp,$tbitem)) {
								if($fgdebug) echo " ajoutée";
								array_push($tbitem,$tbtmp);
							}							
						}						
						
						// Récupérer l'ensemble des pages associées au profil de l'utilisateur
							// 0 = page order
							// 1 = page id
							// 2 = page label
							
						$q = "SELECT * FROM ".$config["dbprefixe"]."panel_ldap_profil, ".$config["dbprefixe"]."panel
										WHERE panel_ldap_profil_ldap_profil=?
										AND panel_ldap_profil_panel=panel_id
										ORDER BY panel_order";
						$query03=$bdd03->prepare($q);
						$query03->execute(array($row02["ldap_profil_id"]));
						while($row03=$query03->fetch()) {
							if($fgdebug) echo "<br>Page = ".$row03["panel_label"];

							$tbtmp=array($row03["panel_order"],$row03["panel_id"],$row03["panel_label"]);
							if(!in_array($tbtmp,$tbpage)) {
								if($fgdebug) echo " ajoutée";
								array_push($tbpage,$tbtmp);
							}
						}
						
						// Récupérer l'ensemble des flux associés au profil de l'utilisateur
							// 0 = flux order
							// 1 = flux id
							// 2 = flux label
							// 3 = type
							// 4 = flux url
							// 5 = flux number
							// 6 = flux color
							// 7 = flux style
							
						$q = "SELECT * FROM ".$config["dbprefixe"]."flux_ldap_profil, ".$config["dbprefixe"]."flux
										WHERE flux_ldap_profil_profil=?
										AND flux_ldap_profil_flux=flux_id
										ORDER BY flux_order";
						$query03=$bdd03->prepare($q);
						$query03->execute(array($row02["ldap_profil_id"]));
						while($row03=$query03->fetch()) {
							if($fgdebug) echo "<br>Flux = ".$row03["flux_name"];
							
							$tbtmp=array($row03["flux_order"],$row03["flux_id"],$row03["flux_name"],$row03["flux_type"],$row03["flux_url"],$row03["flux_number"],$row03["flux_color"],$row03["flux_style"]);
							if(!in_array($tbtmp,$tbflux)) {
								if($fgdebug) echo " ajoutée";
								array_push($tbflux,$tbtmp);
							}							
						}						
					}
				}
				
				if($fgdebug) echo "</ul>";			
			}
		}
	}


//== PROFIL SSO ========================================================================================================================================	

	if($config["modeAuthentification"]=="CAS"&&$_SESSION['user_login']!="") {
		if($fgdebug) echo "<hr><h1>GESTION DES PROFIL SSO</h1>";
		
		// Parcours de l'ensemble des informations détaillées de l'utilisateur
		if($fgdebug) echo "<h2>LISTE DES ATTRIBUTS ASSOCIES</h2>";
		$user = EolephpCAS::getUser();
		$details = EolephpCAS::getDetails();
		$user_datas = $details;	
		
		if($fgdebug) echo "<ul>";
		
		foreach ($user_datas as $tbuser) {
			foreach ($tbuser as $key => $attribut) {
				foreach($attribut as $u) {
					if($fgdebug) echo "<br>$key = $u";
					
					// On recherche s'il exite un attribut parametré pour cette valeur
					$q = "SELECT * FROM ".$config["dbprefixe"]."sso_attribut WHERE sso_attribut_name=? AND sso_attribut_value=?";
					$query01=$bdd01->prepare($q);
					$query01->execute(array($key,$u));
					
					// Si oui on sauvegarde l'id de l'attribut
					if($row01=$query01->fetch()) {
						array_push($tbattrs,$row01['sso_attribut_id']);
						if($fgdebug) echo "<span style='color: red;font_width: strong;'> >> OK</span> >> ".$row01['sso_attribut_id'];
					}
				}
			}
		}

		if($fgdebug) echo "</ul>";
		
		// Déterminer dans quelles communauté appartient l'utilisateur
		$lstattrs=implode(",",$tbattrs);
		$q = "SELECT * FROM ".$config["dbprefixe"]."sso_community";
		$query01=$bdd01->prepare($q);
		$query01->execute();
		while($row01=$query01->fetch()) {
			$fgmembre=false;
			if($fgdebug) echo "<h2>".$row01["sso_community_label"]."</h2>";
			
			if($row01["sso_community_id"]==-1) {
				$fgmembre=true;
				if($fgdebug) echo "appartient à la communauté = ".$row01["sso_community_label"];
			}
			else {
				// Appartient à la communauté si au moins un id dans la liste des attributs
				$q = "SELECT * FROM ".$config["dbprefixe"]."sso_community_attribut
								   WHERE sso_community_attribut_community=?
								   AND sso_community_attribut_attribut in (?)";
				$query02=$bdd02->prepare($q);
				$query02->execute(array($row01["sso_community_id"],$lstattrs));
				if($row02=$query02->fetch()) {				
					$fgmembre=true;
					if($fgdebug) echo "appartient à la communauté = ".$row01["sso_community_label"];
				}
			}
			
			// Il fait partie de cette communauté
			if($fgmembre) {		
				// Appartient au profil si au moins un id dans la liste des attributs de cette communauté
				$q = "SELECT * FROM ".$config["dbprefixe"]."sso_profil, ".$config["dbprefixe"]."sso_profil_attribut
							       WHERE sso_profil_community=?
							       AND sso_profil_id = sso_profil_attribut_profil
							       AND sso_profil_attribut_attribut IN (?)";
				$query02=$bdd02->prepare($q);
				$query02->execute(array($row01["sso_community_id"],$lstattrs));

				// Il fait partie du profil
				if($row02=$query02->fetch()) {				
					if($fgdebug) echo "<h4>".$row02["sso_profil_label"]."</h4>";
					
					// Récupérer l'ensemble des catégories d'item associé au profil de l'utilisateur
						// 0 = categorie order
						// 1 = categorie id
						// 2 = categorie label
						// 3 = categorie color
			
					$q = "SELECT application_categorie_id, application_categorie_label, application_categorie_color FROM ".$config["dbprefixe"]."application_sso_profil, ".$config["dbprefixe"]."application, ".$config["dbprefixe"]."icon, ".$config["dbprefixe"]."application_categorie
									WHERE application_sso_profil_sso_profil=?
									AND application_sso_profil_application=application_id
									AND application_icon=icon_id
									AND application_categorie=application_categorie_id
									GROUP BY application_categorie_id
									ORDER BY application_categorie_order, application_order";
					$query03=$bdd03->prepare($q);
					$query03->execute(array($row02["sso_profil_id"]));
					while($row03=$query03->fetch()) {
						if($fgdebug) echo "<br>Catégorie item = ".$row03["application_categorie_label"];
						
						$tbtmp=array($row03["application_categorie_order"],$row03["application_categorie_id"],$row03["application_categorie_label"],$row03["application_categorie_color"]);
						if(!in_array($tbtmp,$tbcat)) {
							if($fgdebug) echo " ajoutée";
							array_push($tbcat,$tbtmp);
						}
					}
					
					// Récupérer l'ensemble des applications associé au profil de l'utilisateur
						// 0 = categorie order
						// 1 = application order
						// 2 = categorie id
						// 3 = application id
						// 4 = applcation label
						// 5 = application description
						// 6 = application url
						// 7 = icone url
						// 8 = application mode d'ouverture
						// 9 = application couleur
						
					$q = "SELECT * FROM ".$config["dbprefixe"]."application_sso_profil, ".$config["dbprefixe"]."application, ".$config["dbprefixe"]."icon, ".$config["dbprefixe"]."application_categorie
									WHERE application_sso_profil_sso_profil=?
									AND application_sso_profil_application=application_id
									AND application_icon=icon_id
									AND application_categorie=application_categorie_id
									ORDER BY application_categorie_order, application_order";
					$query03=$bdd03->prepare($q);
					$query03->execute(array($row02["sso_profil_id"]));
					while($row03=$query03->fetch()) {
						if($fgdebug) echo "<br>Item = ".$row03["application_label"];
						
						$tbtmp=array($row03["application_categorie_order"],$row03["application_order"],$row03["application_categorie"],$row03["application_id"],$row03["application_label"],$row03["application_description"],$row03["application_url"],$row03["icon_url"],$row03["application_open"],$row03["application_color"]);
						if(!in_array($tbtmp,$tbitem)) {
							if($fgdebug) echo " ajoutée";
							array_push($tbitem,$tbtmp);
						}							
					}						
					
					// Récupérer l'ensemble des pages associées au profil de l'utilisateur
						// 0 = page order
						// 1 = page id
						// 2 = page label
						
					$q = "SELECT * FROM ".$config["dbprefixe"]."panel_sso_profil, ".$config["dbprefixe"]."panel
									WHERE panel_sso_profil_sso_profil=?
									AND panel_sso_profil_panel=panel_id
									ORDER BY panel_order";
					$query03=$bdd03->prepare($q);
					$query03->execute(array($row02["sso_profil_id"]));
					while($row03=$query03->fetch()) {
						if($fgdebug) echo "<br>Page = ".$row03["panel_label"];

						$tbtmp=array($row03["panel_order"],$row03["panel_id"],$row03["panel_label"]);
						if(!in_array($tbtmp,$tbpage)) {
							if($fgdebug) echo " ajoutée";
							array_push($tbpage,$tbtmp);
						}
					}
					
					// Récupérer l'ensemble des flux associés au profil de l'utilisateur
						// 0 = flux order
						// 1 = flux id
						// 2 = flux label
						// 3 = type
						// 4 = flux url
						// 5 = flux number
						// 6 = flux color
						// 7 = flux style
						
					$q = "SELECT * FROM ".$config["dbprefixe"]."flux_sso_profil, ".$config["dbprefixe"]."flux
									WHERE flux_sso_profil_profil=?
									AND flux_sso_profil_flux=flux_id
									ORDER BY flux_order";
					$query03=$bdd03->prepare($q);
					$query03->execute(array($row02["sso_profil_id"]));
					while($row03=$query03->fetch()) {
						if($fgdebug) echo "<br>Flux = ".$row03["flux_name"];
						
						$tbtmp=array($row03["flux_order"],$row03["flux_id"],$row03["flux_name"],$row03["flux_type"],$row03["flux_url"],$row03["flux_number"],$row03["flux_color"],$row03["flux_style"]);
						if(!in_array($tbtmp,$tbflux)) {
							if($fgdebug) echo " ajoutée";
							array_push($tbflux,$tbtmp);
						}							
					}	
				}	
			}
		}
	}
	
	
	
	
	
	// Trie des Tableaux
	foreach ($tbitem as $key => $rowd) {
	$indice[$key] = $rowd[1];
	$libelle[$key] = $rowd[4];
	}
	array_multisort($indice, SORT_ASC, $libelle, SORT_ASC, $tbitem);
	
	foreach ($tbflux as $key => $rowd) {
	$indice[$key] = $rowd[0];
	$libelle[$key] = $rowd[2];
	}
	array_multisort($indice, SORT_ASC, $libelle, SORT_ASC, $tbflux);		

	foreach ($tbpage as $key => $rowd) {
	$indice[$key] = $rowd[0];
	$libelle[$key] = $rowd[2];
	}
	array_multisort($indice, SORT_ASC, $libelle, SORT_ASC, $tbpage);	
		
	asort($tbcat);


	//if($fgdebug) die();	

?>

