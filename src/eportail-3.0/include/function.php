<?
	function autocreate($login,$password,$firstname,$lastname,$email) {
		global $config;
		global $bdd01;
		
		$query=$bdd01->prepare("SELECT * FROM ".$config["dbprefixe"]."user WHERE user_login=?");
		$query->execute(array($login));
		if($row=$query->fetch())
		{
			if($config['modeAuthentification']=="CAS") {
				if($firstname!=$row["user_firstname"]||$lastname!=$row["user_lastname"]||$email!=$row["user_email"]) {
					$query=$bdd01->prepare("UPDATE ".$config["dbprefixe"]."user SET  user_password=md5(?), user_lastname=?, user_firstname= ?, user_email=? WHERE user_id =".$row["user_id"]);
					$query->execute(array($password,$firstname,$lastname,$email));
				}
			}
			else {
				if(md5($password)!=$row["password"]||$firstname!=$row["user_firstname"]||$lastname!=$row["user_lastname"]||$email!=$row["user_email"]) {
					$query=$bdd01->prepare("UPDATE ".$config["dbprefixe"]."user SET  user_password=md5(?), user_lastname=?, user_firstname= ?, user_email=? WHERE user_id =".$row["user_id"]);
					$query->execute(array($password,$firstname,$lastname,$email));
				}
			}
		}
		else {
			$query=$bdd01->prepare("INSERT INTO ".$config["dbprefixe"]."user (user_login, user_password, user_lastname, user_firstname, user_email, user_profil) VALUES(?,md5(?),?,?,?,?)");
			$query->execute(array($login,$password,$firstname,$lastname,$email,50));
		}
	}
	
	function savesession($row,$login,$password,$remember) {
		global $config;
		
		$_SESSION['user_id']			=$row["user_id"];
		$_SESSION['user_login']			=$row["user_login"];
		$_SESSION['user_firstname']		=$row["user_firstname"];
		$_SESSION['user_lastname']		=$row["user_lastname"];
		$_SESSION['user_pseudo']		=$row["user_pseudo"];
		$_SESSION['user_avatar']		=$row["user_avatar"];
		$_SESSION['user_profil'] 		=$row["user_profil"];
		
		$ssdomaine=str_replace($config["host"],"",$config["urlbase"]);
		$ssdomaine=str_replace("https://","",$ssdomaine);
		$ssdomaine=str_replace("http://","",$ssdomaine);
		
		if ($remember) {
			setcookie('username', $login, time()+60*60*24*365, $ssdomaine, $config["host"]);
			setcookie('password', md5($password), time()+60*60*24*365, $ssdomaine, $config["host"]);
			setcookie('remember', "true", time()+60*60*24*365, $ssdomaine, $config["host"]);
			setcookie('fgremember', "true", time()+60*60*24*365, $ssdomaine, $config["host"]);
		}
		else {
			setcookie('username', "", false, $ssdomaine, $config["host"]);
			setcookie('password', "", false, $ssdomaine, $config["host"]);
			setcookie('remember', "", false, $ssdomaine, $config["host"]);
			setcookie('fgremember', "", false, $ssdomaine, $config["host"]);
		}
	}
	
	function generateRandomString($length = 10) {
		return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
	}
?>
