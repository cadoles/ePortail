<?
	include_once("include/config.php");
	include_once("include/mysql.php");
	include_once("include/session.php");
	
	session_destroy();
	$_SESSION['user_id'] = "";

	$ssdomaine=str_replace($config["host"],"",$config["urlbase"]);
	$ssdomaine=str_replace("https://","",$ssdomaine);
	$ssdomaine=str_replace("http://","",$ssdomaine);
	
	setcookie('remember', "", false, $ssdomaine, $config["host"]);

	if ($config['modeAuthentification']=="CAS") {
		include("include/CASlogout.php");
	}
	
	header('Location: index.php');
?>
