<?php
	include_once("include/include.php");

	if(!isset($_SESSION[$keymodal])) {
		header("location:index.php?view=diened.php");
		exit();
	}

	// Init des nom aléatoires
	if (!isset($_POST["upload"])&&$_POST["upload_thumbnail"]==""&&$_POST["fgval"]==""&&$_POST["fgdel"]==""){
		$_SESSION[$keymodal]['random_key'] = strtotime(date('Y-m-d H:i:s')); //assign the timestamp to the session variable
		$_SESSION[$keymodal]['user_file_ext']= "";
	}
	

	$upload_dir	=$_SESSION[$keymodal]['modal-dirimg']; 	// Répertoire de stockage de l'image
	$igico		=$_SESSION[$keymodal]['modal-igico'];  	// id de la balise img de destination
	$lbico		=$_SESSION[$keymodal]['modal-lbico'];	// id de la balise input qui stocke l'url de l'image
	$idquery	=$_SESSION[$keymodal]['modal-idquery'];	// id lié à l'action query associé
	$refresh	=$_SESSION[$keymodal]['modal-refresh'];	// flag de refresh du parent

	$upload_path 			= $upload_dir."/";											// The path to where the image will be saved
	$large_image_prefix 	= "resize_";												// The prefix name to large image
	$thumb_image_prefix 	= "thumbnail_";												// The prefix name to the thumb image
	$large_image_name 		= $large_image_prefix.$_SESSION[$keymodal]['random_key']; 	// New name of the large image (append the timestamp to the filename)
	$thumb_image_name 		= $thumb_image_prefix.$_SESSION[$keymodal]['random_key']; 	// New name of the thumbnail image (append the timestamp to the filename)
	$max_file 				= "3"; 														// Maximum file size in MB
	$max_height				= "400";													// Max height allowed for the large image
	$max_width				= "590";													// Max width allowed for the large image
	$thumb_width 			= "90";														// Width of thumbnail image
	$thumb_height 			= "90";														// Height of thumbnail image

	// Format d'image autorisée
	$allowed_image_types = array('image/pjpeg'=>"jpg",'image/jpeg'=>"jpg",'image/jpg'=>"jpg",'image/png'=>"png",'image/x-png'=>"png",'image/gif'=>"gif");
	$allowed_image_ext = array_unique($allowed_image_types);
	$image_ext = "";
	foreach ($allowed_image_ext as $mime_type => $ext) {
		$image_ext.= strtoupper($ext)." ";
	}


//==RESIZEIMAGES=======================================================================================================================================
	function resizeImage($image,$width,$height,$scale) {
		list($imagewidth, $imageheight, $imageType) = getimagesize($image);
		$imageType = image_type_to_mime_type($imageType);
		$newImageWidth = ceil($width * $scale);
		$newImageHeight = ceil($height * $scale);
		$newImage = imagecreatetruecolor($newImageWidth,$newImageHeight);
		switch($imageType) {
			case "image/gif":
				$source=imagecreatefromgif($image); 
				break;
			case "image/pjpeg":
			case "image/jpeg":
			case "image/jpg":
				$source=imagecreatefromjpeg($image); 
				break;
			case "image/png":
			case "image/x-png":
				$source=imagecreatefrompng($image); 
				break;
		}
		imagecopyresampled($newImage,$source,0,0,0,0,$newImageWidth,$newImageHeight,$width,$height);
		
		switch($imageType) {
			case "image/gif":
				imagegif($newImage,$image); 
				break;
			case "image/pjpeg":
			case "image/jpeg":
			case "image/jpg":
				imagejpeg($newImage,$image,90); 
				break;
			case "image/png":
			case "image/x-png":
				imagepng($newImage,$image);  
				break;
		}
		
		chmod($image, 0777);
		return $image;
	}
	
//==RESIZETHUMNAILIMAGE================================================================================================================================

	function resizeThumbnailImage($thumb_image_name, $image, $width, $height, $start_width, $start_height, $scale){
		list($imagewidth, $imageheight, $imageType) = getimagesize($image);
		$imageType = image_type_to_mime_type($imageType);
		
		$newImageWidth = ceil($width * $scale);
		$newImageHeight = ceil($height * $scale);
		$newImage = imagecreatetruecolor($newImageWidth,$newImageHeight);
		switch($imageType) {
			case "image/gif":
				$source=imagecreatefromgif($image); 
				break;
			case "image/pjpeg":
			case "image/jpeg":
			case "image/jpg":
				$source=imagecreatefromjpeg($image); 
				break;
			case "image/png":
			case "image/x-png":
				$source=imagecreatefrompng($image); 
				break;
		}
		imagecopyresampled($newImage,$source,0,0,$start_width,$start_height,$newImageWidth,$newImageHeight,$width,$height);
		switch($imageType) {
			case "image/gif":
				imagegif($newImage,$thumb_image_name); 
				break;
			case "image/pjpeg":
			case "image/jpeg":
			case "image/jpg":
				imagejpeg($newImage,$thumb_image_name,90); 
				break;
			case "image/png":
			case "image/x-png":
				imagepng($newImage,$thumb_image_name);  
				break;
		}
		chmod($thumb_image_name, 0777);
		return $thumb_image_name;
	}

	// Calcul de la hauteur
	function getHeight($image) {
		$size = getimagesize($image);
		$height = $size[1];
		return $height;
	}

	// Cacul de la largeur
	function getWidth($image) {
		$size = getimagesize($image);
		$width = $size[0];
		return $width;
	}



//==UPLOAD IMAGE=======================================================================================================================================
	if (isset($_POST["upload"])) { 
		//Get the file information
		$userfile_name = $_FILES['image']['name'];
		$userfile_tmp = $_FILES['image']['tmp_name'];
		$userfile_size = $_FILES['image']['size'];
		$userfile_type = $_FILES['image']['type'];
		$filename = basename($_FILES['image']['name']);
		$file_ext = strtolower(substr($filename, strrpos($filename, '.') + 1));
		
		//Only process if the file is a JPG, PNG or GIF and below the allowed limit
		if((!empty($_FILES["image"])) && ($_FILES['image']['error'] == 0)) {
			
			foreach ($allowed_image_types as $mime_type => $ext) {
				//loop through the specified image types and if they match the extension then break out
				//everything is ok so go and check file size
				if($file_ext==$ext && $userfile_type==$mime_type){
					$error = "";
					break;
				}else{
					$error = "Seule les images aux formats suivant sont acceptées <strong>".$image_ext."</strong><br />";
				}
			}
			//check if the file size is above the allowed limit
			if ($userfile_size > ($max_file*1048576)) {
				$error.= "L'image doit être d'une taille inférieure à ".$max_file."MB";
			}
			
		}else{
			$error= "Veuillez sélectionner une image";
		}
		//Everything is ok, so we can upload the image.
		if (strlen($error)==0){
			
			if (isset($_FILES['image']['name'])){
				//this file could now has an unknown file extension (we hope it's one of the ones set above!)
				$large_image_location = $upload_path.$large_image_name.".".$file_ext;
				$thumb_image_location = $upload_path.$thumb_image_name.".".$file_ext;
				
				//put the file ext in the session so we know what file to look for once its uploaded
				$_SESSION[$keymodal]['user_file_ext']=".".$file_ext;
				
				move_uploaded_file($userfile_tmp, $large_image_location);
				chmod($large_image_location, 0777);
				
				$width = getWidth($large_image_location);
				$height = getHeight($large_image_location);
				
				//Scale the image if it is greater than the width set above
				$scale = $max_height/$height;
				if(($width*$scale)>$max_width) {
					$scale = $max_width/$width;
				}
				$uploaded = resizeImage($large_image_location,$width,$height,$scale);
				
				//Delete the thumbnail file so the user can create a new one
				if (file_exists($thumb_image_location)) {
					unlink($thumb_image_location);
				}
			}
		}
	}

//==CROP IMAGE=========================================================================================================================================
	if (isset($_POST["upload_thumbnail"])) {
		$large_image_location = $upload_path.$large_image_name.$_SESSION[$keymodal]['user_file_ext'];
		$thumb_image_location = $upload_path.$thumb_image_name.$_SESSION[$keymodal]['user_file_ext'];
			
		//Get the new coordinates to crop the image.
		$x1 = $_POST["x1"];
		$y1 = $_POST["y1"];
		$x2 = $_POST["x2"];
		$y2 = $_POST["y2"];
		$w = $_POST["w"];
		$h = $_POST["h"];
		
		//Scale the image to the thumb_width set above
		$scale = $thumb_width/$w;
		$cropped = resizeThumbnailImage($thumb_image_location, $large_image_location,$w,$h,$x1,$y1,$scale);
	}


//==VALIDATION=========================================================================================================================================
	if ($_POST['fgval']!=""){
		$large_image_location = $upload_path.$large_image_prefix.$_SESSION[$keymodal]['random_key'].$_SESSION[$keymodal]['user_file_ext'];
		$thumb_image_location = $upload_path.$thumb_image_prefix.$_SESSION[$keymodal]['random_key'].$_SESSION[$keymodal]['user_file_ext'];
		if (file_exists($large_image_location)) {
			unlink($large_image_location);
		}
		
		$jsaction="";
		if($lbico!="") $jsaction=$jsaction." $('$lbico',parent.document).val('".$thumb_image_prefix.$_SESSION[$keymodal]['random_key'].$_SESSION[$keymodal]['user_file_ext']."');";
		if($igico!="") $jsaction=$jsaction." $('$igico',parent.document).attr('src','$thumb_image_location');";
		
		
		if($_SESSION[$keymodal]["type"]=="icon-cropinsert") {
			$q="INSERT INTO ".$config["dbprefixe"]."icon (icon_url) VALUES(?)";
			$tbq=array($thumb_image_prefix.$_SESSION[$keymodal]['random_key'].$_SESSION[$keymodal]['user_file_ext']);
			$query=$bdd01->prepare($q);
			$query->execute($tbq);			
		}
		elseif($_SESSION[$keymodal]["type"]=="icon-cropupdate") {
			$q="UPDATE ".$config["dbprefixe"]."icon SET icon_url=? WHERE icon_id=?";
			$tbq=array($thumb_image_prefix.$_SESSION[$keymodal]['random_key'].$_SESSION[$keymodal]['user_file_ext'],$idquery);
			$query=$bdd01->prepare($q);
			$query->execute($tbq);			
		}
		

		if($refresh!="") $jsaction=$jsaction." window.parent.document.location.reload(); ";
		$jsaction=$jsaction." window.parent.ModalClose('#".$_SESSION[$keymodal]["idmodal"]."');";
		unset($_SESSION[$keymodal]);
	}

//==ANNULATION========================================================================================================================================
	if ($_POST['fgdel']!=""){
		//get the file locations 
		$large_image_location = $upload_path.$large_image_prefix.$_SESSION[$keymodal]['random_key'].$_SESSION[$keymodal]['user_file_ext'];
		$thumb_image_location = $upload_path.$thumb_image_prefix.$_SESSION[$keymodal]['random_key'].$_SESSION[$keymodal]['user_file_ext'];
		if (file_exists($large_image_location)) {
			unlink($large_image_location);
		}
		if (file_exists($thumb_image_location)) {
			unlink($thumb_image_location);
		}

		$jsaction="window.parent.ModalClose('#".$_SESSION[$keymodal]["idmodal"]."');";
		unset($_SESSION[$keymodal]);
	}

//==VERIFIER L'EXISTANCE DES IMAGES===================================================================================================================	
	// Localisation de l'image
	$large_image_location = $upload_path.$large_image_name.$_SESSION[$keymodal]['user_file_ext'];
	$thumb_image_location = $upload_path.$thumb_image_name.$_SESSION[$keymodal]['user_file_ext'];
	
	// Vérifier l'existance d'une image existe ou non
	if (file_exists($large_image_location)){
		if(file_exists($thumb_image_location)){
			$thumb_photo_exists = "<img src=\"".$upload_path.$thumb_image_name.$_SESSION[$keymodal]['user_file_ext']."\" alt=\"Thumbnail Image\"/>";
		}else{
			$thumb_photo_exists = "";
		}
		
		$large_photo_exists = "<img src=\"".$upload_path.$large_image_name.$_SESSION[$keymodal]['user_file_ext']."\" alt=\"Large Image\"/>";
	} else {
		$large_photo_exists = "";
		$thumb_photo_exists = "";
	}	
?>	

	<script type="text/javascript" src="lib/jcrop/jquery-pack.js"></script>
	<script type="text/javascript" src="lib/jcrop/jquery.imgareaselect.min.js"></script>
	
	<script type="text/JavaScript">
		<?php echo $jsaction ?>
	</script>	




<body class="bodymodal">
	
<?php
	// Script javascript uniquement si présence d'une imgage téléchargée
	if(strlen($large_photo_exists)>0){
		$current_large_image_width = getWidth($large_image_location);
		$current_large_image_height = getHeight($large_image_location);
?>

<script type="text/javascript">
	function preview(img, selection) { 
		var scaleX = <?php echo $thumb_width;?> / selection.width; 
		var scaleY = <?php echo $thumb_height;?> / selection.height; 
		
		//$('#thumbnail + div > img').css({ 
		$('#preview img').css({
			width: Math.round(scaleX * <?php echo $current_large_image_width;?>) + 'px', 
			height: Math.round(scaleY * <?php echo $current_large_image_height;?>) + 'px',
			marginLeft: '-' + Math.round(scaleX * selection.x1) + 'px', 
			marginTop: '-' + Math.round(scaleY * selection.y1) + 'px' 
		});
		$('#x1').val(selection.x1);
		$('#y1').val(selection.y1);
		$('#x2').val(selection.x2);
		$('#y2').val(selection.y2);
		$('#w').val(selection.width);
		$('#h').val(selection.height);
	} 

	$(document).ready(function () { 
		$('#save_thumb').click(function() {
			var x1 = $('#x1').val();
			var y1 = $('#y1').val();
			var x2 = $('#x2').val();
			var y2 = $('#y2').val();
			var w = $('#w').val();
			var h = $('#h').val();
			if(x1=="" || y1=="" || x2=="" || y2=="" || w=="" || h==""){
				alert("You must make a selection first");
				return false;
			}else{
				return true;
			}
		});
	}); 

	$(window).load(function () { 
		$('#thumbnail').imgAreaSelect({ aspectRatio: '1:<?php echo $thumb_height/$thumb_width;?>', onSelectChange: preview }); 
	});

</script>
<?php }?>


<?php

	if($large_photo_exists==""){
		echo "<form name='photo' class='form-horizontal' role='form' method='post' enctype='multipart/form-data'>";
		echo "<h4>ETAPE 1 - TELECHARGEZ UNE IMAGE</h4>";
		
		echo "<label for='image' class='control-label'>Image</label>";
		echo "<input name='image' id='image' type='file'>";
		echo "<br>";

		echo "<input name='fgdel' type='submit' class='btn btn-primary' value='Annuler'>";
		echo "&nbsp;";
		echo "<input name='upload' id='upload' type='submit' class='btn btn-primary' value='Télécharger'>";

		echo "</form>";
	}
	else {
		if(strlen($large_photo_exists)>0 && $thumb_photo_exists==""){
			echo "<form name='thumbnail' class='form-horizontal' role='form' method='post' enctype='multipart/form-data'>";
			
			echo "<div id='preview' style='float:left; position:relative; overflow:hidden; width:".$thumb_width."px; height:".$thumb_height."px; margin-right:10px;margin-bottom:10px;'>";
			echo "<img src='".$upload_path.$large_image_name.$_SESSION[$keymodal]['user_file_ext']."' style='position: relative;' alt='Thumbnail Preview' />";
			echo "</div>";
			
			echo "<h4>ETAPE 2 - CREATION DE LA MINIATURE</h4>";

			echo "<input type='hidden' name='x1' value='' id='x1' />";
			echo "<input type='hidden' name='y1' value='' id='y1' />";
			echo "<input type='hidden' name='x2' value='' id='x2' />";
			echo "<input type='hidden' name='y2' value='' id='y2' />";
			echo "<input type='hidden' name='w'  value='' id='w'  />";
			echo "<input type='hidden' name='h'  value='' id='h'  />";

			echo "<input name='fgdel' type='submit' class='btn btn-primary' value='Annuler'>";
			echo "&nbsp;";			
			echo "<input id='save_thumb' name='upload_thumbnail' type='submit' class='btn btn-primary' value='Générer la miniature'>";

			echo "<img src='".$upload_path.$large_image_name.$_SESSION[$keymodal]['user_file_ext']."' style='float: left; margin-right: 10px; clear:both;' id='thumbnail' alt='Create Thumbnail' />";

			echo "</div>";
			echo "</form>";
		}
		else {
			echo "<form name='thumbvalidation' class='form-horizontal' role='form' method='post' enctype='multipart/form-data'>";
			echo "<div id='preview' style='float:left; position:relative; overflow:hidden; width:".$thumb_width."px; height:".$thumb_height."px; margin-right:10px;margin-bottom:10px;'>";
			echo $thumb_photo_exists;
			echo "</div>";		
		
			echo "<input type='hidden' name='a' value='' id='a' />";
			
			echo "<h4>ETAPE 3 - CONFIRMATION</h4>";

			echo "<input name='fgdel' type='submit' class='btn btn-primary' value='Annuler'>";
			echo "&nbsp;";
			echo "<input name='fgval' type='submit' class='btn btn-primary' value='Valider'>";
			
			echo "<br><br>";
			
			echo "<img src='".$upload_path.$large_image_name.$_SESSION[$keymodal]['user_file_ext']."' style='float: left; margin-right: 10px; clear:both;' />";
			echo "</form>";
		}
	}
		

	// Afficher l'erreur
	if(strlen($error)>0){
		echo "<br><strong>Erreur : </strong><br>".$error."<br><br>";
	}


	include("footer.php");
?>

