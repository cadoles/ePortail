<?
	include_once("include/include.php");

	if(!isset($_SESSION[$keymodal])) {
		header("location:index.php?view=diened.php");
		exit();
	}
	
	
	// Récupération des paramétres
	$directory		= $_SESSION[$keymodal]["modal-directory"];		// cible du upload
	$typeupload		= $_SESSION[$keymodal]["modal-typeupload"];		// image | all 
	$multiupload	= $_SESSION[$keymodal]["modal-multiupload"];	// true | false 
	$resize			= $_SESSION[$keymodal]["modal-resize"];			// true | false uniquement si typeupload = image
	$size			= $_SESSION[$keymodal]["modal-size"];			// taille en px pour le rezise d'une image
	$rename			= $_SESSION[$keymodal]["modal-rename"];			// true | false renommage des fichiers uploadés
	$prefix			= $_SESSION[$keymodal]["modal-prefix"];			// prefix des fichiers renommées 
	
	$refresh		= $_SESSION[$keymodal]["modal-refresh"];		// Rafraichir le père fermeture
	$idquery		= $_SESSION[$keymodal]["modal-idquery"];		// identifiant unique associé à la query
	
	$idimage		= $_SESSION[$keymodal]["modal-idimage"];		// id html de l'image à afficher
	$idinput		= $_SESSION[$keymodal]["modal-idinput"];		// id html de l'input de stockage
	
	// Valeur par défaut
	if($directory=="")		$directory="local/upload";
	if($typeupload=="")		$typeupload="all";
	if($multiupload=="")	$multiupload=false;
	if($resize=="")			$resize=false;
	if($size=="")			$size="";
	if($rename=="")			$rename=true;
	if($prefix=="")			$prefix="file_";
	if($idquery=="")		$idquery="";
	
	// Definition de l'url action
	//$action = "js/plugins/dropzone/upload.php?directory=$directory&typeupload=$typeupload&multiupload=$multiupload&resize=$resize&size=$size&rename=$rename&prefix=$prefix&query=$query&idquery=$idquery";
	$action = "lib/dropzone/upload.php?key=$keymodal";
?>
<link href="lib/dropzone/css/dropzone.css" type="text/css" rel="stylesheet" />

<form action="<? echo $action; ?>" class="dropzone" id="MyDropZone"></form>

	
<script src="lib/dropzone/dropzone.min.js"></script>

<script>
Dropzone.options.MyDropZone = {
	<? if(!$multiupload) { ?>
	maxFiles: 1,
	<? } ?>
	
	<? if($typeupload=="image") { ?>
	acceptedMimeTypes: 'image/*',
	<? } ?>
	
	success: function(file, response){
		<? if($refresh) { ?>
		window.parent.document.location.reload();
		<? } ?>

		<? if($idimage!="") { ?>
		window.parent.document.getElementById("<? echo $idimage; ?>").src="<? echo $directory; ?>/"+response['name'];
		<? } ?>

		<? if($idinput!="") { ?>
		window.parent.document.getElementById("<? echo $idinput ?>").value=response['name'];
		<? } ?>

		window.parent.ModalClose('#<? echo $_SESSION[$keymodal]["idmodal"] ?>');
		<? //unset($_SESSION[$keymodal]); ?>
		
	}
};

</script>
