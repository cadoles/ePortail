<? 
	$levelpage=1;
	include("include/permission.php");
	
	$fgval							=$_POST["fgval"];
	$fgdef							=$_POST["fgdef"];
	
	$title							=$_POST["title"];
	$sstitle						=$_POST["sstitle"];
	$modePrivate					=$_POST["modePrivate"];
	$modeAuthentification			=$_POST["modeAuthentification"];
	$modeRemember					=$_POST["modeRemember"];
	$modeCustomize					=$_POST["modeCustomize"];
	$theme    						=$_POST["theme"];
	$locallogo						=$_POST["locallogo"];
	$localheader					=$_POST["localheader"];
	$modeRegistration				=$_POST["modeRegistration"];
	$modeRegistrationFlagExterne	=$_POST["modeRegistrationFlagExterne"];
	$modeRegistrationExterne		=$_POST["modeRegistrationExterne"];
	$modeProfilExterne				=$_POST["modeProfilExterne"];
	$modeAvatarExterne				=$_POST["modeAvatarExterne"];
	$modeRegistrationConfirm		=$_POST["modeRegistrationConfirm"];
	
	if($fgval!="") {
		if($_FILES['filelocallogo']['tmp_name']!="") {
			$destination = "local/images/logo/logo.png";
			$resultat    = move_uploaded_file($_FILES['filelocallogo']['tmp_name'],$destination);
			if($resultat!=1) {
				echo "<script>alert('Le téléchargement du logo a échoué');</script>";
			}
		}
		
		if($_FILES['filelocalheader']['tmp_name']!="") {
			$destination = $repository."local/images/header/header.png";
			$resultat    = move_uploaded_file($_FILES['filelocalheader']['tmp_name'],$destination);
			if($resultat!=1) {
				echo "<script>alert('Le téléchargement de la bannière a échoué');</script>";
			}
		}
		
		if($modeRegistration=='true'&&$modeRegistrationFlagExterne=='false'&&$config["activerLDAP"]=='true'&&($config["LDAPwriterdn"]==""||$config["LDAPwriterpw"]=="")) {
			echo "<script>alert('Vous devez spécifier un compte LDAP writer si vous voulez activer le module inscription avec quand vous activez LDAP');</script>";
			$fgerreur=true;
		}

		if($modeRegistration=='true'&&$modeRegistrationFlagExterne=='true'&&$modeAuthentification!='CAS'&&($modeProfilExterne==""||$modeRegistrationExterne==""||$modeAvatarExterne=="")) {
			echo "<script>alert('En activant le module d\'inscription externe vous devez spécifer les URL associées');</script>";
			$fgerreur=true;
		}
		
		if(!$fgerreur) {
			if($modeRegistrationFlagExterne=='false') {
				$modeRegistrationExterne		= "";
				$modeProfilExterne				= "";
				$modeAvatarExterne				= "";
			}
			
			$fp=fopen($config['localdirectory']."/local/config/config.php","w");
			fwrite($fp, "<?\n");
			fwrite($fp, "\$config['title']                       = '".addslashes($title)."';\n");
			fwrite($fp, "\$config['sstitle']                     = '".addslashes($sstitle)."';\n");
			fwrite($fp, "\$config['modePrivate']                 = '$modePrivate';\n");
			fwrite($fp, "\$config['modeAuthentification']        = '$modeAuthentification';\n");
			fwrite($fp, "\$config['modeRemember']                = '$modeRemember';\n");
			fwrite($fp, "\$config['modeCustomize']               = '$modeCustomize';\n");
			fwrite($fp, "\$config['theme']                       = '$theme';\n");
			fwrite($fp, "\$config['locallogo']                   = '$locallogo';\n");
			fwrite($fp, "\$config['localheader']                 = '$localheader';\n");
			fwrite($fp, "\$config['modeRegistration']            = '$modeRegistration';\n");
			fwrite($fp, "\$config['modeRegistrationExterne']     = '$modeRegistrationExterne';\n");
			fwrite($fp, "\$config['modeProfilExterne']           = '$modeProfilExterne';\n");
			fwrite($fp, "\$config['modeAvatarExterne']           = '$modeAvatarExterne';\n");
			fwrite($fp, "\$config['modeRegistrationConfirm']     = '$modeRegistrationConfirm';\n");

			fwrite($fp, "\n?>");
			fclose($fp);
			
			// Reload de la page
			if($modeAuthentification!=$config['modeAuthentification']) {
				//echo "<script>alert('Changement du mode d\'authentification. Veuillez-vous reconnecter.');</script>";
				header('Location: logout.php');
				echo "<script>parent.document.location.reload();</script>";
			}
			else {
				echo "<script>parent.document.location.reload();</script>";
			}
		}
	}
	
	if($fgdef!="") {
		unlink($config['localdirectory']."/local/config/config.php");
		
		// Reload de la page
		echo "<script>parent.document.location.reload();</script>";		
	}
?>


<form class="form-horizontal" role="form" method='post' enctype="multipart/form-data">
	<legend><h1>CONFIGURATION GENERALE</h1></legend>
	<input type="hidden" name="MAX_FILE_SIZE" value="2097152">
	
	<div class="form-group">
		<div class="col-sm-12">
			<input name="fgval" type="submit" class="btn btn-primary" value="Enregistrer">
			&nbsp;
			<input name="fgdef" type="submit" class="btn btn-primary" value="Rétablir Configuration par Défaut">
		</div>
	</div>

	<div class="form-group">
		<label for="title" class="col-sm-3 control-label">Titre</label>
		<div class="col-sm-6">
			<input name="title" id="title" type="titre" class="form-control" placeholder="Titre" value="<? echo $config['title']; ?>">
		</div>
	</div>

	<div class="form-group">
		<label for="sstitle" class="col-sm-3 control-label">Sous Titre</label>
		<div class="col-sm-6">
			<input name="sstitle" id="sstitle" type="titre" class="form-control" placeholder="Sous Titre" value="<? echo $config['sstitle']; ?>">
		</div>
	</div>

	<div class="form-group">
		<label for="modePrivate" class="col-sm-3 control-label">Portail Privé</label>
		<div class="col-sm-6">
			<select name="modePrivate" id="modePrivate" class="form-control">
				<option value="false" <? if($config['modePrivate']=='false')  echo 'selected';?> >non</option>
				<option value="true"  <? if($config['modePrivate']=='true')   echo 'selected';?> >oui</option>
			</select>			
		</div>
	</div>

	<div class="form-group">
		<label for="modeCustomize" class="col-sm-3 control-label">Portail Personalisable</label>
		<div class="col-sm-6">
			<select name="modeCustomize" id="modeCustomize" class="form-control">
				<option value="false" <? if($config['modeCustomize']=='false')  echo 'selected';?> >non</option>
				<option value="true"  <? if($config['modeCustomize']=='true')   echo 'selected';?> >oui</option>
			</select>			
		</div>
	</div>
		
	<div class="form-group">
		<label for="modeAuthentification" class="col-sm-3 control-label">Mode d'Authentification</label>
		<div class="col-sm-6">
			<select name="modeAuthentification" id="modeAuthentification" class="form-control" onChange="ShowHide();">
				<option value="MYSQL" <? if($config['modeAuthentification']=='MYSQL')  echo 'selected';?> >MYSQL</option>
				<option value="CAS"   <? if($config['modeAuthentification']=='CAS')    echo 'selected';?> >CAS</option>
				<? if($config['activerLDAP']=="true") { ?><option value="LDAP"  <? if($config['modeAuthentification']=='LDAP')   echo 'selected';?> >LDAP</option> <? } ?>
			</select>			
		</div>
	</div>	

	<div id="form-group-remember" class="form-group">
		<label for="modeRemember" class="col-sm-3 control-label">Garder les sessions actives</label>
		<div class="col-sm-6">
			<select name="modeRemember" id="modeRemember" class="form-control">
				<option value="false" <? if($config['modeRemember']=='false')  echo 'selected';?> >non</option>
				<option value="true"  <? if($config['modeRemember']=='true')   echo 'selected';?> >oui</option>
			</select>			
		</div>
	</div>	
	
	<div class="form-group">
		<label for="theme" class="col-sm-3 control-label">Thème</label>
		<div class="col-sm-6">
			<select name="theme" id="theme" class="form-control">
				<option value="" <? if($config['theme']=="")   echo 'selected';?> >Thème de base</option>
				<?
					if ($handle = opendir($config['localdirectory'].'/style/themes/')) {
						while (false !== ($entry = readdir($handle))) {
							if ($entry != "." && $entry != ".." && $entry!="readme.txt") {
								$lbsel="";
								if($entry==$config['theme']) $lbsel=" selected";
								echo "<option value='$entry' $lbsel>$entry</option>";
							}
						}
						closedir($handle);
					}
				?>
			</select>			
		</div>
	</div>		
			
	<div class="form-group">
		<label for="locallogo" class="col-sm-3 control-label">Personnaliser le logo</label>
		<div class="col-sm-6">
			<select name="locallogo" id="locallogo" class="form-control" onChange="ShowHide();">
				<option value=""  <? if($config['locallogo']=="") echo 'selected';?> >non</option>
				<option value="1" <? if($config['locallogo']!="") echo 'selected';?> >oui</option>
			</select>			
		</div>
	</div>

	<div id="form-group-locallogo" class="form-group">
		<label for="filelocallogo" class="col-sm-3 control-label">Logo</label>
		<div class="col-sm-6">
			<input name="filelocallogo" id="filelocallogo" type="file" >
			<?
				echo "<div style='margin:10px 0px 0px 0px; background-color:#cdcdcd;'><img src='".$repository."local/images/logo/logo.png' style='max-width:100%'></div>";
			?>
		</div>
	</div>

	
	<div class="form-group">
		<label for="localheader" class="col-sm-3 control-label">Personnaliser la bannière</label>
		<div class="col-sm-6">
			<select name="localheader" id="localheader" class="form-control" onChange="ShowHide();">
				<option value=""  <? if($config['localheader']=="") echo 'selected';?> >non</option>
				<option value="1" <? if($config['localheader']!="") echo 'selected';?> >oui</option>
			</select>			
		</div>
	</div>

	<div id="form-group-localheader" class="form-group">
		<label for="filelocalheader" class="col-sm-3 control-label">Bannière</label>
		<div class="col-sm-6">
			<input name="filelocalheader" id="filelocalheader" type="file" >
			<?
				echo "<div style='margin:10px 0px 0px 0px; background-color:#cdcdcd;'><img src='".$repository."local/images/header/header.png' style='max-width:100%'></div>";
			?>
		</div>
	</div>		
	
	
	<div id="form-group-module-registration" class="form-group">
		<label for="modeRegistration" class="col-sm-3 control-label">Module d'Inscription</label>
		<div class="col-sm-6">
			<select name="modeRegistration" id="modeRegistration" class="form-control" onChange="ShowHide();">
				<option value="false" <? if($config['modeRegistration']=='false')  echo 'selected';?> >non</option>
				<option value="true"  <? if($config['modeRegistration']=='true')   echo 'selected';?> >oui</option>
			</select>			
		</div>
	</div>
	
	<div id="form-group-registration" class="form-group">
		<label for="modeRegistrationFlagExterne" class="col-sm-3 control-label">Module d'Inscription Externe</label>
		<div class="col-sm-6">
			<select name="modeRegistrationFlagExterne" id="modeRegistrationFlagExterne" class="form-control" onChange="ShowHide();">
				<option value="false" <? if($config['modeRegistrationExterne']=='')  echo 'selected';?> >non</option>
				<option value="true"  <? if($config['modeRegistrationExterne']!='')  echo 'selected';?> >oui</option>
			</select>			
		</div>
	</div>	
	
	<div id="form-group-registration-externe" class="form-group">
		<label for="modeRegistrationExterne" class="col-sm-3 control-label">URL d'Inscription Externe</label>
		<div class="col-sm-6">
			<input name="modeRegistrationExterne" id="modeRegistrationExterne" type="titre" class="form-control" placeholder="URL Inscription Externe" value="<? echo $config['modeRegistrationExterne']; ?>">		
		</div>
	</div>	

	<div id="form-group-profil-externe" class="form-group">
		<label for="modeProfilExterne" class="col-sm-3 control-label">URL Fiche Profil Externe<small style='clear:both;display: block;margin-top: -7px;'><em style='font-size:70%;font-weight:normal;'>mettre le mot clé #login en get de votre URL</em></small></label>
		<div class="col-sm-6">
			<input name="modeProfilExterne" id="modeProfilExterne" type="titre" class="form-control" placeholder="URL Profil Externe" value="<? echo $config['modeProfilExterne']; ?>">		
		</div>
	</div>

	<div id="form-group-avatar-externe" class="form-group">
		<label for="modeAvatarExterne" class="col-sm-3 control-label">URL Callback Avatar Profil<small style='clear:both;display: block;margin-top: -7px;'><em style='font-size:70%;font-weight:normal;'>mettre le mot clé #login en get de votre URL</em></small></label>
		<div class="col-sm-6">
			<input name="modeAvatarExterne" id="modeAvatarExterne" type="titre" class="form-control" placeholder="URL Profil Externe" value="<? echo $config['modeAvatarExterne']; ?>">		
		</div>
	</div>
		
	<div id="form-group-registration-confirm" class="form-group">
		<label for="modeRegistrationConfirm" class="col-sm-3 control-label">Validation Inscription</label>
		<div class="col-sm-6">
			<select name="modeRegistrationConfirm" id="modeRegistrationConfirm" class="form-control" onChange="ShowHide();">
				<option value="direct" <? if($config['modeRegistrationConfirm']=='direct')  echo 'selected';?> >Direct via email</option>
				<option value="admin"  <? if($config['modeRegistrationConfirm']=='admin')   echo 'selected';?> >Validation Administrateur</option>
			</select>			
		</div>
	</div>	
	
	
	<div class="form-group" style='display:none'>
		<label for="activerLDAP" class="col-sm-3 control-label">Activer LDAP*</label>
		<div class="col-sm-6">
			<select name="activerLDAP" id="activerLDAP" class="form-control" onChange="ShowHide();">
				<option value="false" <? if($config['activerLDAP']==false) echo 'selected';?> >non</option>
				<option value="true"  <? if($config['activerLDAP']==true)  echo 'selected';?> >oui</option>
			</select>			
		</div>
	</div>	
</form>


<!-- SCRIPT --------------------------------------------------------------------------------------------------------------------------------------- -->
<script>
	function ShowHide() {
		if($("#locallogo").val()=="1") {
			$('#form-group-locallogo').css('display','block');
		}
		else {
			$('#form-group-locallogo').css('display','none');
		}
		
		if($("#localheader").val()=="1") {
			$('#form-group-localheader').css('display','block');
		}
		else {
			$('#form-group-localheader').css('display','none');
		}
		
		if($("#modeRegistration").val()=="true"&&$("#modeRegistrationFlagExterne").val()=="true") {
			$('#form-group-registration-externe').css('display','block');
			$('#form-group-profil-externe').css('display','block');
			$('#form-group-avatar-externe').css('display','block');
			$('#form-group-registration-confirm').css('display','none');
		}
		else {
			$('#form-group-registration-externe').css('display','none');
			$('#form-group-profil-externe').css('display','none');
			$('#form-group-avatar-externe').css('display','none');
			$('#form-group-registration-confirm').css('display','bock');
		}
		
		if($("#modeRegistration").val()=="true") {
			$('#form-group-registration').css('display','block');
		}
		else {
			$('#form-group-registration').css('display','none');
			$('#form-group-registration-externe').css('display','none');
			$('#form-group-profil-externe').css('display','none');
			$('#form-group-avatar-externe').css('display','none');
			$('#form-group-registration-confirm').css('display','none');
		}


		if($("#modeAuthentification").val()=="CAS") {
			$('#form-group-remember').css('display','none');
		}
		else {
			$('#form-group-remember').css('display','block');
		}

		console.log($("#activerLDAP").val());
		if($("#activerLDAP").val()=="true"||$("#modeAuthentification").val()=="MYSQL") {
			$('#form-group-module-registration').css('display','block');
		}
		else {
			$('#form-group-module-registration').css('display','none');
			$('#form-group-registration').css('display','none');
			$('#form-group-registration-externe').css('display','none');
			$('#form-group-profil-externe').css('display','none');	
			$('#form-group-avatar-externe').css('display','none');
			$('#form-group-registration-confirm').css('display','none');
		}
	}	

	$(document).ready(function()
	{
		ShowHide();
	});
</script>
