<?
	$levelpage=1;
	include("include/permission.php");

	$id					=$_POST['id'];
	$tpmod				=$_POST['tpmod'];
	$fgreload			=$_POST['fgreload'];	

	if($fgreload!="") {
		$tpmod=$fgreload; 
	}
		
	echo "<form id='formulaire' class='form-horizontal' role='form' method='post' enctype='multipart/form-data'>";
	
	echo "<input id='id' name='id' type='hidden' value='".$id."'>";
	echo "<input id='tpmod' name='tpmod' type='hidden' value=''>";
	echo "<input id='fgreload' name='fgreload' type='hidden' value=''>";
	
  
//-- DISPLAY -------------------------------------------------------------------------------------------------------------------------------

if($tpmod=="") { 
    echo "<legend><h1>GESTION DES PAGES PAR PROFIL</h1></legend>";
    
	echo "<em>La première page d'un profil sera concidérée comme étant sa page d'accueil et prendra donc le nom de votre site.</em><br>";
	echo "<em>Si un utilisateur posséde plusieurs pages d'accueil cela sera la première dans l'ordre alphabétique qui sera concidérée comme telle.</em><br><br>";
	
	echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
    echo "<thead>";
	echo "<th width='40px'>Action</th>";
	echo "<th>Profil</th>";
	echo "<th>Pages</th>";
	echo "</thead>";
	
	$q="SELECT * FROM ".$config["dbprefixe"]."profil";
	$query=$bdd01->prepare($q);
	$query->execute();
	while($row=$query->fetch()){	
		echo "<tr>";

		echo "<td align='center'>";
		echo "<a class='mybtn fa fa-file' onClick='$(\"#id\").val(\"".$row['profil_id']."\"); $(\"#tpmod\").val(\"MODIFY\");$(\"#formulaire\").submit();'></a>";
		echo "</td>";

		echo "<td>";
		echo $row['profil_label'];
		echo "</td>";

		echo "<td>";
		$q="SELECT * FROM ".$config["dbprefixe"]."panel_profil, ".$config["dbprefixe"]."panel WHERE panel_profil_profil=? AND panel_profil_panel=panel_id ORDER BY panel_order";
		$query2=$bdd02->prepare($q);
		$query2->execute(array($row["profil_id"]));
		while($row2=$query2->fetch()){
			echo "<a href='index.php?id=".$row2['panel_id']."' target='_blank'>".$row2['panel_label']."</a><br>";
		}
		echo "</td>";

		echo "</tr>";
	} 

	echo "</table>";
}

//-- SUBMIT --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="SUBMIT") {

}

//-- MODIFY --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="MODIFY") {
	// Entete du formulaire
    echo "<legend><h1>MODIFICATION PAGE PAR PROFIL</h1></legend>";

	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlmod' name='vlmod' class='btn btn-primary' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
	echo "</div>";
	echo "</div>";	
	    

	// Valeur par défaut
	$q = "SELECT * FROM ".$config["dbprefixe"]."profil WHERE profil_id=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
	if($row=$query->fetch()){	
		$label 		= $row['profil_label'];
	}
}

//-- DELETE --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="DELETE") {

}


//-- FORMULAIRE -----------------------------------------------------------------------------------------------------------------------------

if($tpmod=="SUBMIT"||$tpmod=="MODIFY") {
?>

	<fieldset class="row fieldset" style="clear:both">
		<legend>Profil</legend>
		
		<div class="form-group">
			<label for="id_bis" class="col-sm-3 control-label">ID Profil*</label>
			<div class="col-sm-6"><input name="id_bis" id="id_bis" type="titre" class="form-control" disabled="disabled" placeholder="ID Application" value="<? echo $id; ?>"></div>
		</div>
		
		<div class="form-group">
			<label for="label" class="col-sm-3 control-label">Nom*</label>
			<div class="col-sm-6"><input name="label" id="label" type="titre" class="form-control" disabled="disabled" placeholder="Nom" value="<? echo $label; ?>"></div>
		</div>
	</fieldset>
	
	<fieldset class="row fieldset" style="clear:both">
		<legend>Liste des Pages</legend>
		<?
		echo "<div class='form-group'>";
		echo "<div class='col-sm-12'>";
		echo "<a class='btn btn-primary' data-toggle='modal' data-target='#mymodal-01' title='Séléctionner une application' onClick='ModalLoad(\"mymodal-01\",\"pageprofil-insert\",$id,0)'>Ajouter</a>";
		echo "</div>";
		echo "</div>";	

		echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
		echo "<thead>";
		echo "<th width='40px'>Action</th>";
		echo "<th>Ordre</th>";
		echo "<th>Page</th>";
		echo "<th>Type</th>";
		echo "<th>Propriétaire</th>";
		echo "</thead>";

		$q="SELECT * FROM ".$config["dbprefixe"]."panel_profil, ".$config["dbprefixe"]."panel, ".$config["dbprefixe"]."panel_type, ".$config["dbprefixe"]."user, ".$config["dbprefixe"]."profil WHERE panel_profil_profil=? AND panel_profil_panel=panel_id AND panel_type = panel_type_id AND panel_user=user_id AND user_profil=profil_id ORDER BY panel_order";
		$query=$bdd01->prepare($q);
		$query->execute(array($id));
		while($row=$query->fetch()){	
			echo "<tr>";

			echo "<td align='center'>";
			echo "<a class='mybtn fa fa-trash' data-toggle='modal' data-target='#mymodal-01' onClick='ModalLoad(\"mymodal-01\",\"pageprofil-delete\",$id,".$row["panel_id"].")' title='Supprimer une page' />";
			echo "</td>";
			
			echo "<td>".$row["panel_order"]."</td>";
			echo "<td>".$row["panel_label"]."</td>";
			echo "<td>".$row["panel_type_name"]."</td>";

			echo "<td>";
			if($row['user_avatar']=="") 
				echo "<img src='style/images/blank.gif' class='myavatarvide' width='45px' height='45px' style='float:left;margin-right:5px;'></img>";
			else
				echo "<img src='local/images/avatar/".$row['user_avatar']."' width='45px' height='45px' style='float:left;margin-right:5px;'></img>";
			
			echo "<span style='float:left; font-size:80%;'>";
			echo "Login : ".$row['user_login']."<br>";
			echo "Pseudo : ".$row['user_pseudo']."<br>";
			echo "Profil : ".$row['profil_label']."<br>";		
			echo "</span>";
			echo "</td>";
					
			echo "</tr>";
		}		
		
		echo "</table>";
		?>
	</fieldset>
	<?
}

echo "</form>";
?>

<!-- SCRIPT --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<? if($tpmod=="") { ?>
	<script>
		$(document).ready(function() {
			$('#datatable').dataTable( {
				"oLanguage": { "sUrl": "lib/dataTables/dataTables.txt" },
				"iDisplayLength": 100,
				"aoColumnDefs" : [ {'bSortable' : false, 'aTargets' : [ 0 ]} ],
				"aaSorting": [[ 1, "asc" ]],
				"stateSave": true
			} );
		} );	
	</script>
<? } ?>

<? if($tpmod=="SUBMIT"||$tpmod=="MODIFY"||$tpmod=="DELETE") { ?>
	<script type="text/javascript">
		$('#name').focus();

		$(document).ready(function() {
			$('#datatable').dataTable( {
				"oLanguage": { "sUrl": "lib/dataTables/dataTables.txt" },
				"iDisplayLength": 100,
				"aoColumnDefs" : [ {'bSortable' : false, 'aTargets' : [ 0 ]} ],
				"aaSorting": [[ 1, "asc" ]],
				"stateSave": true
			} );
		} );	
	</script>
<? } ?>

<script>
		<?php echo $jsaction ?>
</script>







	
	
