<? 
	$levelpage=1;
	include("include/permission.php");

	$fgval				=$_POST["fgval"];
	
	$activerLDAP		=$_POST["activerLDAP"];
	$LDAPserver			=$_POST["LDAPserver"];
	$LDAPport			=$_POST["LDAPport"];
	$LDAPreaderdn		=$_POST["LDAPreaderdn"];
	$LDAPreaderpw		=$_POST["LDAPreaderpw"];
	$LDAPwriterdn		=$_POST["LDAPwriterdn"];
	$LDAPwriterpw		=$_POST["LDAPwriterpw"];
	$LDAPracine			=$_POST["LDAPracine"];
	$LDAPorganisation	=$_POST["LDAPorganisation"];
	$LDAPfilterauth		=$_POST["LDAPfilterauth"];
	$LDAPfirstname		=$_POST["LDAPfirstname"];
	$LDAPlastname		=$_POST["LDAPlastname"];
	$LDAPemail			=$_POST["LDAPemail"];

	
	if($fgval!="") {
		$fgerreur=false;
		if($activerLDAP=="false"&&$config['modeAuthentification']=="LDAP") {
			$fgerreur=true;
			echo "<script>alert('Désactivation LDAP impossible vous ête en mode d\'authentification LDAP');</script>";
		}
		
		// En mode d'authentification LDAP si le module inscription est activé, le 
		if($config['modeRegistration']=='true'&&$config['modeRegistrationExtern']==''&&$activerLDAP=="true"&&($LDAPwriterdn==""||$LDAPwriterpw=="")) {
			$fgerreur=true;
			echo "<script>alert('Un compte en écriture est obligatoire dans le module d\'inscription est activé');</script>";			
		}
		
		if($activerLDAP=="true"&&($LDAPserver==""||$LDAPport==""||$LDAPreaderdn==""||$LDAPreaderpw==""||$LDAPracine==""||$LDAPfilterauth==""||$LDAPfirstname==""||$LDAPlastname==""||$LDAPemail==""||$LDAPorganisation=="")) {
			$fgerreur=true;
			echo "<script>alert('L\'ensemble des champs sont obligatoires');</script>";
		}
		
		if(!$fgerreur) {
			$fp=fopen($config['localdirectory']."/local/config/ldap.php","w");
			fwrite($fp, "<?\n");
			fwrite($fp, "\$config['activerLDAP']        = $activerLDAP;\n");
			fwrite($fp, "\$config['LDAPserver']         = '$LDAPserver';\n");
			fwrite($fp, "\$config['LDAPport']           = '$LDAPport';\n");
			fwrite($fp, "\$config['LDAPreaderdn']       = '$LDAPreaderdn';\n");
			fwrite($fp, "\$config['LDAPreaderpw']       = '$LDAPreaderpw';\n");
			fwrite($fp, "\$config['LDAPwriterdn']       = '$LDAPwriterdn';\n");
			fwrite($fp, "\$config['LDAPwriterpw']       = '$LDAPwriterpw';\n");

			fwrite($fp, "\$config['LDAPracine']         = '$LDAPracine';\n");
			fwrite($fp, "\$config['LDAPorganisation']   = '$LDAPorganisation';\n");
			fwrite($fp, "\$config['LDAPfilterauth']     = '$LDAPfilterauth';\n");
			fwrite($fp, "\$config['LDAPfirstname']      = '$LDAPfirstname';\n");
			fwrite($fp, "\$config['LDAPlastname']       = '$LDAPlastname';\n");
			fwrite($fp, "\$config['LDAPemail']          = '$LDAPemail';\n");
			fwrite($fp, "\n?>");
			fclose($fp);
			
			// Reload de la page
			echo "<script>parent.document.location.reload();</script>";
		}
	}
?>

<form class="form-horizontal" role="form" method='post' enctype="multipart/form-data">
	<legend><h1>CONFIGURATION LDAP</h1></legend>
	<input type="hidden" name="MAX_FILE_SIZE" value="2097152">
	
	<div class="form-group">
		<div class="col-sm-12">
			<input name="fgval" type="submit" class="btn btn-primary" value="Enregistrer">
		</div>
	</div>

	<div class="form-group">
		<label for="activerLDAP" class="col-sm-3 control-label">Activer LDAP*</label>
		<div class="col-sm-6">
			<select name="activerLDAP" id="activerLDAP" class="form-control" onChange="ShowHide();">
				<option value="false" <? if($config['activerLDAP']==false) echo 'selected';?> >non</option>
				<option value="true"  <? if($config['activerLDAP']==true)  echo 'selected';?> >oui</option>
			</select>			
		</div>
	</div>

	<div id="ldapparam">
	<div class="form-group">
		<label for="LDAPserver" class="col-sm-3 control-label">Adresse serveur*</label>
		<div class="col-sm-6">
			<input name="LDAPserver" id="LDAPserver" type="titre" class="form-control" placeholder="Titre" value="<? echo $config['LDAPserver']; ?>">
		</div>
	</div>

	<div class="form-group">
		<label for="LDAPport" class="col-sm-3 control-label">Port*</label>
		<div class="col-sm-6">
			<input name="LDAPport" id="LDAPport" type="titre" class="form-control" placeholder="Port" value="<? echo $config['LDAPport']; ?>">
		</div>
	</div>
					
	<div class="form-group">
		<label for="LDAPreaderdn" class="col-sm-3 control-label">Login Reader*</label>
		<div class="col-sm-6">
			<input name="LDAPreaderdn" id="LDAPreaderdn" type="titre" class="form-control" placeholder="Login" value="<? echo $config['LDAPreaderdn']; ?>">
		</div>
	</div>
									
	<div class="form-group">
		<label for="LDAPreaderpw" class="col-sm-3 control-label">Mot de Passe Reader*</label>
		<div class="col-sm-6">
			<input name="LDAPreaderpw" id="LDAPreaderpw" type="password" class="form-control" placeholder="Mot de Passe" value="<? echo $config['LDAPreaderpw']; ?>">
		</div>
	</div>

	<div class="form-group">
		<label for="LDAPwriterdn" class="col-sm-3 control-label">Login Writer</label>
		<div class="col-sm-6">
			<input name="LDAPwriterdn" id="LDAPwriterdn" type="titre" class="form-control" placeholder="Login" value="<? echo $config['LDAPwriterdn']; ?>">
		</div>
	</div>
									
	<div class="form-group">
		<label for="LDAPwriterpw" class="col-sm-3 control-label">Mot de Passe Writer</label>
		<div class="col-sm-6">
			<input name="LDAPwriterpw" id="LDAPwriterpw" type="password" class="form-control" placeholder="Mot de Passe" value="<? echo $config['LDAPwriterpw']; ?>">
		</div>
	</div>
	
	<div class="form-group">
		<label for="LDAPracine" class="col-sm-3 control-label">Racine*</label>
		<div class="col-sm-6">
			<input name="LDAPracine" id="LDAPracine" type="titre" class="form-control" placeholder="Racine" value="<? echo $config['LDAPracine']; ?>">
		</div>
	</div>	

	<div class="form-group">
		<label for="LDAPorganisation" class="col-sm-3 control-label">Organisation*</label>
		<div class="col-sm-6">
			<input name="LDAPorganisation" id="LDAPorganisation" type="titre" class="form-control" placeholder="Organisation" value="<? echo $config['LDAPorganisation']; ?>">
		</div>
	</div>	
		
	<div class="form-group">
		<label for="LDAPfilterauth" class="col-sm-3 control-label">Filtre Authentification*</label>
		<div class="col-sm-6">
			<input name="LDAPfilterauth" id="LDAPfilterauth" type="titre" class="form-control" placeholder="Filtre Authentification" value="<? echo $config['LDAPfilterauth']; ?>">
		</div>
	</div>	
	
	<div class="form-group">
		<label for="LDAPfirstname" class="col-sm-3 control-label">Atribut Prénom*</label>
		<div class="col-sm-6">
			<input name="LDAPfirstname" id="LDAPfirstname" type="titre" class="form-control" placeholder="Atribut Prénom" value="<? echo $config['LDAPfirstname']; ?>">
		</div>
	</div>				

	<div class="form-group">
		<label for="LDAPlastname" class="col-sm-3 control-label">Atribut Nom*</label>
		<div class="col-sm-6">
			<input name="LDAPlastname" id="LDAPlastname" type="titre" class="form-control" placeholder="Atribut Nom" value="<? echo $config['LDAPlastname']; ?>">
		</div>
	</div>	

	<div class="form-group">
		<label for="LDAPemail" class="col-sm-3 control-label">Atribut Email*</label>
		<div class="col-sm-6">
			<input name="LDAPemail" id="LDAPemail" type="titre" class="form-control" placeholder="Atribut Email" value="<? echo $config['LDAPemail']; ?>">
		</div>
	</div>
	</div>							
</form>


<!-- SCRIPT --------------------------------------------------------------------------------------------------------------------------------------- -->
<script>
	function ShowHide() {
		if($("#activerLDAP").val()=="true") {
			$('#ldapparam').css('display','block');
		}
		else {
			$('#ldapparam').css('display','none');
		}
	}	

	$(document).ready(function()
	{
		ShowHide();
	});
</script>
