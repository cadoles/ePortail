<?
	$levelpage=1;
	include("include/permission.php");
	
	$tpmod				=$_POST['tpmod'];
	$vlmod				=$_POST['vlmod'];
	$vladd				=$_POST['vladd'];
	$vlsup				=$_POST['vlsup'];
	
	$id					=$_POST['id'];
	$label				=$_POST['label'];
	$filter				=$_POST['filter'];
	$idcommunity		=$_POST['idcommunity'];
	
	/*--> Controle de cohérance */
	if($vladd!=""||$vlmod!="") {
		$fgerr="";
		if($label==""||$filter==""||$idcommunity=="") {
			$jsaction="alert('Vous devez renseigner l\'ensemble des informations');";
			$fgerr=1;
		}
	}
	
	/*--> Rester sur le mode encours */
	if($vlmod!=""&&fgerr!="") $tpmod="MODIFY";
	if($vladd!=""&&fgerr!="") $tpmod="SUBMIT";
		
	/*--> Submit */
	if($vladd!=""&&$fgerr=="") {
		$q="INSERT INTO ".$config["dbprefixe"]."ldap_profil(ldap_profil_label, ldap_profil_filter,ldap_profil_ldap_community) VALUES(?,?,?)";
		$query=$bdd01->prepare($q);
		$query->execute(array($label,$filter,$idcommunity));	
		$tpmod="";
	}

	/*--> Modify */
	if($vlmod!=""&&$fgerr=="") {
		$q="UPDATE ".$config["dbprefixe"]."ldap_profil SET ldap_profil_label=?, ldap_profil_filter=?, ldap_profil_ldap_community=? WHERE ldap_profil_id=?";
		$query=$bdd01->prepare($q);
		$query->execute(array($label,$filter,$idcommunity,$id));	
		$tpmod="";
	}

	/*--> Delete */
	if($vlsup!="") {
		delLdapProfil($id);
	}	

	echo "<form id='formulaire' class='form-horizontal' role='form' method='post' enctype='multipart/form-data'>";
	
	echo "<input id='id' name='id' type='hidden' value='".$id."'>";
	echo "<input id='tpmod' name='tpmod' type='hidden' value=''>";
	
  
//-- DISPLAY -------------------------------------------------------------------------------------------------------------------------------

if($tpmod=="") { 
    echo "<legend><h1>GESTION DES LDAP</h1></legend>";
    
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='fgadd' name='fgadd' class='btn btn-primary' onClick='$(\"#id\").val(\"\"); $(\"#tpmod\").val(\"SUBMIT\");' type='submit' value='Ajouter' />";
	echo "</div>";
	echo "</div>";
		
	echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
    echo "<thead>";
	echo "<th width='50px'>Action</th>";
	echo "<th >Communauté</th>";
	echo "<th >Libellé</th>";
	echo "<th >Filtre</th>";
	echo "</thead>";

	$q="SELECT * FROM ".$config["dbprefixe"]."ldap_profil, ".$config["dbprefixe"]."ldap_community WHERE ldap_profil_ldap_community=ldap_community_id ORDER BY ldap_profil_id";
	$query=$bdd01->prepare($q);
	$query->execute();		
	while($row=$query->fetch()){
		echo "<tr>";

		echo "<td align='center'>";
		if($row['ldap_profil_id']>0) {
			echo "<a class='mybtn fa fa-file' onClick='$(\"#id\").val(\"".$row['ldap_profil_id']."\"); $(\"#tpmod\").val(\"MODIFY\");$(\"#formulaire\").submit();'></a>";
			echo "&nbsp;";
			echo "<a class='mybtn fa fa-trash'       onClick='$(\"#id\").val(\"".$row['ldap_profil_id']."\"); $(\"#tpmod\").val(\"DELETE\");$(\"#formulaire\").submit();'></a>";
		}
		echo "</td>";

		echo "<td>";
		echo $row['ldap_community_label'];
		echo "</td>";
		
		echo "<td>";
		echo $row['ldap_profil_label'];
		echo "</td>";
				
		echo "<td>";
		echo $row['ldap_profil_filter'];
		echo "</td>";

		echo "</tr>";
	} 

	echo "</table>";
}

//-- SUBMIT --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="SUBMIT") {
	// Entete du formulaire
    echo "<legend><h1>AJOUT LDAP</h1></legend>";
    
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vladd' name='vladd' class='btn btn-primary' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
	echo "</div>";
	echo "</div>";	

	// Valeur par défaut
	$idcommunauty=-1;
}

//-- MODIFY --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="MODIFY") {
	// Entete du formulaire
    echo "<legend><h1>MODIFICATION COMMUNAUTE LDAP</h1></legend>";
   
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlmod' name='vlmod' class='btn btn-primary' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
	echo "</div>";
	echo "</div>";	

	// Valeur par défaut
	$q = "SELECT * FROM ".$config["dbprefixe"]."ldap_profil WHERE ldap_profil_id=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));		
	if($row=$query->fetch()){
		$label 			= $row['ldap_profil_label'];
		$filter			= $row['ldap_profil_filter'];
		$idcommunauty	= $row['ldap_profil_ldap_community'];
	}
}

//-- DELETE --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="DELETE") {
	echo "<legend><h1>SUPPRESSION COMMUNAUTE LDAP</h1></legend>";
	
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlsup' name='vlsup' class='btn btn-primary' type='submit' value='Confirmer la suppression' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";    
	echo "</div>";
	echo "</div>";
	
	// Valeur par défaut
	$q = "SELECT * FROM ".$config["dbprefixe"]."ldap_profil WHERE ldap_profil_id=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));		
	if($row=$query->fetch()){
		$label 			= $row['ldap_profil_label'];
		$filter			= $row['ldap_profil_filter'];
		$idcommunauty	= $row['ldap_profil_ldap_community'];
	}
}


//-- FORMULAIRE -----------------------------------------------------------------------------------------------------------------------------

if($tpmod=="SUBMIT"||$tpmod=="MODIFY"||$tpmod=="DELETE") {
?>
	<fieldset class="row fieldset" style="clear:both">
		<legend>Description</legend>

		<div class="form-group">
			<label for="id_bis" class="col-sm-3 control-label">ID*</label>
			<div class="col-sm-6"><input name="id_bis" id="id_bis" type="titre" class="form-control" disabled="disabled" placeholder="ID Application" value="<? echo $id; ?>"></div>
		</div>
		

		<div class="form-group">
			<label for="label" class="col-sm-3 control-label">Libellé*</label>
			<div class="col-sm-6"><input name="label" id="label" type="titre" class="form-control" placeholder="Libellé" value="<? echo $label; ?>"></div>
		</div>	
		
		
		<div class="form-group">
			<label for="filter" class="col-sm-3 control-label">Filtre LDAP*</label>
			<div class="col-sm-6">
				<input name="filter" id="filter" type="titre" class="form-control" placeholder="Filtre LDAP" value="<? echo $filter; ?>">
				<em>Utilisez le mot clé #login# dans votre filtre LDAP pour identifier l'utilisateur.</br>
				Exemples :</br>
				(&(uid=#login#)(ENTPersonProfils=eleve))</br>
				(&(uid=#login#)(ENTPersonProfils=enseignant))</em>
				
			</div>
		</div>	
		
		<div class="form-group">
			<label for="idcommunity" class="col-sm-3 control-label">Communauté*</label>
			<div class="col-sm-6">
				<select name="idcommunity" id="idcommunity" class="form-control">
					<?
					$q="SELECT * FROM ".$config["dbprefixe"]."ldap_community";
					$query=$bdd01->prepare($q);
					$query->execute();		
					while($row=$query->fetch()) {
						$lbselection="";
						if($row["ldap_community_id"]==$idcommunauty) 
							$lbselection = " selected";
						echo "<option value='".$row["ldap_community_id"]."' $lbselection>".$row["ldap_community_label"]."</option>";
					}
					?>
				</select>			
			</div>
		</div>		
	</fieldset>
<?
}

echo "</form>";

?>

<!-- SCRIPT --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<? if($tpmod==""||$tpmod=="MODIFY") { ?>
	<script>
		$(document).ready(function() {
			$('#datatable').dataTable( {
				"oLanguage": { "sUrl": "lib/dataTables/dataTables.txt" },
				"iDisplayLength": 100,
				"aoColumnDefs" : [ {'bSortable' : false, 'aTargets' : [ 0 ]} ],
				"aaSorting": [[ 1, "asc" ]],
				"stateSave": true
			} );
		} );	
	</script>
<? } ?>

<? if($tpmod=="SUBMIT"||$tpmod=="MODIFY"||$tpmod=="DELETE") { ?>
	<script type="text/javascript">
		$('#label').focus();
	</script>
<? } ?>

<script>
		<?php echo $jsaction ?>
</script>



	
	
