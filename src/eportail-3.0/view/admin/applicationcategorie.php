<?
	$levelpage=2;
	include("include/permission.php");
	
	$tpmod				=$_POST['tpmod'];
	$vlmod				=$_POST['vlmod'];
	$vladd				=$_POST['vladd'];
	$vlsup				=$_POST['vlsup'];
	
	$id					=$_POST['id'];
	$label				=$_POST['label'];
	$order				=$_POST['order'];
	$color				=$_POST['color'];

	/*--> Controle de cohérance */
	if($vladd!=""||$vlmod!="") {
		$fgerr="";


		if($label==""||$order=="") {
			$jsaction="alert('Les champs avec * sont obligatoires');";
			$fgerr=1;
		}
		
		if($order<1) {
			$jsaction="alert('Le numéro d\'ordre doit être supérieur à 0');";
			$fgerr=1;
		}
		
		$q="SELECT * FROM ".$config["dbprefixe"]."application_categorie WHERE application_categorie_label=?";
		$tbq=array($label);
		if($vlmod!="") {
			$q=$q." AND application_categorie_id!=?";
			array_push($tbq,$id);
		}
		$query=$bdd01->prepare($q);
		$query->execute($tbq);
		if($row=$query->fetch()){			
			$jsaction="alert('Une catégorie avec ce label existe déjà');";
			$fgerr=1;
		}
		
		$q="SELECT * FROM ".$config["dbprefixe"]."application_categorie WHERE application_categorie_order=?";
		$tbq=array($order);
		if($vlmod!="") {
			$q=$q." AND application_categorie_id!=?";
			array_push($tbq,$id);
		}
		$query=$bdd01->prepare($q);
		$query->execute($tbq);
		if($row=$query->fetch()){			
			$jsaction="alert('Une catégorie avec ce numéro d\'ordre existe déjà');";
			$fgerr=1;
		}
	}
	
	/*--> Rester sur le mode encours */
	if($vlmod!=""&&fgerr!="") $tpmod="MODIFY";
	if($vladd!=""&&fgerr!="") $tpmod="SUBMIT";
		
	/*--> Submit */
	if($vladd!=""&&$fgerr=="") {
		$q="INSERT INTO ".$config["dbprefixe"]."application_categorie(application_categorie_label,application_categorie_order,application_categorie_color) VALUES(?,?,?)";
		$query=$bdd01->prepare($q);
		$query->execute(array($label,$order,$color));
		$tpmod="";
	}

	/*--> Modify */
	if($vlmod!=""&&$fgerr=="") {
		$q="UPDATE ".$config["dbprefixe"]."application_categorie SET application_categorie_label=?,application_categorie_order=?,application_categorie_color=? WHERE application_categorie_id=?";
		$query=$bdd01->prepare($q);
		$query->execute(array($label,$order,$color,$id));
		$tpmod="";
	}
	
	/*--> Delete */
	if($vlsup!="") {
		$q="DELETE FROM  ".$config["dbprefixe"]."application_categorie WHERE application_categorie_id=?";
		$query=$bdd01->prepare($q);
		$query->execute(array($id));
		$tpmod="";
	}

	echo "<form id='formulaire' class='form-horizontal' role='form' method='post' enctype='multipart/form-data'>";
	
	
	echo "<input id='id' name='id' type='hidden' value='".$id."'>";
	echo "<input id='tpmod' name='tpmod' type='hidden' value=''>";
	
  
//-- DISPLAY -------------------------------------------------------------------------------------------------------------------------------

if($tpmod=="") { 
    echo "<legend><h1>GESTION DES CATEGORIES D'APPLICATION</h1></legend>";
	
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='fgadd' name='fgadd' class='btn btn-primary' onClick='$(\"#id\").val(\"\"); $(\"#tpmod\").val(\"SUBMIT\");' type='submit' value='Ajouter' />";
	echo "</div>";
	echo "</div>";
    
	echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
    echo "<thead>";
	echo "<th width='50px'>Action</th>";
	echo "<th width='70px'>Ordre</th>";
	echo "<th width='70px'>ID</th>";
	echo "<th>Nom</th>";
	echo "</thead>";
	
	$q="SELECT * FROM ".$config["dbprefixe"]."application_categorie";
	$query=$bdd01->prepare($q);
	$query->execute();
	while($row=$query->fetch()){	
		echo "<tr>";

		echo "<td align='center'>";
		echo "<a class='mybtn fa fa-file' onClick='$(\"#id\").val(\"".$row['application_categorie_id']."\"); $(\"#tpmod\").val(\"MODIFY\");$(\"#formulaire\").submit();'></a>";
		
		// Pas de suppression possible si catégorie liée à une application ou catégorie systeme
		if($row['application_categorie_id']>0) {
			$q="SELECT application_id FROM ".$config["dbprefixe"]."application WHERE application_categorie=?";
			$query2=$bdd02->prepare($q);
			$query2->execute(array($row['application_categorie_id']));
			if(!$row2=$query2->fetch()){	
				echo "&nbsp;";
				echo "<a class='mybtn fa fa-trash' onClick='$(\"#id\").val(\"".$row['application_categorie_id']."\"); $(\"#tpmod\").val(\"DELETE\");$(\"#formulaire\").submit();'></a>";
			}		
		}
		echo "</td>";

		echo "<td>";
		echo $row['application_categorie_order'];
		echo "</td>";
		
		echo "<td>";
		echo $row['application_categorie_id'];
		echo "</td>";
		
		echo "<td>";
		echo $row['application_categorie_label']."<br>";
		echo "</td>";

		echo "</tr>";
	} 
	echo "</table>";
}

//-- SUBMIT --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="SUBMIT") {
	// Entete du formulaire
    echo "<legend><h1>AJOUT CATEGORIE D'APPLICATION</h1></legend>";

	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vladd' name='vladd' class='btn btn-primary' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
	echo "</div>";
	echo "</div>";		

	// Valeur par défaut
	$order = 0;
}

//-- MODIFY --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="MODIFY") {
	// Entete du formulaire
    echo "<legend><h1>MODIFICATION CATEGORIE D'APPLICATION</h1></legend>";

	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlmod' name='vlmod' class='btn btn-primary' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
	echo "</div>";
	echo "</div>";		

	// Valeurs
	$q = "SELECT * FROM ".$config["dbprefixe"]."application_categorie WHERE application_categorie_id=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
	if($row=$query->fetch()){
		$label 	= $row["application_categorie_label"];
		$order	= $row["application_categorie_order"];
		$color	= $row["application_categorie_color"];
	}
	
	if($order=="") $order=0;
}

//-- DELETE --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="DELETE") {
	echo "<legend><h1>SUPPRESSION CATEGORIE D'APPLICATION</h1></legend>";
	
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlsup' name='vlsup' class='btn btn-primary' type='submit' value='Confirmer la suppression' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";    
	echo "</div>";
	echo "</div>";	
	
	// Valeurs
	$q = "SELECT * FROM ".$config["dbprefixe"]."application_categorie WHERE application_categorie_id=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
	if($row=$query->fetch()){
		$label 	= $row["application_categorie_label"];
		$order	= $row["application_categorie_order"];
		$color	= $row["application_categorie_color"];
	}
}


//-- FORMULAIRE -----------------------------------------------------------------------------------------------------------------------------

if($tpmod=="SUBMIT"||$tpmod=="MODIFY"||$tpmod=="DELETE") {
?>

	<fieldset class="row fieldset" style="clear:both">
		<div class="form-group">
			<label for="id_bis" class="col-sm-3 control-label">ID*</label>
			<div class="col-sm-6"><input name="id_bis" id="id_bis" type="titre" class="form-control" disabled="disabled" placeholder="ID" value="<? echo $id; ?>"></div>
		</div>


		<div class="form-group">
			<label for="label" class="col-sm-3 control-label">Nom*</label>
			<div class="col-sm-6"><input name="label" id="label" type="titre" class="form-control" placeholder="Nom" value="<? echo $label; ?>"></div>
		</div>
		

		<div class="form-group">
			<label for="order" class="col-sm-3 control-label">Ordre*</label>
			<div class="col-sm-6"><input name="order" id="order" type="number" class="form-control" placeholder="Ordre" value="<? echo $order; ?>"></div>
		</div>
		
		<div class="form-group">
			<label for="color" class="col-sm-3 control-label">Couleur</label>
			<div class="col-sm-6">
				<input type="text" value="<? echo $color; ?>" id="color" name="color" class="pick-a-color form-control">
			</div>
		</div>		
	</fieldset>

<?
}

echo "</form>";

?>

<link rel="stylesheet" href="lib/pickColor/pick-a-color-1.2.3.min.css">
<script src="lib/pickColor/tinycolor-0.9.15.min.js"></script>
<script src="lib/pickColor/pick-a-color-1.2.3.min.js"></script>

<!-- SCRIPT --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<? if($tpmod=="") { ?>
	<script>
		$(document).ready(function() {
			$('#datatable').dataTable( {
				"oLanguage": { "sUrl": "lib/dataTables/dataTables.txt" },
				"iDisplayLength": 100,
				"aoColumnDefs" : [ {'bSortable' : false, 'aTargets' : [ 0 ]} ],
				"aaSorting": [[ 1, "asc" ]],
				"stateSave": true
			} );
		} );	
	</script>
<? } ?>

<? if($tpmod=="SUBMIT"||$tpmod=="MODIFY"||$tpmod=="DELETE") { ?>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#label').focus();

			$(".pick-a-color").pickAColor({
			  showSpectrum            : true,
				showSavedColors         : true,
				saveColorsPerElement    : true,
				fadeMenuToggle          : true,
				showAdvanced			: true,
				showBasicColors         : true,
				showHexInput            : true,
				allowBlank				: true,
				inlineDropdown			: true
			});		
		} );		
	</script>
<? } ?>

<script>
		<?php echo $jsaction ?>
</script>




	
	
