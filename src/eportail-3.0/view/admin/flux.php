<?
	$levelpage=2;
	include("include/permission.php");

	$id					=$_POST['id'];
	$tpmod				=$_POST['tpmod'];

	$tpmod				= $_POST['tpmod'];
	$vlmod				= $_POST['vlmod'];
	$vladd				= $_POST['vladd'];
	$vlsup				= $_POST['vlsup'];
	$fgreload			= $_POST['fgreload'];

	$flux_id			= $_POST['flux_id'];
	$flux_name			= $_POST['flux_name'];
	$flux_type			= $_POST['flux_type'];
	$flux_url			= $_POST['flux_url'];
	$flux_order			= $_POST['flux_order'];
	$flux_number		= $_POST['flux_number'];
	$flux_color			= $_POST['flux_color'];
	$flux_style			= $_POST['flux_style'];
	
	if($fgreload!="") {
		$tpmod=$fgreload; 
	}

	/*--> Controle de cohérance */
	if($vladd!=""||$vlmod!="") {
		$fgerr="";

		if($flux_name==""||$flux_type==""||$flux_number==""||$flux_style=="") {
			$jsaction="alert('Vous devez renseigner l\'ensemble des informations');";
			$fgerr=1;
		}
		
		if($fgerr!=1&&$flux_type==0&&$flux_url=="") {
			$jsaction="alert('Vous devez renseigner l\'ensemble des informations');";
			$fgerr=1;
		}
		
		$q="SELECT * FROM ".$config["dbprefixe"]."flux WHERE flux_name=?";
		$tbq=array($flux_name);
		if($vlmod!="") {
			$q=$q." AND flux_id!=?";
			array_push($tbq,$flux_id);
		}
		$query=$bdd01->prepare($q);
		$query->execute($tbq);
		if($row=$query->fetch()){			
			$jsaction="alert('Un flux avec ce nom existe déjà');";
			$fgerr=1;
		}		
			
	}

	/*--> Rester sur le mode encours */
	if($vlmod!=""&&fgerr!="") $tpmod="MODIFY";
	if($vladd!=""&&fgerr!="") $tpmod="SUBMIT";

	/*--> Submit */
	if($vladd!=""&&$fgerr=="") {
		$q="INSERT INTO ".$config["dbprefixe"]."flux(flux_name, flux_type, flux_url, flux_order, flux_number, flux_color,flux_style) VALUES(?,?,?,?,?,?,?)";
		$query=$bdd01->prepare($q);
		$query->execute(array($flux_name,$flux_type,$flux_url,$flux_order,$flux_number,$flux_color,$flux_style));
		$tpmod="";
		if($flux_type==1) {
			$tpmod="MODIFY";
			$flux_id=$bdd01->lastInsertId();
		}
	}

	/*--> Modify */
	if($vlmod!=""&&$fgerr=="") {
		$q="UPDATE ".$config["dbprefixe"]."flux SET flux_name=?, flux_type=?, flux_url=?, flux_order=?, flux_number=?, flux_color=?, flux_style=? WHERE flux_id=?";
		$query=$bdd01->prepare($q);
		$query->execute(array($flux_name,$flux_type,$flux_url,$flux_order,$flux_number,$flux_color,$flux_style,$flux_id));
		$tpmod="";
	}

	/*--> Delete */
	if($vlsup!="") {
		delFlux($flux_id);
		$tpmod="";
	}

	echo "<form id='formulaire' class='form-horizontal' role='form' method='post' enctype='multipart/form-data'>";
	
	echo "<input id='flux_id' name='flux_id' type='hidden' value='".$flux_id."'>";
	echo "<input id='tpmod' name='tpmod' type='hidden' value=''>";
	echo "<input id='fgreload' name='fgreload' type='hidden' value=''>";


//-- DISPLAY -------------------------------------------------------------------------------------------------------------------------------

if($tpmod=="") {
    echo "<legend><h1>GESTION DES FLUX</h1></legend>";

	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='fgadd' name='fgadd' class='btn btn-primary' onClick='$(\"#flux_id\").val(\"\"); $(\"#tpmod\").val(\"SUBMIT\");' type='submit' value='Ajouter' />";
	echo "</div>";
	echo "</div>";

	echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
    echo "<thead>";
	echo "<th width='40px'>Action</th>";
	echo "<th>ID</th>";
	echo "<th>Titre</th>";
	echo "<th>Type</th>";
	echo "<th>Ordre</th>";
	echo "<th>URL</th>";
	echo "</thead>";

	$q="SELECT * FROM ".$config["dbprefixe"]."flux";
	$query=$bdd01->prepare($q);
	$query->execute();
	while($row=$query->fetch()){	
		echo "<tr>";

		echo "<td align='center'>";
		echo "<a class='mybtn fa fa-file' onClick='$(\"#flux_id\").val(\"".$row['flux_id']."\"); $(\"#tpmod\").val(\"MODIFY\");$(\"#formulaire\").submit();'></a>";
		if($row['flux_id']>0) {
			echo "&nbsp;";
			echo "<a class='mybtn fa fa-trash'   onClick='$(\"#flux_id\").val(\"".$row['flux_id']."\"); $(\"#tpmod\").val(\"DELETE\");$(\"#formulaire\").submit();'></a>";
		}
		echo "</td>";

		echo "<td>";
		echo $row['flux_id'];
		echo "</td>";

		echo "<td>";
		echo $row['flux_name'];
		echo "</td>";

		echo "<td>";
		if($row['flux_type']==0) 
			echo "RSS";
		elseif ($row['flux_type']==2) 
			echo "URL";
		else
			echo "Articles";
		echo "</td>";

		echo "<td>";
		echo $row['flux_order'];
		echo "</td>";
		
		echo "<td>";
		if($row['flux_type']==1) $a=$config["urlbase"]."/feed.php?id=".$row['flux_id'];
		else $a=$row['flux_url'];
		echo "<a href='$a' target='_blank'>$a</a>";
		
		echo "</td>";		

		echo "</tr>";
	}
	echo "</table>";
}

//-- SUBMIT --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="SUBMIT") {
	// Entete du formulaire
    echo "<legend><h1>AJOUT FLUX</h1></legend>";
	
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vladd' name='vladd' class='btn btn-primary' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
	echo "</div>";
	echo "</div>";	

	// Valeur par défaut
	$flux_type  	= 0;
	$flux_order 	= 1;
	$flux_number 	= 5;
	$flux_style 	= 1;
}

//-- MODIFY --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="MODIFY") {
	// Entete du formulaire
    echo "<legend><h1>MODIFICATION FLUX</h1></legend>";

	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlmod' name='vlmod' class='btn btn-primary' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
	echo "</div>";
	echo "</div>";	

	// Valeurs
	$q = "SELECT * FROM ".$config["dbprefixe"]."flux WHERE flux_id=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($flux_id));
	if($row=$query->fetch()){	
		$flux_name	 	= $row["flux_name"];
		$flux_type 		= $row["flux_type"];
		$flux_url 		= $row["flux_url"];
		$flux_order 	= $row["flux_order"];
		$flux_number 	= $row["flux_number"];
		$flux_color 	= $row["flux_color"];
		$flux_style 	= $row["flux_style"];
	}
}

//-- DELETE --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="DELETE") {
	echo "<legend><h1>SUPPRESSION APPLICATION</h1></legend>";
	
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlsup' name='vlsup' class='btn btn-primary' type='submit' value='Confirmer la suppression' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";    
	echo "</div>";
	echo "</div>";

	// Valeurs
	$q = "SELECT * FROM ".$config["dbprefixe"]."flux WHERE flux_id=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($flux_id));
	if($row=$query->fetch()){	
		$flux_name	 	= $row["flux_name"];
		$flux_type 		= $row["flux_type"];
		$flux_url 		= $row["flux_url"];
		$flux_order 	= $row["flux_order"];
		$flux_number 	= $row["flux_number"];
		$flux_color 	= $row["flux_color"];
		$flux_style 	= $row["flux_style"];
	}
}


//-- FORMULAIRE -----------------------------------------------------------------------------------------------------------------------------

if($tpmod=="SUBMIT"||$tpmod=="MODIFY"||$tpmod=="DELETE") {
?>

	<fieldset class="row fieldset" style="clear:both">

		<div class="form-group">
			<label for="id_bis" class="col-sm-3 control-label">ID Flux*</label>
			<div class="col-sm-6"><input name="id_bis" id="id_bis" type="titre" class="form-control" disabled="disabled" placeholder="ID Flux" value="<? echo $flux_id; ?>"></div>
		</div>
		
		<div class="form-group">
			<label for="flux_name" class="col-sm-3 control-label">Nom*</label>
			<div class="col-sm-6"><input name="flux_name" id="flux_name" type="titre" class="form-control" placeholder="Nom" value="<? echo $flux_name; ?>"></div>
		</div>

		<div class="form-group">
			<label for="flux_type" class="col-sm-3 control-label">Type*</label>
			<div class="col-sm-6">
				<select name="flux_type" id="flux_type" class="form-control">
					<option value=0	<? if($flux_type==0)	{ echo 'selected'; }?>>RSS</option>
					<option value=1	<? if($flux_type==1)	{ echo 'selected'; }?>>Articles</option>
					<option value=2	<? if($flux_type==2)	{ echo 'selected'; }?>>URL</option>
				</select>
			</div>
		</div>
		
		<div id="blockurl" class="form-group">
			<label for="flux_url" class="col-sm-3 control-label">Url*</label>
			<div class="col-sm-6"><input name="flux_url" id="flux_url" type="titre" class="form-control" placeholder="Url" value="<? echo $flux_url; ?>"></div>
		</div>	

		<div class="form-group">
			<label for="flux_order" class="col-sm-3 control-label">Ordre*</label>
			<div class="col-sm-6"><input name="flux_order" id="flux_order" type="number" class="form-control" placeholder="Ordre" value="<? echo $flux_order; ?>"></div>
		</div>

		<div class="form-group">
			<label for="flux_number" class="col-sm-3 control-label">Nombre d'alertes affichées*</label>
			<div class="col-sm-6"><input name="flux_number" id="flux_order" type="number" class="form-control" placeholder="Niombre" value="<? echo $flux_number; ?>"></div>
		</div>
		
		<div class="form-group">
			<label for="color" class="col-sm-3 control-label">Couleur</label>
			<div class="col-sm-6">
				<input type="text" value="<? echo $flux_color; ?>" id="flux_color" name="flux_color" class="pick-a-color form-control">
			</div>
		</div>	
		
		<div class="form-group">
			<label for="flux_style" class="col-sm-3 control-label">Style*</label>
			<div class="col-sm-6">
				<select name="flux_style" id="flux_style" class="form-control">
				<?
					$q="SELECT * FROM ".$config["dbprefixe"]."flux_style";
					$query=$bdd01->prepare($q);
					$query->execute();
					while($row=$query->fetch()){
						$lbsel="";
						if($flux_style==$row["flux_style_id"]) {
							$lbsel="selected";
						}
						echo "<option value='".$row["flux_style_id"]."' ".$lbsel.">".$row["flux_style_label"]."</option>";
					}
				?>					
				</select>			
			</div>
		</div>				
	</fieldset>


	<fieldset id="harticle" class="row fieldset" style="clear:both">
		<legend>Articles</legend>
		<?
			if($tpmod=="SUBMIT") echo "<center><br><br>Veuillez valider la création de votre Flux avant de pouvoir y ajouter des Articles</center>";
			else {
				echo "<div class='form-group'>";
				echo "<div class='col-sm-12'>";
				echo "<a class='btn btn-primary' data-toggle='modal' data-target='#mymodal-01' title='Ajouter un Article' onClick='ModalLoad(\"mymodal-01\",\"fluxmsg-insert\",$flux_id,0)'>Ajouter</a>";
				echo "</div>";
				echo "</div>";	

				echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";				
				echo "<thead>";
				echo "<th width='40px'>Action</th>";
				echo "<th width='150px'>Date</th>";
				echo "<th>Nom</th>";
				echo "</thead>";
				
				$q="SELECT * FROM ".$config["dbprefixe"]."fluxmsg WHERE fluxmsg_flux=? ORDER BY fluxmsg_date";
				$query=$bdd01->prepare($q);
				$query->execute(array($flux_id));
				while($row=$query->fetch()){
					echo "<tr>";
					echo "<td align='center'>";
					echo "<a class='mybtn fa fa-file' data-toggle='modal' data-target='#mymodal-01' onClick='ModalLoad(\"mymodal-01\",\"fluxmsg-update\",$flux_id,".$row["fluxmsg_id"].")' title='Modifier un article' />";
					echo "&nbsp;";
					echo "<a class='mybtn fa fa-trash'       data-toggle='modal' data-target='#mymodal-01' onClick='ModalLoad(\"mymodal-01\",\"fluxmsg-delete\",$flux_id,".$row["fluxmsg_id"].")' title='Supprimer un article' />";
					echo "</td>";
					
					echo "<td>".$row['fluxmsg_date']."</td>";
					echo "<td>".$row['fluxmsg_name']."</td>";
					
					echo "</tr>";
				}		

				echo "</table>";
			}
		?>
	</fieldset>
<?
}

echo "</form>";

?>

<link rel="stylesheet" href="lib/pickColor/pick-a-color-1.2.3.min.css">
<script src="lib/pickColor/tinycolor-0.9.15.min.js"></script>
<script src="lib/pickColor/pick-a-color-1.2.3.min.js"></script>

<!-- SCRIPT --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<script type="text/javascript">
	<?php echo $jsaction ?>
	
<? if($tpmod=="") { ?>
		$(document).ready(function() {
			$('#datatable').dataTable( {
				"oLanguage": { "sUrl": "lib/dataTables/dataTables.txt" },
				"iDisplayLength": 100,
				"aoColumnDefs" : [ {'bSortable' : false, 'aTargets' : [ 0 ]} ],
				"aaSorting": [[ 1, "asc" ]],
				"stateSave": true
			} );
		} );	
<? } ?>
	
<? if($tpmod=="SUBMIT"||$tpmod=="MODIFY"||$tpmod=="DELETE") { ?>	
	$(document).ready(function() {
		$('#flux_type').change(function() {
			ShowHide();
		});	

		$(document).ready(function() {
			$('#datatable').dataTable( {
				"oLanguage": { "sUrl": "lib/dataTables/dataTables.txt" },
				"iDisplayLength": 100,
				"aoColumnDefs" : [ {'bSortable' : false, 'aTargets' : [ 0 ]} ],
				"aaSorting": [[ 1, "asc" ]],
				"stateSave": true
			} );

			$(".pick-a-color").pickAColor({
			  showSpectrum            : true,
				showSavedColors         : true,
				saveColorsPerElement    : true,
				fadeMenuToggle          : true,
				showAdvanced			: true,
				showBasicColors         : true,
				showHexInput            : true,
				allowBlank				: true,
				inlineDropdown			: true
			});	
		} );		
	
		ShowHide();		
	} );
	
	function changeFrame(mode,id1,id2) {
		if(mode=="submit") {
			srcframe="fluxmsg.php?tpmod=SUBMIT&id1="+id1;
		}
		else if(mode=="modify") {
			srcframe="fluxmsg.php?tpmod=MODIFY&id1="+id1+"&id2="+id2;
		}
		else {
			srcframe="fluxmsg.php?tpmod=DELETE&id1="+id1+"&id2="+id2;
		}
		
		$("#framemodal").attr("src",srcframe);
	}
			
	function ShowHide() {
		$this=$("#flux_type");
		
		if($this.val()=="0" || $this.val()=="2") {
			$("#blockurl").css("display","block");
			$("#harticle").css("display","none");
		}
		else {
			$("#blockurl").css("display","none");
			$("#harticle").css("display","block");
		}
	}	

	function recharge() {
		$("#formulaire").submit();
	}
	
	function closemodal() {
		$('#mymodal').modal('hide');
	}	
<? } ?>
	
</script>







