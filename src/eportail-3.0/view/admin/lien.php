<style>
#datatable_wrapper table{
	font-size:80%;
}
</style>

<?
	$levelpage=2;
	include("include/permission.php");

	if(!isset($_SESSION[$keymodal])) {
		header("location:index.php?view=diened.php");
		exit();
	}

	$tptbl	=$_SESSION[$keymodal]['modal-type'];
	$tpmod	=$_SESSION[$keymodal]['modal-mode'];
	$id1	=$_SESSION[$keymodal]['id1'];

	$vladd=$_POST['vladd'];	
	$vlmod=$_POST['vlmod'];	
	$fgclo=$_POST['fgclo'];
	$id2  =$_POST['id2'];
	
	
	if($vladd!="") {
		if($tptbl=="ITEM") {
			$q="INSERT INTO ".$config["dbprefixe"]."application_profil (application_profil_profil,application_profil_application) VALUES(?,?)";
			$query=$bdd01->prepare($q);
			$query->execute(array($id1,$id2));		
		}	
		elseif($tptbl=="ITEMSSO") {
			$q="INSERT INTO ".$config["dbprefixe"]."application_sso_profil (application_sso_profil_sso_profil,application_sso_profil_application) VALUES(?,?)";
			$query=$bdd01->prepare($q);
			$query->execute(array($id1,$id2));		
		}
		elseif($tptbl=="ITEMLDAP") {
			$q="INSERT INTO ".$config["dbprefixe"]."application_ldap_profil (application_ldap_profil_ldap_profil,application_ldap_profil_application) VALUES(?,?)";
			$query=$bdd01->prepare($q);
			$query->execute(array($id1,$id2));		
		}	
		elseif($tptbl=="PAGE") {
			$q="INSERT INTO ".$config["dbprefixe"]."panel_profil (panel_profil_profil,panel_profil_panel) VALUES(?,?)";
			$query=$bdd01->prepare($q);
			$query->execute(array($id1,$id2));				
		}		
		elseif($tptbl=="PAGESSO") {
			$q="INSERT INTO ".$config["dbprefixe"]."panel_sso_profil (panel_sso_profil_sso_profil,panel_sso_profil_panel) VALUES(?,?)";
			$query=$bdd01->prepare($q);
			$query->execute(array($id1,$id2));
		}
		elseif($tptbl=="PAGELDAP") {
			$q="INSERT INTO ".$config["dbprefixe"]."panel_ldap_profil (panel_ldap_profil_ldap_profil,panel_ldap_profil_panel) VALUES(?,?)";
			$query=$bdd01->prepare($q);
			$query->execute(array($id1,$id2));						
		}
		elseif($tptbl=="FLUX") {
			$q="INSERT INTO ".$config["dbprefixe"]."flux_profil (flux_profil_profil,flux_profil_flux) VALUES(?,?)";
			$query=$bdd01->prepare($q);
			$query->execute(array($id1,$id2));
		}					
		elseif($tptbl=="FLUXSSO") {
			$q="INSERT INTO ".$config["dbprefixe"]."flux_sso_profil (flux_sso_profil_profil,flux_sso_profil_flux) VALUES(?,?)";
			$query=$bdd01->prepare($q);
			$query->execute(array($id1,$id2));					
		}
		elseif($tptbl=="FLUXLDAP") {
			$q="INSERT INTO ".$config["dbprefixe"]."flux_ldap_profil (flux_ldap_profil_profil,flux_ldap_profil_flux) VALUES(?,?)";
			$query=$bdd01->prepare($q);
			$query->execute(array($id1,$id2));					
		}		
		elseif($tptbl=="SSOCOMMUNITY") {
			$q="INSERT INTO ".$config["dbprefixe"]."sso_community_attribut (sso_community_attribut_community,sso_community_attribut_attribut) VALUES(?,?)";
			$query=$bdd01->prepare($q);
			$query->execute(array($id1,$id2));
		}			
		elseif($tptbl=="SSOPROFIL") {
			$q="INSERT INTO ".$config["dbprefixe"]."sso_profil_attribut (sso_profil_attribut_profil,sso_profil_attribut_attribut) VALUES(?,?)";
			$query=$bdd01->prepare($q);
			$query->execute(array($id1,$id2));
		}	
	}
	
	if($tpmod=="DELETE") {
		$id2	=$_SESSION[$keymodal]['id2'];
		
		if($tptbl=="ITEM") {
			$q="DELETE FROM ".$config["dbprefixe"]."application_profil WHERE application_profil_profil=? AND application_profil_application=?";
			$query=$bdd01->prepare($q);
			$query->execute(array($id1,$id2));		
		}
		elseif($tptbl=="ITEMSSO") {
			$q="DELETE FROM ".$config["dbprefixe"]."application_sso_profil WHERE application_sso_profil_sso_profil=? AND application_sso_profil_application=?";
			$query=$bdd01->prepare($q);
			$query->execute(array($id1,$id2));		
		}	
		elseif($tptbl=="ITEMLDAP") {
			$q="DELETE FROM ".$config["dbprefixe"]."application_ldap_profil WHERE application_ldap_profil_ldap_profil=? AND application_ldap_profil_application=?";
			$query=$bdd01->prepare($q);
			$query->execute(array($id1,$id2));		
		}
		elseif($tptbl=="PAGE") {
			$q="DELETE FROM ".$config["dbprefixe"]."panel_profil WHERE panel_profil_profil=? AND panel_profil_panel=?";
			$query=$bdd01->prepare($q);
			$query->execute(array($id1,$id2));					
		}	
		elseif($tptbl=="PAGESSO") {
			$q="DELETE FROM ".$config["dbprefixe"]."panel_sso_profil WHERE panel_sso_profil_sso_profil=? AND panel_sso_profil_panel=?";
			$query=$bdd01->prepare($q);
			$query->execute(array($id1,$id2));					
		}
		elseif($tptbl=="PAGELDAP") {
			$q="DELETE FROM ".$config["dbprefixe"]."panel_ldap_profil WHERE panel_ldap_profil_ldap_profil=? AND panel_ldap_profil_panel=?";
			$query=$bdd01->prepare($q);
			$query->execute(array($id1,$id2));					
		}
		elseif($tptbl=="FLUX") {
			$q="DELETE FROM ".$config["dbprefixe"]."flux_profil WHERE flux_profil_profil=? AND flux_profil_flux=?";
			$query=$bdd01->prepare($q);
			$query->execute(array($id1,$id2));					
		}	
		elseif($tptbl=="FLUXSSO") {
			$q="DELETE FROM ".$config["dbprefixe"]."flux_sso_profil WHERE flux_sso_profil_profil=? AND flux_sso_profil_flux=?";
			$query=$bdd01->prepare($q);
			$query->execute(array($id1,$id2));
		}
		elseif($tptbl=="FLUXLDAP") {
			$q="DELETE FROM ".$config["dbprefixe"]."flux_ldap_profil WHERE flux_ldap_profil_profil=? AND flux_ldap_profil_flux=?";
			$query=$bdd01->prepare($q);
			$query->execute(array($id1,$id2));					
		}
		elseif($tptbl=="SSOCOMMUNITY") {
			$q="DELETE FROM ".$config["dbprefixe"]."sso_community_attribut WHERE sso_community_attribut_community=? AND sso_community_attribut_attribut=?";
			$query=$bdd01->prepare($q);
			$query->execute(array($id1,$id2));				
		}	
		elseif($tptbl=="SSOPROFIL") {
			$q="DELETE FROM ".$config["dbprefixe"]."sso_profil_attribut WHERE sso_profil_attribut_profil=? AND sso_profil_attribut_attribut=?";
			$query=$bdd01->prepare($q);
			$query->execute(array($id1,$id2));				
		}	
				
		$jsaction="recharge()";
	}

	
	if($fgclo!="") {
		$jsaction="recharge()";
	}	

if($tpmod=="") {
	echo "<form id='formulaire' class='form-horizontal' role='form' method='post' enctype='multipart/form-data'>";

	echo "<input id='id1'   name='id1'   type='hidden' value='".$id1."'>";
	echo "<input id='id2'   name='id2'   type='hidden' value='".$id2."'>";
	echo "<input id='tptbl' name='tptbl' type='hidden' value='".$tptbl."'>";
	echo "<input id='vladd' name='vladd' type='hidden' value=''>";
	
	echo "<a class='btn btn-primary' onclick='recharge();'>Fermer</a><br><br>";
	
	
	if($tptbl=="ITEM") {
		echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
		echo "<thead>";
		echo "<th width='40px'>Action</th>";
		echo "<th width='70px'>Icône</th>";
		echo "<th>Applications</th>";
		echo "</thead>";
	
		$q="SELECT * FROM ".$config["dbprefixe"]."application,".$config["dbprefixe"]."icon WHERE application_icon=icon_id AND NOT EXISTS(SELECT application_profil_profil FROM ".$config["dbprefixe"]."application_profil WHERE application_profil_profil=? AND application_profil_application=application_id)";
		$query=$bdd01->prepare($q);
		$query->execute(array($id1));
		while($row=$query->fetch()){
			echo "<tr>";

			echo "<td align='center'>";
			echo "<a class='mybtn fa fa-plus' onClick='$(\"#id2\").val(\"".$row['application_id']."\"); $(\"#vladd\").val(\"SUBMIT\");$(\"#formulaire\").submit();'></a>";
			echo "</td>";

			echo "<td align='center'>";
			echo "<a href='".urldecode($row['application_url'])."' target='_blank'><img src='local/images/icon/".$row['icon_url']."' width='30px' height='30px'></img></a>";
			echo "</td>";
		
			echo "<td>";
			echo "<a href='".urldecode($row['application_url'])."' target='_blank'>".$row["application_name"]."</a><br>";
			echo "</td>";
	
			echo "</tr>";
		}

		echo "</table>";
	}

	elseif($tptbl=="ITEMSSO") {
		echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
		echo "<thead>";
		echo "<th width='40px'>Action</th>";
		echo "<th width='70px'>Icône</th>";
		echo "<th>Applications</th>";
		echo "</thead>";
	
		$q="SELECT * FROM ".$config["dbprefixe"]."application,".$config["dbprefixe"]."icon WHERE application_icon=icon_id AND NOT EXISTS(SELECT application_sso_profil_sso_profil FROM ".$config["dbprefixe"]."application_sso_profil WHERE application_sso_profil_sso_profil=? AND application_sso_profil_application=application_id)";
		$query=$bdd01->prepare($q);
		$query->execute(array($id1));
		while($row=$query->fetch()){
			echo "<tr>";

			echo "<td align='center'>";
			echo "<a class='mybtn fa fa-plus' onClick='$(\"#id2\").val(\"".$row['application_id']."\"); $(\"#vladd\").val(\"SUBMIT\");$(\"#formulaire\").submit();'></a>";
			echo "</td>";

			echo "<td align='center'>";
			echo "<a href='".urldecode($row['application_url'])."' target='_blank'><img src='local/images/icon/".$row['icon_url']."' width='30px' height='30px'></img></a>";
			echo "</td>";
		
			echo "<td>";
			echo "<a href='".urldecode($row['application_url'])."' target='_blank'>".$row["application_name"]."</a><br>";
			echo "</td>";
	
			echo "</tr>";
		}

		echo "</table>";
	}
	
	if($tptbl=="ITEMLDAP") {
		echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
		echo "<thead>";
		echo "<th width='40px'>Action</th>";
		echo "<th width='70px'>Icône</th>";
		echo "<th>Applications</th>";
		echo "</thead>";
	
		$q="SELECT * FROM ".$config["dbprefixe"]."application,".$config["dbprefixe"]."icon WHERE application_icon=icon_id AND NOT EXISTS(SELECT application_ldap_profil_ldap_profil FROM ".$config["dbprefixe"]."application_ldap_profil WHERE application_ldap_profil_ldap_profil=? AND application_ldap_profil_application=application_id)";
		$query=$bdd01->prepare($q);
		$query->execute(array($id1));
		while($row=$query->fetch()){
			echo "<tr>";

			echo "<td align='center'>";
			echo "<a class='mybtn fa fa-plus' onClick='$(\"#id2\").val(\"".$row['application_id']."\"); $(\"#vladd\").val(\"SUBMIT\");$(\"#formulaire\").submit();'></a>";
			echo "</td>";

			echo "<td align='center'>";
			echo "<a href='".urldecode($row['application_url'])."' target='_blank'><img src='local/images/icon/".$row['icon_url']."' width='30px' height='30px'></img></a>";
			echo "</td>";
		
			echo "<td>";
			echo "<a href='".urldecode($row['application_url'])."' target='_blank'>".$row["application_name"]."</a><br>";
			echo "</td>";
	
			echo "</tr>";
		}

		echo "</table>";
	}
		
	elseif($tptbl=="PAGE") {
		echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
		echo "<thead>";
		echo "<th width='40px'>Action</th>";
		echo "<th>Page</th>";
		echo "<th>Type</th>";
		echo "<th>Propriétaire</th>";
		echo "</thead>";

		$q="SELECT * FROM ".$config["dbprefixe"]."panel, ".$config["dbprefixe"]."panel_type, ".$config["dbprefixe"]."user, ".$config["dbprefixe"]."profil
			WHERE panel_type_id=panel_type
			AND user_id=panel_user
			AND profil_id=user_profil
			AND NOT EXISTS(SELECT panel_profil_panel FROM ".$config["dbprefixe"]."panel_profil WHERE panel_profil_panel=panel_id AND panel_profil_profil=?)
			ORDER BY panel_order DESC";
		$query=$bdd01->prepare($q);
		$query->execute(array($id1));
		while($row=$query->fetch()){
			echo "<tr>";

			echo "<td align='center'>";
			echo "<a class='mybtn fa fa-plus' onClick='$(\"#id2\").val(\"".$row['panel_id']."\"); $(\"#vladd\").val(\"SUBMIT\");$(\"#formulaire\").submit();'></a>";
			echo "</td>";

			echo "<td>";
			echo $row["panel_label"];
			echo "</td>";
		
			echo "<td>";
			echo $row["panel_type_name"];
			echo "</td>";

			echo "<td>";
			echo $row['user_login'];
			echo "</td>";
	
			echo "</tr>";
		}

		echo "</table>";
	}

	elseif($tptbl=="PAGESSO") {
		echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
		echo "<thead>";
		echo "<th width='40px'>Action</th>";
		echo "<th>Page</th>";
		echo "<th>Type</th>";
		echo "<th>Propriétaire</th>";
		echo "</thead>";

		$q="SELECT * FROM ".$config["dbprefixe"]."panel, ".$config["dbprefixe"]."panel_type, ".$config["dbprefixe"]."user, ".$config["dbprefixe"]."profil
			WHERE panel_type_id=panel_type
			AND user_id=panel_user
			AND profil_id=user_profil
			AND NOT EXISTS(SELECT panel_sso_profil_panel FROM ".$config["dbprefixe"]."panel_sso_profil WHERE panel_sso_profil_panel=panel_id AND panel_sso_profil_sso_profil=?)
			ORDER BY panel_order DESC";
		$query=$bdd01->prepare($q);
		$query->execute(array($id1));
		while($row=$query->fetch()){
			echo "<tr>";

			echo "<td align='center'>";
			echo "<a class='mybtn fa fa-plus' onClick='$(\"#id2\").val(\"".$row['panel_id']."\"); $(\"#vladd\").val(\"SUBMIT\");$(\"#formulaire\").submit();'></a>";
			echo "</td>";

			echo "<td>";
			echo $row["panel_label"];
			echo "</td>";
		
			echo "<td>";
			echo $row["panel_type_name"];
			echo "</td>";

			echo "<td>";
			echo $row['user_login'];
			echo "</td>";
	
			echo "</tr>";
		}

		echo "</table>";
	}	


	elseif($tptbl=="PAGELDAP") {
		echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
		echo "<thead>";
		echo "<th width='40px'>Action</th>";
		echo "<th>Page</th>";
		echo "<th>Type</th>";
		echo "<th>Propriétaire</th>";
		echo "</thead>";

		$q="SELECT * FROM ".$config["dbprefixe"]."panel, ".$config["dbprefixe"]."panel_type, ".$config["dbprefixe"]."user, ".$config["dbprefixe"]."profil
			WHERE panel_type_id=panel_type
			AND user_id=panel_user
			AND profil_id=user_profil
			AND NOT EXISTS(SELECT panel_ldap_profil_panel FROM ".$config["dbprefixe"]."panel_ldap_profil WHERE panel_ldap_profil_panel=panel_id AND panel_ldap_profil_ldap_profil=?)
			ORDER BY panel_order DESC";
		$query=$bdd01->prepare($q);
		$query->execute(array($id1));
		while($row=$query->fetch()){
			echo "<tr>";

			echo "<td align='center'>";
			echo "<a class='mybtn fa fa-plus' onClick='$(\"#id2\").val(\"".$row['panel_id']."\"); $(\"#vladd\").val(\"SUBMIT\");$(\"#formulaire\").submit();'></a>";
			echo "</td>";

			echo "<td>";
			echo $row["panel_label"];
			echo "</td>";
		
			echo "<td>";
			echo $row["panel_type_name"];
			echo "</td>";

			echo "<td>";
			echo $row['user_login'];
			echo "</td>";
	
			echo "</tr>";
		}

		echo "</table>";
	}
	
	elseif($tptbl=="FLUX") {
		echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
		echo "<thead>";
		echo "<th width='40px'>Action</th>";
		echo "<th>Flux</th>";
		echo "</thead>";

		$q="SELECT * FROM ".$config["dbprefixe"]."flux
			WHERE NOT EXISTS(SELECT flux_profil_flux FROM ".$config["dbprefixe"]."flux_profil WHERE flux_profil_flux=flux_id AND flux_profil_profil=?)
			ORDER BY flux_order DESC";
		$query=$bdd01->prepare($q);
		$query->execute(array($id1));
		while($row=$query->fetch()){
			echo "<tr>";

			echo "<td align='center'>";
			echo "<a class='mybtn fa fa-plus' onClick='$(\"#id2\").val(\"".$row['flux_id']."\"); $(\"#vladd\").val(\"SUBMIT\");$(\"#formulaire\").submit();'></a>";
			echo "</td>";

			echo "<td>";
			echo $row["flux_name"];
			echo "</td>";
	
	
			echo "</tr>";
		}
	}
	
	elseif($tptbl=="FLUXSSO") {
		echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
		echo "<thead>";
		echo "<th width='40px'>Action</th>";
		echo "<th>Flux</th>";
		echo "</thead>";

		$q="SELECT * FROM ".$config["dbprefixe"]."flux
			WHERE NOT EXISTS(SELECT flux_sso_profil_flux FROM ".$config["dbprefixe"]."flux_sso_profil WHERE flux_sso_profil_flux=flux_id AND flux_sso_profil_profil=?)
			ORDER BY flux_order DESC";
		$query=$bdd01->prepare($q);
		$query->execute(array($id1));
		while($row=$query->fetch()){
			echo "<tr>";

			echo "<td align='center'>";
			echo "<a class='mybtn fa fa-plus' onClick='$(\"#id2\").val(\"".$row['flux_id']."\"); $(\"#vladd\").val(\"SUBMIT\");$(\"#formulaire\").submit();'></a>";
			echo "</td>";

			echo "<td>";
			echo $row["flux_name"];
			echo "</td>";
	
	
			echo "</tr>";
		}
		echo "</table>";
	}
	
	elseif($tptbl=="FLUXLDAP") {
		echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
		echo "<thead>";
		echo "<th width='40px'>Action</th>";
		echo "<th>Flux</th>";
		echo "</thead>";

		$q="SELECT * FROM ".$config["dbprefixe"]."flux
			WHERE NOT EXISTS(SELECT flux_ldap_profil_flux FROM ".$config["dbprefixe"]."flux_ldap_profil WHERE flux_ldap_profil_flux=flux_id AND flux_ldap_profil_profil=?)
			ORDER BY flux_order DESC";
		$query=$bdd01->prepare($q);
		$query->execute(array($id1));
		while($row=$query->fetch()){
			echo "<tr>";

			echo "<td align='center'>";
			echo "<a class='mybtn fa fa-plus' onClick='$(\"#id2\").val(\"".$row['flux_id']."\"); $(\"#vladd\").val(\"SUBMIT\");$(\"#formulaire\").submit();'></a>";
			echo "</td>";

			echo "<td>";
			echo $row["flux_name"];
			echo "</td>";
	
	
			echo "</tr>";
		}
		echo "</table>";
	}

	elseif($tptbl=="SSOCOMMUNITY") {
		echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
		echo "<thead>";
		echo "<th width='40px'>Action</th>";
		echo "<th>Nom</th>";
		echo "<th>Valeur</th>";
		echo "</thead>";


		$q="SELECT * FROM ".$config["dbprefixe"]."sso_attribut
			WHERE NOT EXISTS(SELECT sso_community_attribut_community FROM ".$config["dbprefixe"]."sso_community_attribut WHERE sso_community_attribut_attribut=sso_attribut_id AND sso_community_attribut_community=?)";
		$query=$bdd01->prepare($q);
		$query->execute(array($id1));
		while($row=$query->fetch()){	
			echo "<tr>";

			echo "<td align='center'>";
			echo "<a class='mybtn fa fa-plus' onClick='$(\"#id2\").val(\"".$row['sso_attribut_id']."\"); $(\"#vladd\").val(\"SUBMIT\");$(\"#formulaire\").submit();'></a>";
			echo "</td>";

			echo "<td>";
			echo $row["sso_attribut_name"];
			echo "</td>";
		
			echo "<td>";
			echo $row["sso_attribut_value"];
			echo "</td>";

			echo "</tr>";
		}

		echo "</table>";
	}	

	elseif($tptbl=="SSOPROFIL") {
		echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
		echo "<thead>";
		echo "<th width='40px'>Action</th>";
		echo "<th>Nom</th>";
		echo "<th>Valeur</th>";
		echo "</thead>";

	
		$q="SELECT * FROM ".$config["dbprefixe"]."sso_attribut
			WHERE NOT EXISTS(SELECT sso_profil_attribut_profil FROM ".$config["dbprefixe"]."sso_profil_attribut WHERE sso_profil_attribut_attribut=sso_attribut_id AND sso_profil_attribut_profil=?)";
		$query=$bdd01->prepare($q);
		$query->execute(array($id1));
		while($row=$query->fetch()){	
			echo "<tr>";

			echo "<td align='center'>";
			echo "<a class='mybtn fa fa-plus' onClick='$(\"#id2\").val(\"".$row['sso_attribut_id']."\"); $(\"#vladd\").val(\"SUBMIT\");$(\"#formulaire\").submit();'></a>";
			echo "</td>";

			echo "<td>";
			echo $row["sso_attribut_name"];
			echo "</td>";
		
			echo "<td>";
			echo $row["sso_attribut_value"];
			echo "</td>";

			echo "</tr>";
		}

		echo "</table>";
	}	
				
	echo "<form>";
}
?>

<!-- SCRIPT --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<script>
	$(document).ready(function() {
		$('#datatable').dataTable( {
			"oLanguage": { "sUrl": "lib/dataTables/dataTables.txt" },
			"iDisplayLength": <? if($tptbl=="ITEM"||$tptbl=="ITEMSSO"||$tptbl=="ITEMLDAP") echo "6"; else echo "9"; ?>,
			"aoColumnDefs" : [ {'bSortable' : false, 'aTargets' : [ 0 ]} ],
			"aaSorting": [[ 1, "asc" ]],
			"bLengthChange": false,
			"stateSave": true
		} );
	} );	
	

	function recharge() {
		window.parent.ModalRecharge();
		window.parent.ModalClose('#<? echo $_SESSION[$keymodal]["idmodal"] ?>');
	}
	
	<?php echo $jsaction ?>
</script>
