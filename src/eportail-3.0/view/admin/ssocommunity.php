<?
	$levelpage=1;
	include("include/permission.php");
		
	$tpmod				=$_POST['tpmod'];
	$vlmod				=$_POST['vlmod'];
	$vladd				=$_POST['vladd'];
	$vlsup				=$_POST['vlsup'];
	$fgreload			=$_POST['fgreload'];
		
	$id					=$_POST['id'];
	$label				=$_POST['label'];
	
	/*--> Controle de cohérance */
	if($vladd!=""||$vlmod!="") {
		$fgerr="";
		if($label=="") {
			$jsaction="alert('Vous devez renseigner l\'ensemble des informations');";
			$fgerr=1;
		}
	}
	
	/*--> Rester sur le mode encours */
	if($vlmod!=""&&fgerr!="") $tpmod="MODIFY";
	if($vladd!=""&&fgerr!="") $tpmod="SUBMIT";
		
	/*--> Submit */
	if($vladd!=""&&$fgerr=="") {
		$q="INSERT INTO ".$config["dbprefixe"]."sso_community(sso_community_label) VALUES(?)";
		$query=$bdd01->prepare($q);
		$query->execute(array($label));		
		$id=$bdd01->lastInsertId();
		$tpmod="MODIFY";
	}

	/*--> Modify */
	if($vlmod!=""&&$fgerr=="") {
		$q="UPDATE ".$config["dbprefixe"]."sso_community SET sso_community_label=? WHERE sso_community_id=?";
		$query=$bdd01->prepare($q);
		$query->execute(array($label,$id));		
		$tpmod="";
	}

	/*--> Delete */
	if($vlsup!="") {
		delSsoCommunity($id);
	}
	
	/*--> Reload */	
	if($fgreload!="") {
		$tpmod=$fgreload; 
		$vladd="";
		$vlmod="";
		$vldel="";
	}

	echo "<form id='formulaire' class='form-horizontal' role='form' method='post' enctype='multipart/form-data'>";
	
	echo "<input id='id' name='id' type='hidden' value='".$id."'>";
	echo "<input id='tpmod' name='tpmod' type='hidden' value=''>";
	echo "<input id='fgreload' name='fgreload' type='hidden' value=''>";
  
//-- DISPLAY -------------------------------------------------------------------------------------------------------------------------------

if($tpmod=="") { 
    echo "<legend><h1>GESTION DES COMMUNAUTES SSO</h1></legend>";
    
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='fgadd' name='fgadd' class='btn btn-primary' onClick='$(\"#id\").val(\"\"); $(\"#tpmod\").val(\"SUBMIT\");' type='submit' value='Ajouter' />";
	echo "</div>";
	echo "</div>";
		
	echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
    echo "<thead>";
	echo "<th width='50px'>Action</th>";
	echo "<th >Libellé</th>";
	echo "<th >Attribut</th>";
	echo "</thead>";

	$q="SELECT * FROM ".$config["dbprefixe"]."sso_community ORDER BY sso_community_id";
	$query=$bdd01->prepare($q);
	$query->execute();
	while($row=$query->fetch()){	
		echo "<tr>";

		echo "<td align='center'>";
		if($row['sso_community_id']>0) {
			echo "<a class='mybtn fa fa-file' onClick='$(\"#id\").val(\"".$row['sso_community_id']."\"); $(\"#tpmod\").val(\"MODIFY\");$(\"#formulaire\").submit();'></a>";
			echo "&nbsp;";
			echo "<a class='mybtn fa fa-trash'       onClick='$(\"#id\").val(\"".$row['sso_community_id']."\"); $(\"#tpmod\").val(\"DELETE\");$(\"#formulaire\").submit();'></a>";
		}
		echo "</td>";

		echo "<td>";
		echo $row['sso_community_label'];
		echo "</td>";

		echo "<td>";
		$q="SELECT * FROM ".$config["dbprefixe"]."sso_attribut, ".$config["dbprefixe"]."sso_community_attribut WHERE sso_community_attribut_community=? AND sso_attribut_id=sso_community_attribut_attribut";
		$query2=$bdd02->prepare($q);
		$query2->execute(array($row["sso_community_id"]));
		$i=0;
		while($row2=$query2->fetch()){	
			if($i>0) echo "<br>";
			echo $row2["sso_attribut_name"]." = ".$row2["sso_attribut_value"];
			$i=$i+1;
		}
		echo "</td>";
		
		echo "</tr>";
	} 
	
	echo "</table>";
}

//-- SUBMIT --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="SUBMIT") {
	// Entete du formulaire
    echo "<legend><h1>AJOUT COMMUNAUTE SSO</h1></legend>";
    
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vladd' name='vladd' class='btn btn-primary' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
	echo "</div>";
	echo "</div>";	

	// Valeur par défaut
	// aucune
}

//-- MODIFY --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="MODIFY") {
	// Entete du formulaire
    echo "<legend><h1>MODIFICATION COMMUNAUTE SSO</h1></legend>";
   
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlmod' name='vlmod' class='btn btn-primary' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
	echo "</div>";
	echo "</div>";	

	// Valeur par défaut
	$q="SELECT * FROM ".$config["dbprefixe"]."sso_community WHERE sso_community_id=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
	if($row=$query->fetch()){	
		$label 		= $row['sso_community_label'];
	}
}

//-- DELETE --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="DELETE") {
	echo "<legend><h1>SUPPRESSION COMMUNAUTE SSO</h1></legend>";
	
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlsup' name='vlsup' class='btn btn-primary' type='submit' value='Confirmer la suppression' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";    
	echo "</div>";
	echo "</div>";
	
	// Valeur par défaut
	$q="SELECT * FROM ".$config["dbprefixe"]."sso_community WHERE sso_community_id=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
	if($row=$query->fetch()){	
		$label 		= $row['sso_community_label'];
	}
}


//-- FORMULAIRE -----------------------------------------------------------------------------------------------------------------------------

if($tpmod=="SUBMIT"||$tpmod=="MODIFY"||$tpmod=="DELETE") {
?>
	<fieldset class="row fieldset" style="clear:both">
		<legend>Description</legend>

		<div class="form-group">
			<label for="id_bis" class="col-sm-3 control-label">ID*</label>
			<div class="col-sm-6"><input name="id_bis" id="id_bis" type="titre" class="form-control" disabled="disabled" placeholder="ID Application" value="<? echo $id; ?>"></div>
		</div>
		

		<div class="form-group">
			<label for="label" class="col-sm-3 control-label">Libellé*</label>
			<div class="col-sm-6"><input name="label" id="label" type="titre" class="form-control" placeholder="Libellé" value="<? echo $label; ?>"></div>
		</div>
		
		<?
			if($tpmod=="MODIFY") {
				echo "<legend>Attributs</legend>";
				echo "<div class='form-group'>";
				echo "<div class='col-sm-12'>";
				echo "<a class='btn btn-primary' data-toggle='modal' data-target='#mymodal-01' title='Séléctionner un Attribut' onClick='ModalLoad(\"mymodal-01\",\"ssocommunity-insert\",$id,0)'>Ajouter</a>";
				echo "</div>";
				echo "</div>";

			
				echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
				echo "<thead>";
				echo "<th width='50px'>Action</th>";
				echo "<th>Nom</th>";
				echo "<th>Valeur</th>";
				echo "</thead>";

				$q="SELECT * FROM ".$config["dbprefixe"]."sso_attribut, ".$config["dbprefixe"]."sso_community_attribut WHERE sso_community_attribut_community=? AND sso_attribut_id=sso_community_attribut_attribut";
				$query=$bdd01->prepare($q);
				$query->execute(array($id));
				while($row=$query->fetch()){	
					echo "<tr>";
					
					echo "<td align='center'>";
					echo "<a class='mybtn fa fa-trash' data-toggle='modal' data-target='#mymodal-01' onClick='ModalLoad(\"mymodal-01\",\"ssocommunity-delete\",$id,".$row["sso_attribut_id"].")' title='Supprimer un attribut' />";
					echo "</td>";
								
					echo "<td>";
					echo $row["sso_attribut_name"];
					echo "</td>";
				
					echo "<td>";
					echo $row["sso_attribut_value"];
					echo "</td>";					
					
					echo "</tr>";
				}
				
				echo "</table>";
			}
			elseif($tpmod=="SUBMIT") {
				echo "<div class='form-group'><center><br>Veuillez valider avant de pouvoir ajouter un attribut</center></div>";
			}
		?>	
	</fieldset>	
<?
}

echo "</form>";

?>

<!-- SCRIPT --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<? if($tpmod==""||$tpmod=="MODIFY") { ?>
	<script>
		$(document).ready(function() {
			$('#datatable').dataTable( {
				"oLanguage": { "sUrl": "lib/dataTables/dataTables.txt" },
				"iDisplayLength": 100,
				"aoColumnDefs" : [ {'bSortable' : false, 'aTargets' : [ 0 ]} ],
				"aaSorting": [[ 1, "asc" ]],
				"stateSave": true
			} );
		} );	
	</script>
<? } ?>

<? if($tpmod=="SUBMIT"||$tpmod=="MODIFY"||$tpmod=="DELETE") { ?>
	<script type="text/javascript">
		$('#label').focus();
	</script>
<? } ?>

<script>
		<?php echo $jsaction ?>
</script>



	
	
