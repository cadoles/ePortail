<?
	$levelpage=1;
	include("include/permission.php");
	
	$tpmod			=$_POST['tpmod'];
	$vladd			=$_POST['vladd'];
	$vlsup			=$_POST['vlsup'];
	
	$icon_id		=$_POST['icon_id'];
	$icon_url		=$_POST['icon_url'];

	if($_GET['tpmod']!="") {
		$tpmod=$_GET['tpmod'];
		$igico=$_GET['igico'];
		$idico=$_GET['idico'];
	}
	
	/*--> Delete */
	if($vlsup!="") {
		$q="DELETE FROM ".$config["dbprefixe"]."icon WHERE icon_id=?";
		$query2=$bdd02->prepare($q);
		$query2->execute(array($icon_id));		
		$tpmod="";
	}
	

	echo "<form id='formulaire' class='form-horizontal' role='form' method='post' enctype='multipart/form-data'>";
	
	echo "<input id='icon_id' name='icon_id' type='hidden' value='".$icon_id."'>";
	echo "<input id='tpmod' name='tpmod' type='hidden' value=''>";
	
  
//-- DISPLAY -------------------------------------------------------------------------------------------------------------------------------

if($tpmod==""||$tpmod=="SELECT") { 
    if($tpmod=="") {
		echo "<legend><h1>GESTION DES ICÔNES</h1></legend>";
    
		echo "<div class='form-group'>";
		echo "<div class='col-sm-12'>";
		echo "<input id='fgadd' name='fgadd' class='btn btn-primary' data-toggle='modal' data-target='#mymodal-01' value='Ajouter avec découpe' onClick='ModalLoad(\"mymodal-01\",\"icon-cropinsert\",0,0)'>";
		echo "&nbsp;";
		echo "<input id='fgupl' name='fgupl' class='btn btn-primary' data-toggle='modal' data-target='#mymodal-01' value='Ajouter sans découpe' onClick='ModalLoad(\"mymodal-01\",\"icon-dropinsert\",0,0)'>";
		echo "</div>";
		echo "</div>";
	}

	$q="SELECT * FROM ".$config["dbprefixe"]."icon";
	$query=$bdd01->prepare($q);
	$query->execute();
	while($row=$query->fetch()){	
		if($tpmod=="") echo "<div style='width:85px; float:left; '>";
		else echo "<div style='width:65px; float:left; '>";
		
		echo "<div id='myavatarprofil' style='float:left; margin-right:5px; margin-bottom:5px; border:none;'>";
		if($tpmod=="SELECT") echo "<a href='' onClick='selIcone(".$$row['icon_id'].",\"".$config['urlbase']."/local/images/icon/".$row['icon_url']."\")'>";
		echo "<img id='imgapp".$row['icon_id']."' src='".$repository."local/images/icon/".$row['icon_url']."' width='60px' height='60px'></img>";
		if($tpmod=="SELECT") echo "</a>";
		echo "</div>";
		
		if($tpmod=="") {
			if($row['icon_id']>0) {
				echo "<div style='float:left;'>";
				echo "<a class='mybtn fa fa-crop' data-toggle='modal' data-target='#mymodal-01' title='Modifier un icône avec découpe' onClick='ModalLoad(\"mymodal-01\",\"icon-cropupdate\",".$row['icon_id'].",0)'></a>";
				echo "<br><a class='mybtn fa fa-file' data-toggle='modal' data-target='#mymodal-01' title='Modifier un icône sans découpe' onClick='ModalLoad(\"mymodal-01\",\"icon-dropupdate\",".$row['icon_id'].",0)'></a>";

				// Pas de suppression possible si icone lié à une application
				$q="SELECT application_id FROM ".$config["dbprefixe"]."application WHERE application_icon=?";
				$query2=$bdd02->prepare($q);
				$query2->execute(array($row['icon_id']));
				if(!($row2=$query2->fetch())) {
					// Pas de suppression possible si icone lié à un widget
					$q="SELECT widget_id FROM ".$config["dbprefixe"]."widget WHERE widget_icon=?";
					$query2=$bdd02->prepare($q);
					$query2->execute(array($row['icon_id']));
					if(!($row2=$query2->fetch())) {
						// Pas de suppression possible si icone lié à un bookmark
						$q="SELECT panel_widget_bookmark_id FROM ".$config["dbprefixe"]."panel_widget_bookmark WHERE panel_widget_bookmark_icon=?";
						$query2=$bdd02->prepare($q);
						$query2->execute(array($row['icon_id']));
						if(!($row2=$query2->fetch())) {						
							echo "<br><a class='mybtn fa fa-trash' onClick='$(\"#icon_id\").val(\"".$row['icon_id']."\"); $(\"#tpmod\").val(\"DELETE\");$(\"#formulaire\").submit();' title='Supprimer icône'></a>";
						}
					}
				}
				
				echo "</div>";
			}
		}
		
		echo "</div>";
	} 
}

//-- DELETE --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="DELETE") {
	echo "<legend><h1>SUPPRESSION ICONE</h1></legend>";
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";	
	echo "<input id='vlsup' name='vlsup' class='btn btn-primary' type='submit' value='Confirmer la suppression' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";    
	echo "</div>";
	echo "</div>";


	$q="SELECT * FROM ".$config["dbprefixe"]."icon  WHERE icon_id=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($icon_id));
	if($row=$query->fetch()) {
		echo "<br><div id='appicon' class='appicon'>";
		echo "<img id='imgapp".$row['icon_id']."' src='local/images/icon/".$row['icon_url']."' width='90px' height='90px'></img>";
		echo "</div>";
	}
}

echo "</form>";

?>


<!-- SCRIPT --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<script type="text/javascript">
	function selIcone(idico,igico) {
		parent.document.getElementById('<? echo $idico; ?>').value=idico;
		parent.document.getElementById('<? echo $igico; ?>').src=igico;
		window.parent.closemodal();	
	}
</script>








	
	
