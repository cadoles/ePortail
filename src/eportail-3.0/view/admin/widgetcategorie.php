<?
	$levelpage=1;
	include("include/permission.php");
	
	$tpmod				=$_POST['tpmod'];
	$vlmod				=$_POST['vlmod'];
	$vladd				=$_POST['vladd'];
	$vlsup				=$_POST['vlsup'];
	
	$id					=$_POST['id'];
	$label				=$_POST['label'];

		
	/*--> Controle de cohérance */
	if($vladd!=""||$vlmod!="") {
		$fgerr="";


		if($label=="") {
			$jsaction="alert('Les champs avec * sont obligatoires');";
			$fgerr=1;
		}


		$q="SELECT * FROM ".$config["dbprefixe"]."widget_categorie WHERE widget_categorie_label=?";
		$tbq=array($label);
		if($vlmod!="") {
			$q=$q." AND widget_categorie_id!=?";
			array_push($tbq,$id);
		}
		$query=$bdd01->prepare($q);
		$query->execute($tbq);
		if($row=$query->fetch()){		
			$jsaction="alert('Une catégorie avec ce label existe déjà');";
			$fgerr=1;
		}
	}
	
	/*--> Rester sur le mode encours */
	if($vlmod!=""&&fgerr!="") $tpmod="MODIFY";
	if($vladd!=""&&fgerr!="") $tpmod="SUBMIT";
		
	/*--> Submit */
	if($vladd!=""&&$fgerr=="") {
		$q="INSERT INTO ".$config["dbprefixe"]."widget_categorie(widget_categorie_label) VALUES(?)";
		$query=$bdd01->prepare($q);
		$query->execute(array($label));
		$tpmod="";
	}

	/*--> Modify */
	if($vlmod!=""&&$fgerr=="") {
		$q="UPDATE ".$config["dbprefixe"]."widget_categorie SET widget_categorie_label=? WHERE widget_categorie_id=?";
		$query=$bdd01->prepare($q);
		$query->execute(array($label,$id));
		$tpmod="";
	}
	
	/*--> Delete */
	if($vlsup!="") {
		$q="DELETE FROM ".$config["dbprefixe"]."widget_categorie WHERE widget_categorie_id=?";
		$query=$bdd01->prepare($q);
		$query->execute(array($id));
		$tpmod="";
	}

	echo "<form id='formulaire' class='form-horizontal' role='form' method='post' enctype='multipart/form-data'>";
	
	
	echo "<input id='id' name='id' type='hidden' value='".$id."'>";
	echo "<input id='tpmod' name='tpmod' type='hidden' value=''>";
	
  
//-- DISPLAY -------------------------------------------------------------------------------------------------------------------------------

if($tpmod=="") { 
    echo "<legend><h1>GESTION DES CATEGORIES DE WIDGET</h1></legend>";
	
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='fgadd' name='fgadd' class='btn btn-primary' onClick='$(\"#id\").val(\"\"); $(\"#tpmod\").val(\"SUBMIT\");' type='submit' value='Ajouter' />";
	echo "</div>";
	echo "</div>";
    
	echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
    echo "<thead>";
	echo "<th width='40px'>Action</th>";
	echo "<th width='70px'>ID</th>";
	echo "<th>Nom</th>";
	echo "</thead>";
	
	$q="SELECT * FROM ".$config["dbprefixe"]."widget_categorie";
	$query=$bdd01->prepare($q);
	$query->execute();
	while($row=$query->fetch()){	
		echo "<tr>";

		echo "<td align='center'>";
		echo "<a class='mybtn fa fa-file' onClick='$(\"#id\").val(\"".$row['widget_categorie_id']."\"); $(\"#tpmod\").val(\"MODIFY\");$(\"#formulaire\").submit();'></a>";
		
		// Pas de suppression possible si catégorie liée à une application
		$q="SELECT widget_id FROM ".$config["dbprefixe"]."widget WHERE widget_categorie=?";
		$query2=$bdd02->prepare($q);
		$query2->execute(array($row['widget_categorie_id']));
		if(!$row2=$query2->fetch()){	
			echo "&nbsp;";
			echo "<a class='mybtn fa fa-trash' onClick='$(\"#id\").val(\"".$row['widget_categorie_id']."\"); $(\"#tpmod\").val(\"DELETE\");$(\"#formulaire\").submit();'></a>";
		}		
		echo "</td>";

		echo "<td align='center'>";
		echo $row['widget_categorie_id'];
		echo "</td>";
		
		echo "<td>";
		echo $row['widget_categorie_label']."<br>";
		echo "</td>";

		echo "</tr>";
	} 
	echo "</table>";
}

//-- SUBMIT --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="SUBMIT") {
	// Entete du formulaire
    echo "<legend><h1>AJOUT CATEGORIE DE WIDGET</h1></legend>";

	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vladd' name='vladd' class='btn btn-primary' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
	echo "</div>";
	echo "</div>";		

	// Valeur par défaut
	$order = 0;
}

//-- MODIFY --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="MODIFY") {
	// Entete du formulaire
    echo "<legend><h1>MODIFICATION CATEGORIE DE WIDGET</h1></legend>";

	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlmod' name='vlmod' class='btn btn-primary' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
	echo "</div>";
	echo "</div>";		

	// Valeurs
	$q = "SELECT * FROM ".$config["dbprefixe"]."widget_categorie WHERE widget_categorie_id=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
	if($row=$query->fetch()){	
		$label 	= $row["widget_categorie_label"];
	}
	
	if($order=="") $order=0;
}

//-- DELETE --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="DELETE") {
	echo "<legend><h1>SUPPRESSION CATEGORIE DE WIDGET</h1></legend>";
	
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlsup' name='vlsup' class='btn btn-primary' type='submit' value='Confirmer la suppression' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";    
	echo "</div>";
	echo "</div>";	
	
	// Valeurs
	$q = "SELECT * FROM ".$config["dbprefixe"]."widget_categorie WHERE widget_categorie_id=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
	if($row=$query->fetch()){	
		$label 	= $row["widget_categorie_label"];
	}
}


//-- FORMULAIRE -----------------------------------------------------------------------------------------------------------------------------

if($tpmod=="SUBMIT"||$tpmod=="MODIFY"||$tpmod=="DELETE") {
?>

	<fieldset class="row fieldset" style="clear:both">
		<div class="form-group">
			<label for="id_bis" class="col-sm-3 control-label">ID*</label>
			<div class="col-sm-6"><input name="id_bis" id="id_bis" type="titre" class="form-control" disabled="disabled" placeholder="ID" value="<? echo $id; ?>"></div>
		</div>


		<div class="form-group">
			<label for="label" class="col-sm-3 control-label">Nom*</label>
			<div class="col-sm-6"><input name="label" id="label" type="titre" class="form-control" placeholder="Nom" value="<? echo $label; ?>"></div>
		</div>
	</fieldset>

<?
}

echo "</form>";
?>

<!-- SCRIPT --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<? if($tpmod=="") { ?>
	<script>
		$(document).ready(function() {
			$('#datatable').dataTable( {
				"oLanguage": { "sUrl": "lib/dataTables/dataTables.txt" },
				"iDisplayLength": 100,
				"aoColumnDefs" : [ {'bSortable' : false, 'aTargets' : [ 0 ]} ],
				"aaSorting": [[ 1, "asc" ]],
				"stateSave": true
			} );
		} );	
	</script>
<? } ?>

<? if($tpmod=="SUBMIT"||$tpmod=="MODIFY"||$tpmod=="DELETE") { ?>
	<script type="text/javascript">
		$('#label').focus();
	</script>
<? } ?>

<script>
		<?php echo $jsaction ?>
</script>






	
	
