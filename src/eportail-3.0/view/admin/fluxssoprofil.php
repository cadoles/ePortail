<?
	$levelpage=2;
	include("include/permission.php");

	$id					=$_POST['id'];
	$tpmod				=$_POST['tpmod'];
	$fgreload			=$_POST['fgreload'];	

	if($fgreload!="") {
		$tpmod=$fgreload; 
	}
		
	echo "<form id='formulaire' class='form-horizontal' role='form' method='post' enctype='multipart/form-data'>";
	
	echo "<input id='id' name='id' type='hidden' value='".$id."'>";
	echo "<input id='tpmod' name='tpmod' type='hidden' value=''>";
	echo "<input id='fgreload' name='fgreload' type='hidden' value=''>";
	
  
//-- DISPLAY -------------------------------------------------------------------------------------------------------------------------------

if($tpmod=="") { 
    echo "<legend><h1>GESTION DES FLUX PAR PROFIL SSO</h1></legend>";
    
	echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
    echo "<thead>";
	echo "<th width='40px'>Action</th>";
	echo "<th>Communauté</th>";
	echo "<th>Profil</th>";
	echo "<th>Flux</th>";
	echo "</thead>";
	
	$q="SELECT * FROM ".$config["dbprefixe"]."sso_profil, ".$config["dbprefixe"]."sso_community WHERE sso_profil_community=sso_community_id";
	$query=$bdd01->prepare($q);
	$query->execute();
	while($row=$query->fetch()){
		echo "<tr>";

		echo "<td align='center'>";
		echo "<a class='mybtn fa fa-file' onClick='$(\"#id\").val(\"".$row['sso_profil_id']."\"); $(\"#tpmod\").val(\"MODIFY\");$(\"#formulaire\").submit();'></a>";
		echo "</td>";

		echo "<td>";
		echo $row['sso_community_label'];
		echo "</td>";
		
		
		echo "<td>";
		echo $row['sso_profil_label'];
		echo "</td>";

		echo "<td>";
		$q="SELECT * FROM ".$config["dbprefixe"]."flux_sso_profil, ".$config["dbprefixe"]."flux WHERE flux_sso_profil_profil=? AND flux_sso_profil_flux=flux_id ORDER BY flux_order";
		$query2=$bdd02->prepare($q);
		$query2->execute(array($row["sso_profil_id"]));
		while($row2=$query2->fetch()){
			echo $row2["flux_name"]."<br>";
		}
		echo "</td>";

		echo "</tr>";
	} 

	echo "</table>";
}

//-- MODIFY --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="MODIFY") {
	// Entete du formulaire
    echo "<legend><h1>MODIFICATION PAGE PAR PROFIL SSO</h1></legend>";

	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlmod' name='vlmod' class='btn btn-primary' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
	echo "</div>";
	echo "</div>";	
	    

	// Valeur par défaut
	$q = "SELECT * FROM ".$config["dbprefixe"]."sso_profil WHERE sso_profil_id=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
	if($row=$query->fetch()){
		$label 		= $row['sso_profil_label'];
	}
}

//-- DELETE --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="DELETE") {

}


//-- FORMULAIRE -----------------------------------------------------------------------------------------------------------------------------

if($tpmod=="SUBMIT"||$tpmod=="MODIFY") {
?>

	<fieldset class="row fieldset" style="clear:both">
		<legend>Profil</legend>
		
		<div class="form-group">
			<label for="id_bis" class="col-sm-3 control-label">ID Profil*</label>
			<div class="col-sm-6"><input name="id_bis" id="id_bis" type="titre" class="form-control" disabled="disabled" placeholder="ID Application" value="<? echo $id; ?>"></div>
		</div>
		
		<div class="form-group">
			<label for="label" class="col-sm-3 control-label">Nom*</label>
			<div class="col-sm-6"><input name="label" id="label" type="titre" class="form-control" disabled="disabled" placeholder="Nom" value="<? echo $label; ?>"></div>
		</div>
	</fieldset>
	
	<fieldset class="row fieldset" style="clear:both">
		<legend>Liste des Flux</legend>
		<?
		echo "<div class='form-group'>";
		echo "<div class='col-sm-12'>";
		echo "<a class='btn btn-primary' data-toggle='modal' data-target='#mymodal-01' title='Séléctionner un flux' onClick='ModalLoad(\"mymodal-01\",\"fluxssoprofil-insert\",$id,0)'>Ajouter</a>";
		echo "</div>";
		echo "</div>";	

		echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
		echo "<thead>";
		echo "<th width='40px'>Action</th>";
		echo "<th>Ordre</th>";
		echo "<th>Flux</th>";
		echo "</thead>";

		$q="SELECT * FROM ".$config["dbprefixe"]."flux_sso_profil, ".$config["dbprefixe"]."flux WHERE flux_sso_profil_profil=? AND flux_sso_profil_flux=flux_id ORDER BY flux_order";
		$query=$bdd01->prepare($q);
		$query->execute(array($id));
		while($row=$query->fetch()){
			echo "<tr>";

			echo "<td align='center'>";
			echo "<a class='mybtn fa fa-trash' data-toggle='modal' data-target='#mymodal-01' onClick='ModalLoad(\"mymodal-01\",\"fluxssoprofil-delete\",$id,".$row["flux_id"].")' title='Supprimer un flux' />";
			echo "</td>";
			
			echo "<td>".$row["flux_order"]."</td>";
			echo "<td>".$row["flux_name"]."</td>";
					
			echo "</tr>";
		}		
		
		echo "</table>";
		?>
	
	</fieldset>
	<?
}

echo "</form>";
?>

<!-- SCRIPT --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<? if($tpmod=="") { ?>
	<script>
		$(document).ready(function() {
			$('#datatable').dataTable( {
				"oLanguage": { "sUrl": "lib/dataTables/dataTables.txt" },
				"iDisplayLength": 100,
				"aoColumnDefs" : [ {'bSortable' : false, 'aTargets' : [ 0 ]} ],
				"aaSorting": [[ 1, "asc" ]],
				"stateSave": true
			} );
		} );	
	</script>
<? } ?>

<? if($tpmod=="SUBMIT"||$tpmod=="MODIFY"||$tpmod=="DELETE") { ?>
	<script type="text/javascript">
		$('#name').focus();

		$(document).ready(function() {
			$('#datatable').dataTable( {
				"oLanguage": { "sUrl": "lib/dataTables/dataTables.txt" },
				"iDisplayLength": 100,
				"aoColumnDefs" : [ {'bSortable' : false, 'aTargets' : [ 0 ]} ],
				"aaSorting": [[ 1, "asc" ]],
				"stateSave": true
			} );
		} );	
	</script>
<? } ?>

<script>
		<?php echo $jsaction ?>
</script>







	
	
