<?
	$levelpage=1;
	include("include/permission.php");
	
	$tpmod					=$_POST['tpmod'];
	$vlmod					=$_POST['vlmod'];
	$vladd					=$_POST['vladd'];
	$vlsup					=$_POST['vlsup'];
	
	$widget_id				=$_POST['widget_id'];
	$widget_name 			=$_POST['widget_name'];
	$widget_label 			=$_POST['widget_label'];
	$widget_url 			=$_POST['widget_url'];
	$widget_adjust			=$_POST['widget_adjust'];
	$widget_height			=$_POST['widget_height'];
	$widget_style 			=$_POST['widget_style'];
	$widget_type			=$_POST['widget_type'];
	$widget_mode			=$_POST['widget_mode'];
	$widget_icon			=$_POST['widget_icon'];
	$widget_code			=$_POST['widget_code'];
	$widget_categorie		=$_POST['widget_categorie'];
	
	
	/*--> Controle de cohérance */
	if($vladd!=""||$vlmod!="") {
		$fgerr="";

		if($widget_name==""||$widget_label==""||$widget_adjust==""||$widget_height==""||$widget_style==""||$widget_mode==""||$widget_type==""||$widget_categorie==""||$widget_icon=="") {
			$jsaction="alert('Vous devez renseigner l\'ensemble des informations');";
			$fgerr=1;
		}
	}
	
	/*--> Rester sur le mode encours */
	if($vlmod!=""&&fgerr!="") $tpmod="MODIFY";
	if($vladd!=""&&fgerr!="") $tpmod="SUBMIT";
		
	/*--> Submit */
	if($vladd!=""&&$fgerr=="") {
		$q="INSERT INTO ".$config["dbprefixe"]."widget(widget_name, widget_label, widget_url, widget_code, widget_adjust, widget_height, widget_style, widget_type, widget_mode, widget_icon, widget_categorie) VALUES(?,?,?,?,?,?,?,?,?,?,?)";
		$query=$bdd01->prepare($q);
		$query->execute(array($widget_name,$widget_label,$widget_url,$widget_code,$widget_adjust,$widget_height."px",$widget_style,$widget_type,$widget_mode,$widget_icon,$widget_categorie));
		$tpmod="";
	}

	/*--> Modify */
	if($vlmod!=""&&$fgerr=="") {
		$q="UPDATE ".$config["dbprefixe"]."widget SET widget_name=?, widget_label=?, widget_url=?, widget_code=?, widget_adjust=?, widget_height=?, widget_style=?, widget_type=?, widget_mode=?, widget_icon=?, widget_categorie=? WHERE widget_id=?";
		$query=$bdd01->prepare($q);
		$query->execute(array($widget_name,$widget_label,$widget_url,$widget_code,$widget_adjust,$widget_height."px",$widget_style,$widget_type,$widget_mode,$widget_icon,$widget_categorie,$widget_id));
		$tpmod="";
	}
	
	/*--> Delete */
	if($vlsup!="") {
		delWidget($widget_id);
		$tpmod="";
	}

	echo "<form id='formulaire' class='form-horizontal' role='form' method='post' enctype='multipart/form-data'>";
	
	echo "<input id='widget_id' name='widget_id' type='hidden' value='".$widget_id."'>";
	echo "<input id='tpmod' name='tpmod' type='hidden' value=''>";
	
  
//-- DISPLAY -------------------------------------------------------------------------------------------------------------------------------

if($tpmod=="") { 
    echo "<legend><h1>GESTION DES WIDGETS</h1></legend>";

	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='fgadd' name='fgadd' class='btn btn-primary' onClick='$(\"#widget_id\").val(\"\"); $(\"#tpmod\").val(\"SUBMIT\");' type='submit' value='Ajouter' />";
	echo "</div>";
	echo "</div>";
	
	echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
    echo "<thead>";
	echo "<th width='40px'>Action</th>";
	//echo "<th width='40px'>ID</th>";
	echo "<th width='40px'>Icone</th>";
	echo "<th width='110px'>Catégorie</th>";
	echo "<th>Nom</th>";
	//echo "<th>Description</th>";
	//echo "<th width='40px'>Hauteur</th>";
	echo "<th width='100px'>Type</th>";
	//echo "<th>Mode</th>";
	echo "</thead>";
	
	$q="SELECT * FROM ".$config["dbprefixe"]."widget, ".$config["dbprefixe"]."widget_type, ".$config["dbprefixe"]."widget_mode, ".$config["dbprefixe"]."icon WHERE widget_type=widget_type_id AND widget_mode=widget_mode_id AND widget_icon=icon_id";
	$query=$bdd01->prepare($q);
	$query->execute();
	while($row=$query->fetch()){	
		echo "<tr style='font-size:90%'>";

		echo "<td align='center'>";
		echo "<a class='mybtn fa fa-file' onClick='$(\"#widget_id\").val(\"".$row['widget_id']."\"); $(\"#tpmod\").val(\"MODIFY\");$(\"#formulaire\").submit();'></a>";
		if($row['widget_id']>0) {
			echo "&nbsp;";
			echo "<a class='mybtn fa fa-trash' onClick='$(\"#widget_id\").val(\"".$row['widget_id']."\"); $(\"#tpmod\").val(\"DELETE\");$(\"#formulaire\").submit();'></a>";
		}
		echo "</td>";

		/*
		echo "<td align='center'>";
		echo $row['widget_id'];
		echo "</td>";
		*/
		
		echo "<td align='center'>";
		echo "<img src='local/images/icon/".$row['icon_url']."' width='30px' height='30px'></img>";
		echo "</td>";

		echo "<td>";
		$q="SELECT * FROM ".$config["dbprefixe"]."widget_categorie WHERE widget_categorie_id=?";
		$query2=$bdd02->prepare($q);
		$query2->execute(array($row['widget_categorie']));
		if(!$row2=$query2->fetch()) echo $row2['widget_categorie_label'];
		echo "</td>";
				
		echo "<td>";
		echo $row['widget_name'];
		echo "</td>";

		/*
		echo "<td>";
		echo $row['widget_label'];
		echo "</td>";
		*/
		
		/*
		echo "<td align='center'>";
		echo $row['widget_height'];
		echo "</td>";
		*/
		
		echo "<td align='center'>";
		echo $row['widget_type_name'];
		echo "</td>";		

		/*
		echo "<td align='center'>";
		echo $db1->f('widget_mode_name');
		echo "</td>";
		*/
		
		echo "</tr>";
	} 
	echo "</table>";
}

//-- SUBMIT --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="SUBMIT") {
	// Entete du formulaire
    echo "<legend><h1>AJOUT WIDGET</h1></legend>";
    
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vladd' name='vladd' class='btn btn-primary' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
	echo "</div>";
	echo "</div>";	  
    


	// Valeur par défaut
	if($widget_style=="") 		$widget_style		="0";
	if($widget_adjust=="") 		$widget_adjust		="0";
	if($widget_height=="") 		$widget_height		="300";
	if($widget_type=="") 		$widget_type		= 1;
	if($widget_mode=="") 		$widget_mode		= 1;
	if($widget_icon=="")	 	$widget_icon		= "";
	if($widget_categorie=="") 	$widget_categorie	= 0;
}

//-- MODIFY --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="MODIFY") {
	// Entete du formulaire
    echo "<legend><h1>MODIFICATION WIDGET</h1></legend>";
	
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlmod' name='vlmod' class='btn btn-primary' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
	echo "</div>";
	echo "</div>";	

	// Valeur par défaut
	$q = "SELECT * FROM ".$config["dbprefixe"]."widget, ".$config["dbprefixe"]."icon WHERE widget_id=? AND widget_icon=icon_id";
	$query=$bdd01->prepare($q);
	$query->execute(array($widget_id));
	if($row=$query->fetch()){	
		$widget_name 			= $row["widget_name"];
		$widget_label 			= $row["widget_label"];
		$widget_url 			= $row["widget_url"];
		$widget_adjust		 	= $row["widget_adjust"];
		$widget_height		 	= $row["widget_height"];
		$widget_style 			= $row["widget_style"];
		$widget_type			= $row["widget_type"];
		$widget_mode			= $row["widget_mode"];
		$widget_icon			= $row["widget_icon"];
		$icon_url				= $row["icon_url"];
		$widget_code			= $row["widget_code"];
		$widget_categorie		= $row["widget_categorie"];
	}
}

//-- DELETE --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="DELETE") {
	echo "<legend><h1>SUPPRESSION WIDGET</h1></legend>";
	
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlsup' name='vlsup' class='btn btn-primary' type='submit' value='Confirmer la suppression' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";    
	echo "</div>";
	echo "</div>";	 	

	// Valeur par défaut
	$q = "SELECT * FROM ".$config["dbprefixe"]."widget, ".$config["dbprefixe"]."icon WHERE widget_id=? AND widget_icon=icon_id";
	$query=$bdd01->prepare($q);
	$query->execute(array($widget_id));
	if($row=$query->fetch()){	
		$widget_name 			= $row["widget_name"];
		$widget_label 			= $row["widget_label"];
		$widget_url 			= $row["widget_url"];
		$widget_adjust		 	= $row["widget_adjust"];
		$widget_height		 	= $row["widget_height"];
		$widget_style 			= $row["widget_style"];
		$widget_type			= $row["widget_type"];
		$widget_mode			= $row["widget_mode"];
		$widget_icon			= $row["widget_icon"];
		$icon_url				= $row["icon_url"];
		$widget_code			= $row["widget_code"];
		$widget_categorie		= $row["widget_categorie"];
	}
}


//-- FORMULAIRE -----------------------------------------------------------------------------------------------------------------------------

if($tpmod=="SUBMIT"||$tpmod=="MODIFY"||$tpmod=="DELETE") {
	if($widget_id<0) $fghidden=" style='display:none;'";
?>


	<fieldset class="row fieldset" style="clear:both">
		<legend>Description</legend>

		<div class="form-group">
			<label for="widget_id_bis" class="col-sm-3 control-label">ID*</label>
			<div class="col-sm-6"><input name="widget_id_bis" id="widget_id_bis" type="titre" class="form-control" disabled="disabled" placeholder="ID" value="<? echo $widget_id; ?>"></div>
		</div>

		<div class="form-group">
			<label for="widget_name" class="col-sm-3 control-label">Nom*</label>
			<div class="col-sm-6"><input name="widget_name" id="widget_name" type="titre" class="form-control" placeholder="Nom" value="<? echo $widget_name; ?>"></div>
		</div>

		<div class="form-group">
			<label for="widget_label" class="col-sm-3 control-label">Description</label>
			<div class="col-sm-6">			
				<textarea id="widget_label" name="widget_label" class="form-control" rows="4"><? echo $widget_label; ?></textarea>
			</div>
		</div>

		<div class="form-group" <? echo $fghidden; ?>>
			<label for="widget_url" class="col-sm-3 control-label">Url</label>
			<div class="col-sm-6"><input name="widget_url" id="widget_url" type="titre" class="form-control" placeholder="Url" value="<? echo $widget_url; ?>"></div>
		</div>

		<div class="form-group">
			<label for="widget_adjust" class="col-sm-3 control-label">Ajuster*</label>
			<div class="col-sm-6">
				<select name="widget_adjust" id="widget_adjust" class="form-control">
				<?
					if($widget_adjust==0) $lbsel="selected";
					echo "<option value='0' ".$lbsel.">Non</option>";
					
					$lbsel="";
					if($widget_adjust==1) $lbsel="selected";
					echo "<option value='1' ".$lbsel.">Oui</option>";
				?>					
				</select>			
			</div>
		</div>

		<div class="form-group">
			<label for="widget_height" class="col-sm-3 control-label">Hauteur*</label>
			<div class="col-sm-6"><input name="widget_height" id="widget_url" type="number" class="form-control" placeholder="Url" value="<? echo $widget_height; ?>"></div>
		</div>

		<div class="form-group">
			<label for="widget_categorie" class="col-sm-3 control-label">Catégorie*</label>
			<div class="col-sm-6">
				<select name="widget_categorie" id="widget_categorie" class="form-control">
				<?
					if($categorie==0) $lbsel="selected";
					echo "<option value='0' ".$lbsel.">Aucune</option>";
					
					$q="SELECT * FROM ".$config["dbprefixe"]."widget_categorie";
					$query=$bdd01->prepare($q);
					$query->execute();
					while($row=$query->fetch()){	
						$lbsel="";
						if($widget_categorie==$row["widget_categorie_id"]) {
							$lbsel="selected";
						}
						echo "<option value='".$row["widget_categorie_id"]."' ".$lbsel.">".$row["widget_categorie_label"]."</option>";
					}
				?>					
				</select>			
			</div>
		</div>
		
		<div class="form-group">
			<label for="widget_style" class="col-sm-3 control-label">Style*</label>
			<div class="col-sm-6">
				<select name="widget_style" id="widget_style" class="form-control">
				<?
					$q="SELECT * FROM ".$config["dbprefixe"]."widget_style";
					$query=$bdd01->prepare($q);
					$query->execute();
					while($row=$query->fetch()){
						$lbsel="";
						if($widget_style==$row["widget_style_id"]) {
							$lbsel="selected";
						}
						echo "<option value='".$row["widget_style_id"]."' ".$lbsel.">".$row["widget_style_label"]."</option>";
					}
				?>					
				</select>			
			</div>
		</div>

		<div class="form-group" style="display:none;">
			<label for="widget_mode" class="col-sm-3 control-label">Mode*</label>
			<div class="col-sm-6">
				<select name="widget_mode" id="widget_mode" class="form-control">
				<?
					$q="SELECT * FROM ".$config["dbprefixe"]."widget_mode";
					$query=$bdd01->prepare($q);
					$query->execute();
					while($row=$query->fetch()){
						$lbsel="";
						if($widget_mode==$row["widget_mode_id"]) {
							$lbsel="selected";
						}
						echo "<option value='".$row["widget_mode_id"]."' ".$lbsel.">".$row["widget_mode_name"]."</option>";
					}
				?>					
				</select>			
			</div>
		</div>

		<div class="form-group" <? echo $fghidden; ?>>
			<label for="widget_type" class="col-sm-3 control-label">Type*</label>
			<div class="col-sm-6">
				<select name="widget_type" id="widget_type" class="form-control">
				<?
					$q="SELECT * FROM ".$config["dbprefixe"]."widget_type";
					$query=$bdd01->prepare($q);
					$query->execute();
					while($row=$query->fetch()){
						$lbsel="";
						if($widget_type==$row["widget_type_id"]) {
							$lbsel="selected";
						}
						echo "<option value='".$row["widget_type_id"]."' ".$lbsel.">".$row["widget_type_name"]."</option>";
					}
				?>					
				</select>			
			</div>
		</div>
	</fieldset>

	<fieldset class="row fieldset" style="clear:both">
		<legend>Icône</legend>
		
		<input 	value="<? echo $widget_icon; ?>" id="icon" name="widget_icon" type="hidden"/>
		<?
		echo "<div style='width:140px; margin:auto; '>";
		echo "<div id='myavatarprofil' style='float:left; margin-right:5px; margin-bottom:10px; border:none;'>";
		echo "<img id='imgapp' src='local/images/icon/".$icon_url."' width='90px' height='90px'></img>";
		echo "</div>";
		echo "<div style='float:left;'>";
		echo "<a class='glyphicon glyphicon-folder-close' data-toggle='modal' data-target='#mymodal-01' title='Séléctionner un icône' onClick='ModalLoad(\"mymodal-01\",\"icon-selection\",0,0)'></a>";
		echo "</div>";
		echo "</div>";
		?>
		
	</fieldset>


	<fieldset class="code row fieldset" style="display: none; clear: both">
		<legend class="code" style="display: none;">Code</legend>	
		<textarea id="widget_code" name="widget_code" class="form-control" rows="10"><? echo $widget_code; ?></textarea>
	</fieldset>
	
<?
}

echo "</form>";
?>

<!-- SCRIPT --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<? if($tpmod=="") { ?>
	<script>
		$(document).ready(function() {
			$('#datatable').dataTable( {
				"oLanguage": { "sUrl": "lib/dataTables/dataTables.txt" },
				"iDisplayLength": 100,
				"aoColumnDefs" : [ {'bSortable' : false, 'aTargets' : [ 0,1 ]} ],
				"aaSorting": [[ 2, "asc" ]],
				"stateSave": true
			} );
		} );	
	</script>
<? } ?>

<? if($tpmod=="SUBMIT"||$tpmod=="MODIFY"||$tpmod=="DELETE") { ?>
	<script type="text/javascript">
		$('#label').focus();

		function showCode() {
			if($('#widget_type').val()==4) {
				$('.code').css("display", "block");
			}			
			else {
				$('#widget_code').val('');
				
				$('.code').css("display", "none");
			}
		}
		
		if($('#widget_type').val()==4) {
			$('.code').css("display", "block");
		}
		
		$('#widget_type').bind("change", function() {		
			showCode();
		});
		
		<? if($widget_id>=0) { ?> showCode(); <? } ?>
		<?php echo $jsaction ?>
		
	</script>
<? } ?>







	
	
