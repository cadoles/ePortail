<?
	$levelpage=1;
	include("include/permission.php");

	$tpmod				=$_POST['tpmod'];
	$vlmod				=$_POST['vlmod'];
	$vladd				=$_POST['vladd'];
	$vlsup				=$_POST['vlsup'];
	
	$id					=$_POST['id'];
	$label				=$_POST['label'];
	$url				=$_POST['url'];
	$order				=$_POST['order'];
	$idproprio			=$_POST['idproprio'];
	
	/*--> Controle de cohérance */
	if($vladd!=""||$vlmod!="") {
		$fgerr="";
		if($label==""||$idproprio==""||$url=="") {
			$jsaction="alert('Vous devez renseigner l\'ensemble des informations');";
			$fgerr=1;
		}
	}
	
	/*--> Rester sur le mode encours */
	if($vlmod!=""&&fgerr!="") $tpmod="MODIFY";
	if($vladd!=""&&fgerr!="") $tpmod="SUBMIT";
		
	/*--> Submit */ 
	if($vladd!=""&&$fgerr=="") {
		$q="INSERT INTO ".$config["dbprefixe"]."panel(panel_label, panel_user, panel_url, panel_order, panel_type) VALUES(?,?,?,?,?)";
		$query=$bdd01->prepare($q);
		$query->execute(array($label,$idproprio,$url,$order,1));
		$tpmod="";
	}

	/*--> Modify */
	if($vlmod!=""&&$fgerr=="") {
		$q="UPDATE ".$config["dbprefixe"]."panel SET panel_label=?, panel_user=?, panel_url=?, panel_order=? WHERE panel_id=?";
		$query=$bdd01->prepare($q);
		$query->execute(array($label,$idproprio,$url,$order,$id));
		$tpmod="";
	}

	/*--> Delete */
	if($vlsup!="") {
		delPanel($id);
	}	

	echo "<form id='formulaire' class='form-horizontal' role='form' method='post' enctype='multipart/form-data'>";
	
	echo "<input id='id' name='id' type='hidden' value='".$id."'>";
	echo "<input id='tpmod' name='tpmod' type='hidden' value=''>";
	
  
//-- DISPLAY -------------------------------------------------------------------------------------------------------------------------------

if($tpmod=="") { 
    echo "<legend><h1>GESTION DES PAGES URL</h1></legend>";
    
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='fgadd' name='fgadd' class='btn btn-primary' onClick='$(\"#id\").val(\"\"); $(\"#tpmod\").val(\"SUBMIT\");' type='submit' value='Ajouter' />";
	echo "</div>";
	echo "</div>";
		
	echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
    echo "<thead>";
	echo "<th width='40px'>Action</th>";
	echo "<th width='70px'>Ordre</th>";
	echo "<th width='70px'>ID</th>";
	echo "<th>Libellé</th>";
	echo "<th>Propriètaire</th>";
	echo "</thead>";

	$q="SELECT * FROM ".$config["dbprefixe"]."panel, ".$config["dbprefixe"]."user, ".$config["dbprefixe"]."profil WHERE (panel_type=1 OR panel_type=2) AND user_id=panel_user AND profil_id=user_profil ORDER BY panel_id DESC";
	$query=$bdd01->prepare($q);
	$query->execute();
	while($row=$query->fetch()){	
		echo "<tr>";

		echo "<td align='center'>";
		echo "<a class='mybtn fa fa-file' onClick='$(\"#id\").val(\"".$row['panel_id']."\"); $(\"#tpmod\").val(\"MODIFY\");$(\"#formulaire\").submit();'></a>";
		if($row['panel_id']>0) {
			echo "&nbsp;";
			echo "<a class='mybtn fa fa-trash' onClick='$(\"#id\").val(\"".$row['panel_id']."\"); $(\"#tpmod\").val(\"DELETE\");$(\"#formulaire\").submit();'></a>";
		}
		echo "</td>";

		echo "<td>";
		echo $row['panel_order'];
		echo "</td>";
				
		echo "<td>";
		echo $row['panel_id'];
		echo "</td>";

		echo "<td>";
		echo "<a href='index.php?id=".$row['panel_id']."' target='_blank'>".$row['panel_label']."</a>";
		echo "</td>";
		
		echo "<td>";
		if($row['user_avatar']=="") 
			echo "<img src='style/images/blank.gif' class='myavatarvide' width='45px' height='45px' style='float:left;margin-right:5px;'></img>";
		else
			echo "<img src='local/images/avatar/".$row['user_avatar']."' width='45px' height='45px' style='float:left;margin-right:5px;'></img>";
		
		echo "<span style='float:left; font-size:80%;'>";
		echo "Login : ".$row['user_login']."<br>";
		echo "Pseudo : ".$row['user_pseudo']."<br>";
		echo "Profil : ".$row['profil_label']."<br>";		
		echo "</span>";
		echo "</td>";
		
		echo "</tr>";
	} 

	echo "</table>";
}

//-- SUBMIT --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="SUBMIT") {
	// Entete du formulaire
    echo "<legend><h1>AJOUT PAGE URL</h1></legend>";
    
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vladd' name='vladd' class='btn btn-primary' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
	echo "</div>";
	echo "</div>";	

	// Valeur par défaut
	// aucune
	$idproprio=-1;
	$idtemplate=1;
	$url="";
	$order=1;
	
	$q="SELECT * FROM ".$config["dbprefixe"]."user, ".$config["dbprefixe"]."profil  WHERE user_id=? AND profil_id=user_profil";
	$query=$bdd01->prepare($q);
	$query->execute(array($idproprio));
	if($row=$query->fetch()){	
		$lbavatar	= $row['user_avatar'];
		$lbproprio	= $row['user_firstname']." ".$row['user_lastname'];
		$lbpseudo	= $row['user_pseudo'];
		$lblogin	= $row['user_login'];
		$lbprofil	= $row['profil_label'];
	}
}

//-- MODIFY --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="MODIFY") {
	// Entete du formulaire
    echo "<legend><h1>MODIFICATION PAGE URL</h1></legend>";
   
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlmod' name='vlmod' class='btn btn-primary' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
	echo "</div>";
	echo "</div>";	

	// Valeur par défaut
	$q = "SELECT * FROM ".$config["dbprefixe"]."panel, ".$config["dbprefixe"]."user, ".$config["dbprefixe"]."profil WHERE panel_id=? AND panel_user=user_id AND profil_id=user_profil";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
	if($row=$query->fetch()){	
		$idproprio	= $row['panel_user'];
		$label 		= $row['panel_label'];
		$lbavatar	= $row['user_avatar'];
		$lbproprio	= $row['user_firstname']." ".$row['user_lastname'];
		$lbpseudo	= $row['user_pseudo'];
		$lblogin	= $row['user_login'];
		$lbprofil	= $row['profil_label'];
		$idtemplate	= $row['panel_template'];
		$url		= $row['panel_url'];
		$order		= $row['panel_order'];
	}
}

//-- DELETE --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="DELETE") {
	echo "<legend><h1>SUPPRESSION PAGE URL</h1></legend>";
	
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlsup' name='vlsup' class='btn btn-primary' type='submit' value='Confirmer la suppression' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";    
	echo "</div>";
	echo "</div>";
	
	$q = "SELECT * FROM ".$config["dbprefixe"]."panel, ".$config["dbprefixe"]."user, ".$config["dbprefixe"]."profil WHERE panel_id=? AND panel_user=user_id AND profil_id=user_profil";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
	if($row=$query->fetch()){	
		$idproprio	= $row['panel_user'];
		$label 		= $row['panel_label'];
		$lbavatar	= $row['user_avatar'];
		$lbproprio	= $row['user_firstname']." ".$row['user_lastname'];
		$lbpseudo	= $row['user_pseudo'];
		$lblogin	= $row['user_login'];
		$lbprofil	= $row['profil_label'];
		$idtemplate	= $row['panel_template'];
		$url		= $row['panel_url'];
		$order		= $row['panel_order'];
	}
}


//-- FORMULAIRE -----------------------------------------------------------------------------------------------------------------------------

if($tpmod=="SUBMIT"||$tpmod=="MODIFY"||$tpmod=="DELETE") {
?>
	<fieldset class="row fieldset" style="clear:both">
		<legend>Description</legend>

		<div class="form-group">
			<label for="id_bis" class="col-sm-3 control-label">ID*</label>
			<div class="col-sm-6"><input name="id_bis" id="id_bis" type="titre" class="form-control" disabled="disabled" placeholder="ID Application" value="<? echo $id; ?>"></div>
		</div>
		

		<div class="form-group">
			<label for="label" class="col-sm-3 control-label">Libellé*</label>
			<div class="col-sm-6"><input name="label" id="label" type="titre" class="form-control" placeholder="Libellé" value="<? echo $label; ?>"></div>
		</div>	
		
		
		<div class="form-group" style="<? if($id<0) echo "display:none"; ?>">
			<label for="url" class="col-sm-3 control-label">Url*</label>
			<div class="col-sm-6"><input name="url" id="url" type="titre" class="form-control" placeholder="Url" value="<? echo urldecode($url); ?>"></div>
		</div>	
		
		<div class="form-group">
			<label for="order" class="col-sm-3 control-label">Ordre*</label>
			<div class="col-sm-6"><input name="order" id="order" type="number" class="form-control" placeholder="Ordre" value="<? echo $order; ?>"></div>
		</div>
	</fieldset>
	
	<fieldset class="row fieldset" style="clear:both">
		<legend>Propriétaire</legend>
		
		<div style="width:200px; margin:auto;">
		<?
		if($lbavatar=="") 
			echo "<img src='style/images/blank.gif' class='myavatarvide' width='45px' height='45px' style='float:left;margin-right:5px;'></img>";
		else
			echo "<img src='local/images/avatar/".$lbavatar."' width='45px' height='45px' style='float:left;margin-right:5px;'></img>";
		
		echo "<span style='float:left; font-size:80%;'>";
		echo "Login : ".$lblogin."<br>";
		echo "Pseudo : ".$lbpseudo."<br>";
		echo "Profil : ".$lbprofil."<br>";		
		echo "</span>";
		?>
		</div>	
		
		<input 	value="<? echo $idproprio; ?>" id="idproprio" name="idproprio" type="hidden" readonly>		
	</fieldset>
<?
}

echo "</form>";

?>

<!-- SCRIPT --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<? if($tpmod=="") { ?>
	<script>
		$(document).ready(function() {
			$('#datatable').dataTable( {
				"oLanguage": { "sUrl": "lib/dataTables/dataTables.txt" },
				"iDisplayLength": 100,
				"aoColumnDefs" : [ {'bSortable' : false, 'aTargets' : [ 0 ]} ],
				"aaSorting": [[ 1, "asc" ]],
				"stateSave": true
			} );
		} );	
	</script>
<? } ?>

<? if($tpmod=="SUBMIT"||$tpmod=="MODIFY"||$tpmod=="DELETE") { ?>
	<script type="text/javascript">
		$('#label').focus();
	</script>
<? } ?>

<script>
		<?php echo $jsaction ?>
</script>




	
	
