<? 
	$levelpage=2;
	include("include/permission.php");
?>

	<div class="navbar-default sidebar" role="navigation">
		<div class="sidebar-nav navbar-collapse">
			<ul class="nav" id="side-menu">
				
				<!-- MENU CONFIGURATION -->
				<? if($_SESSION['user_profil']==1) { ?>
				<? 
					if($view=="admin/config.php"||$view=="admin/ldap.php"||$view=="admin/style.php"||$view=="admin/icon.php")
						echo "<li class='active'>";
					else
						echo "<li>";
				?>
					<a href="#"><i class="fa fa-wrench fa-fw"></i> CONFIGURATION<span class="fa arrow"></span></a>
					<ul class="nav nav-second-level">
						<!-- Configuration Génerale -->
						<li><a href="index.php?view=admin/config.php"	<? if($view=="admin/config.php") 	echo "class='active'"; ?>><i class="fa fa-table fa-fw"></i> Générale</a></li>
						
						<!-- LDAP -->
						<? if($config['activerLDAPModif']=="true") { ?>
						<li><a href="index.php?view=admin/ldap.php"		<? if($view=="admin/ldap.php")		echo "class='active'"; ?>><i class="fa fa-sitemap fa-fw"></i> LDAP</a></li>
						<? } ?>
						
						<!-- Style -->
						<li><a href="index.php?view=admin/style.php"	<? if($view=="admin/style.php")		echo "class='active'"; ?>><i class="fa fa-paint-brush fa-fw"></i> Style</a></li>
						
						<!-- Icone -->
						<li><a href="index.php?view=admin/icon.php"		<? if($view=="admin/icon.php")		echo "class='active'"; ?>><i class="fa fa-file-image-o fa-fw"></i> Gestion des Icônes</a></li>
					</ul>
				</li>
				<? } ?>
								
				<!-- MENU SSO -->
				<? if($_SESSION['user_profil']==1) { ?>
				<? if($config["modeAuthentification"]=="CAS") { ?>
				<? 
					if($view=="admin/ssoattribut.php"||$view=="admin/ssocommunity.php"||$view=="admin/ssoprofil.php")
						echo "<li class='active'>";
					else
						echo "<li>";
				?>
					<a href="#"><i class="fa fa-slideshare fa-fw"></i> SSO<span class="fa arrow"></span></a>
					<ul class="nav nav-second-level">
						<li><a href="index.php?view=admin/ssoattribut.php"	<? if($view=="admin/ssoattribut.php") 	echo "class='active'"; ?>><i class="fa fa-stack-overflow fa-fw"></i> Attributs</a></li>
						<li><a href="index.php?view=admin/ssocommunity.php"	<? if($view=="admin/ssocommunity.php") 	echo "class='active'"; ?>><i class="fa fa-home fa-fw"></i> Communauté</a></li>
						<li><a href="index.php?view=admin/ssoprofil.php"	<? if($view=="admin/ssoprofil.php") 	echo "class='active'"; ?>><i class="fa fa-male fa-fw"></i> Profil</a></li>
						
					</ul>
				</li>
				<? } ?>
				<? } ?>
				
				<!-- MENU LDAP -->
				<? if($_SESSION['user_profil']==1) { ?>
				<? if($config["activerLDAP"]=="true") { ?>
				<? 
					if($view=="admin/ldapcommunity.php"||$view=="admin/ldapprofil.php")
						echo "<li class='active'>";
					else
						echo "<li>";
				?>
					<a href="#"><i class="fa fa-sitemap fa-fw"></i> LDAP<span class="fa arrow"></span></a>
					<ul class="nav nav-second-level">
						<li><a href="index.php?view=admin/ldapcommunity.php"	<? if($view=="admin/ldapcommunity.php") 	echo "class='active'"; ?>><i class="fa fa-home fa-fw"></i> Communauté</a></li>
						<li><a href="index.php?view=admin/ldapprofil.php"		<? if($view=="admin/ldapprofil.php") 		echo "class='active'"; ?>><i class="fa fa-male fa-fw"></i> Profil</a></li>
					</ul>
				</li>
				<? } ?>
				<? } ?>				
				
				<!-- MENU UTILISATEUR -->
				<? if($_SESSION['user_profil']==1) { ?>
				<? 
					if($view=="admin/user.php"||$view=="admin/inscription.php")
						echo "<li class='active'>";
					else
						echo "<li>";
				?>
					<a href="#"><i class="fa fa-male fa-fw"></i> UTILISATEUR<span class="fa arrow"></span></a>
					<ul class="nav nav-second-level">
						
						<? if($config["modeRegistration"]=='true'&&$config["modeRegistrationExterne"]==""&&($config["activerLDAP"]=='true'||$config["modeAuthentification"]=='MYSQL')) { ?>
						<li><a href="index.php?view=admin/inscription.php"	<? if($view=="admin/inscription.php") 	echo "class='active'"; ?>><i class="fa fa-pencil-square-o fa-fw"></i> Inscription</a></li>
						<? } ?>
						<li><a href="index.php?view=admin/user.php"			<? if($view=="admin/user.php") 			echo "class='active'"; ?>><i class="fa fa-child fa-fw"></i> Utilisateur</a></li>
					</ul>
				</li>
				<? } ?>

				<!-- MENU BUREAU -->
				<? 
					if($view=="admin/applicationcategorie.php"||$view=="admin/application.php"||$view=="admin/applicationprofil.php"||$view=="admin/applicationssoprofil.php"||$view=="admin/applicationldapprofil.php")
						echo "<li class='active'>";
					else
						echo "<li>";
				?>
					<a href="#"><i class="fa fa-desktop fa-fw"></i> BUREAU<span class="fa arrow"></span></a>
					<ul class="nav nav-second-level">
						<li><a href="index.php?view=admin/applicationcategorie.php"		<? if($view=="admin/applicationcategorie.php") 		echo "class='active'"; ?>><i class="fa fa-folder fa-fw"></i> Catégorie</a></li>
						<li><a href="index.php?view=admin/application.php"				<? if($view=="admin/application.php")				echo "class='active'"; ?>><i class="fa fa-cube fa-fw"></i> Application</a></li>
						<li><a href="index.php?view=admin/applicationprofil.php"		<? if($view=="admin/applicationprofil.php")			echo "class='active'"; ?>><i class="fa fa-child fa-fw"></i> Application par Profils</a></li>
						
						<? if($config["modeAuthentification"]=="CAS") { ?>
						<li><a href="index.php?view=admin/applicationssoprofil.php"		<? if($view=="admin/applicationssoprofil.php") 		echo "class='active'"; ?>><i class="fa fa-slideshare fa-fw"></i> Application par Profils SSO</a></li>
						<? } ?>
						
						<? if($config["activerLDAP"]=="true") { ?>
						<li><a href="index.php?view=admin/applicationldapprofil.php"	<? if($view=="admin/applicationldapprofil.php") 	echo "class='active'"; ?>><i class="fa fa-sitemap fa-fw"></i> Application par Profils LDAP</a></li>
						<? } ?>
					</ul>
				</li>				
				
				
				<!-- MENU WIDGETS -->
				<? if($_SESSION['user_profil']==1) { ?>
				<? 
					if($view=="admin/widgetcategorie.php"||$view=="admin/widget.php")
						echo "<li class='active'>";
					else
						echo "<li>";
				?>
					<a href="#"><i class="fa fa-cubes fa-fw"></i> WIDGETS<span class="fa arrow"></span></a>
					<ul class="nav nav-second-level">
						<li><a href="index.php?view=admin/widgetcategorie.php"			<? if($view=="admin/widgetcategorie.php") 		echo "class='active'"; ?>><i class="fa fa-folder fa-fw"></i> Catégorie</a></li>
						<li><a href="index.php?view=admin/widget.php"					<? if($view=="admin/widget.php")				echo "class='active'"; ?>><i class="fa fa-cubes fa-fw"></i> Widget</a></li>
					</ul>
				</li>	
				<? } ?>
				
				<!-- MENU PAGES -->
				<? if($_SESSION['user_profil']==1) { ?>
				<? 
					if($view=="admin/panelwidget.php"||$view=="admin/paneleditor.php"||$view=="admin/panelurl.php"||$view=="admin/panelprofil.php"||$view=="admin/panelssoprofil.php"||$view=="admin/panelldapprofil.php")
						echo "<li class='active'>";
					else
						echo "<li>";
				?>
					<a href="#"><i class="fa fa-file o fa-fw"></i> PAGES<span class="fa arrow"></span></a>
					<ul class="nav nav-second-level">
						<li><a href="index.php?view=admin/panelwidget.php"			<? if($view=="admin/panelwidget.php") 		echo "class='active'"; ?>><i class="fa fa-cubes fa-fw"></i> Page de type Widget</a></li>
						<li><a href="index.php?view=admin/paneleditor.php"			<? if($view=="admin/paneleditor.php") 		echo "class='active'"; ?>><i class="fa fa-font fa-fw"></i> Page de type Editeur</a></li>
						<li><a href="index.php?view=admin/panelurl.php"				<? if($view=="admin/panelurl.php")	 		echo "class='active'"; ?>><i class="fa fa-plug fa-fw"></i> Page de type URL</a></li>
						<li><a href="index.php?view=admin/panelprofil.php"			<? if($view=="admin/panelprofil.php")	 	echo "class='active'"; ?>><i class="fa fa-child fa-fw"></i> Page par Profils</a></li>
						<? if($config["modeAuthentification"]=="CAS") { ?>
						<li><a href="index.php?view=admin/panelssoprofil.php"		<? if($view=="admin/panelssoprofil.php") 	echo "class='active'"; ?>><i class="fa fa-slideshare fa-fw"></i>  Page par Profils SSO</a></li>
						<? } ?>
						<? if($config["activerLDAP"]=="true") { ?>
						<li><a href="index.php?view=admin/panelldapprofil.php"		<? if($view=="admin/panelldapprofil.php") 	echo "class='active'"; ?>><i class="fa fa-sitemap fa-fw"></i>  Page par Profils LDAP</a></li>
						<? } ?>

					</ul>
				<? } ?>

				<!-- MENU FLUX -->
				<? 
					if($view=="admin/flux.php"||$view=="admin/fluxprofil.php"||$view=="admin/fluxssoprofil.php"||$view=="admin/fluxldapprofil.php")
						echo "<li class='active'>";
					else
						echo "<li>";
				?>
					<a href="#"><i class="fa fa-rss-square o fa-fw"></i> FLUX<span class="fa arrow"></span></a>
					<ul class="nav nav-second-level">
						<li><a href="index.php?view=admin/flux.php"					<? if($view=="admin/flux.php") 				echo "class='active'"; ?>><i class="fa fa-rss fa-fw"></i> Flux</a></li>
						<li><a href="index.php?view=admin/fluxprofil.php"			<? if($view=="admin/flyxprofil.php")	 	echo "class='active'"; ?>><i class="fa fa-child fa-fw"></i> Flux par Profils</a></li>
						<? if($config["modeAuthentification"]=="CAS") { ?>
						<li><a href="index.php?view=admin/fluxssoprofil.php"		<? if($view=="admin/fluxssoprofil.php") 	echo "class='active'"; ?>><i class="fa fa-slideshare fa-fw"></i>  Flux par Profils SSO</a></li>
						<? } ?>
						<? if($config["activerLDAP"]=="true") { ?>
						<li><a href="index.php?view=admin/fluxldapprofil.php"		<? if($view=="admin/fluxldapprofil.php") 	echo "class='active'"; ?>><i class="fa fa-sitemap fa-fw"></i>  Flux par Profils LDAP</a></li>
						<? } ?>
					</ul>					
				</li>	
				
				
				<!-- MENU SYNCHRONISATION -->
				<? if($_SESSION['user_profil']==1) { ?>
				<?
					if($config["synchro-balado"]=='true'||$config["synchro-cdt"]=='true'||$config["synchro-iconito"]=='true'||$config["synchro-moodle"]=='true'||$config["synchro-wordpress"]=='true') {
						if($view=="admin/synchro.php")
							echo "<li class='active'>";
						else
							echo "<li>";
						
						$page=$_GET["page"];
				?>

					<a href="#"><i class="fa fa-exchange fa-fw"></i> SYNCHRONISATION<span class="fa arrow"></span></a>
					<ul class="nav nav-second-level">
						<? if($config["synchro-balado"]=='true') 	{ ?><li><a href="index.php?view=admin/synchro.php&page=balado"		<? if($page=="balado")						echo "class='active'"; ?>><i class="fa fa-microphone fa-fw"></i> Balado</a></li><? } ?>
						<? if($config["synchro-cdt"]=='true') 		{ ?><li><a href="index.php?view=admin/synchro.php&page=cdt"			<? if($page=="cdt")	 						echo "class='active'"; ?>><i class="fa fa-book  fa-fw"></i> Cdt</a></li><? } ?>
						<? if($config["synchro-iconito"]=='true') 	{ ?><li><a href="index.php?view=admin/synchro.php&page=iconito"		<? if($page=="iconito")	 					echo "class='active'"; ?>><i class="fa fa-reddit fa-fw"></i> Iconito</a></li><? } ?>
						<? if($config["synchro-moodle"]=='true') 	{ ?><li><a href="index.php?view=admin/synchro.php&page=moodle"		<? if($page=="moodle")	 					echo "class='active'"; ?>><i class="fa fa-graduation-cap fa-fw"></i> Moodle</a></li><? } ?>
						<? if($config["synchro-wordpress"]=='true')	{ ?><li><a href="index.php?view=admin/synchro.php&page=wordpress"	<? if($page=="wordpress")					echo "class='active'"; ?>><i class="fa fa-wordpress fa-fw"></i> Wordpress</a></li><? } ?>
					</ul>
				<? } ?>
				<? } ?>				
			</ul>
		</div>
		<!-- /.sidebar-collapse -->
	</div>
	<!-- /.navbar-static-side -->
