<?
	$levelpage=1;
	include("include/permission.php");
	
	$tpmod				=$_POST['tpmod'];
	$vlmod				=$_POST['vlmod'];
	$vladd				=$_POST['vladd'];
	$vlsup				=$_POST['vlsup'];
	
	$user_id			=$_POST['user_id'];

	
	
	/*--> Envoi de l'email d'inscription */
	if($tpmod=="SUBMIT") {
		$q="SELECT * FROM ".$config["dbprefixe"]."inscription WHERE inscription_id=?";
		$query=$bdd01->prepare($q);
		$query->execute(array($user_id));
		if($row=$query->fetch()){
			
			$inscription_login=$row["inscription_login"];
			$inscription_email=$row["inscription_email"];
			$inscription_token=$row["inscription_token"];
			
			if($inscription_token=="") {
				$inscription_token=uniqid(rand(),true);
				$q="UPDATE ".$config["dbprefixe"]."inscription SET inscription_token=? WHERE inscription_id=?";	
				$query=$bdd01->prepare($q);
				$query->execute(array($inscription_token,$user_id));
			}
			
			// Envoi de l'email
			$to			= $inscription_email;
			$subject	= "Inscription au site = ".$config["title"];
			$headers   	= array();
			$headers[] 	= "MIME-Version: 1.0";
			$headers[] 	= "Content-type: text/html; charset=UTF-8";
			$headers[] 	= "From: Sender Name <sender@domain.com>";
			$headers[] 	= "Reply-To: no-replay";
			$headers[] 	= "Subject: Inscription au site = ".$config["title"];
			$headers[] 	= "X-Mailer: PHP/".phpversion();			
			$message 	= "Bonjour,<br>
						   Merci de valider votre inscription à notre site <strong>".$config["title"]."</strong>, en suivant le lien suivant<br>
						   <a href='".$config["urlbase"]."/index.php?view=user/confirm.php&login=".$inscription_login."&token=".$inscription_token."'>Validez votre inscription</a>";
			
			$retour=mail($to, $subject, $message, implode("\r\n", $headers));	
		}
		
		$tpmod="";
	}
	

	if($tpmod=="DELETE") {
		$q="DELETE FROM ".$config["dbprefixe"]."inscription WHERE inscription_id=?";	
		$query=$bdd01->prepare($q);
		$query->execute(array($user_id));
				
		$tpmod="";
	}

	echo "<form id='formulaire' class='form-horizontal' role='form' method='post' enctype='multipart/form-data'>";
	
	echo "<input id='user_id' name='user_id' type='hidden' value='".$user_id."'>";
	echo "<input id='tpmod' name='tpmod' type='hidden' value=''>";
	
  
//-- DISPLAY -------------------------------------------------------------------------------------------------------------------------------

    echo "<legend><h1>GESTION DES INSCRIPTIONS</h1></legend>";

	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	if ($config['modeAuthentification']=="MYSQL") {
		echo "<input id='fgadd' name='fgadd' class='btn btn-primary' onClick='$(\"#user_id\").val(\"\"); $(\"#tpmod\").val(\"SUBMIT\");' type='submit' value='Ajouter' />";
	}
	echo "</div>";
	echo "</div>";
	
	echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
    echo "<thead>";
	echo "<th width='50px'>Action</th>";
	echo "<th>ID</th>";
	echo "<th>Nom</th>";
	echo "<th>Login</th>";
	echo "<th>Email</th>";
	echo "<th>Token</th>";
	echo "</thead>";

	$q="SELECT * FROM ".$config["dbprefixe"]."inscription";
	$query=$bdd01->prepare($q);
	$query->execute();
	while($row=$query->fetch()){	
		echo "<tr>";

		echo "<td align='center'>";
		
			
		echo "<a class='mybtn fa fa-envelope' 	onClick='$(\"#user_id\").val(\"".$row['inscription_id']."\"); $(\"#tpmod\").val(\"SUBMIT\");$(\"#formulaire\").submit();'></a>";
		echo "&nbsp;";		
		echo "<a class='mybtn fa fa-trash'    	onClick='$(\"#user_id\").val(\"".$row['inscription_id']."\"); $(\"#tpmod\").val(\"DELETE\");$(\"#formulaire\").submit();'></a>";
		echo "</td>";

		echo "<td align='center'>";
		echo $row['inscription_id'];
		echo "</td>";
		
		echo "<td>";
		echo $row['inscription_firstname']." ".$row['inscription_lastname']."<br>";
		echo "</td>";

		echo "<td>";
		echo $row['inscription_login']."<br>";
		echo "</td>";

		echo "<td>";
		echo $row['inscription_email']."<br>";
		echo "</td>";

		echo "<td>";
		echo $row['inscription_token']."<br>";
		echo "</td>";

		echo "</tr>";
	} 

	echo "</table>";

	echo "</form>";
?>

<!-- SCRIPT --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<script>
	$(document).ready(function() {
		$('#datatable').dataTable( {
			"oLanguage": { "sUrl": "lib/dataTables/dataTables.txt" },
			"iDisplayLength": 100,
			"aoColumnDefs" : [ {'bSortable' : false, 'aTargets' : [ 0,2 ]} ],
			"aaSorting": [[ 1, "asc" ]],
			"stateSave": true
		} );
	} );	

	<?php echo $jsaction ?>
</script>
	
	
