<?
	$levelpage=2;
	include("include/permission.php");
	
	$tpmod				=$_POST['tpmod'];
	$vlmod				=$_POST['vlmod'];
	$vladd				=$_POST['vladd'];
	$vlsup				=$_POST['vlsup'];
	
	$id					=$_POST['id'];
	$name				=$_POST['name'];
	$label				=$_POST['label'];
	$url				=$_POST['url'];
	$open				=$_POST['open'];
	$color				=$_POST['color'];
	$icon				=$_POST['icon'];
	$order				=$_POST['order'];
	$categorie			=$_POST['categorie'];
	$description		=$_POST['description'];
	$search				=$_POST['search'];
	
	/*--> Controle de cohérance */
	if($vladd!=""||$vlmod!="") {
		$fgerr="";

		if($name==""||$label==""||$url==""||$icon==""||$open=="") {
			$jsaction="alert('Vous devez renseigner l\'ensemble des informations');";
			$fgerr=1;
		}
		
		if($order<1) {
			$jsaction="alert('Le numéro d\'ordre doit être supérieur à 0');";
			$fgerr=1;
		}
		
		$q="SELECT * FROM ".$config["dbprefixe"]."application WHERE application_name=?";
		$tbq=array($name);
		if($vlmod!="") {
			$q=$q." AND application_id!=?";
			array_push($tbq,$id);
		}
		$query=$bdd01->prepare($q);
		$query->execute($tbq);
		if($row=$query->fetch()){			
			$jsaction="alert('Une catégorie avec ce label existe déjà');";
			$fgerr=1;
		}

		/*
		$q="SELECT * FROM env_application WHERE application_categorie='$categorie' AND application_order='$order'";
		if($vlmod!="") $q=$q." AND application_id!=$id";
		$db1->query($q);
		if($db1->next_record()){			
			$jsaction="alert('Une application avec ce numéro d\'ordre et pour cette catégorie existe déjà');";
			$fgerr=1;
		}
		*/	
	}
	
	/*--> Rester sur le mode encours */
	if($vlmod!=""&&$fgerr!="") $tpmod="MODIFY";
	if($vladd!=""&&$fgerr!="") $tpmod="SUBMIT";
		
	/*--> Submit */
	if($vladd!=""&&$fgerr=="") {
		$q="INSERT INTO ".$config["dbprefixe"]."application(application_name, application_label, application_description, application_url, application_open, application_color, application_icon,application_order,application_categorie) VALUES(?,?,?,?,?,?,?,?,?)";
		$query=$bdd01->prepare($q);
		$query->execute(array($name,$label,$description,urlencode($url),$open,$color,$icon,$order,$categorie));
		$tpmod="";
	}

	/*--> Modify */
	if($vlmod!=""&&$fgerr=="") {
		$q="UPDATE ".$config["dbprefixe"]."application SET application_name=?, application_label=?, application_description=?, application_url=?, application_open=?, application_color=?, application_icon=?, application_order=?, application_categorie=? WHERE application_id=?";
		$query=$bdd01->prepare($q);
		$query->execute(array($name,$label,$description,urlencode($url),$open,$color,$icon,$order,$categorie,$id));
		$tpmod="";
	}
	
	/*--> Delete */
	if($vlsup!="") {
		delApplication($id);
	}
	

	echo "<form id='formulaire' class='form-horizontal' role='form' method='post' enctype='multipart/form-data'>";
	
	echo "<input id='id' name='id' type='hidden' value='".$id."'>";
	echo "<input id='tpmod' name='tpmod' type='hidden' value=''>";
	
  
//-- DISPLAY -------------------------------------------------------------------------------------------------------------------------------

if($tpmod=="") { 
    echo "<legend><h1>GESTION DES APPLICATIONS</h1></legend>";
    
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='fgadd' name='fgadd' class='btn btn-primary' onClick='$(\"#id\").val(\"\"); $(\"#tpmod\").val(\"SUBMIT\");' type='submit' value='Ajouter' />";
	echo "</div>";
	echo "</div>";

	echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
    echo "<thead>";
	echo "<th width='50px'>Action</th>";
	echo "<th width='70px'>Ordre</th>";
	echo "<th width='70px'>ID</th>";
	echo "<th width='70px'>Icône</th>";
	echo "<th width='150px'>Catégorie</th>";
	echo "<th>Nom</th>";
	
	echo "</thead>";
	
	$q="SELECT * FROM ".$config["dbprefixe"]."application, ".$config["dbprefixe"]."icon WHERE application_icon=icon_id";
	$query=$bdd01->prepare($q);
	$query->execute();
	while($row=$query->fetch()){	
		echo "<tr>";
		
		echo "<td align='center'>";
		echo "<a class='mybtn fa fa-file' onClick='$(\"#id\").val(\"".$row['application_id']."\"); $(\"#tpmod\").val(\"MODIFY\");$(\"#formulaire\").submit();'></a>";
		if($row['application_id']>0) {
			echo "&nbsp;";
			echo "<a class='mybtn fa fa-trash' onClick='$(\"#id\").val(\"".$row['application_id']."\"); $(\"#tpmod\").val(\"DELETE\");$(\"#formulaire\").submit();'></a>";
		}
		echo "</td>";

		echo "<td align='center'>";
		$q="SELECT * FROM ".$config["dbprefixe"]."application_categorie WHERE application_categorie_id=?";
		$query2=$bdd02->prepare($q);
		$query2->execute(array($row['application_categorie']));
		if($row2=$query2->fetch()){	
			echo $row2["application_categorie_order"]."-";
			$categorie=$row2["application_categorie_label"];
		}
		else {
			echo "0-";
			$categorie="";
		}
		echo str_pad($row["application_order"], 4, "0", STR_PAD_LEFT);
		echo "</td>";
		
		echo "<td align='center'>";
		echo $row['application_id'];
		echo "</td>";
		
		echo "<td align='center'>";
		echo "<a href='".urldecode($row['application_url'])."' target='_blank'><img src='local/images/icon/".$row['icon_url']."' width='40px' height='40px'></img></a>";
		echo "</td>";

		echo "<td>";
		echo $categorie;
		echo "</td>";

		echo "<td>";
		echo "<a href='".urldecode($row['application_url'])."' target='_blank'>".$row['application_name']."</a>";
		echo "</td>";
		
		echo "</tr>";
	} 
	
	echo "</table>";
}

//-- SUBMIT --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="SUBMIT") {
	// Entete du formulaire
    echo "<legend><h1>AJOUT APPLICATION</h1></legend>";
	
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vladd' name='vladd' class='btn btn-primary' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
	echo "</div>";
	echo "</div>";	

	// Valeur par défaut
	$order = 1;
	$categorie = 0;
	$open=0;
}

//-- MODIFY --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="MODIFY") {
	// Entete du formulaire
    echo "<legend><h1>MODIFICATION APPLICATION</h1></legend>";

	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlmod' name='vlmod' class='btn btn-primary' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
	echo "</div>";
	echo "</div>";	

	// Valeur par défaut
	$q = "SELECT * FROM ".$config["dbprefixe"]."application, ".$config["dbprefixe"]."icon WHERE application_id=? AND application_icon=icon_id";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
	if($row=$query->fetch()){		
		$name 			= $row["application_name"];
		$label 			= $row["application_label"];
		$description	= $row["application_description"];
		$url 			= $row["application_url"];
		$open 			= $row["application_open"];
		$color 			= $row["application_color"];
		$urlicon 		= $row["icon_url"];
		$icon 			= $row["application_icon"];
		$order 			= $row["application_order"];
		$categorie		= $row["application_categorie"];
	}
}

//-- DELETE --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="DELETE") {
	echo "<legend><h1>SUPPRESSION APPLICATION</h1></legend>";
	
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlsup' name='vlsup' class='btn btn-primary' type='submit' value='Confirmer la suppression' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";    
	echo "</div>";
	echo "</div>";	  
	
	// Valeur par défaut
	$q = "SELECT * FROM ".$config["dbprefixe"]."application, ".$config["dbprefixe"]."icon WHERE application_id=? AND application_icon=icon_id";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
	if($row=$query->fetch()){		
		echo "<div id='myavatarprofil' style='float:left; margin-right:5px; margin-bottom:10px; border:none;'>";
		echo "<img id='imgapp' src='local/images/icon/".$row["icon_url"]."' width='90px' height='90px'></img>";
		echo "</div>";
		
		echo $row['application_label']."<br>";
		echo "<span id='avatar_description'>";
		echo "id : ".$row['application_name']."<br>";
		echo "url : <a href='".$row['application_url']."' target='_black'>".urldecode($row['application_url'])."</a><br>";				
	}
}


//-- FORMULAIRE -----------------------------------------------------------------------------------------------------------------------------

if($tpmod=="SUBMIT"||$tpmod=="MODIFY") {
?>


	<fieldset class="row fieldset" style="clear:both">
		<legend>Description</legend>


		<div class="form-group">
			<label for="id_bis" class="col-sm-3 control-label">ID Application*</label>
			<div class="col-sm-6"><input name="id_bis" id="id_bis" type="titre" class="form-control" disabled="disabled" placeholder="ID Application" value="<? echo $id; ?>"></div>
		</div>

		<div class="form-group">
			<label for="name" class="col-sm-3 control-label">Nom*</label>
			<div class="col-sm-6"><input name="name" id="name" type="titre" class="form-control" placeholder="Nom" value="<? echo $name; ?>"></div>
		</div>

		<div class="form-group">
			<label for="label" class="col-sm-3 control-label">Libellé*</label>
			<div class="col-sm-6"><input name="label" id="label" type="titre" class="form-control" placeholder="Libellé" value="<? echo $label; ?>"></div>
		</div>		

		<div class="form-group">
			<label for="url" class="col-sm-3 control-label">Url*</label>
			<div class="col-sm-6"><input name="url" id="url" type="titre" class="form-control" placeholder="Url" value="<? echo urldecode($url); ?>"></div>
		</div>	

		<div class="form-group">
			<label for="open" class="col-sm-3 control-label">Mode d'ouverture*</label>
			<div class="col-sm-6">
				<select name="open" id="open" class="form-control">
					<option value='0' <? if($open==0) echo "selected"; ?>>Dans le portail</option>";
					<option value='1' <? if($open==1) echo "selected"; ?>>Nouvel onglet</option>";
				</select>
			</div>
		</div>

		<div class="form-group">
			<label for="color" class="col-sm-3 control-label">Couleur</label>
			<div class="col-sm-6">
				<input type="text" value="<? echo $color; ?>" id="color" name="color" class="pick-a-color form-control">
			</div>
		</div>		
				
		<div class="form-group">
			<label for="order" class="col-sm-3 control-label">Ordre*</label>
			<div class="col-sm-6"><input name="order" id="order" type="number" class="form-control" placeholder="Ordre" value="<? echo $order; ?>"></div>
		</div>

		<div class="form-group">
			<label for="categorie" class="col-sm-3 control-label">Categorie*</label>
			<div class="col-sm-6">
				<select name="categorie" id="categorie" class="form-control">
				<?
					if($categorie==0) $lbsel="selected";
					echo "<option value='0' ".$lbsel.">Aucune</option>";
					
					$q="SELECT * FROM ".$config["dbprefixe"]."application_categorie";
					$query=$bdd01->prepare($q);
					$query->execute(array($id));
					while($row=$query->fetch()){	
						$lbsel="";
						if($categorie==$row["application_categorie_id"]) {
							$lbsel="selected";
						}
						echo "<option value='".$row["application_categorie_id"]."' ".$lbsel.">".$row["application_categorie_label"]."</option>";
					}
				?>					
				</select>	
			</div>		
		</div>
		
		<div class="form-group">
			<label for="categorie" class="col-sm-3 control-label">Description</label>
			<div class="col-sm-6">			
				<textarea id="description" name="description" class="form-control" rows="4"><? echo $description; ?></textarea>
			</div>
		</div>
	</fieldset>

	<fieldset class="row fieldset" style="clear:both">
		<legend>Icône</legend>
		
		<input 	value="<? echo $icon; ?>" id="icon"	name="icon"	type="hidden"/>
		<?
		echo "<div style='width:140px; margin:auto; '>";
		echo "<div id='myavatarprofil' style='float:left; margin-right:5px; margin-bottom:10px; border:none;'>";
		echo "<img id='imgapp' src='local/images/icon/".$urlicon."' width='90px' height='90px'></img>";
		echo "</div>";
		echo "<div style='float:left;'>";
		echo "<a class='glyphicon glyphicon-folder-close' data-toggle='modal' data-target='#mymodal-01' title='Séléctionner un icône' onClick='ModalLoad(\"mymodal-01\",\"icon-selection\",0,0)'></a>";
		echo "</div>";
		echo "</div>";
		?>

		
	</fieldset>

<?
}

echo "</form>";

?>

<link rel="stylesheet" href="lib/pickColor/pick-a-color-1.2.3.min.css">
<script src="lib/pickColor/tinycolor-0.9.15.min.js"></script>
<script src="lib/pickColor/pick-a-color-1.2.3.min.js"></script>

<!-- SCRIPT --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<? if($tpmod=="") { ?>
	<script>
		$(document).ready(function() {
			$('#datatable').dataTable( {
				"oLanguage": { "sUrl": "lib/dataTables/dataTables.txt" },
				"iDisplayLength": 100,
				"aoColumnDefs" : [ {'bSortable' : false, 'aTargets' : [ 0,3 ]} ],
				"aaSorting": [[ 1, "asc" ]],
				"stateSave": true
			} );
		} );	
	</script>
<? } ?>

<? if($tpmod=="SUBMIT"||$tpmod=="MODIFY"||$tpmod=="DELETE") { ?>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#label').focus();

			$(".pick-a-color").pickAColor({
			  showSpectrum            : true,
				showSavedColors         : true,
				saveColorsPerElement    : true,
				fadeMenuToggle          : true,
				showAdvanced			: true,
				showBasicColors         : true,
				showHexInput            : true,
				allowBlank				: true,
				inlineDropdown			: true
			});		
		} );		
	</script>
<? } ?>

<script>
		<?php echo $jsaction ?>
</script>







	
	
