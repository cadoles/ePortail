<?
	$levelpage=1;
	include("include/permission.php");
	
	$tpmod				=$_POST['tpmod'];
	$vlmod				=$_POST['vlmod'];
	$vladd				=$_POST['vladd'];
	$vlsup				=$_POST['vlsup'];
	
	$user_id			=$_POST['user_id'];
	$user_login			=$_POST['user_login'];
	$user_password		=$_POST['user_password'];
	$user_passwordbis	=$_POST['user_passwordbis'];
	$user_lastname		=$_POST['user_lastname'];
	$user_firstname		=$_POST['user_firstname'];
	$user_pseudo		=$_POST['user_pseudo'];
	$user_email			=$_POST['user_email'];
	$user_profil		=$_POST['user_profil'];
	$user_avatar		=$_POST['user_avatar'];
	
	
	/*--> Controle de cohérance */
	if($vladd!=""||$vlmod!="") {
		$fgerr="";

		if($vladd!=""&&$user_password=="") {
			$jsaction="alert('Un mot de passe est obligatoire pour tout nouveau utilisateur');";
			$fgerr=1;
		}

		if($vladd!=""&&$user_login=="") {
			$jsaction="alert('Un login est obligatoire pour tout nouveau utilisateur');";
			$fgerr=1;
		}
		
		if($user_password!=$user_passwordbis) {
			$jsaction="alert('Votre mot de passe n\'est pas confirmé');";
			$fgerr=1;
		}

		if($user_lastname==""||$user_firstname==""||$user_email==""||$user_profil=="") {
			$jsaction="alert('Vous devez renseigner l\'ensemble des informations');";
			$fgerr=1;
		}
		
		$q="SELECT * FROM ".$config["dbprefixe"]."user WHERE (user_login=? OR user_email=? OR (?!='' AND user_pseudo=?))";
		$tbq=array($user_login,$user_email,$user_pseudo,$user_pseudo);
		if($vlmod!="") {
			$q=$q." AND user_id!=?";
			array_push($tbq,$user_id);
		}

		$query=$bdd01->prepare($q);
		$query->execute($tbq);
		if($row=$query->fetch()) {
			$jsaction="alert('Un utilisateur avec cet email ou ce pseudo existe déjà');";
			$fgerr=1;
		}
	}
	
	/*--> Rester sur le mode encours */
	if($vlmod!=""&&fgerr!="") $tpmod="MODIFY";
	if($vladd!=""&&fgerr!="") $tpmod="SUBMIT";
		
	/*--> Submit */
	if($vladd!=""&&$fgerr=="") {
		$q="INSERT INTO ".$config["dbprefixe"]."user(user_login, user_password, user_lastname, user_firstname, user_pseudo, user_email, user_avatar, user_profil) VALUES(?,?,?,?,?,?,?,?)";
		$query=$bdd01->prepare($q);
		$query->execute(array($user_login,md5($user_password),$user_lastname,$user_firstname,$user_pseudo,$user_email,$user_avatar,$user_profil));
		$tpmod="";
	}

	/*--> Modify */
	if($vlmod!=""&&$fgerr=="") {
		$q="UPDATE ".$config["dbprefixe"]."user SET user_lastname=?, user_firstname=?, user_pseudo=?, user_email=?, user_avatar=?, user_profil=? WHERE user_id=?";
		$tbq=array($user_lastname,$user_firstname,$user_pseudo,$user_email,$user_avatar,$user_profil,$user_id);
		$query=$bdd01->prepare($q);
		$query->execute($tbq);
		
		// Changement de mot de passe
		if($user_password!=""&&$user_passwordbis!="") {
			$q="UPDATE ".$config["dbprefixe"]."user SET user_password=md5(?) WHERE user_id=?";
			$tbq=array($user_password,$user_id);
			$query=$bdd01->prepare($q);
			$query->execute($tbq);
		}
		$tpmod="";
	}
	
	/*--> Delete */
	if($vlsup!="") {
		delUser($user_id);
		$tpmod="";
	}

	echo "<form id='formulaire' class='form-horizontal' role='form' method='post' enctype='multipart/form-data'>";
	
	echo "<input id='user_id' name='user_id' type='hidden' value='".$user_id."'>";
	echo "<input id='tpmod' name='tpmod' type='hidden' value=''>";
	
  
//-- DISPLAY -------------------------------------------------------------------------------------------------------------------------------

if($tpmod=="") { 
    echo "<legend><h1>GESTION DES UTILISATEURS</h1></legend>";

	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	if ($config['modeAuthentification']=="MYSQL") {
		echo "<input id='fgadd' name='fgadd' class='btn btn-primary' onClick='$(\"#user_id\").val(\"\"); $(\"#tpmod\").val(\"SUBMIT\");' type='submit' value='Ajouter' />";
	}
	echo "</div>";
	echo "</div>";
	
	echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
    echo "<thead>";
	echo "<th width='50px'>Action</th>";
	echo "<th>ID</th>";
	echo "<th width='50px'>Avatar</th>";
	echo "<th>Nom</th>";
	echo "<th>Login</th>";
	echo "<th>Pseudo</th>";
	echo "<th>Profil</th>";
	echo "</thead>";

	$q="SELECT * FROM ".$config["dbprefixe"]."user, ".$config["dbprefixe"]."profil WHERE profil_id=user_profil AND user_id > 0";
	$query=$bdd01->prepare($q);
	$query->execute();
	while($row=$query->fetch()){	
		echo "<tr>";

		echo "<td align='center'>";
		echo "<a class='mybtn fa fa-file' onClick='$(\"#user_id\").val(\"".$row['user_id']."\"); $(\"#tpmod\").val(\"MODIFY\");$(\"#formulaire\").submit();'></a>";
		if ($config['modeAuthentification']=="MYSQL") {
			echo "&nbsp;";		
			echo "<a class='mybtn fa fa-trash'       onClick='$(\"#user_id\").val(\"".$row['user_id']."\"); $(\"#tpmod\").val(\"DELETE\");$(\"#formulaire\").submit();'></a>";
		}
		echo "</td>";

		echo "<td align='center'>";
		echo $row['user_id'];
		echo "</td>";
		
		echo "<td align='center'>";
		if($row['user_avatar']=="") $class=" class='myavatarvide'";
		echo "<div id='myavatarprofil' style='width:40px; border:none;' $class >";
		if($row['user_avatar']=="") 
			echo "<img src='style/images/blank.gif' width='40px' height='40px'></img>";
		else
			echo "<img src='local/images/avatar/".$row['user_avatar']."' width='40px' height='40px'></img>";
		echo "</div>";
		echo "</td>";
		
		echo "<td>";
		echo $row['user_firstname']." ".$row['user_lastname']."<br>";
		echo "</td>";

		echo "<td>";
		echo $row['user_login']."<br>";
		echo "</td>";

		echo "<td>";
		echo $row['user_pseudo']."<br>";
		echo "</td>";

		echo "<td>";
		echo $row['profil_label']."<br>";
		echo "</td>";

		echo "</tr>";
	} 

	echo "</table>";
}

//-- SUBMIT --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="SUBMIT") {
	// Entete du formulaire
    echo "<legend><h1>AJOUT UTILISATEUR</h1></legend>";
	
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vladd' name='vladd' class='btn btn-primary' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
	echo "</div>";
	echo "</div>";	

	// Valeur par défaut
	if($user_profil=="") {$user_profil="50";}
}

//-- MODIFY --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="MODIFY") {
	// Entete du formulaire
    echo "<legend><h1>MODIFICATION UTILISATEUR</h1></legend>";

	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlmod' name='vlmod' class='btn btn-primary' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
	echo "</div>";
	echo "</div>";	

	// Valeur par défaut
	$q = "SELECT * FROM ".$config["dbprefixe"]."user WHERE user_id=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($user_id));
	if($row=$query->fetch()){	
		$user_login 		= $row["user_login"];
		$user_lastname 		= $row["user_lastname"];
		$user_firstname 	= $row["user_firstname"];
		$user_pseudo	 	= $row["user_pseudo"];
		$user_email 		= $row["user_email"];
		$user_profil		= $row["user_profil"];
		$user_avatar		= $row["user_avatar"];
	}
}

//-- DELETE --------------------------------------------------------------------------------------------------------------------------------

elseif($tpmod=="DELETE") {
	echo "<legend><h1>SUPPRESSION UTILISATEUR</h1></legend>";

	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlsup' name='vlsup' class='btn btn-primary' type='submit' value='Confirmer la suppression' />";
	echo "&nbsp;";
	echo "<input class='btn btn-primary' type='submit' value='Annuler' />";    
	echo "</div>";
	echo "</div>";	

	// Valeur par défaut
	$q="SELECT * FROM ".$config["dbprefixe"]."user, ".$config["dbprefixe"]."profil WHERE user_id=? AND profil_id=user_profil";
	$query=$bdd01->prepare($q);
	$query->execute(array($user_id));
	if($row=$query->fetch()){	
		if($row['user_avatar']=="") $class=" class='myavatarvide'";
		echo "<div id='myavatarprofil' style='float:left; margin-right:5px; margin-bottom:10px; border:none;' $class >";
		if($row['user_avatar']!="") 
			echo "<img id='img_avatar' name='img_avatar' src='local/images/avatar/".$row['user_avatar']."' width='90px' height='90px'></img>";
		else
			echo "<img id='img_avatar' name='img_avatar' src='style/images/blank.gif' width='90px' height='90px'></img>";
		echo "</div>";
		
		echo $row['user_firstname']." ".$row['user_lastname']."<br>";
		echo "<span id='avatar_description'>";
		echo "Login : ".$row['user_login']."<br>";
		echo "Pseudo : ".$row['user_pseudo']."<br>";
		echo "Profil : ".$row['profil_label']."<br>";			
		echo "</span>";
		echo "</div>";

		echo "</div>";
	}
	
	
}


//-- FORMULAIRE -----------------------------------------------------------------------------------------------------------------------------

if($tpmod=="SUBMIT"||$tpmod=="MODIFY") {
	if($user_avatar=="") $class=" class='myavatarvide'";

	echo "<div style='width:110px; margin:auto; '>";
	echo "<div id='myavatarprofil' style='float:left; margin-right:5px; margin-bottom:10px; border:none;' $class >";
	if($user_avatar!="") 
		echo "<img id='img_avatar_profil' name='img_avatar' src='local/images/avatar/".$user_avatar."' width='90px' height='90px'></img>";
	else
		echo "<img id='img_avatar_profil' name='img_avatar' src='style/images/blank.gif' width='90px' height='90px'></img>";
	echo "</div>";
	
	echo "<div style='float:left;'>";
	echo "<a id='btnplus' class='mybtn fa fa-plus' data-toggle='modal' data-target='#mymodal-01' title='Ajouter un avatar' onClick='ModalLoad(\"mymodal-01\",\"avatar-cropupdate\",0,0);'></a>";
	echo "<br>";
	echo "<a id='btndel'  class='mybtn fa fa-trash' onClick='$(\"#myavatarprofil\").addClass(\"myavatarvide\"); $(\"#myavatarprofil > #img_avatar_profil\").attr(\"src\",\"\"); $(\"#user_avatar\").val(\"\");' title='Supprimer avatar'></a>";
	echo "</div>";
	echo "</div>";
	
	echo "<input id='user_avatar' name='user_avatar'  type='hidden' value='".$user_avatar."'>";

?>
	
	<fieldset class="row fieldset" style="clear:both">
		<legend>Coordonnés</legend>

		
		<div class="form-group">
			<label for="user_id_bis" class="col-sm-3 control-label">ID Utilisateur*</label>
			<div class="col-sm-6"><input name="user_id_bis" id="user_id_bis" type="titre" class="form-control" disabled="disabled" placeholder="ID Utilisateur" value="<? echo $user_id; ?>"></div>
		</div>
	
		<? if($tpmod=="MODIFY") $disable="readonly"; ?>
		<div class="form-group">
			<label for="user_login" class="col-sm-3 control-label">Login*</label>
			<div class="col-sm-6"><input name="user_login" id="user_login" type="titre" class="form-control" <? echo $disable; ?> placeholder="Login" value="<? echo $user_login; ?>"></div>
		</div>

		<? if($config['modeAuthentification']=="MYSQL") { $disable=""; ?>
		<div class="form-group"> 
			<label for="user_password" class="col-sm-3 control-label">Password*</label>
			<div class="col-sm-6"><input name="user_password" id="user_password" type="password" class="form-control" placeholder="Password" value="<? echo $user_password; ?>"></div>
		</div>		

		<div class="form-group"> 
			<label for="user_passwordbis" class="col-sm-3 control-label">Confirmer Password*</label>
			<div class="col-sm-6"><input name="user_passwordbis" id="user_passwordbis" type="password" class="form-control" placeholder="Confirmer Password" value="<? echo $user_passwordbis; ?>"></div>
		</div>	
		<? }
		else  $disable="readonly"; 
		?>

		<div class="form-group">
			<label for="user_lastname" class="col-sm-3 control-label">Nom*</label>
			<div class="col-sm-6"><input name="user_lastname" id="user_lastname" type="titre" class="form-control" <? echo $disable; ?> placeholder="Nom" value="<? echo $user_lastname; ?>"></div>
		</div>

		<div class="form-group">
			<label for="user_firstname" class="col-sm-3 control-label">Prénom*</label>
			<div class="col-sm-6"><input name="user_firstname" id="user_firstname" type="titre" class="form-control" <? echo $disable; ?> placeholder="Prénom" value="<? echo $user_firstname; ?>"></div>
		</div>

		<div class="form-group">
			<label for="user_email" class="col-sm-3 control-label">Email*</label>
			<div class="col-sm-6"><input name="user_email" id="user_email" type="email" class="form-control" <? echo $disable; ?> placeholder="Email" value="<? echo $user_email; ?>"></div>
		</div>
		
		<div class="form-group">
			<label for="user_pseudo" class="col-sm-3 control-label">Pseudo</label>
			<div class="col-sm-6"><input name="user_pseudo" id="user_pseudo" type="titre" class="form-control" placeholder="Pseudo" value="<? echo $user_pseudo; ?>"></div>
		</div>
	</fieldset>


	<fieldset  class="row fieldset">
		<legend>Profil</legend>
		
		<div class="form-group">
			<label for="user_profil" class="col-sm-3 control-label">Profil</label>
			<div class="col-sm-6">
				<select name="user_profil" id="user_profil" class="form-control">
				<?
					$q="SELECT * FROM ".$config["dbprefixe"]."profil";
					$query=$bdd01->prepare($q);
					$query->execute();
					while($row=$query->fetch()){						
						$lbsel="";
						if($user_profil==$row["profil_id"]) {
							$lbsel="selected";
						}
						echo "<option value='".$row["profil_id"]."' ".$lbsel.">".$row["profil_label"]."</option>";
					}
				?>					
				</select>			
			</div>
		</div>
	</fieldset>
<?
}

echo "</form>";
?>

<!-- SCRIPT --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<? if($tpmod==""||$tpmod=="MODIFY") { ?>
	<script>
		$(document).ready(function() {
			$('#datatable').dataTable( {
				"oLanguage": { "sUrl": "lib/dataTables/dataTables.txt" },
				"iDisplayLength": 100,
				"aoColumnDefs" : [ {'bSortable' : false, 'aTargets' : [ 0,2 ]} ],
				"aaSorting": [[ 1, "asc" ]],
				"stateSave": true
			} );
		} );	
	</script>
<? } ?>

<? if($tpmod=="SUBMIT"||$tpmod=="MODIFY"||$tpmod=="DELETE") { ?>
	<script type="text/javascript">
		$('#user_login').focus();		
	</script>
<? } ?>

<script>
		<?php echo $jsaction ?>
</script>
	
	
