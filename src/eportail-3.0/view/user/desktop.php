<?

	$mode=$_GET['mode'];

	if($mode=="widget") {
		$id=$_GET['id'];
	}

	// Recherche du mode d'affichage du widget
	$q="SELECT * FROM ".$config["dbprefixe"]."panel_widget WHERE panel_widget_id=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
	if($row=$query->fetch()){
		// Les styles hors "sans bordure" doivent avoir une marge
		if($row["panel_widget_style"]!=2) {
			echo "<style>body{margin:0px 5px 0px 5px;}</style>";
		}
	}
	
	// Affichage des catégorie
	$div=array();
	echo "<ul id='navitem' class='nav'>";
	foreach ($tbcat as $cat) {
		if($firstcat=="") $firstcat=$cat[1];
		
		if($cat[3]!="") {
			echo "<style>";
			echo "#cat-".$cat[1]." {";
			echo "background-color:#".$cat[3]." !important;";
			echo "border-bottom: 5px solid #".$cat[3]." !important;";
			echo "}";
			
			echo "#cat-".$cat[1]." a:hover {";
			echo "background-color:#".$cat[3]." !important;";
			echo "}";
			
			echo "</style>";
		}
		
		echo "<li id='cat-".$cat[1]."'><a onClick='changeCategorie(".$cat[1].");'>".$cat[2]."</a></li>";
	}

	
	// Ajout du widget favoris
	echo "<li id='cat-XX'><a class='mybtn fa-heart fa' onClick='changeBookmark();'></a></li>";
	echo "</ul>";
	
	foreach ($tbcat as $cat) {	
		// Calcul des items pour cette catégorie
		echo "<div id='containeritem-".$cat[1]."' class='containeritem' style='display:none;margin-bottom: 100px;'>";
		foreach ($tbitem as $item) {
			if($cat[1]==$item[2]) {
				$color="";
				if($item[9]!="") $color="background-color: #".$item[9].";";
				
				echo "<div id='myitem' style='$color' rel='tooltip' title='".htmlspecialchars($item[5],ENT_QUOTES)."' data-placement='bottom'>";

				if($item[8]==0)
					echo "<a href='index.php?framed=true&view=".urldecode($item[6])."' target='_top'>";
				else
					echo "<a href='".urldecode($item[6])."' target=_blank>";
					
				echo "<img src='local/images/icon/".$item[7]."'/>";
				echo "<label>".$item[4]."</label>";
				
				echo "</a>";
				echo "</div>";
			}
		}
		
		// Des div en plus pour que les tooltip du bas n'agrandissent la page
		echo "<div id='myitem' style='background-color:transparent'></div>";
		echo "<div id='myitem' style='background-color:transparent'></div>";
		echo "<div id='myitem' style='background-color:transparent'></div>";
		echo "<div id='myitem' style='background-color:transparent'></div>";
		
		echo "</div>";
	}
	
	// Création du bloque favoris
	echo "<div id='containeritem-XX' class='containeritem' style='display:none;margin-bottom: 100px;'>";
	if($mode=="") $tpwid="bureau";
	else $tpwid="application";
	echo "<iframe id='bookmarkframe' class='myframe' src='widget.php?view=view/user/bookmark.php&mode=$tpwid' style='height:250px; width:100%;' frameborder='0' ></iframe>";
	echo "</div>";
	
	// Calcul des items pour cette catégorie
	//echo "<div id='containeritem-".$cat[1]."' class='containeritem' style='display:none'>";
	
?>

<style>

</style>
<script src="lib/iframeAuto/jquery.iframe-auto-height.js"></script>
<script src="lib/iframeAuto/jquery.browser.js"></script>

<script>
	function changeCategorie(idcat) {
		$("#navitem li").removeClass("active");
		$("#cat-"+idcat).addClass("active");
		$(".containeritem").css("display","none");
		$("#containeritem-"+idcat).css("display","block");
		$("[rel=tooltip]").tooltip({'animation':false});
		return false;
	}
	
	function changeBookmark() {
		$(".containeritem").css("display","none");
		$("#navitem li").removeClass("active");
		$("#cat-XX").addClass("active");		
		$("#containeritem-XX").css("display","block");
		$("[rel=tooltip]").tooltip({'animation':false});
		
		$('#bookmarkframe').iframeAutoHeight({
			minHeight: 240, // Sets the iframe height to this value if the calculated value is less
			heightOffset: 50 // Optionally add some buffer to the bottom
		});
				
		return false;	}
	
	
	$(document).ready(function()
	{	
		changeCategorie(<? echo $firstcat; ?>);
	});
	
</script>
