<?
	if(!isset($_SESSION[$keymodal])) {
		header("location:index.php?view=diened.php");
		exit();
	}
	
	$mode	=$_SESSION[$keymodal]['modal-mode'];
	$idpan	=$_SESSION[$keymodal]['id1'];
	$idpwd	=$_SESSION[$keymodal]['id2'];

	
	$fgval			=$_POST['fgval'];
	$idwid			=$_POST['idwid'];
	$fgclo			=$_POST['fgclo'];
	
	$widget_label	=$_POST['widget_label'];
	$widget_url		=$_POST['widget_url'];
	$widget_adjust	=$_POST['widget_adjust'];
	$widget_height	=$_POST['widget_height'];
	$widget_color	=$_POST['widget_color'];
	$widget_style	=$_POST['widget_style'];
	
	
	

	// Permissions = si non utiisateur exist
	if($_SESSION['user_id']=="") die();	
	
	// Permissions = si non non propriéataire de la page exist
	$q="SELECT panel_user FROM ".$config["dbprefixe"]."panel WHERE panel_id=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($idpan));
	if($row=$query->fetch()){
		$idpro=$row['panel_user'];
	}
	if($idpro!=$_SESSION['user_id']&&$_SESSION['user_profil']!=1) {
		die();
	}	

	if($idwid!=""||$mode=="modwidget") {
		$fgparam=1;
	}

	if($fgval!=""&&$mode=="addwidget") {
		
		// Recherche du type de contenu du widget
		$q="SELECT * FROM ".$config["dbprefixe"]."widget WHERE widget_id=?";
		$query=$bdd01->prepare($q);
		$query->execute(array($idwid));
		if($row=$query->fetch()){
			$tpwid=$row["widget_type"];
		}
		
		// Rechercher le dernier order de la location C1R1 de cette page
		$order=1;
		$q="SELECT panel_widget_order FROM ".$config["dbprefixe"]."panel_widget WHERE panel_widget_panel=? AND panel_widget_loc='C1R1' ORDER BY panel_widget_order DESC LIMIT 1";
		$query=$bdd01->prepare($q);
		$query->execute(array($idpan));
		if($row=$query->fetch()){
			$order=$row['page_widget_order']+1;
		}
		
		// Création du widget
		$q="INSERT INTO ".$config["dbprefixe"]."panel_widget (panel_widget_loc,panel_widget_order,panel_widget_widget,panel_widget_panel,panel_widget_label,panel_widget_url,panel_widget_adjust,panel_widget_height,panel_widget_color,panel_widget_style) VALUES(?,?,?,?,?,?,?,?,?,?)";
		$query=$bdd01->prepare($q);
		$query->execute(array('C1R1',$order,$idwid,$idpan,$widget_label,$widget_url,$widget_adjust,$widget_height,$widget_color,$widget_style));
		$fgclo=1;
	}
		

	if($fgval!=""&&$mode=="modwidget") {
		// Création du widget
		$q="UPDATE ".$config["dbprefixe"]."panel_widget SET panel_widget_label=?, panel_widget_url=?, panel_widget_adjust=?, panel_widget_height=?, panel_widget_color=?, panel_widget_style=? WHERE panel_widget_id=?";
		$query=$bdd01->prepare($q);
		$query->execute(array($widget_label,$widget_url,$widget_adjust,$widget_height,$widget_color,$widget_style,$idpwd));
		$fgclo=1;
	}

	
	if($fgclo!="") {
		// Action javascript Ajout du panel directement + fermeture de la popup
		$jsaction="recharge(".$idpan.")";
	}
	
?>


<script type="text/JavaScript">
	function closemodal() {
		window.parent.closemodal();	
	}
	
	function recharge(idpan) {
		//parent.parent.document.location.href='index.php?action=page&id='+idpan;
		window.parent.ModalClose('#<? echo $_SESSION[$keymodal]["idmodal"] ?>');	
		window.parent.location.reload();
	}
	
	$(document).ready(function () {

		$(".pick-a-color").pickAColor({
		  showSpectrum            : true,
			showSavedColors         : true,
			saveColorsPerElement    : true,
			fadeMenuToggle          : true,
			showAdvanced			: true,
			showBasicColors         : true,
			showHexInput            : true,
			allowBlank				: true,
			inlineDropdown			: true
		});
	});	
	
	<?php echo $jsaction ?>
</script>

<link rel="stylesheet" href="lib/pickColor/pick-a-color-1.2.3.min.css">
<script src="lib/pickColor/tinycolor-0.9.15.min.js"></script>
<script src="lib/pickColor/pick-a-color-1.2.3.min.js"></script>
	

<?
//== SELECTION DU WIDGET =====================================================================================================================================================================

echo "<form id='formulaire' class='form-horizontal' role='form' method='post' enctype='multipart/form-data'>";
echo "<input type='hidden' name='idpan' id='idpan' value='$idpan' />";
echo "<input type='hidden' name='idwid' id='idwid' value='$idwid' />";
echo "<input type='hidden' name='idpwd' id='idpwd' value='$idpwd' />";
echo "<input type='hidden' name='mode'  id='mode'  value='$mode'  />";

if($fgparam!=1) {

	echo "<a class='btn btn-primary' onclick='window.parent.ModalClose(\"#".$_SESSION[$keymodal]["idmodal"]."\");'>Fermer</a>";

		
	echo "<table cellpadding='0' cellspacing='0' border='0' class='table table-striped table-bordered' id='datatable'>";
    echo "<thead>";
	echo "<th width='40px'>Action</th>";
	echo "<th width='40px'>Icone</th>";
	echo "<th width='110px'>Catégorie</th>";
	echo "<th>Nom</th>";
	echo "</thead>";
	
	$q="SELECT * FROM ".$config["dbprefixe"]."widget, ".$config["dbprefixe"]."widget_type, ".$config["dbprefixe"]."icon WHERE widget_type=widget_type_id AND widget_icon=icon_id";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
	while($row=$query->fetch()){
		echo "<tr style='font-size:90%'>";

		echo "<td align='center'>";
		echo "<a class='mybtn fa fa-plus' onClick='$(\"#idwid\").val(\"".$row['widget_id']."\"); $(\"#formulaire\").submit();'></a>";
		echo "</td>";

		echo "<td align='center'>";
		echo "<img src='local/images/icon/".$row['icon_url']."' width='40px' height='40px'></img>";
		echo "</td>";
		
		echo "<td>";
		
		$q="SELECT * FROM ".$config["dbprefixe"]."widget_categorie WHERE widget_categorie_id=?";
		$query2=$bdd01->prepare($q);
		$query2->execute(array($row['widget_categorie']));
		if($row2=$query2->fetch()) echo $row2['widget_categorie_label'];
		
		echo "</td>";
		
		echo "<td>";
		echo $row['widget_name']."<br>".$row['widget_label'];
		echo "</td>";
		
		echo "</tr>";
	
	}	
	echo "</table>";
}


//== PARAMETRAGE DU WIDGET ===================================================================================================================================================================
else {
	
	echo "<input id='fgval' name='fgval' class='btn btn-primary' type='submit' value='Valider' />";
	echo "&nbsp;";
	echo "<a class='btn btn-primary' onclick='window.parent.ModalClose(\"#".$_SESSION[$keymodal]["idmodal"]."\");'>Annuler</a>";
	
	// Mode modification
	if($mode=="modwidget") {
		$q="SELECT * FROM ".$config["dbprefixe"]."panel_widget WHERE panel_widget_id=?";
		$query=$bdd01->prepare($q);
		$query->execute(array($idpwd));
		if($row=$query->fetch()){
			$widget_label	= $row["panel_widget_label"];
			$widget_url		= $row["panel_widget_url"];
			$widget_style	= $row["panel_widget_style"];
			$widget_adjust	= $row["panel_widget_adjust"];
			$widget_height	= $row["panel_widget_height"];
			$widget_color	= $row["panel_widget_color"];
			$idwid			= $row["panel_widget_widget"];
		}	
	}

	// Recherche du paramétrage par défaut du widget
	$q="SELECT * FROM ".$config["dbprefixe"]."widget WHERE widget_id=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($idwid));
	if($row=$query->fetch()){
		$lbwid=$row["widget_name"];
		$ulwid=$row["widget_url"];
		$sywid=$row["widget_style"];
		$adwid=$row["widget_adjust"];
		$hewid=$row["widget_height"];
		
		
		if($widget_label=="")	$widget_label=$lbwid;
		if($widget_adjust=="") 	$widget_adjust=$adwid;
		if($widget_height=="") 	$widget_height=$hewid;
		if($widget_style=="") 	$widget_style=$sywid;
		
		if($ulwid!="")			$widget_url="";
	}	
	

?>
	<div class="form-group">
		<label for="widget_label" class="col-sm-3 control-label">Titre*</label>
		<div class="col-sm-6"><input name="widget_label" id="widget_label" type="titre" class="form-control" placeholder="Titre" value="<? echo $widget_label; ?>"></div>
	</div>
	
	
	<div class="form-group" <? if($ulwid!="") echo "style='display:none;'"; ?>>
		<label for="widget_url" class="col-sm-3 control-label">URL*</label>
		<div class="col-sm-6"><input name="widget_url" id="widget_url" type="titre" class="form-control" placeholder="URL" value="<? echo $widget_url; ?>"></div>
	</div>
			

	<div class="form-group">
		<label for="widget_height" class="col-sm-3 control-label">Hauteur*</label>
		<div class="col-sm-6"><input name="widget_height" id="widget_height" type="number" class="form-control" placeholder="Hauteur" value="<? echo $widget_height; ?>"></div>
	</div>

	<div class="form-group">
		<label for="widget_adjust" class="col-sm-3 control-label">Ajuster*</label>
		<div class="col-sm-6">
			<select name="widget_adjust" id="widget_adjust" class="form-control">
			<?
				if($widget_adjust==0) $lbsel="selected";
				echo "<option value='0' ".$lbsel.">Non</option>";
				
				$lbsel="";
				if($widget_adjust==1) $lbsel="selected";
				echo "<option value='1' ".$lbsel.">Oui</option>";
			?>					
			</select>			
		</div>
	</div>		

	<div class="form-group">
		<label for="widget_color" class="col-sm-3 control-label">Couleur</label>
		<div class="col-sm-6">
			<input type="text" value="<? echo $widget_color; ?>" id="widget_color" name="widget_color" class="pick-a-color form-control">
		</div>
	</div>
	
	<div class="form-group">
		<label for="widget_style" class="col-sm-3 control-label">Style*</label>
		<div class="col-sm-6">
			<select name="widget_style" id="widget_style" class="form-control">
			<?
				$q="SELECT * FROM ".$config["dbprefixe"]."widget_style";
				$query=$bdd01->prepare($q);
				$query->execute();
				while($row=$query->fetch()){
					$lbsel="";
					if($widget_style==$row["widget_style_id"]) {
						$lbsel="selected";
					}
					echo "<option value='".$row["widget_style_id"]."' ".$lbsel.">".$row["widget_style_label"]."</option>";
				}
			?>					
			</select>			
		</div>
	</div>

	<script type="text/javascript">
		$('#widget_label').focus();
	</script>	
<?
}
	echo "<form>";
?>


<!-- SCRIPT --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<? if($fgparam!=1) { ?>
	<script>
		$(document).ready(function() {
			$('#datatable').dataTable( {
				"oLanguage": { "sUrl": "lib/dataTables/dataTables.txt" },
				"iDisplayLength": 5,
				"aoColumnDefs" : [ {'bSortable' : false, 'aTargets' : [ 0,1 ]} ],
				"aaSorting": [[ 2, "asc" ]],
				"bLengthChange": false
			} );
		} );	
		
		<?php echo $jsaction ?>
	</script>
<? } ?>

