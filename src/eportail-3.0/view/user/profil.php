<?
	$levelpage=50;
	include("include/permission.php");
	
	$user_id			=$_SESSION['user_id'];
	
	$vlmod				=$_POST['vlmod'];
	
	$user_login			=$_POST['user_login'];
	$user_password		=$_POST['user_password'];
	$user_passwordbis	=$_POST['user_passwordbis'];
	$user_lastname		=$_POST['user_lastname'];
	$user_firstname		=$_POST['user_firstname'];
	$user_pseudo		=$_POST['user_pseudo'];
	$user_email			=$_POST['user_email'];
	$user_avatar		=$_POST['user_avatar'];
	
	
	/*--> Controle de cohérance */
	if($vlmod!="") {
		$fgerr="";

		if($user_password!=$user_passwordbis) {
			$jsaction="alert('Votre mot de passe n\'est pas confirmé');";
			$fgerr=1;
		}

		if($user_lastname==""||$user_firstname==""||$user_email=="") {
			$jsaction="alert('Vous devez renseigner l\'ensemble des informations');";
			$fgerr=1;
		}
		
		$q="SELECT * FROM ".$config["dbprefixe"]."user WHERE (user_login=? OR user_email=? OR (?!='' AND user_pseudo=?))";
		$tbq=array($user_login,$user_email,$user_pseudo,$user_pseudo);
		if($vlmod!="") {
			$q=$q." AND user_id!=?";
			array_push($tbq,$user_id);
		}

		$query=$bdd01->prepare($q);
		$query->execute($tbq);
		if($row=$query->fetch()) {
			$jsaction="alert('Un utilisateur avec cet email ou ce pseudo existe déjà');";
			$fgerr=1;
		}
		$query->closeCursor();
	}
	
	/*--> Rester sur le mode encours */
	if($vlmod!=""&&fgerr!="") $tpmod="MODIFY";
		
	/*--> Modify */
	if($vlmod!=""&&$fgerr=="") {
		$q="UPDATE ".$config["dbprefixe"]."user SET user_lastname=?, user_firstname=?, user_pseudo=?, user_email=?, user_avatar=? WHERE user_id=?";
		$tbq=array($user_lastname,$user_firstname,$user_pseudo,$user_email,$user_avatar,$user_id);
		$query=$bdd01->prepare($q);
		$query->execute($tbq);
		
		// Changement de mot de passe
		if($user_password!=""&&$user_passwordbis!="") {
			$q="UPDATE ".$config["dbprefixe"]."user SET user_password=md5(?) WHERE user_id=?";
			$tbq=array($user_password,$user_id);
			$query=$bdd01->prepare($q);
			$query->execute($tbq);
		}
		$tpmod="";
		
		$_SESSION['user_firstname']		= $user_firstname;
		$_SESSION['user_lastname']		= $user_lastname;
		$_SESSION['user_pseudo']		= $user_pseudo;
		$_SESSION['user_avatar']		= $user_avatar;		
		
		$jsaction="document.location.reload();";
	}

    
    echo "<form class='form-horizontal' role='form' method='post' enctype='multipart/form-data'>";

	echo "<input id='user_id' name='user_id' type='hidden' value='".$user_id."'>";
	echo "<input id='tpmod' name='tpmod' type='hidden' value=''>";
	
  
	// Entete du formulaire
    echo "<legend><h1>PROFIL</h1></legend>";
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlmod' name='vlmod' type='submit' class='btn btn-primary' value='Enregistrer'>";
	echo "</div>";
	echo "</div>";

	// Valeur par défaut
	$q = "SELECT * FROM ".$config["dbprefixe"]."user WHERE user_id=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($user_id));
	if($row=$query->fetch()) {
		$user_login 		= $row["user_login"];
		$user_lastname 		= $row["user_lastname"];
		$user_firstname 	= $row["user_firstname"];
		$user_pseudo	 	= $row["user_pseudo"];
		$user_email 		= $row["user_email"];
		$user_profil_id		= $row["user_profil_id"];
		$user_avatar		= $row["user_avatar"];
	}
	$query->closeCursor();
	
	if($user_avatar=="") $class=" class='myavatarvide'";

	echo "<div style='width:110px; margin:auto; '>";
	echo "<div id='myavatarprofil' style='float:left; margin-right:5px; margin-bottom:10px; border:none;' $class >";
	if($user_avatar!="") 
		echo "<img id='img_avatar_profil' name='img_avatar' src='local/images/avatar/".$user_avatar."' width='90px' height='90px'></img>";
	else
		echo "<img id='img_avatar_profil' name='img_avatar' src='style/images/blank.gif' width='90px' height='90px'></img>";
	echo "</div>";
	
	echo "<div style='float:left;'>";
	echo "<a id='btnplus' class='mybtn fa fa-plus' data-toggle='modal' data-target='#mymodal-01' title='Ajouter un avatar' onClick='ModalLoad(\"mymodal-01\",\"avatar-cropupdate\",0,0);'></a>";
	echo "<br>";
	echo "<a id='btndel'  class='mybtn fa fa-trash' onClick='$(\"#myavatarprofil\").addClass(\"myavatarvide\"); $(\"#myavatarprofil > #img_avatar_profil\").attr(\"src\",\"\"); $(\"#user_avatar\").val(\"\");' title='Supprimer avatar'></a>";
	echo "</div>";
	echo "</div>";
	
	echo "<input id='user_avatar' name='user_avatar'  type='hidden' value='".$user_avatar."'>";
	?>

	<fieldset id="encadrer" style="clear:both">
		<div class="form-group">
			<label for="user_id_bis" class="col-sm-3 control-label">ID Utilisateur*</label>
			<div class="col-sm-6"><input name="user_id_bis" id="user_id_bis" type="titre" class="form-control" disabled="disabled" placeholder="ID Utilisateur" value="<? echo $user_id; ?>"></div>
		</div>
	
		<div class="form-group">
			<label for="user_login" class="col-sm-3 control-label">Login*</label>
			<div class="col-sm-6"><input name="user_login" id="user_login" type="titre" class="form-control" disabled="disabled" placeholder="Login" value="<? echo $user_login; ?>"></div>
		</div>

		<? if($config['modeAuthentification']=="MYSQL") { ?>
		<div class="form-group"> 
			<label for="user_password" class="col-sm-3 control-label">Password*</label>
			<div class="col-sm-6"><input name="user_password" id="user_password" type="password" class="form-control" placeholder="Password" value="<? echo $user_password; ?>"></div>
		</div>		

		<div class="form-group"> 
			<label for="user_passwordbis" class="col-sm-3 control-label">Confirmer Password*</label>
			<div class="col-sm-6"><input name="user_passwordbis" id="user_passwordbis" type="password" class="form-control" placeholder="Confirmer Password" value="<? echo $user_passwordbis; ?>"></div>
		</div>	
		<? }
		else  $disable="readonly"; 
		?>

		<div class="form-group">
			<label for="user_lastname" class="col-sm-3 control-label">Nom*</label>
			<div class="col-sm-6"><input name="user_lastname" id="user_lastname" type="titre" class="form-control" <? echo $disable; ?> placeholder="Nom" value="<? echo $user_lastname; ?>"></div>
		</div>

		<div class="form-group">
			<label for="user_firstname" class="col-sm-3 control-label">Prénom*</label>
			<div class="col-sm-6"><input name="user_firstname" id="user_firstname" type="titre" class="form-control" <? echo $disable; ?> placeholder="Prénom" value="<? echo $user_firstname; ?>"></div>
		</div>

		<div class="form-group">
			<label for="user_email" class="col-sm-3 control-label">Email*</label>
			<div class="col-sm-6"><input name="user_email" id="user_email" type="email" class="form-control" <? echo $disable; ?> placeholder="Email" value="<? echo $user_email; ?>"></div>
		</div>
		
		<div class="form-group">
			<label for="user_pseudo" class="col-sm-3 control-label">Pseudo</label>
			<div class="col-sm-6"><input name="user_pseudo" id="user_pseudo" type="titre" class="form-control" placeholder="Pseudo" value="<? echo $user_pseudo; ?>"></div>
		</div>
	</fieldset>

<?

echo "</form>";

?>

<script type="text/JavaScript">
	$('#user_login').focus();
	
	<?php echo $jsaction ?>
	
</script>








	
	
