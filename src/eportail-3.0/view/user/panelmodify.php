<?
	if(!isset($_SESSION[$keymodal])) {
		header("location:index.php?view=diened.php");
		exit();
	}
	
	$id		=$_SESSION[$keymodal]['id1'];
	$tpmod	=$_SESSION[$keymodal]['modal-mode'];
	
	$fgedt=$_POST['fgedt'];
	$fgweb=$_POST['fgweb'];
	$fgwid=$_POST['fgwid'];
	$lbpan=$_POST['lbpan'];
	$lburl=$_POST['lburl'];
	$tpwid=$_POST['tpwid'];
	$vlmod=$_POST['vlmod'];

	
	// Permissions = si non utiisateur exist
	if($_SESSION['user_id']=="") die();
	
	// Mode Modification
	if($tpmod=="modpanel"){
		// Permissions = si non non propriéataire de la page exist
		$q="SELECT panel_user FROM ".$config["dbprefixe"]."panel WHERE panel_id=?";
		$query=$bdd01->prepare($q);
		$query->execute(array($id));
		if($row=$query->fetch()){	
			$idpro=$row['panel_user'];
		}
		if($idpro!=$_SESSION['user_id']&&$_SESSION['user_profil']!=1) {
			die();
		}
		
		// Modification
		if($vlmod!="") {
			// Gérer les erreurs
			if($lbpan==""&&$lburl=="") {
				$jsaction="alert('Vous devez renseigner l\'ensemble des informations');";
			}
			else {
				$q="UPDATE ".$config["dbprefixe"]."panel SET panel_label=?, panel_url=? WHERE panel_id=?";
				$query=$bdd01->prepare($q);
				$query->execute(array($lbpan,$lburl,$id));
				
				// Action javascript Ajout du panel directement + fermeture de la popup
				$jsaction="recharge(".$id.")";				
			}
		}
	}
	
	// Mode Création
	else {
		// Création d'une page Editeur
		if($fgedt!="") {
			// Gérer les erreurs
			if($lbpan=="") {
				$jsaction="alert('Vous devez renseigner l\'ensemble des informations');";
			}
			else {		
				// Recherche du dernier numéro d'ordre de panel de l'utilisateur 
				$order=1;
				$q="SELECT panel_order FROM ".$config["dbprefixe"]."panel WHERE panel_user=? ORDER BY panel_order DESC";
				$query=$bdd01->prepare($q);
				$query->execute(array($_SESSION['user_id']));
				if($row=$query->fetch()){	
					$order=$row['panel_order']+1;
				}
			
				// Création du panel
				$q="INSERT INTO ".$config["dbprefixe"]."panel (panel_label, panel_url, panel_user, panel_order, panel_type, panel_html) VALUES(?,?,?,?,?,?)";
				$query=$bdd01->prepare($q);
				$query->execute(array($lbpan,'view/user/editor.php',$_SESSION['user_id'],$order,3,'<center><br><br><br>Modifier votre page pour insérer votre texte</center>'));

				// Récupération du panel généré
				$idpan=$bdd01->lastInsertId();
				
				// Action javascript Ajout du panel directement + fermeture de la popup
				$jsaction="recharge(".$idpan.")";
			}
		}
		
		// Création d'une page URL
		if($fgweb!="") {
			// Gérer les erreurs
			if($lbpan==""||$lburl=="") {
				$jsaction="alert('Vous devez renseigner l\'ensemble des informations');";
			}
			else {
				// Recherche du dernier numéro d'ordre de panel de l'utilisateur 
				$order=1;
				$q="SELECT panel_order FROM ".$config["dbprefixe"]."panel WHERE panel_user=? ORDER BY panel_order DESC";
				$query=$bdd01->prepare($q);
				$query->execute(array($_SESSION['user_id']));
				if($row=$query->fetch()){	
					$order=$row['panel_order']+1;
				}
			
				// Création du panel
				$q="INSERT INTO ".$config["dbprefixe"]."panel (panel_label, panel_url, panel_user, panel_order, panel_type) VALUES(?,?,?,?,?)";
				$query=$bdd01->prepare($q);
				$query->execute(array($lbpan,$lburl,$_SESSION['user_id'],$order,1));
				
				// Récupération du panel généré
				$idpan=$bdd01->lastInsertId();
				
				// Action javascript Ajout du panel directement + fermeture de la popup
				$jsaction="recharge(".$idpan.")";
			}
		}
		
		// Création d'une page Widget
		if($fgwid!="") {
			// Gérer les erreurs
			if($lbpan=="") {
				$jsaction="alert('Vous devez renseigner l\'ensemble des informations');";
			}
			else {			
				// Recherche du dernier numéro d'ordre de panel de l'utilisateur 
				$order=1;
				$q="SELECT panel_order FROM ".$config["dbprefixe"]."panel WHERE panel_user=? ORDER BY panel_order DESC";
				$query=$bdd01->prepare($q);
				$query->execute(array($_SESSION['user_id']));
				if($row=$query->fetch()){	
					$order=$row['panel_order']+1;
				}
			
				// Création du panel
				$q="INSERT INTO ".$config["dbprefixe"]."panel (panel_label, panel_url, panel_user, panel_order, panel_type, panel_template) VALUES(?,?,?,?,?,?)";
				$query=$bdd01->prepare($q);
				$query->execute(array($lbpan,'view/user/widget.php',$_SESSION['user_id'],$order,4,1));
				
				// Récupération du panel généré
				$idpan=$bdd01->lastInsertId();
				
				// Action javascript Ajout du panel directement + fermeture de la popup
				$jsaction="recharge(".$idpan.")";
			}
		}
	}	
?>

<script type="text/JavaScript">
	function closemodal() {
		window.parent.closemodal();	
	}
	
	function recharge(idpan) {
		parent.parent.document.location.href='index.php?id='+idpan;
	}
	<?php echo $jsaction ?>
</script>

<form id='formulaire' class='form-horizontal' role='form' method='post' enctype='multipart/form-data'>
<input id='id' name='id' type='hidden' value='<? echo $id; ?>'>
<input id='tpmod' name='tpmod' type='hidden' value='<? echo $tpmod; ?>'>
	
<!-- MODIFICATION ------------------------------------------------------------------------------------------------------------------------------------>
<?

	if($tpmod=="modpanel"){
		$q="SELECT * FROM ".$config["dbprefixe"]."panel WHERE panel_id=?";
		$query=$bdd01->prepare($q);
		$query->execute(array($id));
		if($row=$query->fetch()){	
			if($lbpan=="") $lbpan=$row["panel_label"];
			if($lburl=="") $lburl=$row["panel_url"];
			$tpwid=$row["panel_type"];
		}

?>
<input id='tpwid' name='tpwid' type='hidden' value='<? echo $tpwid; ?>'>

<div class='form-group'>
<input id='vlmod' name='vlmod' class='btn btn-primary' type='submit' value='Valider' />
&nbsp;
<input class='btn btn-primary' type='submit' onClick='window.parent.ModalClose("#<? echo $_SESSION[$keymodal]["idmodal"] ?>");' value='Annuler' />
</div>


<div class="form-group">
	<label for="lbpan" class="col control-label">Nom*</label>
	<div class="col"><input name="lbpan" id="lbpan" type="titre" class="form-control" placeholder="Nom" value="<? echo $lbpan; ?>"></div>
</div>	
		
<div class="form-group" style="<? if($tpwid!=1) echo "display:none"?>">
	<label for="lbpan" class="col control-label">URL*</label>
	<div class="col"><input name="lburl" id="lburl" type="titre" class="form-control" placeholder="Url*" value="<? echo $lburl; ?>"></div>
</div>	

<!-- CREATION ---------------------------------------------------------------------------------------------------------------------------------------->
<? } else { ?>
<div class="form-group">
	<label for="lbpan" class="col control-label">Donnez un nom à votre nouvel onglet*</label>
	<div class="col"><input name="lbpan" id="lbpan" type="titre" class="form-control" placeholder="Nom" value="<? echo $lbpan; ?>"></div>
</div>

<div style='float:left; width:33%'>
<div class="thumbnail" style="margin-top:10px; border:none;">
<img class="imgreflect" alt="editeur" src="local/images/icon/00-editeur.jpg" style="border-radius:10px;" />
<div class="caption">
<h4 style="text-align:center">Page Editeur</h4>
<p style="text-align:center">
<input id='fgval' name='fgedt' class='btn btn-primary' type='submit' value='Choisir' />
</p>
</div>
</div>
</div>

<div style='float:left; width:33%'>
<div class="thumbnail" style="margin-top:10px; border:none;">
<img class="imgreflect" alt="url" src="local/images/icon/00-url.jpg" style="border-radius:10px;" />
<div class="caption">
<h4 style="text-align:center">Page Web</h4>
<p style="text-align:center">
<input id='fgweb' name='fgweb' class='btn btn-primary' type='submit' value='Choisir' />
<div class="form-group">
	<center><label for="lburl" class="col-xs-12 control-label">Saisir l'adresse du site*</label></center>
	<div class="col-xs-12"><input name="lburl" id="lburl" type="titre" class="form-control" placeholder="Url" value="<? echo $lburl; ?>"></div>
</div>
</p>
</div>
</div>
</div>

<div style='float:left; width:33%'>
<div class="thumbnail" style="margin-top:10px; border:none;">
<img class="imgreflect" alt="widget" src="local/images/icon/00-widget.jpg" style="border-radius:10px;" />
<div class="caption">
<h4 style="text-align:center">Page Widgets</h4>
<p style="text-align:center">
<input id='fgwid' name='fgwid' class='btn btn-primary' type='submit' value='Choisir' />
</p>
</div>
</div>
</div>



<? } ?>

<script>
	$(document).ready(function() {
		$('#lbpan').focus();
	});
	
	$('form').bind("keypress", function(e) {
	  if (e.keyCode == 13) {               
		e.preventDefault();
		return false;
	  }
	});	
</script>

</form>




