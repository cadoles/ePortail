<?
	//$id=$_GET['id'];
	
	echo "<script src='lib/jquery-ui/jquery-ui.js'></script>";
	
	// Récupérer le template de la page widget et le propriétaire de la page
	$q="SELECT panel_template, panel_user FROM ".$config["dbprefixe"]."panel WHERE panel_id=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
	if($row=$query->fetch()){	
		$idtpl=$row['panel_template'];
		$idpro=$row['panel_user'];
	}
?>


<script src="lib/iframeAuto/jquery.iframe-auto-height.js"></script>
<script src="lib/iframeAuto/jquery.browser.js"></script>

<script type="text/javascript">  

	function AjustFrame() {
		$('.frameajust').iframeAutoHeight({
			minHeight: 100, // Sets the iframe height to this value if the calculated value is less
			heightOffset: 0 // Optionally add some buffer to the bottom
		});
	}
	
	$(window).resize(function() {
		AjustFrame();
	});  
		
	$(document).ready(function () {  
		AjustFrame();
	});  
	
	<? if($idpro==$_SESSION['user_id']||$_SESSION['user_profil']==1) { ?>
	$(document).ready(function () {  
		$(".column").sortable({
			tolerance: 'pointer',
			revert: 'invalid',
			connectWith: ".column",  
			handle: '.WidgetHeader', 
			opacity: 0.6,
			placeholder: 'widget placeholder',
			forceHelperSize: true,
			
			update: function ()
			{
				var a = $(this);

				$('.column').each(function(index) {
					var order = $(this).sortable('serialize');
					var key=randomstring(20);
					$("#info").load("process.php?process=hash.php&key="+key);
					$("#info").load("process.php?process=process-userwidget-order.php&key="+key+"&"+order+"&idloc="+$(this).attr('id')+"&id=<? echo $id; ?>");
					
				});
			} 			
		});			
    
		$(".column").disableSelection();					
	});  
	
	<? } ?>
	
	function delwidget(idwid) {
		if(confirm("Confirmez-vous la suppression de ce widget ?")) {
			var key=randomstring(20);
			$("#info").load("process.php?process=hash.php&key="+key);
			$("#info").load("process.php?process=process-userwidget-del.php&key="+key+"&id="+idwid);
			setTimeout(function () {location.reload()},1000);			
		}
	}	
</script>

</head>

<?
	// Si l'utilisateur n'est pas le propriétaire / qu'il n'est pas admin / que n'a pas la page dans son profil = OUT
	$fgperm=false;
	foreach ($tbpage as $page) {
		if($page[1]==$id) $fgperm=true;
	}	
	
	if($idpro!=$_SESSION['user_id']&&$_SESSION['user_profil']!=1&&!$fgperm) {
		include("view/diened.php");
		include("footer.php");
		die();			
	}

	// Si l'utilisateur en cours est le propriétaire : il peut modifier la page
	if($idpro==$_SESSION['user_id']||$_SESSION['user_profil']==1) {
		echo "<div id='widgetadmin'>";
		echo "<a style='cursor: pointer;' data-toggle='modal' data-target='#mymodal-01' title='Modifier la forme' onClick='ModalLoad(\"mymodal-01\",\"widget-template\",$id,0)'>Modifier la forme</a>";
		echo "&nbsp-&nbsp";
		echo "<a style='cursor: pointer;' data-toggle='modal' data-target='#mymodal-01' title='Ajouter un widget' onClick='ModalLoad(\"mymodal-01\",\"widget-insert\",$id,0)'>Ajouter un Widget</a>";
		echo "</div>";
	}	
?>

<div id="info"></div>

<?
	// Placer une iframe conteneur 
	echo "<iframe id='itemframe' class='myframe' src='' style='height:100%; width:100%; display:none;' frameborder='0' ></iframe>";	

	// Récupérer le code html du template
	$q="SELECT * FROM ".$config["dbprefixe"]."panel_template WHERE panel_template_id=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($idtpl));
	if($row=$query->fetch()){	
		echo $row['panel_template_html'];
	}
	
	
	// Parcourrir l'ensemble des widget de la page pour les créer dynamiquement
	$q="SELECT * FROM ".$config["dbprefixe"]."panel_widget, ".$config["dbprefixe"]."widget WHERE panel_widget_panel=? AND panel_widget_widget=widget_id ORDER BY panel_widget_order DESC";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
	while($row=$query->fetch()){	
		// Définition de la source de l'url : soit celle de l'utilisateur soit celle du widget si type de widget systeme l'url est forcement déterminé par l'utilisateur
		if($row['widget_url'] != "")
			$url=$row['widget_url'];
		else
			$url=$row['panel_widget_url'];
		
		$adjust="";
		if($row['panel_widget_adjust']==1) $adjust="frameajust";
		
		// Définition de l'url en fonction du type de contenu du widget si type rss on force la page rss.php en http
		if(stripos(" ".$url,"view/")==1) {
			$panel_widget_url="widget.php?view=".$url."&id=".$row['panel_widget_id']."&mode=widget";
		}
		elseif($row['widget_type']==2) {
			$panel_widget_url="widget.php?view=view/user/rss.php&id=".$row['panel_widget_id']."&mode=widget";
		}
		else{
			$panel_widget_url=$url;
		}
		
			
		// Div conteneur global
		$iframe="<div id='widget_".$row['panel_widget_id']."' class='WidgetClass".$row['panel_widget_style']."'>";
		
		// Couleur du Header
		$color="";
		if($row['panel_widget_style']!="3"&&$row['panel_widget_color']!="") {
			$color="background-color: #".$row['panel_widget_color'].";";
		}
		// Div titre
		$iframe=$iframe."<div class='WidgetHeader' style='$color'><a data-toggle='collapse' href='#widgetbody_".$row['panel_widget_id']."'>".$row['panel_widget_label']."</a>";

		// Si propriétaire de la page ou administrateur bouton modifier / supprimer
		if($idpro==$_SESSION['user_id']||$_SESSION['user_profil']==1) {							
			$iframe=$iframe."
				<div class='WidgetHeaderMenu'>
				<a class='mybtn fa-file fa'   onClick='ModalLoad(\"mymodal-01\",\"widget-update\",$id,".$row['panel_widget_id'].")'  data-toggle='modal' data-target='#mymodal-01' title='Modifier'></a>
				&nbsp;
				<a class='mybtn fa-trash fa' onClick='delwidget(".$row['panel_widget_id'].");' title='Supprimer'></a>
				</div>";
		}	 

		// Fermeture de la Div titre
		$iframe=$iframe."</div>";
		
		// Déterminer la hauteur
		$hauteur="";
		if($adjust=="")	$hauteur="height:".($row['panel_widget_height']-30)."px";
		
		
		// Div corps
		$iframe=$iframe."<div id='widgetbody_".$row['panel_widget_id']."' class='WidgetBody collapse in' style='".$hauteur."'>";
		
		// Mode iframe
		if($row['widget_mode']==1) {;
			$iframe=$iframe."
				<iframe
					id='frame_".$row['panel_widget_id']."'
					class='$adjust'
					src='$panel_widget_url'
					width=100%
					frameborder=0
					scrolling=auto
					style='$hauteur'></iframe>";
		}
		
		// Fermeture de la Div corps
		echo "</div>";
			
			
		// Fermeture de la Div global
		$iframe=$iframe."</div></div>";
		
		// Cas hauteur nulle
		//if($db1->f('panel_widget_height')=="") $iframe=str_replace("height=''","",$iframe);
		
		// Suppression des retour chariot pour bonne interprétation en javascript
		$iframe=str_replace("\n","",$iframe);
	?>
		<script type="text/javascript">  
			// Recherche de la colonne où le widget est localisé
			loc = $('#<? echo $row['panel_widget_loc']; ?>');
			
			
			if(loc.length==0) {
				loc=$('#C1R1');
			}
			
			// Ajout du widget
			charloc="<? echo addslashes($iframe); ?>";
			
			//$(".grid").prepend(charloc);
			loc.prepend(charloc);
		</script>
		
	<?
	}
	
	
?>

<!-- SCRIPT --------------------------------------------------------------------------------------------------------------------------------------- -->  	
<script type="text/javascript">
	function changeFrame(tpmod,idpwd) {
		if(tpmod=="modtemplate") {
			srcframe="widgettemplate.php?idpan=<? echo $id; ?>";
		}
		else {
			srcframe="widgetmodify.php?idpan=<? echo $id; ?>&mode="+tpmod+"&idpwd="+idpwd;
		}
		$("#framemodal").attr("src",srcframe);
	}

	function configWidget(idpan,idwid,tpwid) {
		ModalLoad("mymodal-01","widget-config",idpan,idwid);
		$('#mymodal-01').modal('show');
	}

		
	function recharge() {
		location.reload();
	}
	
	function closemodal() {
		$('#mymodal').modal('hide');
	}

	function changeItem(srcframe) {
		$("#itemframe").attr("src",srcframe);
		$("#itemframe").css("display","block");
		$("#widgetadmin").css("display","none");
		$(".column").css("display","none");
	}
	
	<?php echo $jsaction ?>
	
</script>

