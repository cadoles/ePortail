<?
	if($config["modeRegistration"]=='true'&&$config["modeRegistrationExterne"]==""&&($config["activerLDAP"]=='true'||$config["modeAuthentification"]=='MYSQL'))
		$levelpage=99;
	else
		$levelpage=-1;
		
	include("include/permission.php");
	
	echo "<link rel='stylesheet' href='lib/realperson/jquery.realperson.css'>";
	echo "<script src='lib/realperson/jquery.plugin.js'></script>";
	echo "<script src='lib/realperson/jquery.realperson.js'></script>";

	$vlmod				=$_POST['vlmod'];
	
	$inscription_login			=$_POST['inscription_login'];
	$inscription_password		=$_POST['inscription_password'];
	$inscription_passwordbis	=$_POST['inscription_passwordbis'];
	$inscription_lastname		=$_POST['inscription_lastname'];
	$inscription_firstname		=$_POST['inscription_firstname'];
	$inscription_email			=$_POST['inscription_email'];

	function rpHash($value) { 
		$hash = 5381; 
		$value = strtoupper($value); 
		for($i = 0; $i < strlen($value); $i++) { 
			$hash = (($hash << 5) + $hash) + ord(substr($value, $i)); 
		} 
		return $hash; 
	} 
 
	/*--> Controle de cohérance */
	if($vlmod!="") { 
		
		$fgerr="";
		
		if(rpHash($_POST['realPerson'].salt) == $_POST['realPersonHash']) {
			$fgerr="1";
		}
		
		if($inscription_password!=$inscription_passwordbis) {
			$jsaction="alert('Votre mot de passe n\'est pas confirmé');";
			$fgerr=1;
		}

		if($inscription_lastname==""||$inscription_firstname==""||$inscription_email=="") {
			$jsaction="alert('Vous devez renseigner l\'ensemble des informations');";
			$fgerr=1;
		}
		

		if($fgerr!=1) {
			$q="SELECT * FROM ".$config["dbprefixe"]."user WHERE (user_login=? OR user_email=?)";
			$tbq=array($inscription_login,$inscription_email);

			$query=$bdd01->prepare($q);
			$query->execute($tbq);
			if($row=$query->fetch()) {
				$jsaction="alert('Un utilisateur avec cet email ou ce login existe déjà');";
				$fgerr=1;
			}
			$query->closeCursor();
		}
	}
	
	/*--> Rester sur le mode encours */
	if($vlmod!=""&&fgerr!="") $tpmod="";
		
	/*--> Modify */
	if($vlmod!=""&&$fgerr=="") {
		$q="INSERT INTO ".$config["dbprefixe"]."inscription (inscription_login, inscription_password, inscription_lastname, inscription_firstname, inscription_pseudo, inscription_email, inscription_token) VALUE (?,?,?,?,?,?,?)";
		$inscription_token="";
		if($config["modeRegistrationConfirm"]=="direct")
			$inscription_token=uniqid(rand(),true);
		$tbq=array($inscription_login,md5($inscription_password),$inscription_lastname,$inscription_firstname,"",$inscription_email,$inscription_token);
		$query=$bdd01->prepare($q);
		$query->execute($tbq);


		// Si inscription direct par email on envoit l'email
		if($config["modeRegistrationConfirm"]=="direct") {
			$to			= $inscription_email;
			$subject	= "Inscription au site = ".$config["title"];
			$headers   	= array();
			$headers[] 	= "MIME-Version: 1.0";
			$headers[] 	= "Content-type: text/html; charset=UTF-8";
			$headers[] 	= "From: Sender Name <sender@domain.com>";
			$headers[] 	= "Reply-To: no-replay";
			$headers[] 	= "Subject: Inscription au site = ".$config["title"];
			$headers[] 	= "X-Mailer: PHP/".phpversion();			
			$message 	= "Bonjour,<br>
						   Merci de valider votre inscription à notre site <strong>".$config["title"]."</strong>, en suivant le lien suivant<br>
						   <a href='".$config["urlbase"]."/index.php?view=user/confirm.php&login=".$inscription_login."&token=".$inscription_token."'>Validez votre inscription</a>";
			
			$retour=mail($to, $subject, $message, implode("\r\n", $headers));
		}
		
		$tpmod="MESSAGE";
	}


if($tpmod=="") {    
    echo "<form class='form-horizontal' role='form' method='post' enctype='multipart/form-data'>";

	echo "<input id='tpmod' name='tpmod' type='hidden' value=''>";
	
  
	// Entete du formulaire
    echo "<legend><h1>INSCRIPTION</h1></legend>";
	echo "<div class='form-group'>";
	echo "<div class='col-sm-12'>";
	echo "<input id='vlmod' name='vlmod' type='submit' class='btn btn-primary' value='Enregistrer'>";
	echo "</div>";
	echo "</div>";
	?>

	<fieldset id="encadrer" style="clear:both">
		<div class="form-group">
			<label for="inscription_login" class="col-sm-3 control-label">Login*</label>
			<div class="col-sm-6"><input name="inscription_login" id="inscription_login" type="titre" class="form-control" placeholder="Login" value="<? echo $inscription_login; ?>"></div>
		</div>

		<div class="form-group"> 
			<label for="inscription_password" class="col-sm-3 control-label">Password*</label>
			<div class="col-sm-6"><input name="inscription_password" id="inscription_password" type="password" class="form-control" placeholder="Password" value="<? echo $inscription_password; ?>"></div>
		</div>		

		<div class="form-group"> 
			<label for="inscription_passwordbis" class="col-sm-3 control-label">Confirmer Password*</label>
			<div class="col-sm-6"><input name="inscription_passwordbis" id="inscription_passwordbis" type="password" class="form-control" placeholder="Confirmer Password" value="<? echo $inscription_passwordbis; ?>"></div>
		</div>	

		<div class="form-group">
			<label for="inscription_lastname" class="col-sm-3 control-label">Nom*</label>
			<div class="col-sm-6"><input name="inscription_lastname" id="inscription_lastname" type="titre" class="form-control" placeholder="Nom" value="<? echo $inscription_lastname; ?>"></div>
		</div>

		<div class="form-group">
			<label for="inscription_firstname" class="col-sm-3 control-label">Prénom*</label>
			<div class="col-sm-6"><input name="inscription_firstname" id="inscription_firstname" type="titre" class="form-control"  placeholder="Prénom" value="<? echo $inscription_firstname; ?>"></div>
		</div>

		<div class="form-group">
			<label for="inscription_email" class="col-sm-3 control-label">Email*</label>
			<div class="col-sm-6"><input name="inscription_email" id="inscription_email" type="email" class="form-control" placeholder="Email" value="<? echo $inscription_email; ?>"></div>
		</div>
		
		<div class="form-group">
			<label for="inscription_email" class="col-sm-3 control-label">Confirmez ce code*</label>
			<div class="col-sm-6"><input type="text" id="defaultReal" type="titre" class="form-control"  name="defaultReal"></div>
	</fieldset>

<?

echo "</form>";
}
elseif($tpmod=="MESSAGE") {
	echo "<div class='alert alert-success' role='alert' style='margin: 20px auto 0px auto; max-width: 800px;'>";
	if($config["modeRegistrationConfirm"]=="direct") {
		echo "Vous allez recevoir par email un message de confirmation que vous devrez suivre pour finaliser votre inscription";
	}
	else {
		echo "Nous avons prise en compte votre demande d'inscription. Vous recevrez un email de confirmation quand nous aurons validé votre profil.";
	}
	echo "</div>";
}
?>

<script type="text/JavaScript">
	$(document).ready(function() {
		$('#defaultReal').realperson();
		$('#inscription_login').focus();
		
		<?php echo $jsaction ?>
	} );		
</script>








	
	
