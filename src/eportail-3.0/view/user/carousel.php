<?
	$mode=$_GET['mode'];

	if($mode=="widget") {
		$id=$_GET['id'];
	
		$q="SELECT * FROM ".$config["dbprefixe"]."panel,".$config["dbprefixe"]."panel_widget WHERE panel_id=panel_widget_panel AND panel_widget_id=?";
		$query=$bdd01->prepare($q);
		$query->execute(array($id));
		if($row=$query->fetch()){
			$idpanel	=$row['panel_widget_panel'];
			$idpro		=$row['panel_user'];
			$html		=$row['panel_widget_html'];
		}
		
		// Si l'utilisateur n'est pas le propriétaire / qu'il n'est pas admin / que n'a pas la page dans son profil = OUT
		$fgperm=false;
		foreach ($tbpage as $page) {
			if($page[1]==$idpanel) $fgperm=true;
		}	
		if($idpro!=$_SESSION['user_id']&&$_SESSION['user_profil']!=1&&!$fgperm) {
			include("view/diened.php");
			include("footer.php");
			die();
		}		
	}
?>
	
<style>

#widgetadmin {
	position:absolute;
	top:0px;
	right:10px;
	font-size: 0.8em;
	text-align: right;
	float:right;
	z-index: 1000;
}

#widgetadmin a {color: #fff; }

body {
	background-color: #333;
}
</style>
	
<?
	// Si l'utilisateur en cours est le propriétaire ou administrateur : il peut modifier la page
	if($idpro==$_SESSION['user_id']||$_SESSION['user_profil']==1) {
		echo "<div id='widgetadmin'>";
		echo "<a id='configWidget' title='Configurer' onclick='parent.configWidget($idpanel,$id,\"carousel\");' style='cursor:pointer;'>Configurer</a>";
		echo "</div>";
	}
?>


<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

	<ol class="carousel-indicators">
		<?
			$lbclass="class='active'";
			$i=0;
			$q="SELECT * FROM ".$config["dbprefixe"]."panel_widget_carousel WHERE panel_widget_carousel_widget=? ORDER BY panel_widget_carousel_order";
			$query=$bdd01->prepare($q);
			$query->execute(array($id));
			while($row=$query->fetch()){
				echo "<li data-target='#carousel-example-generic' data-slide-to='$i' $lbclass></li>";
				$lbclass="";
				$i=$i+1;
			}
		?>
	</ol>

	<div class="carousel-inner">
		<?
			$lbclass="active";
			$i=0;
			$q="SELECT * FROM ".$config["dbprefixe"]."panel_widget_carousel WHERE panel_widget_carousel_widget=? ORDER BY panel_widget_carousel_order";
			$query=$bdd01->prepare($q);
			$query->execute(array($id));
			while($row=$query->fetch()){
				echo "<div class='item $lbclass' style='background: url(local/images/carousel/".$row["panel_widget_carousel_img"].") no-repeat center fixed; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;'>";
				if($row['panel_widget_carousel_url']!="") echo "<a href='".$row['panel_widget_carousel_url']."' target='_blank'>";
				echo "<img src='style/images/blank.gif' style='height:100%; width:100%'>";
				if($row['panel_widget_carousel_url']!="") echo "</a>";
				echo "<div class='carousel-caption'>";
				echo $row["panel_widget_carousel_html"];
				echo "</div>";
				echo "</div>";
				
				$lbclass="";
				$i=$i+1;
			}
		?>
	</div>

	<!-- Controls -->
	<? if($i>0) { ?>
	<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
	<span class="glyphicon glyphicon-chevron-left"></span>
	</a>

	<a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
	<span class="glyphicon glyphicon-chevron-right"></span>
	</a>
	<? } ?>
</div>
	
<div id="info"></div>
