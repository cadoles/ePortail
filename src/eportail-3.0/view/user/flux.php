<script src="lib/zrssfeed/jquery.zrssfeed.js" type="text/javascript"></script>

<?
	$mode=$_GET['mode'];

	if($mode=="widget") {
		$id=$_GET['id'];
	}

	// Recherche du mode d'affichage du widget
	$q="SELECT * FROM ".$config["dbprefixe"]."panel_widget WHERE panel_widget_id=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
	if($row=$query->fetch()){
		// Les styles hors "sans bordure" doivent avoir une marge
		if($row["panel_widget_style"]!=2) {
			echo "<style>body{margin:0px 5px 0px 5px;}</style>";
		}
	}
	
	// Affichage des catégorie
	$div=array();
	echo "<ul id='navitem' class='nav'>";
	
	
	foreach ($tbflux as $flux) {
		if($firstflux=="") $firstflux=$flux[1];

		if($flux[6]!="") {
			echo "<style>";
			echo "#rss-".$flux[1]." {";
			echo "background-color:#".$flux[6]." !important;";
			echo "border-bottom: 5px solid #".$flux[6]." !important;";
			echo "}";
			
			echo "#rss-".$flux[1]." a:hover {";
			echo "background-color:#".$flux[6]." !important;";
			echo "}";
			
			echo "</style>";
		}
		
		if($flux[3]==0) {
			echo "<li id='rss-".$flux[1]."'><a onClick='loadRSS(".$flux[1].",\"".$flux[4]."\",".$flux[5].",".$flux[7].");'>".$flux[2]."</a></li>";
		}
		elseif($flux[3]==1) {
			echo "<li id='rss-".$flux[1]."'><a onClick='loadArticle(".$flux[1].")'>".$flux[2]."</a></li>";
		}
		elseif($flux[3]==2) {
			echo "<li id='rss-".$flux[1]."'><a onClick='loadURL(".$flux[1].",\"".$flux[4]."\");'>".$flux[2]."</a></li>";
		}		
	}	
	echo "</ul>";
	
	echo "<div id='divRss'></div>";
	
	$fctload="$('#rss-".$firstflux." a').click();";
?>
<input value="0" id="widgetload" name="widgetload" type="hidden"/>

<!-- SCRIPT --------------------------------------------------------------------------------------------------------------------------------------- -->  
<script>
	function loadArticle(id) {
		$("#navitem li").removeClass("active");
		$("#rss-"+id).addClass("active");
		
		key=randomstring(20);
		$("#divRss").load("process.php?process=hash.php&key="+key);
		$("#divRss").load("process.php?process=process-flux.php&key="+key+"&id="+id);
		
		console.log(document.parent.$("#frame_<? echo $id ?>").attr("src"));
	}

	function loadURL(id,url) {
		$("#navitem li").removeClass("active");
		$("#rss-"+id).addClass("active");

		$('#divRss').load(url);
	}

	function loadRSS(id,url,maxrss,style) {
		$("#navitem li").removeClass("active");
		$("#rss-"+id).addClass("active");
		
		if(style==1)
			colonne=1;
		else
			colonne=maxrss;
			
		$('#divRss').rssfeed(url, {
				limit: maxrss,
				linktarget: '_blank',
				ssl: true,
				header: false,
				dateformat:	'dd MMMM yyyy',
				linkcontent: true,
				snippet: true,
				colonne: colonne,
		});
	}		

	$(document).on('click', '#divRss a', function() {
		window.open($(this).attr('href'));
		console.log("GRR LOAD = "+$("#widgetload").val());
		return false;
	});	 
			
	<? echo $fctload; ?>
	
</script>

