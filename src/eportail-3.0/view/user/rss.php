<script src="lib/zrssfeed/jquery.zrssfeed.js" type="text/javascript"></script>

<?
	$mode=$_GET['mode'];

	if($mode=="widget") {
		$id=$_GET['id'];
	
		$q="SELECT * FROM ".$config["dbprefixe"]."panel,".$config["dbprefixe"]."panel_widget WHERE panel_id=panel_widget_panel AND panel_widget_id=?";
		$query=$bdd01->prepare($q);
		$query->execute(array($id));
		if($row=$query->fetch()){
			$idpanel	=$row['panel_widget_panel'];
			$idpro		=$row['panel_user'];
			$html		=$row['panel_widget_html'];
		}
		
		// Si l'utilisateur n'est pas le propriétaire / qu'il n'est pas admin / que n'a pas la page dans son profil = OUT
		$fgperm=false;
		foreach ($tbpage as $page) {
			if($page[1]==$idpanel) $fgperm=true;
		}	
		if($idpro!=$_SESSION['user_id']&&$_SESSION['user_profil']!=1&&!$fgperm) {
			include("view/diened.php");
			include("footer.php");
			die();
		}		
	}
	
	$q="SELECT * FROM ".$config["dbprefixe"]."panel_widget WHERE panel_widget_id=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
	if($row=$query->fetch()){		
		echo "<div id='divRss'></div>";
		$fctload="loadRSS('".$row["panel_widget_url"]."',5);";
	}
?>

<!-- SCRIPT --------------------------------------------------------------------------------------------------------------------------------------- -->  
<script>
	function loadRSS(url,maxrss) {
				
		$('#divRss').rssfeed(url, {
				limit: maxrss,
				linktarget: '_blank',
				ssl: true,
				header: false,
				dateformat:	'dd MMMM yyyy',
				linkcontent: true,
				snippet: true,
			});						
	}		

	$(document).on('click', '#divRss a', function() {
		window.open($(this).attr('href'));
		return false;
	});	 
			
	<? echo $fctload; ?>
	
</script>

