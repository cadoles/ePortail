<?
	$idfluxmsg=$_GET['idflux'];

	// Recherche de l'article
	$q="SELECT * FROM ".$config["dbprefixe"]."fluxmsg WHERE fluxmsg_id=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($idfluxmsg));
	if($row=$query->fetch()){	
		//$idpro	=$row['panel_user'];
		$idflux	=$row['fluxmsg_flux'];
		$titre	=$row['fluxmsg_name'];
		$date	= date("d/m/Y",strtotime($row['fluxmsg_date']));
		$html	=$row['fluxmsg_html'];
		
	}
	
	// On recherche si l'article fait partie d'un flux autorisé pour l'utilisateur
	$fgperm=false;
	foreach ($tbflux as $flux) {
		if($flux[1]==$idflux) $fgperm=true;
	}
	
	if(!$fgperm&&$_SESSION['user_profil']!=1) {
		include("view/diened.php");
		include("footer.php");
		die();	
	}
	
?>
	<!-- This div will be used to display the editor contents. -->
	<div id="editorcontents">
		<?
			echo "<h1 class='page-header'>$titre</h1>";

			echo $html;
		?>
	</div>
	
