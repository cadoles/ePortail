<?
	$mode=$_GET['mode'];

	if($mode=="widget") {
		$id=$_GET['id'];
		
		// Recherche du mode d'affichage du widget
		$q="SELECT * FROM ".$config["dbprefixe"]."panel_widget WHERE panel_widget_id=?";
		$query=$bdd01->prepare($q);
		$query->execute(array($id));
		if($row=$query->fetch()){
			// Les styles "sans bordure" doivent n'ont pas de marge
			if($row["panel_widget_style"]==2) {
				echo "<style>#editorcontents{padding:10px 0px 0px 0px;}</style>";
			}
		}		
	}
?>
	<script src="lib/ckeditor/ckeditor.js"></script>	

	<script type="text/javascript">
		var editor, html = '';

		function createEditor()
		{
			var heightbody = $('html').height();
			
			CKEDITOR.config.toolbar_Basic =
			[
				{ name: 'document', items : ['Bold','Italic','Underline','-','JustifyLeft','JustifyCenter','JustifyRight','-', 'NumberedList','BulletedList' ] },
				{ name: 'insert', items : ['Image','Table','Smiley','Link'] },
				{ name: 'styles', items : [ 'Styles','Format','Font','FontSize'] }
			];
			
			
			CKEDITOR.config.toolbar_Full =
			[
				{ name: 'document', items : [ 'Source','-','Save','NewPage','DocProps','Preview','Print','-','Templates' ] },
				{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
				{ name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },

				{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
				{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv',
				'-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
				{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },
				{ name: 'insert', items : [ 'Image','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ] },

				{ name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
				{ name: 'colors', items : [ 'TextColor','BGColor' ] },
				{ name: 'tools', items : [ 'Maximize', 'ShowBlocks','-','About' ] }
			];			
			

			// Valeur par défaut
			directory="local/upload";
			typeupload="image";
			multiupload=false;
			resize=true;
			size="400px";
			rename=true;
			prefix="file_";
			
			// Definition de l'url action
			action = "/<? echo $config["alias"]?>/lib/ckeditor/upload.php?directory="+directory+"&multiupload="+multiupload+"&resize="+resize+"&size="+size+"&rename="+rename+"&prefix="+prefix;

			
			CKEDITOR.config.filebrowserUploadUrl = action+"&typeupload=all";
			CKEDITOR.config.filebrowserImageUploadUrl = action+"&typeupload=image";

			<?
				if($mode=="widget") {
					echo "CKEDITOR.config.toolbar = 'Basic';";
					echo "CKEDITOR.config.height = heightbody-120;";
				}
				else {
					echo "CKEDITOR.config.toolbar = 'Full';";
					echo "CKEDITOR.config.height = heightbody-120;";
				}
			?>
			
			if (editor)
				return;

			// Create a new editor inside the <div id="editor">, setting its value to html
			var config = {};
			editor = CKEDITOR.appendTo( 'editor', config, html );
			
			$('#createEditor').css( "display","none" );
			$('#removeEditor').css( "display","inline" );
			
			$('#editorcontents').css( "display","none" );
			
			setTimeout(function () {resizeEditor("<? echo $mode ?>")},1000);
		}

		function removeEditor()
		{
			if ( !editor )
				return;

			document.getElementById( 'editorcontents' ).innerHTML = html = editor.getData();
			label=encodeURIComponent(editor.getData());
			
			key=randomstring(20);
			$("#info").load("process.php?process=hash.php&key="+key);
			$("#info").load("process.php?process=process-usereditor-update.php&key="+key+"&mode=<?echo $mode;?>&id=<? echo $id; ?>&html="+label);

			// Destroy the editor.
			editor.destroy();
			editor = null;
			
			$('#createEditor').css( "display","initial" );
			$('#removeEditor').css( "display","none" );	
			$('#editorcontents').css( "display","block" );
			
		}
	</script>

<?
	// Récupérer le template de la page widget et le propriétaire de la page
	if($mode=="") {
		$q="SELECT panel_user, panel_html FROM ".$config["dbprefixe"]."panel WHERE panel_id=?";
		$query=$bdd01->prepare($q);
		$query->execute(array($id));
		if($row=$query->fetch()){	
			$idpro	=$row['panel_user'];
			$html	=$row['panel_html'];
		}

		// Si l'utilisateur n'est pas le propriétaire / qu'il n'est pas admin / que n'a pas la page dans son profil = OUT
		$fgperm=false;
		foreach ($tbpage as $page) {
			if($page[1]==$id) $fgperm=true;
		}
		if($idpro!=$_SESSION['user_id']&&$_SESSION['user_profil']!=1&&!$fgperm) {
			include("view/diened.php");
			include("footer.php");
			die();			
		}

	}
	elseif($mode=="widget") {
		$q="SELECT * FROM ".$config["dbprefixe"]."panel,".$config["dbprefixe"]."panel_widget WHERE panel_id=panel_widget_panel AND panel_widget_id=?";
		$query=$bdd01->prepare($q);
		$query->execute(array($id));
		if($row=$query->fetch()){
			$idpanel	=$row['panel_widget_panel'];
			$idpro		=$row['panel_user'];
			$html		=$row['panel_widget_html'];
		}
		
		// Si l'utilisateur n'est pas le propriétaire / qu'il n'est pas admin / que n'a pas la page dans son profil = OUT
		$fgperm=false;
		foreach ($tbpage as $page) {
			if($page[1]==$idpanel) $fgperm=true;
		}	
		if($idpro!=$_SESSION['user_id']&&$_SESSION['user_profil']!=1&&!$fgperm) {
			include("view/diened.php");
			include("footer.php");
			die();
		}		
	}
	
	// Si l'utilisateur en cours est le propriétaire ou administrateur : il peut modifier la page
	if($idpro==$_SESSION['user_id']||$_SESSION['user_profil']==1) {
		echo "<div id='widgetadmin'>";
		echo "<a id='createEditor' title='Modifier la Page' onclick='createEditor();' style='cursor:pointer;'>Modifier la Page</a>";
		echo "<a id='removeEditor' title='Enregister les Modifications' onclick='removeEditor();' style='display:none; cursor:pointer;'>Enregister les Modifications</a>";
		echo "</div>";
	}
?>

	<!-- This div will hold the editor. -->
	<div id="editor">
	</div>
	
	<!-- This div will be used to display the editor contents. -->
	<div id="editorcontents">
		<? echo $html; ?>
		<script>
			html = '<? echo str_replace(chr(10),'',$html); ?>';
		</script>
	</div>
	
	<div id="info"></div>

<?
	if($mode=="widget") {
		include("footer.php");
	}
?>

<script>
function resizeEditor(mode) {
	if($("#cke_editor1").length>0) {
		var heightbody 			= $('html').height();
		
		if($('#mynavbar').length>0)
			var heightnavbar 		= $('#mynavbar').height();
		else
			var heightnavbar 		= -15;
			
		var heightwidgetadmin	= $('#widgetadmin').height();
		var heighteditor		= $('#cke_1_top').height()+10;
		var heightbottom		= $('#cke_1_bottom').height()+10;
		
		var heightframe = heightbody-heightnavbar-heightwidgetadmin-heighteditor-heightbottom-15;
		$("#cke_1_contents").height(heightframe);
	}
}
	
$('document').ready(function(){
	resizeEditor();
});

$(window).resize(function() {
	resizeEditor();
});
</script>
