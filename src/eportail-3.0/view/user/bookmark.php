<?
	$tpmod				=$_POST['tpmod'];
	$vlmod				=$_POST['vlmod'];
	$vladd				=$_POST['vladd'];
	$vlsup				=$_POST['vlsup'];
	
	$id					=$_POST['id'];
	$name				=$_POST['name'];
	$url				=$_POST['url'];
	$open				=$_POST['open'];
	$color				=$_POST['color'];
	$icon				=$_POST['icon'];
	$idwid				=$_POST['idwid'];
	$mode				=$_POST['mode'];
	
	if($idwid=="")		$idwid=$_GET['id'];
	if($mode=="")		$mode=$_GET['mode'];
	
	// Récupérer le panel du widget
	$height="250";
	$q="SELECT * FROM ".$config["dbprefixe"]."panel_widget WHERE panel_widget_id=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($idwid));
	if($row=$query->fetch()){	
		$idpan=$row['panel_widget_panel'];
		$height=$row['panel_widget_height'];

		if($row["panel_widget_style"]!=2) {
			echo "<style>body{margin:0px 5px 0px 5px;}</style>";
		}
	}

	// Récupérer le template de la page widget et le propriétaire de la page
	$q="SELECT panel_user, panel_html FROM ".$config["dbprefixe"]."panel WHERE panel_id=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($idpan));
	if($row=$query->fetch()){	
		$idpro=$row['panel_user'];
		$html=$row['panel_html'];
	}

	// Si l'utilisateur en cours est le propriétaire ou administrateur : il peut modifier la page
	$fgproprio=false;
	if($idpro==$_SESSION['user_id']||$_SESSION['user_profil']==1) {
		$fgproprio=true;
	}

	// Si l'utilisateur n'est pas le propriétaire / qu'il n'est pas admin / que n'a pas la page dans son profil = OUT
	$fgperm=false;
	foreach ($tbpage as $page) {
		if($page[1]==$idpan) $fgperm=true;
	}	
	if($idpro!=$_SESSION['user_id']&&$_SESSION['user_profil']!=1&&!$fgperm) {
		include("view/diened.php");
		include("footer.php");
		die();
	}	
	
	/*--> Controle de cohérance */
	if($vladd!=""||$vlmod!="") {
		$fgerr="";

		if($name==""||$url==""||$icon==""||$open=="") {
			$jsaction="alert('Vous devez renseigner l\'ensemble des informations');";
			$fgerr=1;
		}
	}
	
	/*--> Rester sur le mode encours */
	if($vlmod!=""&&$fgerr!="") $tpmod="MODIFY";
	if($vladd!=""&&$fgerr!="") $tpmod="SUBMIT";
		
	/*--> Submit */
	if($vladd!=""&&$fgerr=="") {
		$q="INSERT INTO ".$config["dbprefixe"]."panel_widget_bookmark(panel_widget_bookmark_name, panel_widget_bookmark_url, panel_widget_bookmark_open, panel_widget_bookmark_color, panel_widget_bookmark_icon, panel_widget_bookmark_user, panel_widget_bookmark_widget) VALUES(?,?,?,?,?,?,?)";
		$query=$bdd01->prepare($q);
		$query->execute(array($name,urlencode($url),$open,$color,$icon,$_SESSION['user_id'],$idwid));
		$tpmod="";
	}
	
	/*--> Delete */
	if($vlsup!="") {
		$q="DELETE FROM ".$config["dbprefixe"]."panel_widget_bookmark WHERE panel_widget_bookmark_id=?";
		$query=$bdd01->prepare($q);
		$query->execute(array($id));
		$tpmod="";
	}

?>
<style>
	body{
		overflow-x:hidden; 
	}
</style>
<?

	echo "<form id='formulaire' class='form-horizontal' role='form' method='post' enctype='multipart/form-data'>";
	
	echo "<input id='id' name='id' type='hidden' value='".$id."'>";
	echo "<input id='idwid' name='idwid' type='hidden' value='".$idwid."'>";
	echo "<input id='mode' name='mode' type='hidden' value='".$mode."'>";
	echo "<input id='tpmod' name='tpmod' type='hidden' value=''>";
	

//-- DISPLAY -------------------------------------------------------------------------------------------------------------------------------

	if($tpmod=="") { 
		if($mode=="widget") {
			$q="SELECT * FROM ".$config["dbprefixe"]."panel_widget_bookmark, ".$config["dbprefixe"]."icon WHERE panel_widget_bookmark_widget=? AND panel_widget_bookmark_icon=icon_id";
			$query=$bdd01->prepare($q);
			$query->execute(array($idwid));
		}
		else {
			$q="SELECT * FROM ".$config["dbprefixe"]."panel_widget_bookmark, ".$config["dbprefixe"]."icon WHERE panel_widget_bookmark_user=? AND panel_widget_bookmark_icon=icon_id ORDER BY panel_widget_bookmark_widget";
			$query=$bdd01->prepare($q);
			$query->execute(array($_SESSION["user_id"]));
		}
		
		$catwid="0";
		
		
		$fgitem=false;
		
		while($row=$query->fetch()){	
			if($mode!="widget"&&$catwid!=$row["panel_widget_bookmark_widget"]){
				if($fgproprio) {
					echo "<div id='myitemadd'>";
					echo "<button type='submit' class='btn' onClick='$(\"#idwid\").val(\"".$catwid."\"); $(\"#id\").val(\"\"); $(\"#tpmod\").val(\"SUBMIT\");'><span class='glyphicon glyphicon-plus'></span></button>";
					echo "</div>";
				}
				
				if($row["panel_widget_bookmark_widget"]!="") {
					
					$q="SELECT * FROM ".$config["dbprefixe"]."panel_widget WHERE panel_widget_id=?";
					$query2=$bdd02->prepare($q);
					$query2->execute(array($row["panel_widget_bookmark_widget"]));
					if($row2=$query2->fetch()) {
						echo "<h2 id='myitemtitle'>".$row2["panel_widget_label"]."</h2>";
					}
				}
				
				$catwid=$row["panel_widget_bookmark_widget"];
			}

			$color="";
			if($row["panel_widget_bookmark_color"]!="") $color="background-color: #".$row["panel_widget_bookmark_color"].";";
							
			echo "<div id='myitem' class='myitem' style='$color' rel='".$row["panel_widget_bookmark_id"]."'>";
			if($row["panel_widget_bookmark_open"]==0)
				echo "<a href='index.php?framed=true&view=".urldecode($row["panel_widget_bookmark_url"])."' target='_top'>";
			else
				echo "<a href='".urldecode($row["panel_widget_bookmark_url"])."' target='_blank'>";
			
			echo "<img src='local/images/icon/".$row["icon_url"]."'/>";
			echo "<label>".$row["panel_widget_bookmark_name"]."</label>";
			echo "</a>";
			echo "<a id='badge-delete-".$row["panel_widget_bookmark_id"]."' style='display:none;' class='badge-delete mybtn fa-trash fa' onClick='$(\"#idwid\").val(\"".$idwid."\"); $(\"#id\").val(\"".$row["panel_widget_bookmark_id"]."\"); $(\"#tpmod\").val(\"DELETE\");$(\"#formulaire\").submit();'></a>";
			echo "</div>";

		}
		
		if($fgproprio) {
			if($mode=="widget") {
				echo "<div id='myitemadd'>";
				echo "<button type='submit' class='btn' onClick='$(\"#idwid\").val(\"".$idwid."\"); $(\"#id\").val(\"\"); $(\"#tpmod\").val(\"SUBMIT\");'><span class='glyphicon glyphicon-plus'></span></button>";
				echo "</div>";
			}
			else {
				echo "<div id='myitemadd'>";
				echo "<button type='submit' class='btn' onClick='$(\"#idwid\").val(\"".$catwid."\"); $(\"#id\").val(\"\"); $(\"#tpmod\").val(\"SUBMIT\");'><span class='glyphicon glyphicon-plus'></span></button>";
				echo "</div>";
			}
		}
	}

//-- SUBMIT --------------------------------------------------------------------------------------------------------------------------------

	elseif($tpmod=="SUBMIT") {
		echo "<br>";
		echo "<div class='form-group'>";
		echo "<div class='col-sm-12'>";
		echo "<input id='vladd' name='vladd' class='btn btn-primary' type='submit' value='Valider' />";
		echo "&nbsp;";
		echo "<input class='btn btn-primary' type='submit' value='Annuler' />";
		echo "</div>";
		echo "</div>";	

		// Valeur par défaut
		$open=0;
		
		// Récupérer l'url de l'image en cours
		if($icon!="") {
			$q="SELECT * from ".$config["dbprefixe"]."icon WHERE icon_id=?";
			$query=$bdd01->prepare($q);
			$query->execute(array($icon));
			if($row=$query->fetch()) $urlicon=$row["icon_url"];
		}
	}

//-- DELETE --------------------------------------------------------------------------------------------------------------------------------

	elseif($tpmod=="DELETE") {
		echo "<br>";
		echo "<div class='form-group'>";
		echo "<div class='col-sm-12'>";
		echo "<input id='vlsup' name='vlsup' class='btn btn-primary' type='submit' value='Confirmer la suppression' />";
		echo "&nbsp;";
		echo "<input class='btn btn-primary' type='submit' value='Annuler' />";    
		echo "</div>";
		echo "</div>";	  
		
		// Valeur par défaut
		$q = "SELECT * FROM ".$config["dbprefixe"]."panel_widget_bookmark, ".$config["dbprefixe"]."icon WHERE panel_widget_bookmark_id=? AND panel_widget_bookmark_icon=icon_id";
		$query=$bdd01->prepare($q);
		$query->execute(array($id));
		if($row=$query->fetch()) {
			echo "<div id='myavatarprofil' style='float:left; margin-left: 10px; margin-right:5px; margin-bottom:10px; border:none;'>";
			echo "<img id='imgapp' src='local/images/icon/".$row["icon_url"]."' width='90px' height='90px'></img>";
			echo "</div>";
			
			echo $row['panel_widget_bookmark_name']."<br>";
			echo "<span id='avatar_description'>";
			echo "url : <a href='".$row['panel_widget_bookmark_url']."' target='_black'>".urldecode($row['panel_widget_bookmark_url'])."</a><br>";				
		}
	}

//-- FORMULAIRE -----------------------------------------------------------------------------------------------------------------------------

	if($tpmod=="SUBMIT"||$tpmod=="MODIFY") {
	?>
		<div class="form-group">
			<label for="name" class="col-sm-3 control-label">Nom*</label>
			<div class="col-sm-6"><input name="name" id="name" type="titre" class="form-control" placeholder="Nom" value="<? echo $name; ?>"></div>
		</div>

		<div class="form-group">
			<label for="url" class="col-sm-3 control-label">Url*</label>
			<div class="col-sm-6"><input name="url" id="url" type="titre" class="form-control" placeholder="Url" value="<? echo urldecode($url); ?>"></div>
		</div>	

		<div class="form-group">
			<label for="open" class="col-sm-3 control-label">Mode d'ouverture*</label>
			<div class="col-sm-6">
				<select name="open" id="open" class="form-control">
					<option value='0' <? if($open==0) echo "selected"; ?>>Dans le portail</option>";
					<option value='1' <? if($open==1) echo "selected"; ?>>Nouvel onglet</option>";
				</select>
			</div>
		</div>
		
		<div class="form-group">
			<label for="color" class="col-sm-3 control-label">Couleur</label>
			<div class="col-sm-6">
				<input type="text" value="<? echo $color; ?>" id="color" name="color" class="pick-a-color form-control">
			</div>
		</div>	
				
		<input 	value="<? echo $icon; ?>" id="icon"	name="icon"	type="hidden"/>
		<?
		echo "<div class='form-group'>";
		echo "<div style='width:140px; margin:auto; '>";
		echo "<div id='myavatarprofil' style='float:left; margin-right:5px; margin-bottom:10px; border:none;'>";
		echo "<img id='imgapp' src='local/images/icon/".$urlicon."' width='90px' height='90px'></img>";
		echo "</div>";
		echo "<div style='float:left;'>";
		echo "<a class='mybtn glyphicon glyphicon-folder-close' data-toggle='modal' data-target='#mymodal-01' title='Séléctionner un icône' onClick='ModalLoad(\"mymodal-01\",\"icon-selection\",0,0)'></a>";
		echo "</div>";
		echo "</div>";
		echo "</div>";
	}
	
	echo "</form>";
?>

<link rel="stylesheet" href="lib/pickColor/pick-a-color-1.2.3.min.css">
<script src="lib/pickColor/tinycolor-0.9.15.min.js"></script>
<script src="lib/pickColor/pick-a-color-1.2.3.min.js"></script>

<script>
	function chargeApplication(srcframe) {
		if($("#itemframe").length>0) {
			$("#itemframe").attr("src",srcframe);
			$("#itemframe").css("display","block");
		}
		else {
			<? if($mode=="widget") { ?>
			window.parent.changeItem(srcframe);	
			<? } else { ?>
			window.parent.chargeApplication(srcframe);	
			<? } ?>
		}
	}	

	$(".myitem")
		.mouseover(function() {
			<? if($fgproprio) { ?>
			var id = $(this).attr('rel');
			$("#badge-delete-"+id).css("display","block");
			<? } ?>
		})
		.mouseout(function() {
			<? if($fgproprio) { ?>
			var id = $(this).attr('rel');
			$("#badge-delete-"+id).css("display","none");
			<? } ?>
		});
			
<? if($tpmod=="SUBMIT"||$tpmod=="MODIFY"||$tpmod=="DELETE") { ?>
		$(document).ready(function() {
			$('#name').focus();

			$(".pick-a-color").pickAColor({
			  showSpectrum            : true,
				showSavedColors         : true,
				saveColorsPerElement    : true,
				fadeMenuToggle          : true,
				showAdvanced			: true,
				showBasicColors         : true,
				showHexInput            : true,
				allowBlank				: true,
				inlineDropdown			: true
			});		
		} );
	
	
<? } ?>	
	
	<?php echo $jsaction ?>
</script>
