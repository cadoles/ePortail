<?
	$levelpage=99;
	include("include/permission.php");

	$login=$_GET["login"];
	$token=$_GET["token"];
	
	
	if($login==""||$token=="") die();

	$q="SELECT * FROM ".$config["dbprefixe"]."inscription WHERE (inscription_login=? AND inscription_token=?)";	
	$query=$bdd01->prepare($q);
	$query->execute(array($login,$token));
	if($row=$query->fetch()) {
		// On vérifie avant l'inscrption effective si une autre personne c'est pas connecté
		$q="SELECT * FROM ".$config["dbprefixe"]."user WHERE (user_login=? OR user_email=?)";
		$tbq=array($row["inscription_login"],$row["inscription_email"]);

		$query2=$bdd02->prepare($q);
		$query2->execute($tbq);
		if($row2=$query2->fetch()) {
			$q="DELETE FROM ".$config["dbprefixe"]."inscription WHERE (inscription_login=? AND inscription_token=?)";	
			$query=$bdd01->prepare($q);
			$query->execute(array($login,$token));
						
			echo "<div class='alert alert-danger' role='alert' style='margin: 20px auto 0px auto; max-width: 800px;'>";
			echo "Un utilisateur avec cet email ou ce login existe déjà<br>";
			echo "Veuillez vous réinscrire<br>";
			echo "</div>";
		}
		else {
			$fgerreur=false;
			if($config['activerLDAP'] == 'true') {
				$ds = ldap_connect($config['LDAPserver'], $config['LDAPport']);
				if ($ds) {
					$r=ldap_bind($ds,$config['LDAPwriterdn'],$config['LDAPwriterpw']);
					if($r) {
						// Prépare les données
						$info["uid"]				= $row["inscription_login"];
						$info["userPassword"]		= "{MD5}".base64_encode(pack("H*",$row["inscription_password"]));
						$info["cn"]					= $row["inscription_firstname"]." ".$row["inscription_lastname"];
						$info["sn"] 				= $row["inscription_lastname"];
						$info["givenName"]			= $row["inscription_firstname"];
						$info["mail"] 				= $row["inscription_email"];
						$info["objectclass"][0]		= "top";
						$info["objectclass"][1]		= "inetOrgPerson";
						
						// Ajoute les données au dossier
						$r = ldap_add($ds, "uid=".$row["inscription_login"].", ".$config['LDAPorganisation'].",".$config['LDAPracine'], $info);
						ldap_close($ds);
						
						if($r!=1) {
							$fgerreur=true;
							echo "<div class='alert alert-danger' role='alert' style='margin: 20px auto 0px auto; max-width: 800px;'>";
							echo "Confirmation d'inscription impossible = inscription LDAP impossible";
							echo "</div>";
						}
						
					}
					else {
						$fgerreur=true;
						echo "<div class='alert alert-danger' role='alert' style='margin: 20px auto 0px auto; max-width: 800px;'>";
						echo "Confirmation d'inscription impossible";
						echo "</div>";
					}
				}	
				else {
					$fgerreur=true;
					echo "<div class='alert alert-danger' role='alert' style='margin: 20px auto 0px auto; max-width: 800px;'>";
					echo "Confirmation d'inscription impossible";
					echo "</div>";
				}
			}
			
			if(!$fgerreur) {
				$q="INSERT INTO ".$config["dbprefixe"]."user (user_login, user_password, user_lastname, user_firstname, user_email,user_profil) VALUE (?,?,?,?,?,?)";
				$inscription_token=uniqid(rand(),true);
				$tbq=array($row["inscription_login"],$row["inscription_password"],$row["inscription_lastname"],$row["inscription_firstname"],$row["inscription_email"],50);
				$query=$bdd01->prepare($q);
				$query->execute($tbq);
				
				$q="DELETE FROM ".$config["dbprefixe"]."inscription WHERE (inscription_login=? AND inscription_token=?)";	
				$query=$bdd01->prepare($q);
				$query->execute(array($login,$token));

				
				echo "<div class='alert alert-success' role='alert' style='margin: 20px auto 0px auto; max-width: 800px;'>";
				echo "Merci de votre inscription<br>";
				echo "Vous pouvez à présent vous connecter";
				echo "</div>";			
			}
		}
	}
	else {
		echo "<div class='alert alert-danger' role='alert' style='margin: 20px auto 0px auto; max-width: 800px;'>";
		echo "Confirmation d'inscription impossible";
		echo "</div>";
	}
	
?>
