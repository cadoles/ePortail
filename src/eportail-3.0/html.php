<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><? echo $config['title']." - ".$config['sstitle'] ?></title>

    <!-- jQuery UI CSS -->
    <link href="lib/jquery-ui/jquery-ui.css" rel="stylesheet">
    
    <!-- Bootstrap Core CSS -->
    <link href="style/bootstrap.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="lib/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- dataTables CSS -->
    <link href="lib/dataTables/dataTables.bootstrap.css" rel="stylesheet">
    
    <!-- Custom Fonts -->
    <link href="style/fonts/font-awesome-4.2.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- Base CSS -->
    <link href="style/base.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="style/style.css" rel="stylesheet">
   
    <!-- Local Theme -->
    <? if($config['theme']!="") { ?>
    <link href="style/themes/<? echo $config['theme']; ?>/style.css" rel="stylesheet" type="text/css">
    <? } ?>

	<!-- Local CSS -->
	<? if(file_exists($config['localdirectory']."/local/config/style.css")) { ?>
		<link href="local/config/style.css" rel="stylesheet" type="text/css">
	<? } ?>
	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->





    <!-- jQuery Version 1.11.0 -->
    <script src="lib/jquery/jquery.js"></script>

    <!-- JQuery UI Core JavaScript -->
    <script src="lib/jquery-ui/jquery-ui.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="lib/bootstrap/bootstrap.js"></script>

    <!-- dataTables Plugin JavaScript -->
    <script src="lib/dataTables/jquery.dataTables.js"></script>
    <script src="lib/dataTables/dataTables.bootstrap.js"></script>
    
    <!-- Metis Menu Plugin JavaScript -->
    <script src="lib/metisMenu/metisMenu.min.js"></script>

    <!-- Local JavaScript -->
    <script src="lib/local.js"></script>
    
</head>


