<style>
	#banner  {
		background: url(style/themes/tulkas/images/banner.jpg) no-repeat center 0px #222;
		background-size: cover;
		height: 250px;
		width: 100%;
		position:absolute;
	}
	
	.imgreflect {
        -webkit-transition: all 0.2s ease;
        -moz-transition:    all 0.2s ease;
        -o-transition:      all 0.2s ease;
        -ms-transition:     all 0.2s ease;
        transition:         all 0.2s ease;
        -webkit-box-reflect: below 0 -webkit-gradient(linear, left bottom, left top, from(rgba(255,255,255,0.25)), color-stop(0.3, transparent));
        -webkit-box-reflect: none;
         box-shadow: 1px 1px 12px #555;
	}
	
	
	.imgreflect:hover {
		-webkit-transform:scale(1.05); 	/* Safari and Chrome 	*/
		-moz-transform:scale(1.05); 		/* Firefox 				*/
		-ms-transform:scale(1.05); 		/* IE 9 				*/
		-o-transform:scale(1.05); 		/* Opera 				*/
		 transform:scale(1.05);	
		 box-shadow: 1px 1px 20px #555;
	}
</style>

<div id="banner" style="margin:0px -15px 0px -15px; box-shadow: 0px 0px 12px #555">

</div>

<div class='container-fluid'>
<?
	// Affichage des catégorie
	$div=array();
	echo "<ul id='navitem' class='nav' style='margin-top: 30px; margin-left: 50px;'>";
	$moreone=false;
	foreach ($tbcat as $cat) {
		if($firstcat=="") $firstcat=$cat[1];
		else $moreone=true;
		
		if($cat[3]!="") {
			echo "<style>";
			echo "#cat-".$cat[1]." {";
			echo "background-color:#".$cat[3]." !important;";
			echo "border-bottom: 5px solid #".$cat[3]." !important;";
			echo "}";
			
			echo "#cat-".$cat[1]." a:hover {";
			echo "background-color:#".$cat[3]." !important;";
			echo "}";
			
			echo "</style>";
		}
		
		echo "<li id='cat-".$cat[1]."'><a onClick='changeCategorie(".$cat[1].");'>".$cat[2]."</a></li>";
	}
	echo "</ul>";

	if(!$moreone) {
		echo "<style>#navitem{display:none;}</style>";
	}
?>
</div>

<div class='container-fluid' style='padding-top:<? if($moreone) echo "110px"; else echo "170px"; ?>;'>
	
<? 
	$fgperm=false;
	foreach ($tbitem as $item) {
?>

<div class='tulkasitem tulkasitem-<? echo $item[2]; ?>' style='width:200px; float:left; position:relative; height:350px;'>
<?
	if($item[8]==0){
		if(stripos(" ".$item[6],"index.php")==1)
			echo "<a href='".urldecode($item[6])."' target='_top'>";
		else
			echo "<a href='index.php?framed=true&view=".urldecode($item[6])."' target='_top'>";
	}
	else
		echo "<a href='".urldecode($item[6])."' target=_blank>";
?>

<div class="thumbnail imgreflect" style="border:none;background:url(local/images/icon/<? echo $item[7]; ?>) no-repeat #fff; background-size:130px 180px; width:130px; height:180px;margin: 0px auto 5px auto">
<img alt="envole" src="style/images/blank.gif" style="border-radius:10px;" />
</div>
</a>

<div class="caption">
<h3 style="text-align:center"><? echo $item[4]; ?></h3>
<p style="text-align:center; font-size:10px; width:80%; margin: 0px auto 30px auto;"><? echo $item[5]; ?></p>
</div>
</div>

<? } ?>


</div>



<script>
	function changeCategorie(idcat) {
		$("#navitem li").removeClass("active");
		$("#cat-"+idcat).addClass("active");
		$(".tulkasitem").css("display","none");
		$(".tulkasitem-"+idcat).css("display","block");
		return false;
	}
	
	
	$(document).ready(function()
	{	
		changeCategorie(<? echo $firstcat; ?>);
	});
	
</script>
