<?php
	header("Content-Type: text/xml; charset=utf-8");
	include_once("include/include.php");


	echo "<rss version='2.0'>";
	echo "<channel>";
	echo "<title>".$config["title"]."</title>";
	echo "<link>".$config["urlbase"]."</link>";
	echo "<description>".$config["sstitle"]."</description>";

	$idfeed=$_GET["id"];
	$fgperm=false;
	foreach ($tbflux as $flux) {
		if($flux[1]==$idfeed) $fgperm=true;
	}
	
	if(!$fgperm) {
		echo
		"<item>
			<title>Permission</title>
			<link>".$config["urlbase"]."</link>
			<description>Vous n'êtes pas autorisé à lire ce flux.</description>
		</item>";
	}
	else {
		$q="SELECT * FROM ".$config["dbprefixe"]."fluxmsg WHERE fluxmsg_flux=? ORDER BY fluxmsg_date";
		$query=$bdd01->prepare($q);
		$query->execute(array($idfeed));
		while($row=$query->fetch()){
		echo
			"<item>
				<title>".$row["fluxmsg_name"]."</title>
				<link>".$row["fluxmsg_date"]."</link>
				<description>".$row["fluxmsg_html"]."</description>
			</item>";
		}
	
	}
	echo "</channel></rss>";
?>
