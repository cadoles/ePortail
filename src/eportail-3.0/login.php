<?
	include_once("include/config.php");
	include_once("include/ldap.php");
	include_once("include/mysql.php");
	include_once("include/session.php");
	include_once("include/function.php");
	if ($config['modeAuthentification']=="CAS") include("include/CASauto.php");
	

	if ($config['modeAuthentification']=="CAS") {
		include("include/CASlogin.php");
	}
	
	$fglog		= $_POST["fglog"];
	$login 		= $_POST["login"];
	$password 	= $_POST["password"];
	
	// Vérifier que l'utilisateur existe bien
	if($fglog!="") {
		if($config['modeAuthentification']=="MYSQL") {
			$_SESSION['user_id'] = "";
			$query=$bdd01->prepare("SELECT * FROM ".$config["dbprefixe"]."user WHERE user_login=? AND user_password=md5(?)");
			$query->execute(array($login,$password));
			if($row=$query->fetch()) {
				savesession($row,$login,$password,isset($_POST['remember']));
				header('Location: index.php');			
			}
		}
		
		if($config['modeAuthentification']=="LDAP") {
			$fgerreur=false;
			$_SESSION['user_id'] = "";
			
			// Connection à l'annuaire
			$ds = ldap_connect($config['LDAPserver'], $config['LDAPport']);
			if (!$ds) {
				$fgerreur=true;
				echo "<script>alert('Serveur LDAP injoingnable');</script>";
			}
			else {
				$r=ldap_bind($ds,$config['LDAPreaderdn'],$config['LDAPreaderpw']);
				if(!$r) {
					$fgerreur=true;
					echo "<script>alert('Impossible de se connecter à l\annuaire LDAP');</script>";
				}
			}
			
			if(!$fgerreur) {
				// Recherche du login dans l'annuaire
				$lbfilter=str_replace("#login#",$login,$config['LDAPfilterauth']);
				$res = ldap_search($ds,$config['LDAPracine'],$lbfilter);
				$ldapinfo = ldap_get_entries($ds, $res);
				if($ldapinfo["count"]>0) {
					$r=ldap_bind($ds,$ldapinfo[0]["dn"],$password);
					if($r) {
						autocreate($login,$password,$ldapinfo[0][$config['LDAPfirstname']][0],$ldapinfo[0][$config['LDAPlastname']][0],$ldapinfo[0][$config['LDAPemail']][0]);
						
						$query=$bdd01->prepare("SELECT * FROM ".$config["dbprefixe"]."user WHERE user_login=?");
						$query->execute(array($login));
						if($row=$query->fetch())
						{
							savesession($row,$login,$password,isset($_POST['remember']));
							header('Location: index.php');
						}
					}
				}
			}
		}
	}
	
	include("html.php");
?>
	<body id="bodylogin">
    <div class="container">
		<div id="login-header-logo" <? if($config["locallogo"]!="") echo "style='background-image:url(\"local/images/logo/logo.png\");".$config["localcsslogo"]."'"; ?>></div>
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Identifiez-vous</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" method='post' >
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Login" name="login" type="login" autofocus <? if ($config["modeRemember"]=="true" && isset($_COOKIE['username'])) echo "value='".$_COOKIE['username']."'"; ?> >
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" type="password">
                                </div>
                                <? if($config["modeRemember"]=="true") { ?>
                                <div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="Remember Me" <? if ($config["modeRemember"]=="true" && isset($_COOKIE['fgremember'])) echo "checked"; ?>>Garder ma session active
                                    </label>
                                </div>
                                <? } ?>
                                <!-- Change this to a button or input when using this as a form -->
                                <input id="fglog" name="fglog" type="submit" class="btn btn-lg btn-success btn-block" value="Login">
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?
	include("footer.php");
?>
