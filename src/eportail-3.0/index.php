<?
	include("include/include.php");
	
	$view=$_GET["view"];
	$id=$_GET["id"];
	$framed=$_GET["framed"];
	
	// Si aucune vue on prend la premiere page de l'utilisateur
	if($view==""&&$id=="") {
		if($tbpage[0][1]!="")
			$id=$tbpage[0][1];
		else		
			$view="envole/index.php";
	}
	
	
	// Si id de page demandé
	if($id!="") {
		$q="SELECT * FROM ".$config["dbprefixe"]."panel WHERE panel_id=?";
		$query=$bdd01->prepare($q);
		$query->execute(array($id));		
		if($row=$query->fetch()){	
			// Si url interne on passe en mode intégré
			if(stripos(" ".$row["panel_url"],"view/")==1) {
				$view=str_replace("view/","",$row["panel_url"]);
				if(file_exists($config["localdirectory"]."/style/themes/".$config["theme"]."/template/".$view))
					$fgtemplate=true; 
					
				$framed=false;
			}
			// Sinon affiche de l'url en mode frame
			else {
				$view=$row["panel_url"];
				$framed=true;
			}
		}
	}
	
	include("html.php");
	if($framed)	echo "<style>body{overflow:hidden;}</style>";
	include("header.php");
?>

<!-- Page Content -->
<?
	if($framed) {
		echo "<iframe id='frameresize' src='$view' frameborder=0 width='100%' height='500px'></iframe>";
	}	
	else {
		echo "<div id='page-wrapper'>";
		echo "<div class='row'>";
		echo "<div class='col-lg-12'>";	
		if($fgtemplate) {
			include($config["localdirectory"]."/style/themes/".$config["theme"]."/template/".$view);
		}
		else {
			include("view/".$view);
		}
		echo "</div>";
		echo "</div>";
		echo "</div>";
	}
?>

<div id="mymodal-01" class="modal fade bs-item-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title">TITRE</h4>
		</div>
		<div class="modal-body">
			<iframe id="framemodal" frameborder=0 width="100%" height="520px"></iframe>
		</div>
	</div>
  </div>
</div>

<div id="myload"></div>

<?
	include("footer.php");
?>

