<?
	include("include/include.php");
	
	$keymodal	= $_GET["key"];					// Clé de session
	$view		= $_SESSION[$keymodal]["view"];	// view
	
	
	if($view=="") $view="diened.php";
	
	// GESTION DES ICONES
	if($_SESSION[$keymodal]["type"]=="icon-cropinsert") {
		$_SESSION[$keymodal]['modal-dirimg']		= "local/images/icon";
		$_SESSION[$keymodal]['modal-refresh']		= "1";		
	}
	elseif($_SESSION[$keymodal]["type"]=="icon-cropupdate") {
		$_SESSION[$keymodal]['modal-dirimg']		= "local/images/icon";
		$_SESSION[$keymodal]['modal-igico']			= "#imgapp".$_SESSION[$keymodal]["id1"];
		$_SESSION[$keymodal]['modal-idquery']		= $_SESSION[$keymodal]["id1"];
		$_SESSION[$keymodal]['modal-refresh']		= 0;
	}
	elseif($_SESSION[$keymodal]["type"]=="icon-dropinsert") {
		$_SESSION[$keymodal]['modal-directory']		= "local/images/icon";					// cible du upload
		$_SESSION[$keymodal]['modal-typeupload']	= "image";								// image | all 
		$_SESSION[$keymodal]['modal-multiupload']	= false;								// true | false 
		$_SESSION[$keymodal]['modal-resize']		= true;									// true | false uniquement si typeupload = image
		$_SESSION[$keymodal]['modal-size']			= 90;									// taille en px pour le rezise d'une image
		$_SESSION[$keymodal]['modal-rename']		= true;									// true | false renommage des fichiers uploadés
		$_SESSION[$keymodal]['modal-prefix']		= "thumbnail_";							// prefix des fichiers renommées 
		
		$_SESSION[$keymodal]['modal-refresh']		= 1;									// Rafraichir le père fermeture
		$_SESSION[$keymodal]['modal-idquery']		= 0;									// identifiant unique associé à la query
		
		$_SESSION[$keymodal]['modal-idimage']		= "";									// id html de l'image à afficher
		$_SESSION[$keymodal]['modal-idinput']		= "";									// id html de l'input de stockage		
	}
	elseif($_SESSION[$keymodal]["type"]=="icon-dropupdate") {
		$_SESSION[$keymodal]['modal-directory']		= "local/images/icon";					// cible du upload
		$_SESSION[$keymodal]['modal-typeupload']	= "image";								// image | all 
		$_SESSION[$keymodal]['modal-multiupload']	= false;								// true | false 
		$_SESSION[$keymodal]['modal-resize']		= true;									// true | false uniquement si typeupload = image
		$_SESSION[$keymodal]['modal-size']			= 90;									// taille en px pour le rezise d'une image
		$_SESSION[$keymodal]['modal-rename']		= true;									// true | false renommage des fichiers uploadés
		$_SESSION[$keymodal]['modal-prefix']		= "thumbnail_";							// prefix des fichiers renommées 
		
		$_SESSION[$keymodal]['modal-refresh']		= 1;									// Rafraichir le père fermeture
		$_SESSION[$keymodal]['modal-idquery']		=  $_SESSION[$keymodal]["id1"];			// identifiant unique associé à la query
		
		$_SESSION[$keymodal]['modal-idimage']		= "#imgapp".$_SESSION[$keymodal]["id1"];// id html de l'image à afficher
		$_SESSION[$keymodal]['modal-idinput']		= "";									// id html de l'input de stockage		
	}
	elseif($_SESSION[$keymodal]["type"]=="icon-selection") {
		$_SESSION[$keymodal]['modal-idimage']		= "imgapp";								// id html de l'image à afficher
		$_SESSION[$keymodal]['modal-idinput']		= "icon";								// id html de l'input de stockage	
	}
	
	// GESTION DES AVATARS
	elseif($_SESSION[$keymodal]["type"]=="avatar-cropupdate") {
		$_SESSION[$keymodal]['modal-dirimg']		= "local/images/avatar";
		$_SESSION[$keymodal]['modal-lbico']			= "#user_avatar";
		$_SESSION[$keymodal]['modal-igico']			= "#img_avatar_profil";
	}
	
	// GESTION SSO
	elseif($_SESSION[$keymodal]["type"]=="ssocommunity-insert") {
		$_SESSION[$keymodal]['modal-type']			= "SSOCOMMUNITY";
		$_SESSION[$keymodal]['modal-mode']			= "";
	}	
	elseif($_SESSION[$keymodal]["type"]=="ssocommunity-delete") {
		$_SESSION[$keymodal]['modal-type']			= "SSOCOMMUNITY";
		$_SESSION[$keymodal]['modal-mode']			= "DELETE";
	}		
	elseif($_SESSION[$keymodal]["type"]=="ssoprofil-insert") {
		$_SESSION[$keymodal]['modal-type']			= "SSOPROFIL";
		$_SESSION[$keymodal]['modal-mode']			= "";
	}	
	elseif($_SESSION[$keymodal]["type"]=="ssoprofil-delete") {
		$_SESSION[$keymodal]['modal-type']			= "SSOPROFIL";
		$_SESSION[$keymodal]['modal-mode']			= "DELETE";
	}
	
	// GESTION DES APPLICATIONS
	elseif($_SESSION[$keymodal]["type"]=="applicationprofil-insert") {
		$_SESSION[$keymodal]['modal-type']			= "ITEM";
		$_SESSION[$keymodal]['modal-mode']			= "";
	}
	elseif($_SESSION[$keymodal]["type"]=="applicationprofil-delete") {
		$_SESSION[$keymodal]['modal-type']			= "ITEM";
		$_SESSION[$keymodal]['modal-mode']			= "DELETE";
	}	
	elseif($_SESSION[$keymodal]["type"]=="applicationssoprofil-insert") {
		$_SESSION[$keymodal]['modal-type']			= "ITEMSSO";
		$_SESSION[$keymodal]['modal-mode']			= "";
	}
	elseif($_SESSION[$keymodal]["type"]=="applicationssoprofil-delete") {
		$_SESSION[$keymodal]['modal-type']			= "ITEMSSO";
		$_SESSION[$keymodal]['modal-mode']			= "DELETE";
	}	
	elseif($_SESSION[$keymodal]["type"]=="applicationldapprofil-insert") {
		$_SESSION[$keymodal]['modal-type']			= "ITEMLDAP";
		$_SESSION[$keymodal]['modal-mode']			= "";
	}
	elseif($_SESSION[$keymodal]["type"]=="applicationldapprofil-delete") {
		$_SESSION[$keymodal]['modal-type']			= "ITEMLDAP";
		$_SESSION[$keymodal]['modal-mode']			= "DELETE";
	}
	
	// GESTION DES PAGES
	elseif($_SESSION[$keymodal]["type"]=="pageprofil-insert") {
		$_SESSION[$keymodal]['modal-type']			= "PAGE";
		$_SESSION[$keymodal]['modal-mode']			= "";
	}
	elseif($_SESSION[$keymodal]["type"]=="pageprofil-delete") {
		$_SESSION[$keymodal]['modal-type']			= "PAGE";
		$_SESSION[$keymodal]['modal-mode']			= "DELETE";
	}
	elseif($_SESSION[$keymodal]["type"]=="pagessoprofil-insert") {
		$_SESSION[$keymodal]['modal-type']			= "PAGESSO";
		$_SESSION[$keymodal]['modal-mode']			= "";
	}
	elseif($_SESSION[$keymodal]["type"]=="pagessoprofil-delete") {
		$_SESSION[$keymodal]['modal-type']			= "PAGESSO";
		$_SESSION[$keymodal]['modal-mode']			= "DELETE";
	}
	elseif($_SESSION[$keymodal]["type"]=="pageldapprofil-insert") {
		$_SESSION[$keymodal]['modal-type']			= "PAGELDAP";
		$_SESSION[$keymodal]['modal-mode']			= "";
	}
	elseif($_SESSION[$keymodal]["type"]=="pageldapprofil-delete") {
		$_SESSION[$keymodal]['modal-type']			= "PAGELDAP";
		$_SESSION[$keymodal]['modal-mode']			= "DELETE";
	}	

	// GESTION DES FLUX
	elseif($_SESSION[$keymodal]["type"]=="fluxmsg-insert") {
		$_SESSION[$keymodal]['modal-mode']			= "SUBMIT";
	}	
	elseif($_SESSION[$keymodal]["type"]=="fluxmsg-update") {
		$_SESSION[$keymodal]['modal-mode']			= "MODIFY";
	}	
	elseif($_SESSION[$keymodal]["type"]=="fluxmsg-delete") {
		$_SESSION[$keymodal]['modal-mode']			= "DELETE";
	}		
	elseif($_SESSION[$keymodal]["type"]=="fluxprofil-insert") {
		$_SESSION[$keymodal]['modal-type']			= "FLUX";
		$_SESSION[$keymodal]['modal-mode']			= "";
	}
	elseif($_SESSION[$keymodal]["type"]=="fluxprofil-delete") {
		$_SESSION[$keymodal]['modal-type']			= "FLUX";
		$_SESSION[$keymodal]['modal-mode']			= "DELETE";
	}
	elseif($_SESSION[$keymodal]["type"]=="fluxssoprofil-insert") {
		$_SESSION[$keymodal]['modal-type']			= "FLUXSSO";
		$_SESSION[$keymodal]['modal-mode']			= "";
	}
	elseif($_SESSION[$keymodal]["type"]=="fluxssoprofil-delete") {
		$_SESSION[$keymodal]['modal-type']			= "FLUXSSO";
		$_SESSION[$keymodal]['modal-mode']			= "DELETE";
	}
	elseif($_SESSION[$keymodal]["type"]=="fluxldapprofil-insert") {
		$_SESSION[$keymodal]['modal-type']			= "FLUXLDAP";
		$_SESSION[$keymodal]['modal-mode']			= "";
	}
	elseif($_SESSION[$keymodal]["type"]=="fluxldapprofil-delete") {
		$_SESSION[$keymodal]['modal-type']			= "FLUXLDAP";
		$_SESSION[$keymodal]['modal-mode']			= "DELETE";
	}
		
	// GESTION DES ONGLETS
	elseif($_SESSION[$keymodal]["type"]=="panel-insert") {
		
	}
	elseif($_SESSION[$keymodal]["type"]=="panel-update") {
		$_SESSION[$keymodal]['modal-mode']			= "modpanel";
	}
	
	// GESTION DES WIDGETS
	elseif($_SESSION[$keymodal]["type"]=="widget-insert") {
		$_SESSION[$keymodal]['modal-mode']			= "addwidget";
	}
	elseif($_SESSION[$keymodal]["type"]=="widget-update") {
		$_SESSION[$keymodal]['modal-mode']			= "modwidget";
	}
	elseif($_SESSION[$keymodal]["type"]=="widget-template") {
		
	}
	elseif($_SESSION[$keymodal]["type"]=="widget-config") {
		
	}
	elseif($_SESSION[$keymodal]["type"]=="widget-carouseldrop") {
		$_SESSION[$keymodal]['modal-directory']		= "local/images/carousel";				// cible du upload
		$_SESSION[$keymodal]['modal-typeupload']	= "image";								// image | all 
		$_SESSION[$keymodal]['modal-multiupload']	= false;								// true | false 
		$_SESSION[$keymodal]['modal-resize']		= false;								// true | false uniquement si typeupload = image
		$_SESSION[$keymodal]['modal-size']			= 0;									// taille en px pour le rezise d'une image
		$_SESSION[$keymodal]['modal-rename']		= true;									// true | false renommage des fichiers uploadés
		$_SESSION[$keymodal]['modal-prefix']		= "thumbnail_";							// prefix des fichiers renommées 
		
		$_SESSION[$keymodal]['modal-refresh']		= 0;									// Rafraichir le père fermeture
		$_SESSION[$keymodal]['modal-idquery']		= 0;									// identifiant unique associé à la query
		
		$_SESSION[$keymodal]['modal-idimage']		= "monimage"; 							// id html de l'image à afficher
		$_SESSION[$keymodal]['modal-idinput']		= "image";								// id html de l'input de stockage		
	}

		
	// REFUS
	else{
		$view="diened.php";
	}
	
	include("html.php");
	
?>

<style>
	.form-group {
		margin-right: 0px !important;
		margin-left: 0px !important;
	}
</style>

<body class="bodymodal">
	
<? include("view/".$view); ?>
</body>
</html>
