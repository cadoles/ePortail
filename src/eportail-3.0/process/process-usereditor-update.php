<?php 
	// No direct access to this file 
	define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'); 
	if(!IS_AJAX) {die('Restricted access');}


	$keymodal	= $_GET['key'];
	$mode		= $_GET['mode'];
	$id			= $_GET['id'];
	$html		= $_GET['html'];

	if(!isset($_SESSION[$keymodal])) {
		die("$keymodal");
		exit();
	}
	
	// Récupérer le template de la page widget et le propriétaire de la page
	$q="SELECT panel_user FROM ".$config["dbprefixe"]."panel WHERE panel_id=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
	if($row=$query->fetch()) {
		$idpro=$row['panel_user'];
	}

	if($mode=="") {
		$q="SELECT panel_user FROM ".$config["dbprefixe"]."panel WHERE panel_id=?";
		$query=$bdd01->prepare($q);
		$query->execute(array($id));
		if($row=$query->fetch()) {
			$idpro=$row['panel_user'];
		}
	}
	elseif($mode=="widget") {
		$q="SELECT panel_user FROM ".$config["dbprefixe"]."panel, ".$config["dbprefixe"]."panel_widget WHERE panel_id=panel_widget_panel AND panel_widget_id=?";
		$query=$bdd01->prepare($q);
		$query->execute(array($id));
		if($row=$query->fetch()) {
			$idpro=$row['panel_user'];
		}
	}
	
	// Si l'utilisateur en cours est le propriétaire ou administrateur : il peut modifier la page
	if($idpro==$_SESSION['user_id']||$_SESSION['user_profil']==1) {
		if($mode=="") {
			$q="UPDATE ".$config["dbprefixe"]."panel SET panel_html=? WHERE panel_id=?";
			$query=$bdd01->prepare($q);
			$query->execute(array($html,$id));
		}
		elseif($mode=="widget") {
			$q="UPDATE ".$config["dbprefixe"]."panel_widget SET panel_widget_html=? WHERE panel_widget_id=?";
			$query=$bdd01->prepare($q);
			$query->execute(array($html,$id));
		}
	}
?>
