<?
	// No direct access to this file 
	define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'); 
	if(!IS_AJAX) {die('Restricted access');}

	$keymodal	= $_GET['key'];
	$idfeed=$_GET["id"];

	if(!isset($_SESSION[$keymodal])) {
		die("$keymodal");
		exit();
	}

	$fgperm=false;
	foreach ($tbflux as $flux) {
		if($flux[1]==$idfeed) $fgperm=true;
	}	
	
	if($fgperm) {
		$html=$html."<div class='rssBody'>";
		$q="SELECT * FROM ".$config["dbprefixe"]."flux WHERE flux_id=?";
		$query=$bdd01->prepare($q);
		$query->execute(array($idfeed));
		if($row=$query->fetch()){	
			$maxrss=$row['flux_number'];
			
			$q="SELECT * FROM ".$config["dbprefixe"]."fluxmsg WHERE fluxmsg_flux=? ORDER BY fluxmsg_date DESC LIMIT $maxrss";
			$query=$bdd01->prepare($q);
			$query->execute(array($idfeed));
			while($row=$query->fetch()){
				// Recherche d'une image dans le contenu HTML
				$str01=$row['fluxmsg_html'];
				$pos = strpos($str01, "<img");
				if($pos>=0) {
					$str02=substr($str01,$pos);
					$pos=strpos($str02,"src=\"");
					$str03=substr($str02,$pos+5);
					$pos=strpos($str03,"\"");
					$str04=substr($str03,0,$pos);
				}				
				
				$a="index.php?view=user/article.php&idflux=".$row['fluxmsg_id'];
				$html=$html."<div class='rssRow odd col-sm-12 column'>";
				$html=$html."<h4><a href='$a'>".$row['fluxmsg_name']."</a></h4>";
				if($str04!="") $html=$html."<div class='rssMedia'><a href='$a'><img src='".$str04."'></a></div>";
				$html=$html."<div class='rssDate'>".date("d/m/Y",strtotime($row["fluxmsg_date"]))."</div>";
				$html=$html."<div class='rssMore'><a class='btn btn-primary btn-sm' href='$a'>En savoir plus</a></div>";
				$html=$html."<p>".str_replace("\n","<br>",$row['fluxmsg_description'])."</p>";
				$html=$html."</div>";
			}
		}
		$html=$html."</div>";
	}
	
	print_r($html);
?>
