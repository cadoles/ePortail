<?
	// No direct access to this file 
	define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'); 
	if(!IS_AJAX) {die('Restricted access');}

	include_once("../include/config.php");
	include_once("../include/session.php");
		
	$key		=$_GET["key"];		// Clé de sécurité
	$idmodal	=$_GET["idmodal"];	// id de la modal
	$view		=$_GET["view"];		// iframe executée dans la modal
	$type		=$_GET["type"];		// type d'action executée dans la modal
	$id1		=$_GET["id1"];		// identifiant associé à l'action
	$id2		=$_GET["id2"];		// identifiant associé à l'action
	
	$_SESSION[$key]["idmodal"]	=$idmodal;
	$_SESSION[$key]["view"]		=$view;
	$_SESSION[$key]["type"]		=$type;
	$_SESSION[$key]["id1"]		=$id1;
	$_SESSION[$key]["id2"]		=$id2;
?>
