<?php 
	// No direct access to this file 
	define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'); 
	if(!IS_AJAX) {die('Restricted access');}
	
	$keymodal	= $_GET['key'];
	$id			= $_GET['id'];

	if(!isset($_SESSION[$keymodal])) {
		die("$keymodal");
		exit();
	}

	// Récupérer le template de la page widget et le propriétaire de la page
	$q="SELECT panel_user FROM ".$config["dbprefixe"]."panel WHERE panel_id=?";
	$query=$bdd01->prepare($q);
	$query->execute(array($id));
	if($row=$query->fetch()) {
		$idpro=$row['panel_user'];
	}
	
	// Si l'utilisateur en cours est le propriétaire ou administrateur : il peut modifier la page
	if($idpro==$_SESSION['user_id']||$_SESSION['user_profil']==1) {
		delPanel($id);
	}
?>
