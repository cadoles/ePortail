<?

/* LDAP */
%if %%activer_sso == "distant"
$config['activerLDAPModif']				= true;				/* Permettre la modification des paramétres LDAP 	*/
$config['activerLDAP']					= false;			/* Activer le mode LDAP								*/

$config['LDAPserver']					= "";
$config['LDAPserver']					= "";
$config['LDAPport'] 					= "";
$config['LDAPreaderdn'] 				= "";
$config['LDAPreaderpw']					= "";
$config['LDAPwriterdn'] 				= "";
$config['LDAPwriterpw']					= "";
$config['LDAPracine'] 					= "";
$config['LDAPfilterauth'] 				= "";
$config['LDAPfirstname'] 				= "";
$config['LDAPlastname'] 				= "";
$config['LDAPemail'] 					= "";

%else

$config['activerLDAPModif']				= false;			/* Permettre la modification des paramétres LDAP 	*/
$config['activerLDAP']					= true;				/* Activer le mode LDAP								*/

// Adresse du Serveur LDAP
%if(%%eolesso_ldap[0] in ("localhost","127.0.0.1"))
$config['LDAPserver']					= "%%adresse_ip_ldap";
%else
$config['LDAPserver']					= "%%eolesso_ldap[0]";
%end if
$config['LDAPport'] 					= "%%eolesso_ldap[0].eolesso_port_ldap";

$config['LDAPreaderdn'] 				= "%%eolesso_ldap[0].eolesso_ldap_reader";
$config['LDAPreaderpw']					= "%%pwdreader("",%%eolesso_ldap[0].eolesso_ldap_reader_passfile)";

$config['LDAPwriterdn'] 				= "";
$config['LDAPwriterpw']					= "";

$config['LDAPracine'] 					= "%%eolesso_ldap[0].eolesso_base_ldap";
$config['LDAPorganisation']     		= "ou=education";
$config['LDAPfilterauth'] 				= "uid=#login#";
$config['LDAPfirstname'] 				= "givenname";
$config['LDAPlastname'] 				= "sn";
$config['LDAPemail'] 					= "%%eolesso_ldap[0].eolesso_ldap_fill_mail";
%end if

if($config['activerLDAPModif']&&file_exists($config['localdirectory']."/local/config/ldap.php")) {
	include("local/config/ldap.php");
}


?>
