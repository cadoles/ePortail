<?
$config=Array();


/* Serveur */
$config["host"] 						= "%%web_url";
$config["urlbase"]						= "https://%%web_url/eportail";
$config["localdirectory"]				= "/var/www/html/eportail";
$config["alias"]						= "eportail";


/* Database */
$dbpassword="123456";
$config["dbhost"]						= "%%adresse_ip_mysql";
$config["dbname"] 						= "eportail";
$config["dblogin"]						= "eportail";
$config["dbpassword"]					= $dbpassword;
$config["dbprefixe"]					= "env_";


/* Connexion */
$config["modeAuthentification"]			= "CAS";						// Mode Authentification = CAS ou LDAP ou MYSQL
$config["modePrivate"]					= 'false';						// Mode privé
$config["modeRemember"]					= 'true';						// Activation de l'auto login via sauvegarde de session

/* Inscription */
$config["modeRegistration"]				= 'false';						// Activer l'inscription en ligne
$config["modeRegistrationConfirm"]		= 'direct';						// Validation de l'inscription direct email ou via validation admin
$config["modeRegistrationExterne"]		= '';							// Si module d'inscription externe mettre url du module
$config["modeProfilExterne"]			= '';							// Si module d'inscription externe mettre url du module
$config["modeAvatarExterne"]			= '';							// Si module d'inscription externe mettre url du module

/* Variables Globales */
$config["theme"]						= "";							// Nom du theme si vide theme par défaut
$config["title"]						= "%%libelle_etab";				// Titre du site
$config["sstitle"]						= "Site Envole";				// Sous-Titre du site
$config["locallogo"]   					= "";							// Logo personnalisé
$config["localheader"] 					= "";							// Bannière personnalisé
$config["modeCustomize"]				= 'true';						// Personnalisable par les utilisateur

/* Variables Synchronisation */
%if %%is_defined('activer_balado') and %%activer_balado == 'oui'
$config["synchro-balado"]				= 'true';
%end if
%if %%is_defined('activer_cdt') and %%activer_cdt == 'oui'
$config["synchro-cdt"]					= 'true';
%end if
%if %%is_defined('activer_iconito') and %%activer_iconito == 'oui'
$config["synchro-iconito"]				= 'true';
%end if
%if %%is_defined('activer_moodle') and %%activer_moodle == 'oui'
$config["synchro-moodle"]				= 'true';
%end if
%if %%is_defined('activer_wordpress') and %%activer_wordpress == 'oui'
$config["synchro-wordpress"]			= 'true';
%end if


/* Configuration locale */
if(file_exists($config["localdirectory"]."/local/config/config.php")) {
	include($config["localdirectory"]."/local/config/config.php");
}

$_SERVER["DOCUMENT_ROOT"] = $config["localdirectory"]."/";
?>
