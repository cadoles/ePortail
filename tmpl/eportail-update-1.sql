
\r eportail

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


-- Insertion des icones systèmes
INSERT IGNORE INTO `env_icon` (`icon_id`, `icon_url`) VALUES
(-1001, '00-envole001.jpg'),
(-1002, '00-envole002.jpg'),
(-1003, '00-envole003.jpg'),
(-1004, '00-envole004.png'),
(-1005, '00-envole005.png'),
(-1006, '00-envole006.png'),
(-1007, '00-envole007.png'),
(-1008, '00-envole008.png'),
(-1009, '00-envole009.png'),
(-1010, '00-envole010.png'),
(-1011, '00-envole011.png'),
(-1012, '00-envole012.png'),
(-1013, '00-envole013.png'),
(-1014, '00-envole014.png'),
(-1015, '00-envole015.png'),
(-1016, '00-envole016.png'),
(-1017, '00-envole017.png'),
(-1018, '00-envole018.png'),
(-1019, '00-envole019.png'),
(-1020, '00-envole020.png'),
(-1021, '00-envole021.png'),
(-1022, '00-envole022.png'),
(-1023, '00-envole023.png'),
(-1024, '00-envole024.png'),
(-1025, '00-envole025.png'),
(-1026, '00-envole026.png'),
(-1027, '00-envole027.png'),
(-1028, '00-envole028.png'),
(-1029, '00-envole029.png'),
(-1030, '00-envole030.png'),
(-1031, '00-envole031.png'),
(-1032, '00-envole032.png'),
(-1033, '00-envole033.png'),
(-1034, '00-envole034.png'),
(-1035, '00-envole035.png'),
(-1036, '00-envole036.png'),
(-1037, '00-envole037.jpg'),
(-1038, '00-envole038.jpg'),
(-1039, '00-envole039.jpg'),
(-1040, '00-envole040.jpg'),
(-1041, '00-envole041.png'),
(-1042, '00-envole042.png'),
(-1043, '00-envole043.jpg'),
(-1044, '00-envole044.png'),
(-1045, '00-envole045.png'),
(-1046, '00-envole046.png'),

(-500, '00-editeur.jpg'),
(-499, '00-rss.gif'),
(-498, '00-url.jpg'),
(-497, '00-owncloud.png'),
(-496, '00-phpmyadmin.png'),
(-495, '00-piwigo.png'),
(-494, '00-wordpress.png'),
(-493, '00-ead.png'),
(-492, '00-deviantart.png'),
(-491, '00-taskcalendar.png'),
(-490, '00-desktop.png'),
(-489, '00-dokuwiki.png'),
(-487, '00-roundcube.png'),
(-486, '00-ajaxplorer.png'),
(-485, '00-cdt.png'),
(-484, '00-cdc.png'),
(-483, '00-fluxbb.png'),
(-482, '00-gepi.png'),
(-481, '00-grr.png'),
(-480, '00-moodle.png'),
(-479, '00-spip.png'),
(-478, '00-taskfreak.png'),
(-477, '00-widget.jpg'),
(-476, '00-ethercalc.png'),
(-475, '00-carouseol.png'),
(-474, '00-opensondage.png'),
(-473, '00-etherpad.png'),
(-472, '00-bookmark.png'),
(-471, '00-mahara.png'),
(-470, '00-piwik.png'),
(-469, '00-evenement.png'),
(-468, '00-webcalendar.png'),
(-467, '00-balado.png'),
(-466, '00-econnect.png');

-- Insertion utilisateur système
INSERT IGNORE INTO `env_user` (`user_id`, `user_login`, `user_password`, `user_lastname`, `user_firstname`, `user_pseudo`, `user_avatar`, `user_email`, `user_profil`) VALUES
(-1, 'system', 'system', 'system', 'system', 'system', 'system.jpg', '', 1),
(1, 'admin', MD5('admin'), 'admin', 'admin', 'admin', 'admin.jpg', 'admin@mail.com', 1);


-- Insertion ldap_community système
INSERT IGNORE INTO `env_ldap_community` (`ldap_community_id`, `ldap_community_label`, `ldap_community_filter`) VALUES
(-1, 'Tout le monde', '');


-- Insertion ldap_profil système
INSERT IGNORE INTO `env_ldap_profil` (`ldap_profil_id`, `ldap_profil_label`, `ldap_profil_filter`, `ldap_profil_ldap_community`) VALUES
(-500, 'Administrateur', '(&(uid=admin)(uid=#login#))', -1),
(-499, 'Elèves', '(&(uid=#login#)(ENTPersonProfils=eleve))', -1),
(-498, 'Enseignants', '(&(uid=#login#)(ENTPersonProfils=enseignant))', -1),
(-497, 'Responsables', '(&(uid=#login#)(ENTPersonProfils=responsable))', -1),
(-496, 'Administratifs', '(&(uid=#login#)(ENTPersonProfils=administratif))', -1);


-- Insertion sso_community système
INSERT IGNORE INTO `env_sso_community` (`sso_community_id`, `sso_community_label`) VALUES
(-1, 'Tout le monde');


-- Insertion des panel système
INSERT IGNORE INTO `env_panel` (`panel_id`, `panel_label`, `panel_url`, `panel_order`, `panel_user`, `panel_type`, `panel_template`, `panel_html`) VALUES
(-500, 'Envole', 'view/envole/index.php	', 1, -1, 1, NULL, ''),
(-499, 'Applications', 'view/user/desktop.php', 2, -1, 2, NULL, '');


-- Insertion des templates système
INSERT IGNORE INTO `env_panel_template` (`panel_template_id`, `panel_template_order`, `panel_template_icon`, `panel_template_html`) VALUES
(1, 1, 'colx04x04x04.png', '<div id="C1R1" class="col-md-4 column">  \r\n<div width="100%">&nbsp;</div>\r\n</div>  \r\n  \r\n<div id="C2R1" class="col-md-4 column">  \r\n<div width="100%">&nbsp;</div>\r\n</div>  \r\n  \r\n<div id="C3R1" class="col-md-4 column">  \r\n<div width="100%">&nbsp;</div>\r\n</div>'),
(2, 2, 'colx06x06.png', '<div id="C1R1" class="col-md-6 column">  \r\n<div width="100%">&nbsp;</div>\r\n</div>  \r\n\r\n<div id="C2R1" class="col-md-6 column">  \r\n<div width="100%">&nbsp;</div>\r\n</div>  \r\n'),
(3, 3, 'colx08x04.png', '<div id="C1R1" class="col-md-8 column">  \r\n<div width="100%">&nbsp;</div>\r\n</div>  \r\n\r\n<div id="C2R1" class="col-md-4 column">  \r\n<div width="100%">&nbsp;</div>\r\n</div>  \r\n'),
(4, 4, 'colx12x04x04x04.png', '<div id="C1R1" class="col-md-12 column">  \r\n<div width="100%">&nbsp;</div>\r\n</div>  \r\n\r\n<div id="C2R1" class="col-md-4 column">  \r\n<div width="100%">&nbsp;</div>\r\n</div>  \r\n\r\n<div id="C3R1" class="col-md-4 column">  \r\n<div width="100%">&nbsp;</div>\r\n</div>\r\n\r\n<div id="C4R1" class="col-md-4 column">  \r\n<div width="100%">&nbsp;</div>\r\n</div>  '),
(5, 5, 'colx12x06x06.png', '<div id="C1R1" class="col-md-12 column">  \r\n<div width="100%">&nbsp;</div>\r\n</div>  \r\n\r\n<div id="C2R1" class="col-md-6 column">  \r\n<div width="100%">&nbsp;</div>\r\n</div>  \r\n\r\n<div id="C3R1" class="col-md-6 column">  \r\n<div width="100%">&nbsp;</div>\r\n</div>  \r\n');


-- Insertion des profils base système
INSERT IGNORE INTO `env_profil` (`profil_id`, `profil_label`) VALUES
(1, 'administrateur'),
(2, 'moderateur'),
(50, 'utilisateur'),
(99, 'invite');


-- Insertion mode d'insertion widget système
INSERT IGNORE INTO `env_widget_mode` (`widget_mode_id`, `widget_mode_name`) VALUES
(1, 'iFrame'),
(2, 'Div');


-- Insertion style de widget système
INSERT IGNORE INTO `env_widget_style` (`widget_style_id`, `widget_style_label`) VALUES
(1, 'Avec Bordure'),
(2, 'Sans Bordure'),
(3, 'Post-it');


-- Insertion tye de widget système
INSERT IGNORE INTO `env_widget_type` (`widget_type_id`, `widget_type_name`) VALUES
(1, 'Page Web'),
(2, 'Flux RSS'),
(3, 'Page Editeur'),
(4, 'Code'),
(5, 'Bureau'),
(6, 'Widget eportail');

-- Insertion style de flux système
INSERT IGNORE INTO `env_flux_style` (`flux_style_id`, `flux_style_label`) VALUES
(1, 'Ligne'),
(2, 'Colonne');


-- Insertion type de panel système
INSERT IGNORE INTO `env_panel_type` (`panel_type_id`, `panel_type_name`) VALUES
(1, 'url'),
(2, 'application'),
(3, 'editeur'),
(4, 'widget');


-- Insertion type de panel système
INSERT IGNORE INTO `env_application_categorie` (`application_categorie_id`, `application_categorie_label`, `application_categorie_order`, `application_categorie_color`) VALUES
(-100, 'Applications', 1, '03C9A9'),
(-99, 'Adminsitration', 2, '663399');


-- Insertion widget système
INSERT IGNORE INTO `env_widget` (`widget_id`, `widget_name`, `widget_label`, `widget_icon`, `widget_url`, `widget_code`, `widget_adjust`, `widget_height`, `widget_style`, `widget_type`, `widget_mode`, `widget_categorie`) VALUES
(-500, 'Page Web', 'Afficher une page web', '-498', '', '', 0, 300, 1, 1, 1, 0),
(-499, 'Flux RSS', 'Afficher le contenu d''un flux RSS provenant d''un autre site', '-499', '', '', 0, 300, 1, 2, 1, 0),
(-498, 'Page Editeur', 'Votre propre texte à éditer', '-500', 'view/user/editor.php', '', 0, 615, 1, 3, 1, 0),
(-497, 'Bureau', 'Votre Bureau', '-490', 'view/user/desktop.php', '', 0, 615, 1, 5, 1, 0),
(-496, 'Carousel', 'Carrousel', '-475', 'view/user/carousel.php', '', 0, 300, 1, 6, 1, 0),
(-495, 'Favoris', 'Favoris', '-472', 'view/user/bookmark.php', '', 0, 300, 1, 6, 1, 0),
(-464, 'Flux Profils', 'Afficher de flux RSS en fonction de votre profil', '-499', 'view/user/flux.php', '', 0, 615, 1, 1, 1, 0);

-- Insertion flux système
INSERT IGNORE INTO `env_flux` (`flux_id`, `flux_name`, `flux_type`, `flux_url`, `flux_order`, `flux_number`, `flux_color`, `flux_style`) VALUES
(-500, 'Envole', 0, 'http://dev-eole.ac-dijon.fr/projects/envole/news.atom?key=0c3837f9fcaf2704056f8915c92c9dfb3d27579c', 1, 5, '03C9A9',1);

-- Insertion wordpress
%if %%is_defined('activer_wordpress') and %%activer_wordpress == 'oui'
	INSERT IGNORE INTO `env_widget` (`widget_id`, `widget_name`, `widget_label`, `widget_icon`, `widget_url`, `widget_code`, `widget_height`, `widget_style`, `widget_type`, `widget_mode`, `widget_categorie`) VALUES
	(-400, 'Wordpress', 'Les derniers articles publiés sur le Wordpress d''Envole', '-494', '/wordpress/wp-content/plugins/poshwidget/wordpresswidget.php?format=light&appli=eportail', '', 300, 1, 1, 1, 0);

	INSERT IGNORE INTO `env_application` (`application_id`, `application_name`, `application_label`, `application_description`, `application_order`, `application_url`, `application_open`, `application_icon`, `application_categorie`) VALUES
	(-400, 'wordpress', 'Wordpress', 'WordPress est le nec plus ultra des plates-formes sémantiques de publication personnelle, alliant esthétique, standards du Web et utilisabilité.', 3, '%2Fwordpress', 0, '-494', -100);
%end if


-- Insertion dokuwiki
%if %%is_defined('activer_dokuwiki') and %%activer_dokuwiki == 'oui'
	INSERT IGNORE INTO `env_widget` (`widget_id`, `widget_name`, `widget_label`, `widget_icon`, `widget_url`, `widget_code`, `widget_height`, `widget_style`, `widget_type`, `widget_mode`, `widget_categorie`) VALUES
	(-399, 'Dokuwiki', 'DokuWiki est un Wiki, conforme aux standards, simple à utiliser, dont le but est de créer des documentations de toute sorte.', '-489', '/dokuwiki/widgetdokuwiki.php', '', 300, 1, 1, 1, 0);

	INSERT IGNORE INTO `env_application` (`application_id`, `application_name`, `application_label`, `application_description`, `application_order`, `application_url`, `application_open`, `application_icon`, `application_categorie`) VALUES
	(-399, 'dokuwiki', 'Dokuwiki', 'DokuWiki est un Wiki, conforme aux standards, simple à utiliser, dont le but est de créer des documentations de toute sorte.', 5, '%2Fdokuwiki', 0, '-489', -100);
%end if


-- Insertion roundcube
%if %%is_defined('activer_roundcube') and %%activer_roundcube == 'oui'
	INSERT IGNORE INTO `env_widget` (`widget_id`, `widget_name`, `widget_label`, `widget_icon`, `widget_url`, `widget_code`, `widget_height`, `widget_style`, `widget_type`, `widget_mode`, `widget_categorie`) VALUES
	(-398, 'Roudcube', 'Roundcube est un client webmail pour le protocole IMAP écrit en PHP.', '-487', '/roundcube/?_task=envole&_action=mini', '', 300, 1, 1, 1, 0);

	INSERT IGNORE INTO `env_application` (`application_id`, `application_name`, `application_label`, `application_description`, `application_order`, `application_url`, `application_open`, `application_icon`, `application_categorie`) VALUES
	(-398, 'roundcube', 'Roundcube', 'Roundcube est un client webmail pour le protocole IMAP écrit en PHP.', 6, '%2Froundcube', 0, '-487', -100);
%end if


-- Insertion fluxbb
%if %%is_defined('activer_fluxbb') and %%activer_roundcube == 'oui'
	INSERT IGNORE INTO `env_widget` (`widget_id`, `widget_name`, `widget_label`, `widget_icon`, `widget_url`, `widget_code`, `widget_height`, `widget_style`, `widget_type`, `widget_mode`, `widget_categorie`) VALUES
	(-397, 'Fluxbb', 'FluxBB est un forum de discussions écrit en PHP rapide et léger.', '-483', '/fluxbb/extern.php?eportail=true', '', 300, 1, 1, 1, 0);
	
	INSERT IGNORE INTO `env_application` (`application_id`, `application_name`, `application_label`, `application_description`, `application_order`, `application_url`, `application_open`, `application_icon`, `application_categorie`) VALUES
	(-397, 'fluxbb', 'Fluxbb', 'FluxBB est un forum de discussions écrit en PHP rapide et léger.', 10, '%2Ffluxbb', 0, '-483', -100);
%end if


-- Insertion piwigo
%if %%is_defined('activer_piwigo') and %%activer_piwigo == 'oui'
	INSERT IGNORE INTO `env_widget` (`widget_id`, `widget_name`, `widget_label`, `widget_icon`, `widget_url`, `widget_code`, `widget_height`, `widget_style`, `widget_type`, `widget_mode`, `widget_categorie`) VALUES
	(-396, 'Piwigo', 'Piwigo est un logiciel de galerie photo pour le web', '-495', '/piwigo/feedhtml.php?appli=eportail', '', 300, 1, 1, 1, 0);

	INSERT IGNORE INTO `env_application` (`application_id`, `application_name`, `application_label`, `application_description`, `application_order`, `application_url`, `application_open`, `application_icon`, `application_categorie`) VALUES
	(-396, 'piwigo', 'Piwigo', 'Piwigo est un logiciel de galerie photo pour le web', 2, '%2Fpiwigo', 0, '-495', -100);
%end if


-- Insertion owncloud
%if %%is_defined('activer_owncloud') and %%activer_owncloud == 'oui'
	INSERT IGNORE INTO `env_application` (`application_id`, `application_name`, `application_label`, `application_description`, `application_order`, `application_url`, `application_open`, `application_icon`, `application_categorie`) VALUES
	(-395, 'owncloud', 'Owncloud', 'ownCloud est une implémentation open source de services en ligne de stockage', 1, '%2Fowncloud', 0, '-497', -100);
%end if


-- Insertion phpmyadmin
%if %%is_defined('activer_phpmyadmin') and %%activer_phpmyadmin == 'oui'
	INSERT IGNORE INTO `env_application` (`application_id`, `application_name`, `application_label`, `application_description`, `application_order`, `application_url`, `application_open`, `application_icon`, `application_categorie`) VALUES
	(-394, 'phpmyadmin', 'phpMyAdmin', 'phpMyAdmin peut gérer un serveur MySQL', 1, '%2Fphpmyadmin', 1, '-496', -99);
%end if


-- Insertion ead
%if %%is_defined('activer_ead_web') and %%activer_ead_web == 'oui'
	INSERT IGNORE INTO `env_application` (`application_id`, `application_name`, `application_label`, `application_description`, `application_order`, `application_url`, `application_open`, `application_icon`, `application_categorie`) VALUES
	(-393, 'ead', 'EAD', 'Console d''administration du serveur scribe', 2, '', 0, '-493', -99);

	%if %%is_defined('activer_ead_reverseproxy') and %%activer_ead_reverseproxy == 'oui'
		UPDATE `env_application` SET `application_url`='https://%%web_url:%%port_ead_reverseproxy/connect/?server=1' WHERE `application_id`='-393';
	%else
		UPDATE `env_application` SET `application_url`='https://%%web_url:4200/connect/?server=1' WHERE `application_id`='-393';
	%end if
%end if


-- Insertion ajaxplorer
%if %%is_defined('activer_ajaxplorer') and %%activer_ajaxplorer == 'oui'
	INSERT IGNORE INTO `env_application` (`application_id`, `application_name`, `application_label`, `application_description`, `application_order`, `application_url`, `application_open`, `application_icon`, `application_categorie`) VALUES
	(-392, 'ajaxplorer', 'Ajaxplorer', 'Ajaxplorer est un gestionnaire de fichier en ligne.', 7, '%2Fajaxplorer', 0, '-486', -100);
%end if


-- Insertion cdt
%if %%is_defined('activer_cdt') and %%activer_cdt == 'oui'
	INSERT IGNORE INTO `env_widget` (`widget_id`, `widget_name`, `widget_label`, `widget_icon`, `widget_url`, `widget_code`, `widget_height`, `widget_style`, `widget_type`, `widget_mode`, `widget_categorie`) VALUES
	(-391, 'CdT', 'Application complète de gestion de cahier de texte scolaire', '-485', '/cdt/widgetCDT.php', '', 615, 1, 1, 1, 0);

	INSERT IGNORE INTO `env_application` (`application_id`, `application_name`, `application_label`, `application_description`, `application_order`, `application_url`, `application_open`, `application_icon`, `application_categorie`) VALUES
	(-391, 'cdt', 'CDT', 'Application complète de gestion de cahier de texte scolaire', 8, '%2Fcdt', 0, '-485', -100);
%end if


-- Insertion cdc
%if %%is_defined('activer_cdc') and %%activer_cdc == 'oui'
	INSERT IGNORE INTO `env_application` (`application_id`, `application_name`, `application_label`, `application_description`, `application_order`, `application_url`, `application_open`, `application_icon`, `application_categorie`) VALUES
	(-390, 'cdc', 'CDC', 'Application web CDC (Carnet de Correspondance) : Une version électronique du carnet de correspondance.', 9, '%2Fcdc', 0, '-484', -100);
%end if


-- Insertion gepi
%if %%is_defined('activer_gepi') and %%activer_gepi == 'oui'
	INSERT IGNORE INTO `env_application` (`application_id`, `application_name`, `application_label`, `application_description`, `application_order`, `application_url`, `application_open`, `application_icon`, `application_categorie`) VALUES
	(-389, 'gepi', 'GEPI', 'Intégration du logiciel de gestion des notes, absences, cahier de texte GEPI avec authentification automatique.', 11, '%2Fgepi', 0, '-482', -100);
%end if


-- Insertion grr
%if %%is_defined('activer_grr') and %%activer_grr == 'oui'
	INSERT IGNORE INTO `env_application` (`application_id`, `application_name`, `application_label`, `application_description`, `application_order`, `application_url`, `application_open`, `application_icon`, `application_categorie`) VALUES
	(-388, 'grr', 'GRR', 'GRR est un système de Gestion et de Réservations de Ressources. GRR est particulièrement adapté à la gestion et la réservation de salles et de matériels.', 12, '%2Fgrr', 0, '-481', -100);
%end if


-- Insertion moodle
%if %%is_defined('activer_moodle') and %%activer_moodle == 'oui'
	INSERT IGNORE INTO `env_application` (`application_id`, `application_name`, `application_label`, `application_description`, `application_order`, `application_url`, `application_open`, `application_icon`, `application_categorie`) VALUES
	(-387, 'moodle', 'Moodle', 'Moodle est une plateforme d’apprentissage en ligne (en anglais : e-learning) sous licence libre servant à créer des communautés s’instruisant autour de contenus et d’activités pédagogiques', 13, '%2Fmoodle', 0, '-480', -100);
%end if


-- Insertion spip
%if %%is_defined('activer_spip') and %%activer_spip == 'oui'
	INSERT IGNORE INTO `env_widget` (`widget_id`, `widget_name`, `widget_label`, `widget_icon`, `widget_url`, `widget_code`, `widget_height`, `widget_style`, `widget_type`, `widget_mode`, `widget_categorie`) VALUES
	(-386, 'Spip', 'Affichage des derniers articles de Spip', '-479', '/spip/index.php?page=widget', '', 300, 1, 1, 1, 0);

	INSERT IGNORE INTO `env_application` (`application_id`, `application_name`, `application_label`, `application_description`, `application_order`, `application_url`, `application_open`, `application_icon`, `application_categorie`) VALUES
	(-386, 'spip', 'Spip', 'SPIP est un Système de Publication pour l’Internet.', 14, '%2Fspip', 0, '-479', -100);
%end if


-- Insertion taskfreak
%if %%is_defined('activer_taskfreak') and %%activer_taskfreak == 'oui'
	INSERT IGNORE INTO `env_application` (`application_id`, `application_name`, `application_label`, `application_description`, `application_order`, `application_url`, `application_open`, `application_icon`, `application_categorie`) VALUES
	(-385, 'taskfreak', 'Taskfreak', 'Gestion des taches et des priorités. Il permet de créer des tâches au sein d’une interface simplissime et ergonomique. Selon le niveau de permission de l’utilisateur, celui-ci peut créer de nouvelles tâches, de nouveaux projets, de nouveaux utilisateurs. Il permettra une gestion de projet simple ou servira de pense-bête.', 15, '%2Ftaskfreak', 0, '-478', -100);
%end if


-- Insertion ethercalc
%if %%is_defined('activer_ethercalc') and %%activer_ethercalc == 'oui'
	INSERT IGNORE INTO `env_application` (`application_id`, `application_name`, `application_label`, `application_description`, `application_order`, `application_url`, `application_open`, `application_icon`, `application_categorie`) VALUES
	(-384, 'ethercalc', 'Ethercalc', 'En plus des fonctionnalités de base de n\'importe quel tableur dont vous pourriez avoir besoin, Ethercalc résout le problème du travail collaboratif sur une même feuille de calcul.', 16, '%2Fethercalc%2F', 0, '-476', -100);
%end if


-- Insertion opensondage
%if %%is_defined('activer_ethercalc') and %%activer_ethercalc == 'oui'
	INSERT IGNORE INTO `env_application` (`application_id`, `application_name`, `application_label`, `application_description`, `application_order`, `application_url`, `application_open`, `application_icon`, `application_categorie`) VALUES
	(-383, 'opensondage', 'OpenSondage', 'Organiser des rendez-vous simplement, librement.', 17, '%2Fopensondage', 0, '-474', -100);
%end if


-- Insertion etherpad
%if %%is_defined('activer_etherpad') and %%activer_etherpad == 'oui'
	INSERT IGNORE INTO `env_application` (`application_id`, `application_name`, `application_label`, `application_description`, `application_order`, `application_url`, `application_open`, `application_icon`, `application_categorie`) VALUES
	(-382, 'etherpad', 'Etherpad', 'Un pad est un éditeur de texte collaboratif en ligne. Son vrai plus ? Les contributions de chaque utilisateur apparaissent immédiatement dans les pads de tous les participants, signalées par un code couleur.', 18, '%2Fetherpad%2F', 0, '-473', -100);
%end if


-- Insertion mahara
%if %%is_defined('activer_mahara') and %%activer_mahara == 'oui'
	INSERT IGNORE INTO `env_application` (`application_id`, `application_name`, `application_label`, `application_description`, `application_order`, `application_url`, `application_open`, `application_icon`, `application_categorie`) VALUES
	(-381, 'mahara', 'Mahara', 'Mahara c\’est deux choses différentes : c\’est avant tout un système de gestion d\’ePortfolios, mais aussi d\’un système de réseau social, combinés. Un système de gestion d\’ePortfolios est un système qui permet aux étudiants de collecter et ordonnancer leurs preuves d\’apprentissage tout au long de la vie', 19, '%2Fmahara', 0, '-471', -100);
%end if


-- Insertion piwik
%if %%is_defined('activer_piwik') and %%activer_piwik == 'oui'
	INSERT IGNORE INTO `env_application` (`application_id`, `application_name`, `application_label`, `application_description`, `application_order`, `application_url`, `application_open`, `application_icon`, `application_categorie`) VALUES
	(-380, 'piwik', 'Piwk', 'Un tableau de bord personnalisable permet d\’avoir les principales statistiques en un coup d’oeil : de très nombreux widgets peuvent y être ajoutés, déplacés ou supprimés. Les statistiques détaillées sont aussi disponibles et sont réparties en quatre catégories : Visiteurs, Actions, Référents et Objectifs.', 20, '%2Fpiwik', 0, '-470', -99);
%end if


-- Insertion evénement
%if %%is_defined('activer_calendrier') and %%activer_calendrier == 'oui'
	INSERT IGNORE INTO `env_widget` (`widget_id`, `widget_name`, `widget_label`, `widget_icon`, `widget_url`, `widget_code`, `widget_height`, `widget_style`, `widget_type`, `widget_mode`, `widget_categorie`) VALUES
	(-379, 'Evènement', 'Affichage des Evènements', '-469', '/calendrier/widgetcalendrier.php', '', 300, 1, 1, 1, 0);
	
	INSERT IGNORE INTO `env_application` (`application_id`, `application_name`, `application_label`, `application_description`, `application_order`, `application_url`, `application_open`, `application_icon`, `application_categorie`) VALUES
	(-379, 'evenement', 'Evènement', 'Gestion des évènements.', 21, '%2Fcalendrier', 0, '-469', -100);
%end if


-- Insertion webcalendar
%if %%is_defined('activer_webcalendar') and %%activer_webcalendar == 'oui'
	INSERT IGNORE INTO `env_widget` (`widget_id`, `widget_name`, `widget_label`, `widget_icon`, `widget_url`, `widget_code`, `widget_height`, `widget_style`, `widget_type`, `widget_mode`, `widget_categorie`) VALUES
	(-378, 'Webcalendar', 'Affichage de votre calendrier', '-468', '/webcalendar/rss.php?format=html&max=4', '', 300, 1, 1, 1, 0);

	INSERT IGNORE INTO `env_application` (`application_id`, `application_name`, `application_label`, `application_description`, `application_order`, `application_url`, `application_open`, `application_icon`, `application_categorie`) VALUES
	(-378, 'webcalendar', 'Webcalendar', 'Cette application permet la gestion des emploi du temps ou des projets spécifiques. Elle peut être synchronisée sur l\’application Cahier de texte.', 22, '%2Fwebcalendar', 0, '-468', -100);
%end if


-- Insertion balado
%if %%is_defined('activer_balado') and %%activer_balado == 'oui'
	INSERT IGNORE INTO `env_application` (`application_id`, `application_name`, `application_label`, `application_description`, `application_order`, `application_url`, `application_open`, `application_icon`, `application_categorie`) VALUES
	(-377, 'balado', 'Balado', 'Dans le domaine éducatif, l\’espace Balad((O)) de l\’académie de Créteil permet de s\’enregistrer directement en ligne et de partager ses enregistrements.', 23, '%2Fbalado', 0, '-467', -100);
%end if


-- Inserton econnect
%if %%is_defined('activer_econnect') and %%activer_econnect == 'oui'
	INSERT IGNORE INTO `env_application` (`application_id`, `application_name`, `application_label`, `application_description`, `application_order`, `application_url`, `application_open`, `application_icon`, `application_categorie`) VALUES
	(-376, 'econnect', 'eConnect', 'Gestion de connecteurs Envole', 24, '%2Feconnect', 0, '-466', -99);
%end if


-- Init profil


-- Mise à jour de la version applicative
INSERT IGNORE INTO `env_config` (`config_appli`, `config_key`, `config_value`) VALUES ('core', 'version', '3.0');

