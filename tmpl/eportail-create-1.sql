-- création de la base de donnée
CREATE DATABASE eportail CHARACTER SET utf8;

-- Creation utilisateur
grant all privileges on eportail.* to eportail@%%adresse_ip_web identified by 'eportail';
flush privileges ;

-- connexion à la base
\r eportail
SET character_set_client = utf8;




-- phpMyAdmin SQL Dump
-- version 3.3.2deb1ubuntu1
-- http://www.phpmyadmin.net
--
-- Serveur: 127.0.0.1
-- Généré le : Lun 15 Décembre 2014 à 15:16
-- Version du serveur: 5.1.73
-- Version de PHP: 5.3.2-1ubuntu4.28

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `eportail`
--

-- --------------------------------------------------------

--
-- Structure de la table `env_application`
--

CREATE TABLE IF NOT EXISTS `env_application` (
  `application_id` int(11) NOT NULL AUTO_INCREMENT,
  `application_name` varchar(255) NOT NULL,
  `application_label` varchar(255) NOT NULL,
  `application_description` longtext NOT NULL,
  `application_order` int(11) NOT NULL,
  `application_url` varchar(255) NOT NULL,
  `application_open` int(11) NOT NULL,
  `application_color` varchar(10) NOT NULL,
  `application_icon` varchar(255) NOT NULL,
  `application_categorie` int(11) NOT NULL,
  PRIMARY KEY (`application_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

-- --------------------------------------------------------

--
-- Structure de la table `env_application_categorie`
--

CREATE TABLE IF NOT EXISTS `env_application_categorie` (
  `application_categorie_id` int(11) NOT NULL AUTO_INCREMENT,
  `application_categorie_label` varchar(255) NOT NULL,
  `application_categorie_order` int(11) NOT NULL,
  `application_categorie_color` varchar(10) NOT NULL,
  PRIMARY KEY (`application_categorie_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Structure de la table `env_application_ldap_profil`
--

CREATE TABLE IF NOT EXISTS `env_application_ldap_profil` (
  `application_ldap_profil_application` int(11) NOT NULL,
  `application_ldap_profil_ldap_profil` int(11) NOT NULL,
  UNIQUE KEY `appprofil_application_id` (`application_ldap_profil_application`,`application_ldap_profil_ldap_profil`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `env_application_profil`
--

CREATE TABLE IF NOT EXISTS `env_application_profil` (
  `application_profil_application` int(11) NOT NULL,
  `application_profil_profil` int(11) NOT NULL,
  UNIQUE KEY `appprofil_application_id` (`application_profil_application`,`application_profil_profil`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `env_application_sso_profil`
--

CREATE TABLE IF NOT EXISTS `env_application_sso_profil` (
  `application_sso_profil_application` int(11) NOT NULL,
  `application_sso_profil_sso_profil` int(11) NOT NULL,
  UNIQUE KEY `appprofil_application_id` (`application_sso_profil_application`,`application_sso_profil_sso_profil`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `env_config`
--

CREATE TABLE IF NOT EXISTS `env_config` (
  `config_appli` varchar(25) NOT NULL,
  `config_key` varchar(25) NOT NULL,
  `config_value` varchar(255) NOT NULL,
  UNIQUE KEY `config_appli` (`config_appli`,`config_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `env_flux`
--

CREATE TABLE IF NOT EXISTS `env_flux` (
  `flux_id` int(11) NOT NULL AUTO_INCREMENT,
  `flux_name` varchar(255) NOT NULL,
  `flux_type` int(11) NOT NULL,
  `flux_url` varchar(255) NOT NULL,
  `flux_order` int(11) NOT NULL,
  `flux_number` int(11) NOT NULL,
  `flux_color` varchar(10) NOT NULL,
  `flux_style` int(11) NOT NULL,
  PRIMARY KEY (`flux_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Structure de la table `env_fluxmsg`
--

CREATE TABLE IF NOT EXISTS `env_fluxmsg` (
  `fluxmsg_id` int(11) NOT NULL AUTO_INCREMENT,
  `fluxmsg_name` varchar(255) NOT NULL,
  `fluxmsg_date` date NOT NULL,
  `fluxmsg_description` longtext NOT NULL,
  `fluxmsg_html` longtext NOT NULL,
  `fluxmsg_flux` int(11) NOT NULL,
  PRIMARY KEY (`fluxmsg_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Structure de la table `env_flux_ldap_profil`
--

CREATE TABLE IF NOT EXISTS `env_flux_ldap_profil` (
  `flux_ldap_profil_flux` int(11) NOT NULL,
  `flux_ldap_profil_profil` int(11) NOT NULL,
  PRIMARY KEY (`flux_ldap_profil_flux`,`flux_ldap_profil_profil`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `env_flux_profil`
--

CREATE TABLE IF NOT EXISTS `env_flux_profil` (
  `flux_profil_flux` int(11) NOT NULL,
  `flux_profil_profil` int(11) NOT NULL,
  PRIMARY KEY (`flux_profil_flux`,`flux_profil_profil`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `env_flux_sso_profil`
--

CREATE TABLE IF NOT EXISTS `env_flux_sso_profil` (
  `flux_sso_profil_flux` int(11) NOT NULL,
  `flux_sso_profil_profil` int(11) NOT NULL,
  PRIMARY KEY (`flux_sso_profil_flux`,`flux_sso_profil_profil`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `env_flux_style`
--

CREATE TABLE IF NOT EXISTS `env_flux_style` (
  `flux_style_id` int(11) NOT NULL AUTO_INCREMENT,
  `flux_style_label` varchar(255) NOT NULL,
  PRIMARY KEY (`flux_style_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Structure de la table `env_group`
--

CREATE TABLE IF NOT EXISTS `env_group` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(255) NOT NULL,
  `group_description` longtext NOT NULL,
  `group_avatar` varchar(255) NOT NULL,
  `group_mode` int(11) NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `env_group_user`
--

CREATE TABLE IF NOT EXISTS `env_group_user` (
  `group_user_group` int(11) NOT NULL,
  `group_user_user` int(11) NOT NULL,
  `group_user_profil` int(11) NOT NULL,
  PRIMARY KEY (`group_user_group`,`group_user_user`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `env_icon`
--

CREATE TABLE IF NOT EXISTS `env_icon` (
  `icon_id` int(11) NOT NULL AUTO_INCREMENT,
  `icon_url` varchar(255) NOT NULL,
  PRIMARY KEY (`icon_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

-- --------------------------------------------------------

--
-- Structure de la table `env_inscription`
--

CREATE TABLE IF NOT EXISTS `env_inscription` (
  `inscription_id` int(11) NOT NULL AUTO_INCREMENT,
  `inscription_login` varchar(250) NOT NULL,
  `inscription_password` varchar(250) NOT NULL,
  `inscription_lastname` varchar(250) NOT NULL,
  `inscription_firstname` varchar(250) NOT NULL,
  `inscription_pseudo` varchar(255) NOT NULL,
  `inscription_token` varchar(250) NOT NULL,
  `inscription_email` varchar(250) NOT NULL,
  `inscription_profil` int(11) NOT NULL,
  `inscription_date` date NOT NULL,
  PRIMARY KEY (`inscription_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `env_ldap_community`
--

CREATE TABLE IF NOT EXISTS `env_ldap_community` (
  `ldap_community_id` int(11) NOT NULL AUTO_INCREMENT,
  `ldap_community_label` varchar(255) NOT NULL,
  `ldap_community_filter` varchar(255) NOT NULL,
  PRIMARY KEY (`ldap_community_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Structure de la table `env_ldap_profil`
--

CREATE TABLE IF NOT EXISTS `env_ldap_profil` (
  `ldap_profil_id` int(11) NOT NULL AUTO_INCREMENT,
  `ldap_profil_label` varchar(255) NOT NULL,
  `ldap_profil_filter` varchar(255) NOT NULL,
  `ldap_profil_ldap_community` int(11) NOT NULL,
  PRIMARY KEY (`ldap_profil_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Structure de la table `env_panel`
--

CREATE TABLE IF NOT EXISTS `env_panel` (
  `panel_id` int(11) NOT NULL AUTO_INCREMENT,
  `panel_label` varchar(250) NOT NULL,
  `panel_url` varchar(250) NOT NULL,
  `panel_order` int(11) NOT NULL,
  `panel_user` int(11) NOT NULL,
  `panel_type` int(11) NOT NULL,
  `panel_template` int(11) DEFAULT NULL,
  `panel_html` longtext NOT NULL,
  PRIMARY KEY (`panel_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

-- --------------------------------------------------------

--
-- Structure de la table `env_panel_ldap_profil`
--

CREATE TABLE IF NOT EXISTS `env_panel_ldap_profil` (
  `panel_ldap_profil_panel` int(11) NOT NULL,
  `panel_ldap_profil_ldap_profil` int(11) NOT NULL,
  UNIQUE KEY `panel_profil_panel_id` (`panel_ldap_profil_panel`,`panel_ldap_profil_ldap_profil`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `env_panel_profil`
--

CREATE TABLE IF NOT EXISTS `env_panel_profil` (
  `panel_profil_panel` int(11) NOT NULL,
  `panel_profil_profil` int(11) NOT NULL,
  UNIQUE KEY `panel_profil_panel_id` (`panel_profil_panel`,`panel_profil_profil`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `env_panel_sso_profil`
--

CREATE TABLE IF NOT EXISTS `env_panel_sso_profil` (
  `panel_sso_profil_panel` int(11) NOT NULL,
  `panel_sso_profil_sso_profil` int(11) NOT NULL,
  UNIQUE KEY `panel_profil_panel_id` (`panel_sso_profil_panel`,`panel_sso_profil_sso_profil`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `env_panel_template`
--

CREATE TABLE IF NOT EXISTS `env_panel_template` (
  `panel_template_id` int(11) NOT NULL AUTO_INCREMENT,
  `panel_template_order` int(11) NOT NULL,
  `panel_template_icon` varchar(250) NOT NULL,
  `panel_template_html` longtext NOT NULL,
  PRIMARY KEY (`panel_template_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Structure de la table `env_panel_type`
--

CREATE TABLE IF NOT EXISTS `env_panel_type` (
  `panel_type_id` int(11) NOT NULL,
  `panel_type_name` varchar(255) NOT NULL,
  UNIQUE KEY `panel_type_id` (`panel_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `env_panel_widget`
--

CREATE TABLE IF NOT EXISTS `env_panel_widget` (
  `panel_widget_id` int(11) NOT NULL AUTO_INCREMENT,
  `panel_widget_loc` varchar(250) NOT NULL,
  `panel_widget_order` int(11) NOT NULL,
  `panel_widget_label` varchar(250) NOT NULL,
  `panel_widget_style` varchar(250) NOT NULL,
  `panel_widget_widget` int(11) NOT NULL,
  `panel_widget_panel` int(11) NOT NULL,
  `panel_widget_url` varchar(250) NOT NULL,
  `panel_widget_adjust` tinyint(1) NOT NULL,
  `panel_widget_height` int(11) NOT NULL,
  `panel_widget_color` varchar(10) NOT NULL,
  `panel_widget_html` longtext NOT NULL,
  PRIMARY KEY (`panel_widget_id`),
  KEY `widget_panel_id` (`panel_widget_panel`,`panel_widget_loc`,`panel_widget_order`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=42 ;

-- --------------------------------------------------------

--
-- Structure de la table `env_panel_widget_bookmark`
--

CREATE TABLE IF NOT EXISTS `env_panel_widget_bookmark` (
  `panel_widget_bookmark_id` int(11) NOT NULL AUTO_INCREMENT,
  `panel_widget_bookmark_name` varchar(255) NOT NULL,
  `panel_widget_bookmark_icon` int(11) NOT NULL,
  `panel_widget_bookmark_url` varchar(255) NOT NULL,
  `panel_widget_bookmark_open` int(11) NOT NULL,
  `panel_widget_bookmark_color` varchar(10) NOT NULL,
  `panel_widget_bookmark_user` int(11) NOT NULL,
  `panel_widget_bookmark_widget` int(11) NOT NULL,
  PRIMARY KEY (`panel_widget_bookmark_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Structure de la table `env_panel_widget_carousel`
--

CREATE TABLE IF NOT EXISTS `env_panel_widget_carousel` (
  `panel_widget_carousel_id` int(11) NOT NULL AUTO_INCREMENT,
  `panel_widget_carousel_html` longtext NOT NULL,
  `panel_widget_carousel_img` varchar(255) NOT NULL,
  `panel_widget_carousel_url` varchar(255) NOT NULL,
  `panel_widget_carousel_order` int(11) NOT NULL,
  `panel_widget_carousel_widget` int(11) NOT NULL,
  PRIMARY KEY (`panel_widget_carousel_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Structure de la table `env_profil`
--

CREATE TABLE IF NOT EXISTS `env_profil` (
  `profil_id` int(11) NOT NULL AUTO_INCREMENT,
  `profil_label` varchar(250) NOT NULL,
  PRIMARY KEY (`profil_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=100 ;

-- --------------------------------------------------------

--
-- Structure de la table `env_sso_attribut`
--

CREATE TABLE IF NOT EXISTS `env_sso_attribut` (
  `sso_attribut_id` int(11) NOT NULL AUTO_INCREMENT,
  `sso_attribut_name` varchar(255) NOT NULL,
  `sso_attribut_value` varchar(255) NOT NULL,
  PRIMARY KEY (`sso_attribut_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

-- --------------------------------------------------------

--
-- Structure de la table `env_sso_community`
--

CREATE TABLE IF NOT EXISTS `env_sso_community` (
  `sso_community_id` int(11) NOT NULL AUTO_INCREMENT,
  `sso_community_label` varchar(255) NOT NULL,
  PRIMARY KEY (`sso_community_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Structure de la table `env_sso_community_attribut`
--

CREATE TABLE IF NOT EXISTS `env_sso_community_attribut` (
  `sso_community_attribut_community` int(11) NOT NULL,
  `sso_community_attribut_attribut` int(11) NOT NULL,
  UNIQUE KEY `sso_community_attrribut_community` (`sso_community_attribut_community`,`sso_community_attribut_attribut`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `env_sso_profil`
--

CREATE TABLE IF NOT EXISTS `env_sso_profil` (
  `sso_profil_id` int(11) NOT NULL AUTO_INCREMENT,
  `sso_profil_label` varchar(255) NOT NULL,
  `sso_profil_community` int(11) NOT NULL,
  PRIMARY KEY (`sso_profil_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Structure de la table `env_sso_profil_attribut`
--

CREATE TABLE IF NOT EXISTS `env_sso_profil_attribut` (
  `sso_profil_attribut_profil` int(11) NOT NULL,
  `sso_profil_attribut_attribut` int(11) NOT NULL,
  UNIQUE KEY `sso_community_attrribut_community` (`sso_profil_attribut_profil`,`sso_profil_attribut_attribut`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `env_user`
--

CREATE TABLE IF NOT EXISTS `env_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_login` varchar(250) NOT NULL,
  `user_password` varchar(250) NOT NULL,
  `user_lastname` varchar(250) NOT NULL,
  `user_firstname` varchar(250) NOT NULL,
  `user_pseudo` varchar(255) NOT NULL,
  `user_avatar` varchar(250) NOT NULL,
  `user_email` varchar(250) NOT NULL,
  `user_profil` int(11) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_login` (`user_login`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

-- --------------------------------------------------------

--
-- Structure de la table `env_widget`
--

CREATE TABLE IF NOT EXISTS `env_widget` (
  `widget_id` int(11) NOT NULL AUTO_INCREMENT,
  `widget_name` varchar(255) NOT NULL,
  `widget_label` varchar(255) NOT NULL,
  `widget_icon` varchar(255) NOT NULL,
  `widget_url` varchar(255) NOT NULL,
  `widget_code` longtext NOT NULL,
  `widget_adjust` tinyint(1) NOT NULL,
  `widget_height` int(11) NOT NULL,
  `widget_style` int(11) NOT NULL,
  `widget_type` int(11) NOT NULL,
  `widget_mode` int(11) NOT NULL,
  `widget_categorie` int(11) NOT NULL,
  PRIMARY KEY (`widget_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Structure de la table `env_widget_categorie`
--

CREATE TABLE IF NOT EXISTS `env_widget_categorie` (
  `widget_categorie_id` int(11) NOT NULL AUTO_INCREMENT,
  `widget_categorie_label` varchar(255) NOT NULL,
  PRIMARY KEY (`widget_categorie_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Structure de la table `env_widget_mode`
--

CREATE TABLE IF NOT EXISTS `env_widget_mode` (
  `widget_mode_id` int(11) NOT NULL AUTO_INCREMENT,
  `widget_mode_name` varchar(255) NOT NULL,
  PRIMARY KEY (`widget_mode_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Structure de la table `env_widget_style`
--

CREATE TABLE IF NOT EXISTS `env_widget_style` (
  `widget_style_id` int(11) NOT NULL AUTO_INCREMENT,
  `widget_style_label` varchar(255) NOT NULL,
  PRIMARY KEY (`widget_style_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Structure de la table `env_widget_type`
--

CREATE TABLE IF NOT EXISTS `env_widget_type` (
  `widget_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `widget_type_name` varchar(255) NOT NULL,
  PRIMARY KEY (`widget_type_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;
